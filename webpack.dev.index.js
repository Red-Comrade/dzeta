// webpack.config.js

'use strict';

const path = require( 'path' );
const LoadablePlugin = require('@loadable/webpack-plugin')
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');

module.exports = {
    // https://webpack.js.org/configuration/entry-context/
    entry: {
        'index': './src/index.tsx',
    },

    // https://webpack.js.org/configuration/output/
    output: {
        path: path.resolve( __dirname, './public/js' ),
        filename: '[name].js',
        chunkFilename: './chunks/[name].[id].[chunkhash].js',//тут наверное все же стоит добавить еще и от какого плагина этот chunk
        publicPath: './js/'
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.scss', '.css']
    },

    module: {
        rules: [
            {
                test: /\.ts|\.tsx|\.js|\.jsx?$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.scss$/,
                exclude: /(node_modules)/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: true,
                            sourceMap: false,
                            localIdentName: '[path]___[name]__[local]___[hash:base64:5]'
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require("sass"),
                        },
                    }
                ]
            },
            {
                test: /\.css$/,
                exclude: /(node_modules)/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: true,
                            sourceMap: false,
                            localIdentName: '[path]___[name]__[local]___[hash:base64:5]'
                        }
                    },
                ]
            },
            {
                test: /\.css$/,
                include: /(node_modules)/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader"
                ]
            },
            {
                test: /\.less$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    {
                        loader: "less-loader",
                    }
                ]
            },
            {
              test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
              use: [{
                loader: 'file-loader',
                options: {
                  name: '[name].[ext]',
                  outputPath: 'fonts/'
                }
              }],
            },
            {
              test: /\.(png|jpe?g|gif)$/i,
              use: [{
                loader: 'file-loader',
                options: {
                  name: '[name].[ext]',
                  outputPath: 'img/'
                }
              }],
            }
        ]
    },
    plugins: [
        new MonacoWebpackPlugin(
          {
            'filename':'./../../public/js/monacoEditor/[name].workerCalc.js'
          }
        )
    ],

    // Useful for debugging.
    //devtool: 'source-map',
    devtool: 'inline-source-map',
    //mode: 'development', //production or development не имеет никакого значения, так как мы можем писать "build": "webpack --mode production",
    //     "build-dev": "webpack --mode development"


    // By default webpack logs warnings if the bundle is bigger than 200kb.
    performance: { hints: false }
};
