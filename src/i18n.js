import i18n from 'i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

if(document.location.host == "form.enemat.fr"  || document.location.host == "enemat.misnik.by") {
    i18n
        .use(Backend)
        .use(initReactI18next)
        .init({
            fallbackLng: 'fr',
            debug: false,
            ns: ['init', 'import'],
            defaultNS: 'init',
            saveMissing: true,
            interpolation: {
                escapeValue: false, // not needed for react as it escapes by default
            },
            react: {
                wait: true,
            }
        });
}
else {
    i18n
        .use(Backend)
        .use(LanguageDetector)
        .use(initReactI18next)
        .init({
            fallbackLng: 'en',
            debug: false,
            ns: ['init', 'import'],
            defaultNS: 'init',
            saveMissing: true,
            interpolation: {
                escapeValue: false, // not needed for react as it escapes by default
            },
            react: {
                wait: true,
            }
        });
}


export default i18n;
