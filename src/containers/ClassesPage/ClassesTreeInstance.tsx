import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import Loader from 'react-loaders';
import i18next from 'i18next';
import {WithTranslation, withTranslation} from 'react-i18next';

import CoolTree from '../../components/CoolTree'
import * as ct_api from '../../api/ClassTree-api'

import ModalTree from './ModalTree'
import ToastrStore from "../../stores/ToastrStore";
import {ClassesStore} from "../../stores/ClassesStore";


export interface IClassesTreeInstanceProps  extends WithTranslation {
    ClassesStore?: {
        treeData: object[],
        UpdTreeDataAction: (newTreeData:object[]) => void,
        AddNode: (payload:object) => void,
        SelectNode?: any,
        SelectNodeForParamsAction: () => void,
        ActiveGroupID: any,
        IsOpenTable: boolean,
    },
}
interface IClassesTreeInstanceState {
    Settings: object;
    CM_target: HTMLElement;
    CM_content: object[];
    ShowFromLevelFunc?: (callback:(NodeID?)=>void)=> void,
    expandNodeFunc?: (callback:()=>void)=> string[],
    selectNodeFunc?: (callback:()=>void)=> string[],

    ModalTreeConfig: {
        show: boolean,
        TypeModal: string,
        data?: object,
        SuccessFunction?: (node?) => void
    },

    AddBtnDisabled?: boolean;
    EditBtnDisabled?: boolean;
    DelBtnDisabled?: boolean;

    CopyNode: any,
}

@inject("ClassesStore")
@observer
class ClassesTreeInstanceView extends React.Component<IClassesTreeInstanceProps, IClassesTreeInstanceState> {
    static defaultProps = {
        isOpen: null,
        isFullLoad: true, //
        treeData:[],
    };
    state: Readonly<IClassesTreeInstanceState> = {
        Settings: null,
        CM_target: null,
        CM_content: [
            {
                "label": "Создать",
                "action": ()=> {},
                "icon_className": 'fa fa-plus',
                "children": [
                    {
                        "label": "Create",
                        "action": ()=> {},
                        "icon_className": 'glyphicon glyphicon-th',
                        "children": false,
                    },
                    {
                        "label": "Edit",
                        "action": ()=> {},
                        "icon_className": 'glyphicon glyphicon-th',
                        "children": false
                    },
                    {
                        "label": "Delete",
                        "action": ()=> {},
                        "icon_className": 'glyphicon glyphicon-th',
                        "children": false
                    }
                ]
            }
        ],
        ShowFromLevelFunc: null,

        ModalTreeConfig: {
            show: false,
            TypeModal: "create",
            data: {}
        },

        AddBtnDisabled: false,
        EditBtnDisabled: false,
        DelBtnDisabled: false,

        CopyNode: null,
    };

    constructor(props: IClassesTreeInstanceProps) {
        super(props);
    };
    componentDidMount(): void {
        this.core.GetTree((data_tree) => {

            //МНЕ Проще написать функцию преобразования, чем разбираться в коде на PHP, нихрена не понятно...

            let expandedKeys = [];

            const convertTree = (classes, parentTree, parent) => {
                const tree = [];
                for (let key in classes) {
                    const _class = classes[key];
                    const node = Object.assign({}, _class);
                    node.Name = _class.name;
                    node.id = parentTree + '|cl|' + _class.uid;
                    node.ParentIDTree = parentTree;
                    node.key = node.id;

                    if (parentTree) {
                        node.icon = ClassesStore.CLASS_TREE_ICON;
                        node.isRoot = false;
                    } else {
                        node.key = '|0';
                        node.id = '|0';
                        expandedKeys.push(node.id);
                        node.icon = ClassesStore.ROOT_TREE_ICON;
                        node.isRoot = true;
                    }
                    node.ParentID = parent;
                    node.iddb = _class.uid;
                    node.uid = _class.uid;
                    node.ParrentTypeID = parent;
                    node.TypeID = _class.uid;
                    node.type = ClassesStore.CLASS_TYPE;
                    node.groups = _class.groups;
                    node.children = [
                        {
                            Name: i18next.t('init:general.entities.classes'),
                            icon: ClassesStore.CLASSES_FOLDER_TREE_ICON,
                            id: node.id + '|cl',
                            key: node.id + '|cl',
                            type: ClassesStore.CLASSES_FOLDER_TYPE,
                            ParentIDTree: parentTree,
                            children: null,
                        },
                        {
                            Name: i18next.t('init:general.entities.methods'),
                            icon: ClassesStore.METHODS_FOLDER_TREE_ICON,
                            id: node.id + '|m',
                            key: node.id + '|m',
                            type: ClassesStore.METHODS_FOLDER_TYPE,
                            TypeID: _class.uid,
                            ParentIDTree: parentTree,
                            children: null
                        }
                    ];

                    if (_class.children.length != 0 || _class.calculations.length != 0) {
                        if (_class.children.length > 0) {
                            node.children[0].children = convertTree(_class.children, node.id, node.iddb);
                        } else {
                            node.children[0].children = null;
                        }
                        if (_class.calculations.length > 0) {
                            node.children[1].children = [];
                            for (let k in _class.calculations) {
                                const method = Object.assign({}, _class.calculations[k]);
                                method.Name = method.name;
                                method.iddb = method.uid;
                                method.icon = ClassesStore.METHOD_TREE_ICON;
                                method.id = node.id + '|m|' + method.uid;
                                method.key = parentTree + '|m|' + method.uid;
                                method.type = ClassesStore.METHOD_TYPE;
                                method.ParentIDTree = parentTree;
                                method.ParrentTypeID = parent;
                                method.parent = node.uid;
                                method.children = null;
                                node.children[1].children.push(method)
                            }
                        } else {
                            node.children[1].children = null;
                        }
                    }

                    tree.push(node);
                }

                return tree;
            }
            const tree = convertTree(data_tree, "", "");

            let testTreeData = [
                {
                    icon: false,
                    id: "12321",
                    iddb: 100,
                    key: 1233,
                    Name: "ClassesTree",
                    children: [
                        {
                            icon: false,
                            iddb: 1001,
                            children: [{
                                iddb: 101132,
                                icon: "fas fa-folder",
                                children: false,
                                id: 121233,
                                key: 122133,
                                Name: "TE213ST"

                            },],
                            id: 123,
                            key: 123,
                            Name: "TEST123"

                        },
                        {
                            iddb: 1001,
                            icon: false,
                            children: false,
                            id: 1232,
                            key: 1233,
                            Name: "TEST123"

                        }
                    ]
                },

            ];
            this.props.ClassesStore.UpdTreeDataAction(tree);

            let FuncExpand = () => {
                this.setState({expandNodeFunc: null});
                return expandedKeys;
            };

            this.setState({expandNodeFunc: FuncExpand});
        });
    };


    core = {
        GetTree: function (SuccFunc) {
            ct_api
                .getTree()
                .then(res => {
                    console.error(res);
                    SuccFunc(res.data);
                })
                .catch(function (error) {
                    console.log(error);
                })
        },

        addLink: function (type, type_parent, SuccFunc, ErrFunc) {
            ct_api
                .addLink(type, type_parent)
                .then(res => {
                    console.error(res);
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc(res.data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        deleteAllObjs: function (type, SuccFunc, ErrFunc) {
            ct_api
                .deleteAllObjs(type)
                .then(res => {
                    console.error(res);
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc(res.data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    };


    render() {
        const DataCoolTree = {
            RootTab: {
                ID: "T00000000-0000-0000-0000-000000000000",
                Name: i18next.t('init:general.entities.root'),
            },

            treeData: this.props.ClassesStore.treeData,
            isFullLoad: true,
            expandNodeFunc: this.state.expandNodeFunc,
            selectNodeFunc: this.state.selectNodeFunc,
            ShowFromLevelFunc: this.state.ShowFromLevelFunc,

            //Кнопки
            AddBtnDisabled: this.state.AddBtnDisabled,
            EditBtnDisabled: this.state.EditBtnDisabled,
            DelBtnDisabled: this.state.DelBtnDisabled,
            OnAddBtnFunc: (selectData) => {
                this.setState({
                    ModalTreeConfig: {
                        show: true,
                        TypeModal: "CreateClass",
                        data: {}
                    }
                });
            },
            OnEditBtnFunc: (selectData) => {
                this.setState({
                    ModalTreeConfig: {
                        show: true,
                        TypeModal: "Edit",
                        data: {}
                    }
                });
            },
            OnDelBtnFunc: (selectData) => {
                this.setState({
                    ModalTreeConfig: {
                        show: true,
                        TypeModal: "Delete",
                        data: {}
                    }
                });
            },

            onSelect: (selectData) => {
                if(selectData) {
                    this.props.ClassesStore.SelectNode = selectData;
                    this.props.ClassesStore.ActiveGroupID = "00000000-0000-0000-0000-000000000000";
                    if(selectData.id == "T00000000-0000-0000-0000-000000000000") {
                        //Проверка на корень
                        location.hash = `/classes/T0/0`;
                        this.props.ClassesStore.IsOpenTable = false;
                    }
                    else {
                        location.hash = `/classes/${selectData.id}/0`;
                        this.props.ClassesStore.SelectNodeForParamsAction();
                    }

                    if(selectData.isRoot) {
                        //root
                        this.setState({
                            AddBtnDisabled: false,
                            EditBtnDisabled: true,
                            DelBtnDisabled: true,
                        });
                    }
                    else {
                        this.setState({
                            AddBtnDisabled: false,
                            EditBtnDisabled: false,
                            DelBtnDisabled: false,
                        });
                    }

                }
                else {
                    location.hash = `/classes/`;
                }
            },
            onSelectR: (selectData) => {
                if(selectData) {
                    this.props.ClassesStore.SelectNode = selectData;
                }
            },
            onOpenCM: (params: {SelectData:any, callbackCloseCM:()=>void, IsBtnCM?:boolean}, callback) => {
                let contentCM = [];
                let contentCMothers = [];
                if (params.SelectData && params.SelectData.type) {
                    if (params.SelectData.type == ClassesStore.CLASS_TYPE) {
                        if(!params.IsBtnCM) {
                            //Контекстное меню от кнопки
                            contentCM.push({
                                "label": i18next.t('classes:operations.create_class'),
                                "action": ()=> {
                                    this.setState({
                                        ModalTreeConfig: {
                                            show: true,
                                            TypeModal: "CreateClass",
                                            data: {
                                            },
                                            SuccessFunction: (node) => {}
                                        }
                                    });

                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-file-invoice',
                                "children": false
                            });
                            contentCM.push({
                                "label": i18next.t('init:general.operations.create_folder'),
                                "action": ()=> {

                                    this.setState({
                                        ModalTreeConfig: {
                                            show: true,
                                            TypeModal: "create folder",
                                            data: {}
                                        }
                                    });
                                    /*
                                    props.ShowModalAction({
                                        Name: "ModalCreateNode",
                                        data: {
                                            Type: "folder",
                                            ParentIDTree: selectData.id
                                        }
                                    });
                                    */
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fa fa-folder',
                                "children": false
                            });

                            if(params.SelectData.uid !== "00000000-0000-0000-0000-000000000000") {
                                contentCM.push({
                                    "label": i18next.t('init:general.operations.edit'),
                                    "action": ()=> {
                                        this.setState({
                                            ModalTreeConfig: {
                                                show: true,
                                                TypeModal: "Edit",
                                                data: {}
                                            }
                                        });
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-edit',
                                    "children": false
                                });
                                contentCM.push({
                                    "label": i18next.t('init:general.operations.delete'),
                                    "action": ()=> {
                                        this.setState({
                                            ModalTreeConfig: {
                                                show: true,
                                                TypeModal: "Delete",
                                                data: {}
                                            }
                                        });
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-times',
                                    "children": false
                                });
                            }
                        }
                        else {

                        }
                        if(params.SelectData.uid !== "00000000-0000-0000-0000-000000000000") {
                            contentCMothers.push({
                                "label": i18next.t('init:general.operations.copy'),
                                "action": ()=> {
                                    this.setState({
                                        CopyNode: params.SelectData,
                                    });

                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-copy',
                                "children": false
                            });


                            let isDisabledPaste = true;
                            let NamePaste = i18next.t('init:general.operations.paste');
                            if(this.state.CopyNode) {
                                NamePaste = i18next.t('init:general.operations.paste') + " (" + i18next.t('init:general.operations.copy') + ") (\'" + this.state.CopyNode.Name + "\')";
                                isDisabledPaste = false;
                            }

                            contentCMothers.push({
                                "label": NamePaste,
                                "disabled" : isDisabledPaste,
                                "action": ()=> {
                                    if(!isDisabledPaste) {
                                        if(this.state.CopyNode) {
                                            this.core.addLink(this.state.CopyNode.TypeID, params.SelectData.TypeID,(data) => {
                                                ToastrStore.success('Success', 'class added successfully!');
                                                this.props.ClassesStore.AddNode({
                                                    Name: this.state.CopyNode.Name,
                                                    icon: ClassesStore.CLASS_TREE_ICON,

                                                    ParentID: params.SelectData.TypeID,
                                                    iddb: this.state.CopyNode.TypeID,

                                                    ParrentTypeID: params.SelectData.TypeID,
                                                    TypeID: this.state.CopyNode.TypeID,
                                                });

                                                this.setState({CopyNode: null});

                                            }, () => {

                                            })
                                        }
                                    }

                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-paste',
                                "children": false
                            });
                            contentCMothers.push({
                                "label": i18next.t('classes:operations.delete_all_objs'),
                                "disabled" : false,
                                "action": ()=> {

                                    this.setState({
                                        ModalTreeConfig: {
                                            show: true,
                                            TypeModal: "DeleteAllObjs",
                                            data: {}
                                        }
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-times',
                                "children": false
                            });


                            if(contentCMothers.length > 0) {
                                contentCM.push({
                                    "label": i18next.t('init:general.more'),
                                    "action": ()=> {},
                                    "icon_className": 'fa fa-ellipsis-h',
                                    "children": contentCMothers
                                });
                            }

                            contentCM.push({
                                "label": i18next.t('init:general.operations.current_level'),
                                "action": ()=> {
                                    this.setState({
                                        ShowFromLevelFunc: (
                                            (callback) => {
                                                this.setState({ShowFromLevelFunc: null});
                                                callback(params.SelectData.id);
                                            })
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'far fa-eye',
                                "children": false
                            });
                        }
                    } else if (params.SelectData.type == ClassesStore.METHODS_FOLDER_TYPE) {
                        contentCM.push({
                            "label": i18next.t('classes:operations.create_method'),
                            "action": () => {
                                this.setState({
                                    ModalTreeConfig: {
                                        show: true,
                                        TypeModal: "CreateMethod",
                                        data: {},
                                        SuccessFunction: (node) => {
                                            this.setState({
                                                selectNodeFunc: () => {
                                                    this.setState({ selectNodeFunc: null });
                                                    return node.id;
                                                }
                                            });
                                            DataCoolTree.onSelect(node);
                                        }
                                    }
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": ClassesStore.METHOD_TREE_ICON,
                            "children": false
                        });
                    } else if (params.SelectData.type == ClassesStore.METHOD_TYPE) {
                        contentCM.push({
                            "label": i18next.t('init:general.operations.new_tab'),
                            "action": () => {
                                window.open('/calc?class=' + (params.SelectData.TypeID ? params.SelectData.TypeID : '') + '&uid=' + params.SelectData.uid, '_blank');
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-plus-square',
                            "children": false
                        });
                        contentCM.push({
                            "label": i18next.t('init:general.operations.delete'),
                            "action": () => {
                                this.setState({
                                    ModalTreeConfig: {
                                        show: true,
                                        TypeModal: "DeleteMethod",
                                        data: {}
                                    }
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-times',
                            "children": false
                        });
                    }
                }
                return contentCM;
            },
            onExpandNode: (node_info) => {},
        };

        const dataModalTree = {
            CloseCallback: () => { this.state.ModalTreeConfig.show = false; this.setState({ModalTreeConfig: this.state.ModalTreeConfig})},
            SuccessFunction: this.state.ModalTreeConfig.SuccessFunction,
            show: this.state.ModalTreeConfig.show,
            TypeModal: this.state.ModalTreeConfig.TypeModal,
            data: this.state.ModalTreeConfig.data,
        };

        return (<div id="zt_FilterSidebar_body_co">
            <CoolTree {...DataCoolTree} />
            <ModalTree {...dataModalTree}/>
            </div>
        );
    }
}

const ClassesTreeInstanceTranslation = withTranslation(['init', 'classes'])(ClassesTreeInstanceView);

export default function ClassesTreeInstance(props) {
    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <ClassesTreeInstanceTranslation {...props} />
        </React.Suspense>
    );
}