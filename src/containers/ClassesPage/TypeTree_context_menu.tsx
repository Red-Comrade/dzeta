import * as React from "react";
import * as ReactDOM from "react-dom";

import './TypeTree_context_menu.scss';

//пункт меню по умолчанию
const def_item = {
    "separator_before": false,
    "separator_after": false,
    "label": "#Пусто#",
    "action": ()=>{}, //callback(data)
    "icon_className": '',
    "children": []
};

export interface ITypeTree_context_menuProps {
    className?: string;
    content: any;
    _ref?: (ref:HTMLInputElement) => void;
}
interface ITypeTree_context_menuState {
    hovers: any
}

export default class TypeTree_context_menu extends React.Component<ITypeTree_context_menuProps, ITypeTree_context_menuState> {
    constructor(props) {
        super(props);
    }
    state: Readonly<ITypeTree_context_menuState> = {
        hovers: []
    };

    private stepRef: React.RefObject<HTMLInputElement>;

    _ref: HTMLDivElement;

    componentDidMount = () => {

    };

    onMouseEnterHandler = (e, data) => {
        let hovers = this.state.hovers;

        hovers.splice(0,0);



        if(data.level == 0 || hovers[data.level-1] != undefined) {
            hovers[data.level] = data.index;
            hovers.splice(data.level+1);
        }

        this.setState({
            hovers: hovers
        });
    };
    onMouseLeaveHandler = (e, data) => {
        let hovers = this.state.hovers;
        hovers.splice();

        if(e.relatedTarget!== undefined && e.relatedTarget !== window) {
            if(this._ref.contains(e.relatedTarget)) {
                hovers.splice(data.level);
                this.setState({
                    hovers: hovers
                });
            }
        }
    };

    render() {
        const GenerateMenuRec = (content, level) => {
            return content.map((item, index) => {

                item = Object.assign({}, def_item, item);

                let tree = [];

                let isHoverClass = this.state.hovers[level] === index ? 'hover_TypeTreeCM_li' : '';

                if(item.separator_before) {
                    tree.push(
                        <li key={(level+'_'+index + '_sb')} className="TypeTreeCM_separator">
                            <a className="TypeTreeCM_a" href="#">&nbsp;</a>
                        </li>);
                }

                if(item.children.length > 0) {
                    tree.push(
                        <li className={`TypeTreeCM_li TypeTreeCM_co_sub ${isHoverClass}`}
                            onClick={()=>{console.log('li_click')}}
                            onMouseEnter={(e)=>{this.onMouseEnterHandler(e, {level: level, index: index})}}
                            onMouseLeave={(e)=>{this.onMouseLeaveHandler(e, {level: level, index: index})}}
                            key={(level+'_'+index)} >
                            <a className="TypeTreeCM_a" onClick={item.action} >
                                <i className={`${item.icon_className} TypeTreeCM_a_i`} />
                                <span className="TypeTreeCM_a_separator">&nbsp;</span>
                                {item.label}
                                <span className="TypeTreeCM_children_span" >»</span>
                            </a>
                            <ul className="TypeTreeCM_ul">{GenerateMenuRec(item.children, (level+1))}</ul>
                        </li>);
                }
                else {
                    tree.push(
                        <li className={`TypeTreeCM_li ${isHoverClass}`}
                            onClick={()=>{console.log('li_click')}} key={(level+'_'+index)}
                            onMouseEnter={()=>{this.onMouseEnterHandler(this, {level: level, index: index})}}
                            onMouseLeave={()=>{this.onMouseLeaveHandler(this, {level: level, index: index})}}
                        >
                            <a className="TypeTreeCM_a"  onClick={item.action} >
                                <i className={`${item.icon_className} TypeTreeCM_a_i`} />
                                <span className="TypeTreeCM_a_separator">&nbsp;</span>
                                {item.label}
                            </a>
                        </li>);
                }

                if(item.separator_after) {
                    tree.push(
                        <li key={(level+'_'+index + '_sa')} className="TypeTreeCM_separator">
                            <a className="TypeTreeCM_a" href="#">&nbsp;</a>
                        </li>);
                }

                return tree;
            });
        };

        let _className = this.props.className ? this.props.className : "";

        return (
            <div className={"TypeTreeCM_wrap " + _className} ref={(ref)=> {
                this.props._ref(ref as HTMLInputElement);
                this._ref = ref;
            }}>
                <ul className="TypeTreeCM_ul">
                    {GenerateMenuRec(this.props.content, 0)}
                </ul>
            </div>
        );
    }
}