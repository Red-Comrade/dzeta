import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';
import {inject, observer} from "mobx-react";

import * as ct_api from "../../api/ClassTree-api";
import * as c_api from "../../api/Calc-api";
import ToastrStore from "../../stores/ToastrStore";
import {ClassesStore} from "../../stores/ClassesStore";
import {number} from "prop-types";

interface IModalTreeProps {
    show:boolean;
    TypeModal: string; //create
    CloseCallback: () => void,
    SuccessFunction?: () => void,

    data?: {
        TypeName?: string,
    };

    ClassesStore?: {
        treeData: object[],
        UpdTreeDataAction: (newTreeData:object[]) => void,
        AddNode: (payload:object) => void,
        RenameNode: (payload:object) => void,
        DeleteNode: (payload:object) => void,
        SelectNode?: any,
    },
    ToastrStore?: any;
}
interface IModalTreeState {
    blocking: boolean
    TypeName?: string
    count: number
}

@inject("ClassesStore")
@inject("ToastrStore")
@observer
class ModalTreeView extends React.Component<IModalTreeProps,IModalTreeState> {
    state: Readonly<IModalTreeState> = {
        blocking: false,
        TypeName: "",
        count: 0,
    };
    _ref_inp_type: HTMLElement;

    constructor(props) {
        super(props);
    }

    core = {
        addNode: (name:string, parent:number, SuccFunc) => {
            ct_api
                .addNode(name, parent)
                .then(res => {
                    SuccFunc(res.data);
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },
        renameNode: (uid:string, name:string, SuccFunc) => {
            ct_api
                .renameNode(uid,name)
                .then(res => {
                    SuccFunc(res.data);
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },
        deleteNode: (uid, type_parent, SuccFunc) => {
            ct_api
                .deleteNode(uid, type_parent)
                .then(res => {
                    SuccFunc(res.data);
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },

        deleteAllObjs: function (type, SuccFunc, ErrFunc) {
            ct_api
                .deleteAllObjs(type)
                .then(res => {
                    console.error(res);
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc(res.data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        deleteAllObjsCount: function (type, SuccFunc, ErrFunc) {
            ct_api
                .deleteAllObjsCount(type)
                .then(res => {
                    console.error(res);
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc(res.data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        addMethodNode: (name:string, parent:string, SuccFunc) => {
            c_api
                .SaveCalc('$x = 0', parent, '', name)
                .then(res => {
                    SuccFunc(res.data);
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },
        deleteMethodNode: (uid:string, SuccFunc) => {
            c_api
                .DeleteCalc(uid)
                .then(res => {
                    SuccFunc(res.data);
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        }
    };


    handleCreateClass = () => {
        let TypeName = this.state.TypeName;
        if(TypeName.trim()) {
            var ParentID = this.props.ClassesStore.SelectNode.TypeID;
            this.setState({blocking: true})
            this.core.addNode(TypeName, ParentID, (data) => {
                if(data.result === 1) {
                    this.props.ToastrStore.success('Success', 'Class added successfully!');
                    let iddb = data.data.uid;
                    this.props.ClassesStore.AddNode({
                        Name: TypeName,
                        icon: ClassesStore.CLASS_TREE_ICON,

                        ParentID: ParentID,
                        iddb: iddb,

                        ParrentTypeID: ParentID,
                        TypeID: iddb,
                    });

                    this.props.CloseCallback();
                }
                else 
                {
                    for(let i=0;i<data.errors.length;i++)
                    {
                        this.props.ToastrStore.error(data.errors[i].text);
                    }                    
                }
                this.setState({blocking: false})
            });
        }
    };
    handleEdit = () => {
        let newName = this.state.TypeName;
        if(newName.trim()) {
            let iddb = this.props.ClassesStore.SelectNode.TypeID;
            this.setState({blocking: true})
            this.core.renameNode(iddb, newName,(data) => {
                if(data.result === 1) {
                    this.props.ToastrStore.success('Success', 'Class renamed successfully!');
                    this.props.ClassesStore.RenameNode({
                        iddb: iddb,
                        newName: newName
                    });

                    this.props.CloseCallback();
                }
                else 
                {
                    for(let i=0;i<data.errors.length;i++)
                    {
                        this.props.ToastrStore.error(data.errors[i].text);
                    }
                }
                this.setState({blocking: false})
            });
        }
    };
    handleDelete = () => {
        let iddb = this.props.ClassesStore.SelectNode.TypeID;
        let type_parent = this.props.ClassesStore.SelectNode.ParrentTypeID;
        this.setState({blocking: true});
        this.core.deleteNode(iddb, type_parent, (data) => {
            if(data.result === 1) {
                this.props.ToastrStore.success('Success', 'Class deleted successfully!');
                this.props.ClassesStore.DeleteNode({
                    iddb: iddb,
                    ParrentTypeID: type_parent
                });

                this.props.CloseCallback();
            }
            else {
                let text_err = '';
                if(data.errors) {
                    text_err = data.errors.map((item) => { return item.text}).join(', ');
                }
                this.props.ToastrStore.error(text_err, 'ERROR CODE:' + data.result + ' ');
            }
            this.setState({blocking: false})
        });
    };
    handleDeleteAllObjs = () => {
        let iddb = this.props.ClassesStore.SelectNode.TypeID;
        let type_parent = this.props.ClassesStore.SelectNode.ParrentTypeID;
        this.setState({blocking: true});

        this.core.deleteAllObjs(iddb, (data) => {
            ToastrStore.success('Success', 'Class added successfully!');
            this.setState({blocking: false})
            this.props.CloseCallback();

        }, () => {

        });
    };
    handleCreateMethod = () => {
        let MethodName = this.state.TypeName;
        if (MethodName.trim()) {
            var ParentID = this.props.ClassesStore.SelectNode.TypeID;
            this.setState({ blocking: true })
            this.core.addMethodNode(MethodName, ParentID, (data) => {
                if (data.result === 1) {
                    this.props.ToastrStore.success('Success', 'Method added successfully!');
                    let iddb = data.data.uid;
                    let methodNode = this.props.ClassesStore.AddMethodNode({
                        Name: MethodName,
                        icon: ClassesStore.CLASS_TREE_ICON,

                        parent: ParentID,
                        iddb: iddb,

                        ParrentTypeID: ParentID,
                        TypeID: ParentID,
                    });
                    if (this.props.SuccessFunction) {
                        this.props.SuccessFunction(methodNode);
                    }
                    this.props.CloseCallback();
                } else {
                    for (let i = 0; i < data.errors.length; i++) {
                        this.props.ToastrStore.error(data.errors[i].text);
                    }
                }
                this.setState({blocking: false})
            });
        }
    };
    handleDeleteMethod = () => {
        let iddb = this.props.ClassesStore.SelectNode.uid;
        let type_parent = this.props.ClassesStore.SelectNode.ParrentTypeID;
        this.setState({blocking: true});
        this.core.deleteMethodNode(iddb, (data) => {
            if(data.result === 1) {
                this.props.ToastrStore.success('Success', 'Method deleted successfully!');
                this.props.ClassesStore.DeleteNode({
                    iddb: iddb,
                    ParrentTypeID: type_parent
                });

                this.props.CloseCallback();
            }
            else {
                let text_err = '';
                if(data.errors) {
                    text_err = data.errors.map((item) => { return item.text}).join(', ');
                }
                this.props.ToastrStore.error(text_err, 'ERROR CODE:' + data.result + ' ');
            }
            this.setState({blocking: false})
        });
    };

    handleClose = () => {
        this.props.CloseCallback();
    };
    ChangeInput = (e) => {
        this.setState({
            TypeName: e.target.value,
        });
    };


    onShow = () => {
        let TypeName = "";
        if (this.props.TypeModal == "CreateClass" || this.props.TypeModal == "Edit") {
            this._ref_inp_type.focus();
            this.setState({TypeName: TypeName, blocking: false});
        }
        else if(this.props.TypeModal == "Edit") {
            TypeName = this.props.ClassesStore.SelectNode.Name;
            this.setState({TypeName: TypeName, blocking: false});
        }
        else if (this.props.TypeModal == "DeleteAllObjs") {
            this.setState({blocking: true});

            let iddb = this.props.ClassesStore.SelectNode.TypeID;
            this.core.deleteAllObjsCount(iddb, (data) => {
                this.setState({blocking: false, count: data.count})
            }, () => {
            });

        }
        else {
            this.setState({TypeName: TypeName, blocking: false});
        }

    };

    render() {
        let Title = i18next.t('classes:operations.create_class');
        if (this.props.TypeModal == "CreateClass") {
            Title = i18next.t('classes:operations.create_class');
        }
        else if (this.props.TypeModal == "Edit") {
            Title = i18next.t('classes:operations.edit_class');
        }
        else if(this.props.TypeModal == "Delete") {
            Title = i18next.t('init:general.operations.delete') + ' \"' + this.props.ClassesStore.SelectNode.Name +'\"';
        }
        else if(this.props.TypeModal == "DeleteAllObjs") {
            Title = i18next.t('classes:operations.delete_all_objs') + ' \"' + this.props.ClassesStore.SelectNode.Name +'\"';
        }
        else if (this.props.TypeModal == "CreateMethod") {
            Title = i18next.t('classes:operations.create_method');
        }
        else if (this.props.TypeModal == "DeleteMethod") {
            Title = i18next.t('classes:operations.delete_method');
        }

        let Body = <div/>;
        if (this.props.TypeModal == "CreateClass" || this.props.TypeModal == "Edit") {
            Body = <div>
                {i18next.t('init:general.labels.enter_name')}
                <input
                    key={"ModalCreateNodeTypeName"} ref={(ref) => (this._ref_inp_type = ref)}
                    onChange={this.ChangeInput}
                    value={this.state.TypeName}
                    className="form-control"/>
            </div>
        }
        else if(this.props.TypeModal == "Delete") {
            Body = <div>
                {i18next.t('init:general.messages.want_delete')} "{this.props.ClassesStore.SelectNode.Name}"?
            </div>
        }
        else if(this.props.TypeModal == "DeleteAllObjs") {
            Body = <div>
                {i18next.t('init:general.messages.want_delete')} {this.state.count} {i18next.t('init:general.entities.genitive.objects')}?
            </div>
        }
        else if(this.props.TypeModal == 'CreateMethod') {
            Body = <div>
                {i18next.t('init:general.labels.enter_name')}
                <input
                    key={"ModalCreateNodeTypeName"} ref={(ref) => (this._ref_inp_type = ref)}
                    onChange={this.ChangeInput}
                    value={this.state.TypeName}
                    className="form-control"/>
            </div>
        }
        else if(this.props.TypeModal == "DeleteMethod") {
            Body = <div>
                {i18next.t('init:general.messages.want_delete')} "{this.props.ClassesStore.SelectNode.Name}"?
            </div>
        }

        let Footer = <div/>;
        if (this.props.TypeModal == "CreateClass") {
            Footer = <div>
                <Button variant="success" onClick={this.handleCreateClass}>
                    {i18next.t('init:general.operations.create')}
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    {i18next.t('init:general.operations.close')}
                </Button>
            </div>;
        }
        else if (this.props.TypeModal == "Edit") {
            Footer = <div>
                <Button variant="success" onClick={this.handleEdit}>
                    {i18next.t('init:general.operations.save')}
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    {i18next.t('init:general.operations.close')}
                </Button>
            </div>;
        }
        else if(this.props.TypeModal == "Delete") {
            Footer = <div>
                <Button variant="danger" onClick={this.handleDelete}>
                    {i18next.t('init:general.operations.delete')}
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    {i18next.t('init:general.operations.close')}
                </Button>
            </div>;
        }
        else if(this.props.TypeModal == "DeleteAllObjs") {
            Footer = <div>
                <Button variant="danger" onClick={this.handleDeleteAllObjs}>
                    {i18next.t('init:general.operations.delete_all')}
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    {i18next.t('init:general.operations.close')}
                </Button>
            </div>;
        }
        else if (this.props.TypeModal == "CreateMethod") {
            Footer = <div>
                <Button variant="success" onClick={this.handleCreateMethod}>
                    {i18next.t('init:general.operations.create')}
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    {i18next.t('init:general.operations.close')}
                </Button>
            </div>;
        }
        else if(this.props.TypeModal == "DeleteMethod") {
            Footer = <div>
                <Button variant="danger" onClick={this.handleDeleteMethod}>
                    {i18next.t('init:general.operations.delete')}
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    {i18next.t('init:general.operations.close')}
                </Button>
            </div>;
        }


        return (
            <Modal onShow={this.onShow} animation={true} show={this.props.show} onHide={this.handleClose}>
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}

const ModalTreeViewTranslation = withTranslation(['init', 'classes'])(ModalTreeView);

export default function ModalTree(props) {
    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <ModalTreeViewTranslation {...props} />
        </React.Suspense>
    );
}