import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect, useParams, useRouteMatch } from 'react-router-dom'
import { inject, observer } from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import i18next from 'i18next';
import {WithTranslation, withTranslation} from 'react-i18next';

import CoolTable from '../../components/CoolTable'
import HZTabs from "../../components/HZTabs";
import HZContextMenu from "../../components/HZContextMenu";
import * as ct_api from '../../api/ClassTree-api'
import ModalAttribute from './ModalAttribute'
import {ClassesStore} from "../../stores/ClassesStore";
import RootStore from "../../stores/RootStore";
import CalcMain from '../CalcPage/CalcPage';
import HelperModal from "./HelperModal";

export interface IClassesPageProps extends WithTranslation {
    ToastrStore?: any,
    ClassesStore: {
        ActiveGroupID?: string,
        IsOpenTable: boolean,
        IsGetDataForTab: boolean,
        IsLoadParams: boolean,
        SelectNodeForParams: any,
        SelectNodeForParamsAction: () => void,
        UpdParamsTrigAction: (isLoading:boolean, data:any) => void,
        SelectGroupAction: (item?:string) => void,
        AttrData?: any[],
        SelectTr?: any,
    },

}
interface IClassesPageState {
    blocking: boolean;
    ModalAttributeConfig: {
        show: boolean,
        TypeModal: string,
        data?: object,
    },

    HelperModalData: {
        show: boolean,
        TypeModal: "SortParams" | "other",
        AttrData?: any[],
        SelectNodeForParams?: any,
    },


    RecalcStyleFunc?: () => void,
}
@inject("ToastrStore")
@inject("ClassesStore")
@observer
class ClassesPageView extends React.Component<IClassesPageProps, IClassesPageState> {
    state: Readonly<IClassesPageState> = {
        blocking: false,
        ModalAttributeConfig: {
            show: false,
            TypeModal: "add",
            data: {},
        },
        HelperModalData: {
            show: false,
            TypeModal: "SortParams",
        }
    };

    constructor(props: IClassesPageProps) {
        super(props);
    };
    componentDidUpdate(prevProps: Readonly<IClassesPageProps>, prevState: Readonly<IClassesPageState>, snapshot?: any): void {
        if(this.props.ClassesStore.IsGetDataForTab) {
            //this.props.ClassesStore.IsGetDataForTab = false;
            //this.props.ClassesStore.AttrData = [];
            //this.props.ClassesStore.IsOpenTable = false;

            this.props.ClassesStore.UpdParamsTrigAction(true, []);

            let iddb =  this.props.ClassesStore.SelectNodeForParams.iddb;



            let GroupID = this.props.ClassesStore.ActiveGroupID == "0" ? "00000000-0000-0000-0000-000000000000" : this.props.ClassesStore.ActiveGroupID;

            location.hash = `/classes/${this.props.ClassesStore.SelectNodeForParams.id}/${this.props.ClassesStore.ActiveGroupID}`;


            this.core.getAttributesOfGroup(iddb, GroupID, (data) => {
                if(data.result === 1) {
                    console.log(data)
                    this.props.ClassesStore.UpdParamsTrigAction(false, data.data);

                    //this.props.ClassesStore.AttrData = data.data;
                }
                else {
                    this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
                    this.props.ClassesStore.UpdParamsTrigAction(false, []);

                }
            });
        }
    }

    componentWillMount(): void {
        document.title = i18next.t('init:general.entities.classes') + ' | ' + RootStore.config.nameSystem;
        document.addEventListener("zt_resize", this.resizeTable);
    };
    componentWillUnmount(): void {
        document.removeEventListener("zt_resize", this.resizeTable);
    }

    resizeTable = () => {
        this.setState({RecalcStyleFunc: () => {
                this.setState({RecalcStyleFunc: null});
            }})
    };

    core = {
        getAttributesOfGroup: (type:string,group:string, SuccFunc) => {
            ct_api
                .getAttributesOfGroup(type,group)
                .then(res => {
                    SuccFunc(res.data);
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },
        handleClickGroup: (e, GroupID) => {
            if(this.props.ClassesStore.ActiveGroupID == GroupID) {
                //Пока будет аякс выгрузка
            }
            else {

            }

            this.props.ClassesStore.SelectGroupAction(GroupID);
        },
    };

    events = {
        handleClickAddAttribute: () => {

            this.setState({
                ModalAttributeConfig: {
                    show: true,
                    TypeModal: "add",
                    data: {
                    },
                }
            });
        },
        handleClickEditAttribute: () => {
            this.setState({
                ModalAttributeConfig: {
                    show: true,
                    TypeModal: "edit",
                    data: {
                    },
                }
            });
        },
        handleClickRemoveAttribute: () => {
            this.setState({
                ModalAttributeConfig: {
                    show: true,
                    TypeModal: "del",
                    data: {
                    },
                }
            });
        },
    };

    RenderHZTabs = () => {
        let DataTabs = [];
        let classActive = "active";
        const SelectNode = this.props.ClassesStore.SelectNodeForParams;

        if(SelectNode) {
            let MainGroupID = "00000000-0000-0000-0000-000000000000";
            DataTabs.push({
                className: MainGroupID==this.props.ClassesStore.ActiveGroupID?classActive:"",
                Name: "Main",
                key: MainGroupID,
                onClick: (e) => { this.core.handleClickGroup(e, MainGroupID) },
                onContextMenu: (e) => {
                    console.error(e,'cclick');
                    e.preventDefault();
                    return false;
                }
            });

            SelectNode.groups.forEach( (item, key) => {

            });




            DataTabs.push({
                className: "",
                Name: <i className="fa fa-plus"/>,
                key: "add_group",
                onClick: (e)=>{
                    // this.core.AddGroupAjax(e);
                },
                onContextMenu: (e)=>{
                    console.error(e,'cclick')
                    e.preventDefault();
                }
            });
        }



        const dataHZTabs = {
            callbackRecalc: () => {},
            className: "pp_tollbar_tabs",
            data: DataTabs
        };

        return (
            <HZTabs {...dataHZTabs} />
        );
    };
    RenderHZTabsTools = () => {
        let isDisabledEdit = true;
        let isDisabledRemove = true;
        let isDisabledSort = true;
        if(this.props.ClassesStore.SelectTr) {
            isDisabledEdit = false;
            isDisabledRemove = false;
        }
        if(this.props.ClassesStore.AttrData) {
            isDisabledSort = false;
        }

        let toolsJSX = [
            <button title={i18next.t('init:general.operations.add')} key={"btn_add"}
                    disabled={false}
                    onClick={(e) => {
                        this.events.handleClickAddAttribute();
                    }}
                    className={"pp_c pp_c_btn "}>
                <i className="fa fa-plus"/>{i18next.t('init:general.operations.add')}
            </button>,
            <button title={i18next.t('init:general.operations.edit')} key={"btn_edit"}
                    className={"pp_c pp_c_btn "}
                    disabled={isDisabledEdit}
                    onClick={(e) => {
                        this.events.handleClickEditAttribute();
                    }}
            >
                <i className="fa fa-save"/>{i18next.t('init:general.operations.edit')}
            </button>,
            <button title={i18next.t('init:general.operations.delete')} key={"btn_remove"}
                    disabled={isDisabledRemove}
                    className={"pp_c pp_c_btn"}
                    onClick={(e) => {
                        this.events.handleClickRemoveAttribute();
                    }}
            >
                <i className="fas fa-times"/>{i18next.t('init:general.operations.delete')}
            </button>,
            <button title={i18next.t('init:general.operations.sort_params')} key={"btn_sort"}
                    disabled={isDisabledSort}
                    className={"pp_c pp_c_btn"}
                    onClick={(e) => {
                        if(this.props.ClassesStore.AttrData) {
                            this.state.HelperModalData.show = true;
                            this.state.HelperModalData.TypeModal = "SortParams";
                            this.state.HelperModalData.AttrData = this.props.ClassesStore.AttrData;
                            this.state.HelperModalData.SelectNodeForParams = this.props.ClassesStore.SelectNodeForParams;
                            this.setState({});
                        }
                    }}
            >
                <span style={{height:"14px", width: "14px", display: "flex"}}>
                    <svg viewBox="0 0 34.761 26.892">
                        <path d="M15.359 9.478l-6.226-6.21v17.557H7.426V3.268L1.2 9.478 0 8.281 8.279 0l8.279 8.281z"/>
                        <path d="M34.761 18.612l-8.279 8.281-8.282-8.281 1.2-1.2 6.226 6.21V6.068h1.707v17.557l6.226-6.21z"/>
                    </svg>
                </span>
            </button>,
        ];

        const dataHZTabs_tools = {
            callbackRecalc: () => {},
            className: "pp_tollbar_tools",
            dataJSX: toolsJSX
        };

        return (
            <HZTabs {...dataHZTabs_tools} />
        );
    };

    render() {
        let data_trs = [];
        if(this.props.ClassesStore.AttrData) {
            this.props.ClassesStore.AttrData.forEach( (item, key) => {
                data_trs.push({
                    trKey: item.uid,
                    trCustomData: item,
                    tds: [
                        {name: item.name},
                        {name: item.datatype},
                        {name: item.isArray ? i18next.t('init:general.yes'): ""},
                        {name: item.isKey ? i18next.t('init:general.yes'): ""},
                        {name: item.require ? i18next.t('init:general.yes'): ""},
                        {name: item.calc ? item.calc.name : ""},
                        {name: item.LinkType ? i18next.t('init:general.yes') : ""},
                    ]
                })
            });
        }
        const DataForCoolTable = {
            settings: {
                data: data_trs,
            },
            headerData: [
                {key: "Name", IsFixedWidth: false, MinWidth: 100, name: "Name"},
                {key: "Data_type", IsFixedWidth: false, MinWidth: 100, name: "Data type"},
                {key: "Array", IsFixedWidth: true, MinWidth: 80, name: "Array"},
                {key: "Unique", IsFixedWidth: true, MinWidth: 80, name: "Unique"},
                {key: "Require", IsFixedWidth: true, MinWidth: 80, name: "Require"},
                {key: "calc", IsFixedWidth: true, MinWidth: 80, name: "Method"},
                {key: "LinkType", IsFixedWidth: true, MinWidth: 80, name: "LinkType"},
            ],
            callbackRenderTD: (dataTD, dataTR) => {
                return dataTD.name;
            },
            callbackRenderTH: (dataTH) => {
                return dataTH.name;
            },


            RecalcStyleFunc: this.state.RecalcStyleFunc,


            selectRowKey: this.props.ClassesStore.SelectTr ? this.props.ClassesStore.SelectTr.trKey : null,
            onSelectRow: (item_tr) => {
                this.props.ClassesStore.SelectTr = item_tr;
            },

            onDoubleClickTD: (e, item_tr, item_td) => {
                if(!this.props.ClassesStore.SelectTr) {
                    this.props.ClassesStore.SelectTr = item_tr;
                }
                this.events.handleClickEditAttribute();
            },

            //IsResized
        };
        const dataModalAttribute = {
            CloseCallback: () => { this.state.ModalAttributeConfig.show = false; this.setState({})},
            show: this.state.ModalAttributeConfig.show,
            TypeModal: this.state.ModalAttributeConfig.TypeModal,
            data: this.state.ModalAttributeConfig.data,
        };


        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        if(!this.props.ClassesStore.IsOpenTable) {
            LoaderBlockUI = <h4 className="loader-text">Select a node in the tree...</h4>;
        }

        let isBlocking = !this.props.ClassesStore.IsOpenTable || this.props.ClassesStore.IsLoadParams || this.props.ClassesStore.IsGetDataForTab;

        let CoolTableJSX;
        if(!isBlocking) {
            CoolTableJSX =  <CoolTable {...DataForCoolTable} />;
        }

        let HZ_TABS, HZTabsTools, ClassPage, MethodPage;

        if(this.props.ClassesStore.IsOpenTable) {
            if (this.props.ClassesStore.SelectNodeForParams.type == ClassesStore.CLASS_TYPE) {
                HZ_TABS = this.RenderHZTabs();
                HZTabsTools = this.RenderHZTabsTools();
                ClassPage = 
                    <>
                        <div className="cool_t_tollbar">
                            {HZ_TABS}
                            {HZTabsTools}
                        </div>
                        {CoolTableJSX}
                    </>
            } else if (this.props.ClassesStore.SelectNodeForParams.type == ClassesStore.METHOD_TYPE) {
                const methodParams = {
                    uid: this.props.ClassesStore.SelectNodeForParams.uid,
                    class: this.props.ClassesStore.SelectNodeForParams.parent
                };
                MethodPage = <CalcMain params={methodParams}/>
            }
        }
        else {
            isBlocking = false;
        }

        const HelperModalData = {
            ...{
                CloseCallback: () => {
                    this.state.HelperModalData.show = false;
                    this.setState({});
                },
                SuccSaveSortParamCallback: () => {
                    this.state.HelperModalData.show = false;
                    this.setState({});
                    this.props.ClassesStore.SelectGroupAction();
                }
            },
            ...this.state.HelperModalData
        };

        return (
            <BlockUi id="zt_ClassPage" tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                <div className="cool_t_wrapper_main">
                    {ClassPage}
                    {MethodPage}
                </div>
                <ModalAttribute {...dataModalAttribute} />
                <HelperModal {...HelperModalData} />
            </BlockUi>
        )
    }
}

const ClassesPageTranslation = withTranslation(['init', 'classes'])(ClassesPageView);

export function ClassesPage(props) {
    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <ClassesPageTranslation {...props} />
        </React.Suspense>
    );
}