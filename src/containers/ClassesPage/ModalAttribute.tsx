import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/ClassTree-api";
import {th} from "date-fns/locale";

import TreeSelect, { TreeNode, SHOW_PARENT } from 'rc-tree-select';
import 'rc-tree-select/assets/index.css';
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";



interface IModalAttributeProps {
    show:boolean;
    TypeModal: string; //add//edit
    CloseCallback: () => void,
    SuccessFunction?: () => void,

    data?: {
        TypeName?: string,
    };

    ClassesStore?: any,
    ToastrStore?: any;
}
interface IModalAttributeState {
    blocking: boolean,
    TypeName?: string,

    controls: {
        name: string,
        datatype: string,
        array: boolean,
        unique: boolean,
        require: boolean,
        TextType: number,
        ShowCameraModule?:number,

        calc: string,
        calc_triggers: {
            manual: string,
        }
        calcs: any[],

        SelectClass: {
            value: string,
            searchValue: any,
            isOpen: boolean,
            expandedKeys: string[],
        },
    },
    TabsSett: number;
}

@inject("ClassesStore")
@inject("ToastrStore")
@observer
export default class ModalAttribute extends React.Component<IModalAttributeProps,IModalAttributeState> {
    state: Readonly<IModalAttributeState> = {
        blocking: false,
        TypeName: "",

        controls: {
            name: "",
            datatype: "text",
            array: false,
            unique: false,
            require: false,
            TextType: 0,
            ShowCameraModule:0,

            calcs: [],
            calc: "",
            calc_triggers: {
                manual: "",
            },
            SelectClass: {
                value: "",
                searchValue: "",
                isOpen: false,
                expandedKeys: []
            },
        },
        TabsSett: 0,
    };
    _refs: { name: HTMLElement,
        datatype: HTMLElement,
        array: HTMLElement,
        unique: HTMLElement
        require: HTMLElement
        TextType: HTMLElement
    } = {
        name:  null,
        datatype: null,
        array: null,
        unique: null,
        require: null,
        TextType: null,
    };

    _ref_inp_type: HTMLElement;

    constructor(props) {
        super(props);
    }

    events = {
        onChangeTabs: (event: React.ChangeEvent<{}>, newValue: number) => {
            this.setState({TabsSett: newValue });
        }
    };

    core = {
        addAttr: (params:ct_api.IAttrDataAdd, SuccFunc) => {
            ct_api
                .addAttr(params)
                .then(res => {
                    if(res.data.result == 1) {
                        this.props.ToastrStore.success('Success!', 'Success!');
                        SuccFunc(res.data);
                    }
                    else {
                        this.core.showErrors(res.data.errors);
                    }
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },
        updateAttr: (params: ct_api.IAttrDataUpdate, SuccFunc) => {
            ct_api
                .updateAttr(params)
                .then(res => {
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        this.core.showErrors(res.data.errors);
                    }
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },
        deleteAttr: (uid, type, SuccFunc) => {
            ct_api
                .deleteAttr(uid, type)
                .then(res => {
                    if(res.data.result == 1) {
                        this.props.ToastrStore.success('Success!', 'Success!');
                        SuccFunc(res.data);
                    }
                    else {
                        this.props.ToastrStore.error('Server error!', 'Error');
                    }
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },

        getCalcs: (uid, SuccFunc) => {
            ct_api
                .getCalcs(uid)
                .then(res => {
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        this.props.ToastrStore.error('Server error!', 'Error');
                    }
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false});
                })
        },

        showErrors: (errors) => {
            if (errors) {
                let text = [];
                for (let i = 0; i < errors.length; i++) {
                    text.push(errors[i].text);
                }
                this.props.ToastrStore.error(text.join("\n"), 'Error');
            } else {
                this.props.ToastrStore.error('Server error!', 'Error');
            }
        },


        build: {
            controlsPanel: () => {
                let ClassSelectTree;
                if(this.state.controls.datatype == "object") {
                    ClassSelectTree =  <div className="form-group">{this.helper.ClassSelectRender()}</div>;
                }
                let CameraModule = null;
                if(this.state.controls.datatype == "file"){
                  CameraModule=<div className="form-group">{this.helper.CameraModuleSelectRender()}</div>;
                }

                return (<div>
                    <div className="form-group">
                        Name
                        <input
                            key={"ModalCreateNodeTypeName"}
                            onChange={(e)=>{ this.state.controls.name = e.target.value; this.setState({})}}
                            value={this.state.controls.name}
                            className="form-control"/>
                    </div>
                    <div className="form-group">
                        Data type
                        <div className={"input-group"}>
                            <select disabled={this.props.TypeModal == "edit"} className="form-control" ref={(ref) => (this._refs.datatype = ref)}
                                    onChange={(e) => {this.state.controls.datatype = e.target.value; this.setState({});}} value={this.state.controls.datatype}
                            >
                                <option value="text">text</option>
                                <option value="number">number</option>
                                <option value="datetime">datetime</option>
                                <option value="object">object</option>
                                <option value="file">file</option>
                                <option value="counter">counter</option>
                            </select>
                        </div>
                    </div>
                    <div className={"form-group " + (!(this.state.controls?.datatype == "text") ? "hide-el" : "")}>
                        Control type
                        <div className={"input-group"}>
                            <select className="form-control" ref={(ref) => (this._refs.TextType = ref)}
                                    onChange={(e) => {this.state.controls.TextType = Number(e.target.value); this.setState({});}} value={this.state.controls.TextType}
                            >
                                <option value="0">text</option>
                                <option value="1">ckEditor</option>
                                <option value="2">ColorPicker</option>
                                <option value="3">Link</option>
                                <option value="4">ImageLink</option>
                            </select>
                        </div>
                    </div>
                    {CameraModule}
                    {ClassSelectTree}
                    <div className="form-group">
                        Array
                        <input className="form-control" type="checkbox" ref={(ref) => (this._refs.array = ref)}
                               onChange={(e) => {this.state.controls.array = e.target.checked; this.setState({});}} checked={this.state.controls.array}
                        />
                    </div>
                    <div className="form-group">
                        Unique
                        <input className="form-control" type="checkbox" ref={(ref) => (this._refs.unique = ref)}
                               onChange={(e) => {this.state.controls.unique = e.target.checked; this.setState({});}} checked={this.state.controls.unique}
                        />
                    </div>
                    <div className="form-group">
                        Require
                        <input className="form-control" type="checkbox" ref={(ref) => (this._refs.require = ref)}
                               onChange={(e) => {this.state.controls.require = e.target.checked; this.setState({});}} checked={this.state.controls.require}
                        />
                    </div>
                    {this.helper.CalcSelectRender()}
                </div>);
            },
            triggersPanel: () => {
                return (<div>
                    {this.helper.CalcTrigSelectRender()}
                </div>);
            },
            TabPanel: (props) => {
                const { children, value, index, ...other } = props;

                return (
                    <Typography
                        component="div"
                        role="tabpanel"
                        hidden={value !== index}
                        id={`scrollable-auto-tabpanel-${index}`}
                        aria-labelledby={`scrollable-auto-tab-${index}`}
                        {...other}
                    >
                        {value === index && <Box p={3}>{children}</Box>}
                    </Typography>
                );
            },

            body_add_edit: () => {

                return (
                    <div>
                        <Tabs
                            value={this.state.TabsSett}
                            onChange={this.events.onChangeTabs}
                            indicatorColor="primary"
                            textColor="primary"
                            centered>
                            <Tab label="General" />
                            <Tab label="Triggers" />
                        </Tabs>
                        {this.core.build.TabPanel({
                            children: this.core.build.controlsPanel(),
                            index: 0,
                            value: this.state.TabsSett
                        })}
                        {this.core.build.TabPanel({
                            children: this.core.build.triggersPanel(),
                            index: 1,
                            value: this.state.TabsSett
                        })}
                    </div>
                );
            }
        },
    };

    CreateAttr = (SuccessFunction) => {
        const SelectNodeForParams = this.props.ClassesStore.SelectNodeForParams;
        let params = {
            name: this.state.controls.name,
            datatype: this.state.controls.datatype,
            type_parent: SelectNodeForParams.iddb,
            group: this.props.ClassesStore.ActiveGroupID,
            array: this.state.controls.array ? 1 : 0,
            unique: this.state.controls.unique ? 1 : 0,
            require: this.state.controls.require ? 1 : 0,
            TextType: !(this.state.controls?.datatype == "text" && this.state.controls?.TextType) ? 0 : this.state.controls.TextType,
            ShowCameraModule: !(this.state.controls?.datatype == "file" && this.state.controls?.ShowCameraModule) ? 0 : this.state.controls.ShowCameraModule,

            calc: this.state.controls.calc,
            type_link: this.state.controls.datatype == "object" ? this.state.controls.SelectClass.value : "",
            calc_triggers: this.state.controls.calc_triggers,
        };
        this.core.addAttr(params,() => {
            SuccessFunction();
        });
    };

    handleCreate = () => {
        this.CreateAttr(() => {
            this.props.CloseCallback();
            this.props.ClassesStore.SelectGroupAction();
        })
    };
    handleCreateMore = () => {
        this.CreateAttr(() => {
            this.props.ClassesStore.SelectGroupAction();
        })
    };

    handleEdit = () => {
        const SelectNodeForParams = this.props.ClassesStore.SelectNodeForParams;
        let params = {
            attr: this.props.ClassesStore.SelectTr.trCustomData.uid,
            name: this.state.controls.name,
            datatype: this.state.controls.datatype,
            type_parent: SelectNodeForParams.iddb,
            group: this.props.ClassesStore.ActiveGroupID,
            array: this.state.controls.array ? 1 : 0,
            unique: this.state.controls.unique ? 1 : 0,
            require: this.state.controls.require ? 1 : 0,
            TextType: !(this.state.controls?.datatype == "text" && this.state.controls?.TextType) ? 0 : this.state.controls.TextType,
            ShowCameraModule: !(this.state.controls?.datatype == "file" && this.state.controls?.ShowCameraModule) ? 0 : this.state.controls.ShowCameraModule,

            calc: this.state.controls.calc,
            type_link: this.state.controls.datatype == "object" ? this.state.controls.SelectClass.value : "",
            calc_triggers: this.state.controls.calc_triggers,
        };

        this.core.updateAttr(params,() => {
            this.props.CloseCallback();
            this.props.ClassesStore.SelectGroupAction();
        });
    };
    handleDelete = () => {
        let uid = this.props.ClassesStore.SelectTr.trCustomData.uid;
        let typeID = this.props.ClassesStore.SelectNodeForParams.TypeID;
        this.core.deleteAttr(uid, typeID,() => {
            this.props.CloseCallback();
            this.props.ClassesStore.SelectGroupAction();
        });
    };

    handleClose = () => {
        this.props.CloseCallback();
    };

    ChangeInput = (e) => {
        this.setState({
            TypeName: e.target.value,
        });
    };

    onShow = () => {
        let TypeName = "";

        if(this.props.TypeModal == "add") {
            //this._refs.name.focus();
            this.helper.clear_controls();
            this.setState({TypeName: TypeName, blocking: true});
            this.core.getCalcs(this.props.ClassesStore.SelectNodeForParams.iddb, (res) => {
                this.state.controls.calcs = res.data;
                this.setState({TypeName: TypeName, blocking: false});
            });
        }
        else if(this.props.TypeModal == "edit") {
            //this._refs.name.focus();
            let trCustomData = this.props.ClassesStore.SelectTr.trCustomData;
            this.helper.fill_controls(trCustomData);
            this.setState({TypeName: TypeName, blocking: true});
            this.core.getCalcs(this.props.ClassesStore.SelectNodeForParams.iddb, (res) => {
                this.state.controls.calcs = res.data;
                this.setState({TypeName: TypeName, blocking: false});
            });
        }
        else if(this.props.TypeModal == "del") {
            this.setState({TypeName: TypeName, blocking: false});
        }

    };


    helper = {
        CalcSelectRender: () => {
            //this.state.controls.calcs
            let options = [
                <option key={"none"} value="">none</option>
            ];

            if(this.state.controls.calcs) {
                this.state.controls.calcs.forEach( (item, key) => {
                    options.push(
                        <option key={item.uid} value={item.uid}>{item.name}</option>
                    );
                });
            }

            return <div className="form-group">
                Method
                <div className={"input-group"}>
                    <select className="form-control"
                            onChange={(e) => {
                                this.state.controls.calc = e.target.value; this.setState({});
                            }} value={this.state.controls.calc}
                    >
                        {options}
                    </select>
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button" onClick={() => {
                            let uid_calc = "";
                            if(this.state.controls.calc) {
                                uid_calc = '&uid='+this.state.controls.calc;
                            }
                            window.open('/calc/?class=' + this.props.ClassesStore.SelectNodeForParams.iddb + uid_calc, '_blank');
                        }}>
                            <i className="fas fa-calculator"/>
                        </button>
                    </div>
                </div>
            </div>;
        },
        CalcTrigSelectRender: () => {
            let options = [
                <option key={"none"} value="">none</option>
            ];

            if(this.state.controls.calcs) {
                this.state.controls.calcs.forEach( (item, key) => {
                    options.push(
                        <option key={item.uid} value={item.uid}>{item.name}</option>
                    );
                });
            }

            return <div className="form-group">
                Manual trigger
                <div className={"input-group"}>
                    <select className="form-control"
                            onChange={(e) => {
                                this.state.controls.calc_triggers.manual = e.target.value; this.setState({});
                            }} value={this.state.controls.calc_triggers.manual}
                    >
                        {options}
                    </select>
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button" onClick={() => {
                            let uid_calc = "";
                            if(this.state.controls.calc_triggers.manual) {
                                uid_calc = '&uid='+this.state.controls.calc_triggers.manual;
                            }
                            window.open('/calc/?class=' + this.props.ClassesStore.SelectNodeForParams.iddb + uid_calc, '_blank');
                        }}>
                            <i className="fas fa-calculator"/>
                        </button>
                    </div>
                </div>
            </div>;
        },
        ClassSelectRender: () => {
            let TreeDataStore = [];
            if(this.props.ClassesStore.treeData.length > 0 && this.props.ClassesStore.treeData[0].children) {
                TreeDataStore = this.props.ClassesStore.treeData[0].children;
            }

            let selectValue = this.state.controls.SelectClass.value;
            let expandedKeys = [];

            if(selectValue && this.state.controls.SelectClass.expandedKeys.indexOf(selectValue) == -1) {
                const loopGenerateExpKeys = (data) => {
                    if(data.length > 0) {
                        for(let key_t in data) {
                            let item = data[key_t];

                            if (item.children) {
                                let expandedKeys_tmp = loopGenerateExpKeys(item.children);
                                if(expandedKeys_tmp) {
                                    expandedKeys_tmp.push(item.id);
                                    return expandedKeys_tmp;
                                }
                            }
                            else {
                                if(item.iddb == selectValue) {
                                    return [item.id];
                                }
                            }
                        }
                    }
                    return null;
                };

                let expandedKeys_tmp = loopGenerateExpKeys(TreeDataStore);
                if(expandedKeys_tmp) {
                    expandedKeys = expandedKeys.concat(expandedKeys_tmp);
                }
            }

            const loop = (data, SearchString) => {
                let children = [];
                if(data.length > 0) {
                    data.forEach( (item) => {
                        let icon = item.icon ? item.icon : 'rc-tree-iconEle rc-tree-icon__open';
                        let is_founded = item.Name.toLocaleLowerCase().indexOf(SearchString) !== -1;
                        let ClassName = is_founded && SearchString != "" ? "ttp_tree_node_founded" : "";

                        const DateTreeNode = {
                            active: false,
                            icon: <span className={icon}/>,
                            title: item.Name,
                            key: item.id,
                            label: ClassName,
                            value: item.iddb,

                            iddb: item.iddb,

                            className: ClassName,
                            isLeaf: false,
                            disabled: false,
                        };

                        if (item.children) {
                            let childrenJSX = loop(item.children, SearchString);
                            if( is_founded || childrenJSX.length > 0 ) {
                                if(SearchString) {
                                    expandedKeys.push(item.id);
                                }
                                let itemNode = {...{children: childrenJSX},...DateTreeNode};
                                children.push(itemNode);
                            }
                        }
                        else {
                            if( item.Name.toLocaleLowerCase().indexOf(SearchString) !== -1) {
                                DateTreeNode.isLeaf = true;
                                DateTreeNode.disabled = item.key === '0-0-0';

                                let itemNode = {...{},...DateTreeNode};
                                children.push(itemNode);
                            }
                        }
                    });
                }
                return children;
            };
            const treeNodesData = loop(TreeDataStore, this.state.controls.SelectClass.searchValue.toLocaleLowerCase());

            expandedKeys = expandedKeys.concat(this.state.controls.SelectClass.expandedKeys);
            const TreeSelectData = {
                treeData: treeNodesData,
                value: selectValue,
                //searchValue: this.state.controls.SelectClass.searchValue,
                expandedKeys: expandedKeys,
                open: this.state.controls.SelectClass.isOpen,
                placeholder: <i>Select class</i>,

                showSearch: true,
                allowClear: true,
                treeLine: true,
                filterTreeNode: false,

                treeExpandedKeys: expandedKeys,

                onTreeExpand: (expandedKeys) => {
                    this.state.controls.SelectClass.expandedKeys = expandedKeys;
                    this.setState({});
                },
                onSearch: (value) => {
                    this.state.controls.SelectClass.searchValue = value;
                    this.setState({});
                },
                onChange: (val, ...args) => {
                    console.log('onChange', val, ...args);
                    if (val === '0-0-0-0-value') {
                        this.state.controls.SelectClass.isOpen = true;
                    } else {
                        this.state.controls.SelectClass.isOpen = false;
                    }
                    this.state.controls.SelectClass.value = val;
                    this.setState({});
                },
                onDropdownVisibleChange: (v, info) => {
                    console.log('single onDropdownVisibleChange', v, info);
                    // document clicked
                    if (info.documentClickClose && this.state.controls.SelectClass.value === '0-0-0-0-value') {
                        return false;
                    }
                    this.state.controls.SelectClass.isOpen = v;
                    this.setState({});
                    return true;
                },
                onSelect: (...args) => {
                    // use onChange instead
                    console.log(args);
                }
            };


            return <div className="form-group">
                Select class
                <TreeSelect
                    {...TreeSelectData}
                    className={"form_control_select_tree"}
                    transitionName="rc-tree-select-dropdown-slide-up"
                    choiceTransitionName="rc-tree-select-selection__choice-zoom"
                    dropdownStyle={{ maxHeight: 200, overflow: 'auto', zIndex: "3000" }}
                    searchPlaceholder="please search"
                    treeNodeFilterProp="label"
            /></div>;
        },
        CameraModuleSelectRender:()=>{
          return(<React.Fragment>
                Display Camera Module
                <select value={this.state.controls.ShowCameraModule} onChange={(e)=>{
                  let {value}= e.currentTarget;
                  this.setState({controls:{...this.state.controls, ShowCameraModule:Number(value)}})
                }} className="form-control">
                    <option value={0}>hide</option>
                    <option value={1}>display</option>
                </select>
            </React.Fragment>);
        },

        clear_controls: () => {
            this.state.controls.name = "";
            this.state.controls.datatype = "text";
            this.state.controls.array = false;
            this.state.controls.unique = false;
            this.state.controls.require = false;
            this.state.controls.TextType = 0;
            this.state.controls.ShowCameraModule = 0;
            this.state.controls.calc = "";
            this.state.controls.SelectClass.value = "";
            this.state.controls.SelectClass.searchValue = "";
            this.state.controls.SelectClass.isOpen = false;
            this.state.controls.SelectClass.expandedKeys = [];
            this.state.controls.calc_triggers.manual = "";
        },
        fill_controls: (trCustomData) => {
            this.state.controls.name = trCustomData.name;
            this.state.controls.datatype = trCustomData.datatype;
            this.state.controls.array = trCustomData["isArray"];
            this.state.controls.unique = trCustomData.isKey;
            this.state.controls.require = trCustomData.require;
            this.state.controls.TextType = !trCustomData.TextType ? 0 : trCustomData.TextType;
            this.state.controls.ShowCameraModule = !trCustomData.ShowCameraModule ? 0 : trCustomData.ShowCameraModule;
            this.state.controls.SelectClass.value = trCustomData.LinkType ? trCustomData.LinkType.uid : "";
            this.state.controls.SelectClass.searchValue = "";
            this.state.controls.SelectClass.isOpen = false;
            this.state.controls.SelectClass.expandedKeys = [];

            this.state.controls.calc = trCustomData.calc && trCustomData.calc.uid ? trCustomData.calc.uid : "";
            this.state.controls.calc_triggers.manual = trCustomData.calc_triggers.manual && trCustomData.calc_triggers.manual.uid ? trCustomData.calc_triggers.manual.uid : "";
        },
    };

    render() {
        let Title = "Create attribute";
        if (this.props.TypeModal == "add") {
            Title = "Add attribute";
        }
        else if (this.props.TypeModal == "edit") {
            Title = "Edit attribute";
        }

        let Body = <div/>;
        if (this.props.TypeModal == "add" || this.props.TypeModal == "edit") {
            Body = this.core.build.body_add_edit();
        }
        else if(this.props.TypeModal == "del") {
            let NameRemoveAttr = this.props.ClassesStore.SelectTr?this.props.ClassesStore.SelectTr.trCustomData.name:"";
            Title = "Delete attribute";
            Body = <div>
                Are you sure you want to delete "{NameRemoveAttr}"?
            </div>;
        }

        let Footer = <div/>;
        if (this.props.TypeModal == "add") {
            Footer = <div>
                <Button variant="success" onClick={this.handleCreate}>
                    Add
                </Button>
                <Button variant="success" onClick={this.handleCreateMore}>
                    Add more
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    Close
                </Button>
            </div>;
        }
        else if (this.props.TypeModal == "edit") {
            Footer = <div>
                <Button variant="success" onClick={this.handleEdit}>
                    Save
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    Close
                </Button>
            </div>;
        }
        else if (this.props.TypeModal == "del") {
            Footer = <div>
                <Button variant="danger" onClick={this.handleDelete}>
                    Delete
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    Close
                </Button>
            </div>;
        }

        return (
            <Modal onShow={this.onShow} animation={true} show={this.props.show} onHide={this.handleClose} enforceFocus={false} size={"xl"}>
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}
