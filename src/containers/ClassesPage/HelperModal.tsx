import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/ClassTree-api";
import {parse, format} from "date-fns";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as f_helper from "../../helpers/form_datatype_helper"
import ToastrStore from "../../stores/ToastrStore";
import axios from "axios";
import RootStore from "../../stores/RootStore";
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


interface IHelperModalProps {
    show:boolean;
    TypeModal: "SortParams" | "other",

    //sort
    SelectNodeForParams?: any,
    AttrData?: any[],

    CloseCallback: () => void,
    SuccSaveSortParamCallback: () => void
}
interface IHelperModalState {
    blocking: boolean,

    keyItems: any[],
    Items: any[],
    isLoadParamsSave: boolean,
}

const ContainerModal = document.getElementById('root_theme');

export default class HelperModal extends React.Component<IHelperModalProps, IHelperModalState> {
    state: Readonly<IHelperModalState> = {
        blocking: false,
        isLoadParamsSave: false,
        keyItems: [],
        Items: [],
    };
    xhrGenerateModalContent = {cancel: null};


    constructor(props) {
        super(props);
    }


    core = {
        AJAX: {
            SaveSortParams:(paramsArr:ct_api.ISortParams[], SuccFunc, ErrFunc) => {
                ct_api
                    .sortAttr(this.props.SelectNodeForParams.uid, paramsArr)
                    .then((res) => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                            ToastrStore.success('Success!', 'Success');
                        }
                        else {
                            ErrFunc(res.data);
                            ToastrStore.error('Server error!', 'Error');
                        }
                    })
                    .catch((ex) => {
                        ErrFunc({result: 0});
                        ToastrStore.error('Server error!', 'Error');
                    });
            }
        },
        //sortModal
        reorder: (list, startIndex, endIndex) => {
            const result = Array.from(list);
            const [removed] = result.splice(startIndex, 1);
            result.splice(endIndex, 0, removed);

            return result;
        },
        SaveSortParams: () => {
            this.setState({isLoadParamsSave: true});

            let tmpIDarr : ct_api.ISortParams[] = [];

            let lastIndex = 1;
            for ( let i = 0; i < this.state.keyItems.length; i++ ) {
                tmpIDarr.push({
                    ID: this.state.keyItems[i].uid,
                    Index: lastIndex,
                });

                lastIndex ++;
            }
            for ( let i = 0; i < this.state.Items.length; i++ ) {
                tmpIDarr.push({
                    ID: this.state.Items[i].uid,
                    Index: lastIndex,
                });
                lastIndex ++;
            }

            if(tmpIDarr.length <= 1) {
                this.props.CloseCallback();
            }
            else {
                this.core.AJAX.SaveSortParams(tmpIDarr, () => {
                    this.props.SuccSaveSortParamCallback();
                }, () => {
                    this.setState({isLoadParamsSave: false});
                });
            }
        },

        build: {
            sortModal: {
                content: () => {

                    let KeyDroppableJSX = (
                        <Droppable droppableId="droppable-0" type="PERSON_0">
                        {(provided, snapshot) => {
                        let uit_isDraggingOver = snapshot.isDraggingOver ? 'uit_isDraggingOver' : '';

                        return (
                            <div ref={provided.innerRef}
                        className={"uit_drop " + uit_isDraggingOver}
                        {...provided.droppableProps}
                    >
                        {
                            this.state.keyItems.map((item, index) => {
                                return (
                                    <Draggable key={item.id} draggableId={item.id} index={index}>
                                    {
                                    (provided, snapshot) => {
                                    let uit_isDragging_elem = snapshot.isDragging ? "uit_isDragging_elem" : "";

                                    return (
                                        <div ref={provided.innerRef}
                                    className={"uit_drag_elem " + uit_isDragging_elem}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={provided.draggableProps.style}
                                        >
                                        {item.content}
                                        </div>);
                                }
                            }
                                </Draggable>);
                            })
                        }

                        {provided.placeholder}
                        </div>);}
                    }
                    </Droppable>
                );
                    let DroppableJSX = (
                        <Droppable droppableId="droppable-1" type="PERSON_1">
                        {(provided, snapshot) => {
                        let uit_isDraggingOver = snapshot.isDraggingOver ? 'uit_isDraggingOver' : '';

                        return (
                            <div ref={provided.innerRef}
                        className={"uit_drop " + uit_isDraggingOver}
                        {...provided.droppableProps}
                    >
                        {
                            this.state.Items.map((item, index) => {
                                return (
                                    <Draggable key={item.id} draggableId={item.id} index={index}>
                                    {
                                    (provided, snapshot) => {
                                    let uit_isDragging_elem = snapshot.isDragging ? "uit_isDragging_elem" : "";

                                    return (
                                        <div ref={provided.innerRef}
                                    className={"uit_drag_elem " + uit_isDragging_elem}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={provided.draggableProps.style}
                                        >
                                        {item.content}
                                        </div>);
                                }
                            }
                                </Draggable>);
                            })
                        }

                        {provided.placeholder}
                        </div>);}
                    }
                    </Droppable>
                );


                    return (
                        <DragDropContext onDragEnd={this.events.onDragEnd}>
                        <h4><i className="fa fa-key"/> KeyParams</h4>
                    {KeyDroppableJSX}
                    <h4>Other params</h4>
                    {DroppableJSX}
                    </DragDropContext>
                );
                }
            },
        }
    };

    events = {
        onShow: () => {
            this.setState({
                blocking: true,
                isLoadParamsSave: false,
                Items: [],
                keyItems: []
            });
            if(this.props.TypeModal == "SortParams") {
                const Params = this.props.AttrData;
                let keyItems = [];
                let Items = [];


                for( let i = 0; i < Params.length; i++ ) {
                    if(Params[i]["isKey"]) {
                        keyItems.push({
                            id: "pi-"+Params[i].uid,
                            content: Params[i].name,
                            uid: Params[i].uid,
                        });
                    }
                    else {
                        Items.push({
                            id: "pi-"+Params[i].uid,
                            content: Params[i].name,
                            uid: Params[i].uid
                        });
                    }

                }

                this.setState({blocking: false, Items: Items, keyItems: keyItems});
            }
        },
        handleClose: () => {
            this.props.CloseCallback();
        },


        //sort
        onDragEnd: (result) => {
            if (!result.destination) {
                return;
            }
            console.error(result);
            if(result.destination.droppableId == "droppable-0") {
                const keyItems = this.core.reorder(
                    this.state.keyItems,
                    result.source.index,
                    result.destination.index
                );

                this.setState({keyItems});
            }
            else if(result.destination.droppableId == "droppable-1") {
                const Items = this.core.reorder(
                    this.state.Items,
                    result.source.index,
                    result.destination.index
                );

                this.setState({Items});
            }


        },
        handleSaveSortParams: () => {
            this.core.SaveSortParams();
        },

    };


    render() {
        let isBlocking = false;
        let Title = "";
        let Body = <div/>;
        let Footer = <div/>;

        if (this.props.TypeModal == "SortParams") {
            if(this.props.SelectNodeForParams) {
                Title = "Sort params for \"" + this.props.SelectNodeForParams.Name + "\"";
            }
            else {
                Title = "Sort params"
            }
            Body = this.core.build.sortModal.content();

            Footer = <div>
                <Button variant="success" onClick={this.events.handleSaveSortParams}>
                    Apply
                </Button>
                <Button variant="secondary" onClick={this.events.handleClose}>
                    Close
                </Button>
            </div>;
        }


        if(this.state.blocking || this.state.isLoadParamsSave) {
            isBlocking = true;
        }

        return (
            <Modal onShow={this.events.onShow}
                   animation={true} show={this.props.show}
                   onHide={this.events.handleClose} size="lg"
                   container={document.getElementById('root_theme')}>
                <BlockUi className={"ClassPage_sort_modal"} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}
