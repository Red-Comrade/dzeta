import React from 'react';
import ReactDOM from "react-dom";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import queryString from 'query-string';
import { inject, observer } from "mobx-react";
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';

import { GetCalc } from '../../api/Calc-api';
import RootStore from "../../stores/RootStore";

import Editor from "./CalcEditor";
import CalcPanel from "./CalcPanel";
import CalcFooter from "./CalcFooter";
import CalcSidebar from "./CalcSidebar";

import 'react-block-ui/style.css';
import 'loaders.css/loaders.min.css';

@inject("CalcStore")
@observer
class CalcMainView extends React.Component {
    constructor(props) {
        super(props);
        i18next.loadNamespaces('calc');
        this.state = {
            formulaInit: '$x="Hello Word"',
            functions: [],
            methods: [],
            showEditor: false,
            isVisibleFunctionsDock: true,
        };
    }

    componentDidMount(): void {
        let paramsUrl;
        if (this.props.params) {
            paramsUrl = this.props.params;
        } else {
            document.title = 'Method | ' + RootStore.config.nameSystem;
            paramsUrl = queryString.parse(this.props.location.search);
        }

        this.showMethod(paramsUrl);
    }

    componentDidUpdate(prevProps) {
        if (this.props.params && prevProps.params) {
            if (this.props.params.uid != prevProps.params.uid || this.props.params.class != prevProps.params.class) {
                this.setState({
                    showEditor: false,
                });
                const paramsUrl = this.props.params;
                this.showMethod(paramsUrl);
            }
        }
    }

    initEditor = (editor) => {
        this.props.CalcStore.setEditor(editor);
    }

    showMethod = (paramsUrl) => {
        if (paramsUrl.class != undefined) {
            this.props.CalcStore.setClass(paramsUrl.class);
        }

        if (paramsUrl.uid != undefined) {
            //делаем запрос на формулу
            this.props.CalcStore.setCalc(paramsUrl.uid);
        }
        GetCalc(paramsUrl.uid)
            .then((response) => {
                console.log(response);
                this.setState({
                    formulaInit: response.data.data.formula,
                    functions: response.data.data.functions,
                    methods: response.data.data.methods,
                    showEditor: true,
                });
                this.props.CalcStore.setName(response.data.data.name);
                if (Object.keys(this.props.CalcStore.editor).length !== 0) {
                    this.props.CalcStore.setFormula(response.data.data.formula);
                }
            })
            .finally(() => {});
    }

    render() {
        let elem = null;
        if (this.state.showEditor) {
            elem =
                <div style={{overflowX: 'hidden'}}>
                    <CalcPanel/>
                    <Editor
                        initFunc={this.initEditor}
                        language="dzetaCalc"
                        functions={this.state.functions}
                        methods={this.state.methods}
                        formulaInit={this.state.formulaInit}
                    />
                    <CalcFooter/>
                    <CalcSidebar
                        functions={this.state.functions}
                    />
                </div>;
        } else {
            elem = <BlockUi className="calc-page-block" tag="div" loader={<Loader active type="ball-pulse"/>} blocking={!this.state.showEditor}/>
        }
        return elem;
    }
}

const CalcMainTranslation = withTranslation(['calc'])(CalcMainView);

export default function CalcMain(props) {
    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <CalcMainTranslation {...props}/>
        </React.Suspense>
    );
}
