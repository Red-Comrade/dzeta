import React from "react";
import ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Loader from 'react-loaders';
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';
import 'react-tabs/style/react-tabs.css';

import SaveButton from './Controls/SaveButton' ;
import RunButton from './Controls/RunButton' ;
import SidebarButton from './Controls/SidebarButton' ;
import NameInput from './Controls/NameInput' ;

@inject("CalcStore")
@observer
class CalcPanelTabs extends React.Component {
    constructor(props) {
        super(props);
        i18next.loadNamespaces('calc');
    }

    render() {
        return (
            <Tabs style={{ 'height': '80px' }}>
                <TabList>
                    <SaveButton/>
                    <RunButton/>
                    <SidebarButton/>
                    <Tab>{i18next.t('calc:Tabs.Settings')}</Tab>
                </TabList>
                <TabPanel>
                    <NameInput/>
                </TabPanel>
            </Tabs>
        );
    }
}

const CalcPanelTranslation = withTranslation(['calc'])(CalcPanelTabs);

export default function CalcPanel() {
    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <CalcPanelTranslation />
        </React.Suspense>
    );
}
