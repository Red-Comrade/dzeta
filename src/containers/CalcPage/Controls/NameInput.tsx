import React from 'react';
import ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';

@inject("CalcStore")
@inject("ToastrStore")
@observer
class NameInputView extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.props.CalcStore.setName(event.target.value);
    }

    render() {
        const marginStyle = { marginRight: '5px' };
        return (
            <label>
                <span style={marginStyle}>{i18next.t('calc:Settings.LabelNameClass')}</span>
                <input type="text"  value={this.props.CalcStore.name} onChange={this.handleChange}/>
            </label>
        );
    }
}

const NameInputTranslation = withTranslation()(NameInputView);

export default function NameInput() {
    return (
        <React.Suspense fallback='loader'>
            <NameInputTranslation/>
        </React.Suspense>
    );
}