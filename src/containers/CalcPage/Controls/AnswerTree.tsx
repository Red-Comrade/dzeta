import React from 'react';
import ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";

import i18next from 'i18next';
import { withTranslation } from 'react-i18next';
import Tree from 'rc-tree';


@inject("CalcStore")
@observer
class AnswerTreeView extends React.Component
{
	constructor(props) {
		super(props);
		this.treeRef = React.createRef();
	}

	onSelect = (selectedKeys, info) => {
	    console.log('selected', selectedKeys, info);
	};

	onExpand = expandedKeys => {
	    console.log('onExpand', expandedKeys);
	};

	onSelect = (selectedKeys, info) => {
	    console.log('selected', selectedKeys, info);
	    this.selKey = info.node.props.eventKey;
	};

  	onCheck = (checkedKeys, info) => {
    	console.log('onCheck', checkedKeys, info);
 	};

	render() {
		return (
			<div style={{overflow: 'auto', height: '100%'}}>
				<Tree
		          	className="myCls"
		          	showLine
		          	selectable={false}
		          	onExpand={this.props.onExpand ? this.props.onExpand : this.onExpand}
		          	onSelect={this.onSelect}
		          	onCheck={this.onCheck}
		          	treeData={this.props.treeData}
		          	expandedKeys={this.props.expandedKeys ? this.props.expandedKeys : []}
		        />
		    </div>
		)
	}
}

const AnswerTreeTranslation = withTranslation()(AnswerTreeView);

export default function AnswerTree(props) {
  return (
    <React.Suspense fallback='...'>
      <AnswerTreeTranslation {...props} />
    </React.Suspense>
  );
}