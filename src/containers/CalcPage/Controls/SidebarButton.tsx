import React from 'react';
import ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';

@inject("CalcStore")
@inject("ToastrStore")
@observer
class RunButtonView extends React.Component {
    constructor(props) {
        super(props);
    }

    handleClick = () => {
        this.props.CalcStore.triggerSidebar()
    }

    render() {
        return (
            <button
                className="calc-toolbar-btn btn btn-default"
                title={i18next.t('calc:Buttons.FuncsButtons.tooltip')}
                onClick={this.handleClick}
            >
                <i className='fas fa-square-root-alt'></i>
            </button>
        );
    }
}


const RunButtonTranslation = withTranslation()(RunButtonView);

export default function RunButton() {
    return (
        <React.Suspense fallback='loader'>
            <RunButtonTranslation/>
        </React.Suspense>
    );
}
