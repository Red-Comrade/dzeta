import React from 'react';
import ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';
import { RunCalc } from '../../../api/Calc-api';

@inject("CalcStore")
@inject("ToastrStore")
@observer
class RunButtonView extends React.Component {
    constructor(props) {
        super(props);
    }

    convertAnswers = (answers, key) => {
        const tree = [];
        for (const index in answers) {
            const answer = this.convertAnswer(answers[index], key, index);
            if (answer) {
                tree.push(answer);
            }
        }
        return tree;
    }

    convertAnswer = (answer, key, index) => {
        const k = key + index;
        let icon = null;
        let title = null;
        let children = [];
        if (answer.type == 'string') {
            title = answer.var ? (answer.var + ' = '  + answer.value) : index + ' = ' + answer.value;
            icon = <span className="fas fa-font"/>;
        } else if (answer.type == 'number') {
            title = answer.var ? (answer.var + ' = '  + answer.value) : index + ' = ' + answer.value;
            icon = <span className="fas fa-calculator"/>;
        } else if (answer.type == 'datetime') {
            const value = answer.props && answer.props.formatted ? answer.props.formatted : answer.value;
            title = answer.var ? (answer.var + ' = '  +  value) : index + ' = ' + value;
            icon = <span className="fas fa-calendar-alt"/>;
        } else if (answer.type == 'bool') {
            title = answer.var ? (answer.var + ' = '  + answer.value) : index + ' = ' + answer.value;
            icon = answer.value ? <span className="fas fa-equals"/> : <span className="fas fa-not-equal"/>;
        } else if (answer.type == 'null') {
            title = answer.var ? (answer.var + ' = '  + answer.value) : index + ' = ' + answer.value;
            icon = <span className="far fa-square"/>;
        } else if (answer.type == 'object') {
            const value = answer.props && answer.props.name ? answer.props.name : answer.value;
            title = answer.var ? (answer.var + ' = '  + value) : index + ' = ' + value;
            icon = <span className="fas fa-file-invoice"/>;
            children.push({
                key: 'answerCalcuid' + index,
                title:'uid = ' + answer.value,
                icon: <span className="fas fa-address-book"/>,
                children: []
            });
        } else if (answer.type == 'class') {
            const value = answer.props && answer.props.name ? answer.props.name : answer.value;
            title = answer.var ? (answer.var + ' = '  + value) : index + ' = ' + value;
            icon = <span className="fas fa-file"/>;
            children.push({
                key: 'answerCalcuid' + index,
                title:'uid = ' + answer.value,
                icon: <span className="fas fa-address-book"/>,
                children: []
            });
        } else if (answer.type == 'attribute') {
            const value = answer.props && answer.props.name ? answer.props.name : answer.value;
            title = answer.var ? (answer.var + ' = '  + value) : index + ' = ' + value;
            icon = <span className="fas fa-bookmark"/>;
            children.push({
                key: 'answerCalcuid' + index,
                title:'uid = ' + answer.value,
                icon: <span className="fas fa-address-book"/>,
                children: []
            });
        } else if (answer.type == 'array') {
            title = answer.var ? (answer.var + ' = array') : 'array';
            icon = <span className="fas fa-layer-group"/>;
            children.push({
                key: k + 'Values',
                title: i18next.t('calc:Tree.answer.values'),
                icon: icon,
                children: this.convertAnswers(answer.value, k + 'Values'),
            })
        }
        if (icon) {
            children.push({
                key: k + 'Type',
                title: i18next.t('calc:Tree.answer.type') + ' = ' + answer.type,
                icon: <span className="fas fa-tag"/>,
                children: null,
            });
            if (answer.rows) {
                children.push({
                    key: k + 'Rows',
                    title: i18next.t('calc:Tree.answer.rows') + ' = ' + answer.rows.join(', '),
                    icon: <span className="fas fa-list-ol"/>,
                    children: null,
                });
            }
            let loop = [];
            if (answer.loop) {
                loop = this.convertAnswers(answer.loop, k + 'Loop');
            }
            if (loop.length) {
                children.push({
                    key: k + 'Loop',
                    title: i18next.t('calc:Tree.answer.loop'),
                    icon: <span className="fas fa-undo"/>,
                    children: loop,
                });
            }
            let history = [];
            if (answer.history) {
                history = this.convertAnswers(answer.history, k + 'History')
            }
            if (history.length) {
                children.push({
                    key: k + 'History',
                    title: i18next.t('calc:Tree.answer.history'),
                    icon: <span className="fas fa-history"/>,
                    children: history,
                });
            }
            return {
                key: k,
                title: title,
                icon: icon,
                children: children
            };
        }
        return null;
    }

    convertErrorsAndWarnings = (data, key) => {
        const tree = [];
        let icon = null;
        if (key == 'errorsCalc') {
            icon = <span className="fas fa-bug"/>;
        } else if (key == 'warningsCalc') {
            icon = <span className="fas fa-exclamation-triangle"/>;
        }
        for (const index in data) {
            tree.push({
                key: key + index,
                title: '[' + data[index].rows.join(', ') + '] ' + data[index].text,
                icon: icon,
                children: [],
            });
        }
        return tree;
    }

    handleClick = () => {

        RunCalc(
            this.props.CalcStore.getFormula(),
            this.props.CalcStore.calc,
            this.props.CalcStore.class
        )
            .then((response) => {
                const data = response.data;
                if (data.result == 1) {
                    let answerTree = [],
                        rootAnswer = {
                            key: 'answerCalcRoot0',
                            title: i18next.t('calc:Tree.answer.rootLabel'),
                            icon: <span className="fas fa-sitemap"/>,
                            children: []
                        };
                    if (data.data.answer) {
                        rootAnswer.children = this.convertAnswers(data.data.answer, 'answerCalc');
                    }
                    this.props.CalcStore.setAnswers([rootAnswer]);
                    let errors = [];
                    if (data.data.errors.errors) {
                        errors = this.convertErrorsAndWarnings(data.data.errors.errors, 'errorsCalc');
                    }
                    this.props.CalcStore.setErrors(errors);
                    let warnings = [];
                    if (data.data.errors.warnings) {
                        warnings = this.convertErrorsAndWarnings(data.data.errors.warnings, 'warningsCalc');
                    }
                    this.props.CalcStore.setWarnings(warnings);
                } else {
                    this.props.ToastrStore.error(
                        i18next.t('calc:Buttons.RunButtons.errors.runError'),
                        i18next.t('calc:Buttons.RunButtons.errors.title')
                    );
                    data.errors.forEach((val) => {
                        this.props.ToastrStore.error(
                            val.text,
                            i18next.t('calc:Buttons.RunButtons.errors.title')
                        );
                    });
                }
            });
    }

    render() {
        return (
            <button
                className="calc-toolbar-btn btn btn-default"
                title={i18next.t('calc:Buttons.RunButtons.tooltip')}
                onClick={this.handleClick}
            >
                <i className='fas fa-play'></i>
            </button>
        );
    }
}


const RunButtonTranslation = withTranslation()(RunButtonView);

export default function RunButton() {
    return (
        <React.Suspense fallback='loader'>
            <RunButtonTranslation/>
        </React.Suspense>
    );
}
