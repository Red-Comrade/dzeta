import React from 'react';
import ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';
import { SaveCalc } from '../../../api/Calc-api';

@inject("CalcStore")
@inject("ToastrStore")
@observer
class SaveButtonView extends React.Component {
    constructor(props) {
        super(props);
    }

    handleClick = () => {

        if (this.props.CalcStore.name == '') {
            this.props.ToastrStore.error(
                i18next.t('calc:Buttons.SaveButtons.errors.notName'),
                i18next.t('calc:Buttons.SaveButtons.errors.title')
            );
            return;
        }

        if(this.props.CalcStore.getFormula() == '') {
            this.props.ToastrStore.error(
                i18next.t('calc:Buttons.SaveButtons.errors.notValue'),
                i18next.t('calc:Buttons.SaveButtons.errors.title')
            );
            return;
        }

        SaveCalc(
            this.props.CalcStore.getFormula(),
            this.props.CalcStore.class,
            this.props.CalcStore.calc,
            this.props.CalcStore.name
        )
            .then((response) => {
                const data = response.data;
                if (data.result == 1) {
                    if (this.props.CalcStore.calc == '') {
                        history.pushState(null, null, `${location.href}&uid=${data.data.uid}`);
                    }
                    this.props.CalcStore.setCalc(data.data.uid);
                    this.props.ToastrStore.success(
                        i18next.t('calc:Buttons.SaveButtons.success.goodSave'),
                        i18next.t('calc:Buttons.SaveButtons.success.title')
                    );
                } else {
                    data.errors.forEach((val) => {
                        this.props.ToastrStore.error(
                            val.text,
                            i18next.t('calc:Buttons.SaveButtons.errors.title')
                        )
                    });
                }
            });
    }

    render() {
        return (
            <button
                className="calc-toolbar-btn btn btn-default"
                title={i18next.t('calc:Buttons.SaveButtons.tooltip')}
                onClick={this.handleClick}
            >
                <i className='fas fa-save'></i>
            </button>
        );
    }
}


const SaveButtonTranslation = withTranslation()(SaveButtonView);

export default function SaveButton() {
    return (
        <React.Suspense fallback='loader'>
            <SaveButtonTranslation/>
        </React.Suspense>
    );
}
