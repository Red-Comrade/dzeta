import React from "react";
import ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import Loader from 'react-loaders';
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';

@inject("CalcStore")
@observer
class CalcSidebarView extends React.Component {
    constructor(props) {
        super(props);
        i18next.loadNamespaces('calc');
        this.state = {
            search: '',
            categories: {},
        };
    }

    insertSnippet = (snippet) => {
        this.props.CalcStore.insertSnippet(snippet);
    }

    handleSearch = (e) => {
        this.setState({
            search: e.target.value.trim()
        });
    }

    handleCategoryClick = (category) => {
        const categories = this.state.categories;
        categories[category] = !categories[category];
        this.setState({
            categories: categories
        });
    }

    render() {
        const functions = this.props.functions ? this.props.functions : [];
        const categories = [];
        for (const [index, category] of functions.entries()) {
            const buttons = [];
            for (const [i, func] of category.functions.entries()) {
                let push = false;
                if (this.state.search.length > 0) {
                    if (func.name.indexOf(this.state.search) !== -1) {
                        push = true;
                    }
                } else {
                    push = true;
                }
                if (push) {
                    buttons.push(
                        <button
                            className="calc-function-btn btn btn-default"
                            key={"calc-function-" + func.name}
                            title={func.label}
                            onClick={() => {this.insertSnippet(func.insertText)}}
                        >
                            {func.name}
                        </button>
                    );
                }
            }
            if (buttons.length) {
                const showFunctions = this.state.categories[category.category] || this.state.search.length > 0;
                categories.push(
                    <div className={"calc-sidebar-functions-category"  + (showFunctions ? "" : " hide-calc-sidebar-functions-category")} key={"calc-function-category" + category.category}>
                        <div
                            className="calc-sidebar-functions-category-label"
                            onClick={() => {this.handleCategoryClick(category.category)}}
                        >
                            <i className={(showFunctions ? "fas fa-angle-down" : "fas fa-angle-right") + " calc-sidebar-functions-category-label-icon"}></i>
                            {i18next.t('calc:Functions.Categories.' + category.category)}
                        </div>
                        <div className="calc-sidebar-functions-category-functions">
                            {buttons}
                        </div>
                    </div>
                )
            }
        }

        return (
            <div className={(this.props.CalcStore.showSidebar ? '' : 'hide-calc-sidebar') + ' calc-sidebar'}>
                <div className="calc-toolbar">
                    <button
                        className="calc-toolbar-btn btn btn-default"
                        onClick={() => {this.props.CalcStore.hideSidebar()}}
                    >
                        <i className="fas fa-window-close"></i>
                    </button>
                    <input className="calc-toolbar-search form-control" value={this.state.search} onChange={this.handleSearch}/>
                </div>
                <div className="calc-sidebar-functions">
                    {categories}
                </div>
            </div>
        );
    }
}

const CalcSidebarTranslation = withTranslation(['calc'])(CalcSidebarView);

export default function CalcSidebar(props) {
    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <CalcSidebarTranslation {...props} />
        </React.Suspense>
    );
}
