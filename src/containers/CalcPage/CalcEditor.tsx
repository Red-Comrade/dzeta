import * as React from 'react';
import * as monaco from 'monaco-editor';

interface IEditorPorps {
    language: string;
    initFunc: any
}

const Editor: React.FC<IEditorPorps> = (props: IEditorPorps) => {
    let divNode;
    const assignRef = React.useCallback((node) => {
        // On mount get the ref of the div and assign it the divNode
        divNode = node;
    }, []);

    const getSuggestion = (data) => {
        let kind = monaco.languages.CompletionItemKind.Keyword;
        switch (data.kind) {
            case 'function':
                monaco.languages.CompletionItemKind.Function;
                break;
            case 'method':
                monaco.languages.CompletionItemKind.Method;
                break;
        }
        return {
            label: data.label,
            kind: kind,
            insertText: data.insertText,
            insertTextRules: monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
            detail: data.detail,
            documentation: data.documentation
        }
    }

    React.useEffect(() => {
        if (divNode) {
            monaco.languages.register({ id: 'dzetaCalc' });
            monaco.languages.setLanguageConfiguration('dzetaCalc', {
                wordPattern: /(-?\d*\.\d\w*)|([^\`\~\!\@\#\%\^\&\*\(\)\-\=\+\[\{\]\}\\\|\;\:\'\"\,\.\<\>\/\?\s]+)/g,
                comments: {
                    lineComment: "//",
                    blockComment: ["/*", "*/"]
                },
                brackets: [["{", "}"], ["[", "]"], ["(", ")"]],
                autoClosingPairs: [{
                    open: "{",
                    close: "}",
                    notIn: ["string"]
                }, {
                    open: "[",
                    close: "]",
                    notIn: ["string"]
                }, {
                    open: "(",
                    close: ")",
                    notIn: ["string"]
                }, {
                    open: '"',
                    close: '"',
                    notIn: ["string"]
                }, {
                    open: "'",
                    close: "'",
                    notIn: ["string", "comment"]
                }],
                folding: {
                    markers: {
                        start: new RegExp("^\\s*(#|//)region\\b"),
                        end: new RegExp("^\\s*(#|//)endregion\\b")
                    }
                }
            });
            monaco.languages.setMonarchTokensProvider('dzetaCalc', {
                defaultToken: "",
                tokenPostfix: "",
                tokenizer: {
                    root: [
                        [
                            /[a-zA-Z_]\w*/,
                            {
                                cases: {
                                    "@dzetaCalcKeywords": {
                                        token: "keyword.dzetaCalc"
                                    },
                                    "@default": "identifier.dzetaCalc"
                                }
                            }
                        ],
                        [
                            /[$a-zA-Z_]\w*/,
                            {
                                cases: {
                                    "@default": "variable.dzetaCalc"
                                }
                            }
                        ],
                        [/[{}]/, "delimiter.bracket.dzetaCalc"],
                        [/[\[\]]/, "delimiter.array.dzetaCalc"],
                        [/[()]/, "delimiter.parenthesis.dzetaCalc"],
                        [/[ \t\r\n]+/],
                        [/(#|\/\/)$/, "comment.dzetaCalc"],
                        [/(#|\/\/)/, "comment.dzetaCalc", "@dzetaCalcLineComment"],
                        [/\/\*/, "comment.dzetaCalc", "@dzetaCalcComment"],
                        [/"/, "string.dzetaCalc", "@dzetaCalcDoubleQuoteString"],
                        [/'/, "string.dzetaCalc", "@dzetaCalcSingleQuoteString"],
                        [/[\+\-\*\%\&\|\^\~\!\=\<\>\/\?\;\:\.\,\@]/, "delimiter.dzetaCalc"],
                        [/\d*\d+[eE]([\-+]?\d+)?/, "number.float.dzetaCalc"],
                        [/\d*\.\d+([eE][\-+]?\d+)?/, "number.float.dzetaCalc"],
                        [/0[xX][0-9a-fA-F']*[0-9a-fA-F]/, "number.hex.dzetaCalc"],
                        [/0[0-7']*[0-7]/, "number.octal.dzetaCalc"],
                        [/0[bB][0-1']*[0-1]/, "number.binary.dzetaCalc"],
                        [/\d[\d']*/, "number.dzetaCalc"],
                        [/\d/, "number.dzetaCalc"]
                    ],
                    dzetaCalcComment: [
                        [/\*\//, "comment.dzetaCalc", "@pop"],
                        [/[^*]+/, "comment.dzetaCalc"],
                        [/./, "comment.dzetaCalc"]
                    ],
                    dzetaCalcLineComment: [
                        [
                            /\?>/,
                            {
                                token: "@rematch",
                                next: "@pop"
                            }
                        ],
                        [/.$/, "comment.dzetaCalc", "@pop"],
                        [/[^?]+$/, "comment.dzetaCalc", "@pop"],
                        [/[^?]+/, "comment.dzetaCalc"],
                        [/./, "comment.dzetaCalc"]
                    ],
                    dzetaCalcDoubleQuoteString: [
                        [/[^\\"]+/, "string.dzetaCalc"],
                        [/@escapes/, "string.escape.dzetaCalc"],
                        [/\\./, "string.escape.invalid.dzetaCalc"],
                        [/"/, "string.dzetaCalc", "@pop"]
                    ],
                    dzetaCalcSingleQuoteString: [
                        [/[^\\']+/, "string.dzetaCalc"],
                        [/@escapes/, "string.escape.dzetaCalc"],
                        [/\\./, "string.escape.invalid.dzetaCalc"],
                        [/'/, "string.dzetaCalc", "@pop"]
                    ]
                },
                dzetaCalcKeywords: ["and", "array", "break", "case", "continue", "do", "else", "elseif", "false", "for", "foreach", "if", "null", "or", "switch", "true", "while", "return"],
                escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/
            });
            monaco.languages.registerCompletionItemProvider('dzetaCalc', {
                triggerCharacters: ["."],
                provideCompletionItems: (model, position, context, token) => {
                    let suggestions = [];
                    if (props.functions && Array.isArray(props.functions)) {
                        for (let i = 0; i < props.functions.length; i++) {
                            for (let j = 0; j < props.functions[i].functions.length; j++) {
                                suggestions.push(getSuggestion(props.functions[i].functions[j]));
                            }
                        }
                    }

                    const wordModel = model.getWordAtPosition(position);
                    const columnBeforeWord =
                        !!wordModel && wordModel.word.length
                            ? position.column - wordModel.word.length
                            : 0;

                    var textUntilPosition = model.getValueInRange({
                        startLineNumber: 1,
                        startColumn: 1,
                        endLineNumber: position.lineNumber,
                        endColumn: Math.abs(columnBeforeWord),
                    });

                    let words = [...textUntilPosition.matchAll(/\$?\w+/gi)].map((x) => x[0]);
                    if (!!!words) {
                        return { suggestions: suggestions };
                    }

                    const methodsSuggestions = [];
                    if (props.methods && Array.isArray(props.methods)) {
                        for (let i = 0; i < props.methods.length; i++) {
                            for (let j = 0; j < props.methods[i].methods.length; j++) {
                                methodsSuggestions.push(getSuggestion(props.methods[i].methods[j]));
                            }
                        }
                    }
                    const labels = [
                        ...suggestions.map((s) => s.label.toUpperCase()),
                        ...methodsSuggestions.map((s) => s.label.toUpperCase()),
                    ];
                    words = [
                        ...new Set(
                            words.filter((w) => labels.every((l) => l !== w.toUpperCase()))
                        ),
                    ];

                    let merged = []
                    if (context.triggerCharacter == '.') {
                        merged = [
                            ...methodsSuggestions
                        ];
                    } else {
                        merged = [
                            ...suggestions,
                            ...words.map((w) => {
                                return {
                                    kind: w.charAt(0) == '$' ? monaco.languages.CompletionItemKind.Variable : monaco.languages.CompletionItemKind.Text,
                                    label: w,
                                    insertText: w,
                                };
                            }),
                        ];
                    }

                    // Return combined suggestions
                    return { suggestions: merged };
                }
            })
            const editor = monaco.editor.create(divNode, {
                language: props.language,
                minimap: { enabled: false }

            });
            props.initFunc(editor);
            const resizeObserver = new ResizeObserver((entries) => {
                window.requestAnimationFrame(() => {
                    if (!Array.isArray(entries) || !entries.length) {
                        return;
                    }
                    editor.layout();
                });
            });
            resizeObserver.observe(divNode);
            return () => {
                resizeObserver.disconnect();
            }
        }
    }, [assignRef])

    return <div ref={assignRef} style={{height: '60vh'}}></div>;
}

export default Editor;
