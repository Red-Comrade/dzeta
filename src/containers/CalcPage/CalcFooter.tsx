import React from "react";
import ReactDOM from "react-dom";
import Tree, { TreeNode } from 'rc-tree';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Loader from 'react-loaders';
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';
import 'react-tabs/style/react-tabs.css';

import { inject, observer } from "mobx-react";

import AnswerTree from './Controls/AnswerTree';

@inject("CalcStore")
@observer
class CalcFooterView extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {
        const tabStyle = { height: 'calc(100% - 48px)' };
        return (
            <Tabs style={{height:'calc(100% - 60vh - 80px)'}}>
                <TabList>
                    <Tab>{i18next.t('calc:Tabs.Answer')}</Tab>
                    <Tab>{i18next.t('calc:Tabs.Warnings')}</Tab>
                    <Tab>{i18next.t('calc:Tabs.Errors')}</Tab>
                </TabList>
                <TabPanel style={tabStyle}>
                    <AnswerTree
                        treeData={this.props.CalcStore.answers}
                        expandedKeys={this.props.CalcStore.answersExpandedKeys}
                        onExpand={(expandedKeys) => this.props.CalcStore.setAnswersExpandedKeys(expandedKeys)}
                    />
                </TabPanel>
                <TabPanel style={tabStyle}>
                    <AnswerTree
                        treeData={this.props.CalcStore.warnings}
                        expandedKeys={this.props.CalcStore.warningsExpandedKeys}
                        onExpand={(expandedKeys) => this.props.CalcStore.setWarningsExpandedKeys(expandedKeys)}
                    />
                </TabPanel>
                <TabPanel style={tabStyle}>
                    <AnswerTree
                        treeData={this.props.CalcStore.errors}
                        expandedKeys={this.props.CalcStore.errorsExpandedKeys}
                        onExpand={(expandedKeys) => this.props.CalcStore.setErrorsExpandedKeys(expandedKeys)}
                    />
                </TabPanel>
            </Tabs>
        );
    }
}

const CalcFooterTranslation = withTranslation(['calc'])(CalcFooterView);

export default function CalcFooter() {
    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <CalcFooterTranslation />
        </React.Suspense>
    );
}

