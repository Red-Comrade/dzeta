import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect, useParams, useRouteMatch, NavLink } from 'react-router-dom'
import { inject, observer } from "mobx-react";


import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import i18next from 'i18next';
import Lang from '../lang-change/lang';

//import {ViewTableCloth} from "../ViewPage/ViewTableCloth";
import * as ks_api from '../../api/KeysSo-api'
import RootStore from "../../stores/RootStore";


const ViewTableCloth = React.lazy(() =>
    import('../ViewPage/ViewTableCloth')
        .then(({ ViewTableCloth }) => ({ default: ViewTableCloth })),
);

export interface ILoginPageProps {
    ToastrStore?: any,
    RootStore?: any,
}
interface ILoginPageState {
    blocking: boolean;
    isLoadedParentTable: boolean;
    isLoadedTable: boolean;
    isOpenParentTable: boolean;
    isOpenTable: boolean;
    url_value: string;
    ParentObj: string;
}

@inject("loginStore")
@inject("ToastrStore")
@inject("RootStore")
@observer
export class LoginPage extends React.Component<ILoginPageProps, ILoginPageState> {
    LocalConfig = localStorage.config ? JSON.parse(localStorage.config) : {};

    state: Readonly<ILoginPageState> = {
        blocking: true,
        isLoadedParentTable: true, //инициатор загрузки таблицы
        isLoadedTable: true, //инициатор загрузки таблицы
        isOpenParentTable: false, //Если false, то нужно нажать кнопку прогрузки таблицы
        isOpenTable: false, //Если false, то нужно нажать кнопку прогрузки таблицы
        url_value: "",
        ParentObj: "",
    };

    constructor(props: ILoginPageProps) {
        super(props);
    };

    componentDidMount(): void {
        document.title = 'Login | ' + RootStore.config.nameSystem;
    }

    events = {
        onclickURLbtn: (e) => {
            if(this.state.url_value.trim()) {
                this.setState({isLoadedParentTable: false, isOpenParentTable: true});
                this.core.topsActionAJAX(this.state.url_value,(answer) => {
                    this.setState({
                        ParentObj: answer.data.website,
                        isLoadedParentTable: true,
                        isOpenParentTable: true
                    });
                    this.setState({isLoadedTable: false, isOpenTable: true});
                    this.core.queryActionAJAX(this.state.url_value, (answer) => {
                        this.setState({
                            ParentObj: answer.data.website,
                            isLoadedTable: true,
                            isOpenTable: true
                        });
                    }, () => {});

                }, () => {});
            }
        }
    };

    core = {
        topsActionAJAX: (domain, SuccFunc, ErrFunc) => {
            ks_api
                .tops(domain)
                .then(res => {
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc(res.data)
                    }
                })
                .catch( (error) =>{
                    console.log(error);
                    ErrFunc({result: 0});
                });
        },
        queryActionAJAX: (domain, SuccFunc, ErrFunc) => {
            /*
            SuccFunc({data:{website: 'e9f3f54b-c7cc-4617-8a94-fb47381b096c'}});
            return;
            */
            ks_api
                .query(domain)
                .then(res => {
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                        if (res.data.data.block) {
                            this.core.fillActionAJAX(res.data.data.block);
                        }
                    }
                    else {
                        ErrFunc(res.data)
                    }
                })
                .catch( (error) =>{
                    console.log(error);
                    ErrFunc({result: 0});
                });
        },
        fillActionAJAX: (block) => {
            ks_api
                .fill(block)
                .catch( (error) =>{
                    console.log(error);
                    //ErrFunc({result: 0});
                });
        }
    };

    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        //  LoaderBlockUI = <h4 className="loader-text">Loading...</h4>;
        let isBlocking = false;
        let isBlockingParent = false;



        let ViewTableVisible;
        let ViewTableParentVisible;


        if(this.LocalConfig.AppUID) {
            let DataViewTableParentCloth = {
                id: "MainTableParent",
                AppUid: this.LocalConfig.AppUID,
                IsHideTabs: true,
                SelectObjs: [this.state.ParentObj],
                ViewUid: null,
            };

            if(this.LocalConfig.ViewUIDparrent) {
                DataViewTableParentCloth.ViewUid = this.LocalConfig.ViewUIDparrent;
            }

            let DataViewTableCloth = {
                id: "MainTable",
                ParentApp: null,
                //ParentView: this.props.ActiveTabUid,

                AppUid: this.LocalConfig.AppUID,
                ViewUid: null,
                ActiveTabUid: null,

                ActiveTabName: "",
                SelectObjs: [],
                ParentObj: null,
                IsZaglushka: false,
                IsHideTabs: true,

                Sort: null,
            };

            if(this.LocalConfig.ViewUID) {
                DataViewTableCloth.ParentApp = this.LocalConfig.AppUID;
                DataViewTableCloth.ViewUid = this.LocalConfig.ViewUID;
                DataViewTableCloth.ParentObj = this.state.ParentObj;
                DataViewTableCloth.Sort = {
                    Attr: this.LocalConfig.SortAttrUID,
                    Dir: this.LocalConfig.SortAttrDir
                }
            }

            if(this.state.isOpenParentTable && this.state.isLoadedParentTable) {
                ViewTableParentVisible = <ViewTableCloth {...DataViewTableParentCloth} />;
            }

            if(this.state.isOpenTable && this.state.isLoadedTable) {
                ViewTableVisible = <ViewTableCloth {...DataViewTableCloth} />;
            }
        }


        if(this.state.isOpenParentTable && !this.state.isLoadedParentTable) {
            //значит, что на данный момент загружается таблица
            isBlockingParent = true;
        }


        if(this.state.isOpenTable && !this.state.isLoadedTable) {
            //значит, что на данный момент загружается таблица
            isBlocking = true;
        }


        let isDisabledUrlSubmButn = this.state.url_value.trim() == "";


        return (<BlockUi id="zt_LoginPage" tag="div" className={(this.state.isOpenParentTable ? "zt_login_submit_table_panel_open" : "")} loader={LoaderBlockUI} blocking={isBlockingParent}>
                <div className={"zt_login_top_panel"}>
                    <div className="zt_login-header-image" >
                        <a href="/">
                            <img className={'zt_login-img-lang'} src={this.LocalConfig.img_logo_url} alt={this.LocalConfig.img_logo_alt}  />
                        </a>
                    </div>
                    <div className="zt_login-header-c">

                    </div>
                    <div className="zt_login-header-lang">
                        <div style={{padding:"16px 0px 16px 0"}}>
                            <a href="#/login/authorization"><span>Вход/Регистрация</span></a>
                        </div>

                        <div style={{ height: "20px", "right":'10px',"marginTop":"15px" }} >
                            <Lang/>
                        </div>
                    </div>
                </div>
                <div className={"zt_login_submit_table_panel"}>
                    <div className="input-group zt_login_wrapper_url_top">
                        <input title="Enter URL" placeholder={'Введите адрес сайта'} className="form-control zt_login_input_url_top" type="text" value={this.state.url_value}
                               onChange={ (e) => {
                                   this.setState({url_value: e.target.value});
                               }}
                               onKeyDown={(e) => {
                                   if (e.key === 'Enter') {
                                       this.events.onclickURLbtn(e);
                                   }
                               }} />
                        <div className="input-group-append">
                            <button disabled={isDisabledUrlSubmButn} type="button" className="btn btn-outline-secondary" onClick={this.events.onclickURLbtn}>Проверить позиции!</button>
                        </div>
                    </div>
                </div>
                <div className={"LoginContainer"}>
                    <div className={"zt_co_table_1"}>
                        {ViewTableParentVisible}
                    </div>
                    <BlockUi className={"zt_co_table_2"} tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                        {ViewTableVisible}
                    </BlockUi>
                </div>
            </BlockUi>
    )
    }
}