import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect, useParams, useRouteMatch, NavLink } from 'react-router-dom'
import { inject, observer } from "mobx-react";
import { withTranslation, WithTranslation } from 'react-i18next';
import { login,forgotPasswordEmail } from '../../api/login-api';

import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';


import i18next from 'i18next';

import { default as myI18n } from '../../i18n'
import Lang from '../lang-change/lang';



import * as h_api from '../../api/Home-api'
import {ViewTableCloth} from "../ViewPage/ViewTableCloth";
import {coral} from "color-name";
import * as ct_api from "../../api/ClassTree-api";
import ToastrStore from "../../stores/ToastrStore";

import ItemAttr from "../ObjectsPage/Interfaces/IitemAttr";
import * as f_helper from "../../helpers/form_datatype_helper";
import {IValueFile} from "../../api/ObjTree-api";
import RootStore from "../../stores/RootStore";
import * as ot_api from "../../api/ObjTree-api";


export interface IRegistrationPageProps extends WithTranslation {
    ToastrStore?: any,
    RootStore?: any,
    loginStore?: any,
}
interface IRegistrationPageState {
    isLoadAttr: boolean,
    isLoadSaveContent: boolean,

    attrData: ItemAttr[],
    attrDataCache: any,
    attrDataFileCache: any,

    attr_custom: {
        pass1: string,
        pass2: string,
    },
}

@inject("loginStore")
@inject("ToastrStore")
@inject("RootStore")
@observer
class RegistrationPage1 extends React.Component<IRegistrationPageProps, IRegistrationPageState> {
    LocalConfig = localStorage.config ? JSON.parse(localStorage.config) : {};

    node_sett = {
        TypeID: '00000000-0000-0000-0000-000000000001',
        ObjID: this.LocalConfig.registration_group,
    };

    state: Readonly<IRegistrationPageState> = {
        isLoadAttr: false,
        isLoadSaveContent: false,
        attrData: [],
        attrDataCache: {},
        attrDataFileCache: {},

        attr_custom: {
            pass1: "",
            pass2: "",
        },
    };

    constructor(props: IRegistrationPageProps) {
        super(props);
    };

    componentDidMount(): void {
        console.log(this.LocalConfig);

        this.core.GenerateParamsContent();
    }

    events = {

        onSubmitReg: () => {
            let values = [];
            let Name_obj = "";
            let ValidErrorArr = [];

            this.state.attrData.forEach( (item, index) => {
                let _item = {
                    value: item.value ? item.value.value : item.value,
                    attr: item.uid,
                    key: item.isKey ? 1 : 0,
                    datatype: item.datatype,
                };
                if(_item.datatype != "counter") {
                    if(this.state.attrDataCache[item.uid] != undefined) {
                        _item.value = this.state.attrDataCache[item.uid].value;
                        if(item.datatype == "object") {
                            _item.value = _item.value && Object.keys( _item.value).length > 0 ? _item.value : "";
                        }
                        values.push(_item);
                    }
                    else if(item.isKey) {
                        values.push(_item);
                    }

                    if(item.isKey) {
                        Name_obj += (_item.value === null ? '' : _item.value) + ' ';
                    }

                    if((item.require && !_item.value) || (item.isKey && !_item.value)) {
                        if(item.datatype == "file") {
                            if(this.state.attrDataFileCache[item.uid] == undefined || this.state.attrDataFileCache[item.uid].files.length == 0 ) {
                                ValidErrorArr.push({name: item.name, text: "Enter value!"})
                            }
                        }
                        else {
                            ValidErrorArr.push({name: item.name, text: "Enter value!"})
                        }
                    }
                }
            });

            let ValidErrorArr_custom = this.helper.validationCustom();

            ValidErrorArr = ValidErrorArr.concat(ValidErrorArr_custom);

            if(ValidErrorArr.length > 0) {
                ToastrStore.clear();
                ValidErrorArr.forEach((item_err) => {
                    ToastrStore.error("parameter|" + item_err.name + "| " + item_err.text);
                });
            }
            else {
                this.setState({isLoadSaveContent: true});
                if(Name_obj) {
                    Name_obj = Name_obj.slice(0, -1)
                }
                let password = this.state.attr_custom.pass1;

                this.core.AJAX.addObject(this.node_sett.TypeID, this.node_sett.ObjID, password, values, (answer) => {
                    ToastrStore.success('Success!', 'Success!');
                    this.setState({isLoadSaveContent: false});
                    this.helper.saveCache();

                    let new_ObjID = answer.data.uid;
                    let new_Name = answer.objName;

                    let Files = [];
                    let values:IValueFile[] = [];
                    let iter = 0;
                    if(Object.keys(this.state.attrDataFileCache).length > 0) {
                        RootStore.ProgressIsOpen = true;
                        for( let key_FileCache in this.state.attrDataFileCache) {
                            let File_o = this.state.attrDataFileCache[key_FileCache];
                            let Files_tmp = File_o.files;
                            for( let i = 0; i < Files_tmp.length; i++ ) {
                                Files.push(Files_tmp[i]);
                                values.push({
                                    uid: File_o.data.uid, //uid-параметра
                                    new: 1, //0/1
                                    key: Number(File_o.data.isKey), //0/1
                                    value: iter,
                                    del: 0,
                                });
                                iter ++;
                            }
                        }

                        this.core.AJAX.addFileAJAX(new_ObjID, this.node_sett.TypeID, this.node_sett.ObjID, Files, values, () => {
                            this.setState({attrDataFileCache: {}});
                            RootStore.ProgressIsOpen = false;
                            this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}});
                        }, () => {
                            this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}});
                            RootStore.ProgressIsOpen = false;
                        });
                    }
                    else {
                        this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}});
                    }

                }, (data) => {
                    // ToastrStore.error('Error!', 'ERROR CODE='+data.result);
                    this.helper.showErrors(data.errors);
                    this.setState({isLoadSaveContent: false});
                });
            }
        },

        //Сработает при добавлении пользователя в систему, далее нам нужно будет пройти процедуру создания пароля
        addedObject: (Conf?: {SuccFunc?:()=>void, ObjID?:string, Name?:string }) => {
            if(Conf) {
                login(Conf.Name, this.state.attr_custom.pass1)
                    .then((response) => {
                        const data = response.data;
                        if (data.result == 1) {
                            window.location.reload();
                        }
                        else if(data.result == 0) {
                            ToastrStore.error("Server error!");
                        }
                    })
                    .catch(() => {
                        ToastrStore.error("Server error!");
                    });
            }
        },
    };
    core = {
        AJAX: {
            getAttributesOfGroup: (type: string, group: string, SuccFunc, ErrFunc) => {
                ct_api
                    .getAttributesOfGroup(type, group)
                    .then(res => {
                        if (res.data.result == 1) {
                            SuccFunc(res.data);
                        } else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch((error) => {
                        ErrFunc({result: 0});
                    })
            },
            addObject: function (type: string, parent: string, password: string, values: ot_api.IValue[], SuccFunc, ErrFunc) {
                ot_api
                    .registration(type, parent, password, values)
                    .then(res => {
                        if (res.data.result == 1) {
                            SuccFunc(res.data);
                        } else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        ErrFunc({result: 0});
                    })
            },
            addFileAJAX: (uid:string, type:string, parent:string, Files:any, values: IValueFile[], SuccFunc, ErrorFunc) => {
                ot_api
                    .addFile(uid, type, parent, Files, values, (progressEvent) => {
                        let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                        RootStore.ProgressData.completed = percentCompleted;
                    })
                    .then(res => {
                        SuccFunc(res.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrorFunc();
                    })
            },
        },
        GenerateParamsContent: () => {
            this.setState({isLoadAttr: true});
            this.core.AJAX.getAttributesOfGroup(this.node_sett.TypeID, '00000000-0000-0000-0000-000000000000', (answer)=> {
                let attrData = [];
                if(answer.result == 1) {
                    attrData = answer.data;
                }
                this.setState({isLoadAttr: false, attrData: attrData, attrDataCache: {}, attrDataFileCache: {}})
            }, (answer) => {
                ToastrStore.error('Server error!', 'ErrorCode='+answer.result);
                this.setState({isLoadAttr: false, attrDataCache: {}, attrDataFileCache: {}})
            });
        },


        build: {
            paramsPanel: () =>  {
                let controls = [];
                let controls_key = [];
                let controls_oth = [];


                this.state.attrData.forEach( (item, index) => {
                    const paramsControl:f_helper.IFormDatatypeClass = {
                        item: item,
                        key: index,
                        attrData: this.state.attrData,
                        attrDataCache: this.state.attrDataCache,
                        attrDataFileCache: this.state.attrDataFileCache,
                        type: "add",
                        ObjID: "",
                        TypeID: this.node_sett.TypeID,

                        ChangeStateFunc: () => { this.setState({});}, // this.setState();
                    };
                    let itemControl = new f_helper.FormDatatypeClass(paramsControl);

                    if(item.require) {
                        controls_key.push(itemControl.GetControl());
                    }
                    else {
                        controls_oth.push(itemControl.GetControl());
                    }
                });


                //Тут мы генерируем контролы паролей и еще чего-нибудь основного
                let GenerateLightControls = (key, name) => {
                    let value = this.state.attr_custom[key] ? this.state.attr_custom[key] : "";
                    let is_danger = false;
                    let is_succ = false;
                    let succ_text = "Success";
                    let array_title = [];
                    let type = "text";



                    if(!value) {
                        is_danger = true;
                        array_title.push("Value cannot be empty!")
                    }
                    else if(key == "pass1" || key == "pass2") {
                        type = "password";
                        let err_valid_pass = [];

                        if(key == "pass1") {
                            err_valid_pass = this.helper.validationPass(this.state.attr_custom.pass1);
                        }

                        if(err_valid_pass.length > 0) {
                            is_danger = true;
                            array_title.push(err_valid_pass[0])
                        }
                        if(this.state.attr_custom["pass1"] != this.state.attr_custom["pass2"]) {
                            is_danger = true;
                            array_title.push("Password mismatch!")
                        }
                        else if(value) {
                            is_succ = true;
                        }
                    }

                    return (
                        <div className={"form-group zt_form_control " + (is_danger?"zt_form_control_dang":"") + (is_succ?"zt_form_control_succ":"")}>
                            <div className="hover-label-parent">
                                <div className="hover-label_wrap1">
                                    <label className="hover-label">
                                        <i title={array_title.join(', ')} className="fas fa-exclamation-circle zt_require_i_label"/>{name}
                                    </label>
                                    <label className="hover-label-dang">
                                        <i title={array_title.join(', ')} className="fas fa-exclamation-triangle zt_require_i_label"/>{array_title.join(', ')}
                                    </label>
                                    <label className={"hover-label-succ"}>
                                        <i title={succ_text} className="fas fa-check-circle zt_require_i_label"/>{succ_text}
                                    </label>
                                </div>
                                <div className="hover-label_wrap2"> </div>
                            </div>
                            <div className="input-group">
                                <input placeholder="Enter value" type={type} title="" className="form-control zt_require_input" value={value}
                                       onChange={(e) => {
                                           this.state.attr_custom[key] = e.target.value;
                                           this.setState({});
                                       }}
                                />
                            </div>
                        </div>);
                };


                return <div className={"zt_regp_params"}>
                    <div className={"zt_regp_main_params"}>
                        {controls_key}
                        {GenerateLightControls("pass1", "Password")}
                        {GenerateLightControls("pass2", "Repeat password")}
                    </div>
                    <hr/>
                    {controls_oth}
                </div>;
            }
        }
    };

    helper = {
        resetCache: () => {
            this.setState({attrDataCache: {}, attrDataFileCache:{} });
        },
        saveCache: () => {
            this.state.attrData.forEach( (item, index) => {
                if(this.state.attrDataCache[item.uid] != undefined) {
                    if(!item.value) {
                        item.value = {};
                    }
                    if (item.isArray) {
                        if (item.datatype == 'object') {
                            item.value = this.state.attrDataCache[item.uid].value.map((v) => {
                                return { uid: v.value, name: v.name };
                            });
                        } else {
                            item.value = this.state.attrDataCache[item.uid].value
                        }
                    } else {
                        item.value.value = this.state.attrDataCache[item.uid].value
                    }
                }
            });

            this.setState({attrDataCache: {}});
        },

        getNameModal: (attrData) => {
            let nameModal = "";

            attrData.forEach((item_a, key_a) => {
                if(item_a.isKey) {
                    if(item_a.datatype == "object") {
                        nameModal += (item_a.value &&  item_a.value.name ? item_a.value.name : "") + ' ';
                    }
                    else {
                        nameModal += (item_a.value ? item_a.value.value : "") + ' ';
                    }
                }
            });

            if(nameModal) {
                nameModal = nameModal.slice(0, -1)
            }

            return nameModal;
        },

        showErrors: (errors) => {
            if (errors) {
                let text = [];
                for (let i = 0; i < errors.length; i++) {
                    text.push(errors[i].text);
                }
                ToastrStore.error(text.join("\n"), 'Error');
            } else {
                ToastrStore.error('Server error!', 'Error');
            }
        },


        validationCustom: () => {
            let err_arr = [];


            if(!this.state.attr_custom.pass1) {
                err_arr.push({
                    name: "Password",
                    text: "Value cannot be empty!",
                })
            }
            else {
                let err_valid = this.helper.validationPass(this.state.attr_custom.pass1);
                if(err_valid.length > 0) {
                    err_valid.forEach((value) => {
                        err_arr.push({
                            name: "Password",
                            text: value,
                        })
                    });
                }
            }



            if(!this.state.attr_custom.pass2) {
                err_arr.push({
                    name: "Repeat password",
                    text: "Value cannot be empty!",
                })
            }
            if(this.state.attr_custom.pass1 != this.state.attr_custom.pass2) {
                err_arr.push({
                    name: "Repeat password",
                    text: "Password mismatch!",
                })
            }

            return err_arr
        },

        validationPass: (password) => {
            let err_arr = [];

            if (password.length <= 8) {
                err_arr.push('Password must contain between 8 and 20 characters!');
            }
            if (!/[A-Z]/.test(password)) {
                err_arr.push('Password must contain at least 1 capital letter!');
            }
            if (!/[a-z]/.test(password)) {
                err_arr.push('Password must contain at least 1 small letter!');
            }

            return err_arr;
        }

    };


    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        let isBlocking = false;
        const { t, tReady } = this.props;

        let ParamsForm;
        if(this.state.isLoadAttr || this.state.isLoadSaveContent) {
            isBlocking = true;
        }
        else {
            ParamsForm = this.core.build.paramsPanel();
        }


        return (<BlockUi id="zt_RegistrationPage" tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                <div className={"zt_regp_top_panel"}>
                    <div className="zt_regp-header-image" >
                        <a href="/" onClick={(e)=>{e.preventDefault()}}>
                            <img className={'zt_regp-img-lang'} src={this.LocalConfig.img_logo_url} alt={this.LocalConfig.img_logo_alt}  />
                        </a>
                    </div>
                    <div className="zt_regp-header-c">
                    </div>
                    <div className="zt_regp-header-lang">
                        <div style={{padding:"16px 0px 16px 0"}}>
                            <a href={"#/login/authorization"}><span>{t('authorization')}</span></a>
                        </div>
                        <div style={{ height: "20px", "right":'10px',"marginTop":"15px" }} >
                            <Lang/>
                        </div>
                    </div>
                </div>
                <div className={"zt_regp_container"}>
                    <div className={"zt_regp_params_form"}>
                        {ParamsForm}
                    </div>
                </div>
                <div className={"zt_regp_footer"}>
                    <button className="zt_regp_btn" onClick={()=>{this.events.onSubmitReg();}}>
                        {t('register now')}
                    </button>
                </div>
            </BlockUi>
        )
    }
}


export function RegistrationPage(props) {
    const LoginFormPageTranslation = withTranslation('login')(RegistrationPage1);

    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <LoginFormPageTranslation {...props} i18n={myI18n} />
        </React.Suspense>
    );
}