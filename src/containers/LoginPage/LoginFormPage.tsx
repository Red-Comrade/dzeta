import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect, useParams, useRouteMatch, NavLink } from 'react-router-dom'
import { inject, observer } from "mobx-react";
import { withTranslation, WithTranslation } from 'react-i18next';
import { login,forgotPasswordEmail } from '../../api/login-api';

import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';


import i18next from 'i18next';

import { default as myI18n } from '../../i18n'
import Lang from '../lang-change/lang';



import * as h_api from '../../api/Home-api'
import {ViewTableCloth} from "../ViewPage/ViewTableCloth";
import {coral} from "color-name";
import ToastrStore from "../../stores/ToastrStore";

export interface ILoginFormPageProps extends WithTranslation {
    ToastrStore?: any,
    RootStore?: any,
    loginStore?: any,
}
interface ILoginFormPageState {
    blocking: boolean;

}

@inject("loginStore")
@inject("ToastrStore")
@inject("RootStore")
@observer
class LoginFormPage1 extends React.Component<ILoginFormPageProps, ILoginFormPageState> {
    LocalConfig = localStorage.config ? JSON.parse(localStorage.config) : {};

    loginInputStyle = {
        border: "solid 1px " + this.LocalConfig.color_login,
        background: this.LocalConfig.background_login,
        color: this.LocalConfig.color_login
    };
    state: Readonly<ILoginFormPageState> = {
        blocking: true,

    };

    constructor(props: ILoginFormPageProps) {
        super(props);
    };

    componentDidMount(): void {
    }

    events = {
        handleSubmit: () => {
            this.props.loginStore.setDisable(true);
            login(this.props.loginStore.login, this.props.loginStore.password)
                .then((response) => {
                    const data = response.data;
                    if (data.result == 1) {
                        window.location.reload();
                    }
                    else if(data.result == 0) {
                        this.props.loginStore.setError(true);
                    }
                    else {
                        this.helpers.showErrors(data.errors);
                    }
                })
                .catch(() => {
                    this.props.loginStore.setDisable(false);
                });
        },
        handleLoginChange: (event) => {
            this.props.loginStore.setLogin(event.target.value);
        },
        handleDismiss: (e) => {
            this.props.loginStore.setError(false);
        },
        handlePasswordChange: e => {
            this.props.loginStore.setPassword(e.target.value);
        },
        handleForgotPasswordLink: e => {
            e.preventDefault()
            this.props.loginStore.setVisibleForgotForm(true);
            this.props.loginStore.setPassword('');
        },
        handleForgotPasswordEmailChange: e => {
            this.props.loginStore.setForgotPWDEmail(e.target.value);
        },
        handleForgotPasswordCancel: e => {
            this.props.loginStore.setVisibleForgotForm(false);
        },
        handleForgotPasswordSubmit: e => {
            forgotPasswordEmail(this.props.loginStore.login)
                .then((response) => {
                    const data = response.data;
                    if (data.result == 1) {
                        this.props.loginStore.setVisibleForgotForm(false);
                        this.props.ToastrStore.success(this.props.t('goodForgotEmail'));
                        //ну и тострик
                    } else{
                        //берем массив Errors
                        for(let i=0;i<data.errors.length;i++)
                        {
                            this.props.ToastrStore.error(data.errors[i].text);
                        }

                    }
                });
        },

        onKeyDownInput: e => {
            if (e.key === 'Enter' && !this.props.loginStore.disableLogin) {
                this.events.handleSubmit();
            }
        }
    };

    helpers = {
        showErrors: (errors) => {
            if (errors) {
                let text = [];
                for (let i = 0; i < errors.length; i++) {
                    text.push(errors[i].text);
                }
                ToastrStore.error(text.join("\n"), 'Error');
            } else {
                ToastrStore.error('Server error!', 'Error');
            }
        },
    };

    core = {

    };

    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        //  LoaderBlockUI = <h4 className="loader-text">Loading...</h4>;
        let isBlocking = false;
        const { t, tReady } = this.props;

        let forgotPWDEmailForm;

        let isDisableForgPass = this.props.RootStore.config.nameSystem == "Knowledge Sharing Platform" || this.props.loginStore.disableForgotPWD;

        let reg_div;
        if(this.LocalConfig.forgotPWDEmail || this.LocalConfig.registration_group) {
            let forgotPWDEmail;
            if(this.LocalConfig.forgotPWDEmail)
            {
                forgotPWDEmail=<a className="text-dark-50 text-hover-primary my-3 mr-2" id="kt_login_forgot" href='#' onClick={this.events.handleForgotPasswordLink}>{t('forgotPWD')}</a>;
                forgotPWDEmailForm=<div className="kt-form" style={{"display": (this.props.loginStore.visibleForgotForm ? 'block' : 'none')}}>
                    <div className="text-center mb-10 mb-lg-20">
                        <h3 className="font-size-h1" style={{color: this.LocalConfig.color_login}}>{t('forgotPWD')}</h3>
                        <div className="text-muted font-weight-bold">{t('enterEmail')}</div>
                    </div>
                    <div className="form-group fv-plugins-icon-container">
                        <input type="email" placeholder="E-mail" className="form-control" style={this.loginInputStyle} autoComplete="off" value={this.props.loginStore.login} onChange={this.events.handleLoginChange}/>
                    </div>
                    <div className="kt-login__actions">
                        <button type="button" disabled={isDisableForgPass}  className="btn btn-pill kt-login__btn-primary uppercase" style={{borderColor: this.LocalConfig.color_login, color: this.LocalConfig.color_login}} onClick={this.events.handleForgotPasswordSubmit} >{t('submit')}</button>
                        <button type="button"  className="btn btn-pill kt-login__btn-primary uppercase" style={{borderColor: this.LocalConfig.color_login, color: this.LocalConfig.color_login}} onClick={this.events.handleForgotPasswordCancel}>{t('cancel')}</button>
                    </div>
                </div>;
            }
            let reg_btn;
            if(this.LocalConfig.registration_group) {
                reg_btn = <a className="text-dark-50 text-hover-primary my-3 mr-2" id="kt_login_reg" href={'#/registration'} >{t('registration')}</a>;
            }
            reg_div = <div style={{display:"flex", justifyContent: "space-between"}}>
                {reg_btn}
                {forgotPWDEmail}
            </div>
        }


        let position;
        if(this.props.RootStore.config.isAppLogin) {
            position = <div style={{padding:"16px", position: "absolute", right: "80px"}}>
                <a href="#/index"><span>Проверка позиций</span></a>
            </div>;
        }

        return (<BlockUi id="zt_LoginPage" tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                    <div className="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
                        <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style={{"background": this.LocalConfig.background_login ,"backgroundSize": "cover"}}>
                            {position}
                            <div style={{ height: "20px","position":"absolute","right":'10px',"marginTop":"15px" }} >
                                <Lang/>
                            </div>
                            <div className='kt-grid__item kt-grid__item--fluid kt-login__wrapper' style={{marginBottom:0, paddingTop:"50px"}}>
                                <div className='kt-login__container'>
                                    <div className="kt-login__logo" style={{margin:0}}>
                                        <a href="/">
                                            <img style={{width: "100%", height: "calc(var(--vh, 1vh) * 100 - 400px)", objectFit: "contain"}} src={this.LocalConfig.img_logo_url} alt={this.LocalConfig.img_logo_alt}  />
                                        </a>
                                    </div>
                                    <div className="kt-login__signin">
                                        <div className="kt-login__head">
                                            <h3 className="kt-login__title" style={{color: this.LocalConfig.color_login}} >{t('welcome')} {document.title}</h3>
                                        </div>
                                        <div className="kt-form"  style={{margin:0,"display": (this.props.loginStore.visibleForgotForm ? 'none' : 'block')}} >
                                            <div className="input-group">
                                                <input style={this.loginInputStyle} className="form-control" type="text" placeholder={t('login')}  autoComplete="off" value={this.props.loginStore.login} onChange={this.events.handleLoginChange} onKeyDown={this.events.onKeyDownInput}/>
                                            </div>
                                            <div className="input-group">
                                                <input style={this.loginInputStyle} className="form-control" type="password" placeholder={t('password')}  value={this.props.loginStore.password} onChange={this.events.handlePasswordChange} onKeyDown={this.events.onKeyDownInput}/>
                                            </div>
                                            {reg_div}
                                            <div className="alert alert-danger login-error" style={{'visibility':(this.props.loginStore.error ? 'visible' : 'hidden'),'margin':0}}>
                                                <i className="fa-lg fa fa-warning" />{t('error')}
                                                <button type="button" className="close" data-dismiss="alert" onClick={this.events.handleDismiss}>
                                                    <span aria-hidden="true" style={{"position": "absolute","right": 0,"top": 0}} >×</span>
                                                </button>
                                            </div>
                                            <div className="kt-login__actions" style={{"margin": "0"}}>
                                                <button style={{borderColor: this.LocalConfig.color_login, color: this.LocalConfig.color_login}}  className="btn btn-pill kt-login__btn-primary uppercase" onClick={this.events.handleSubmit} disabled={this.props.loginStore.disableLogin}>{t('sign_in')}</button>
                                            </div>
                                        </div>
                                        {forgotPWDEmailForm}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </BlockUi>
        )
    }
}


export function LoginFormPage(props) {
    const LoginFormPageTranslation = withTranslation('login')(LoginFormPage1);

    return (
        <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
            <LoginFormPageTranslation {...props} i18n={myI18n} />
        </React.Suspense>
    );
}