import * as React from "react";
import { Modal, Button, Table } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import EnematLoader from '../../components/svg/EnematLoader'

import {inject, observer} from "mobx-react";

import {NumberInput} from './Components/NumberInput'
import {CustomRadio} from './Components/CustomRadio'
import {IOSSwitch} from './Components/IOSSwitch'

import * as moment from 'moment';

import * as ct_apiobj from "../../api/ObjTree-api";
import * as ct_apiview from "../../api/View-api";


const DateFormat='DD/MM/yyyy';


interface IAdditionalBlocksProps{
  UserStepStore?:any,
  ToastrStore?:any,
  data:any,
  onChange:(data)=>void,
}
interface IAdditionalBlocksState{
    blocking:boolean,
    data_child:any,
    data:any,
}

@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class AdditionalBlocks extends React.Component<IAdditionalBlocksProps, IAdditionalBlocksState> {

    constructor(props) {
        super(props);
        this.state={
            blocking:true,
            data_child:[],
            data:this.props.data,
        };
    }
    componentDidMount(){
      ct_apiobj.getAllChildObjValues(this.state.data.uid).then((res)=>{
        if(res.data.result===1){
          let data_child=res.data.data.filter((item)=>{
            let attr_deleted=item.attributes.find((attr)=>{
              return attr.uid=='17d1a58c-06af-4bcf-bc18-053f183e811e'?true:false;
            });
            let existSave= this.state.data.data.find((item_exist)=>{
              return item_exist.uid==item.uid?true:false;
            })
            return !attr_deleted.value || existSave?true:false;
          });
          this.setState({data_child:data_child, blocking:false})
        }
      });
    }
    componentDidUpdate(prevProps){
      if(this.props.data.uid!=prevProps.data.uid){
        this.setState({blocking:true, data_child:[], data:this.props.data}, ()=>{
          ct_apiobj.getAllChildObjValues(this.props.data.uid).then((res)=>{
            if(res.data.result===1){
              let data_child=res.data.data.filter((item)=>{
                let attr_deleted=item.attributes.find((attr)=>{
                  return attr.uid=='17d1a58c-06af-4bcf-bc18-053f183e811e'?true:false;
                });
                let existSave= this.state.data.data.find((item_exist)=>{
                  return item_exist.uid==item.uid?true:false;
                })
                return !attr_deleted.value || existSave?true:false;
              });
              this.setState({data_child:data_child, blocking:false})
            }
          });
        })
      }
    }
    checkBoxChange=(value, name)=>{
      let data=this.state.data;
      let exist_data=null;
      if(data.data){
        exist_data=data.data.map((item)=>{return item.uid})
      }
      if(exist_data && exist_data.indexOf(value)==-1){
        data.data.push({uid:value, name:name});
      }else{
        data.data=data.data.filter((item)=>{
          return item.uid==value?false:true;
        })
      }
      this.setState({data:data}, ()=>{
        this.props.onChange(this.state.data);
      });
    }
    radioBoxChange=(value, id)=>{
      let data=this.state.data;
      let exist_data=null;
      if(data.data){
        exist_data=data.data.map((item)=>{return item.uid})
      }
      let name='';
      let data_exist=this.state.data_child.find((item)=>{
        return item.uid==value?true:false;
      })
      if(data_exist){
        let attr_name= data_exist.attributes.find((attr_item)=>{
          return attr_item.uid=='9f661741-ad54-4a20-a62f-1b84eaae4076'?true:false;
        })
        if(attr_name.value){
          name=attr_name.value.value;
        }
      }
      if(data && exist_data.indexOf(value)!==-1){
        data.data=[];
      }else{
        data.data=[{uid:value, name:name}];
      }
      this.setState({data:data}, ()=>{
        this.props.onChange(this.state.data);
      });
    }
    inputTableCHange=(e, i, key)=>{
      let {value}= e.currentTarget;
      let cur_data= this.state.data;
      if(cur_data && cur_data[i]){
        cur_data[i][key]=value;
      }else{
        let new_row={};
        this.state.data_child.forEach((item)=>{
          new_row[item.uid]='';
          if(item.uid==key){
            new_row[item.uid]=value;
          }
        });
        cur_data.push(new_row);
      }
      this.setState({data:cur_data});
    }
    userDataInputChange=(e, attr_type, uid, name)=>{
      let {value}=e.currentTarget;
      let data=this.state.data;
      if(value){
        if(attr_type.value.uid=='af4ea083-cbf3-44bf-b5f0-2606a1344be7'){
          value=Number(value);
          e.currentTarget.value=value;
        }
        let obj_data=data.data.find((item)=>{
          return item.uid==uid?true:false;
        });
        if(obj_data){
          obj_data.userData=value;
        }else{
          data.data.push({uid:uid, name:name, userData:value})
        }
      }else{
        data.data=data.data.filter((item)=>{
          return item.uid==uid?false:true;
        })
      }
      this.setState({data:data});
    }
    removeTableRow=(i)=>{
      let cur_data= this.state.data.filter((item, index)=>{
        return index==i?false:true;
      });
      this.setState({data:cur_data});
    }

    renderBody=(data, data_child)=>{
      let body=null;
      if(data.type.name=='Checkbox'){
        let exist_data=null;
        if(this.state.data.data){
          exist_data=this.state.data.data.map((item)=>{return item.uid})
        }
         body=data_child.map((item)=>{
          let attr_type=item.attributes.find((attr)=>{
             return attr.uid=='104995b9-b651-447f-b483-682a914ec435' && attr.value?true:false;
          })
          let attr_name= item.attributes.find((attr_item)=>{
            return attr_item.uid=='9f661741-ad54-4a20-a62f-1b84eaae4076'?true:false;
          })
          if(attr_type){
            return this.renderUserValueInput(attr_type, item);
          }
          return (<div key={'addit_checkBox_'+item.uid} className="addit_checkbox_item">
                <span style={{display:'inline-block'}} onClick={()=>{this.checkBoxChange(item.uid, (attr_name.value?attr_name.value.value:''))}}>
                  <IOSSwitch checked={(exist_data && exist_data.indexOf(item.uid)!==-1?true:false)}/>
                  <label>{(attr_name.value?attr_name.value.value:'')}</label>
                </span>
              </div>);
        })

      }else if(data.type.name=='Radio'){
        let exist_data=null;
        if(this.state.data.data){
          exist_data=this.state.data.data.map((item)=>{return item.uid})
        }
        let data=data_child.filter((item)=>{
          let attr_type=item.attributes.find((attr)=>{
             return attr.uid=='104995b9-b651-447f-b483-682a914ec435' && attr.value?true:false;
          })
          return !attr_type?true:false;
        }).map((item)=>{
         let attr_name= item.attributes.find((attr_item)=>{
           return attr_item.uid=='9f661741-ad54-4a20-a62f-1b84eaae4076'?true:false;
         })
         return {value:item.uid, name:attr_name.value?attr_name.value.value:''};
        })
        let iputData=data_child.filter((item)=>{
          let attr_type=item.attributes.find((attr)=>{
             return attr.uid=='104995b9-b651-447f-b483-682a914ec435' && attr.value?true:false;
          })
          return attr_type?true:false;
        }).map((item)=>{
          let attr_type=item.attributes.find((attr)=>{
             return attr.uid=='104995b9-b651-447f-b483-682a914ec435' && attr.value?true:false;
          })
         let attr_name= item.attributes.find((attr_item)=>{
           return attr_item.uid=='9f661741-ad54-4a20-a62f-1b84eaae4076'?true:false;
         })
         return this.renderUserValueInput(attr_type, item);
       })
        body=<React.Fragment><CustomRadio
          id={'radio'}
          value={(exist_data?exist_data:[])}
          data={data}
          onChange={this.radioBoxChange}
          display={'block'}
          type={'radio'}
        />
        {iputData}
        </React.Fragment>


      }
      /*else if(data.type.name=='Table'){
        let cur_data= this.state.data;

        let empty_row={};
        let exist_keys=[];
        data_child.forEach((item)=>{
          empty_row[item.uid]='';
        });

        let fill_data=[];
        cur_data.forEach((item)=>{
          let keys= Object.keys(item);
          let row=Object.assign({}, empty_row);
          keys.forEach((key)=>{
            if(row[key]!=undefined){
              row[key]=item[key];
            }
          })
          fill_data.push(row)
        });

        let isEmptyLast=true;
        if(fill_data.length){
          let last_row=fill_data[fill_data.length-1];
          let keys_fill= Object.keys(last_row);
          keys_fill.forEach((key)=>{
            if(last_row[key]){
              isEmptyLast=false;
            }
          })
        }

        if(!fill_data.length || !isEmptyLast){
          fill_data.push(empty_row);
        }

        let head_table=data_child.map((item)=>{
            return <th key={'head_table_'+item.uid}>{item.name}</th>
        });
        head_table.push(<th key={'delete_th'}></th>);

        let body_table=fill_data.map((row, i)=>{
          let row_keys=Object.keys(row);
          let rows=row_keys.map((key)=>{
            return <td key={'body_table_td'+i+'_'+key}><input className="form-control" onChange={(e)=>{this.inputTableCHange(e, i, key)}} value={row[key]}/></td>
          })
          if(fill_data.length-1==i){
            rows.push(<td key={'delete_td_'+i}></td>);
          }else{
            rows.push(<td key={'delete_td_'+i}><button onClick={()=>{this.removeTableRow(i)}} type="button" className="btn btn-danger btn-sm"><i className="fas fa-trash"></i></button></td>);
          }
          return <tr key={'body_table_tr'+i}>{rows}</tr>
        })

        body=<table className="table table-bordered additional_table">
          <thead>
            <tr>
              {head_table}
            </tr>
          </thead>
          <tbody>
            {body_table}
          </tbody>
        </table>

      }*/
      return(<div>
          <div className="title title_step">{data.name}</div>
          <div className="addit_content">{body}</div>
        </div>);
    }
    renderUserValueInput=(attr_type, item)=>{
      let value='';
      if(this.state.data.data){
        let sel_value=this.state.data.data.find((item_sel)=>{
          return item_sel.uid==item.uid?true:false;
        });
        if(sel_value && sel_value.userData){
          value=sel_value.userData;
        }
      }
      let attr_name= item.attributes.find((attr_item)=>{
        return attr_item.uid=='9f661741-ad54-4a20-a62f-1b84eaae4076'?true:false;
      })
      let ctrl=<input type="text" value={value} onChange={(e)=>{this.userDataInputChange(e, attr_type, item.uid, (attr_name.value?attr_name.value.value:''))}}  className="form-control"/>
      if(attr_type.value.uid=='af4ea083-cbf3-44bf-b5f0-2606a1344be7'){
        ctrl=<NumberInput onChange={(e)=>{this.userDataInputChange(e, attr_type, item.uid, (attr_name.value?attr_name.value.value:''))}} value={value} AdvProps={{className:"form-control"}}/>
      }
      return (<div key={'input_'+item.uid} className="form-group group_input addit_input_value">
              {ctrl}
              <label><span className="text">{(attr_name.value?attr_name.value.value:'')}</span><span className="label_ph"></span></label>
          </div>);
    }

    render() {
        return (
              <BlockUi className="addit_block_content" tag="div" loader={<EnematLoader />} blocking={this.state.blocking}>
                {this.renderBody(this.state.data, this.state.data_child)}
              </BlockUi>
        );
    }
}
