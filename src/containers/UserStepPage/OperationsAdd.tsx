import * as React from "react";
import { Modal, Button, Table } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import EnematLoader from '../../components/svg/EnematLoader'

import {inject, observer} from "mobx-react";
import {Icon} from './Components/Icon'
import * as moment from 'moment'

import * as ct_apiobj from "../../api/ObjTree-api";
import * as ct_apiview from "../../api/View-api";
import * as ct_apistep from "../../api/StepForm-api";

import {CustomSelect} from './Components/CustomSelect'
import {AdditionalBlocks} from './AdditionalBlocks'
import {NumberInput} from './Components/NumberInput'
import getErrorMsg from './Components/ErrorHandler'


const DateFormat='DD/MM/yyyy';

const rounded = function(number){
    return +number.toFixed(2);
}
const numberWithSpaces=(x)=> {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ").replaceAll(/\./gi, ',');
}


interface IOperationsAddProps{
  UserStepStore?:any,
  ToastrStore?:any,
  isShow:boolean,
}
interface IOperationsAddState{
  dataTree:any,
  operation:{
    name:string,
    uid:string,
    rge:string,
  },
  climatic:any,
  data_addit_blocks:any,
  stepper:{
    data:{name:string, index?:number, isolant_uid?:string, step_surface?:boolean}[],
    step:number,
  }
  operation_obj:any,
  showError:boolean,
  isolants_data:any,
  blocking:boolean,
  validateError:any,
}


@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class OperationsAdd extends React.Component<IOperationsAddProps, IOperationsAddState> {

    ajax={
      rootObj:{
        parent:'00000000-0000-0000-0000-000000000000',
        type:'46ca906e-cfae-4a63-bfbf-a22b4ca4c37e',
        title:'Operation',
      },
      viewRootObj:{
        uid:'bf3d1123-6769-42a5-804a-d50c780cb28f',
        attr_sort:'f5d60a0a-bb00-4bc2-a7db-f1cec9c4e7f4',
        attr_name:'f5d60a0a-bb00-4bc2-a7db-f1cec9c4e7f4',
        attr_text:'296185ab-746d-47fa-bf74-cf9b881172cc',
        attr_forb_org:'71eae1cf-dbec-4eb0-80af-3e232c82f1bf',
        attr_adv_opt_id:'d0045cb0-5576-4edc-a13d-9a08d5b57831',
        attr_min_price:'',
        attr_rge:'b21d5d9e-56cd-4a67-847b-18e52651daa1',
      },
      dateRange:{
        uid:'89a9922b-5c91-42bb-b8bf-19f14b0bb20f',
        uidStartDate:'06e55dbd-1574-4aa5-9b8e-f062b73aad62',
        uidFinishDate:'23361d2b-5c69-432e-a7f5-34efa1168d4b',
        uidvalueClassic:'0248b2c6-28e9-43f9-bb35-d5f613026218',
        uidvaluePrecair:'d27572a9-a93f-4ecf-94d5-85630ca8fb8d',
        uidvalueGrandPrecair:'c2003968-73be-45d1-8de4-00a9ad05910b',
      },
      climatic:{
        uid:'45f312e2-3666-475e-81c7-676ae70f9462',
        class_uid:'917d4fb6-708a-4812-96c9-fb12df135632',
        attr_code:'d8e960f7-de55-454a-a6aa-73c49edbc2a3',
        attr_zone:'91b0d12b-fb6b-460d-b7d2-81cf48663ce9',
      },
      sector_activity:{
        class_uid:'3a4c07c8-b1ba-4a36-b7bb-a51c01c82423',
      },
      aditionalBlock:{
        type:'bc85f46f-7fdd-496e-bfe0-1ac578cb66c1',
        type_attr:'5736390f-272e-46ff-a186-89e474a4d910',
        deleted_attr:'b741480b-4ed7-48a2-a7bd-54d5efdf25cc',
      },
      ignoreClasses:[
        'bc85f46f-7fdd-496e-bfe0-1ac578cb66c1'
      ],
      digitalClass:{
        uid:'79f34693-9ad6-429b-8584-b5b047991183',
        uid_name:'c4853fa0-ad9f-493e-8d93-90d04df90420',
      },
    }

    constructor(props) {
        super(props);
        this.state={
            dataTree:[],
            operation:{
              name:'',
              uid:'',
              rge:null,
            },
            climatic:[],
            data_addit_blocks:null,
            stepper:{
              data:[{name:'operation'}, {name:'isolants', step_surface:false, isolant_uid:null}, {name:'prix_unitare'}],
              step:0,
            },
            operation_obj:null,
            showError:false,
            isolants_data:{
              isolant_attr_sett:null,
              data:[],
            },
            blocking:false,
            validateError:[],
        };
    }


    onShow=()=>{
        let content=document.getElementsByClassName('userstep_form')[0];
        content.scrollTo({top:content.scrollHeight, behavior:'smooth'});
        this.setState({dataTree:[], validateError:[], data_addit_blocks:null, showError:false, stepper:{data:[{name:'operation'}, {name:'isolants', step_surface:false, isolant_uid:null}, {name:'prix_unitare'}], step:0}}, ()=>{
          let chosen_org=this.props.UserStepStore.dataClaculator.organization;

            Promise.all([
              ct_apiview.getTableData({
                view: this.ajax.viewRootObj.uid,
                attr_sort: '',
                sort: 1,
                start: 1,
                num: 100000,
            }),

            ct_apiview.getTableData({
                view: this.ajax.climatic.uid,
                attr_sort: '',
                sort: 1,
                start: 1,
                num: 100000,
            }),
          ]).then((res)=>{
            let Data=[], climatic=[];

            if(res[0].data.result===1){
              let data_oper=res[0].data.data.map((item)=>{
                let name= item[this.ajax.viewRootObj.attr_name]?item[this.ajax.viewRootObj.attr_name].v:'';
                let text= item[this.ajax.viewRootObj.attr_text]?item[this.ajax.viewRootObj.attr_text].v:'';
                return {
                  name:name,
                  label:name+' '+text,
                  uid:item[this.ajax.viewRootObj.attr_name]?item[this.ajax.viewRootObj.attr_name].o:'',
                  forb_org:item[this.ajax.viewRootObj.attr_forb_org]?item[this.ajax.viewRootObj.attr_forb_org].ln:'',
                  forb_org_uid:item[this.ajax.viewRootObj.attr_forb_org]?item[this.ajax.viewRootObj.attr_forb_org].v:'',
                  oper_options_split:item[this.ajax.viewRootObj.attr_adv_opt_id] && item[this.ajax.viewRootObj.attr_adv_opt_id].ln?item[this.ajax.viewRootObj.attr_adv_opt_id].ln.split(','):[],
                  oper_options:item[this.ajax.viewRootObj.attr_adv_opt_id] && item[this.ajax.viewRootObj.attr_adv_opt_id].ln?item[this.ajax.viewRootObj.attr_adv_opt_id].ln:'',
                  min_price:item[this.ajax.viewRootObj.attr_min_price]?item[this.ajax.viewRootObj.attr_min_price].v:0,
                  rge:item[this.ajax.viewRootObj.attr_rge] && item[this.ajax.viewRootObj.attr_rge].v?item[this.ajax.viewRootObj.attr_rge].v:null,
                }
              }).filter((item)=>{
                if(!item.forb_org_uid || item.forb_org_uid!==chosen_org.uid){
                  return true;
                }
                return false;
              });
             Data=[
                {
                  title: this.ajax.rootObj.title,
                  obj:data_oper,
                  classuid:this.ajax.rootObj.type,
                  children:[],
                }
              ];

            }
            if(res[1].data.result===1){
              climatic=res[1].data.data.map((item)=>{
                return {
                  code:item[this.ajax.climatic.attr_code]?item[this.ajax.climatic.attr_code].v:'',
                  zone:item[this.ajax.climatic.attr_zone]?item[this.ajax.climatic.attr_zone].o:'',
                  zone_name:item[this.ajax.climatic.attr_zone]?item[this.ajax.climatic.attr_zone].v:'',
                }
              });
            }

            this.setState({dataTree:Data, climatic:climatic});

          });
        });
    };
    componentDidUpdate(prevProps){
        if(this.props.isShow && !prevProps.isShow){
          this.onShow();
        }
    }

    handleClose = () => {
       this.props.UserStepStore.setOperationModal(false);
    };
    getChainObj_old=(Data)=>{
      let objUid=[];
      Data.forEach((item)=>{
        let ObjData:any={
          uid:item.uid,
        };
        if(item.children && item.children.length){
          ObjData.children=this.getChainObj(item.children);
        }
        objUid.push(ObjData);
      });
      return objUid;
    };
    getChainObj=(Data)=>{
      let objUid=[];
      Data.forEach((item)=>{
        var chosen_opt=item.obj.find((opt)=>{
          return opt.uid===item.uid?true:false;
        })
        objUid.push({
          label:item.title,
          name:chosen_opt.name,
          uid:item.uid,
        })
        if(item.children && item.children.length){
          var childre_obj=this.getChainObj(item.children);
          objUid=[...objUid, ...childre_obj];
        }
      });
      return objUid;
    };


    loadIsolant_data=(operation)=>{
        let view_uid='5eadb3b0-7c44-4403-807a-45a845de70ea';
        let param_scenario_uid='6b0a469c-64d7-410e-a045-e2fd2afd96f6';
        let param_name_uid='95cf9bce-3782-4b4c-bb4d-bb98a3708e58';
        let param_marque_uid='202fd14c-ea8e-4043-aa5c-a616641b7bae';

        const get_isolant_obj= (item, param_isolant_data)=>{


          let marq=(item[param_marque_uid] && item[param_marque_uid].ln?item[param_marque_uid].ln:'');
          let name=(item[param_name_uid] && item[param_name_uid].v?item[param_name_uid].v:'');

          let adv_params={};
          param_isolant_data.forEach((param)=>{
            if(param.name.indexOf('Attribute')!==-1){
              let uid= param.uid;
              adv_params[param.name]={title:param.name, value:(item[uid] && item[uid].v?item[uid].v:''), type:param.datatype};
            }
          })

          let label=marq+' '+name;
          let uid=item[param_name_uid] && item[param_name_uid].o?item[param_name_uid].o:'';

          return {
            name:label,
            uid:uid,
            ad_size:[],
            advParams:{
              'Marque':{title:'Marque', value:marq},
              'Reference':{title:'Reference', value:name},
              ...adv_params
            },
          };
        };

        let filter_data= {
          attr:'6b0a469c-64d7-410e-a045-e2fd2afd96f6',
          op:'ANY',
          v:[operation.operation.name],
        };

        return Promise.all([
          ct_apiview.getHeadData({
              uid: '5eadb3b0-7c44-4403-807a-45a845de70ea',
          }),
          ct_apiview.getTableData({
              view: view_uid,
              attr_sort: '',
              sort: 1,
              start: 1,
              num: 100000,
              attr_filter:[filter_data],
          })
        ]).then((res_arr)=>{
          let isolant_attr_sett=null;
          if(res_arr[0].data.result==1){
            isolant_attr_sett=res_arr[0].data.data
          }
          let fullIsolantOpt=[];
          if(res_arr[1].data.result===1){
            res_arr[1].data.data.forEach((item)=>{

              var isolant=get_isolant_obj(item, isolant_attr_sett);

              fullIsolantOpt.push(isolant);
            });
          }
          return {isolant_attr_sett:isolant_attr_sett, data:fullIsolantOpt}
        })
      }

    checkRGE=()=>{
      console.log(this.state.operation);
      this.setState({blocking:true});
      return new Promise((resolve, reject)=>{
        if(this.state.operation && this.state.operation.rge){
            let Data= this.props.UserStepStore.dataClaculator;

            let date_de_signature=Data.date.date_de_signature;
            let siret = Data.sous_traitance.siret;

            ct_apistep.getQualification(siret, date_de_signature).then((res)=>{
              if(res.data.result==1){
                  if(res.data.data.qualifications.length && res.data.data.qualifications.indexOf(this.state.operation.rge)!==-1){
                    resolve(true);
                  }else{
                    resolve(false);
                  }
              }else{
                resolve(false);
              }

            })
        }else{
          resolve(true);
        }
      })
    }

    createOperation=()=>{
      let Data= this.props.UserStepStore.dataClaculator;
      let persone= Data.persone=='Personne morale'?0:1;
      let operation=null;
      let objUid= this.getChainObj(this.state.dataTree);

      let data_calc={
        form:Data,
        oper_data:objUid,
      };

      return Promise.all([
        ct_apiobj.getAllChildObjValues(this.state.operation.uid),
        ct_apiobj.stepFormGetCoefUser(Data.organization.uid, this.state.operation.uid, persone, Data.date.date_de_signature),
        ct_apiobj.getAllValues(this.state.operation.uid),
        ct_apistep.getOperCalcData('c960cdcb-59d7-4494-8c8a-1e60a7910068', data_calc)

      ]).then((res_arr)=>{
        let additional_blocks=[];
        let res=res_arr[0];
        let coef=5.5;
        let res_coef=res_arr[1];
        if(res_coef.data.result==1){
            coef=Number(res_coef.data.data.coef);
        }
      /*  if(res.data.result===1){
          res.data.data.forEach((item)=>{
            let attr_type= item.attributes.find((attr_item)=>{
              return attr_item.uid==this.ajax.aditionalBlock.type_attr?true:false;
            })
            let attr_deleted=item.attributes.find((attr_item)=>{
              return attr_item.uid==this.ajax.aditionalBlock.deleted_attr?true:false;
            })
            let attr_name= item.attributes.find((attr_item)=>{
              return attr_item.uid=='0fd7cb64-29cf-4232-85b1-e81d78682a42'?true:false;
            })
            if(item.type.uid==this.ajax.aditionalBlock.type && attr_type && attr_type.value && attr_deleted && !attr_deleted.value){
              additional_blocks.push({
                uid:item.uid,
                name:attr_name.value?attr_name.value.value:'',
                data:[],
                type:{
                  uid:attr_type.value.uid,
                  name:attr_type.value.name,
                },
              });
            }
          })

        }*/
        var newDataOperation=this.props.UserStepStore.dataOperation;

        let resultValue=this.getResultValue();
        let rangeValue= this.getRangeValue();

        let adv_attr_name=[], params=[];
        let def_quant=0, day_between_date=0;
        let options_adv_size=[];
        if(res_arr[2].data.result==1){
          params= res_arr[2].data.data[this.state.operation.uid];
          adv_attr_name=params.filter((item)=>{
            return item.name.indexOf('Attribute')!==-1 && item.value && item.value.value?true:false;
          }).map((item)=>{
            return {name:item.name, value:item.value.value};
          })

          let def_quant_param= params.find((item)=>{
            return item.uid=='a176581e-e030-42ca-9f56-a9f207703b03'?true:false;
          })
          if(def_quant_param && def_quant_param.value && def_quant_param.value.value){
            def_quant= def_quant_param.value.value;
          }

          //колв-во дней между датам
          let date_param=params.find((item)=>{
            return item.uid=='db32bbd2-d1ec-441f-961e-77b8f8b9e72e'?true:false;
          })
          if(date_param && date_param.value){
            day_between_date= date_param.value.value;
          }

          //данные об отображаемых полях
          let option_param= params.find((item)=>{
            return item.uid=='d0045cb0-5576-4edc-a13d-9a08d5b57831'?true:false;
          })
          if(option_param && option_param.value){
            options_adv_size=option_param.value.map((opt_item)=>{
              return {uid:opt_item.uid, name:opt_item.name}
            })
          }
        }


        let min_prix=null;
        if((Data.persone=='Personne physique' && Data.beneficiaire.TableAflag==1) || (Data.persone=='Personne morale' && Data.information_lieu_travaux.operationavec!='Operation classique')){
          if(rangeValue && rangeValue.rangeValue && rangeValue.rangeValue.ValueGP){
            resultValue.resultValue=rangeValue.rangeValue.ValueGP;
            resultValue.objValue=rangeValue.objValue;
          }else{
              resultValue.resultValue=resultValue.resultValue*2;
          }
          let min_price_GP_attr= params.find((item)=>{
            return item.uid=='74e4af68-d6f4-4abd-b2ff-fd51f316f4f3'?true:false;
          });
          if(min_price_GP_attr && min_price_GP_attr.value && min_price_GP_attr.value.value){
            min_prix=Number(min_price_GP_attr.value.value);
          }
        }else if(Data.persone=='Personne physique' && Data.beneficiaire.TableBflag==1){
          if(rangeValue && rangeValue.rangeValue  && rangeValue.rangeValue.ValueP){
            resultValue.resultValue=rangeValue.rangeValue.ValueP;
            resultValue.objValue=rangeValue.objValue;
          }
          let min_price_P_attr= params.find((item)=>{
            return item.uid=='5898dc36-7152-4153-ae8a-c1a1ee2e49ed'?true:false;
          });
          if(min_price_P_attr && min_price_P_attr.value && min_price_P_attr.value.value){
            min_prix=Number(min_price_P_attr.value.value);
          }
        }else{
          if(rangeValue && rangeValue.rangeValue && rangeValue.rangeValue.ValueC){
            resultValue.resultValue=rangeValue.rangeValue.ValueC;
            resultValue.objValue=rangeValue.objValue;
          }
          let min_price_attr= params.find((item)=>{
            return item.uid=='3dc7b442-0228-4ddc-868a-f4220e6b47b7'?true:false;
          });
          if(min_price_attr && min_price_attr.value && min_price_attr.value.value){
            min_prix=Number(min_price_attr.value.value);
          }
        }

        let prix_unitaire=0;
        let max_prix=0;
        if(res_arr[3].data.result==1){
          //calculated data Operation
          max_prix=res_arr[3].data.data.value;
          prix_unitaire=max_prix;
        }
        min_prix=!min_prix?(prix_unitaire/2):min_prix;

        operation={
            id:Math.round(Math.random()*1000000000),
            value:Number(resultValue.resultValue),
            objValue:resultValue.objValue,
            operation:this.state.operation,
            objUid:objUid,
            surface:0,
            isolant:[],
            bureaudecontrole:'',
            referencerapport:'',
            cost:0,
            cost_client:0,
            coef:coef,
            prix_unitaire:prix_unitaire,
            prix_material:rounded(prix_unitaire*0.4),
            prix_work:rounded(prix_unitaire*0.6),
            cost_pro:0,
            reste_a_charge:0,
            additional_blocks:[],
            adv_attr_name:adv_attr_name,
            min_prix:min_prix,
            max_prix:max_prix,
            cumac:0,
            def_quant:def_quant,
            day_between_date:day_between_date,
            options_adv_size:options_adv_size,
        };
        return this.loadIsolant_data(operation)
      }).then((isolants)=>{

        return {operation:operation, isolant:isolants};
      })
    };
    handleSave=()=>{
      let newDataOperation=this.props.UserStepStore.dataOperation;
      let add_operation_data=this.state.operation_obj;

      add_operation_data.additional_blocks=this.state.data_addit_blocks?this.state.data_addit_blocks:[];
      this.updatePriceData(add_operation_data)

      newDataOperation.push(add_operation_data);
      this.props.UserStepStore.updateDataOperation(newDataOperation);
      this.handleClose();
      //this.validData(true, true, null);
    }



    clickNexStep=()=>{
      let stepper=this.state.stepper;
      let new_step=stepper.step+1;
      if(stepper.step==0 && !this.checkOperationData()){
        this.setState({showError:true});
        return false;
      }
      //validate
      if(stepper && stepper.data[new_step]){
        let valid_cur_adv_par=false;
        if(stepper && stepper.data[stepper.step] && stepper.data[stepper.step].name=='isolants' && !stepper.data[stepper.step].step_surface){
          new_step=stepper.step;
          valid_cur_adv_par=true;
        }
        if(!this.validStep(stepper, new_step, valid_cur_adv_par)){
          return false;
        };
      }

      if(stepper && stepper.data[stepper.step] && stepper.data[stepper.step].name=='isolants' && !stepper.data[stepper.step].isolant_uid){
        this.setState({showError:true});
        return false;
      }
      if(stepper && stepper.data[stepper.step] && stepper.data[stepper.step].name=='isolants'){
        if(!stepper.data[stepper.step].step_surface){
          stepper.data[stepper.step].step_surface=true;
          this.setState({stepper:stepper,  showError:false});
          return false;
        }else{
          stepper.data[stepper.step].step_surface=false;
        }
      }
      if(stepper && stepper.data[stepper.step] && stepper.data[stepper.step].name=='operation'){
        this.checkRGE().then((result)=>{
          if(result){
            this.createOperation().then((data)=>{
              stepper.step=new_step;
              this.setState({stepper:stepper, operation_obj:data.operation, isolants_data:data.isolant, showError:false, blocking:false});
            })
          }else{
            this.props.ToastrStore.error('RGE incorrect!', 'Error!');
            this.setState({showError:false,  blocking:false});
          }
        })
      }else if(stepper && stepper.data[new_step]){
        stepper.step=new_step;
        this.setState({stepper:stepper,  showError:false});
      }else{
        this.handleSave();
      }
    }
    clickOnStep=(new_step)=>{
      let stepper=this.state.stepper;
      if(stepper.step==0 && !this.checkOperationData()){
        this.setState({showError:true});
        return false;
      }
      stepper.data.forEach((step_item)=>{
        if(step_item.step_surface){
          step_item.step_surface=false;
        }
      })

      //validate
      if(stepper && stepper.data[new_step]){
        if(!this.validStep(stepper, new_step, false)){
          return false;
        };
      }

      if(stepper && stepper.data[stepper.step] && stepper.data[stepper.step].name=='isolants' && new_step>stepper.step && !stepper.data[stepper.step].isolant_uid){
        this.setState({showError:true});
        return false;
      }
      if(stepper && stepper.data[stepper.step] && stepper.data[stepper.step].name=='operation'){
        this.checkRGE().then((result)=>{
          if(result){
            this.createOperation().then((data)=>{
              stepper.step=new_step;
              this.setState({stepper:stepper, operation_obj:data.operation, isolants_data:data.isolant,  showError:false,  blocking:false});
            })
          }else{
            this.props.ToastrStore.error('RGE incorrect!', 'Error!');
            this.setState({showError:false,  blocking:false});
          }
        })

      }else if(stepper && stepper.data[new_step]){
        stepper.step=new_step;
        this.setState({stepper:stepper,  showError:false});
      }else{
        this.handleSave();
      }
    }
    checkOperationData=()=>{
      let tree_data=this.state.dataTree;
      let isChecked=true;
      let checkRecusive=(Data)=>{
        Data.forEach((item)=>{
            if(!item.uid || item.uid==-1){
              isChecked=false;
            }
            if(item.children && item.children.length){
              checkRecusive(item.children)
            }
        })
      }
      checkRecusive(tree_data);
      return isChecked;
    }

    addIsolant=()=>{
      let stepper=this.state.stepper;
      stepper.data=[...stepper.data.slice(0, stepper.data.length-1), {name:'isolants', step_surface:false, isolant_uid:null}, {name:'prix_unitare'}];
      if(stepper && stepper.data[stepper.step] && stepper.data[stepper.step].name=='isolants'){
        stepper.data[stepper.step].step_surface=false;
      }
      this.clickOnStep(stepper.step+1)
    }
    removeIsolant=()=>{
      let stepper=this.state.stepper;
      let step=this.state.stepper.step;
      let oper_data=this.state.operation_obj;
      let new_error=[];
      if(stepper.data[step]){
        let removeId_isolant=stepper.data[step].isolant_uid;
        new_error=this.removeErrorValidate(this.state.validateError, [removeId_isolant]);
        stepper.data=stepper.data.filter((item, i)=>{
          return i==step?false:true;
        })
        oper_data.isolant=oper_data.isolant.filter((item)=>{
          return item.id==removeId_isolant?false:true;
        })

        let sumSurface=0;
        oper_data.isolant.forEach((item_isolant)=>{
            sumSurface+=Number(item_isolant.surface);
        });
        oper_data.surface=sumSurface;
      }
      if(stepper.data[step].name!=='isolants'){
        stepper.step=stepper.step-1;
      }
      this.setState({operation_obj:oper_data, stepper:stepper, validateError:new_error}, ()=>{
        this.calcMaxPrix();
      });

    }

    setRecursiveDataTree=(classuid, Data, uid, isAuto)=>{
      let dataTree=this.state.dataTree;
      let loopRec=function(DataTree){
        DataTree.forEach((item)=>{
          if(item.classuid===classuid){
            item.children=Data;
            item.uid=uid;
            item.isAuto=isAuto;
            if(item.value){
              delete item.value;
            }
            return;
          }
          if(item.children.length){
            loopRec(item.children);
          }
        })
      };
      loopRec(dataTree);
      console.log(dataTree);
      this.setState({dataTree:dataTree}, ()=>{
        for (let index = 0; index < Data.length; index++) {

          if(Data[index] && Data[index].classuid===this.ajax.dateRange.uid){
             this.getDateRangeData(Data, index, classuid, uid);
             //return null;
           }
          if(Data[index] && Data[index].classuid===this.ajax.climatic.class_uid){
             this.getDateClimaticData(Data, index, classuid, uid);
            //  return null;
          }
          if(Data[index] && Data[index].classuid===this.ajax.sector_activity.class_uid){
            this.getDateSectorData(Data, index, classuid, uid);
          }
        }
      });
    }
    setDataTree=(res_data, classuid, value, isAuto)=>{
      let DataObj={}, Data=[];
      res_data.forEach((item)=>{
        if(DataObj[item.uuidC]){
          DataObj[item.uuidC].obj.push({
            uid:item.uidO,
            name:item.nameO,
          });
        }else{
          DataObj[item.uuidC]={
              title: item.nameC,
              obj:[{
                uid:item.uidO,
                name:item.nameO,
              }],
              classuid:item.uuidC,
              children:[],
            }
        }
      });
      var keys=Object.keys(DataObj);
      Data=keys.map((key)=>{
        return DataObj[key];
      });
      this.setRecursiveDataTree(classuid, Data, value, isAuto);
      if(classuid==this.ajax.dateRange.uid){
        this.setRangeValueOper(value);
      }
      if(res_data.length===0){
        this.setValueOper(value);
      }

      return Data;
    }
    selectChange=(value, classuid)=>{
      if(value && value!=='-1'){
        ct_apiobj.getChildAllObj(value, classuid).then((res)=>{
            let data_res=res.data.data.filter((item)=>{
              return this.ajax.ignoreClasses.indexOf(item.uuidC)==-1 && item.nameC.indexOf('(ext)')==-1?true:false;
            })
            if(res.data.result===1){
              this.setDataTree(data_res, classuid, value, false);
            }
        })
      }else{
        this.setRecursiveDataTree(classuid, [], -1, false);
      }
      if(classuid===this.ajax.rootObj.type){
        var operation=this.getObjRec(classuid, value);
        if(value && value!=='-1'){
          ct_apiobj.getFullChildObjValues([value]).then((res)=>{
            if(res.data.result==1){
              let data_oper=res.data.data[value];
              let data_addit_blocks=data_oper.filter((item)=>{
                return item.type.uid=='bc85f46f-7fdd-496e-bfe0-1ac578cb66c1'?true:false;
              });
              let addit_block_step=data_addit_blocks.map((item, i)=>{return {name:'adit_block', index:i}});
              let stepper={
                step:0,
                data:[{name:'operation'}, ...addit_block_step, {name:'isolants', step_surface:false, isolant_uid:null}, {name:'prix_unitare'}],
              }
              this.setState({data_addit_blocks:this.convertAdditBlockData(data_addit_blocks), stepper:stepper})
            }
          });
        }else{
          this.setState({data_addit_blocks:null, stepper:{data:[{name:'operation'}, {name:'isolants', step_surface:false, isolant_uid:null}, {name:'prix_unitare'}],step:0,}});
        }
        console.log(operation);
        this.setState({operation:operation});
      }
    }
    inputChange=(e, uid, classuid)=>{
      let {value} = e.currentTarget;
      let dataTree=this.state.dataTree;

      let loopRec=function(DataTree){
        DataTree.forEach((item)=>{
          if(item.classuid===classuid){
            if(value){
              item.uid=uid;
              item.value=value;
            }else{
              item.uid=null;
              delete item.value;
            }
          }
          if(item.children.length){
            loopRec(item.children);
          }
        })
      };
      loopRec(dataTree);

      this.setState({dataTree:dataTree});

    }
    getObjRec=(classuid, value)=>{
      let loopRec=function(DataTree){
        let ObjData=null;
        for (let index = 0; index < DataTree.length; index++) {
          if(DataTree[index].classuid==classuid){
            ObjData=DataTree[index].obj.find((item)=>{
                return item.uid==value?true:false;
            });
            return ObjData;
          }
          if(DataTree[index].children.length){
            ObjData=loopRec(DataTree[index].children);
          }
          if(ObjData){
            return ObjData;
          }
        }
        return ObjData;
      }
      return loopRec(this.state.dataTree);
    }
    getDateRangeData=(DataRoot, index, rootClass, rootValue)=>{
    //  let value=DataRoot[0].obj[0].uid;
      let classuid=DataRoot[index].classuid;
      let obj =DataRoot[index].obj;

      var callback=(value, classuid)=>{
        if(value && value!='-1'){
          ct_apiobj.getChildAllObj(value, classuid).then((res)=>{
              let data_res=res.data.data.filter((item)=>{
                return this.ajax.ignoreClasses.indexOf(item.uuidC)==-1 && item.nameC.indexOf('(ext)')==-1?true:false;
              })
              if(res.data.result===1){
                this.setDataTree(data_res, classuid, value, true);
              }
          })
        }else{
          this.setRecursiveDataTree(rootClass, DataRoot, rootValue, false);
        }
      }
      this.getDateRangeObj(obj, classuid, callback)

    }
    getDateClimaticData=(DataRoot, index, rootClass, rootValue)=>{
    //  let value=DataRoot[0].obj[0].uid;
      let classuid=DataRoot[index].classuid;
      let obj =DataRoot[index].obj;

      var callback=(value, classuid)=>{
        if(value && value!='-1'){
          ct_apiobj.getChildAllObj(value, classuid).then((res)=>{
              let data_res=res.data.data.filter((item)=>{
                return this.ajax.ignoreClasses.indexOf(item.uuidC)==-1 && item.nameC.indexOf('(ext)')==-1?true:false;
              })
              if(res.data.result===1){
                this.setDataTree(data_res, classuid, value, true);
              }
          })
        }else{
          this.setRecursiveDataTree(classuid, [], value, false);
        }
      }
      this.getDateClimaticObj(obj, classuid, callback)

    }
    getDateSectorData=(DataRoot, index, rootClass, rootValue)=>{
    //  let value=DataRoot[0].obj[0].uid;
      let classuid=DataRoot[index].classuid;
      let obj =DataRoot[index].obj;

      var callback=(value, classuid)=>{
        if(value && value!='-1'){
          ct_apiobj.getChildAllObj(value, classuid).then((res)=>{
              let data_res=res.data.data.filter((item)=>{
                return this.ajax.ignoreClasses.indexOf(item.uuidC)==-1 && item.nameC.indexOf('(ext)')==-1?true:false;
              })
              if(res.data.result===1){
                this.setDataTree(data_res, classuid, value, true);
              }
          })
        }
      }
      this.getDateSectorObj(obj, classuid, callback)

    }
    getDateRangeObj=(Obj, classuid, callback)=>{
      let DataDate=this.props.UserStepStore.dataClaculator.date;
      let obj=Obj.map((item)=>{
        return item.uid;
      })
      let attrs=[this.ajax.dateRange.uidStartDate, this.ajax.dateRange.uidFinishDate];
      ct_apiobj.getAllValuesMany(obj, attrs).then((res)=>{
        if(res.data.result===1){
          let ObjDate=null;
          if(DataDate.date_de_signature){
            let ClientData=moment(DataDate.date_de_signature, DateFormat).toDate();
            let keys=Object.keys(res.data.data);
            keys.forEach((uid)=>{
                let objParams=res.data.data[uid];
                let finishdate=null, startData=null;
                for (let i = 0; i < objParams.length; i++) {
                  if(objParams[i].uid===this.ajax.dateRange.uidFinishDate){
                    finishdate= this.getDateHelper(objParams[i].value.value);
                  }
                  if(objParams[i].uid===this.ajax.dateRange.uidStartDate){
                    startData= this.getDateHelper(objParams[i].value.value);
                  }
                }
                if(startData && finishdate && startData<=ClientData && finishdate>ClientData){
                  ObjDate={uid:uid};
                }
            });

          }else{

            let keys=Object.keys(res.data.data);
            keys.forEach((uid)=>{
                let objParams=res.data.data[uid];
                let finishdate=null;
                for (let i = 0; i < objParams.length; i++) {
                  if(objParams[i].uid===this.ajax.dateRange.uidFinishDate){
                    finishdate= this.getDateHelper(objParams[i].value.value);
                  }
                }
                if(!ObjDate){
                  ObjDate={uid:uid, finish:finishdate};
                }else if(finishdate>Obj.finish){
                  ObjDate={uid:uid, finish:finishdate};
                }
            });

          }
          if(ObjDate){
            console.log(ObjDate);
            callback(ObjDate.uid, classuid);
          }
        }
      });

    }
    getDateHelper(Data){
      let DateIn=Data.split(' ')[0];
      let DateArr=DateIn.split('/');
      return new Date(DateArr[2], (Number(DateArr[1])-1), DateArr[0]);
    }

    getDateClimaticObj=(Obj, classuid, callback)=>{
      let code=this.props.UserStepStore.dataClaculator.information_lieu_travaux.code_postale_travaux;
      code=code.toString().slice(0,2);
      let ObjClimatic={uid:-1};
      if(code){
        let climatic_obj=this.state.climatic.find((item)=>{
          return item.code==code?true:false;
        })
        if(climatic_obj){
          let climatic_exist=Obj.find((item)=>{
            return item.name==climatic_obj.zone_name?true:false;
          })
          if(climatic_exist){
            ObjClimatic.uid=climatic_exist.uid;
          }
        }
      }
      callback(ObjClimatic.uid, classuid);

    }
    getDateSectorObj=(Obj, classuid, callback)=>{
      let ObjSector={uid:-1};
      let dataOperation =this.props.UserStepStore.dataOperation;
      if(dataOperation && dataOperation.length){
        let chosen_data=dataOperation[0].objUid;
        let available_value= Obj.map((item)=>{
          return item.name;
        });
        let chosen_value=chosen_data.find((item)=>{
          return available_value.indexOf(item.name)!==-1?true:false;
        });
        if(chosen_value){
          let chose_obj=Obj.find((item)=>{
            return chosen_value.name==item.name?true:false;
          });
          if(chose_obj){
            ObjSector.uid=chose_obj.uid;
          }
        }
      }
      callback(ObjSector.uid, classuid);

    }

    convertAdditBlockData=(addit_blocks)=>{
      return addit_blocks.filter((addit_block_exist) => {
          let attr_deleted = addit_block_exist.attributes.find((attr_item) => {
              return attr_item.uid == 'b741480b-4ed7-48a2-a7bd-54d5efdf25cc' ? true : false;
          })
          return !attr_deleted.value ? true : false;
      }).map((addit_block_exist) => {
          let attr_type = addit_block_exist.attributes.find((attr_item) => {
              return attr_item.uid == '5736390f-272e-46ff-a186-89e474a4d910' ? true : false;
          })
          let attr_deleted = addit_block_exist.attributes.find((attr_item) => {
              return attr_item.uid == 'b741480b-4ed7-48a2-a7bd-54d5efdf25cc' ? true : false;
          })
          let attr_name = addit_block_exist.attributes.find((attr_item) => {
              return attr_item.uid == '0fd7cb64-29cf-4232-85b1-e81d78682a42' ? true : false;
          })
          return {
              uid: addit_block_exist.uid,
              name: attr_name.value ? attr_name.value.value : '',
              data: [],
              type: {
                  uid: attr_type.value.uid,
                  name: attr_type.value.name,
              },
              isDeleted: attr_deleted.value,
          };
      });
    }
    changeAdditBlock=(data)=>{
      let stepper=this.state.stepper;
      let index=stepper.data[stepper.step].index;
      let new_data_addit_blocks=this.state.data_addit_blocks;
      new_data_addit_blocks[index]=data;
      this.setState({data_addit_blocks:new_data_addit_blocks});
    }
    changePrix=(e)=>{
      let {value}=e.currentTarget;
      let oper_data=this.state.operation_obj;

      let showModalConfirm=false;

      const updatePrix=(data, new_value)=>{
        //update prix_material and prix_work
        let coef= data.prix_material/data.prix_unitaire;
        data.prix_material=rounded(new_value*coef);
        data.prix_work=rounded(new_value-data.prix_material);
      }

      value=Number(value)?Number(value):0;
      if(value){
        value=value<=0?0:value;
      }
      let coef=oper_data.coef;
      let max_prix= oper_data.max_prix;

      if(value<=max_prix){
        value=value<Number(oper_data.min_prix)?Number(oper_data.min_prix):value;
      }else{
        showModalConfirm=true;
      }

      updatePrix(oper_data, value);
      oper_data.prix_unitaire=rounded(value);
      this.updatePriceData(oper_data);

      if(showModalConfirm){
        this.props.UserStepStore.setVisibleConfirmModal({
          show:true,
          text:'Le prix entraine un reste à charge pour le bénéficiaire de '+numberWithSpaces(oper_data.reste_a_charge)+' EUR',
          title:'Confirm',
          YesTitle:'Oui',
          NonTitle:'Non',
          callbackYes:()=>{
            updatePrix(oper_data, value);
            oper_data.prix_unitaire=rounded(value);
            this.updatePriceData(oper_data);
            this.setState({operation:oper_data});
          },
          callbackNo:()=>{
            updatePrix(oper_data, max_prix);
            oper_data.prix_unitaire=rounded(max_prix);
            this.updatePriceData(oper_data);
            this.setState({operation:oper_data});
          }
        });
      }else{
        this.setState({operation:oper_data});
      }


    }
    changePrix_adv=(e)=>{
      let {value}=e.currentTarget;
      let oper_data=this.state.operation_obj;

      value=Number(value)?Number(value):0;
      if(value){
        value=value<=0?0:value;
        value=value>=oper_data.prix_unitaire?oper_data.prix_unitaire:value;
      }

      oper_data.prix_material=rounded(value);
      oper_data.prix_work=rounded(oper_data.prix_unitaire-oper_data.prix_material);

      this.setState({operation:oper_data});


    }
    updatePriceData=(oper_data)=>{
      let coef=oper_data.coef;
      let max_prix= oper_data.max_prix;

      oper_data.cost_client=rounded(Number(oper_data.prix_unitaire)*Number(oper_data.surface));
      oper_data.cost_client=oper_data.cost_client>oper_data.surface*max_prix?rounded(oper_data.surface*max_prix):oper_data.cost_client;
      oper_data.cost_client=oper_data.cost_client<oper_data.surface*oper_data.min_prix?rounded(oper_data.surface*oper_data.min_prix):oper_data.cost_client;

      oper_data.cost=rounded(max_prix*Number(oper_data.surface));
      oper_data.cost_pro= rounded(Number(oper_data.cost)-Number(oper_data.cost_client));
      oper_data.reste_a_charge= rounded(oper_data.prix_unitaire*oper_data.surface-oper_data.cost_client);
    }

    setValueOper=(uid)=>{
      if(uid){
        ct_apiobj.getAllValues(uid).then((res)=>{
          if(res.data.result===1){
            let params=res.data.data[uid];
            let paramValue=params.find((item)=>{
              return item.name.toLowerCase()==='value'?true:false;
            })
            if(paramValue && paramValue.value){
              this.setValueRecData(uid, 'value', paramValue.value.value)
            }
          }
        })
      }
    }
    setRangeValueOper=(uid)=>{
      if(uid){
        ct_apiobj.getAllValues(uid).then((res)=>{
          if(res.data.result===1){
            let params=res.data.data[uid];
            let paramValueC=params.find((item)=>{
              return item.uid===this.ajax.dateRange.uidvalueClassic?true:false;
            })
            let paramValueP=params.find((item)=>{
              return item.uid===this.ajax.dateRange.uidvaluePrecair?true:false;
            })
            let paramValueGP=params.find((item)=>{
              return item.uid===this.ajax.dateRange.uidvalueGrandPrecair?true:false;
            })
            if(paramValueC && paramValueC.value && paramValueP && paramValueP.value && paramValueGP && paramValueGP.value){
              let rangeValue={
                ValueC:paramValueC.value.value,
                ValueP:paramValueP.value.value,
                ValueGP:paramValueGP.value.value,
              };
              this.setValueRecData(uid, 'rangeValue', rangeValue);
            }
          }
        })
      }
    }
    setValueRecData=(uid, key, value)=>{
      let dataTree=this.state.dataTree;
      let loopRec=function(DataTree){
        DataTree.forEach((item)=>{
          if(item.uid===uid){
            item[key]=value;
            return;
          }
          if(item.children.length){
            loopRec(item.children);
          }
        })
      };
      loopRec(dataTree);
      console.log(dataTree);
      this.setState({dataTree:dataTree});
    }
    getResultValue=()=>{
      let resultValue=1, objValue=[];
      let dataTree=this.state.dataTree;
      let loopRec=function(DataTree){
        DataTree.forEach((item)=>{
          if(item.value){
            resultValue*=Number(item.value);
            objValue.push({uid:item.uid, value:item.value});
          }
          if(item.children.length){
            loopRec(item.children);
          }
        })
      };
      loopRec(dataTree);
      return {resultValue:resultValue, objValue:objValue};
    }
    getRangeValue=()=>{
      let rangeValue=null, objValue=[];
      let dataTree=this.state.dataTree;
      let loopRec=function(DataTree){
        DataTree.forEach((item)=>{
          if(item.rangeValue){
            rangeValue=item.rangeValue;
            objValue.push({uid:item.uid, value:item.value});
          }
          if(item.children.length){
            loopRec(item.children);
          }
        })
      };
      loopRec(dataTree);
      return {rangeValue:rangeValue, objValue:objValue};
    }

    renderSelect=(Data, classuid, title, uid, value, isAuto)=>{
      let Option=[];
      if(Data){
        if(classuid===this.ajax.rootObj.type){
          //Oper BAR or BAT
          var newDataOperation=this.props.UserStepStore.dataOperation;
          let StepData=this.props.UserStepStore.dataClaculator;
          let isBar=false, isBat=false, isInd=false;
          if(newDataOperation.length){
            isBar=newDataOperation[0].operation.name.indexOf('BAR')!==-1?true:false;
            isBat=newDataOperation[0].operation.name.indexOf('BAT')!==-1?true:false;
            isInd=newDataOperation[0].operation.name.indexOf('IND')!==-1?true:false;

          }
          if(StepData.persone =='Personne physique'){
            isBar=true;
          }
          Data=Data.filter((item)=>{
            if(isBar){
              return item.name.indexOf('BAR')!==-1?true:false;
            }else if(isBat){
              return item.name.indexOf('BAT')!==-1?true:false;
            }else if(isInd){
              return item.name.indexOf('IND')!==-1?true:false;
            }
            return true;
          });
        }
        Option = Data.map((item)=>{
            let lable= item.name;
            if(item.label){
              lable=item.label;
            }
            return {value:item.uid, text:lable};
        })
      }
      //Option=[{value:null, text:'-Chose-'}, ...Option]

      let style:any={};
      if(this.ajax.dateRange.uid===classuid){
        style.display='none';
      }
      if(((uid && uid!=-1 && isAuto) || uid==undefined) && this.ajax.climatic.class_uid===classuid){
        style.display='none';
      }
      let disabled=false;
      if(isAuto && (this.ajax.climatic.class_uid===classuid || this.ajax.sector_activity.class_uid===classuid) && value!=-1){
          disabled=true;
      }
      const ValueInput=value?<div style={{display:'none'}} className="form-group"><label>Value</label><input disabled={true} value={value} className="form-control"/></div>:null;
      return(<React.Fragment>
        <div className="col-md-12" style={style}>
          <div className={'form-group '+(this.state.showError && (!uid || uid==-1)?'validate_error':'')}>
            <CustomSelect
              data={Option}
              value={uid}
              onChange={(opt_data)=>{this.selectChange(opt_data.value, classuid)}}
              disabled={disabled}
              placeHolderText={'-Choisir-'}
              label={title}
            />
            <label><span className="text">{title}</span><span className="label_ph"></span></label>
        </div>
        {ValueInput}
      </div>
      </React.Fragment>
    )
    };
    renderInput=(Data, classuid, title, uid, value)=>{
      let obj_data=Data[0];
      value=value?value:0;
      return(<div className="col-md-12">
        <div className={'form-group group_input user_digit '+(this.state.showError && (!uid || uid==-1)?'validate_error':'')}>
          <NumberInput onChange={(e)=>{this.inputChange(e, obj_data.uid, classuid)}} value={value} AdvProps={{className:"form-control"}}/>
          <label><span className="text">{obj_data.name}</span><span className="label_ph"></span></label>
        </div>
      </div>)
    }
    renderRecursive=(dataTree)=>{
      let Data=[];
      dataTree.forEach((item)=>{
        let value=item.value?item.value:null;
        var DataSelect=[this.renderSelect(item.obj, item.classuid, item.title,  item.uid, value, item.isAuto)];
        if(item.classuid==this.ajax.digitalClass.uid && item.obj.length){
          DataSelect=[this.renderInput(item.obj, item.classuid, item.title,  item.uid, value)];
        }
        if(item.children.length){
          let childData=this.renderRecursive(item.children);
          DataSelect=[...DataSelect, ...childData];
        }
        Data=[...Data, ...DataSelect];
      });
      return Data;
    }
    renderOperations=()=>{
      let Data=this.renderRecursive(this.state.dataTree);
    //  let resultValue=this.getResultValue();
    //  Data.push(<div className="form-group"><label>Result value</label><input disabled={true} value={resultValue} className="form-control"/></div>)
      return (<React.Fragment>{Data}</React.Fragment>);
    }
    renderPrix=()=>{
      return(<div className="prix_unitare_content">
              <div className="title title_step">Prix unitaire, TTC</div>
              <div className="form-group group_input">
                <NumberInput priceFormat={true} changeOnBlur={true} prefix={'€ '} onChange={this.changePrix} value={this.state.operation_obj.prix_unitaire} AdvProps={{className:"form-control"}}/>
              </div>
              <div className="adv_prix group_input">
                <div className="adv_prix_item">
                  <label>Materiel</label>
                  <NumberInput priceFormat={true} changeOnBlur={true} prefix={'€ '} onChange={this.changePrix_adv} value={this.state.operation_obj.prix_material} AdvProps={{className:"form-control"}}/>
                </div>
                <div style={{marginLeft:25}} className="adv_prix_item">
                  <label>Main d'oeuvre</label>
                  <span className="value">{'€ '+numberWithSpaces(this.state.operation_obj.prix_work)}</span>
                </div>
              </div>
      </div>);
    }
    renderIsolants=(data)=>{
      let errors_data=this.state.validateError;
      let error=errors_data.map((error_item)=>{return error_item.id});

      let data_isolant=this.state.operation_obj.isolant.find((item)=>{
          return item.id==data.isolant_uid?true:false;
      })
      if(!data_isolant){
        data_isolant={value:'empty', id:null, advParams:{}, adv_size:[]};
      }
      if(data.step_surface){
        return(<React.Fragment>{this.renderSurfaceInput(this.state.operation_obj, data_isolant, error)}</React.Fragment>)
      }else{
        return(<React.Fragment>{this.renderSelectIsolants(this.state.operation_obj, data_isolant)}</React.Fragment>)
      }

    }
    renderSurfaceInput=(Data, itemIsolant, error)=>{
      if(itemIsolant.id){
        /*let Operations=['BAR-TH-160', 'BAT-TH-146'];
        let isSpOper=Operations.indexOf(Data.operation.name)!==-1?true:false;*/
        let opt_adv_size=Data.options_adv_size.filter((item)=>{
          //remove bure de contole from options
          return item.uid=='cfb7d57f-7b3e-49f7-8fd4-e7fd43eb2110'?false:true;
        });
        let isSpOper=opt_adv_size.length?true:false;

        let surfaceInput=<NumberInput onChange={(e)=>{this.surfaceChange(e, itemIsolant)}} value={itemIsolant.surface} AdvProps={{name:"d", className:"form-control surface_input"}}/>;
        return(
          <React.Fragment>
            {!isSpOper && <div className="title title_step">Quantité</div>}
            <div className={'form-group group_input '+(error.indexOf(itemIsolant.id+'_surface')!==-1?'validate_error':'')}>
                {!isSpOper && surfaceInput}
                {isSpOper?this.renderAdvSurfaceInput(Data, itemIsolant):null}
              </div>
          </React.Fragment>
          );
      }else{
        return null;
      }
    }
    renderAdvSurfaceInput=(Data, itemIsolant)=>{
      let isEmptyLast=true;
      let adv_data = JSON.parse(JSON.stringify(itemIsolant.adv_size));

      let def_epp=0;
      let ep_key_attr= Data.adv_attr_name.find((item)=>{
        return item.value=='Epaisseur'?true:false;
      });
      let ep_key=null;
      if(ep_key_attr){
        ep_key=ep_key_attr.name.replaceAll(/name/ig, '').trim();
      }
      if(ep_key && itemIsolant.advParams && itemIsolant.advParams[ep_key] && itemIsolant.advParams[ep_key].value){
        def_epp=itemIsolant.advParams[ep_key].value;
      }
      if(adv_data.length){
          let last=adv_data[adv_data.length-1];
          if(last.d || last.l || (last.ep && last.ep!=def_epp)){
            isEmptyLast=false;
          }
      }
      if(!adv_data.length){
        let empty_item:any={d:0, l:0, ep:def_epp};
        adv_data.push(empty_item);
      }

      let opt_adv_size=Data.options_adv_size.map((item)=>{
        return item.uid;
      });
      let adv_size_items=null;
      let change_max_prix=opt_adv_size.indexOf('12af3eb1-ecd0-4b87-884e-039ad62851e7')!==-1?true:false;
      if(opt_adv_size.indexOf('5dc70d2c-8882-4e1a-807d-409b0e368c51')!==-1){
          adv_size_items=this.renderDiameter(Data, itemIsolant, adv_data, change_max_prix);
      }else if(opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!==-1){
        adv_size_items=this.renderDiameterTemerature(Data, itemIsolant, adv_data, change_max_prix);
      }else if(opt_adv_size.indexOf('5c312056-099e-4ac6-9a5f-2954b797ab96')!==-1){
        adv_size_items=this.renderTemerature(Data, itemIsolant, adv_data, change_max_prix);
      }
      return (<React.Fragment>
      <div className="title">Total</div>
      <div className="adv_size_cont">
        <div style={{textAlign:'right', paddingRight:40}}><span className="bold">{(itemIsolant.surface?itemIsolant.surface:'')}</span></div>
        <div className="adv_size_items">{adv_size_items}</div>
        <div className="adv_size_btn">
          <button disabled={Data.disableSurface} onClick={(e)=>{this.addAdvSize(e, itemIsolant.id, change_max_prix)}} className="btn">+</button>
        </div>
      </div></React.Fragment>);
    }
    renderDiameter=(Data, itemIsolant, adv_data, change_max_prix)=>{
      let onBlur={};
      if(change_max_prix){
        onBlur={onBlur:()=>{this.calcMaxPrix()}}
      }
      return adv_data.map((item, i)=>{
        let btn=<div className="btn_cont"><button disabled={Data.disableSurface} onClick={(e)=>{this.removeAdvsize(e, itemIsolant.id, i, change_max_prix)}} type="button" className="btn"><Icon name="trash" size={20} color={'#FF2E00'}/></button></div>;
        if(!item.ep){
          item.ep=0;
        }
        return (<div key={'ad_size_'+itemIsolant.id+'_'+i} className="adv_size_item flex_responsive">
          <div className="input_cont form-group">
            <div className="input_unit">
              <NumberInput AdvProps={{name:"ep", className:"form-control", disabled:Data.disableSurface}} {...onBlur} onChange={(e)=>{this.changeAdvSize(e, itemIsolant.id, i, change_max_prix)}}  value={(item.ep)}  />
              <span className="unit">mm</span>
              <label><span className="text">Epaisseur (mm)</span><span className="label_ph"></span></label>
            </div>
          </div>
          <div className="input_cont form-group">
            <div className="input_unit">
              <NumberInput AdvProps={{name:"d", className:"form-control", disabled:Data.disableSurface}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, itemIsolant.id, i, change_max_prix)}}  value={item.d}  />
              <span className="unit">mm</span>
              <label><span className="text">Diamètre (mm)</span><span className="label_ph"></span></label>
            </div>
          </div>
          <div className="input_cont form-group">
            <div className="input_unit">
              <NumberInput AdvProps={{name:"l", className:"form-control", disabled:Data.disableSurface}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, itemIsolant.id, i, change_max_prix)}}  value={item.l}  />
              <span className="unit">m</span>
              <label><span className="text">Longueur (linéaire)</span><span className="label_ph"></span></label>
            </div>
          </div>
          {btn}
        </div>);
      })
    }
    renderDiameterTemerature=(Data, itemIsolant, adv_data, change_max_prix)=>{
      let onBlur={};
      if(change_max_prix){
        onBlur={onBlur:()=>{this.calcMaxPrix()}}
      }
      return adv_data.map((item, i)=>{
        let btn=<div className="btn_cont"><button disabled={Data.disableSurface} onClick={(e)=>{this.removeAdvsize(e, itemIsolant.id, i, change_max_prix)}} type="button" className="btn"><Icon name="trash" size={20} color={'#FF2E00'}/></button></div>;
        if(!item.ep){
          item.ep=0;
        }
        let errortext=null;
        if(item.d<20){
          errortext=getErrorMsg([{id:'error_d', id_msg:'error_d_msg'}], 'error_d')
        }
        return (<div key={'ad_size_'+itemIsolant.id+'_'+i} className="adv_size_item flex_responsive">
          <div className="input_cont form-group">
            <div className="input_unit">
              <NumberInput AdvProps={{name:"d", className:"form-control", disabled:Data.disableSurface}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, itemIsolant.id, i, change_max_prix)}}  value={item.d}  />
              <span className="unit">mm</span>
              <label><span className="text">Diamètre (mm)</span><span className="label_ph"></span></label>
            </div>
            {errortext}
          </div>
          <div className="input_cont form-group">
            <div className="input_unit">
              <NumberInput AdvProps={{name:"l", className:"form-control", disabled:Data.disableSurface}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, itemIsolant.id, i, change_max_prix)}}  value={item.l}  />
              <label><span className="text">Quantité</span><span className="label_ph"></span></label>
            </div>
          </div>
          {btn}
        </div>);
      })
    }
    renderTemerature=(Data, itemIsolant, adv_data, change_max_prix)=>{
      let onBlur={};
      if(change_max_prix){
        onBlur={onBlur:()=>{this.calcMaxPrix()}}
      }
      return adv_data.map((item, i)=>{
        let btn=<div className="btn_cont"><button disabled={Data.disableSurface} onClick={(e)=>{this.removeAdvsize(e, itemIsolant.id, i, change_max_prix)}} type="button" className="btn"><Icon name="trash" size={20} color={'#FF2E00'}/></button></div>;
        if(!item.ep){
          item.ep=0;
        }
        return (<div key={'ad_size_'+itemIsolant.id+'_'+i} className="adv_size_item flex_responsive">
          <div className="input_cont form-group">
              <CustomSelect
                data={[
                  {value:'100', text:'50°C ≤ T ≤ 120°C'},
                  {value:'200', text:'T > 120°C'},
                ]}
                value={item.ep}
                onChange={(opt_data)=>{
                    this.changeAdvSize({currentTarget:{value:opt_data.value, name:'ep'}}, itemIsolant.id, i, change_max_prix);
                    this.calcMaxPrix();
                }}
                label={'Température (°C)'}
                disabled={Data.disableSurface}
                placeHolderText={'-Chose Température-'}
              />
              <label><span className="text">Température (°C)</span><span className="label_ph"></span></label>
          </div>
          <div className="input_cont form-group">
            <div className="input_unit">
              <NumberInput AdvProps={{name:"l", className:"form-control", disabled:Data.disableSurface}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, itemIsolant.id, i, change_max_prix)}}  value={item.l}  />
              <label><span className="text">Quantité</span><span className="label_ph"></span></label>
            </div>
          </div>
          {btn}
        </div>);
      })
    }

    getIsolantAdvParam=(data_name_param, adv_param)=>{
      let adv_params_name={};
      let keys_param= Object.keys(adv_param);
      if(keys_param.length){
        keys_param.forEach((key)=>{
          if(key.indexOf('Attribute')!==-1){
            let name_attr= data_name_param.find((item)=>{
              return item.name.indexOf(key)!==-1?true:false;
            });
            if(name_attr){
              adv_params_name[key]={...adv_param[key]};
              adv_params_name[key]['title']=name_attr.value;
            }
          }else{
            adv_params_name[key]=adv_param[key];
          }
        })
      }else{
        let adv_params_new={};
        data_name_param.forEach((item)=>{
          let key=item.name.replaceAll(/name/ig, '').trim();
          adv_params_new[key]={title:item.value, value:''};
        })
        adv_params_name={
          'Marque':{title:'Marque', value:''},
          'Reference':{title:'Reference', value:''},
          ...adv_params_new
        }
      }
      return adv_params_name;
    }
    changeParamIsolant=(e, item_isolant)=>{
      let {name, value}= e.currentTarget;
      if(item_isolant.value!=-2){
         item_isolant.value=-2;
         item_isolant.name='-Ajouter-';
      }
      let paramChange=item_isolant.advParams[name]?item_isolant.advParams[name]:null;
      if(paramChange){
        paramChange.value=value;
      }
      let oper_data=this.state.operation_obj;
      oper_data.isolant=oper_data.isolant.map((item)=>{
          if(item.id==item_isolant.id){
            return item_isolant;
          }
          return item;
      })
      this.setState({operation_obj:oper_data});


    }
    surfaceChange=(e, item_isolant)=>{
      let oper_data=this.state.operation_obj;
      let {value}= e.currentTarget;
      oper_data.isolant=oper_data.isolant.map((item)=>{
          if(item.id==item_isolant.id){
            item.surface=value;
          }
          return item;
      })
      let sumSurface=0;
      oper_data.isolant.forEach((item_isolant)=>{
          sumSurface+=Number(item_isolant.surface);
      });
      oper_data.surface=sumSurface;
      this.setState({operation_obj:oper_data});

    }
    isolantChange=(opt_data, item_isolant_id)=>{
      console.log(opt_data);
      let oper_data=this.state.operation_obj;

      let adv_params_name={};
      opt_data.value=opt_data.value==-1?null:opt_data.value;
      if(opt_data.value!==null){
          adv_params_name=this.getIsolantAdvParam(this.state.operation_obj.adv_attr_name, opt_data.advParams)
      }

      let isolantData={value:opt_data.value, name:opt_data.label, surface:this.state.operation_obj.def_quant, adv_size:[], advParams:adv_params_name, id:(item_isolant_id?item_isolant_id:Math.round(Math.random()*1000000000))};
      if(opt_data.value){
        if(item_isolant_id){
          oper_data.isolant=oper_data.isolant.map((item)=>{
              if(item.id==item_isolant_id){
                item.value=isolantData.value;
                item.name=isolantData.name;
                item.advParams=isolantData.advParams;
              }
              return item;
          })
        }else{
          oper_data.isolant.push(isolantData);
        }
      }else if(!opt_data.value && item_isolant_id){
        oper_data.isolant=oper_data.isolant.filter((item)=>{
          return item.id==item_isolant_id?false:true;
        })
      }
      console.log(oper_data);
      let stepper=this.state.stepper;
      if(opt_data.value){
          stepper.data[stepper.step].isolant_uid=item_isolant_id?item_isolant_id:isolantData.id;
      }else{
          stepper.data[stepper.step].isolant_uid=null;
      }

      let sumSurface=0;
      oper_data.isolant.forEach((item_isolant)=>{
          sumSurface+=Number(item_isolant.surface);
      });
      oper_data.surface=sumSurface;

      this.setState({operation_obj:oper_data, stepper:stepper});

    }

    changeAdvSize=(e, isolant_id, index, change_max_prix)=>{
      let {value, name}=e.currentTarget;
      value=value!==''?Number(value):'';
      if(value){
        value=value<=0?0:value;
      }
    //  let Operations=['BAR-TH-160', 'BAT-TH-146'];
      let oper_data=this.state.operation_obj;

      let data_isolant=oper_data.isolant.find((item_isolant)=>{
          return item_isolant.id==isolant_id?true:false;
      });
      if(data_isolant && data_isolant.adv_size){
      //  let uniq_oper=Operations.indexOf(oper_data.operation.name)!==-1?true:false;
        let def_epp=0;
        let ep_key_attr= oper_data.adv_attr_name.find((attr_adv)=>{
          return attr_adv.value=='Epaisseur'?true:false;
        });
        let ep_key=null;
        if(ep_key_attr){
          ep_key=ep_key_attr.name.replaceAll(/name/ig, '').trim();
        }
        if( ep_key && data_isolant.advParams && data_isolant.advParams[ep_key] && data_isolant.advParams[ep_key].value){
          def_epp=data_isolant.advParams[ep_key].value;
        }
        if(!data_isolant.adv_size[index]){
          let new_adv_size:any={l:0, d:0, ep:def_epp};
          data_isolant.adv_size.push(new_adv_size);
        }
        data_isolant.adv_size[index][name]=value;
        if(name=='l'){
          let sum_l = 0;
          data_isolant.adv_size.forEach((adv_size_item)=>{
            sum_l+=Number(adv_size_item.l);
          })
          data_isolant.surface=sum_l;
        }
      }
      let sumSurface=0;
      oper_data.isolant.forEach((item_isolant)=>{
          sumSurface+=Number(item_isolant.surface);
      });
      oper_data.surface=sumSurface;
      this.setState({operation_obj:oper_data});

    }
    addAdvSize = (e, isolant_id, change_max_prix)=>{
    //  let Operations=['BAR-TH-160', 'BAT-TH-146'];
      let oper_data=this.state.operation_obj;

      let data_isolant=oper_data.isolant.find((item_isolant)=>{
          return item_isolant.id==isolant_id?true:false;
      });
      if(data_isolant && data_isolant.adv_size){
        //let uniq_oper=Operations.indexOf(oper_data.operation.name)!==-1?true:false;
        let def_epp=0;
        let ep_key_attr= oper_data.adv_attr_name.find((attr_adv)=>{
          return attr_adv.value=='Epaisseur'?true:false;
        });
        let ep_key=null;
        if(ep_key_attr){
          ep_key=ep_key_attr.name.replaceAll(/name/ig, '').trim();
        }
        if(ep_key && data_isolant.advParams && data_isolant.advParams[ep_key] && data_isolant.advParams[ep_key].value){
          def_epp=data_isolant.advParams[ep_key].value;
        }

        let new_adv_size:any={l:0, d:0, ep:def_epp};
        if(!data_isolant.adv_size.length){
          data_isolant.adv_size.push({...new_adv_size});
        }
        data_isolant.adv_size.push(new_adv_size);
      }
      this.setState({operation_obj:oper_data}, ()=>{
        if(change_max_prix){
          this.calcMaxPrix();
        }
      });
    }
    removeAdvsize=(e, isolant_id, index, change_max_prix)=>{
      let {value, name}=e.currentTarget;
      let oper_data=this.state.operation_obj;
      if(value){
        value=value<=0?0:value;
      }

      let data_isolant=oper_data.isolant.find((item_isolant)=>{
          return item_isolant.id==isolant_id?true:false;
      });
      if(data_isolant && data_isolant.adv_size){
        data_isolant.adv_size=data_isolant.adv_size.filter((adv_size_item, adv_size_index)=>{
          return adv_size_index==index?false:true;
        })
        let sum_l = 0;
        data_isolant.adv_size.forEach((adv_size_item)=>{
          sum_l+=Number(adv_size_item.l);
        })
        data_isolant.surface=sum_l;
      }
      let sumSurface=0;
      oper_data.isolant.forEach((item_isolant)=>{
          sumSurface+=Number(item_isolant.surface);
      });
      oper_data.surface=sumSurface;
      this.setState({operation_obj:oper_data}, ()=>{
        if(change_max_prix){
          this.calcMaxPrix();
        }
      });
    }
    calcMaxPrix=()=>{
      let data_oper= this.state.operation_obj;
      if(data_oper){
        let Data=this.props.UserStepStore.dataClaculator;
        let isolant_data=JSON.parse(JSON.stringify(data_oper.isolant));

        let opt_adv_size=data_oper.options_adv_size.map((item)=>{
          return item.uid;
        });

        isolant_data.forEach((isolant_item)=>{
          if(!isolant_item.adv_size || !isolant_item.adv_size.length){
            isolant_item.adv_size=[{ep:0, d:0, l:0}];
          }
        })
        if(opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!==-1){
        //  adv_size_items=this.renderDiameterTemerature(Data, itemIsolant, adv_data, change_max_prix);
          isolant_data.forEach((isolant_item)=>{
            isolant_item.adv_size=  isolant_item.adv_size.map((adv_size_item)=>{
              return {t:adv_size_item.ep, d:adv_size_item.d, q:adv_size_item.l};
            })
          })
        }else if(opt_adv_size.indexOf('5c312056-099e-4ac6-9a5f-2954b797ab96')!==-1){
          //adv_size_items=this.renderTemerature(Data, itemIsolant, adv_data, change_max_prix);
          isolant_data.forEach((isolant_item)=>{
            isolant_item.adv_size=  isolant_item.adv_size.map((adv_size_item)=>{
              return {t:adv_size_item.ep, q:adv_size_item.l};
            })
          })
        }

        let data_calc={
          form:Data,
          oper_data:data_oper.objUid,
          isolant_data:isolant_data,
        };
        ct_apistep.getOperCalcData('c960cdcb-59d7-4494-8c8a-1e60a7910068', data_calc).then((res)=>{
          if(res.data.result==1){
            data_oper.max_prix=res.data.data.value;

            data_oper.prix_unitaire=data_oper.max_prix;
            data_oper.prix_material=rounded(data_oper.prix_unitaire*0.4);
            data_oper.prix_work=rounded(data_oper.prix_unitaire*0.6);

            this.setState({operation_obj:data_oper});
          }
        })
      }
    }

    validStep=(stepper, new_step, valid_cur_adv_par)=>{
      let isolant_ids_before=stepper.data.slice(0, new_step).filter((item)=>{
        return item.name=='isolants' && item.isolant_uid?true:false;
      }).map((item)=>{
        return item.isolant_uid;
      })

      let isolant_ids_after=stepper.data.slice((new_step+1)).filter((item)=>{
        return item.name=='isolants' && item.isolant_uid?true:false;
      }).map((item)=>{
        return item.isolant_uid;
      })

      let errors_data=this.state.validateError;
      if(isolant_ids_before.length){
        errors_data=this.validData(errors_data, true, true, isolant_ids_before);
      }
      if(valid_cur_adv_par){
        errors_data=this.validData(errors_data, true, false, [stepper.data[stepper.step].isolant_uid]);
      }
      if(isolant_ids_after.length){
        errors_data=this.removeErrorValidate(errors_data, isolant_ids_after);
      }
      this.setState({validateError:errors_data});

      if(errors_data && errors_data.length){
        return false;
      }
      return true;
    }
    validData=(errors_data, valid_adv_params, valid_size, checked_isolant)=>{
      let Data=[this.state.operation_obj];
      let errors=[], removeErrors=[];
      Data.forEach((item)=>{
        if(item.isolant && item.isolant.length){
          let opt_adv_size=item.options_adv_size.map((item)=>{
            return item.uid;
          });
          item.isolant.forEach((itemIsolant)=>{
            if(!checked_isolant || (checked_isolant && checked_isolant.indexOf(itemIsolant.id)!==-1)){
              if(itemIsolant && (!itemIsolant.surface || itemIsolant.surface==0) && valid_size){
                  errors.push({id:itemIsolant.id+'_surface'});
              }else{
                if(itemIsolant.adv_size && itemIsolant.adv_size.length && valid_size){

                  let no_d=opt_adv_size.indexOf('5c312056-099e-4ac6-9a5f-2954b797ab96')!=-1?true:false;
                  let no_ep=opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!=-1?true:false;

                  let has_empty_val = itemIsolant.adv_size.find((adv_size_item)=>{
                      return !adv_size_item.l || (!no_d && !adv_size_item.d) || (!no_ep && !adv_size_item.ep) ? true:false;
                  });

                  let check_d=opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!=-1?true:false;
                  if(!has_empty_val && check_d){
                    has_empty_val=itemIsolant.adv_size.find((adv_size_item)=>{
                        return adv_size_item.d<20?true:false;
                    });
                  }

                  if(has_empty_val){
                    errors.push({id:itemIsolant.id+'_surface'});
                  }else{
                    removeErrors.push(itemIsolant.id+'_surface');
                  }

                }else{
                  removeErrors.push(itemIsolant.id+'_surface');
                }
              }

              if(itemIsolant && itemIsolant.advParams && valid_adv_params){
                let keys= Object.keys(itemIsolant.advParams);
                keys.forEach((key)=>{
                  if(!itemIsolant.advParams[key].value){
                    errors.push({id:'adv_param_isolant_'+itemIsolant.id+'_'+key});
                  }else{
                    removeErrors.push('adv_param_isolant_'+itemIsolant.id+'_'+key);
                  }
                })
              }
          }
          })
        }
      });
      let new_errors=errors_data;
      if(errors.length){
        errors.forEach((error)=>{
          let exist_error= new_errors.find((error_item)=>{
            return error_item.id==error.id?true:false;
          })
          if(!exist_error){
            new_errors.push(error);
          }
        });
      }
      if(removeErrors.length){
        new_errors=new_errors.filter((error_item)=>{
          return removeErrors.indexOf(error_item.id)==-1?true:false;
        });
      }
      return new_errors;
    }
    removeErrorValidate=(errors_data, isolant_ids)=>{
      let Data=[this.state.operation_obj];
      let removeErrors=[];
      Data.forEach((item)=>{
        if(item.isolant && item.isolant.length){
          item.isolant.forEach((itemIsolant)=>{
            if(isolant_ids.indexOf(itemIsolant.id)!==-1){
              removeErrors.push(itemIsolant.id+'_surface');
            }
            if(itemIsolant && itemIsolant.advParams){
              let keys= Object.keys(itemIsolant.advParams);
              keys.forEach((key)=>{
                removeErrors.push('adv_param_isolant_'+itemIsolant.id+'_'+key);
              })
            }
          })
        }
      });

      let new_errors=errors_data;
      if(removeErrors.length){
        new_errors=new_errors.filter((error_item)=>{
          return removeErrors.indexOf(error_item.id)==-1?true:false;
        });
      }
      return new_errors;
    }

    renderSelectIsolants=(Data, itemIsolant)=>{
      let DataIsolant=this.state.isolants_data.data, dataSelect=null, uid=null;;

      let Option=DataIsolant.map((item)=>{
        if(itemIsolant && itemIsolant.value==item.uid){
          dataSelect={ value: item.uid, text: item.name, advParams:itemIsolant.advParams , adv_size:item.adv_size};
          uid=item.uid;
        }
        return { value: item.uid, text: item.name, advParams:item.advParams, adv_size:item.adv_size };
      })

      let errors_data=this.state.validateError;
      let error=errors_data.map((error_item)=>{return error_item.id});

        let adv_params_name=this.getIsolantAdvParam(Data.adv_attr_name, itemIsolant.advParams);
        let new_item_isolant={ value: '-2', text:'-Ajouter-', advParams:adv_params_name, adv_size:itemIsolant.adv_size};
        Option=[new_item_isolant, ...Option];
        if(itemIsolant && itemIsolant.value==-2){
          dataSelect=new_item_isolant;
          uid=new_item_isolant.value;
        }
      //}
      if(itemIsolant && itemIsolant.id){
        Option=[{ value: '-1', advParams:null,  adv_size:[], text:'-Retirer l\'opération-'}, ...Option];
      }
    //
      let advParam_html=null;
      if(dataSelect && dataSelect.advParams){
        let Operations=['BAR-TH-160', 'BAT-TH-146'];
        advParam_html=Object.keys(dataSelect.advParams).filter((key)=>{
          return Operations.indexOf(Data.operation.name)!==-1 && dataSelect.advParams[key].title=='Epaisseur'?false:true;
        }).map((key)=>{
          let param=dataSelect.advParams[key];
          let input=<input name={key} onChange={(e)=>{this.changeParamIsolant(e, itemIsolant)}} value={param.value} className="form-control"/>;
          if(dataSelect.advParams[key].type && dataSelect.advParams[key].type=='number'){
             input=<NumberInput onChange={(e)=>{this.changeParamIsolant(e, itemIsolant)}} value={param.value} AdvProps={{name:param.title, className:"form-control"}}/>;
          }
          let label=param.title;
          return <div key={itemIsolant.id+'_'+param.title} style={{padding:'0px', margin:'0.7rem 0px'}} className={'adv_param_item form-group group_input '+(error.indexOf('adv_param_isolant_'+itemIsolant.id+'_'+key)!==-1?'validate_error':'')}>{input}<label><span className="text">{label}</span><span className="label_ph"></span></label></div>
        });
      }

      let title=<div className="title title_step">Matériel / équipement</div>;
      return(
        <React.Fragment>
          {title}
          <div className={'form-group '+(this.state.showError && !itemIsolant.id?'validate_error':'')}>
            <CustomSelect
              data={Option}
              value={uid}
              onChange={(opt_data)=>{this.isolantChange(opt_data, itemIsolant.id)}}
              placeHolderText={'-Ajouter matériel/équipement pour la même opération-'}
            />
          </div>
          {advParam_html}
        </React.Fragment>

      )
    }


    render() {

        let content=null;
        if(this.props.isShow){
          let stepper_elem=null;
          if(this.state.stepper.data){
            stepper_elem=this.state.stepper.data.map((item, i)=>{
              return <span onClick={()=>{this.clickOnStep(i)}} key={'step_ad_op_'+(i+1)} className={'step_item '+(i==this.state.stepper.step?'selected':'')}></span>;
            });
          }

          let body=null;
          let curr_step_data=this.state.stepper.data[this.state.stepper.step];
          if(curr_step_data.name=="operation"){
            body=<div className="add_oper_content_flex">{this.renderOperations()}</div>;
          }else if(curr_step_data.name=="adit_block"){
            body=<AdditionalBlocks data={this.state.data_addit_blocks[curr_step_data.index]} onChange={this.changeAdditBlock} />
          }else if(curr_step_data.name=="isolants"){
            body=<div className="isolants_content">{this.renderIsolants(curr_step_data)}</div>;
          }else if(curr_step_data.name=="prix_unitare"){
            body=this.renderPrix();
          }


          let add_isolant_btn=null
          if(curr_step_data && curr_step_data.name=="isolants" &&  curr_step_data.step_surface){
            add_isolant_btn=<button onClick={()=>{this.addIsolant()}} type="button" className="btn btn_add_isolant">
                <span style={{display:'flex', alignItems:'center'}}>
                    <span className="ico">+</span>
                    Ajouter un autre isolant
                </span>
              </button>
          }
          let isolants_steps=this.state.stepper.data.filter((item)=>{
            return item.name=='isolants'?true:false;
          })
          if(curr_step_data && curr_step_data.name=="isolants" &&  !curr_step_data.step_surface && isolants_steps.length>1){
            add_isolant_btn=<button onClick={this.removeIsolant} type="button" className="btn btn_remove_isolant">
                <span style={{display:'flex', alignItems:'center'}}>
                    <Icon size={20} name="trash" color={'#0069A5'} />
                    Retirer l'isolant
                </span>
              </button>
          }
          content=<div className="operations_add_comp">
              <div className="operations_add_backdrop"></div>
              <div className="operations_add_modal">
                <div className="operations_add_data">
                    <div className="add_oper_stepper">{stepper_elem}</div>
                    <div className="add_oper_content">{body}</div>
                    <div className="add_oper_btn">
                      <div className="btn_left">
                        <button onClick={this.handleClose} type="button" className="btn btn_cancel"><Icon name="trash" size={20} color={'#FF2E00'}/></button>
                      </div>
                      <div className="btn_right">
                        {add_isolant_btn}
                        <button onClick={this.clickNexStep}  type="button" className="btn btn_next">{(this.state.stepper.data.length-1==this.state.stepper.step?'Valider':'Suivant →')}</button>
                      </div>
                    </div>
                </div>
              </div>
          </div>
        }
        return(<BlockUi className="addit_block_content" tag="div" loader={<EnematLoader />} blocking={this.state.blocking}>
                <div className="col-md-12">
                    {content}
                </div>
              </BlockUi>)
    }
}
