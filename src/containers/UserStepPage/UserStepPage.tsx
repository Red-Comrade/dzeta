import * as React from "react";

import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import EnematLoader from '../../components/svg/EnematLoader'
import {inject, observer} from "mobx-react";

import {CustomDatePicker} from './Components/CustomDatePicker'
import {CustomSelect} from './Components/CustomSelect'
import {CustomRadio} from './Components/CustomRadio'
import {CustomSwitcher} from './Components/CustomSwitcher'
import {CustomTolltip} from './Components/CustomTolltip'
import getErrorMsg from './Components/ErrorHandler'

import {NumberInput} from './Components/NumberInput'
import {Icon} from './Components/Icon'

import * as moment from 'moment';

import * as ct_apiobj from "../../api/ObjTree-api";
import * as ct_apiview from "../../api/View-api";
import * as ct_apistep from "../../api/StepForm-api";

import axios from 'axios';
import qs from 'qs';

import {OperationsAdd} from './OperationsAdd'
import {OperationsEdit} from './OperationsEdit'

import './UserStepPage.scss'
import RootStore from "../../stores/RootStore";



const DateFormat='DD/MM/yyyy';



const numberWithSpaces=(x)=> {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ").replaceAll(/\./gi, ',');
}
const rounded = function(number){
    return +number.toFixed(2);
}


interface IUserStepPageProps {
  UserStepStore?:any,
  ToastrStore?:any,
  initialStore:{
    dataClaculator:any,
    dataOperation:any[],
  },
  config:any,
}
interface IUserStepPageState {
  blocking:boolean,
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class UserStepPage extends React.Component<IUserStepPageProps, IUserStepPageState> {

  saveData=(FullData)=>{
    this.setState({blocking:true})
    axios.post(
        '/dop/stepFormGetDataJson',
        qs.stringify(FullData), {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        }
    ).then((res)=>{
      if(res.data.result==1){
        this.setState({blocking:false}, ()=>{
          window.location.href='Object/downloadFile?file='+res.data.data.key;
        });
      }else{
        this.setState({blocking:false});
      }
    }).catch(()=>{
      this.setState({blocking:false});
    });
  }
  config={
    mode:'AHFull',
    Data:[
      {title:'Date lieés au projet', step:0, showOrganization:true},
      {title:'Bénéficiaire', step:1},
      {title:'Adresse', step:2},
      {title:'Sous-traitance', step:3},
      {title:'Opérations', step:4},
    ],
    finishFunc:this.saveData,
    finishLabel:'Générer AH',
    showStepper:true,
    showDate_de_debut:true,
    showDate_de_finDeTravaux:true,
    oper_action:['add', 'remove', 'change_previsite', 'change_bureaudecontrole', 'change_referencerapport', 'change_oper'],
    oper_adv_column:['bureaudecontrole', 'referencerapport'],
    isolant_action:['change', 'change_param', 'change_surf', 'change_prime', 'change_prix', 'change_adv_size'],
    blockOrganization:false,
    cancelBtn:null,

  }

  state={
    blocking:false
  };
  constructor(props) {
    super(props);
    if(this.props.initialStore){
      this.props.UserStepStore.initCalculatorData(this.props.initialStore.dataClaculator, this.props.initialStore.dataOperation);
    }else{
      this.props.UserStepStore.initCalculatorData({
        persone:'Personne morale',
        organization:{
          name:'Esso',
          uid:'b63dd855-2b40-4cef-ba4a-201e76966136',
        },
        date:{
          date_de_signature:'',
          date_de_debut:'',
          date_facture:'',
          numero_facture:'',
        },
        information_lieu_travaux:{
          operationavec:'Operation classique',
          nom_travaux:'',
          adresse_travaux:'',
          code_postale_travaux:'',
          ville_travaux:'',
          siern_travaux:'',
          same_adress:0,
          parcelle_cadastrale:'',
          adv_field:{
            //qpv
            code_quartier:'',
            //Bailleur Social
            raison_sociale:'',
            siern:'',
            nom:'',
            prenom:'',
            fonction:'',
            phone:'',
            adresse:'',
            code_postale:'',
            ville:'',

            nombre_total:'',
            nombre_total_menages:'',

            //Operation avec precarite
            adv_fiscal_data:[{
              numero_fiscal:'',
              reference_fiscal:'',
            }],
          }
        },
        beneficiaire:{
          previsite:'',
          nom:'',
          prenom:'',
          fonction:'',
          phone:'',
          email:'',
          raison_sociale:'',
          siern:'',
          adresse:'',
          code_postale:'',
          ville:'',
          persone_phys_data:[],
          number_de_perosne:'',
          revenue:'',
        },
        sous_traitance:{
          sous_traitance:1,
          siret:'',
          nom:'',
          prenom:'',
          raison_sociale:'',
        },
      }, []);
    }
    this.props.UserStepStore.setAnimObj({clientHeight:0, scrollTop:0});

  }
  componentDidMount(): void {
    document.title = 'User Step | ' + RootStore.config.nameSystem;
  }

  clickOnStep=(step)=>{
    let FullData:any={};

    if(this.props.UserStepStore.dataClaculator.persone=='Personne morale'){
        FullData={...this.props.UserStepStore.dataClaculator};
    }else{
      //TODO
      FullData={...this.props.UserStepStore.dataClaculator};
    }
    let dataOperation=this.props.UserStepStore.dataOperation.map((item)=>{
      return {...item, operation:{...item.operation}, objUid:[...item.objUid]};
    });
    FullData.operation=dataOperation;
    let hasphys_data=FullData.beneficiaire.persone_phys_data.find((item)=>{
        return item.numero || item.reference?true:false;
    })

    if(this.config.Data[this.props.UserStepStore.currentStep].step==2 && this.config.Data[this.props.UserStepStore.currentStep].step<this.config.Data[step].step && FullData.persone=='Personne morale' && (FullData.information_lieu_travaux.operationavec=='Operation classique' || (FullData.information_lieu_travaux.operationavec=='QPV' && !FullData.information_lieu_travaux.adv_field.code_quartier))){
      let remove_oper=false;
      if(FullData.information_lieu_travaux.operationavec!='QPV'){
        remove_oper=true;
      }
      this.checkQpv(FullData).then((FullDataQpv)=>{
        if(FullDataQpv){
          this.props.UserStepStore.setVisibleConfirmModal({
              show:true,
              text:'Le dossier est reconnu et valorisé en QPV'+(remove_oper?' .Cette modification entraine l\'annulation des données precédement renseignées':''),
              title:'Confirm',
              YesTitle:'Oui',
              NonTitle:'Non',
              callbackYes:()=>{
                this.validate(FullDataQpv, (step-1));
                let error=this.props.UserStepStore.validateError;
                if(error.length){
                  //this.props.ToastrStore.error('Please check incoming data!', 'Error!');
                  this.setErrorStep(error, step);
                  return false;
                }
                this.props.UserStepStore.setCurrentStep(step);
                this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', FullDataQpv.information_lieu_travaux);
                if(remove_oper){
                  this.props.UserStepStore.updateDataOperation([]);
                }
              },
              callbackNo:()=>{

              }
            });
        }else{
          this.validate(FullData, (step-1));
          let error=this.props.UserStepStore.validateError;
          if(error.length){
          //  this.props.ToastrStore.error('Please check incoming data!', 'Error!');
            this.setErrorStep(error, step);
            return false;
          }
          this.props.UserStepStore.setCurrentStep(step);
        }
      });
    }else if(this.config.Data[this.props.UserStepStore.currentStep].step==1 && this.config.Data[this.props.UserStepStore.currentStep].step<this.config.Data[step].step && FullData.persone=='Personne physique' && !hasphys_data){
      this.props.UserStepStore.setVisibleConfirmModal({
          show:true,
          text:'Les données fiscales du bénéficiaire n’ont pas été renseignées, l’operation requiert un justificatif de domicile et n’enregistre aucune bonification',
          title:'Confirm',
          YesTitle:'Oui',
          NonTitle:'',
          callbackYes:()=>{
            this.validate(FullData, (step-1));
            let error=this.props.UserStepStore.validateError;
            if(error.length){
            //  this.props.ToastrStore.error('Please check incoming data!', 'Error!');
              this.setErrorStep(error, step);
              return false;
            }
            this.props.UserStepStore.setCurrentStep(step);
            FullData.beneficiaire.TableAflag=0;
            FullData.beneficiaire.TableBflag=0;
            FullData.beneficiaire.persone_phys_data=[];
            FullData.beneficiaire.number_de_perosne=null;
            FullData.beneficiaire.revenue=null;
            this.props.UserStepStore.updateCalculatorData('beneficiaire', FullData.beneficiaire);
          },
          callbackNo:()=>{

          }
        });
  }else{
      this.validate(FullData, (step-1));

      let error=this.props.UserStepStore.validateError;
      if(error.length){
      //  this.props.ToastrStore.error('Please check incoming data!', 'Error!');
        this.setErrorStep(error, step);
        return false;
      }

      this.props.UserStepStore.setCurrentStep(step);
    }


  };
  nextStep=(step, allStep)=>{
    let FullData:any={};

    if(this.props.UserStepStore.dataClaculator.persone=='Personne morale'){
        FullData={...this.props.UserStepStore.dataClaculator};
    }else{
      //TODO
        FullData={...this.props.UserStepStore.dataClaculator};
    }
    let dataOperation=this.props.UserStepStore.dataOperation.map((item)=>{
      return {...item, operation:{...item.operation}, objUid:[...item.objUid]};
    });
    FullData.operation=dataOperation;

    let hasphys_data=FullData.beneficiaire.persone_phys_data.find((item)=>{
        return item.numero || item.reference?true:false;
    })

    if(this.config.Data[this.props.UserStepStore.currentStep].step==2 &&  FullData.persone=='Personne morale' && (FullData.information_lieu_travaux.operationavec=='Operation classique' || (FullData.information_lieu_travaux.operationavec=='QPV' && !FullData.information_lieu_travaux.adv_field.code_quartier))){
      let remove_oper=false;
      if(FullData.information_lieu_travaux.operationavec!='QPV'){
        remove_oper=true;
      }
      this.checkQpv(FullData).then((FullDataQpv)=>{
        if(FullDataQpv){
        this.props.UserStepStore.setVisibleConfirmModal({
            show:true,
            text:'Le dossier est reconnu et valorisé en QPV'+(remove_oper?' .Cette modification entraine l\'annulation des données precédement renseignées':''),
            title:'Confirm',
            YesTitle:'Oui',
            NonTitle:'Non',
            callbackYes:()=>{
              this.validate(FullDataQpv, step);

              let error=this.props.UserStepStore.validateError;
              if(error.length){
                //let TextError=this.getErrorText(error);
              //  this.props.ToastrStore.error(TextError, 'Error!');
                this.setErrorStep(error, step);
                return false;
              }

              let newStep=step+1;
              newStep>(allStep-1)?allStep-1:newStep;
              this.props.UserStepStore.setCurrentStep(newStep);
              this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', FullDataQpv.information_lieu_travaux);
              if(remove_oper){
                this.props.UserStepStore.updateDataOperation([]);
              }
            },
            callbackNo:()=>{

            }
          });
        }else{
          this.validate(FullData, step);

          let error=this.props.UserStepStore.validateError;
          if(error.length){
          //  let TextError=this.getErrorText(error);
          //  this.props.ToastrStore.error(TextError, 'Error!');
            this.setErrorStep(error, step);
            return false;
          }

          let newStep=step+1;
          newStep>(allStep-1)?allStep-1:newStep;
          this.props.UserStepStore.setCurrentStep(newStep);
        }
      });
    }else if(this.config.Data[this.props.UserStepStore.currentStep].step==1 &&  FullData.persone=='Personne physique' && !hasphys_data){
      this.props.UserStepStore.setVisibleConfirmModal({
          show:true,
          text:'Les données fiscales du bénéficiaire n’ont pas été renseignées, l’operation requiert un justificatif de domicile et n’enregistre aucune bonification',
          title:'Confirm',
          YesTitle:'Oui',
          NonTitle:'',
          callbackYes:()=>{
            this.validate(FullData, step);

            let error=this.props.UserStepStore.validateError;
            if(error.length){
              //let TextError=this.getErrorText(error);
            //  this.props.ToastrStore.error(TextError, 'Error!');
              this.setErrorStep(error, step);
              return false;
            }

            let newStep=step+1;
            newStep>(allStep-1)?allStep-1:newStep;
            this.props.UserStepStore.setCurrentStep(newStep);
            FullData.beneficiaire.TableAflag=0;
            FullData.beneficiaire.TableBflag=0;
            FullData.beneficiaire.number_de_perosne=null;
            FullData.beneficiaire.revenue=null;
            FullData.beneficiaire.persone_phys_data=[];
            this.props.UserStepStore.updateCalculatorData('beneficiaire', FullData.beneficiaire);
          },
          callbackNo:()=>{

          }
        });
    }else{
      this.validate(FullData, step);

      let error=this.props.UserStepStore.validateError;
      if(error.length){
      //  let TextError=this.getErrorText(error);
      //  this.props.ToastrStore.error(TextError, 'Error!');
        this.setErrorStep(error, step);
        return false;
      }

      let newStep=step+1;
      newStep>(allStep-1)?allStep-1:newStep;
      this.props.UserStepStore.setCurrentStep(newStep);

    }

  }
  prevStep=(step, allStep)=>{
    let newStep=step-1;
    newStep<0?0:newStep;
    this.props.UserStepStore.setCurrentStep(newStep);
  }
  finish=()=>{
    let FullData:any={};

    if(this.props.UserStepStore.dataClaculator.persone=='Personne morale'){
        FullData={...this.props.UserStepStore.dataClaculator};
    }else{
      //TODO
      FullData={...this.props.UserStepStore.dataClaculator};
    }
    let dataOperation=this.props.UserStepStore.dataOperation.map((item)=>{
      return {...item, operation:{...item.operation}, objUid:[...item.objUid]};
    });
    FullData.operation=dataOperation;
    FullData.information_lieu_travaux.adv_field.adv_fiscal_data=FullData.information_lieu_travaux.adv_field.adv_fiscal_data.filter((item)=>{
      return !item.numero_fiscal && !item.reference_fiscal?false:true;
    })

    let step=this.config.Data.length-1;
    this.validate(FullData, step);

    let errors_data=this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    if(error.length){
    //  let text_error='Please check incoming data!';
      let max_day_diff_error= errors_data.find((error_data_item)=>{
        return error_data_item.type && error_data_item.type=='max_day_diff'?true:false;
      })
      if(max_day_diff_error && this.config.Data[0].step==0){
        this.props.UserStepStore.setCurrentStep(0);
      }
      if(max_day_diff_error){
          let max_day_diff=0, oper_name='';
          let DataOperation=this.props.UserStepStore.dataOperation;
          DataOperation.forEach((item)=>{
            if(item.day_between_date && (!max_day_diff || item.day_between_date>max_day_diff)){
              max_day_diff=item.day_between_date;
              oper_name=item.operation.name;
            }
          })
          let text_error='L’opération '+oper_name+' ne respecte pas la durée légale de '+max_day_diff+' jours entre la date de signature du devis et la date de début de travaux';
          this.props.ToastrStore.error(text_error, 'Error!');
      }


      return false;
    }


    this.config.finishFunc(FullData);
  }
  setErrorStep=(error_data, step)=>{
    let step_error=null;
    error_data.forEach((error_item)=>{
      if(error_item.step!==undefined && (!step_error ||(step_error && error_item.step<step_error))){
        step_error=error_item.step;
      }
    })
    if(step_error){
      let set_step_error=null;
      this.config.Data.forEach((config_data_item, i)=>{
        if(config_data_item.step==step_error){
          set_step_error=i;
        }
      })
      if(set_step_error){
        this.props.UserStepStore.setCurrentStep(set_step_error);
      }
    }
  }
  removeErrorNextStep=(step)=>{
    if(step<2){
      this.props.UserStepStore.removeValidateErrorArray(['nom_travaux', 'code_postale_travaux', 'siern_travaux', 'adresse_travaux', 'ville_travaux', 'parcelle_cadastrale','adv_field-code_quartier',
      'adv_field-raison_sociale', 'adv_field-siern', 'adv_field-nom', 'adv_field-prenom', 'adv_field-fonction', 'adv_field-phone',
      'adv_field-adresse', 'adv_field-code_postale', 'adv_field-ville', 'adv_field-nombre_total', 'adv_field-nombre_total_menages']);
    }
    if(step<1){
      let removeError=['siern_beneficiaire', 'beneficiaire-raison_sociale', 'beneficiaire-nom', 'previsite',
      'beneficiaire-prenom', 'beneficiaire-fonction', 'beneficiaire-adresse', 'beneficiaire-ville', 'beneficiaire-phone', 'beneficiaire-email',
      'beneficiaire-code_postale,', 'persone_phys_data'];
      let Data=this.props.UserStepStore.dataClaculator.beneficiaire.persone_phys_data;
      Data.forEach((item)=>{
          removeError.push('persone_phys_data_'+item.id)
      })
      this.props.UserStepStore.removeValidateErrorArray(removeError);
    }
    if(step<3){
      this.props.UserStepStore.removeValidateErrorArray(['sous-siret', 'sous-nom', 'sous-prenom', 'sous-raison_sociale']);
    }
    if(step<4){
      let removeError=[];
      let Data=this.props.UserStepStore.dataOperation;
      Data.forEach((item)=>{
        removeError.push(item.id+'_previsite');
        removeError.push(item.id+'_isolants');
        removeError.push(item.id+'_oper_price');
        if(item.isolant && item.isolant.length){
          item.isolant.forEach((itemIsolant)=>{
            removeError.push(itemIsolant.id+'_surface');
            if(itemIsolant.advParams){
              let keys= Object.keys(itemIsolant.advParams);
              keys.forEach((key)=>{
                removeError.push('adv_param_isolant_'+itemIsolant.id+'_'+key);
              })
            }
          })
        }
      });
      if(removeError.length){
        this.props.UserStepStore.removeValidateErrorArray(removeError);
      }
    }
  }
  validate=(FullData, step)=>{
    step=step<0?0:step;
    step=this.config.Data[step].step;
    this.removeErrorNextStep(step-1);
    let errors=[], removeErrors=[];
    if(step>=0){
      if(!FullData.date.date_de_signature){
        errors.push({id:'date_de_signature', step:0});
      }else if(FullData.date.date_de_signature && moment(FullData.date.date_de_signature, DateFormat).toDate()>new Date()){
        errors.push({id:'date_de_signature', id_msg:'error_future_date', step:0});
      }else{
        removeErrors.push('date_de_signature');
      }

      /*if(this.config.showDate_de_debut && !FullData.date.date_de_debut){
        errors.push('date_de_debut');
      }else if(this.config.showDate_de_debut && FullData.date.date_de_debut && FullData.date.date_de_signature && moment(FullData.date.date_de_signature, DateFormat).toDate()>moment(FullData.date.date_de_debut, DateFormat).toDate()){
        errors.push('date_de_debut');
      }else{
        removeErrors.push('date_de_debut');
      }*/

      if(this.config.showDate_de_finDeTravaux && !FullData.date.finDeTravaux){
        errors.push({id:'finDeTravaux', step:0});
      }else if(this.config.showDate_de_finDeTravaux && FullData.date.finDeTravaux && moment(FullData.date.finDeTravaux, DateFormat).toDate()>new Date()){
          errors.push({id:'finDeTravaux', id_msg:'error_future_date', step:0});
      }else if(this.config.showDate_de_finDeTravaux && FullData.date.finDeTravaux && FullData.date.date_de_debut && moment(FullData.date.date_de_debut, DateFormat).toDate()>moment(FullData.date.finDeTravaux, DateFormat).toDate()){
        errors.push({id:'finDeTravaux', step:0});
      }else if(this.config.showDate_de_finDeTravaux && FullData.date.finDeTravaux && FullData.date.date_facture && moment(FullData.date.finDeTravaux, DateFormat).toDate()>moment(FullData.date.date_facture, DateFormat).toDate()){
        errors.push({id:'finDeTravaux', step:0});
      }else{
        removeErrors.push('finDeTravaux');
      }

      if(FullData.date.date_facture && moment(FullData.date.date_facture, DateFormat).toDate()>new Date()){
          errors.push({id:'date_facture', id_msg:'error_future_date', step:0});
      }else if(FullData.date.date_de_signature && FullData.date.date_facture && moment(FullData.date.date_de_signature, DateFormat).toDate()>moment(FullData.date.date_facture, DateFormat).toDate()){
        errors.push({id:'date_facture', step:0});
      }else if(this.config.showDate_de_debut && FullData.date.date_de_debut && FullData.date.date_facture && moment(FullData.date.date_de_debut, DateFormat).toDate()>moment(FullData.date.date_facture, DateFormat).toDate()){
        errors.push({id:'date_facture', step:0});
      }else{
        removeErrors.push('date_facture');
      }

    }
    if(step>=2){

      if(!FullData.information_lieu_travaux.code_postale_travaux){
        errors.push({id:'code_postale_travaux', step:2});
      }else if(FullData.information_lieu_travaux.code_postale_travaux.toString().length!==5){
        errors.push({id:'code_postale_travaux', step:2});
      }else{
        removeErrors.push('code_postale_travaux');
      }

      if(!FullData.information_lieu_travaux.adresse_travaux){
        errors.push({id:'adresse_travaux', step:2});
      }else{
        removeErrors.push('adresse_travaux');
      }

      if(!FullData.information_lieu_travaux.ville_travaux){
        errors.push({id:'ville_travaux', step:2});
      }else{
        removeErrors.push('ville_travaux');
      }

      if(FullData.persone==='Personne morale'){

        if(!FullData.information_lieu_travaux.nom_travaux){
          errors.push({id:'nom_travaux', step:2});
        }else{
          removeErrors.push('nom_travaux');
        }

        if(FullData.information_lieu_travaux.siern_travaux && FullData.information_lieu_travaux.siern_travaux.toString().replace(/ /g, '').length!=9){
            errors.push({id:'siern_travaux', step:2});
          }else{
            removeErrors.push('siern_travaux');
          }

        if(FullData.information_lieu_travaux.operationavec=='QPV'){
          if(!FullData.information_lieu_travaux.adv_field.code_quartier){
            errors.push({id:'adv_field-code_quartier', step:2});
          }else{
            removeErrors.push('adv_field-code_quartier');
          }
        }
        if(FullData.information_lieu_travaux.operationavec=='Bailleur Social'){
          if(!FullData.information_lieu_travaux.adv_field.raison_sociale){
            errors.push({id:'adv_field-raison_sociale', step:2});
          }else{
            removeErrors.push('adv_field-raison_sociale');
          }
          if(!FullData.information_lieu_travaux.adv_field.siern){
            errors.push({id:'adv_field-siern', step:2});
          }else if(FullData.information_lieu_travaux.adv_field.siern.toString().replace(/ /g, '').length!=9){
            errors.push({id:'adv_field-siern', step:2});
          }else{
            removeErrors.push('adv_field-siern');
          }
          if(!FullData.information_lieu_travaux.adv_field.nom){
            errors.push({id:'adv_field-nom', step:2});
          }else{
            removeErrors.push('adv_field-nom');
          }
          if(!FullData.information_lieu_travaux.adv_field.prenom){
            errors.push({id:'adv_field-prenom', step:2});
          }else{
            removeErrors.push('adv_field-prenom');
          }
          if(!FullData.information_lieu_travaux.adv_field.fonction){
            errors.push({id:'adv_field-fonction', step:2});
          }else{
            removeErrors.push('adv_field-fonction');
          }
          if(!FullData.information_lieu_travaux.adv_field.phone || (FullData.information_lieu_travaux.adv_field.phone && FullData.information_lieu_travaux.adv_field.phone.toString().length!=10)){
            errors.push({id:'adv_field-phone', step:2});
          }else{
            removeErrors.push('adv_field-phone');
          }
          if(!FullData.information_lieu_travaux.adv_field.adresse){
            errors.push({id:'adv_field-adresse', step:2});
          }else{
            removeErrors.push('adv_field-adresse');
          }
          if(!FullData.information_lieu_travaux.adv_field.code_postale){
            errors.push({id:'adv_field-code_postale', step:2});
          }else{
            removeErrors.push('adv_field-code_postale');
          }
          if(!FullData.information_lieu_travaux.adv_field.ville){
            errors.push({id:'adv_field-ville', step:2});
          }else{
            removeErrors.push('adv_field-ville');
          }
          if(!FullData.information_lieu_travaux.adv_field.nombre_total){
            errors.push({id:'adv_field-nombre_total', step:2});
          }else{
            removeErrors.push('adv_field-nombre_total');
          }
          if(!FullData.information_lieu_travaux.adv_field.nombre_total_menages){
            errors.push({id:'adv_field-nombre_total_menages', step:2});
          }else{
            removeErrors.push('adv_field-nombre_total_menages');
          }
        }
      }else{
        removeErrors.push('siern_travaux');
        removeErrors.push('nom_travaux');
        removeErrors.push('adv_field-code_quartier');
        removeErrors.push('adv_field-raison_sociale');
        removeErrors.push('adv_field-siern');
        removeErrors.push('adv_field-nom');
        removeErrors.push('adv_field-prenom');
        removeErrors.push('adv_field-fonction');
        removeErrors.push('adv_field-phone');
        removeErrors.push('adv_field-adresse');
        removeErrors.push('adv_field-code_postale');
        removeErrors.push('adv_field-ville');
        removeErrors.push('adv_field-nombre_total');
        removeErrors.push('adv_field-nombre_total_menages');
      }
      if(FullData.persone==='Personne physique'){
        if(!FullData.information_lieu_travaux.parcelle_cadastrale){
          errors.push({id:'parcelle_cadastrale', step:2});
        }else{
          removeErrors.push('parcelle_cadastrale');
        }
      }else{
        removeErrors.push('parcelle_cadastrale');
      }

    }
    if(step>=1){
      // beneficiaire
      if(!FullData.beneficiaire.previsite){
          errors.push({id:'previsite', step:1});
      }else if(FullData.beneficiaire.previsite && moment(FullData.beneficiaire.previsite, DateFormat).toDate()>new Date()){
          errors.push({id:'previsite', id_msg:'error_future_date', step:1});
      }else if(FullData.beneficiaire.previsite && FullData.date.date_de_signature &&  moment(FullData.beneficiaire.previsite, DateFormat).toDate()>moment(FullData.date.date_de_signature, DateFormat).toDate()){
          errors.push({id:'previsite', step:1});
      }else{
        removeErrors.push('previsite');
      }

      if(!FullData.beneficiaire.nom){
        errors.push({id:'beneficiaire-nom', step:1});
      }else{
        removeErrors.push('beneficiaire-nom');
      }
      if(!FullData.beneficiaire.prenom){
        errors.push({id:'beneficiaire-prenom', step:1});
      }else{
        removeErrors.push('beneficiaire-prenom');
      }
      if(!FullData.beneficiaire.adresse){
        errors.push({id:'beneficiaire-adresse', step:1});
      }else{
        removeErrors.push('beneficiaire-adresse');
      }
      if(!FullData.beneficiaire.ville){
        errors.push({id:'beneficiaire-ville', step:1});
      }else{
        removeErrors.push('beneficiaire-ville');
      }

      if(!FullData.beneficiaire.phone){
        errors.push({id:'beneficiaire-phone', step:1});
      }else if(FullData.beneficiaire.phone && FullData.beneficiaire.phone.toString().length!=10){
          errors.push({id:'beneficiaire-phone', step:1});
      }else{
          removeErrors.push('beneficiaire-phone');
      }
      const validateEmail= function(email) {
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
      }
      if(!FullData.beneficiaire.email){
        errors.push({id:'beneficiaire-email', step:1});
      }else if(FullData.beneficiaire.email && !validateEmail(FullData.beneficiaire.email) && FullData.beneficiaire.email!='néant'){
        errors.push({id:'beneficiaire-email', step:1});
      }else{
        removeErrors.push('beneficiaire-email');
      }
      if(!FullData.beneficiaire.code_postale){
        errors.push({id:'beneficiaire-code_postale', step:1});
      }else if(FullData.beneficiaire.code_postale && FullData.beneficiaire.code_postale.toString().length!==5){
        errors.push({id:'beneficiaire-code_postale', step:1});
      }else{
        removeErrors.push('beneficiaire-code_postale');
      }
      //end beneficiaire

      if(FullData.persone==='Personne morale'){
        if(!FullData.beneficiaire.fonction){
          errors.push({id:'beneficiaire-fonction', step:1});
        }else{
          removeErrors.push('beneficiaire-fonction');
        }
        if(!FullData.beneficiaire.siern){
          errors.push({id:'siern_beneficiaire', step:1});
        }else if(FullData.beneficiaire.siern.toString().replace(/ /g, '').length!=9){
          errors.push({id:'siern_beneficiaire', step:1});
        }else{
          removeErrors.push('siern_beneficiaire');
        }
        if(!FullData.beneficiaire.raison_sociale){
          errors.push({id:'beneficiaire-raison_sociale', step:1});
        }else{
          removeErrors.push('beneficiaire-raison_sociale');
        }
      }else{
        removeErrors.push('beneficiaire-fonction');
        removeErrors.push('siern_beneficiaire');
        removeErrors.push('beneficiaire-raison_sociale');
      }

      if(FullData.persone==='Personne physique'){
      /*  if(!FullData.beneficiaire.persone_phys_data.length){
          errors.push('persone_phys_data');
        }else{
          removeErrors.push('persone_phys_data');
        }*/
        let yer_previsite=Number(moment(FullData.beneficiaire.previsite, DateFormat).year());
        FullData.beneficiaire.persone_phys_data.forEach((item)=>{
          if((item.numero || item.reference) && (!item['JSON'] || (yer_previsite && item.year && Number(item.year)<(yer_previsite-1)))){
            errors.push({id:'persone_phys_data_'+item.id, step:1});
          }else{
            removeErrors.push('persone_phys_data_'+item.id);
          }
        });
      }else{
        FullData.beneficiaire.persone_phys_data.forEach((item)=>{
          removeErrors.push('persone_phys_data_'+item.id);
        });
        removeErrors.push('persone_phys_data');
        removeErrors.push('adress_valid_error');
      }
    }
    if(step>=3){
      if(FullData.sous_traitance.sous_traitance==1){
        if(!FullData.sous_traitance.siret){
          errors.push({id:'sous-siret', step:3});
        }else if(FullData.sous_traitance.siret.toString().replace(/ /g, '').length!=14){
          errors.push({id:'sous-siret', step:3});
        }else if(FullData.sous_traitance.siret_error){
          errors.push({id:'sous-siret', id_msg:'sous-siret_expired', step:3});
        }else{
          removeErrors.push('sous-siret');
        }
        if(!FullData.sous_traitance.nom){
          errors.push({id:'sous-nom', step:3});
        }else{
          removeErrors.push('sous-nom');
        }
        if(!FullData.sous_traitance.prenom){
          errors.push({id:'sous-prenom', step:3});
        }else{
          removeErrors.push('sous-prenom');
        }
        if(!FullData.sous_traitance.raison_sociale){
          errors.push({id:'sous-raison_sociale', step:3});
        }else{
          removeErrors.push('sous-raison_sociale');
        }
      }else{
        removeErrors.push('sous-siret');
        removeErrors.push('sous-nom');
        removeErrors.push('sous-prenom');
        removeErrors.push('sous-raison_sociale');
      }

    }
    if(step>=4) {
      let DataDateForm=this.props.UserStepStore.dataClaculator.date;
      let Data=this.props.UserStepStore.dataOperation;
      let max_day_diff=0;
      Data.forEach((item)=>{
        if(item.isolant && item.isolant.length){
          let opt_adv_size=item.options_adv_size.map((item)=>{
            return item.uid;
          });
          item.isolant.forEach((itemIsolant)=>{
            if(itemIsolant && (!itemIsolant.surface || itemIsolant.surface==0)){
                errors.push({id:itemIsolant.id+'_surface', step:4});
            }else{
              if(itemIsolant.adv_size && itemIsolant.adv_size.length){

                let no_d=opt_adv_size.indexOf('5c312056-099e-4ac6-9a5f-2954b797ab96')!=-1?true:false;
                let no_ep=opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!=-1?true:false;

                let has_empty_val = itemIsolant.adv_size.find((adv_size_item)=>{
                    return !adv_size_item.l || (!no_d && !adv_size_item.d) || (!no_ep && !adv_size_item.ep) ? true:false;
                });

                let check_d=opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!=-1?true:false;
                if(!has_empty_val && check_d){
                  has_empty_val=itemIsolant.adv_size.find((adv_size_item)=>{
                      return adv_size_item.d<20?true:false;
                  });
                }


                if(has_empty_val){
                  errors.push({id:itemIsolant.id+'_surface', step:4});
                }else{
                  removeErrors.push(itemIsolant.id+'_surface');
                }

              }else{
                removeErrors.push(itemIsolant.id+'_surface');
              }
            }

            if(itemIsolant && itemIsolant.advParams){
              let keys= Object.keys(itemIsolant.advParams);
              keys.forEach((key)=>{
                if(!itemIsolant.advParams[key].value){
                  errors.push({id:'adv_param_isolant_'+itemIsolant.id+'_'+key, step:4});
                }else{
                  removeErrors.push('adv_param_isolant_'+itemIsolant.id+'_'+key);
                }
              })
            }
          })
          removeErrors.push(item.id+'_isolants');
        }else {
          errors.push({id:item.id+'_isolants', step:4});
        }
        if(item.day_between_date && (!max_day_diff || item.day_between_date>max_day_diff)){
          max_day_diff=item.day_between_date;
        }

        if(item.operation && item.operation.min_price && Number(item.cost_client)<Number(item.operation.min_price)){
          errors.push({id:item.id+'_oper_price', step:4});
        }else{
          removeErrors.push(item.id+'_oper_price');
        }

        if(item.operation.oper_options.length && item.operation.oper_options.indexOf('Bureau de controle')!==-1){
            if(this.config.oper_adv_column.indexOf('bureaudecontrole')!==-1 && !item.referencerapport){
              errors.push({id:item.id+'_referencerapport', step:4});
            }else{
              removeErrors.push(item.id+'_referencerapport');
            }
            if(this.config.oper_adv_column.indexOf('bureaudecontrole')!==-1 && !item.bureaudecontrole || item.bureaudecontrole==-1){
              errors.push({id:item.id+'_bureaudecontrole', step:4});
            }else{
              removeErrors.push(item.id+'_bureaudecontrole');
            }
        }else{
          removeErrors.push(item.id+'_referencerapport');
          removeErrors.push(item.id+'_bureaudecontrole');
        }

      });

      if(max_day_diff){
        if(this.config.showDate_de_debut && !FullData.date.date_de_debut){
          errors.push({id:'date_de_debut', step:4});
        }else if(this.config.showDate_de_debut && FullData.date.date_de_debut && moment(FullData.date.date_de_debut, DateFormat).toDate()>new Date()){
            errors.push({id:'date_de_debut', id_msg:'error_future_date', step:4});
        }else if(this.config.showDate_de_debut && FullData.date.date_de_debut && FullData.date.date_de_signature && moment(FullData.date.date_de_signature, DateFormat).toDate()>moment(FullData.date.date_de_debut, DateFormat).toDate()){
          errors.push({id:'date_de_debut', step:4});
        }else if(this.config.showDate_de_debut && FullData.date.date_de_debut && FullData.date.date_de_signature && moment(FullData.date.date_de_debut, DateFormat).toDate()>=moment(FullData.date.date_de_signature, DateFormat).toDate() && moment(FullData.date.date_de_debut, DateFormat).diff(moment(FullData.date.date_de_signature, DateFormat), 'days')<max_day_diff){
          errors.push({id:'date_de_debut', type:'max_day_diff', step:4});
        }else{
          removeErrors.push('date_de_debut');
        }
      }

    }
    if(errors.length){
      this.props.UserStepStore.addValidateErrorArray(errors);
    }
    if(removeErrors.length){
      this.props.UserStepStore.removeValidateErrorArray(removeErrors);
    }
    this.removeErrorNextStep(step);
  }
  getContent(curStep){
    let step=this.config.Data[curStep].step;
    switch (step) {
      case 0:
        return <DateForm blockOrganization={this.config.blockOrganization} showOrganization={this.config.Data[curStep].showOrganization}/>
      case 1:
        return <BeneficiaireForm/>
      case 2:
        return <InformationForm/>
      case 3:
        return <SousTraitanceForm/>
      case 4:
        return <OperationsForm config={this.config} configData={this.config.Data[curStep]}/>
      default:
        break;
    }
  }
  getFooter=(step, allStep)=>{
    return(
      <div className={'footer_cont '+(step===(allStep-1)?'fixed':'')}>
           <div className="footer_btn">
             <div className="footer_right-btn">
              {step!==0 && <button onClick={()=>{this.prevStep(step, allStep)}} className="btn-user-step-retour">← <span>Retour</span></button>}
              {step==0 && this.config.cancelBtn && <button onClick={this.config.cancelBtn} className="btn-user-step-retour">← <span>Retour</span></button>}
             </div>
             <div className="footer_left-btn">
               {step!==(allStep-1) && <button onClick={()=>{this.nextStep(step, allStep)}} className="btn-user-step-next">Enregistrer et suivant →</button>}
               {step===(allStep-1) && <button onClick={()=>{this.finish()}} className="btn-user-step-next">{this.config.finishLabel}</button>}
             </div>
          </div>
     </div>
    )
  }
  addClick=()=>{
    if(this.props.config.oper_action.indexOf('add')==-1){
      return false;
    }
    this.props.UserStepStore.setOperationModal(true);
  }
  getFixedFooter=(step, allStep)=>{
    let DataOperation=this.props.UserStepStore.dataOperation;
    let totalSumm=0, totalSummClient=0, totalSummPro=0, totalSummCharge=0;
    DataOperation.forEach((item)=>{
      totalSumm+=Number(item.cost);
      totalSummClient+=Number(item.cost_client);
      totalSummPro+=Number(item.cost_pro);
      totalSummCharge+=Number(item.reste_a_charge);
    })
    return(
      <div className={'footer_cont fixed'}>
           <div className="footer_btn">
             <div className="footer_left-btn">
              {step!==0 && <button onClick={()=>{this.prevStep(step, allStep)}} className="btn-user-step-retour">← <span>Retour</span></button>}
              {step==0 && this.config.cancelBtn && <button onClick={this.config.cancelBtn} className="btn-user-step-retour">← <span>Retour</span></button>}
             </div>
             <div className="footer_center-btn">
                 <div className="sum_total_item add_btn_cont"><button style={(this.props.config.oper_action.indexOf('add')==-1?{display:'none'}:{})} type="button" onClick={this.addClick} disabled={(this.props.config.oper_action.indexOf('add')==-1?true:false)}  className={'btn btn-add-new-oper'+(this.props.config.oper_action.indexOf('add')==-1?' disabled':'')}><Icon name={'plus'} size={12} color={'#fff'} /> Ajouter une opération </button></div>
                 <div className="sum_total_item">{'€ '+numberWithSpaces(Number(totalSummClient).toFixed(2))}<div className="sum_label">Prime Client</div></div>
                 <div className="sum_total_item">{'€ '+numberWithSpaces(Number(totalSummPro).toFixed(2))}<div className="sum_label">Prime Pro</div></div>
                 <div className="sum_total_item">{'€ '+numberWithSpaces(Number(totalSumm).toFixed(2))}<div className="sum_label">Prime Totale</div></div>
                 <div className="sum_total_item">{'€ '+numberWithSpaces(Number(totalSummCharge).toFixed(2))}<div className="sum_label">Reste à Charge</div></div>
             </div>
             <div className="footer_right-btn">
               {step===(allStep-1) && <button onClick={()=>{this.finish()}} className="btn-user-step-next">{this.config.finishLabel}</button>}
             </div>
          </div>
          <div className="footer_mobile">
             <button style={(this.props.config.oper_action.indexOf('add')==-1?{display:'none'}:{})} type="button" onClick={this.addClick} disabled={(this.props.config.oper_action.indexOf('add')==-1?true:false)}  className={'btn btn-add-new-oper'+(this.props.config.oper_action.indexOf('add')==-1?' disabled':'')}><Icon name={'plus'} size={12} color={'#fff'} /> Ajouter une opération </button>
             {step===(allStep-1) && <button onClick={()=>{this.finish()}} className="btn-user-step-next">{this.config.finishLabel}</button>}
          </div>
     </div>
    )
  }

  checkQpv=(FullData)=>{
    let address= FullData.information_lieu_travaux.adresse_travaux+' '+FullData.information_lieu_travaux.code_postale_travaux+' '+FullData.information_lieu_travaux.ville_travaux;
    let digit_part= address.substr(0, address.indexOf(' '));
    let digit_part_arr=digit_part.split(/\,|-/gi);
    if(digit_part_arr.length){
      address=digit_part_arr[digit_part_arr.length-1]+address.substr(address.indexOf(' '));
    }
    return axios.get(
        '/Dop/territoryCode',
        {
            params: {address:address},
        }
    ).then((res)=>{
      let Data=JSON.parse(JSON.stringify(FullData))
      if(res.data.result==1 && res.data.data.code.code){
        Data.information_lieu_travaux.operationavec='QPV';
        Data.information_lieu_travaux.adv_field.code_quartier=res.data.data.code.code;
        return Data;
      }
      return false;
    })
  }

  getErrorText=(error)=>{
    if(error.indexOf('beneficiaire-email')!=-1){
      return 'Indiquer : "néant" si le bénéficiaire ne dispose pas d\'une adresse de courriel';
    }
    return 'Please check incoming data!';
  }

  checkboxchange=(e)=>{
    let {id, checked, name}=e.currentTarget;
    let Data=this.props.UserStepStore.dataClaculator[id];
    let key = id || name;
    Data={...Data, [key]:Number(checked)};
    if(!checked){
      Data.siret='';
      Data.nom='';
      Data.prenom='';
      Data.raison_sociale='';
    }
    this.props.UserStepStore.updateCalculatorData(id, Data);
  }


  renderMainTitle=(Data, step, allStep)=>{
    let title='', isShow=false, tags=[];
    if(Data.persone=='Personne physique'){
      title=Data.beneficiaire.nom+' '+Data.beneficiaire.prenom;
      if(Data.beneficiaire.TableBflag==1){
         tags.push(<div key={'operation_tags'} className="main_tag_item">Operation <span className="bold">Précaire</span></div>);
      }else if(Data.beneficiaire.TableAflag==1){
        tags.push(<div key={'operation_tags'} className="main_tag_item">Operation <span className="bold">Grand Précaire</span></div>);
      }else{
        tags.push(<div key={'operation_tags'} className="main_tag_item">Operation <span className="bold">Classique</span></div>);
      }
      if(Data.beneficiaire.revenue || Data.beneficiaire.revenue===0){
        tags.push(<div  key={'revenue_tags'} className="main_tag_item">Revenu fiscal de reference: <span className="bold">{numberWithSpaces(Data.beneficiaire.revenue)+' €'}</span></div>);
      }
      if(Data.beneficiaire.number_de_perosne){
        tags.push(<div  key={'number_de_perosne_tags'} className="main_tag_item"><i className="fas fa-users"></i> {Data.beneficiaire.number_de_perosne}</div>);
      }
    }else if(Data.persone=='Personne morale'){
      title=Data.information_lieu_travaux.nom_travaux;
      if(Data.information_lieu_travaux.operationavec){
        tags.push(<div  key={'operation_tags'} className="main_tag_item">{Data.information_lieu_travaux.operationavec}</div>);
      }
    }
    if(title.trim()){
      isShow=true;
    }
    return(<div className={'form_main_title '+(isShow?'show':'')+' '+(!this.config.showStepper?'no_stepper':'')}>

        <div className="form_main_title_cont">
              <div className="main_tags_conteiner"><div className="main_tag_item_title">{title}</div>{tags}</div>
        </div>
      </div>)
  }

  render(){
    let LoaderBlockUI = <Loader active type="ball-pulse"/>;
    let Data=this.props.UserStepStore.dataClaculator;
    let step=this.props.UserStepStore.currentStep;
    if(this.props.config){
      this.config=this.props.config;
    }

    return(
        <React.Fragment>
        <BlockUi id="zt_ViewPage_step_form" tag="div" loader={<EnematLoader/>} blocking={this.state.blocking}>
          <div className="userstep_form">
            {this.config.showStepper && <Steper Step={step} config={this.config} allStep={this.config.Data.length-1} prevStep={this.prevStep} Data={this.config.Data} clickHandler={this.clickOnStep} />}
            {this.renderMainTitle(Data, step, this.config.Data.length)}
            <div className="step_form_body">
              <div className="form_cont">{this.getContent.call(this, step)}</div>
              {step!==(this.config.Data.length-1) && this.getFooter(step, this.config.Data.length)}
            </div>
            {step==(this.config.Data.length-1) && this.getFixedFooter(step, this.config.Data.length)}
          </div>
          </BlockUi>
        </React.Fragment>
    );
  }
}

interface ISteperProps {
  Step:number,
  allStep:number,
  Data:{title:string}[],
  clickHandler:(index)=>void,
  prevStep:(step, allStep)=>void,
  config:any,
}
class Steper extends React.Component<ISteperProps> {

  renderItem(Data, index, isActive){
    return(<div onClick={()=>{this.props.clickHandler(index)}} key={'step_'+index} className={'mt-step-col'+(isActive?' active':'')}>
            <div className="step-number">{(index+1)}</div>
            <div className={'mt-step-content '+(index<this.props.Step?'step-checked':'')}><span className="text">{Data.title}</span><Icon name="check" color="#AFE05E" size={12}/></div>
      </div>)
  }
  renderMobileItem=(Data, index, isActive)=>{
    return(<div onClick={()=>{this.props.clickHandler(index)}} key={'step_'+index} className={'mt-step-col'+(isActive?' active':'')}>
            <div className={'mt-step-content-mobile '+(index<this.props.Step?'step-checked':'')}><span className="text">{(index+1)+'. '+Data.title}</span><Icon name="check" color="#AFE05E" size={12}/></div>
      </div>)
  }
  render(){
      let Items= this.props.Data.map((item, index)=>{
        let isActive=index===this.props.Step?true:false;
        return this.renderItem(item, index, isActive);
      })

      let Items_mobile=this.props.Data.map((item, index)=>{
        let isActive=index===this.props.Step?true:false;
        return this.renderMobileItem(item, index, isActive);
      })
      let Items_odd=Items_mobile.map((item, index)=>{
        return (index+1)%2!=0?item:<div className="mt-step-col"></div>;
      })
      let Items_even=Items_mobile.map((item, index)=>{
          return (index+1)%2==0?item:<div className="mt-step-col"></div>;
      })
      if(Items_odd.length!==Items_even.length){
        Items_even.push(<div className="mt-step-col"></div>);
      }
      let fill_prcnt=(this.props.Step+1)/Items.length*100;
      return(<div className="mt-element-step">
          <div className="mt-el-step-items">
          {Items}
          </div>
          <div className="form_main_btn_back">
            {this.props.Step!==0 && <button onClick={()=>{this.props.prevStep(this.props.Step, this.props.allStep)}} className="btn"><Icon name="arrow_left" size={16} color={'#0069A5'}/></button>}
            {this.props.Step==0 && this.props.config.cancelBtn && <button onClick={this.props.config.cancelBtn} className="btn"><Icon name="arrow_left" size={16} color={'#0069A5'}/></button>}
          </div>
          <div className="m-step-progress-cont">
            <div className="m-el-step-odd">{Items_odd}</div>
            <div className="m-step-progress"><div style={{width:fill_prcnt+'%'}} className="m-step-fill"></div></div>
            <div className="m-el-step-even">{Items_even}</div>
          </div>
      </div>)
  }
}

interface IOperationsFormProps {
  UserStepStore?:any,
  ToastrStore?:any,
  config:any,
  configData:any,
}
interface IOperationsFormState {
  isolantOpt:any,
  berueOpt:any[],
  fullIsolantOpt:any[],
  isolant_attr_sett:any,
  animation:{
    opacity:number,
    height:any,
  },
  edit_oper_id:string,
  remove_pop_open:string[],
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class OperationsForm extends React.Component<IOperationsFormProps, IOperationsFormState>{
  private anim_elem : React.RefObject<HTMLDivElement>;
  constructor(props){
      super(props);
      this.state={
        isolantOpt:{},
        berueOpt:[],
        fullIsolantOpt:[],
        isolant_attr_sett:null,
        animation:{
          opacity:0,
          height:'auto',
        },
        edit_oper_id:null,
        remove_pop_open:[],
      }
      this.anim_elem=React.createRef();
    }


  validate=(Data, id)=>{
    /*  let DataDateForm=this.props.UserStepStore.dataClaculator.date;
      var errors=[], removeError=[];
      Data.forEach((item)=>{
        if(item && item.previsite && moment(item.previsite, DateFormat).toDate()>new Date()){
          errors.push(item.id+'_previsite');
        }else if(item && item.previsite && DataDateForm && DataDateForm.date_de_signature && moment(item.previsite, DateFormat).toDate()>moment(DataDateForm.date_de_signature, DateFormat).toDate()){
            errors.push(item.id+'_previsite');
        }else{
          removeError.push(item.id+'_previsite');
        }
      });
      if(errors.length){
        this.props.UserStepStore.addValidateErrorArray(errors);
      }
      if(removeError.length){
        this.props.UserStepStore.removeValidateErrorArray(removeError);
      }*/
  }

  componentDidMount(){
    this.props.UserStepStore.setOperationModal(false);
    this.animation();
    let classuidBerue='536eebe6-a866-4805-9ec3-0df30fcaa649';
    let parentuid='00000000-0000-0000-0000-000000000000'
    ct_apiobj.getChildrenByType(parentuid, classuidBerue).then((res)=>{
        if(res.data.result===1){
          this.setState({berueOpt:res.data.data});
        }
    })


  }

  componentDidUpdate(prevProps){
    let exist_isolat_data= this.state.isolantOpt;
    const get_filter_isolant= function(Operations){
      let filter_obj= {
        attr:'6b0a469c-64d7-410e-a045-e2fd2afd96f6',
        op:'ANY',
        v:[],
      };
      filter_obj.v=Operations.filter((item)=>{
        return exist_isolat_data[item.operation.uid]?false:true;
      }).map((item)=>{
        return item.operation.name;
      });
      return filter_obj.v.length?[filter_obj]:null;
    };
    let filter_data= get_filter_isolant(this.props.UserStepStore.dataOperation);
    if(filter_data && filter_data.length && filter_data[0].v.length){
      let data_operation=this.props.UserStepStore.dataOperation.filter((item)=>{
        return exist_isolat_data[item.operation.uid]?false:true;
      }).map((item)=>{
        return item.operation.uid;
      });
      this.loadIsolant_data(data_operation, filter_data);
    }
  }

  componentWillUnmount(){
    this.props.UserStepStore.setAnimObj({clientHeight:this.anim_elem.current.clientHeight, scrollTop:document.getElementsByClassName('step_form_body')[0].scrollTop});
  }

  animation=()=>{
    let anim_height=this.anim_elem.current.scrollHeight;
    let animation=this.props.UserStepStore.animation;
    return new Promise((resolve, reject)=>{
      this.setState({animation:{...this.state.animation, height:animation.clientHeight}}, ()=>{
        //document.getElementsByClassName('step_form_body')[0].scrollTop=animation.scrollTop;
        setTimeout(()=>{
          this.setState({animation:{...this.state.animation, opacity:1, height:anim_height}}, ()=>{
            document.getElementsByClassName('userstep_form')[0].scrollTo({behavior: "smooth", top:0});
            setTimeout(()=>{
              this.setState({animation:{...this.state.animation, height:'auto'}}, ()=>{
                resolve(true);
              });
            }, 700);
          });
        }, 100)
      });
    })
  }



  loadIsolant_data=(operation_uid, filter_data)=>{
    let view_uid='5eadb3b0-7c44-4403-807a-45a845de70ea';
    let param_scenario_uid='6b0a469c-64d7-410e-a045-e2fd2afd96f6';
    let param_name_uid='95cf9bce-3782-4b4c-bb4d-bb98a3708e58';
    let param_marque_uid='202fd14c-ea8e-4043-aa5c-a616641b7bae';

    let data=this.state.isolantOpt;
    operation_uid.forEach((oper_uid)=>{
      data[oper_uid]=[];
    });

    const get_isolant_obj= (item, param_isolant_data)=>{


      let marq=(item[param_marque_uid] && item[param_marque_uid].ln?item[param_marque_uid].ln:'');
      let name=(item[param_name_uid] && item[param_name_uid].v?item[param_name_uid].v:'');

      let adv_params={};
      param_isolant_data.forEach((param)=>{
        if(param.name.indexOf('Attribute')!==-1){
          let uid= param.uid;
          adv_params[param.name]={title:param.name, value:(item[uid] && item[uid].v?item[uid].v:''), type:param.datatype};
        }
      })

      let label=marq+' '+name;
      let uid=item[param_name_uid] && item[param_name_uid].o?item[param_name_uid].o:'';

      return {
        name:label,
        uid:uid,
        ad_size:[],
        advParams:{
          'Marque':{title:'Marque', value:marq},
          'Reference':{title:'Reference', value:name},
          ...adv_params
        },
      };
    };
    const loadData=()=>{
      let param_isolant_data=this.state.isolant_attr_sett;
      ct_apiview.getTableData({
          view: view_uid,
          attr_sort: '',
          sort: 1,
          start: 1,
          num: 100000,
          attr_filter:filter_data,
      }).then((res)=>{
          var data_isolant_by_oper=this.state.isolantOpt;
          var fullIsolantOpt=[];
          if(res.data.result===1){
            res.data.data.forEach((item)=>{

              let oper_uid=item[param_scenario_uid] && item[param_scenario_uid].v? item[param_scenario_uid].v:'';
              let oper_uid_arr=oper_uid.split(',');

              var isolant=get_isolant_obj(item, param_isolant_data);
              //sturc
              if(oper_uid_arr && oper_uid_arr.length){
                oper_uid_arr.forEach((uid_scen)=>{
                  if(operation_uid.indexOf(uid_scen)!==-1){
                      if(!data_isolant_by_oper[uid_scen]){
                        data_isolant_by_oper[uid_scen]=[];
                      }
                      data_isolant_by_oper[uid_scen].push(isolant);
                  }
                });
              }
              //full
              fullIsolantOpt.push(isolant);
            });
          }
          console.log(data_isolant_by_oper);
          this.setState({isolantOpt:data_isolant_by_oper});
      })
    }

    if(!this.state.isolant_attr_sett){
      ct_apiview.getHeadData({
          uid: '5eadb3b0-7c44-4403-807a-45a845de70ea',
      }).then((res)=>{
        if(res.data.result==1){
          this.setState({isolant_attr_sett:res.data.data, isolantOpt:data}, loadData);
        }
      })
    }else{
        this.setState({isolantOpt:data}, loadData)
    }

  }

  setEditoper=(operation_id)=>{
    if(this.props.config.oper_action.indexOf('change_oper')==-1){
      return false;
    }
    this.setState({edit_oper_id:operation_id});
  }
  saveEditOper=(data, operation_id)=>{
    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id==operation_id){
        item.additional_blocks=data;
      }
      return item;
    })
    this.props.UserStepStore.updateDataOperation(newDataOperation);
  }
  surfaceChange=(e, id, isolant_id)=>{
    if(this.props.config.isolant_action.indexOf('change_surf')==-1){
      return false;
    }
    let {value}=e.currentTarget;
    value=value!==''?Number(value):'';
    if(value){
      value=value<=0?0:value;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===id){
        let data_isolant=item.isolant.find((item_isolant)=>{
            return item_isolant.id==isolant_id?true:false;
        });
        if(data_isolant){
            data_isolant.surface=value;

            //update adv_size
            if(data_isolant.adv_size && data_isolant.adv_size.length){
              let sum=0;
              let surf=Number(data_isolant.surface);
              data_isolant.adv_size= data_isolant.adv_size.map((adv_size_item)=>{
                adv_size_item.l= adv_size_item.l>surf?surf:adv_size_item.l;
                if((sum+adv_size_item.l)>surf){
                  adv_size_item.l=surf-sum;
                }
                sum+=adv_size_item.l;
                return adv_size_item;
              })
            }
        }

        this.updateSurfaceValue(item);


      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);
  }
  updateSurfaceValue=(item)=>{
    //update Prime and summ Surface
    let coef=item.coef;
    let Data= this.props.UserStepStore.dataClaculator;

    let sumSurface=0, coef_cahnge = 1;
    item.isolant.forEach((item_isolant)=>{
        sumSurface+=Number(item_isolant.surface);
    });

    if(item.surface){
      coef_cahnge = sumSurface/item.surface;
    }
    item.cost_client=rounded(item.cost_client*coef_cahnge);

    item.surface=sumSurface;
    item.cumac = Number(item.surface)*Number(item.value);

    let max_prix= item.max_prix;

    if(item.prix_unitaire<=max_prix){
      item.cost_client=rounded(Number(item.prix_unitaire)*Number(item.surface));
    }else{
      item.cost_client=item.cost_client>item.surface*max_prix?rounded(item.surface*max_prix):item.cost_client;
      item.cost_client=item.cost_client<item.surface*item.min_prix?rounded(item.surface*item.min_prix):item.cost_client;
    }
    item.cost=rounded(max_prix*Number(item.surface));
    item.cost_pro= rounded(Number(item.cost)-Number(item.cost_client));
    item.reste_a_charge= rounded(item.prix_unitaire*item.surface-item.cost_client);
  }

  changeAdvSize=(e, id, isolant_id, index, change_max_prix)=>{
    if(this.props.config.isolant_action.indexOf('change_adv_size')==-1){
      return false;
    }
    let {value, name}=e.currentTarget;
    value=value!==''?Number(value):'';
    if(value){
      value=value<=0?0:value;
    }
  //  let Operations=['BAR-TH-160', 'BAT-TH-146'];

    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===id){
        let data_isolant=item.isolant.find((item_isolant)=>{
            return item_isolant.id==isolant_id?true:false;
        });
        if(data_isolant && data_isolant.adv_size){
          //let uniq_oper=Operations.indexOf(item.operation.name)!==-1?true:false;
          let def_epp=0;
          let ep_key_attr= item.adv_attr_name.find((attr_adv)=>{
            return attr_adv.value=='Epaisseur'?true:false;
          });
          let ep_key=null;
          if(ep_key_attr){
            ep_key=ep_key_attr.name.replaceAll(/name/ig, '').trim();
          }
          if(ep_key && data_isolant.advParams && data_isolant.advParams[ep_key] && data_isolant.advParams[ep_key].value){
            def_epp=data_isolant.advParams[ep_key].value;
          }
          if(!data_isolant.adv_size[index]){
            let new_adv_size:{l:number, d:number, ep?:number}={l:0, d:0, ep:def_epp};
            data_isolant.adv_size.push(new_adv_size);
          }
          data_isolant.adv_size[index][name]=value;
          if(name=='l'){
            let sum_l = 0;
            data_isolant.adv_size.forEach((adv_size_item)=>{
              sum_l+=Number(adv_size_item.l);
            })
            data_isolant.surface=sum_l;

            this.updateSurfaceValue(item);
            /*value = (sum_l+value)>data_isolant.surface?data_isolant.surface-sum_l:value;*/
          }
        }

      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);
    this.validAdvSize();
    /*if(change_max_prix){
      this.calcMaxPrix(id, newDataOperation);
    }*/
  }
  addAdvSize = (e, id, isolant_id, change_max_prix)=>{
    //let Operations=['BAR-TH-160', 'BAT-TH-146'];
    if(this.props.config.isolant_action.indexOf('change_adv_size')==-1){
      return false;
    }

    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===id){
        let data_isolant=item.isolant.find((item_isolant)=>{
            return item_isolant.id==isolant_id?true:false;
        });
        if(data_isolant && data_isolant.adv_size){
          //let uniq_oper=Operations.indexOf(item.operation.name)!==-1?true:false;
          let def_epp=0;
          let ep_key_attr= item.adv_attr_name.find((attr_adv)=>{
            return attr_adv.value=='Epaisseur'?true:false;
          });
          let ep_key=null;
          if(ep_key_attr){
            ep_key=ep_key_attr.name.replaceAll(/name/ig, '').trim();
          }
          if(ep_key && data_isolant.advParams && data_isolant.advParams[ep_key] && data_isolant.advParams[ep_key].value){
            def_epp=data_isolant.advParams[ep_key].value;
          }

          let new_adv_size:{l:number, d:number, ep?:number}={l:0, d:0, ep:def_epp};
          if(!data_isolant.adv_size.length){
            data_isolant.adv_size.push({...new_adv_size});
          }
          data_isolant.adv_size.push(new_adv_size);
        }

      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);
    if(change_max_prix){
      this.calcMaxPrix(id, newDataOperation);
    }

  }
  removeAdvsize=(e, id, isolant_id, index, change_max_prix)=>{
    if(this.props.config.isolant_action.indexOf('change_adv_size')==-1){
      return false;
    }
    let {value, name}=e.currentTarget;
    if(value){
      value=value<=0?0:value;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===id){
        let data_isolant=item.isolant.find((item_isolant)=>{
            return item_isolant.id==isolant_id?true:false;
        });
        if(data_isolant && data_isolant.adv_size){
          data_isolant.adv_size=data_isolant.adv_size.filter((adv_size_item, adv_size_index)=>{
            return adv_size_index==index?false:true;
          })
          let sum_l = 0;
          data_isolant.adv_size.forEach((adv_size_item)=>{
            sum_l+=Number(adv_size_item.l);
          })
          data_isolant.surface=sum_l;

          this.updateSurfaceValue(item);
        }

      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);
    if(change_max_prix){
      this.calcMaxPrix(id, newDataOperation);
    }
    this.validAdvSize();

  }
  calcMaxPrix=(oper_id, data_new)=>{
    let data = this.props.UserStepStore.dataOperation;
    if(data_new){
      data=data_new;
    }
    let data_oper= data.find((item)=>{
      return item.id==oper_id?true:false;
    });
    if(data_oper){
      let Data=this.props.UserStepStore.dataClaculator;
      let isolant_data=JSON.parse(JSON.stringify(data_oper.isolant));

      let opt_adv_size=data_oper.options_adv_size.map((item)=>{
        return item.uid;
      });

      isolant_data.forEach((isolant_item)=>{
        if(!isolant_item.adv_size || !isolant_item.adv_size.length){
          isolant_item.adv_size=[{ep:0, d:0, l:0}];
        }
      })
      if(opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!==-1){
      //  adv_size_items=this.renderDiameterTemerature(Data, itemIsolant, adv_data, change_max_prix);
        isolant_data.forEach((isolant_item)=>{
          isolant_item.adv_size=  isolant_item.adv_size.map((adv_size_item)=>{
            return {t:adv_size_item.ep, d:adv_size_item.d, q:adv_size_item.l};
          })
        })
      }else if(opt_adv_size.indexOf('5c312056-099e-4ac6-9a5f-2954b797ab96')!==-1){
        //adv_size_items=this.renderTemerature(Data, itemIsolant, adv_data, change_max_prix);
        isolant_data.forEach((isolant_item)=>{
          isolant_item.adv_size=  isolant_item.adv_size.map((adv_size_item)=>{
            return {t:adv_size_item.ep, q:adv_size_item.l};
          })
        })
      }

      let data_calc={
        form:Data,
        oper_data:data_oper.objUid,
        isolant_data:isolant_data,
      };
      ct_apistep.getOperCalcData('c960cdcb-59d7-4494-8c8a-1e60a7910068', data_calc).then((res)=>{
        if(res.data.result==1){
          data_oper.max_prix=res.data.data.value;
          if(data_oper.prix_unitaire>data_oper.max_prix){
            data_oper.prix_unitaire=data_oper.max_prix;
          }
          this.updateSurfaceValue(data_oper);
          this.props.UserStepStore.updateDataOperation(data);
        }
      })
    }
  }
  validAdvSize=()=>{
    let Data=this.props.UserStepStore.dataOperation;
    let errors=[], removeErrors=[];
    Data.forEach((item)=>{
      if(item.isolant && item.isolant.length){
        let opt_adv_size=item.options_adv_size.map((item)=>{
          return item.uid;
        });
        item.isolant.forEach((itemIsolant)=>{
          if(itemIsolant.adv_size && itemIsolant.adv_size.length){

            let no_d=opt_adv_size.indexOf('5c312056-099e-4ac6-9a5f-2954b797ab96')!=-1?true:false;
            let no_ep=opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!=-1?true:false;

            let has_empty_val = itemIsolant.adv_size.find((adv_size_item)=>{
                return !adv_size_item.l || (!no_d && !adv_size_item.d) || (!no_ep && !adv_size_item.ep) ? true:false;
            });

            let check_d=opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!=-1?true:false;
            if(!has_empty_val && check_d){
              has_empty_val=itemIsolant.adv_size.find((adv_size_item)=>{
                  return adv_size_item.d<20?true:false;
              });
            }

            if(has_empty_val){
              errors.push({id:itemIsolant.id+'_surface'});
            }else{
              removeErrors.push(itemIsolant.id+'_surface');
            }
          }
        })
      }
    });
    if(errors.length){
      this.props.UserStepStore.addValidateErrorArray(errors);
    }
    if(removeErrors.length){
      this.props.UserStepStore.removeValidateErrorArray(removeErrors);
    }
  }

  removeError=(Data)=>{
    let removeError=[];
    removeError.push(Data.id+'_previsite');
    removeError.push(Data.id+'_isolants');
    removeError.push(Data.id+'_oper_price');
    removeError.push(Data.id+'_referencerapport');
    removeError.push(Data.id+'_bureaudecontrole');
    if(Data.isolant && Data.isolant.length){
      Data.isolant.forEach((itemIsolant)=>{
        removeError.push(itemIsolant.id+'_surface');
        if(itemIsolant.advParams){
          let keys= Object.keys(itemIsolant.advParams);
          keys.forEach((key)=>{
            removeError.push('adv_param_isolant_'+itemIsolant.id+'_'+key);
          })
        }
      })
    }
    if(removeError.length){
      this.props.UserStepStore.removeValidateErrorArray(removeError);
    }
  }
  removeErrorIsolant=(Data, item_isolant_id)=>{
    let removeError=[];
    if(Data.isolant && Data.isolant.length){
      let itemIsolant= Data.isolant.find((item)=>{
          return item.id==item_isolant_id?true:false;
      })
      if(itemIsolant && itemIsolant.advParams){
        removeError.push(itemIsolant.id+'_surface');
        let keys= Object.keys(itemIsolant.advParams);
        keys.forEach((key)=>{
          removeError.push('adv_param_isolant_'+itemIsolant.id+'_'+key);
        })
      }
    }
    if(removeError.length){
      this.props.UserStepStore.removeValidateErrorArray(removeError);
    }

  }
  removeOperBtn=(e, id)=>{
    if(this.props.config.oper_action.indexOf('remove')==-1){
      return false;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    let removeData=newDataOperation.find((item)=>{
      return item.id===id?true:false;
    });
    if(removeData){
      this.removeError(removeData);
    }
    newDataOperation=newDataOperation.filter((item)=>{
      return item.id===id?false:true;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);
  }
  isolantChange=(opt_data, Data, item_isolant_id)=>{
    let id = Data.id;
    if(this.props.config.isolant_action.indexOf('change')==-1){
      return false;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    console.log(opt_data);
    this.removeErrorIsolant(Data, item_isolant_id);

    let adv_params_name={};
    opt_data.value=opt_data.value==-1?null:opt_data.value;
    if(opt_data.value!==null){
        adv_params_name=this.getIsolantAdvParam(Data.adv_attr_name, opt_data.advParams)
    }

    let isolantData={value:opt_data.value, name:opt_data.label, surface:Data.def_quant, adv_size:[], advParams:adv_params_name, id:(item_isolant_id?item_isolant_id:Math.round(Math.random()*1000000000))};


    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===id){
        if(isolantData.value==null){
          item.isolant=item.isolant.filter((itemIs)=>{
            return itemIs.id!=isolantData.id?true:false;
          });
        }else{
          let isolantItem=item.isolant.find((itemIs)=>{
            return itemIs.id==isolantData.id?true:false;
          });
          if(isolantItem){
            isolantItem.value=isolantData.value;
            isolantItem.name=isolantData.name;
            isolantItem.advParams=isolantData.advParams;
          //  isolantItem.adv_size=isolantData.adv_size;
          }else{
            item.isolant.push(isolantData);
          }
        }


        this.updateSurfaceValue(item);
      }

      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);

    //
    let opt_adv_size=Data.options_adv_size.map((item)=>{
      return item.uid;
    });
    let change_max_prix=opt_adv_size.indexOf('12af3eb1-ecd0-4b87-884e-039ad62851e7')!==-1?true:false;
    if(change_max_prix){
      this.calcMaxPrix(Data.id, newDataOperation)
    }

  }

  getIsolantAdvParam=(data_name_param, adv_param)=>{
    let adv_params_name={};
    let keys_param= Object.keys(adv_param);
    if(keys_param.length){
      keys_param.forEach((key)=>{
        if(key.indexOf('Attribute')!==-1){
          let name_attr= data_name_param.find((item)=>{
            return item.name.indexOf(key)!==-1?true:false;
          });
          if(name_attr){
            adv_params_name[key]={...adv_param[key]};
            adv_params_name[key]['title']=name_attr.value;
          }
        }else{
          adv_params_name[key]=adv_param[key];
        }
      })
    }else{
      let adv_params_new={};
      data_name_param.forEach((item)=>{
        let key=item.name.replaceAll(/name/ig, '').trim();
        adv_params_new[key]={title:item.value, value:''};
      })
      adv_params_name={
        'Marque':{title:'Marque', value:''},
        'Reference':{title:'Reference', value:''},
        ...adv_params_new
      }
    }
    return adv_params_name;
  }

  changeParamIsolant=(e, id, item_isolant)=>{
    if(this.props.config.isolant_action.indexOf('change_param')==-1){
      return false;
    }
    let {name, value}= e.currentTarget;
    if(item_isolant.value!=-2){
       item_isolant.value=-2;
       item_isolant.name='-Ajouter-';
    }
    let paramChange=item_isolant.advParams[name]?item_isolant.advParams[name]:null;
    if(paramChange){
      paramChange.value=value;
    }

    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===id){
        item.isolant=item.isolant.map((itemIs)=>{
          if(itemIs.id==item_isolant.id){
            return item_isolant;
          }
          return itemIs;
        });
      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);

  }
  advColumnChange=(e, idoper)=>{
    let {value, id}=e.currentTarget;
    if(this.props.config.oper_action.indexOf('change_'+id)==-1){
      return false;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===idoper){
        item[id]=value;
      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);
    this.validate(newDataOperation, id);
  }
  handleChangeDate=(id, value, idoper)=>{
    if(this.props.config.oper_action.indexOf('change_'+id)==-1){
      return false;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===idoper){
        item[id]=value;
      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);
    this.validate(newDataOperation, id);
  }

  renderSurfaceInput=(Data, itemIsolant, error)=>{
    if(itemIsolant.id){
      /*let Operations=['BAR-TH-160', 'BAT-TH-146'];
      let isSpOper=Operations.indexOf(Data.operation.name)!==-1?true:false;*/
      let opt_adv_size=Data.options_adv_size.filter((item)=>{
        //remove bure de contole from options
        return item.uid=='cfb7d57f-7b3e-49f7-8fd4-e7fd43eb2110'?false:true;
      });
      let isSpOper=opt_adv_size.length?true:false;

      let surfaceInput=<NumberInput onChange={(e)=>{this.surfaceChange(e, Data.id, itemIsolant.id)}} value={itemIsolant.surface} AdvProps={{name:"d", className:"form-control surface_input", disabled:(this.props.config.isolant_action.indexOf('change_surf')==-1 || Data.disableSurface?true:false)}}/>;
      return(
        <React.Fragment>
          {!isSpOper && <div className="title">Quantité</div>}
          <div className={'form-group '+(error.indexOf(itemIsolant.id+'_surface')!==-1?'validate_error':'')}>
              {!isSpOper && surfaceInput}
              {isSpOper?this.renderAdvSurfaceInput(Data, itemIsolant):null}
            </div>
        </React.Fragment>
        );
    }else{
      return null;
    }
  }
  renderAdvSurfaceInput=(Data, itemIsolant)=>{
    let isEmptyLast=true;
    let adv_data = JSON.parse(JSON.stringify(itemIsolant.adv_size));

    let def_epp=0;
    let ep_key_attr= Data.adv_attr_name.find((item)=>{
      return item.value=='Epaisseur'?true:false;
    });
    let ep_key=null;
    if(ep_key_attr){
      ep_key=ep_key_attr.name.replaceAll(/name/ig, '').trim();
    }
    if(ep_key && itemIsolant.advParams && itemIsolant.advParams[ep_key] && itemIsolant.advParams[ep_key].value){
      def_epp=itemIsolant.advParams[ep_key].value;
    }
    if(adv_data.length){
        let last=adv_data[adv_data.length-1];
        if(last.d || last.l || (last.ep && last.ep!=def_epp)){
          isEmptyLast=false;
        }
    }
    if(!adv_data.length){
      let empty_item:any={d:0, l:0, ep:def_epp};
      adv_data.push(empty_item);
    }

    let opt_adv_size=Data.options_adv_size.map((item)=>{
      return item.uid;
    });
    let adv_size_items=null;
    let change_max_prix=opt_adv_size.indexOf('12af3eb1-ecd0-4b87-884e-039ad62851e7')!==-1?true:false;
    if(opt_adv_size.indexOf('5dc70d2c-8882-4e1a-807d-409b0e368c51')!==-1){
        adv_size_items=this.renderDiameter(Data, itemIsolant, adv_data, change_max_prix);
    }else if(opt_adv_size.indexOf('b6f94695-cf9b-4d12-aae2-3f172110ab74')!==-1){
      adv_size_items=this.renderDiameterTemerature(Data, itemIsolant, adv_data, change_max_prix);
    }else if(opt_adv_size.indexOf('5c312056-099e-4ac6-9a5f-2954b797ab96')!==-1){
      adv_size_items=this.renderTemerature(Data, itemIsolant, adv_data, change_max_prix);
    }

    let isDisabled=this.props.config.isolant_action.indexOf('change_adv_size')==-1?true:false;
    let style_dis={};
    if(isDisabled){
      style_dis={display:'none'};
    }

    return (<div className="adv_size_cont">
      <div className="title">Total <span className="bold">{(itemIsolant.surface?itemIsolant.surface:'')}</span></div>
      <div className="adv_size_items">{adv_size_items}</div>
      <div className="adv_size_btn">
        <button disabled={isDisabled} style={style_dis} onClick={(e)=>{this.addAdvSize(e, Data.id, itemIsolant.id, change_max_prix)}} className="btn">+</button>
      </div>
    </div>);
  }
  renderDiameter=(Data, itemIsolant, adv_data, change_max_prix)=>{
    let onBlur={};
    if(change_max_prix){
      onBlur={onBlur:()=>{this.calcMaxPrix(Data.id, null)}}
    }

    let isDisabled=this.props.config.isolant_action.indexOf('change_adv_size')==-1?true:false;
    let style_dis={};
    if(isDisabled){
      style_dis={display:'none'};
    }

    return adv_data.map((item, i)=>{
      let btn=<div className="btn_cont"><button disabled={isDisabled} style={style_dis} onClick={(e)=>{this.removeAdvsize(e, Data.id, itemIsolant.id, i, change_max_prix)}} type="button" className="btn"><Icon name="trash" size={20} color={'#FF2E00'}/></button></div>;
      if(!item.ep){
        item.ep=0;
      }
      let errortext=null;
      if(item.d<20){
      //  errortext=getErrorMsg([{id:'error_d', id_msg:'error_d_msg'}], 'error_d')
      }
      return (<div key={'ad_size_'+itemIsolant.id+'_'+i} className="adv_size_item flex_responsive">
        <div className="input_cont form-group">
          <div className="input_unit">
            <NumberInput AdvProps={{name:"ep", className:"form-control", disabled:isDisabled}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, Data.id, itemIsolant.id, i, change_max_prix)}}  value={(item.ep)}  />
            <span className="unit">mm</span>
            <label><span className="text">Epaisseur (mm)</span><span className="label_ph"></span></label>
          </div>
        </div>
        <div className="input_cont form-group">
          <div className="input_unit">
            <NumberInput AdvProps={{name:"d", className:"form-control", disabled:isDisabled}}  {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, Data.id, itemIsolant.id, i, change_max_prix)}}  value={item.d}  />
            <span className="unit">mm</span>
            <label><span className="text">Diamètre (mm)</span><span className="label_ph"></span></label>
          </div>
          {errortext}
        </div>
        <div className="input_cont form-group">
          <div className="input_unit">
            <NumberInput AdvProps={{name:"l", className:"form-control", disabled:Data.disableSurface}}  {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, Data.id, itemIsolant.id, i, change_max_prix)}}  value={item.l}  />
            <span className="unit">m</span>
            <label><span className="text">Longueur (linéaire)</span><span className="label_ph"></span></label>
          </div>
        </div>
        {btn}
      </div>);
    })
  }
  renderDiameterTemerature=(Data, itemIsolant, adv_data, change_max_prix)=>{
    let onBlur={};
    if(change_max_prix){
      onBlur={onBlur:()=>{this.calcMaxPrix(Data.id, null)}}
    }

    let isDisabled=this.props.config.isolant_action.indexOf('change_adv_size')==-1?true:false;
    let style_dis={};
    if(isDisabled){
      style_dis={display:'none'};
    }

    return adv_data.map((item, i)=>{
      let btn=<div className="btn_cont"><button disabled={isDisabled} style={style_dis} onClick={(e)=>{this.removeAdvsize(e, Data.id, itemIsolant.id, i, change_max_prix)}} type="button" className="btn"><Icon name="trash" size={20} color={'#FF2E00'}/></button></div>;
      if(!item.ep){
        item.ep=0;
      }
      let errortext=null;
      if(item.d<20){
        errortext=getErrorMsg([{id:'error_d', id_msg:'error_d_msg'}], 'error_d')
      }
      return (<div key={'ad_size_'+itemIsolant.id+'_'+i} className="adv_size_item flex_responsive">
        <div className="input_cont form-group">
          <div className="input_unit">
            <NumberInput AdvProps={{name:"d", className:"form-control", disabled:isDisabled}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, Data.id, itemIsolant.id, i, change_max_prix)}}  value={item.d}  />
            <span className="unit">mm</span>
            <label><span className="text">Diamètre (mm)</span><span className="label_ph"></span></label>
          </div>
          {errortext}
        </div>
        <div className="input_cont form-group">
          <div className="input_unit">
            <NumberInput AdvProps={{name:"l", className:"form-control", disabled:Data.disableSurface}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, Data.id, itemIsolant.id, i, change_max_prix)}}  value={item.l}  />
            <label><span className="text">Quantité</span><span className="label_ph"></span></label>
          </div>
        </div>
        {btn}
      </div>);
    })
  }
  renderTemerature=(Data, itemIsolant, adv_data, change_max_prix)=>{
    let onBlur={};
    if(change_max_prix){
      onBlur={onBlur:()=>{this.calcMaxPrix(Data.id, null)}}
    }

    let isDisabled=this.props.config.isolant_action.indexOf('change_adv_size')==-1?true:false;
    let style_dis={};
    if(isDisabled){
      style_dis={display:'none'};
    }

    return adv_data.map((item, i)=>{
      let btn=<div className="btn_cont"><button disabled={isDisabled} style={style_dis} onClick={(e)=>{this.removeAdvsize(e, Data.id, itemIsolant.id, i, change_max_prix)}} type="button" className="btn"><Icon name="trash" size={20} color={'#FF2E00'}/></button></div>;
      if(!item.ep){
        item.ep=0;
      }
      return (<div key={'ad_size_'+itemIsolant.id+'_'+i} className="adv_size_item flex_responsive">
        <div className="input_cont form-group">
            <CustomSelect
              data={[
                {value:'100', text:'50°C ≤ T ≤ 120°C'},
                {value:'200', text:'T > 120°C'},
              ]}
              value={item.ep}
              onChange={(opt_data)=>{
                  this.changeAdvSize({currentTarget:{value:opt_data.value, name:'ep'}}, Data.id, itemIsolant.id, i, change_max_prix);
                  this.calcMaxPrix(Data.id, null);
              }}
              label={'Température (°C)'}
              disabled={isDisabled}
              placeHolderText={'-Chose Température-'}
            />
            <label><span className="text">Température (°C)</span><span className="label_ph"></span></label>
        </div>
        <div className="input_cont form-group">
          <div className="input_unit">
            <NumberInput AdvProps={{name:"l", className:"form-control", disabled:Data.disableSurface}} {...onBlur}  onChange={(e)=>{this.changeAdvSize(e, Data.id, itemIsolant.id, i, change_max_prix)}}  value={item.l}  />
            <label><span className="text">Quantité</span><span className="label_ph"></span></label>
          </div>
        </div>
        {btn}
      </div>);
    })
  }

  renderSelectIsolant=(Data, itemIsolant)=>{
      let DataIsolant=[], dataSelect=null, uid=null;;
      let errors_data=this.props.UserStepStore.validateError;
      let error=errors_data.map((error_item)=>{return error_item.id});
      if(this.state.isolantOpt && this.state.isolantOpt[Data.operation.uid]){
        DataIsolant=this.state.isolantOpt[Data.operation.uid];
      }
  //    DataIsolant=this.state.fullIsolantOpt;
      let Option=DataIsolant.map((item)=>{
        if(itemIsolant.value==item.uid){
          dataSelect={ value: item.uid, text: item.name, advParams:itemIsolant.advParams , adv_size:item.adv_size};
          uid=item.uid;
        }
        return { value: item.uid, text: item.name, advParams:item.advParams, adv_size:item.adv_size };
      })
      //if(!itemIsolant.id || itemIsolant.value!=-2){

        let adv_params_name=this.getIsolantAdvParam(Data.adv_attr_name, itemIsolant.advParams);
        let new_item_isolant={ value: '-2', text:'-Ajouter-', advParams:adv_params_name, adv_size:itemIsolant.adv_size};
        Option=[new_item_isolant, ...Option];
        if(itemIsolant.value==-2){
          dataSelect=new_item_isolant;
          uid=new_item_isolant.value;
        }
      //}
      if(itemIsolant.id){
        Option=[{ value: '-1', advParams:null,  adv_size:[], text:'-Retirer l\'opération-'}, ...Option];
      }
    //
      let advParam_html=null;
      if(dataSelect && dataSelect.advParams){
        let Operations=['BAR-TH-160', 'BAT-TH-146'];
        advParam_html=Object.keys(dataSelect.advParams).filter((key)=>{
          return Operations.indexOf(Data.operation.name)!==-1 && dataSelect.advParams[key].title=='Epaisseur'?false:true;
        }).map((key)=>{
          let param=dataSelect.advParams[key];
          let input=<input disabled={(this.props.config.isolant_action.indexOf('change_param')==-1 || Data.disableSurface?true:false)} name={key} onChange={(e)=>{this.changeParamIsolant(e, Data.id, itemIsolant)}} value={param.value} className="form-control"/>;
          if(dataSelect.advParams[key].type && dataSelect.advParams[key].type=='number'){
             input=<NumberInput onChange={(e)=>{this.changeParamIsolant(e, Data.id, itemIsolant)}} value={param.value} AdvProps={{name:param.title, className:"form-control", disabled:(this.props.config.isolant_action.indexOf('change_param')==-1 || Data.disableSurface?true:false)}}/>;
          }
          let label=param.title;
        /*  if(Operations.indexOf(Data.operation.name)!==-1 && param.title=='Résistance thermique'){
            label='Classe';
          }*/
          return <div key={itemIsolant.id+'_'+param.title} style={{padding:'0px', margin:'0.7rem 0px'}} className={'adv_param_item form-group '+(error.indexOf('adv_param_isolant_'+itemIsolant.id+'_'+key)!==-1?'validate_error':'')}>{input}<label><span className="text">{label}</span><span className="label_ph"></span></label></div>
        });
      }

      let title=<div className="title">Matériel / équipement</div>;
      if(!itemIsolant.id){
        title=null;
      }
      let style={}
      if(this.props.config.isolant_action.indexOf('change')==-1 || Data.disableSurface){
        style={display:'none'};
      }
      return(
        <React.Fragment>
          {title}
          <CustomSelect
            data={Option}
            value={uid}
            onChange={(opt_data)=>{this.isolantChange(opt_data, Data, itemIsolant.id)}}
            disabled={(this.props.config.isolant_action.indexOf('change')==-1 || Data.disableSurface?true:false)}
            placeHolderText={'-Ajouter matériel/équipement pour la même opération-'}
            style={style}
          />
          {advParam_html}
        </React.Fragment>

      )
  }
  renderAdvColumn=(Data, error, rowSpan)=>{
    //let Operations=['BAR-TH-160', 'BAT-TH-146'];
    //console.log(Data);
    if(!Data.disableSurface){
      let Option=this.state.berueOpt.map((item)=>{
        return {value:item.uid, text:item.name};
      })

      let berule_item=null;
      if(this.props.config.oper_adv_column.indexOf('bureaudecontrole')!==-1){
        berule_item=<div className="col-md-6 adv_param_item">
            <div className={'input_cont form-group '+(error.indexOf(Data.id+'_bureaudecontrole')!==-1?'validate_error':'')}>
              <CustomSelect
                data={Option}
                value={Data.bureaudecontrole}
                onChange={(opt_data)=>{this.advColumnChange({currentTarget:{value:opt_data.value, id:'bureaudecontrole'}}, Data.id)}}
                disabled={(this.props.config.oper_action.indexOf('change_bureaudecontrole')==-1 || Data.disableSurface?true:false)}
                placeHolderText={'-Chose bureau de controle-'}
                style={{height:30}}
                label={'Bureaue de controle'}
              />
              <label><span className="text">Bureaue de controle</span><span className="label_ph"></span></label>
          </div>
        </div>;
      }

      let rapport_item=null;
      if(this.props.config.oper_adv_column.indexOf('referencerapport')!==-1){
        rapport_item=<div className="col-md-6 adv_param_item">
            <div className={'input_cont form-group '+(error.indexOf(Data.id+'_referencerapport')!==-1?'validate_error':'')}>
              <input disabled={(this.props.config.oper_action.indexOf('change_referencerapport')==-1 || Data.disableSurface?true:false)} onChange={(e)=>{this.advColumnChange(e, Data.id)}} id="referencerapport" value={Data.referencerapport} className="form-control"/>
              <label><span className="text">Référence rapport</span><span className="label_ph"></span></label>
          </div>
        </div>;
      }
      return(
        <React.Fragment>
        <div className="operation_adv_column group_input flex_responsive">
          {berule_item}
          {rapport_item}
          </div>
        </React.Fragment>
      );
    }else{
      return null;
    }
  }

  changePrime=(e, oper_id)=>{
    let {id, value}=e.currentTarget;
    let Data= this.props.UserStepStore.dataClaculator;
    value=Number(value)?Number(value):0;
    if(value){
      value=value<=0?0:value;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    const updatePrix=(data, new_value)=>{
      //update prix_material and prix_work
      let coef= data.prix_material/data.prix_unitaire;
      data.prix_material=rounded(new_value*coef);
      data.prix_work=rounded(new_value-data.prix_material);
    }
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===oper_id){

        let coef=item.coef;
        let max_prix= item.max_prix;

        let min_value= rounded(item.min_prix*Number(item.surface));

        value=value>item.cost?item.cost:value;
        value=value<min_value?min_value:value;

        if(id=='cost_client'){
          item.cost_client= rounded(Number(value));
          item.cost_pro= rounded(Number(item.cost)-Number(value));
          if(item.surface && item.prix_unitaire <= max_prix){
            updatePrix(item, rounded(item.cost_client/item.surface));
            item.prix_unitaire =rounded(item.cost_client/item.surface);
          }
          if(item.prix_unitaire > max_prix){
            item.reste_a_charge= rounded(item.prix_unitaire*item.surface-item.cost_client);
          }else{
            item.reste_a_charge=0;
          }

        }else if(id=='cost_pro'){
          item.cost_pro= rounded(Number(value));
          item.cost_client= rounded(Number(item.cost)-Number(value));
          if(item.surface){
            item.prix_unitaire =rounded(item.cost_client/item.surface);
          }else{
            item.prix_unitaire =0;
          }
        }
      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);

  }
  changePrix=(e, oper_id)=>{
    let {id, value}=e.currentTarget;
    value=Number(value)?Number(value):0;
    if(value){
      value=value<=0?0:value;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    let Data= this.props.UserStepStore.dataClaculator;
    let showModalConfirm= false, reste_a_charge=0;
    const updatePrix=(data, new_value)=>{
      //update prix_material and prix_work
      let coef= data.prix_material/data.prix_unitaire;
      data.prix_material=rounded(new_value*coef);
      data.prix_work=rounded(new_value-data.prix_material);
    }
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===oper_id){
        //item.cost item.cost_client item.cost_pro
        let coef=item.coef;

        let max_prix= item.max_prix;

        if(value<=max_prix){
          value=value<Number(item.min_prix)?Number(item.min_prix):value;

          updatePrix(item, value);

          item.prix_unitaire=rounded(value);
          this.updateSurfaceValue(item);
        }else{
          showModalConfirm=true;
          reste_a_charge=rounded(value*item.surface-item.cost_client);
        }
      }
      return item;
    });

    if(showModalConfirm){
      this.props.UserStepStore.setVisibleConfirmModal({
        show:true,
        text:'Le prix entraine un reste à charge pour le bénéficiaire de '+numberWithSpaces(reste_a_charge)+' EUR',
        title:'Confirm',
        YesTitle:'Oui',
        NonTitle:'Non',
        callbackYes:()=>{
          let newDataOperation=this.props.UserStepStore.dataOperation;
          let Data= this.props.UserStepStore.dataClaculator;
          newDataOperation=newDataOperation.map((item)=>{
            if(item.id===oper_id){
              updatePrix(item, value);
              item.prix_unitaire=rounded(value);
              this.updateSurfaceValue(item);
            }
            return item;
          });
          this.props.UserStepStore.updateDataOperation(newDataOperation);
        },
        callbackNo:()=>{
          let newDataOperation=this.props.UserStepStore.dataOperation;
          let Data= this.props.UserStepStore.dataClaculator;
          newDataOperation=newDataOperation.map((item)=>{
            if(item.id===oper_id){
              //item.cost item.cost_client item.cost_pro
              let coef=item.coef;

              let max_prix= item.max_prix;

              value=max_prix;

              updatePrix(item, value);

              item.prix_unitaire=rounded(value);
              this.updateSurfaceValue(item);
            }
            return item;
          });
          this.props.UserStepStore.updateDataOperation(newDataOperation);
        }
      });
    }else{
      this.props.UserStepStore.updateDataOperation(newDataOperation);
    }

  }
  changePrix_adv=(e, oper_id)=>{
    let {id, value}=e.currentTarget;
    value=Number(value)?Number(value):0;
    if(value){
      value=value<=0?0:value;
    }
    let newDataOperation=this.props.UserStepStore.dataOperation;
    newDataOperation=newDataOperation.map((item)=>{
      if(item.id===oper_id){
        value=value>=item.prix_unitaire?item.prix_unitaire:value;
        item.prix_material=value;
        item.prix_work=rounded(item.prix_unitaire-item.prix_material);
      }
      return item;
    });
    this.props.UserStepStore.updateDataOperation(newDataOperation);

  }

  add_remove_pop_id=(id)=>{
    let remove_pop_open=this.state.remove_pop_open;
    if(remove_pop_open.indexOf(id)==-1){
      remove_pop_open.push(id);
    }else{
      remove_pop_open=remove_pop_open.filter((id_item)=>{
        return id_item==id?false:true;
      })
    }
    this.setState({remove_pop_open:remove_pop_open})
  }
  rem_remove_pop_id=(id)=>{
    let remove_pop_open=this.state.remove_pop_open;
    remove_pop_open=remove_pop_open.filter((id_item)=>{
      return id_item==id?false:true;
    })
    this.setState({remove_pop_open:remove_pop_open});
  }

  rendeRrowOperation=(Data)=>{
      let errors_data=this.props.UserStepStore.validateError;
      let error=errors_data.map((error_item)=>{return error_item.id});
      return Data.map((item)=>{

        if(item.id==this.state.edit_oper_id){
          return <OperationsEdit onClose={()=>{this.setState({edit_oper_id:null})}} onSave={(data)=>{this.setState({edit_oper_id:null}, ()=>{this.saveEditOper(data, item.id)})}} data={item.additional_blocks} />
        }

        let removeBtn=<div className="remove_btn_cont">
                        <div className={'btn remove_btn'+(this.props.config.oper_action.indexOf('remove')==-1?' disabled_remove':'')+(this.state.remove_pop_open.indexOf(item.id)!==-1?' nt_open_del_pop':'')} onClick={(e)=>{this.add_remove_pop_id(item.id)}}>
                              <div className={"nt_div_inf_t"}>Supprimer</div>
                              <div className={"nt_div_inf_t_del"}>
                                  <div className={"nt_div_inf_t_del_c1"}>Cette action est inévitable. Etes-vous sûr que vous voulez supprimer?</div>
                                  <div className={"nt_div_inf_t_del_c2"}>
                                      <button className={"nt_div_inf_t_del_b1"} onClick={(e)=>{this.removeOperBtn(e, item.id)}}>Oui</button>
                                      <button className={"nt_div_inf_t_del_b2"} onClick={(e)=>{this.rem_remove_pop_id(item.id)}}>Non</button>
                                  </div>
                              </div>
                              <Icon name="trash" size={16} color={'#FF2E00'}/>
                        </div>
                      </div>;

        let isolantData=item.isolant.slice(0);
        isolantData.push({value:'empty', id:null, advParams:{}, adv_size:[]});

        let AdvColumn=this.renderAdvColumn(item, error, 0);

        let additional_items=null;
        if(item.additional_blocks && item.additional_blocks.length){
          additional_items=item.additional_blocks.map((addit_block)=>{
            let data_tags=[];
            if((addit_block.type.name=='Checkbox' || addit_block.type.name=='Radio') && addit_block.data && addit_block.data.length){
              data_tags=addit_block.data.map((item_check)=>{
                let userData=item_check.userData?' - '+item_check.userData:'';
                return <span className={'addit_block_data_item'} key={'addit_block_data_'+item_check.uid}>{item_check.name+userData}</span>
              });
            }
            return (<div className={'addit_block_item'} key={'addit_block_'+addit_block.uid}>
                  <span className={'item_text'}>
                    <span className={'text'}>{addit_block.name}</span>
                    {data_tags}
                  </span>
            </div>);
          })
        }
        if(item.objUid && item.objUid.length){
          let additional_items_objUid=item.objUid.filter((objUid_item)=>{
            return objUid_item.label=='Operation' || objUid_item.label=='Date range'?false:true;
          }).map((objUid_item)=>{
            let data_tags=[<span className={'addit_block_data_item'} key={'addit_block_data_'+objUid_item.uid}>{objUid_item.name}</span>];
            return (<div className={'addit_block_item'} key={'addit_block_'+objUid_item.uid}>
                  <span className={'item_text'}>
                    <span className={'text'}>{objUid_item.label}</span>
                    {data_tags}
                  </span>
            </div>);
          })
          if(additional_items){
            additional_items=[...additional_items_objUid, ...additional_items];
          }else{
            additional_items=additional_items_objUid;
          }

        }

        let isolants_items = isolantData.map((itemIsolant, i)=>{
          let sep=null;
          if(isolantData.length>1){
            sep=<div className="seporator"></div>
          }
          let content_isolant=null;
          if(itemIsolant.id){
            content_isolant=[<div className="isolant_select_item col-md-4">{this.renderSelectIsolant(item, itemIsolant)}</div>, <div className="isolant_surface_item col-md-8">{this.renderSurfaceInput(item, itemIsolant, error)}</div>];
          }else{
            content_isolant=[<div className="isolant_select_item col-md-8">{this.renderSelectIsolant(item, itemIsolant)}</div>];
          }
          return(<div key={'isolant_'+i+'_'+itemIsolant.id} className={'operation_isolant_items group_input '+(!itemIsolant.id?'empty':'')}>
            <div className="isolant_data row">
              {content_isolant}
            </div>
            {sep}
          </div>);
        })

        let oper_prices=null;
        if(isolantData.length>1){
          oper_prices=<React.Fragment>
          <div className="operation_prices_item">
            <div className="title">Prime Client €</div>
            <div className={(error.indexOf(item.id+'_oper_price')!==-1?'validate_error':'')}>
              <div className="group_input">
                <NumberInput priceFormat={true} changeOnBlur={true} prefix={'€ '} onChange={(e)=>{this.changePrime(e, item.id)}} value={item.cost_client} AdvProps={{id:"cost_client", className:"form-control", disabled:(this.props.config.isolant_action.indexOf('change_prime')==-1?true:false)}}/>
              </div>
              <div className="group_input">
                  <label>Prix unitaire, TTC</label>
                  <NumberInput priceFormat={true} changeOnBlur={true} prefix={'€ '} onChange={(e)=>{this.changePrix(e, item.id)}} value={item.prix_unitaire} AdvProps={{name:"prix_unitaire", className:"form-control", disabled:(this.props.config.isolant_action.indexOf('change_prix')==-1?true:false)}}/>

              </div>
            </div>
          </div>
          <div className="operation_prices_item">
            <div className="title">Prime Pro €</div>
            <div className="value">€ {numberWithSpaces(Number(item.cost_pro).toFixed(2))}</div>
            <div className="group_input">
                <label>Materiel</label>
                <NumberInput priceFormat={true} changeOnBlur={true} prefix={'€ '} onChange={(e)=>{this.changePrix_adv(e, item.id)}} value={item.prix_material} AdvProps={{name:"prix_material", className:"form-control", disabled:(this.props.config.isolant_action.indexOf('change_prix')==-1?true:false)}}/>

            </div>
          </div>
          <div className="operation_prices_item">
            <div className="title">Prime Totale €</div>
            <div className="value">€ {numberWithSpaces(Number(item.cost).toFixed(2))}</div>
            <div className="group_input">
                <label>Main d'oeuvre</label>
                <div className="value">€ {numberWithSpaces(Number(item.prix_work).toFixed(2))}</div>
            </div>
          </div>
          <div className="operation_prices_item">
            <div className="title">Reste à Charge € </div>
            <div className="value">€ {numberWithSpaces(Number(item.reste_a_charge).toFixed(2))}</div>
          </div>
          </React.Fragment>
        }

        let btn_edit=null;
        if(item.additional_blocks && item.additional_blocks.length && this.props.config.oper_action.indexOf('change_oper')!=-1){
          btn_edit=<button onClick={()=>{this.setEditoper(item.id)}} className="btn" type="button"><Icon name="edit" size={16} color={'#0069A5'}/>Éditer</button>;
        }
        let sub_title=null;
        if(this.props.config.mode && (this.props.config.mode=='AH' || this.props.config.mode=='AHFull')){
          sub_title=<div className="operation_sub_title">
            Merci d'indiquer les valeurs finales enregistrées par l'inspection sur site
          </div>
        }

        return(<div key={'oper_'+item.id} className="operation_item">
              <div className="operation_title">
                  <div className="title">{item.operation.name}</div>
                  {btn_edit}
              </div>
              <div className="operation_addit_block">
                  {additional_items}
              </div>
              {AdvColumn}
              {sub_title}
              <div className="operation_isolants">
                {isolants_items}
              </div>
              <div className="operation_prices flex_responsive">
                {oper_prices}
              </div>
              {removeBtn}
          </div>);
      })
  }
  addClick=()=>{
    if(this.props.config.oper_action.indexOf('add')==-1){
      return false;
    }
    this.props.UserStepStore.setOperationModal(true);
  }

  handleChangeAdvDate=(id, value)=>{
    let Data=this.props.UserStepStore.dataClaculator.date;
    Data={...Data, [id]:value};
    this.props.UserStepStore.updateCalculatorData('date', Data);
    this.validate(Data, id);
  }
  handleChangeAHDate=(id, value)=>{
    let Data=this.props.UserStepStore.dataClaculator.date;
    Data={...Data, [id]:value};
    Data.date_de_signature=value;
    this.props.UserStepStore.updateCalculatorData('date', Data);
    this.validate(Data, id);
  }
  renderAdvDate=()=>{
    let Data=this.props.UserStepStore.dataClaculator.date;
    let errors_data=this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    return (<div className="col-md-4">
              <div className={'form-group '+(error.indexOf('date_de_debut')!==-1?'validate_error':'')}>
                <CustomDatePicker id="date_de_debut" value={Data.date_de_debut} handleChange={this.handleChangeAdvDate} label={'Date de début des travaux'} />
                {getErrorMsg(errors_data, 'date_de_debut')}
              </div>
        </div>);
  }
  renderDeTravDate=()=>{
    let Data=this.props.UserStepStore.dataClaculator.date;
    let errors_data=this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    return (
        <div className="col-md-4">
            <div className={'form-group '+(error.indexOf('finDeTravaux')!==-1?'validate_error':'')}>
              <CustomDatePicker id="finDeTravaux" value={Data.finDeTravaux} handleChange={this.handleChangeAdvDate} label={'Date fin de travaux'} />
              {getErrorMsg(errors_data, 'finDeTravaux')}
            </div>
        </div>);
  }
  renderAHDate=()=>{
    let Data=this.props.UserStepStore.dataClaculator.date;
    let errors_data=this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    return (<div className="col-md-4">
            <div className={'form-group '+(error.indexOf('date_de_signature')!==-1 || error.indexOf('previsite')!==-1?'validate_error':'')}>
              <CustomDatePicker id="date_de_signature_AH" disabled={(this.props.config.disable_DateSignatureAH?true:false)} label={'Date de signature du devis'} value={Data.date_de_signature_AH} handleChange={this.handleChangeAHDate} />
              {getErrorMsg(errors_data, 'date_de_signature')}
            </div>
        </div>);
  }
  renderPrevisiteDate=()=>{
    let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
    let errors_data=this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    return (<div className="row group_input">
      <div className="col-md-4">
          <div className={'form-group '+(error.indexOf('previsite')!==-1?'validate_error':'')}>
            <CustomDatePicker disabled={true} id="previsite" value={Data.previsite} label={'Date de visite préalable'} handleChange={()=>{}}/>
          </div>
      </div>
     </div>);
  }

  render(){
    let DataOperation=this.props.UserStepStore.dataOperation;
    let rowOperation=DataOperation && DataOperation.length?this.rendeRrowOperation(DataOperation):null;
    return(
      <div ref={this.anim_elem} style={this.state.animation} className={'form_item'}>
        <div className="operation_step_cont">
          <div className={'title_step'}>OPÉRATIONS</div>
          <div className={'title_step'}>Opérations valorisées par</div>
          {this.props.configData.showOrganization?<OrganizationFormImage blockOrganization={this.props.config.blockOrganization}/>:null}
          <div className="col-md-12">
            {this.props.configData.showDate_previsite && this.renderPrevisiteDate()}
            <div className="row group_input required">
                { this.props.configData.showDateSignatureAH && this.renderAHDate()}
                { this.props.configData.showDate_de_debut && this.renderAdvDate()}
                { this.props.configData.showDate_de_finDeTravaux && this.renderDeTravDate()}
            </div>
          </div>
          <div className="title_step">Opérations
            <span style={{marginLeft:17, display:'inline-flex'}}><CustomTolltip size={20}>
              Ajouter toutes les opérations réalisées<br/>
              Démultiplier les opérations en fonction des marques, references et épaisseurs
            </CustomTolltip></span>
           </div>
           <div className="col-md-12 operation_cont">
            {rowOperation}
           </div>
          <OperationsAdd isShow={this.props.UserStepStore.showOperationModal} />
        </div>
      </div>
    )
  }
}


interface IBtnSwitchProps {
  UserStepStore?:any,
  ToastrStore?:any,
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class BtnSwitch extends React.Component<IBtnSwitchProps>{

  changeHandler=(value)=>{
      let Data_ben=this.props.UserStepStore.dataClaculator.beneficiaire;
      let Data_inf=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
      let removeError=[];
      if(value=='Personne morale'){
        removeError=Data_ben.persone_phys_data.map((item)=>{
          return 'persone_phys_data_'+item.id;
        });
        Data_ben.persone_phys_data=[];
        Data_ben.adress_error=0;
        Data_ben.number_de_perosne='';
        Data_ben.revenue=null;
        Data_ben.TableAflag=0;
        Data_ben.TableBflag=0;

        Data_inf.same_adress=0;
        Data_inf.nom_travaux='';
        Data_inf.parcelle_cadastrale='';

      }
      if(value=='Personne physique'){
        Data_ben.fonction='';
        Data_ben.raison_sociale='';
        Data_ben.siern='';
        Data_inf.same_adress=0;

        Data_inf.siern_travaux='';
        Data_inf.nom_travaux='';
        Data_inf.operationavec='Operation classique';
        Data_inf.adv_field = {
          //qpv
          code_quartier:'',
          //Bailleur Social
          raison_sociale:'',
          siern:'',
          nom:'',
          prenom:'',
          fonction:'',
          phone:'',
          adresse:'',
          code_postale:'',
          ville:'',

          nombre_total:'',
          nombre_total_menages:'',

          //Operation avec precarite
          adv_fiscal_data:[{
            numero_fiscal:'',
            reference_fiscal:'',
          }],
        };
      }

      this.props.UserStepStore.updateCalculatorData('beneficiaire', Data_ben);
      this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data_inf);
      if(removeError.length){
        this.props.UserStepStore.removeValidateErrorArray(removeError);
      }

      this.props.UserStepStore.updateCalculatorData('persone', value);
  }
  beforeChange=()=>{
    return new Promise<boolean>((resolve, reject)=>{
      let DataOperation=this.props.UserStepStore.dataOperation;
      if(DataOperation.length){
        this.props.UserStepStore.setVisibleConfirmModal({
            show:true,
            text:'Cette modification entraine l\'annulation des données precédement renseignées',
            title:'Confirm',
            YesTitle:'Oui',
            NonTitle:'Non',
            callbackYes:()=>{
              resolve(true);
              this.props.UserStepStore.updateDataOperation([]);
            },
            callbackNo:()=>{
              resolve(false);
            }
          });
      }else{
        resolve(true);
      }
    });
  }

  render(){
    return (
      <div className="col-md-12" style={{padding:'20px 15px 40px'}}>
        <CustomSwitcher
        data={[
          {value:'Personne morale', name:'Personne morale'},
          {value:'Personne physique', name:'Personne physique'},
        ]}
        value={this.props.UserStepStore.dataClaculator.persone}
        onChange={this.changeHandler}
        beforeChange={this.beforeChange}
        />
    </div>
    )
  }
}


interface IOrganizationFormImageProps {
  UserStepStore?:any,
  ToastrStore?:any,
  blockOrganization:boolean,
}
interface IOrganizationFormImageState {
  dataObj:any,
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class OrganizationFormImage extends React.Component<IOrganizationFormImageProps, IOrganizationFormImageState>{
  state={
    dataObj:[],
  }
  rootObj={
    uid:'d1069a32-efdf-41f7-a3da-bd416ae8cc7d',
    attr_name:'f3a88b54-40bc-4967-a2e1-923d36990ff4',
    attr_file:'896fe297-be94-4056-8ee8-07ae2635eada',
  }
  componentDidMount(){
      ct_apiview.getTableData({
        view: this.rootObj.uid,
        attr_sort: '',
        sort: 1,
        start: 1,
        num: 100000,
    }).then((res)=>{
          if(res.data.result===1){
            let DataOrg_by_pair=[];
            let index=0;
            let Dataorg=res.data.data.map((item)=>{
                let imgArr=item[this.rootObj.attr_file] && item[this.rootObj.attr_file].pvw?item[this.rootObj.attr_file].pvw.split(','):[];
                return {
                  name:item[this.rootObj.attr_name] && item[this.rootObj.attr_name].v?item[this.rootObj.attr_name].v:'',
                  uid:item[this.rootObj.attr_name] && item[this.rootObj.attr_name].o?item[this.rootObj.attr_name].o:'',
                  img:imgArr.length?imgArr[0]:'',
                };
              /*  if(DataOrg_by_pair[index] && DataOrg_by_pair[index].length<2){
                  DataOrg_by_pair[index].push(obj_org);
                  if(DataOrg_by_pair[index].length==2){
                    index=DataOrg_by_pair.length;
                  }
                }else{
                  DataOrg_by_pair.push([obj_org]);
                }*/
            });
            this.setState({dataObj:Dataorg});
          }
      })
  }
  choseOrganization=(uid, name)=>{
    let DataOperation=this.props.UserStepStore.dataOperation;
    if(DataOperation.length || this.props.blockOrganization){
      return false;
    }
    this.props.UserStepStore.updateCalculatorData('organization', {name:name,uid: uid});
  }
  render(){
    let chose_org= this.props.UserStepStore.dataClaculator.organization.uid;
    let DataOperation=this.props.UserStepStore.dataOperation;
  /*  let DataImg=this.state.dataObj.map((item)=>{
      let org_img= item.map((subitem)=>{
          return(<div onClick={(e)=>{this.choseOrganization(subitem.uid, subitem.name)}}  className={'header_img_item'+(chose_org==subitem.uid?' active':'')+(DataOperation.length?' disabled':'')}>
                    <img alt={subitem.name} src={(subitem.img?'/Object/getPreviewFile/'+subitem.img:'')} />
                </div>);
      });
      return <div className="header_img">{org_img}</div>;
    });*/
    let DataImg= this.state.dataObj.map((subitem)=>{
        return(<div key={subitem.uid+'_orgimg'} onClick={(e)=>{this.choseOrganization(subitem.uid, subitem.name)}}  className={'header_img_item'+(chose_org==subitem.uid?' active':'')+(DataOperation.length || this.props.blockOrganization?' disabled':'')}>
                  <img alt={subitem.name} src={(subitem.img?'/Object/getPreviewFile/'+subitem.img:'')} />
              </div>);
    });
    return (
      <div className="header_img">
        {DataImg}
      </div>
    )
  }
}


interface IDateFormProps {
  UserStepStore?:any,
  ToastrStore?:any,
  showOrganization:boolean,
  blockOrganization:boolean,
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class DateForm extends React.Component<IDateFormProps>{
  validate=(Data, id)=>{
    if(!Data.date_de_signature){
        this.props.UserStepStore.addValidateError({id:'date_de_signature'});
    }else if(Data.date_de_signature && moment(Data.date_de_signature, DateFormat).toDate()>new Date()){
      this.props.UserStepStore.addValidateError({id:'date_de_signature', id_msg:'error_future_date'});
    }else{
      this.props.UserStepStore.removeValidateError('date_de_signature');
    }

    let max_day_diff=0;
    let DataOperation=this.props.UserStepStore.dataOperation;
    DataOperation.forEach((item)=>{
      if(item.day_between_date && (!max_day_diff || item.day_between_date>max_day_diff)){
        max_day_diff=item.day_between_date;
      }
    })

    if(!Data.date_de_debut){
      this.props.UserStepStore.addValidateError({id:'date_de_debut'});
    }else if(Data.date_de_debut && moment(Data.date_de_debut, DateFormat).toDate()>new Date()){
      this.props.UserStepStore.addValidateError({id:'date_de_debut', id_msg:'error_future_date'});
    }else if(Data.date_de_debut && Data.date_de_signature && moment(Data.date_de_signature, DateFormat).toDate()>moment(Data.date_de_debut, DateFormat).toDate()){
      this.props.UserStepStore.addValidateError({id:'date_de_debut'});
    }else if(max_day_diff && Data.date_de_debut && Data.date_de_signature && moment(Data.date_de_debut, DateFormat).toDate()>=moment(Data.date_de_signature, DateFormat).toDate() && moment(Data.date_de_debut, DateFormat).diff(moment(Data.date_de_signature, DateFormat), 'days')<max_day_diff){
      this.props.UserStepStore.addValidateError({id:'date_de_debut', type:'max_day_diff'});
    }else{
      this.props.UserStepStore.removeValidateError('date_de_debut');
    }

    if(!Data.finDeTravaux){
      this.props.UserStepStore.addValidateError({id:'finDeTravaux'});
    }else if(Data.finDeTravaux && moment(Data.finDeTravaux, DateFormat).toDate()>new Date()){
      this.props.UserStepStore.addValidateError({id:'finDeTravaux', id_msg:'error_future_date'});
    }else if(Data.finDeTravaux && Data.date_de_debut && moment(Data.date_de_debut, DateFormat).toDate()>moment(Data.finDeTravaux, DateFormat).toDate()){
      this.props.UserStepStore.addValidateError({id:'finDeTravaux'});
    }else if(Data.finDeTravaux && Data.date_facture && moment(Data.finDeTravaux, DateFormat).toDate()>moment(Data.date_facture, DateFormat).toDate()){
      this.props.UserStepStore.addValidateError({id:'finDeTravaux'});
    }else{
      this.props.UserStepStore.removeValidateError('finDeTravaux');
    }

    if(Data.date_facture && moment(Data.date_facture, DateFormat).toDate()>new Date()){
      this.props.UserStepStore.addValidateError({id:'date_facture', id_msg:'error_future_date'});
    }else if(Data.date_de_signature && Data.date_facture && moment(Data.date_de_signature, DateFormat).toDate()>moment(Data.date_facture, DateFormat).toDate()){
      this.props.UserStepStore.addValidateError({id:'date_facture'});
    }else if(Data.date_de_debut && Data.date_facture && moment(Data.date_de_debut, DateFormat).toDate()>moment(Data.date_facture, DateFormat).toDate()){
      this.props.UserStepStore.addValidateError({id:'date_facture'});
    }else{
      this.props.UserStepStore.removeValidateError('date_facture');
    }
  }
  handleChange=(e)=>{
    let {id, value}=e.currentTarget;
    let Data=this.props.UserStepStore.dataClaculator.date;
    Data={...Data, [id]:value};
    this.props.UserStepStore.updateCalculatorData('date', Data);
    this.validate(Data, id);
  }
  handleChangeDate=(id, value)=>{
    let Data=this.props.UserStepStore.dataClaculator.date;
    Data={...Data, [id]:value};
    this.props.UserStepStore.updateCalculatorData('date', Data);
    this.validate(Data, id);
  }
  render(){
    let Data=this.props.UserStepStore.dataClaculator.date;
    let DataOperation=this.props.UserStepStore.dataOperation;
    let errors_data= this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    return (
      <React.Fragment>
        {this.props.showOrganization?<OrganizationFormImage blockOrganization={this.props.blockOrganization} />:null}
        <div className="title_step">Renseignez les différentes dates liées au projet</div>
        <form className="date__form">
          <div className="row group_input">
            <div className="col-md-6">
                <div className={'form-group '+(error.indexOf('date_de_signature')!==-1?'validate_error':'')}>
                  <CustomDatePicker id="date_de_signature" value={Data.date_de_signature} label={'Date de signature du devis*'} handleChange={this.handleChangeDate} disabled={DataOperation.length} />
                  {getErrorMsg(errors_data, 'date_de_signature')}
                </div>
            </div>
           </div>
           <div className="row group_input required">
             <div className="col-md-6">
                 <div className={'form-group '+(error.indexOf('date_de_debut')!==-1?'validate_error':'')}>
                   <CustomDatePicker id="date_de_debut" value={Data.date_de_debut} label={'Date de début des travaux'} handleChange={this.handleChangeDate} />
                   {getErrorMsg(errors_data, 'date_de_debut')}
                 </div>
             </div>
             <div className="col-md-6">
                 <div className={'form-group '+(error.indexOf('finDeTravaux')!==-1?'validate_error':'')}>
                   <CustomDatePicker id="finDeTravaux" value={Data.finDeTravaux} label={'Date fin de travaux'} handleChange={this.handleChangeDate} />
                   {getErrorMsg(errors_data, 'finDeTravaux')}
                 </div>
             </div>
            </div>
          <div className="row group_input">
            <div className="col-md-6">
              <div className={'form-group '+(error.indexOf('date_facture')!==-1?'validate_error':'')}>
                <CustomDatePicker id="date_facture" value={Data.date_facture} handleChange={this.handleChangeDate} disabled={false} label={'Date de la facture'} />
                {getErrorMsg(errors_data, 'date_facture')}
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <input onChange={this.handleChange} id="numero_facture" value={Data.numero_facture} placeholder="Numéro de facture" className="form-control"/>
                <label><span className="text">Numéro de facture</span><span className="label_ph"></span></label>
                {getErrorMsg(errors_data, 'numero_facture')}
              </div>
            </div>
          </div>
        </form>
      </React.Fragment>
    )
  }
}


interface IInformationFormProps {
  UserStepStore?:any,
  ToastrStore?:any,
}
interface IInformationFormState {
  animation:{
    opacity:number,
    height:any,
  }
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class InformationForm extends React.Component<IInformationFormProps, IInformationFormState>{
  private anim_elem : React.RefObject<HTMLDivElement>;
  constructor(props){
    super(props);
    this.state={
      animation:{
        opacity:0,
        height:'auto',
      }
    }
    this.anim_elem=React.createRef();
  }
  componentDidMount(){
    this.animation();
  }
  componentWillUnmount(){
    this.props.UserStepStore.setAnimObj({clientHeight:this.anim_elem.current.clientHeight, scrollTop:document.getElementsByClassName('step_form_body')[0].scrollTop});
  }
  animation=()=>{
    let anim_height=this.anim_elem.current.scrollHeight;
    let animation=this.props.UserStepStore.animation;
    return new Promise((resolve, reject)=>{
      this.setState({animation:{...this.state.animation, height:animation.clientHeight}}, ()=>{
        //document.getElementsByClassName('step_form_body')[0].scrollTop=animation.scrollTop;
        setTimeout(()=>{
          this.setState({animation:{...this.state.animation, opacity:1, height:anim_height}}, ()=>{
            document.getElementsByClassName('userstep_form')[0].scrollTo({behavior: "smooth", top:0});
            setTimeout(()=>{
              this.setState({animation:{...this.state.animation, height:'auto'}}, ()=>{
                resolve(true);
              });
            }, 700);
          });
        }, 100)
      });
    })
  }
  handleChange=(e)=>{
    let {id, value, name}=e.currentTarget;
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let key = id || name;
    Data={...Data, [key]:value};
    this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data);
    this.validate(Data, id);
  }
  selectRadio=(value, id)=>{
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    Data={...Data, [id]:value};
    if(id=='same_adress' && value==1){
      let Data_ben=this.props.UserStepStore.dataClaculator.beneficiaire;
      Data.adresse_travaux=Data_ben.adresse;
      Data.code_postale_travaux=Data_ben.code_postale;
      Data.ville_travaux=Data_ben.ville;
    }
    this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data);
    this.validate(Data, id);
  }
  radioBeforechange=()=>{
    return new Promise<boolean>((resolve, reject)=>{
      let DataOperation=this.props.UserStepStore.dataOperation;
      if(DataOperation.length){
        this.props.UserStepStore.setVisibleConfirmModal({
            show:true,
            text:'Cette modification entraine l\'annulation des données precédement renseignées',
            title:'Confirm',
            YesTitle:'Oui',
            NonTitle:'Non',
            callbackYes:()=>{
              resolve(true);
              this.props.UserStepStore.updateDataOperation([]);
            },
            callbackNo:()=>{
              resolve(false);
            }
          });
      }else{
        resolve(true);
      }
    });
  }
  handleChangeAdvField=(e)=>{
    let {id, value, name}=e.currentTarget;
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let key = id || name;
    Data.adv_field={...Data.adv_field, [key]:value};
    this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data);
    this.validate(Data, id);
  }
  handleChangeAdvFiscalField=(e, i)=>{
    let {id, value, name}=e.currentTarget;
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let key = id || name;
    Data.adv_field.adv_fiscal_data[i]={...Data.adv_field.adv_fiscal_data[i], [key]:value};
    if(i==Data.adv_field.adv_fiscal_data.length-1){
      Data.adv_field.adv_fiscal_data.push({
        numero_fiscal:'',
        reference_fiscal:'',
      })
    }
    if(!Data.adv_field.adv_fiscal_data[i].numero_fiscal && !Data.adv_field.adv_fiscal_data[i].reference_fiscal){
      Data.adv_field.adv_fiscal_data=Data.adv_field.adv_fiscal_data.filter((item, i_item)=>{
        return i==i_item?false:true;
      })
    }
    this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data);
    this.validate(Data, id);
  }
  getSIRN=(e)=>{
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let valueSirn=Data.siern_travaux.toString().replace(/ /g, '');
    if(valueSirn){
        axios.get(
            '/dop/stepFormGetInfoPoSiren?siren='+valueSirn, {
                headers: { 'content-type': 'application/x-www-form-urlencoded' }
            }
        ).then((res)=>{
          if(res.data.result==1){
              Data.adresse_travaux=res.data.data.Adresse;
              Data.code_postale_travaux=res.data.data['Code postale'];
              Data.ville_travaux=res.data.data.Ville;
              Data.nom_travaux=res.data.data['Raison Socliale'];
              if(Data.operationavec=='Operation classique' || Data.operationavec=='QPV'){
                let remove_oper=false;
                if(Data.operationavec!='QPV'){
                  remove_oper=true;
                }
                this.checkQpv(Data).then((DataQpv)=>{
                  let DataOperation=this.props.UserStepStore.dataOperation;
                  if(remove_oper && DataQpv.operationavec=='QPV' && DataOperation.length){
                    this.props.UserStepStore.setVisibleConfirmModal({
                        show:true,
                        text:'Cette modification entraine l\'annulation des données precédement renseignées',
                        title:'Confirm',
                        YesTitle:'Oui',
                        NonTitle:'Non',
                        callbackYes:()=>{
                          this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', DataQpv);
                          this.props.UserStepStore.updateDataOperation([]);
                        },
                        callbackNo:()=>{

                        }
                      });
                  }else{
                    this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', DataQpv);
                  }
                });
              }else{
                this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data);
              }
          }else{
            this.props.ToastrStore.error('Error!', 'Error');
          }
          this.validate(Data, null);
        }).catch(()=>{
          this.props.ToastrStore.error('Error!', 'Error');
        });
    }
  }
  getSIRNAdv=(e)=>{
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let valueSirn=Data.adv_field.siern.toString().replace(/ /g, '');
    if(valueSirn){
        axios.get(
            '/dop/stepFormGetInfoPoSiren?siren='+valueSirn, {
                headers: { 'content-type': 'application/x-www-form-urlencoded' }
            }
        ).then((res)=>{
          if(res.data.result==1){
              Data.adv_field.adresse=res.data.data.Adresse;
              Data.adv_field.code_postale=res.data.data['Code postale'];
              Data.adv_field.ville=res.data.data.Ville;
              Data.adv_field.raison_sociale=res.data.data['Raison Socliale'];
              this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data);
          }else{
            this.props.ToastrStore.error('Error!', 'Error');
          }
        }).catch(()=>{
          this.props.ToastrStore.error('Error!', 'Error');
        });
    }
  }
  validate=(Data, id)=>{
    if(id=='nom_travaux'){
      if(!Data.nom_travaux){
        this.props.UserStepStore.addValidateError({id:'nom_travaux'});
      }else{
        this.props.UserStepStore.removeValidateError('nom_travaux');
      }


    }
    if(id=='code_postale_travaux'){
      if(!Data.code_postale_travaux){
        this.props.UserStepStore.addValidateError({id:'code_postale_travaux'});
      }else if(Data.code_postale_travaux.toString().length!==5){
        this.props.UserStepStore.addValidateError({id:'code_postale_travaux'});
      }else{
        this.props.UserStepStore.removeValidateError('code_postale_travaux');
      }
    }
    if(id=='siern_travaux'){
      if(Data.siern_travaux && Data.siern_travaux.toString().replace(/ /g, '').length!=9){
        this.props.UserStepStore.addValidateError({id:'siern_travaux'});
      }else{
        this.props.UserStepStore.removeValidateError('siern_travaux');
      }
    }
    if(id=='adresse_travaux'){
      if(!Data.adresse_travaux){
        this.props.UserStepStore.addValidateError({id:'adresse_travaux'});
      }else{
        this.props.UserStepStore.removeValidateError('adresse_travaux');
      }
    }
    if(id=='ville_travaux'){
      if(!Data.ville_travaux){
        this.props.UserStepStore.addValidateError({id:'ville_travaux'});
      }else{
        this.props.UserStepStore.removeValidateError('ville_travaux');
      }
    }
    if(id=='parcelle_cadastrale'){
      if(!Data.parcelle_cadastrale){
        this.props.UserStepStore.addValidateError({id:'parcelle_cadastrale'});
      }else{
        this.props.UserStepStore.removeValidateError('parcelle_cadastrale');
      }
    }
  }
  renderAdvInputs=(errors_data)=>{
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let error=errors_data.map((error_item)=>{return error_item.id});
    if(Data.operationavec=='QPV'){
      return (
        <div className="adv_input_cont">
          <div className="row group_input">
              <div className="col-md-12">
                <div className={'required form-group '+(error.indexOf('adv_field-code_quartier')!==-1?'validate_error':'')}>
                  <div className="input-group">
                    <input onChange={this.handleChangeAdvField} id="code_quartier" value={Data.adv_field.code_quartier} className="form-control"/>
                    <label><span className="text">Code quartier du quartier prioritaire de la politique de la ville</span><span className="label_ph"></span></label>
                    <div className="input-group-append"><button onClick={()=>{
                      let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
                      this.checkQpv(Data).then((DataQpv)=>{
                        this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', DataQpv);
                      });
                    }} className="btn" type="button">Valider</button></div>
                  </div>
                  {getErrorMsg(errors_data, 'adv_field-code_quartier')}
                </div>
              </div>
          </div>
        </div>
      );
    }else if(Data.operationavec=='Bailleur Social'){
      return (
        <div className="adv_input_cont">
          <div className="row group_input">
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-siern')!==-1?'validate_error':'')}>
                  <div className="input-group">
                    <NumberInput onChange={this.handleChangeAdvField} onlyDigits={true} allowSpace={true} value={Data.adv_field.siern} AdvProps={{id:"siern", className:"form-control", placeholder:"Numéro SIREN du bailleur social"}}/>
                    <label><span className="text">Numéro SIREN du bailleur social</span><span className="label_ph"></span></label>
                    <div className="input-group-append">
                      <button onClick={this.getSIRNAdv} className="btn" type="button">Valider</button>
                    </div>
                  </div>
                  {getErrorMsg(errors_data, 'adv_field-siern')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-raison_sociale')!==-1?'validate_error':'')}>
                  <input placeholder="Raison sociale du bailleur social" onChange={this.handleChangeAdvField} id="raison_sociale" value={Data.adv_field.raison_sociale} className="form-control"/>
                  <label><span className="text">Raison sociale du bailleur social</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-raison_sociale')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-nom')!==-1?'validate_error':'')}>
                  <input placeholder="Nom du signataire" onChange={this.handleChangeAdvField} id="nom" value={Data.adv_field.nom} className="form-control"/>
                  <label><span className="text">Nom du signataire</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-nom')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-prenom')!==-1?'validate_error':'')}>
                  <input placeholder="Prénom du signataire" onChange={this.handleChangeAdvField} id="prenom" value={Data.adv_field.prenom} className="form-control"/>
                  <label><span className="text">Prénom du signataire</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-prenom')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-fonction')!==-1?'validate_error':'')}>
                  <input placeholder="Fonction du signataire" onChange={this.handleChangeAdvField} id="fonction" value={Data.adv_field.fonction} className="form-control"/>
                  <label><span className="text">Fonction du signataire</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-fonction')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-phone')!==-1?'validate_error':'')}>
                  <NumberInput onChange={this.handleChangeAdvField} value={Data.adv_field.phone} onlyDigits={true} AdvProps={{id:"phone", className:"form-control", placeholder:"06 23 45 67 89"}}/>
                  <label><span className="text">Numero de telephone</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-phone')}
                </div>
              </div>
              <div className="col-md-12">
                <div className={'required form-group '+(error.indexOf('adv_field-adresse')!==-1?'validate_error':'')}>
                  <input placeholder="Adresse" onChange={this.handleChangeAdvField} id="adresse" value={Data.adv_field.adresse} className="form-control"/>
                  <label><span className="text">Adresse</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-adresse')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-code_postale')!==-1?'validate_error':'')}>
                  <NumberInput onChange={this.handleChangeAdvField} value={Data.adv_field.code_postale} onlyDigits={true} AdvProps={{id:"code_postale", className:"form-control", placeholder:"Code postale"}}/>
                  <label><span className="text">Code postale</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-code_postale')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-ville')!==-1?'validate_error':'')}>
                  <input placeholder="Ville" onChange={this.handleChangeAdvField} id="ville" value={Data.adv_field.ville} className="form-control"/>
                  <label><span className="text">Ville</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-ville')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-nombre_total')!==-1?'validate_error':'')}>
                  <input placeholder="le nombre total de ménages concernés par l'opération" onChange={this.handleChangeAdvField} id="nombre_total" value={Data.adv_field.nombre_total} className="form-control"/>
                  <label title="le nombre total de ménages concernés par l'opération"><span className="text">le nombre total de ménages concernés par l'opération</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-nombre_total')}
                </div>
              </div>
              <div className="col-md-6">
                <div className={'required form-group '+(error.indexOf('adv_field-nombre_total_menages')!==-1?'validate_error':'')}>
                  <input placeholder="Nombre total de ménages" onChange={this.handleChangeAdvField} id="nombre_total_menages" value={Data.adv_field.nombre_total_menages} className="form-control"/>
                  <label title="Nombre total de ménages"><span className="text">Nombre total de ménages</span><span className="label_ph"></span></label>
                  {getErrorMsg(errors_data, 'adv_field-nombre_total_menages')}
                </div>
              </div>
          </div>
        </div>
      );
    }else if(Data.operationavec=='Operation avec precarite'){
      let input_data=Data.adv_field.adv_fiscal_data.map((item, i)=>{
        return (
          <React.Fragment key={'adv_field_'+i}>
            <div className="col-md-6">
              <div className={'form-group'}>
                <input onChange={(e)=>{this.handleChangeAdvFiscalField(e, i)}} name='numero_fiscal' value={item.numero_fiscal} className="form-control"/>
                <label><span className="text">Numero Fiscal</span><span className="label_ph"></span></label>
              </div>
            </div>
            <div className="col-md-6">
              <div className={'form-group'}>
                <input onChange={(e)=>{this.handleChangeAdvFiscalField(e, i)}} name='reference_fiscal'  value={item.reference_fiscal} className="form-control"/>
                <label><span className="text">Reference Fiscal</span><span className="label_ph"></span></label>
              </div>
            </div>
          </React.Fragment>
        );
      })
      return(
        <div className="adv_input_cont">
          <div className="row group_input">
            {input_data}
          </div>
      </div>  );
    }

  }

  checkQpv=(Data)=>{
    let address= Data.adresse_travaux+' '+Data.code_postale_travaux+' '+Data.ville_travaux;
    let digit_part= address.substr(0, address.indexOf(' '));
    let digit_part_arr=digit_part.split(/\,|-/gi);
    if(digit_part_arr.length){
      address=digit_part_arr[digit_part_arr.length-1]+address.substr(address.indexOf(' '));
    }
    return axios.get(
        '/Dop/territoryCode',
        {
            params: {address:address},
        }
    ).then((res)=>{
      let DataNew=JSON.parse(JSON.stringify(Data))
      if(res.data.result==1 && res.data.data.code.code){
        DataNew.operationavec='QPV';
        DataNew.adv_field.code_quartier=res.data.data.code.code;
      }
      return DataNew;
    })
  }

  renderType=(Data, errors_data)=>{
    return(<React.Fragment>
      <div style={{marginBottom:30}} className="row radio_group">
        <div className="col-md-12 group_label">
          <div className="sub_title_step">
            Type d'operation
          </div>
        </div>
        <div className="col-md-12 radio_cont">
        <CustomRadio
          id={'operationavec'}
          value={[Data.operationavec]}
          data={[
            {value:'Operation classique', name:'Operation classique'},
            {value:'QPV', name:'QPV'},
            {value:'Bailleur Social', name:'Bailleur Social'},
            {value:'Operation avec precarite', name:'Operation avec precarite'},
          ]}
          onChange={this.selectRadio}
          display={'responsive'}
          type={'radio'}
          beforeChange={this.radioBeforechange}
        />
        </div>
      </div>
      {this.renderAdvInputs(errors_data)}
      </React.Fragment>)
  }
  renderAdress=(Data, errors_data)=>{
    let error=errors_data.map((error_item)=>{return error_item.id});
    let typePersone=this.props.UserStepStore.dataClaculator.persone;
    return(<React.Fragment>
          <div className="col-md-12">
            <div className={'form-group required '+(error.indexOf('adresse_travaux')!==-1?'validate_error':'')}>
              <input disabled={(Data.same_adress==1?true:false)}  placeholder="Adresse des travaux" onChange={this.handleChange} id="adresse_travaux" value={Data.adresse_travaux} className="form-control"/>
              <label><span className="text">Adresse des travaux</span><span className="label_ph"></span></label>
              {getErrorMsg(errors_data, 'adresse_travaux')}
            </div>
          </div>
          <div className="col-md-6">
            <div className={'form-group required '+(error.indexOf('code_postale_travaux')!==-1?'validate_error':'')}>
              <NumberInput onChange={this.handleChange} value={Data.code_postale_travaux} onlyDigits={true} AdvProps={{id:"code_postale_travaux", className:"form-control", placeholder:"Code postale des travaux",  disabled:(Data.same_adress==1?true:false)}}/>
              <label><span className="text">Code postale des travaux</span><span className="label_ph"></span></label>
              {getErrorMsg(errors_data, 'code_postale_travaux')}
            </div>
          </div>
          <div className="col-md-6">
            <div className={'form-group required '+(error.indexOf('ville_travaux')!==-1?'validate_error':'')}>
              <input  disabled={(Data.same_adress==1?true:false)} placeholder="Ville des travaux" onChange={this.handleChange} id="ville_travaux" value={Data.ville_travaux} className="form-control"/>
              <label><span className="text">Ville des travaux</span><span className="label_ph"></span></label>
              {getErrorMsg(errors_data, 'ville_travaux')}
            </div>
          </div>
          {typePersone==='Personne physique' && this.renderParcelle(Data, error, errors_data)}
      </React.Fragment>)
  }
  renderParcelle=(Data, error, errors_data)=>{
    return (<div className="col-md-12">
      <div className={'form-group required '+(error.indexOf('parcelle_cadastrale')!==-1?'validate_error':'')}>
        <input placeholder="Parcelle Cadastrale" onChange={this.handleChange} id="parcelle_cadastrale" value={Data.parcelle_cadastrale} className="form-control"/>
        <label><span className="text">Parcelle Cadastrale</span><span className="label_ph"></span></label>
        {getErrorMsg(errors_data, 'parcelle_cadastrale')}
      </div>
    </div>)
  }

  renderMorale=(Data, errors_data)=>{
    let error=errors_data.map((error_item)=>{return error_item.id});
    return(<React.Fragment>
      <div className="col-md-6">
        <div className={'form-group '+(error.indexOf('siern_travaux')!==-1?'validate_error':'')}>
          <div className="input-group">
            <NumberInput onChange={this.handleChange} value={Data.siern_travaux} onlyDigits={true} allowSpace={true} AdvProps={{id:"siern_travaux", className:"form-control"}}/>
            <label><span className="text">Numero de SIREN</span><span className="label_ph"></span></label>
            <div className="input-group-append">
              <button onClick={this.getSIRN} className="btn" type="button">Valider</button>
            </div>
          </div>
          {getErrorMsg(errors_data, 'siern_travaux')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'required form-group '+(error.indexOf('nom_travaux')!==-1?'validate_error':'')}>
          <div style={{position:'absolute', top:-22, right:20}}><CustomTolltip style={{backgroundColor:'white', color:'black'}} position={'top-right'} size={20}>Si la copropriété n’a pas de nom merci d’indiquer «RESIDENCE suivi de l’adresse»  sans le code postal ni la ville</CustomTolltip></div>
          <input onChange={this.handleChange} id="nom_travaux" value={Data.nom_travaux} className="form-control"/>
          <label><span className="text">Nom du site des travaux ou nom de la copropriété</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'nom_travaux')}
        </div>
      </div>
      </React.Fragment>);
  }
  renderPhys=(Data)=>{
    return(<React.Fragment>
      <div className="radio_group">
        <div className="col-md-12">
          <div style={{marginTop:5}} className="sub_title_step">Adresse des travaux
            <div className="regular_font" style={{fontSize:22}}>Est ce que l'adresse des travaux correspond à l'adresse du beneficiaire</div>
          </div>
        </div>
        <div className="col-md-12 radio_cont">
          <CustomRadio
            id={'same_adress'}
            value={[Data.same_adress]}
            data={[
              {value:'1', name:'Oui'},
              {value:'0', name:'Non'},
            ]}
            onChange={this.selectRadio}
            display={'inline'}
            type={'radio'}
          />
        </div>
      </div>
      </React.Fragment>)
  }

  render(){
    let Data=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let errors_data= this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    let typePersone=this.props.UserStepStore.dataClaculator.persone;
    return (
      <div ref={this.anim_elem} style={this.state.animation} className={'form_item'}>
        <div className="title_step">INFORMATIONS LIEUX DES TRAVAUX</div>
        <form className="information__form">
          <div className="row group_input">
            {typePersone==='Personne morale' && this.renderMorale(Data, errors_data)}
          </div>
          {typePersone==='Personne morale' && this.renderType(Data, errors_data)}
          <div className="row group_input">
            {this.renderPhys(Data)}
          </div>
          <div className="row group_input">
          {this.renderAdress(Data, errors_data)}
          </div>
        </form>
      </div>
    )
  }
}

interface IBeneficiaireFormProps {
  UserStepStore?:any,
  ToastrStore?:any,
}
interface IBeneficiaireFormState {
  animation:{
    opacity:number,
    height:any,
  }
  data_privileges:any,
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class BeneficiaireForm extends React.Component<IBeneficiaireFormProps, IBeneficiaireFormState>{
  private anim_elem : React.RefObject<HTMLDivElement>;
  constructor(props){
    super(props);
    this.state={
      data_privileges:null,
      animation:{
        opacity:0,
        height:'auto',
      }
    };
    this.anim_elem=React.createRef();
  }

  componentDidMount(){
    this.animation();

    ct_apiobj.getChildObjValues('00000000-0000-0000-0000-000000000000', '63ec62f9-9b37-4daf-903f-fa03bda47f48').then((res)=>{
      if(res.data.result==1){
        this.setState({data_privileges:res.data.data});
      }
    })
  }
  componentWillUnmount(){
    this.props.UserStepStore.setAnimObj({clientHeight:this.anim_elem.current.clientHeight, scrollTop:document.getElementsByClassName('step_form_body')[0].scrollTop});
  }

  animation=()=>{
    let anim_height=this.anim_elem.current.scrollHeight;
    let animation=this.props.UserStepStore.animation;
    return new Promise((resolve, reject)=>{
      this.setState({animation:{...this.state.animation, height:animation.clientHeight}}, ()=>{
        //document.getElementsByClassName('step_form_body')[0].scrollTop=animation.scrollTop;
        setTimeout(()=>{
          this.setState({animation:{...this.state.animation, opacity:1, height:anim_height}}, ()=>{
            document.getElementsByClassName('userstep_form')[0].scrollTo({behavior: "smooth", top:0});
            setTimeout(()=>{
              this.setState({animation:{...this.state.animation, height:'auto'}}, ()=>{
                resolve(true);
              });
            }, 700);
          });
        }, 100)
      });
    })
  }

  handleChange=(e)=>{
    let {id, value, name}=e.currentTarget;
    let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
    let Data_info=this.props.UserStepStore.dataClaculator.information_lieu_travaux;
    let key = id || name;
    Data={...Data, [key]:value};
    this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
    if(id=='code_postale'){
      this.checkPrivileges(Data);
    }
    if(Data_info.same_adress==1 && (key=='adresse' || key=='code_postale' || key=='ville')){
      Data_info.adresse_travaux=Data.adresse;
      Data_info.code_postale_travaux=Data.code_postale;
      Data_info.ville_travaux=Data.ville;
      this.props.UserStepStore.updateCalculatorData('information_lieu_travaux', Data_info);
    }
    this.validate(Data, id);
  }
  handleChangeDate=(id, value)=>{
    let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
    Data={...Data, [id]:value};
    if(Data.persone_phys_data.length){
      let yer_previsite=Number(moment(value, DateFormat).year());
      Data.persone_phys_data.forEach((item)=>{
        if(item.year && Number(item.year)<(yer_previsite-1)){
          item.year_error=true;
        }else{
          item.year_error=false;
        }
      });
    }
    this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
    this.validate(Data, id);
  }

  checkPrivileges=(Data_ben)=>{
      const uid_data={
        start_uid:'447beb6e-3921-48c6-8abf-1332a1b3a0a4',
        end_uid:'4b2eb476-b0f6-4f94-b469-213d6adac1da',

        count_pers_uid:'d79816d5-f099-44f7-9122-d9648716adce',
        fr_a_revenue_uid:'33ac3cf6-de43-4c50-83fc-02267b13da85',
        fr_b_revenue_uid:'d8c6f27f-b470-44d8-84fb-f663c689db33',
        a_revenue_uid:'30db2466-0a3f-497c-8980-00b9cdbe0aa7',
        b_revenue_uid:'eae6fb5c-ca1f-4807-8cfe-3c7cc8a34644',
      };
      const getValueFromParam=(param)=>{
          if(param.datatype=='text' && param.value){
            return param.value.value;
          }
          if(param.datatype=='object' && param.value){
            return param.value;
          }
          if(param.datatype=='datetime' && param.value){
            return param.value.value.split(' ')[0];
          }
          if(param.datatype=='number' && param.value){
            return Number(param.value.value);
          }

          return '';
        }
      let Data =this.props.UserStepStore.dataClaculator;
      if(Data.date.date_de_signature && Data_ben.number_de_perosne!=undefined && Data_ben.code_postale && (Data_ben.revenue || Data_ben.revenue===0)){
        let date_signat=moment(Data.date.date_de_signature, DateFormat).toDate();
        let Obj_data= this.state.data_privileges.find((obj)=>{
          let attr_start=obj.attributes.find((attr)=>{
            return attr.uid==uid_data.start_uid?true:false;
          })
          let attr_end=obj.attributes.find((attr)=>{
            return attr.uid==uid_data.end_uid?true:false;
          })
          if(attr_start && attr_end){
            let start_data=getValueFromParam(attr_start);
            start_data=moment(start_data, DateFormat).toDate();
            let end_data=getValueFromParam(attr_end);
            end_data=moment(end_data, DateFormat).toDate();
            if(date_signat>=start_data && date_signat<=end_data){
              return true;
            }
          }
          return false;
        });
        if(Obj_data){
          if(Data_ben.number_de_perosne>5){
            let row_obj_5=Obj_data.children.find((child_obj)=>{
              let nuber_pers_attr=child_obj.attributes.find((attr)=>{
                return attr.uid==uid_data.count_pers_uid?true:false;
              })
              if(nuber_pers_attr && getValueFromParam(nuber_pers_attr)==5){
                return true;
              }
              return false;
            })
            let row_ob_0=Obj_data.children.find((child_obj)=>{
              let nuber_pers_attr=child_obj.attributes.find((attr)=>{
                return attr.uid==uid_data.count_pers_uid?true:false;
              })
              if(nuber_pers_attr && getValueFromParam(nuber_pers_attr)==0){
                return true;
              }
              return false;
            })
            if(row_obj_5 && row_ob_0){
              let fr_index=['75', '77','78','91', '92', '93', '94', '95'];
              let a_param_5=null;
              let b_param_5=null;
              let a_param_0=null;
              let b_param_0=null;
              if(fr_index.indexOf(Data_ben.code_postale.toString().slice(0,2))!==-1){
                 a_param_5=row_obj_5.attributes.find((attr)=>{
                   return attr.uid==uid_data.fr_a_revenue_uid?true:false;
                 })
                 b_param_5=row_obj_5.attributes.find((attr)=>{
                   return attr.uid==uid_data.fr_b_revenue_uid?true:false;
                 })

                 a_param_0=row_ob_0.attributes.find((attr)=>{
                   return attr.uid==uid_data.fr_a_revenue_uid?true:false;
                 })
                 b_param_0=row_ob_0.attributes.find((attr)=>{
                   return attr.uid==uid_data.fr_b_revenue_uid?true:false;
                 })
              }else{
                a_param_5=row_obj_5.attributes.find((attr)=>{
                  return attr.uid==uid_data.a_revenue_uid?true:false;
                })
                b_param_5=row_obj_5.attributes.find((attr)=>{
                  return attr.uid==uid_data.b_revenue_uid?true:false;
                })

                a_param_0=row_ob_0.attributes.find((attr)=>{
                  return attr.uid==uid_data.a_revenue_uid?true:false;
                })
                b_param_0=row_ob_0.attributes.find((attr)=>{
                  return attr.uid==uid_data.b_revenue_uid?true:false;
                })
              }
              if(a_param_5 && b_param_5 && a_param_0 && b_param_0){
                let A = getValueFromParam(a_param_5)+((Data_ben.number_de_perosne-5)*getValueFromParam(a_param_0));
                let B = getValueFromParam(b_param_5)+((Data_ben.number_de_perosne-5)*getValueFromParam(b_param_0));
                if(Data_ben.revenue<A){
                  Data_ben.TableAflag=1;
                  Data_ben.TableBflag=0;
                }
                if(Data_ben.revenue>A && Data_ben.revenue<B){
                  Data_ben.TableAflag=0;
                  Data_ben.TableBflag=1;
                }
                if(Data_ben.revenue>B){
                  Data_ben.TableAflag=0;
                  Data_ben.TableBflag=0;
                }
                this.props.UserStepStore.updateCalculatorData('beneficiaire', Data_ben);
              }

            }
          }else{
            let row_obj=Obj_data.children.find((child_obj)=>{
              let nuber_pers_attr=child_obj.attributes.find((attr)=>{
                return attr.uid==uid_data.count_pers_uid?true:false;
              })
              if(nuber_pers_attr && getValueFromParam(nuber_pers_attr)==Data_ben.number_de_perosne){
                return true;
              }
              return false;
            })
            if(row_obj){
              let fr_index=['75', '77','78','91', '92', '93', '94', '95'];
              let a_param=null;
              let b_param=null;
              if(fr_index.indexOf(Data_ben.code_postale.toString().slice(0,2))!==-1){
                 a_param=row_obj.attributes.find((attr)=>{
                   return attr.uid==uid_data.fr_a_revenue_uid?true:false;
                 })
                 b_param=row_obj.attributes.find((attr)=>{
                   return attr.uid==uid_data.fr_b_revenue_uid?true:false;
                 })
              }else{
                a_param=row_obj.attributes.find((attr)=>{
                  return attr.uid==uid_data.a_revenue_uid?true:false;
                })
                b_param=row_obj.attributes.find((attr)=>{
                  return attr.uid==uid_data.b_revenue_uid?true:false;
                })
              }
              if(a_param && b_param ){
                if(Data_ben.revenue<getValueFromParam(a_param)){
                  Data_ben.TableAflag=1;
                  Data_ben.TableBflag=0;
                }
                if(Data_ben.revenue>getValueFromParam(a_param) && Data_ben.revenue<getValueFromParam(b_param)){
                  Data_ben.TableAflag=0;
                  Data_ben.TableBflag=1;
                }
                if(Data_ben.revenue>getValueFromParam(b_param)){
                  Data_ben.TableAflag=0;
                  Data_ben.TableBflag=0;
                }
                this.props.UserStepStore.updateCalculatorData('beneficiaire', Data_ben);
              }

            }
          }

        }
      }

  }

  getSIRN=(e)=>{
    let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
    let valueSirn=Data.siern.toString().replace(/ /g, '');
    if(valueSirn){
        axios.get(
            '/dop/stepFormGetInfoPoSiren?siren='+valueSirn, {
                headers: { 'content-type': 'application/x-www-form-urlencoded' }
            }
        ).then((res)=>{
          if(res.data.result==1){
              Data.adresse=res.data.data.Adresse;
              Data.code_postale=res.data.data['Code postale'];
              Data.ville=res.data.data.Ville;
              Data.raison_sociale=res.data.data['Raison Socliale'];
              this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
          }else{
            this.props.ToastrStore.error('Error!', 'Error');
          }
        }).catch(()=>{
          this.props.ToastrStore.error('Error!', 'Error');
        });
    }
  }
  getTAX=(e)=>{
    let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
    let Data_tax=null;
    if(Data.persone_phys_data && Data.persone_phys_data.length){
      Data_tax=Data.persone_phys_data.filter((item)=>{
        return item.numero && item.reference?true:false;
      }).map((item)=>{
        return {number:item.numero.toString().replace(/ /g, ''), reference:item.reference}
      });
    }
    if(Data_tax && Data_tax.length){
        axios.post(
          '/dop/verifyTax',
          qs.stringify({Data: Data_tax}),
          {
              headers: { 'content-type': 'application/x-www-form-urlencoded' }
          }
        ).then((res)=>{
          if(res.data.result==1){
              let result=res.data.data.length?res.data.data[0]:null;
              let year=null;
              if(result && result.declarant1){
                let keys=Object.keys(result.declarant1);
                Data.nom=result.declarant1['Nom']?result.declarant1['Nom']:'';
                Data.prenom=result.declarant1['Prenom(s)']?result.declarant1['Prenom(s)']:'';
                let adresse_key=keys.find((key_item)=>{
                  return key_item.indexOf('Adresse')!==-1?true:false;
                })
                let adresse_data= adresse_key && result.declarant1[adresse_key]?result.declarant1[adresse_key]:'';
                if(adresse_data){
                  let match_index=adresse_data.match(/\b\d{5}\b/g);
                  if(match_index && match_index.length){
                    Data.code_postale= match_index[0];
                    let pos_index=adresse_data.indexOf(Data.code_postale);
                    Data.adresse= adresse_data.substr(0, pos_index).trim();
                    Data.ville= adresse_data.substr(pos_index+5).trim();
                  }else{
                    Data.adresse= adresse_data.trim();
                  }

                }
              }
              if(res.data.data.length){
                let sum_pers=0; let sum_rev=0;
                let has_error_adresse=false;
                res.data.data.forEach((item, i)=>{
                  if(item.common && Object.keys(item.common).length){
                    Data.persone_phys_data[i]['JSON']=JSON.stringify(item);
                    if(item.common['Year']){
                      Data.persone_phys_data[i].year=Number(item.common['Year']);
                      if(Data.previsite){
                        let yer_previsite=Number(moment(Data.previsite, DateFormat).year());
                        if(Data.persone_phys_data[i].year<(yer_previsite-1)){
                          Data.persone_phys_data[i].year_error=true;
                        }else{
                          Data.persone_phys_data[i].year_error=false;
                        }
                      }
                    }

                    Data.persone_phys_data[i].personnes=item.common && item.common['Nombre de personne(s) a charge']?Number(item.common['Nombre de personne(s) a charge']):0;
                    let keys_item= Object.keys(item);
                    keys_item=keys_item.filter((key)=>{
                      return key.indexOf('declarant')!==-1 && item[key].Nom?true:false;
                    })
                    Data.persone_phys_data[i].personnes+=keys_item.length;

                    Data.persone_phys_data[i].revenue =item.common  && item.common['Revenu fiscal de reference']?Number(item.common['Revenu fiscal de reference'].replaceAll(/\s|€/ig, '')):0;
                    sum_pers+=Data.persone_phys_data[i].personnes;
                    sum_rev+=Data.persone_phys_data[i].revenue;
                    if(i!=0 && item.declarant1){
                      let keys=Object.keys(item.declarant1);
                      let adresse_key=keys.find((key_item)=>{
                        return key_item.indexOf('Adresse')!==-1?true:false;
                      })
                      if(item.declarant1[adresse_key].indexOf(Data.adresse)==-1){
                        has_error_adresse=true;
                      }
                    }
                  }
                })
                Data.adress_error=has_error_adresse?1:0;
                Data.number_de_perosne=sum_pers;
                Data.revenue=sum_rev;
              }
              this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
              this.checkPrivileges(Data);
              this.validate(Data, 'persone_phys_data');
          }else{
            this.props.ToastrStore.error('Error!', 'Error');
          }
        }).catch(()=>{
          this.props.ToastrStore.error('Error!', 'Error');
        });
    }
  }
  validate=(Data, id)=>{
    if(id==='phone'){
        if(!Data.phone){
          this.props.UserStepStore.addValidateError({id:'beneficiaire-phone'});
        }else if(Data.phone && Data.phone.toString().length!=10){
          this.props.UserStepStore.addValidateError({id:'beneficiaire-phone'});
        }else{
          this.props.UserStepStore.removeValidateError('beneficiaire-phone');
        }
    }
    if(id==='email'){
      const validateEmail= function(email) {
          const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
        }
      if(!Data.email){
        this.props.UserStepStore.addValidateError({id:'beneficiaire-email'});
      }else if(Data.email && !validateEmail(Data.email) && Data.email!='néant'){
        this.props.UserStepStore.addValidateError({id:'beneficiaire-email'});
      }else{
        this.props.UserStepStore.removeValidateError('beneficiaire-email');
      }
    }
    if(id==='siern'){
      if(!Data.siern){
        this.props.UserStepStore.addValidateError({id:'siern_beneficiaire'});
      }else if(Data.siern.toString().replace(/ /g, '').length!=9){
        this.props.UserStepStore.addValidateError({id:'siern_beneficiaire'});
      }else{
        this.props.UserStepStore.removeValidateError('siern_beneficiaire');
      }
    }
    if(id==='code_postale'){
      if(!Data.code_postale){
        this.props.UserStepStore.addValidateError({id:'beneficiaire-code_postale'});
      }else if(Data.code_postale && Data.code_postale.toString().length!==5){
        this.props.UserStepStore.addValidateError({id:'beneficiaire-code_postale'});
      }else{
        this.props.UserStepStore.removeValidateError('beneficiaire-code_postale');
      }
    }
    if(id=='previsite'){
      let Date_data=this.props.UserStepStore.dataClaculator.date;

      if(Data.previsite && moment(Data.previsite, DateFormat).toDate()>new Date()){
        this.props.UserStepStore.addValidateError({id:'previsite', id_msg:'error_future_date'});
      }else if(Data.previsite && Date_data.date_de_signature &&  moment(Data.previsite, DateFormat).toDate()>moment(Date_data.date_de_signature, DateFormat).toDate()){
        this.props.UserStepStore.addValidateError({id:'previsite'});
      }else{
        this.props.UserStepStore.removeValidateError('previsite');
      }
      if(Data.previsite && Data.persone_phys_data.length){
        let yer_previsite=Number(moment(Data.previsite, DateFormat).year());
        Data.persone_phys_data.forEach((item)=>{
          if(item.year && Number(item.year)<(yer_previsite-1)){
            this.props.UserStepStore.addValidateError({id:'persone_phys_data_'+item.id});
          }else{
            this.props.UserStepStore.removeValidateError('persone_phys_data_'+item.id);
          }
        });
      }
    }
    if(id=='persone_phys_data'){
      let yer_previsite=Number(moment(Data.previsite, DateFormat).year());
      Data.persone_phys_data.forEach((item)=>{
        if(!item.numero || !item.reference || !item['JSON'] || (yer_previsite && item.year && Number(item.year)<(yer_previsite-1))){
          this.props.UserStepStore.addValidateError({id:'persone_phys_data_'+item.id});
        }else{
          this.props.UserStepStore.removeValidateError('persone_phys_data_'+item.id);
        }
      });
    }
  }
  physDataAdd=()=>{
    let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
    if(!Data.persone_phys_data.length){
      Data.persone_phys_data.push({id:Math.round(Math.random()*1000000000), numero:'', reference:''});
    }
    Data.persone_phys_data.push({id:Math.round(Math.random()*1000000000), numero:'', reference:''});
    this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
  }
  physDataChange=(e, id)=>{
    let {name, value}=e.currentTarget;
    const changeData=()=>{
      let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
      let isExist=false;
      Data.persone_phys_data=Data.persone_phys_data.map((item)=>{
          if(item.id==id){
            isExist=true;
            item[name]=value;
            item['JSON']=null;
          }
          return item;
      })
      if(!isExist){
        let newData={id:id, numero:'', reference:''};
        newData[name]=value;
        Data.persone_phys_data.push(newData);
      }
      this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
      this.validate(Data, id);
    }

    let DataOperation=this.props.UserStepStore.dataOperation;
    if(DataOperation.length && name=='reference'){
      this.props.UserStepStore.setVisibleConfirmModal({
          show:true,
          text:'Cette modification entraine l\'annulation des données precédement renseignées',
          title:'Confirm',
          YesTitle:'Oui',
          NonTitle:'Non',
          callbackYes:()=>{
            changeData();
            this.props.UserStepStore.updateDataOperation([]);
          },
          callbackNo:()=>{
          }
        });
    }else{
      changeData();
    }
  }
  physNumberBeforeChange=()=>{
    return new Promise<boolean>((resolve, reject)=>{
      let DataOperation=this.props.UserStepStore.dataOperation;
      if(DataOperation.length){
        this.props.UserStepStore.setVisibleConfirmModal({
            show:true,
            text:'Cette modification entraine l\'annulation des données precédement renseignées',
            title:'Confirm',
            YesTitle:'Oui',
            NonTitle:'Non',
            callbackYes:()=>{
              resolve(true);
              this.props.UserStepStore.updateDataOperation([]);
            },
            callbackNo:()=>{
              resolve(false);
            }
          });
      }else{
        resolve(true);
      }
    });
  }
  physDataRemove=(id)=>{
    let Data=JSON.parse(JSON.stringify(this.props.UserStepStore.dataClaculator.beneficiaire));
    let hasJson=false;
    Data.persone_phys_data=Data.persone_phys_data.filter((item)=>{
        if(item.id==id && item['JSON']){
          hasJson=true;
        }
        return item.id==id?false:true;
    })
    if(hasJson){
      Data.persone_phys_data=Data.persone_phys_data.map((item)=>{
          delete item['JSON'];
          return item;
      })
    }
    let DataOperation=this.props.UserStepStore.dataOperation;
    if(hasJson && DataOperation.length){
        this.props.UserStepStore.setVisibleConfirmModal({
            show:true,
            text:'Cette modification entraine l\'annulation des données precédement renseignées',
            title:'Confirm',
            YesTitle:'Oui',
            NonTitle:'Non',
            callbackYes:()=>{
              this.props.UserStepStore.updateDataOperation([]);
              this.props.UserStepStore.removeValidateError('persone_phys_data_'+id);
              this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
              this.validate(Data, id);
            },
            callbackNo:()=>{

            }
          });
    }else{
      this.props.UserStepStore.removeValidateError('persone_phys_data_'+id);
      this.props.UserStepStore.updateCalculatorData('beneficiaire', Data);
      this.validate(Data, id);
    }


  }

  renderMorale=(Data, errors_data)=>{
    let error=errors_data.map((error_item)=>{return error_item.id});
    return(<React.Fragment>
      <div className="col-md-12">
        <div className="sub_title_step">Données d'enregistrement</div>
      </div>
      <div className="col-md-6">
        <div className={'required form-group '+(error.indexOf('siern_beneficiaire')!==-1?'validate_error':'')}>
          <div className="input-group">
            <NumberInput onChange={this.handleChange} value={Data.siern} onlyDigits={true} allowSpace={true} AdvProps={{id:"siern", className:"form-control"}}/>
            <label><span className="text">Numero de SIREN</span><span className="label_ph"></span></label>
            <div className="input-group-append">
              <button onClick={this.getSIRN} className="btn" type="button">Valider</button>
            </div>
          </div>
          {getErrorMsg(errors_data, 'siern_beneficiaire')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'required form-group '+(error.indexOf('beneficiaire-raison_sociale')!==-1?'validate_error':'')}>
          <input onChange={this.handleChange} id="raison_sociale" value={Data.raison_sociale} className="form-control"/>
          <label><span className="text">Raison sociale</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-raison_sociale')}
        </div>
      </div>
    </React.Fragment>)
  }
  renderPhys=(Data, errors_data)=>{
    let error=errors_data.map((error_item)=>{return error_item.id});
    let Data_phys=[...Data.persone_phys_data];
    if(!Data_phys.length){
      Data_phys.push({id:Math.round(Math.random()*1000000000), numero:'', reference:''});
    }
    let Data_items=Data_phys.map((item, i)=>{
      let btn=null;
      if(i==0){
      //    btn=  <button type="button" onClick={this.getTAX} className="btn">Valider</button>;
          btn=  <button type="button" onClick={()=>{this.physDataRemove(item.id)}} className="btn"><Icon name={'trash'} size={16} color={'#0069A5'} /></button>;
      }else{
          btn=  <button type="button" onClick={()=>{this.physDataRemove(item.id)}} className="btn"><Icon name={'trash'} size={16} color={'#0069A5'} /></button>;
      }
      return(<div key={'phys_data_'+item.id} className={'item_cont '+(error.indexOf('persone_phys_data_'+item.id)!==-1?'validate_error':'')}>
          <div style={{paddingRight:10}} className="input_cell form-group">
            <NumberInput onChange={(e)=>{this.physDataChange(e, item.id)}} beforeChange={this.physNumberBeforeChange} value={item.numero} onlyDigits={true} allowSpace={true} AdvProps={{name:"numero", className:"form-control"}}/>
            <label><span className="text">Numéro fiscal</span><span className="label_ph"></span></label>
          </div>
          <div style={{paddingLeft:10}} className="input_cell form-group">
            <div className="input-group">
              <input name="reference" onChange={(e)=>{this.physDataChange(e, item.id)}} className="form-control" type="text" value={item.reference}/>
              <label><span className="text">Référence de l'avis</span><span className="label_ph"></span></label>
              <div className="input-group-append">
                {btn}
              </div>
            </div>
          </div>
      </div>);
    })
    let number_pers=null;
    if(Data.number_de_perosne){
      number_pers=<div><span>Nombre de personne(s):</span> {Data.number_de_perosne}</div>;
    }
    let revenue=null;
    if(Data.revenue || Data.revenue===0){
      revenue=<div><span>Revenu fiscal de reference:</span> {numberWithSpaces(Data.revenue)+' €'}</div>
    }
    let has_year_error=Data_phys.find((item)=>{
      return  item.year_error?true:false;
    })
    let error_text=null;
    if(has_year_error){
      error_text=<div className="adress_error">Seuls les avis d’imposition 2020 sont acceptés pour les opérations engagées à partir du 1 janvier () pour enregistrer un statut de précarité</div>
    }
    let Operation_type=<span>Operation Classique</span>;
    if(Data.TableBflag==1){
       Operation_type=<span>Operation Précaire</span>;
    }
    if(Data.TableAflag==1){
       Operation_type=<span>Operation Grand Précaire</span>;
    }
    return(<React.Fragment>
      <div className={'col-md-12 group_label persone_phys_data '+(error.indexOf('persone_phys_data')!==-1?'validate_error':'')}>
          <div className="sub_title_step">Données fiscales</div>
          {Data_items}
          {error_text}
          <div className="btn_cont">
              <div className="btn_left">
                <button  type="button" onClick={this.physDataAdd} className="btn"><Icon size={12} name={'plus'} color={'#F2A73B'} />{'Ajouter une résidant numéro'}</button>
              </div>
              <div className="btn_right">
                <button type="button" onClick={this.getTAX} className="btn btn_valider">Valider</button>
              </div>
          </div>
      </div>
    </React.Fragment>)
  }

  renderMoraleForm=(Data, errors_data)=>{
    let error=errors_data.map((error_item)=>{return error_item.id});
    return(<React.Fragment>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-nom')!==-1?'validate_error':'')}>
          <input placeholder="Nom" onChange={this.handleChange} id="nom" value={Data.nom} className="form-control"/>
          <label><span className="text">Nom</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-nom')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-prenom')!==-1?'validate_error':'')}>
          <input placeholder="Prénom" onChange={this.handleChange} id="prenom" value={Data.prenom} className="form-control"/>
          <label><span className="text">Prénom</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-prenom')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-fonction')!==-1?'validate_error':'')}>
          <input placeholder="Fonction" onChange={this.handleChange} id="fonction" value={Data.fonction} className="form-control"/>
          <label><span className="text">Fonction</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-fonction')}
        </div>
      </div>

      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-email')!==-1?'validate_error':'')}>
          <input placeholder="Email" onChange={this.handleChange} id="email" value={Data.email} className="form-control"/>
          <label><span className="text">Email</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-email')}
        </div>
      </div>

      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-adresse')!==-1?'validate_error':'')}>
          <input placeholder="Adresse" onChange={this.handleChange} id="adresse" value={Data.adresse} className="form-control"/>
          <label><span className="text">Adresse</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-adresse')}
        </div>
      </div>

      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-phone')!==-1?'validate_error':'')}>
          <NumberInput onChange={this.handleChange} value={Data.phone} onlyDigits={true} AdvProps={{id:"phone", className:"form-control", placeholder:"06 23 45 67 89"}}/>
          <label><span className="text">Numero de telephone</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-phone')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-code_postale')!==-1?'validate_error':'')}>
          <NumberInput onChange={this.handleChange} value={Data.code_postale} onlyDigits={true} AdvProps={{id:"code_postale", className:"form-control", placeholder:"Code postale"}}/>
          <label><span className="text">Code postale</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-code_postale')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-ville')!==-1?'validate_error':'')}>
          <input placeholder="Ville" onChange={this.handleChange} id="ville" value={Data.ville} className="form-control"/>
          <label><span className="text">Ville</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-ville')}
        </div>
      </div>
    </React.Fragment>)
  }
  renderPhysForm=(Data, errors_data)=>{
    let error=errors_data.map((error_item)=>{return error_item.id});
    return(<React.Fragment>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-nom')!==-1?'validate_error':'')}>
          <input placeholder="Nom" onChange={this.handleChange} id="nom" value={Data.nom} className="form-control"/>
          <label><span className="text">Nom</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-nom')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-prenom')!==-1?'validate_error':'')}>
          <input placeholder="Prénom" onChange={this.handleChange} id="prenom" value={Data.prenom} className="form-control"/>
          <label><span className="text">Prénom</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-prenom')}
        </div>
      </div>
      <div className="col-md-12">
        <div className={'form-group required '+(error.indexOf('beneficiaire-adresse')!==-1?'validate_error':'')}>
          <input placeholder="Adresse" onChange={this.handleChange} id="adresse" value={Data.adresse} className="form-control"/>
          <label><span className="text">Adresse</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-adresse')}
        </div>
      </div>

      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-code_postale')!==-1?'validate_error':'')}>
          <NumberInput onChange={this.handleChange} value={Data.code_postale} onlyDigits={true} AdvProps={{id:"code_postale", className:"form-control", placeholder:"Code postale"}}/>
          <label><span className="text">Code postale</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-code_postale')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-ville')!==-1?'validate_error':'')}>
          <input placeholder="Ville" onChange={this.handleChange} id="ville" value={Data.ville} className="form-control"/>
          <label><span className="text">Ville</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-ville')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-email')!==-1?'validate_error':'')}>
          <input placeholder="Email" onChange={this.handleChange} id="email" value={Data.email} className="form-control"/>
          <label><span className="text">Email</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-email')}
        </div>
      </div>
      <div className="col-md-6">
        <div className={'form-group required '+(error.indexOf('beneficiaire-phone')!==-1?'validate_error':'')}>
          <NumberInput onChange={this.handleChange} value={Data.phone} onlyDigits={true} AdvProps={{id:"phone", className:"form-control", placeholder:"06 23 45 67 89"}}/>
          <label><span className="text">Numero de telephone</span><span className="label_ph"></span></label>
          {getErrorMsg(errors_data, 'beneficiaire-phone')}
        </div>
      </div>
    </React.Fragment>)
  }


  render(){
    let Data=this.props.UserStepStore.dataClaculator.beneficiaire;
    let errors_data= this.props.UserStepStore.validateError;
    let error=errors_data.map((error_item)=>{return error_item.id});
    let typePersone=this.props.UserStepStore.dataClaculator.persone;
    return (
      <div ref={this.anim_elem} style={this.state.animation} className={'form_item'}>
        <div className="title_step">INFORMATIONS BÉNÉFICIAIRE</div>
        <form className="beneficiaire__form">
        <div className="row group_input">
          <BtnSwitch/>
          <div className="col-md-6">
              <div className={'form-group '+(error.indexOf('previsite')!==-1?'validate_error':'')}>
                <CustomDatePicker id="previsite" value={Data.previsite} handleChange={this.handleChangeDate} label={'Date de visite préalable'} />
                {getErrorMsg(errors_data, 'previsite')}
              </div>
          </div>
         </div>
          <div className="row group_input">
              {typePersone==='Personne morale' && this.renderMorale(Data, errors_data)}
              {typePersone==='Personne physique' && this.renderPhys(Data, errors_data)}
          </div>
          <div className="sub_title_step">
            Informations personnelles du bénéficiaire
          </div>
          <div className="row group_input">
            {typePersone==='Personne morale' && this.renderMoraleForm(Data, errors_data)}
            {typePersone==='Personne physique' && this.renderPhysForm(Data, errors_data)}

          </div>
        </form>
      </div>
    )
  }
}


interface ISousTraitanceFormProps {
  UserStepStore?:any,
  ToastrStore?:any,
}
interface ISousTraitanceFormState {
  animation:{
    opacity:number,
    height:any,
  }
}
@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class SousTraitanceForm extends React.Component<ISousTraitanceFormProps, ISousTraitanceFormState>{
  private anim_elem : React.RefObject<HTMLDivElement>;
  constructor(props){
    super(props);
    this.state={
      animation:{
        opacity:0,
        height:'auto',
      }
    }
    this.anim_elem=React.createRef();
  }
  componentDidMount(){
    this.animation();
  }
  componentWillUnmount(){
    this.props.UserStepStore.setAnimObj({clientHeight:this.anim_elem.current.clientHeight, scrollTop:document.getElementsByClassName('step_form_body')[0].scrollTop});
  }

  animation=()=>{
    let anim_height=this.anim_elem.current.scrollHeight;
    let animation=this.props.UserStepStore.animation;
    return new Promise((resolve, reject)=>{
      this.setState({animation:{...this.state.animation, height:animation.clientHeight}}, ()=>{
        //document.getElementsByClassName('step_form_body')[0].scrollTop=animation.scrollTop;
        setTimeout(()=>{
          this.setState({animation:{...this.state.animation, opacity:1, height:anim_height}}, ()=>{
            document.getElementsByClassName('userstep_form')[0].scrollTo({behavior: "smooth", top:0});
            setTimeout(()=>{
              this.setState({animation:{...this.state.animation, height:'auto'}}, ()=>{
                resolve(true);
              });
            }, 700);
          });
        }, 100)
      });
    })
  }

  handleChange=(e)=>{
    let {id, value, name}=e.currentTarget;
    let Data=this.props.UserStepStore.dataClaculator.sous_traitance;
    let key = id || name;
    if(key=='siret'){
      delete Data.siret_error;
    }
    Data={...Data, [key]:value};
    this.props.UserStepStore.updateCalculatorData('sous_traitance', Data);
    this.validate(Data, key);
  }
  validate=(Data, key)=>{
    if(key=='siret'){
      if(!Data.siret){
        this.props.UserStepStore.addValidateError({id:'sous-siret'});
      }else if(Data.siret.toString().replace(/ /g, '').length!=14){
        this.props.UserStepStore.addValidateError({id:'sous-siret'});
      }else if(Data.siret_error){
        this.props.UserStepStore.addValidateError({id:'sous-siret', id_msg:'sous-siret_expired'});
      }else{
        this.props.UserStepStore.removeValidateError('sous-siret');
      }
    }
  }
  getSIRET=(e)=>{
    let Data=this.props.UserStepStore.dataClaculator.sous_traitance;
    let valueSirn=Data.siret.toString().replace(/ /g, '');
    if(valueSirn){
        axios.get(
            '/dop/stepFormGetInfoPoSiren?siren='+valueSirn, {
                headers: { 'content-type': 'application/x-www-form-urlencoded' }
            }
        ).then((res)=>{
          if(res.data.result==1){
              Data.raison_sociale=res.data.data['Raison Socliale'];
              this.props.UserStepStore.updateCalculatorData('sous_traitance', Data);
          }else if(res.data.result==-2){
            //this.props.ToastrStore.error('Error!', 'Error');
            Data.siret_error=true;
            this.props.UserStepStore.addValidateError({id:'sous-siret', id_msg:'sous-siret_expired'});
          }
        }).catch(()=>{
          this.props.ToastrStore.error('Error!', 'Error');
        });
    }
  }
  changeSous=(value, id)=>{
    let Data=this.props.UserStepStore.dataClaculator.sous_traitance;
    if(!value || value=='0'){
      Data.siret='';
      Data.nom='';
      Data.prenom='';
      Data.raison_sociale='';
    }
    Data={...Data, [id]:Number(value)};
    this.props.UserStepStore.updateCalculatorData('sous_traitance', Data);
  }

  renderSousForm=(Data, errors_data)=>{
    let error=errors_data.map((error_item)=>{return error_item.id});
    return(
      <div className="row group_input">
        <div className="col-md-12">
          <div style={{fontSize:22}} className="sub_title_step regular_font">Informations du sous traitant</div>
        </div>
        <div className="col-md-6">
          <div className={'form-group required '+(error.indexOf('sous-siret')!==-1?'validate_error':'')}>
            <div className="input-group">
              <NumberInput onChange={this.handleChange} value={Data.siret} onlyDigits={true} allowSpace={true} AdvProps={{id:"siret", className:"form-control", placeholder:"Numero SIRET"}}/>
              <label><span className="text">Numero SIRET</span><span className="label_ph"></span></label>
              <div className="input-group-append">
                <button onClick={this.getSIRET} className="btn" type="button">Valider</button>
              </div>
            </div>
            {getErrorMsg(errors_data, 'sous-siret')}
          </div>
        </div>
        <div className="col-md-6">
          <div className={'form-group required '+(error.indexOf('sous-raison_sociale')!==-1?'validate_error':'')}>
            <input placeholder="Raison sociale" onChange={this.handleChange} id="raison_sociale" value={Data.raison_sociale} className="form-control"/>
            <label><span className="text">Raison sociale</span><span className="label_ph"></span></label>
            {getErrorMsg(errors_data, 'sous-raison_sociale')}
          </div>
        </div>
        <div className="col-md-6">
          <div className={'form-group required '+(error.indexOf('sous-nom')!==-1?'validate_error':'')}>
            <input placeholder="Nom" onChange={this.handleChange} id="nom" value={Data.nom} className="form-control"/>
            <label><span className="text">Nom</span><span className="label_ph"></span></label>
            {getErrorMsg(errors_data, 'sous-nom')}
          </div>
        </div>
        <div className="col-md-6">
          <div className={'form-group required '+(error.indexOf('sous-prenom')!==-1?'validate_error':'')}>
            <input placeholder="Prenom" onChange={this.handleChange} id="prenom" value={Data.prenom} className="form-control"/>
            <label><span className="text">Prenom</span><span className="label_ph"></span></label>
            {getErrorMsg(errors_data, 'sous-prenom')}
          </div>
        </div>
      </div>
    );
  }

  render(){
    let Data=this.props.UserStepStore.dataClaculator.sous_traitance;
    let errors_data=this.props.UserStepStore.validateError;
    let sousForm=null;
    if(Data.sous_traitance){
      sousForm=this.renderSousForm(Data, errors_data);
    }
    return (
      <div ref={this.anim_elem} style={this.state.animation} className={'form_item'}>
        <div className="title_step">SOUS-TRAITANCE</div>
        <form className={'soustraitance__form'}>
          <div className="sub_title_step">Est-ce que ce projet est sous-traite?</div>
          <div style={{marginTop:25, marginBottom:25}} className="radio_group row">
            <div className="col-md-12 radio_cont">
              <CustomRadio
                id={'sous_traitance'}
                value={[Data.sous_traitance]}
                data={[
                  {value:'1', name:'Oui'},
                  {value:'0', name:'Non'},
                ]}
                onChange={this.changeSous}
                display={'inline'}
                type={'radio'}
              />
            </div>
          </div>
          {sousForm}
        </form>
      </div>
    )
  }
}
