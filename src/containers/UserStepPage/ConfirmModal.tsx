import * as React from "react";
import { Modal, Button, Table } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";


const DateFormat='DD/MM/yyyy';


interface IConfirmModalProps{
  UserStepStore?:any,
  ToastrStore?:any,
}

@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class ConfirmModal extends React.Component<IConfirmModalProps> {

    constructor(props) {
        super(props);
    }
    componentDidUpdate(prevProps){

    }
    handleNo = () => {
      if(this.props.UserStepStore.confirmModal.callbackNo){
        this.props.UserStepStore.confirmModal.callbackNo();
      }
      this.props.UserStepStore.setVisibleConfirmModal({show:false, YesTitle:'', NonTitle:'', text:'', title:'', callbackYes:()=>{}, callbackNo:()=>{}});
    };
    handleYes = ()=>{
      if(this.props.UserStepStore.confirmModal.callbackYes){
        this.props.UserStepStore.confirmModal.callbackYes();
      }
      this.props.UserStepStore.setVisibleConfirmModal({show:false, YesTitle:'', NonTitle:'', text:'', title:'', callbackYes:()=>{}, callbackNo:()=>{}});
    }

    render() {
        let icoStyle={
          marginRight:7,
          fontSize: 13,
        }
        let NonBtn=null;
        if(this.props.UserStepStore.confirmModal.NonTitle){
          NonBtn=<button style={{margin:'0px 5px'}} className="nt_btn_def" onClick={this.handleNo}>
              <i className="fa fa-times" style={icoStyle}></i>
              {this.props.UserStepStore.confirmModal.NonTitle}
          </button>
        }
        let YesBtn=null;
        if(this.props.UserStepStore.confirmModal.YesTitle){
          YesBtn= <button style={{margin:'0px 5px'}} className="nt_btn_def" onClick={this.handleYes}>
                <i className="fa fa-check" style={icoStyle}></i>
                {this.props.UserStepStore.confirmModal.YesTitle}
            </button>
        }
        let Footer=<div>
            {YesBtn}
            {NonBtn}
        </div>;
        return (
            <Modal className="confirm_modal nt_modal" animation={true} show={this.props.UserStepStore.confirmModal.show} onHide={this.handleNo}>
                    <Modal.Header>
                        <button onClick={this.handleNo} type="button" className="close nt_close_btn">
                            <span aria-hidden="true">×</span>
                            <span className="sr-only">Close</span>
                        </button>
                        <div className={"nt_title"}>{this.props.UserStepStore.confirmModal.title}</div>
                    </Modal.Header>
                    <Modal.Body>{this.props.UserStepStore.confirmModal.text}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
            </Modal>
        );
    }
}
