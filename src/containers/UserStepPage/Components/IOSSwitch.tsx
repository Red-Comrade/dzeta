import * as React from "react";
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';

export const IOSSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 46,
            height: 24,
            padding: 0,
            margin: theme.spacing(1),
        },
        switchBase: {
            padding: 1,
            transform: 'translate(3px)',
            '&$checked': {
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#51B5F3',
                    opacity: 1,
                    border: 'none',
                    boxShadow: 'inset 0px 1px 2px rgba(116, 140, 49, 0.9)',
                },
            },
            '&$focusVisible $thumb': {
                color: '#069cf6',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 21,
            height: 21,
            background:'linear-gradient(180deg, #FFFFFF 52.6%, #EEEEEE 100%)',
            border:'1px solid #C2C2C2',
            boxShadow:'0px 2px 2px rgb(116 140 49 / 80%)',

        },
        track: {
            borderRadius: 13,
            border:' 1px solid #6c5b5b',
            background: 'none',

        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }: any) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});


//Зелёный свич для таблицы, написан отдельно, так как может еще чего поменяется кординально
export const IOSSwitchGreen = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 46,
            height: 24,
            padding: 0,
            margin: theme.spacing(1),
        },
        switchBase: {
            padding: 1,
            transform: 'translate(3px)',
            '&$checked': {
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#D4FF5C',
                    opacity: 1,
                    border: 'none',
                    boxShadow: 'inset 0px 1px 2px rgba(116, 140, 49, 0.9)',
                },
            },
            '&$focusVisible $thumb': {
                color: '#D4FF5C',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 21,
            height: 21,
            background:'linear-gradient(180deg, #FFFFFF 52.6%, #EEEEEE 100%)',
            border:'1px solid #C2C2C2',
            boxShadow:'0px 2px 2px rgb(116 140 49 / 80%)',

        },
        track: {
            borderRadius: 13,
            border:' 1px solid #6c5b5b',
            background: 'none',

        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }: any) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});
