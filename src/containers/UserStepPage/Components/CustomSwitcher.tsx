import * as React from "react";

interface ICustomSwitcherProps {
  data:{
    name:string,
    value:string,
  }[],
  value:string,
  onChange:(value)=>void,
  beforeChange?:()=>Promise<boolean>,
}
interface ICustomSwitcherState {
  data:{
    name:string,
    value:string,
  }[],
  value:string,
  anim_value:string,
  style_th:any,
  thum_trans_class:boolean,
}

let resizeTimeout=null;

export class CustomSwitcher extends React.Component<ICustomSwitcherProps, ICustomSwitcherState> {
  private refElem : React.RefObject<HTMLDivElement>[];
  constructor(props){
    super(props);
    this.state={
      data:this.props.data,
      value:this.props.value,
      anim_value:null,
      style_th:{left:0,top:0},
      thum_trans_class:false,
    };
    this.refElem=this.props.data.map(()=>{
      return React.createRef();
    })
  }

  selectElem=(value, i)=>{
    if(this.props.beforeChange){
      this.props.beforeChange().then((data)=>{
        if(data){
          this.changeData(value, i);
        }
      })
    }else{
      this.changeData(value, i);
    }

  }
  changeData=(value, i)=>{
    let old_value=this.state.value;
    let old_index=0;
    this.state.data.forEach((item, i)=>{
      if(item.value==old_value){
        old_index=i;
      }
    });
    this.setState({anim_value:value, style_th:{left:this.refElem[old_index].current.offsetLeft, top:0, width:this.refElem[old_index].current.clientWidth}}, ()=>{

      setTimeout(()=>{
        this.setState({value:value, style_th:{left:this.refElem[i].current.offsetLeft, top:0, width:this.refElem[i].current.clientWidth}}, ()=>{
          setTimeout(()=>{
            this.setState({anim_value:null})
          }, 600);
        })
      }, 100)

      this.props.onChange(value);
    })

  }

  render(){
    let elemnts=this.state.data.map((item, i)=>{
      let style={};
      let el=<div ref={this.refElem[i]} onClick={()=>{this.selectElem(item.value, i)}} key={'custom_sw_'+item.value} style={style} className={'switch_element '+(item.value==this.state.value?'selected':'')+' '+(item.value==this.state.value && !this.state.anim_value?'blue':'')}><span>{item.name}</span></div>;
      return el;
    });
    let thumb=null;
    if(this.state.anim_value){
      thumb=<div style={this.state.style_th} className={'custom_sw_thum '+(this.state.thum_trans_class?'transition':'')}></div>
    }
    return(
      <div className="custom_switcher_cont">
        {elemnts}
        {thumb}
      </div>
    );
  }
}
