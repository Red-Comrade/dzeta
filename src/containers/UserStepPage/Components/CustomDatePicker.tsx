import * as React from "react";
import DatePicker, { registerLocale }  from "react-datepicker";
import {Icon} from './Icon'
import * as moment from 'moment';

import fr from "date-fns/locale/fr";
registerLocale("fr", fr);

const DateFormat='DD/MM/yyyy';

interface ICustomDatePickerProps {
  id:string,
  disabled?:boolean,
  handleChange:(id:string, date:string)=>void,
  value:string,
  label?:string,
}
export class CustomDatePicker extends React.Component<ICustomDatePickerProps> {
  private _calendar:any;

  render() {
    const dpFormat='dd/MM/yyyy';

    let lable=null;
    if(this.props.label){
      lable=<label><span className="text">{this.props.label}</span><span className="label_ph"></span></label>;
    }

    let value=this.props.value?moment(this.props.value, DateFormat).toDate():null;
    return (
      <div className="input-group">
        <DatePicker
          ref={(c) => this._calendar = c}
          selected={value}
          locale="fr"
          onChange={(date)=>{
            let value = date?moment(date).format(DateFormat):'';
            this.props.handleChange(this.props.id, value);
          }}
          /*customInput={<ExampleCustomInput />}*/
          disabled={this.props.disabled}
          dateFormat={dpFormat}
        />
        {lable}
        <div className="input-group-append">
          <button onClick={()=>{this._calendar.setOpen(true)}} className="btn" type="button">Date <Icon name={'calendar'} size={15} color={'#0069A5'} /></button>
        </div>
      </div>
    );
  }
}
