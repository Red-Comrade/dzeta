import * as React from "react";

const ErrorText={
  'sous-siret_expired':'LE SIRET de votre sous traitant n\'est plus actif',
  'error_d_msg':'Ce diametre n\'existe pas',
  'error_future_date':'Les dates renseignées sont impossibles',
};

const getErrorMsg = function(errors, field_id){
  let error_data= errors.find((error_item)=>{
    return error_item.id==field_id?true:false;
  })
  if(error_data){
    if(ErrorText && error_data.id_msg && ErrorText[error_data.id_msg]){
      return (<span className="error_msg">{ErrorText[error_data.id_msg]}</span>);
    }else{
      return (<span className="error_msg">{'S\'il vous plaît vérifier les données'}</span>);
    }
  }
  return null;

}

export default getErrorMsg;
