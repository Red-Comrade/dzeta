import * as React from "react";

interface INumberInputProps {
  AdvProps?:any,
  value:any,
  onChange:(data:any)=>void,
  changeOnBlur?:boolean,
  maxLength?:number,
  onlyDigits?:boolean,
  allowSpace?:boolean,
  prefix?:string,
  beforeChange?:()=>Promise<boolean>
  onBlur?:()=>void,
  priceFormat?:boolean,

}
interface INumberInputState {
  value:any,
}

const numberWithSpaces=(x)=> {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ").replaceAll(/\./gi, ',');
}

export class NumberInput extends React.Component<INumberInputProps, INumberInputState>{
  constructor(props){
    super(props);
    this.state={
      value:'',
    }
  }
  componentDidMount(){
    let cur_val=this.props.value.toString().replaceAll(/\./gi, ',');
    if(this.props.priceFormat){
      cur_val=numberWithSpaces(cur_val);
    }
    this.setState({value:cur_val});
  }
  componentDidUpdate(prevProps, prevState){
    let propsValue=this.props.value?this.props.value:'';
    let cur_value=this.state.value.replaceAll(/,/gi, '.').replaceAll(/ /gi, '');
    if(!isNaN(cur_value) && !isNaN(propsValue) && Number(cur_value)!=Number(propsValue) && this.state.value==prevState.value){
      propsValue=propsValue.toString().replaceAll(/\./gi, ',');
      if(this.props.priceFormat){
        propsValue=numberWithSpaces(propsValue);
      }
      this.setState({value:propsValue});
    }
  }
  change=(e)=>{
    let {value, id, name}= e.currentTarget;
    if(this.props.beforeChange){
      this.props.beforeChange().then((data)=>{
        if(data){
          this.dataChange(value, id, name);
        }
      })
    }else{
      this.dataChange(value, id, name);
    }
  }
  dataChange=(value, id, name)=>{
    if(this.props.prefix){
      value=this.removePrefix(value, this.props.prefix);
    }
    if(this.props.maxLength && value && value.toString().length>this.props.maxLength){
      return false;
    }
    if(this.props.onlyDigits){
      let allDigits=false;
      if(value.length){
        allDigits=true;
        for (let index = 0; index < value.length; index++) {
            if(isNaN(Number(value[index]))){
              allDigits=false;
            }
            if(value[index]==' ' && !this.props.allowSpace){
              allDigits=false;
            }
        }
      }
      if(!value ||(value && allDigits)){
        this.setState({value:value}, ()=>{
          if(this.props.onChange && !this.props.changeOnBlur){
            this.props.onChange({currentTarget:{value:value, id:id, name:name}});
          }
        });
      }
      return false;
    }
    value=value.replaceAll(/,/gi, '.').replaceAll(/ /gi, '');
    if(Number(value) || Number(value)===0){
      let value_disp=value.toString().replaceAll(/\./gi, ',');

      //удаление незначемых нулей
      if(value_disp && value_disp.length!=1){
        value_disp = value_disp.replace(/^0+/g, '');
        if(value_disp && value_disp[0]==','){
          value_disp='0'+value_disp;
        }
      }

      if(this.props.priceFormat){
        value_disp=numberWithSpaces(value_disp);
      }

      value=Number(value);
      this.setState({value:value_disp}, ()=>{
        if(this.props.onChange && !this.props.changeOnBlur){
          this.props.onChange({currentTarget:{value:value, id:id, name:name}});
        }
      })
    }
  }
  onBlur=(e)=>{
    let {value, id, name}= e.currentTarget;
    if(this.props.prefix){
      value=this.removePrefix(value, this.props.prefix);
    }
    if(value && value.length && value[value.length-1]==','){
      value=value.slice(0, (value.length-1)).replaceAll(/ /gi, '');
      this.setState({value:value}, ()=>{
        value=Number(value);
        if(this.props.onChange && this.props.changeOnBlur){
          this.props.onChange({currentTarget:{value:value, id:id, name:name}});
        }
      });
    }else{
      value=Number(value.toString().replaceAll(/\,/gi, '.').replaceAll(/ /gi, ''));
      if(this.props.onChange && this.props.changeOnBlur){
        this.props.onChange({currentTarget:{value:value, id:id, name:name}});
      }
    }
    if(this.props.onBlur){
      this.props.onBlur();
    }

  }
  removePrefix=(value, prefix)=>{
    for (let i = prefix.length; i > 0; i--) {
      let remove_pref=prefix.substring(0, i);
      value=value.replaceAll(remove_pref, '').trim()
    }
    return value;
  }

  render(){
    let prefix=this.props.prefix?this.props.prefix:'';
    return(
      <React.Fragment>
        <input {...this.props.AdvProps} value={prefix+this.state.value} onBlur={this.onBlur} onChange={this.change} type="text"/>
      </React.Fragment>
    )
  }
}
