import * as React from "react";
import calendar from '../Icons/calendar';
import check from '../Icons/check';
import doc_edit from '../Icons/doc_edit';
import download from '../Icons/download';
import edit from '../Icons/edit';
import info from '../Icons/info';
import plus from '../Icons/plus';
import sign from '../Icons/sign';
import trash from '../Icons/trash';
import tringle from '../Icons/tringle';
import upload from '../Icons/upload';
import arrow_left from '../Icons/arrow_left';
import search from '../Icons/search';
import upload_file from "../Icons/upload_file";
import archive from "../Icons/archive";
import unarchive from "../Icons/unarchive";
import nt_uncheck from "../Icons/nt_uncheck";
import nt_check from "../Icons/nt_check";
import nt_intermediate from "../Icons/nt_intermediate";


interface INumberInputProps {
  name:string,
  color:string,
  size:number,
}
export class Icon extends React.Component<INumberInputProps>{
  getIconByName=()=>{
    let name= this.props.name;
    let icon=null;
    switch (name) {
      case 'calendar':
        icon=calendar({color:this.props.color, size:this.props.size})
        break;
      case 'check':
        icon=check({color:this.props.color, size:this.props.size})
        break;
      case 'doc_edit':
        icon=doc_edit({color:this.props.color, size:this.props.size})
        break;
      case 'download':
        icon=download({color:this.props.color, size:this.props.size})
        break;
      case 'edit':
        icon=edit({color:this.props.color, size:this.props.size})
        break;
      case 'info':
        icon=info({color:this.props.color, size:this.props.size})
        break;
      case 'plus':
        icon=plus({color:this.props.color, size:this.props.size})
        break;
      case 'sign':
        icon=sign({color:this.props.color, size:this.props.size})
        break;
      case 'tringle':
        icon=tringle({color:this.props.color, size:this.props.size})
        break;
      case 'upload':
        icon=upload({color:this.props.color, size:this.props.size})
        break;
      case 'upload_file':
        icon=upload_file({color:this.props.color, size:this.props.size})
        break;
      case 'trash':
        icon=trash({color:this.props.color, size:this.props.size})
        break;
      case 'arrow_left':
          icon=arrow_left({color:this.props.color, size:this.props.size})
          break;
      case 'search':
          icon=search({color:this.props.color, size:this.props.size})
          break;
      case 'archive':
          icon=archive({color:this.props.color, size:this.props.size})
          break;
      case 'unarchive':
          icon=unarchive({color:this.props.color, size:this.props.size})
          break;
      case 'nt_uncheck':
          icon=nt_uncheck({color:this.props.color, size:this.props.size})
          break;
      case 'nt_check':
          icon=nt_check({color:this.props.color, size:this.props.size})
          break;
      case 'nt_intermediate':
          icon=nt_intermediate({color:this.props.color, size:this.props.size})
          break;
    }
    return icon;
  }
  render(){
    let icon=this.getIconByName();
    return(
      <span className="custom_icon">{icon}</span>
    );
  }
}
