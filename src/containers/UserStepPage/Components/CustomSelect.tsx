import * as React from "react";
import {Icon} from './Icon'

const DateFormat='DD/MM/yyyy';

interface ICustomSelectProps {
  value:string,
  data:{value:string, text:string}[],
  style?:any,
  placeHolderText?:string,
  disabled?:boolean,
  onChange:(opt_data)=>void,
  label?:string,
}
interface ICustomSelectState {
  value:string,
  data:{value:string, text:string}[],
  showList:boolean,
  searchText:string,
}
export class CustomSelect extends React.Component<ICustomSelectProps, ICustomSelectState> {

  constructor(props){
    super(props);
    this.state={
      showList:false,
      value:this.props.value?this.props.value:null,
      data:this.props.data?this.props.data:[],
      searchText:'',
    }
  }
  componentDidUpdate(prevProps){
    if(prevProps.value!=this.props.value || prevProps.data.length!=this.props.data.length || prevProps.disabled!=this.props.disabled){
        this.setState({value:this.props.value, data:this.props.data});
    }
  }

  handlerClick=()=>{
    this.setState({showList:true, searchText:''});
  }
  handlerBlur=(e)=>{
    if(e.relatedTarget && e.relatedTarget.className && (e.relatedTarget.className.indexOf('custom_select_search_input')!==-1 || e.relatedTarget.className.indexOf('custom_select')!==-1)){
      return false;
    }
    this.setState({showList:false});
  }
  selectValue=(item)=>{
    this.setState({value:item.value, showList:false}, ()=>{
      this.props.onChange(item);
    });
  }
  changeSearch=(e)=>{
    let {value}= e.currentTarget;
    this.setState({searchText:value});
  }


  renderConteiner=()=>{
    let chosen_value=this.state.data.find((item)=>{
      return item.value==this.state.value?true:false;
    });
    return(
      <div onClick={this.handlerClick} className="custom_select_conteiner"><Icon name="search" color="#B8B8B8" size={18}/><span className={'text '+(!chosen_value || (chosen_value && (chosen_value.value==null || chosen_value.value=='-1'))?'chosen_null':'')}>{(chosen_value?chosen_value.text:this.props.placeHolderText)}</span><Icon name="tringle" color="none" size={10}/></div>
    )
  }
  renderList=()=>{
    let chosen_value=this.state.data.find((item)=>{
      return item.value==this.state.value?true:false;
    });
    let options=this.state.data.filter((item)=>{
      return item.text.toLowerCase().includes(this.state.searchText.toLowerCase())?true:false;
    }).map((item, i)=>{
      return <div title={item.text} key={'custom_select_'+i+'_'+item.value} onClick={()=>{this.selectValue(item)}} className="custom_select_option"><span className="text">{item.text}</span></div>
    })
    let old=<div onClick={()=>{this.selectValue({value:null, text:''})}} className="custom_select_selected"><span className="text">{(chosen_value?chosen_value.text:this.props.placeHolderText)}</span><Icon name="tringle" color="#0069A5" size={10}/></div>;
    let label=null;
    if(this.props.label){
      label=<label><span className="text">{this.props.label}</span><span className="label_ph"></span></label>
    }
    return(
      <div className="custom_select_list">
        {label}
        <div className="custom_select_selected">
              <Icon name="search" color="#B8B8B8" size={18}/>
              <span className="text"><input onBlur={this.handlerBlur} className="custom_select_search_input" autoFocus onChange={this.changeSearch} value={this.state.searchText} /></span>
              <Icon name="tringle" color="#0069A5" size={10}/>
        </div>
        <div className="custom_select_list_data">
          <div className="list">
            {options}
          </div>
        </div>
      </div>
    )
  }
  render(){
    let style=this.props.style?this.props.style:{};
    return(
      <div style={style} tabIndex={0} onBlur={this.handlerBlur} className={'custom_select '+(this.props.disabled?'disabled':'')}>
          {!this.state.showList && this.renderConteiner()}
          {this.state.showList && this.renderList()}
      </div>
    );
  }
}
