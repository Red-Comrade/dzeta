import * as React from "react";
import {Icon} from './Icon'

interface ICustomTolltipProps {
  alwaysDisplay?:boolean,
  position?:string,
  style?:any,
  size?:any,
}
export class CustomTolltip extends React.Component<ICustomTolltipProps>{
  state={
    showTolltip:false,
    style:{},
  }
  size={
    small:{
      w:20,
      h:20
    },
    medium:{
      w:24,
      h:24
    },
    pop_w:200,
  }
  hideTooltip=(e)=>{
    if(this.props.alwaysDisplay){
      return false;
    }
    let el=e.relatedTarget, parentsClasses=[];
    while(el){
      parentsClasses.push(el.className);
      el=el.parentNode;
    }
    if(e.relatedTarget && e.relatedTarget.localName==='a' && parentsClasses.indexOf('tooltip_cont')!==-1){
      window.open(e.relatedTarget.href);
      this.setState({
        showTolltip:false,
      })
    }
    if(parentsClasses.indexOf('tooltip_cont')!==-1){
      return false;
    }
    this.setState({
      showTolltip:false,
    })
  }
  showTooltip=(e)=>{
    if(this.props.alwaysDisplay){
      return false;
    }
    let x=(e.currentTarget.offsetWidth/2)-100;
    let y=e.currentTarget.offsetHeight+10;
    let style:any={left:x, top:y};
    if(this.props.position=='bottom-left'){
       x=e.currentTarget.offsetWidth+15;
       style={left:x, bottom:0};
    }
    if(this.props.position=='top-right'){
       x=e.currentTarget.offsetWidth-15;
       style={left:-210, top:0};
    }
    this.setState({
      showTolltip:true,
      style:style,
    });
  }
  render(){
    let tolltip_cont= null;
    let style={};
    if(this.props.alwaysDisplay){
      let span_size=this.props.size?{w:this.props.size, h:this.props.size}:{w:14, h:14};
      style={left:(span_size.w/2)-100, top:span_size.h+15};
      if(this.props.position=='bottom-left'){
        style={bottom:0, left:span_size.w+15};
      }
    }else{
      style=this.state.style;
    }
    if(this.props.style){
      style={...style, ...this.props.style}
    }
    if(this.state.showTolltip || this.props.alwaysDisplay){
      tolltip_cont=<div style={style} className="tooltip_cont">
          <div className={'arrow_tooltip '+(this.props.position?this.props.position:'up-top-middle')}></div>
          {this.props.children}
        </div>
    }

    return(
      <React.Fragment>
        <span tabIndex={0} onBlur={this.hideTooltip} onClick={this.showTooltip} className={'ico-info '+(this.props.alwaysDisplay?'always-display':'')}><Icon name="info" color={'#0069A5'} size={this.props.size}/> {tolltip_cont}</span>
      </React.Fragment>
    )
  }
}
