import * as React from "react";

interface ICustomRadioProps {
  id:string,
  data:{
    name:string,
    value:string,
  }[],
  value:string[],
  type:string,
  onChange:(value, id)=>void,
  display?:string,
  beforeChange?:()=>Promise<boolean>,
}
interface ICustomRadioState {
  data:{
    name:string,
    value:string,
  }[],
  value:string[],
}

export class CustomRadio extends React.Component<ICustomRadioProps, ICustomRadioState> {
  constructor(props){
     super(props);
     this.state={
       data:this.props.data?this.props.data:[],
       value:this.props.value?this.props.value.map((value)=>{return value!==null && value!==undefined?value.toString():''}):[],
     }
  }
  componentDidUpdate(prevProps){
    if(this.props.value!=prevProps.value || this.props.data.length!=prevProps.data.length){
      this.setState({data:this.props.data, value:this.props.value?this.props.value.map((value)=>{return value!==null && value!==undefined?value.toString():''}):[]})
    }
  }

  handleChange=(value)=>{
    if(this.props.beforeChange){
      this.props.beforeChange().then((data)=>{
        if(data){
          this.changeData(value);
        }
      })
    }else{
      this.changeData(value);
    }

  }

  changeData=(value)=>{
    let value_arr=this.state.value;
     if(this.props.type=='radio'){
       value_arr=[value];
     }else if(this.props.type=='checkbox'){
       let exist_value= value_arr.find((ex_val)=>{
         return ex_val==value?true:false;
       })
       if(exist_value){
          value_arr=value_arr.filter((ex_val)=>{
            return ex_val==exist_value?false:true;
          })
       }else{
         value_arr.push(value);
       }
     }
     this.setState({value:value_arr}, ()=>{
       this.props.onChange(value, this.props.id);
     });
  }

  render(){
    let Opt= this.state.data.map((item)=>{
      let chek_elem=null;
      if(this.state.value.indexOf(item.value)!==-1){
        chek_elem=<span className="checked"></span>
      }
      return (<div key={'custom_radio_'+item.value} onClick={()=>{this.handleChange(item.value)}} className={'custom_radio_item '+(chek_elem?'checked':'')}>
                <span className="check_item">{chek_elem}</span>
                <label className="text">{item.name}</label>
        </div>);
    })
    return(
      <div className={'custom_radio '+ (this.props.display?this.props.display:'')}>
        {Opt}
      </div>
    )
  }
}
