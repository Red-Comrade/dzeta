import * as React from "react";
import {inject, observer} from "mobx-react";
import {Icon} from './Components/Icon'

import {AdditionalBlocks} from './AdditionalBlocks'


const DateFormat='DD/MM/yyyy';

const rounded = function(number){
    return +number.toFixed(2);
}
const numberWithSpaces=(x)=> {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ").replaceAll(/\./gi, ',');
}


interface IOperationsEditProps{
  UserStepStore?:any,
  ToastrStore?:any,
  data:any,
  onSave:(data)=>void,
  onClose:()=>void
}
interface IOperationsEditState{
  data_addit_blocks:any,
  step:number
}


@inject("UserStepStore")
@inject("ToastrStore")
@observer
export class OperationsEdit extends React.Component<IOperationsEditProps, IOperationsEditState> {


    constructor(props) {
        super(props);
        this.state={
            data_addit_blocks:this.props.data,
            step:0
        };
    }

    changeAdditBlock=(data)=>{
      let step=this.state.step;
      let new_data_addit_blocks=this.state.data_addit_blocks;
      new_data_addit_blocks[step]=data;
      this.setState({data_addit_blocks:new_data_addit_blocks});
    }
    clickOnStep=(step)=>{
      if(this.state.data_addit_blocks[step]){
        this.setState({step:step});
      }else{
        this.saveChanges();
      }
    }
    handleClose=()=>{
      this.props.onClose();
    }
    saveChanges=()=>{
      this.props.onSave(this.state.data_addit_blocks);
    }

    render() {

          let content=null;
          let stepper_elem=null;
          if(this.state.data_addit_blocks){
            stepper_elem=this.state.data_addit_blocks.map((item, i)=>{
              return <span onClick={()=>{this.clickOnStep(i)}} key={'step_ad_op_'+(i+1)} className={'step_item '+(i==this.state.step?'selected':'')}></span>;
            });
          }

          let body=<AdditionalBlocks data={this.state.data_addit_blocks[this.state.step]} onChange={this.changeAdditBlock} />;

          content=<div className="operations_edit_comp">
              <div className="operations_edit_backdrop"></div>
              <div className="operations_edit_modal">
                <div className="operations_edit_data">
                    <div className="add_oper_stepper">{stepper_elem}</div>
                    <div className="add_oper_content">{body}</div>
                    <div className="add_oper_btn">
                      <div className="btn_left">
                        <button onClick={this.handleClose} type="button" className="btn btn_cancel"><Icon name="trash" size={20} color={'#FF2E00'}/></button>
                      </div>
                      <div className="btn_right">
                        <button onClick={()=>{this.clickOnStep(this.state.step+1)}}  type="button" className="btn btn_next">{(this.state.data_addit_blocks.length-1==this.state.step?'Valider':'Suivant →')}</button>
                      </div>
                    </div>
                </div>
              </div>
          </div>
          return(<React.Fragment>
              {content}
          </React.Fragment>)
    }
}
