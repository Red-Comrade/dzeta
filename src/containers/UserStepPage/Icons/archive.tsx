import * as React from "react";

const archive = (props) => {
    return (
        <svg height={props.size} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="2.5" y="3.5" width="15" height="15" rx="0.5" stroke="#0069A5"/>
            <path d="M7 2L3 3.5H17L13 2H7Z" fill={props.color} stroke="#0069A5" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M11 7.5C11 6.94772 10.5523 6.5 10 6.5C9.44772 6.5 9 6.94772 9 7.5L9 12.0858L7.70711 10.7929C7.31658 10.4024 6.68342 10.4024 6.29289 10.7929C5.90237 11.1834 5.90237 11.8166 6.29289 12.2071L9.29289 15.2071C9.68342 15.5976 10.3166 15.5976 10.7071 15.2071L13.7071 12.2071C14.0976 11.8166 14.0976 11.1834 13.7071 10.7929C13.3166 10.4024 12.6834 10.4024 12.2929 10.7929L11 12.0858L11 7.5Z" stroke="#0069A5" strokeLinecap="round" strokeLinejoin="round"/>
        </svg>
    );
}
export default archive;



