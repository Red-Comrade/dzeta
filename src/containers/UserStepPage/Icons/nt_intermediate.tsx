import * as React from "react";

const nt_intermediate = (props) => {
    return (
        <svg height={props.size} width={props.size}  viewBox={"0 0 " + (props.size+2) +' ' + (props.size+2)}  fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="1" y="1" height={props.size} width={props.size} rx="8" fill="white" stroke={props.color} strokeWidth="2"/>
            <line x1="9" y1="15" x2="18" y2="15" stroke={props.color} strokeWidth="2" strokeLinecap="round"/>
        </svg>

    );
};
export default nt_intermediate;
