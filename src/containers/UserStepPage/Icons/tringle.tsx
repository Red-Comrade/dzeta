import * as React from "react";

const tringle = (props) => {
  return (
  <svg height={props.size} viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M7.36848 9.11133C6.79113 10.1113 5.34776 10.1113 4.77041 9.11133L1.14994 2.8405C0.572589 1.8405 1.29428 0.590496 2.44898 0.590496L9.68991 0.590496C10.8446 0.590495 11.5663 1.8405 10.9889 2.8405L7.36848 9.11133Z" fill={props.color} stroke="#0069A5"/>
  </svg>
  );
}
export default tringle;
