import * as React from "react";

const nt_check = (props) => {
    return (<svg width="26" height={props.size}  viewBox={"0 0 " + (props.size+2) +' ' + (props.size+2)}  fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="1" y="1" height={props.size} width={props.size} rx="9" fill="white" stroke={props.color} strokeWidth="2"/>
            <path d="M8 13.5L12 16.5L19.5 9" stroke={props.color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        </svg>

    )
}
export default nt_check;
