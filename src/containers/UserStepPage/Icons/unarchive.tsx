import * as React from "react";

const unarchive = (props) => {
    return (<svg height={props.size} data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.87 15.31">
        <rect className="cls-1 cls_stroke" x="0.75" y="2.08" width="12.5" height="12.5" rx="0.42" fill={"none"} stroke={props.color} />
        <path className="cls-2 cls_stroke cls_fill" d="M4.5.83,1.17,2.08H12.83L9.5.83Z" stroke={props.color} fill={props.color} strokeLinecap={"round"} strokeLinejoin={"round"} />
        <path className="cls-3 cls_stroke" fill={"none"} stroke={props.color} strokeLinecap={"round"} strokeLinejoin={"round"}
              d="M6.17,11.25a.83.83,0,0,0,1.66,0V7.43L8.91,8.51a.83.83,0,0,0,1.18-1.18l-2.5-2.5a.83.83,0,0,0-1.18,0l-2.5,2.5A.83.83,0,0,0,5.09,8.51L6.17,7.43Z"/>
    </svg>);
};
export default unarchive;







