import * as React from "react";

const trash = (props) => {
  return (
    <svg height={props.size} viewBox="0 0 13 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="1.5" y="4.5" width="10" height="13" rx="2.5" fill="white" stroke={props.color}/>
    <rect x="4.5" y="0.5" width="4" height="3" rx="1.5" fill="white" stroke={props.color}/>
    <rect x="0.5" y="2.5" width="12" height="4" rx="2" fill="white" stroke={props.color}/>
    <line x1="4.5" y1="9.5" x2="4.5" y2="14.5" stroke={props.color} strokeLinecap="round"/>
    <line x1="6.5" y1="9.5" x2="6.5" y2="14.5" stroke={props.color} strokeLinecap="round"/>
    <line x1="8.5" y1="9.5" x2="8.5" y2="14.5" stroke={props.color} strokeLinecap="round"/>
    </svg>
  );
}
export default trash;
