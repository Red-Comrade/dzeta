import * as React from "react";

const calendar = (props) => {
  return (
  <svg height={props.size} viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M11 3H4C2.34315 3 1 4.34315 1 6V13C1 14.6569 2.34315 16 4 16H11C12.6569 16 14 14.6569 14 13V6C14 4.34315 12.6569 3 11 3ZM4 2C1.79086 2 0 3.79086 0 6V13C0 15.2091 1.79086 17 4 17H11C13.2091 17 15 15.2091 15 13V6C15 3.79086 13.2091 2 11 2H4Z" fill={props.color}/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M3 5.5C3 5.22386 3.22386 5 3.5 5H11.5C11.7761 5 12 5.22386 12 5.5C12 5.77614 11.7761 6 11.5 6H3.5C3.22386 6 3 5.77614 3 5.5Z" fill={props.color}/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.5 3C4.22386 3 4 2.77614 4 2.5V0.5C4 0.223858 4.22386 1.20706e-08 4.5 0C4.77614 -1.20706e-08 5 0.223858 5 0.5V2.5C5 2.77614 4.77614 3 4.5 3Z" fill={props.color}/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.5 3C10.2239 3 10 2.77614 10 2.5V0.5C10 0.223858 10.2239 1.20706e-08 10.5 0C10.7761 -1.20706e-08 11 0.223858 11 0.5V2.5C11 2.77614 10.7761 3 10.5 3Z" fill={props.color}/>
  </svg>
  );
}
export default calendar;
