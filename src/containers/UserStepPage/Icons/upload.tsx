import * as React from "react";

const upload = (props) => {
  return (
  <svg height={props.size} viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M11.05 6.7C10.95 6.6 10.75 6.5 10.65 6.5C10.55 6.5 10.35 6.6 10.25 6.7L7.15 10C6.95 10.2 6.95 10.5 7.15 10.7C7.35 10.9 7.75 10.9 7.85 10.7L10.05 8.4V14.2C10.05 14.5 10.25 14.7 10.55 14.7C10.85 14.7 11.05 14.5 11.05 14.2V8.4L13.25 10.7C13.45 10.9 13.75 10.9 13.95 10.7C14.15 10.5 14.15 10.2 13.95 10L11.05 6.7Z" fill={props.color}/>
  <circle cx="10.5" cy="10.5" r="10" stroke="#0069A5"/>
  </svg>
  );
}
export default upload;
