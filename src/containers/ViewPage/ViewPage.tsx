import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect, useParams, useRouteMatch } from 'react-router-dom'
import { inject, observer } from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import CoolTable from '../../components/CoolTable'
import HZTabs from "../../components/HZTabs";

import * as v_api from '../../api/View-api'
import {ViewTableCloth} from "./ViewTableCloth";
import {IDataMarketFilter} from "../../stores/ViewStore"
import HZContextMenu from "../../components/HZContextMenu";
import axios from "axios";
import RootStore from "../../stores/RootStore";

export interface IObjectsPageProps {
    ToastrStore?: any,
    ViewStore?: {
        isMainTableOpen: boolean,

        ActiveClothID: string,
        isLoadMarketFilter: boolean,
        DataMarketFilter: IDataMarketFilter

    },
    match?: any,
    history?: any,
    location?: any,
}
interface IObjectsPageState {
    isLoadedAppInfo: boolean;
    isLoadedAppInfo_begin: boolean;
    RecalcStyleFunc: () => void,
    AppUid: string,
    AppName: string,
    FolderName: string,
}

@inject("ToastrStore")
@inject("ViewStore")
@observer
export class ViewPage extends React.Component<IObjectsPageProps, IObjectsPageState> {
    state: Readonly<IObjectsPageState> = {
        isLoadedAppInfo: true,
        isLoadedAppInfo_begin: false,
        RecalcStyleFunc: null,

        AppUid: "",
        AppName: "",
        FolderName: "",
    };
    xhrLoadApp = {cancel: null};

    constructor(props: IObjectsPageProps) {
        super(props);
    };

    componentDidMount(): void {
        document.title = 'View | ' + RootStore.config.nameSystem;
        this.setState({
            AppUid: this.props.match.params.NodeID,
            isLoadedAppInfo: false
        })
    }
    componentDidUpdate(prevProps: Readonly<IObjectsPageProps>, prevState: Readonly<IObjectsPageState>, snapshot?: any): void {
        if(prevState.AppUid != this.props.match.params.NodeID) {
            this.setState({
                AppUid: this.props.match.params.NodeID,
                isLoadedAppInfo: false
            })
        }
        if(!this.state.isLoadedAppInfo && !this.state.isLoadedAppInfo_begin) {
            this.setState({isLoadedAppInfo_begin: true});
            this.core.generateAppInfo();
        }
    }
    componentWillUnmount(): void {
        if(this.xhrLoadApp.cancel) {
            this.xhrLoadApp.cancel();
        }
    }

    core = {
        AJAX: {
            getAppInfo: (SuccFunc, ErrFunc) => {
                if(this.xhrLoadApp.cancel) {
                    this.xhrLoadApp.cancel();
                }

                v_api
                    .getAppInfo(this.state.AppUid, this.xhrLoadApp)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch( (error) =>{
                        if(axios.isCancel(error)) {
                            console.error("Отмена");
                            //ручная отмена
                        }
                        else {
                            console.log(error);
                            ErrFunc({result: 0});
                        }
                    })
            }
        },

        generateAppInfo: () => {
            this.core.AJAX.getAppInfo((answer) => {

                document.title = answer.InfoApp.Name + ' | ' + RootStore.config.nameSystem;

                this.setState({
                    isLoadedAppInfo: true,
                    isLoadedAppInfo_begin: false,
                    AppName: answer.InfoApp.Name,
                    FolderName: answer.InfoApp.FolderName,
                });

                }, () => {

            });
        },

        generateBreadcrumb: () => {
            let folder_li;
            if(this.state.FolderName) {
                folder_li = <li>{this.state.FolderName}</li>;
            }

            return (
                <ul className="zt_breadcrumb">
                    <li><a href="/index#/home">Home</a></li>
                    {folder_li}
                    <li>{this.state.AppName}</li>
                </ul>
            );
        },
    };



    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        //  LoaderBlockUI = <h4 className="loader-text">Loading...</h4>;



        let is_blocking = !this.state.isLoadedAppInfo || this.state.isLoadedAppInfo_begin;

        let ViewTableVisible;
        if(this.state.AppUid && !is_blocking) {
            const DataViewTableCloth = {
                id: "MainTable",
                AppUid: this.state.AppUid,
            };
            ViewTableVisible = <ViewTableCloth {...DataViewTableCloth} />;
        }

        return (
            <BlockUi id="zt_ViewPage" tag="div" loader={LoaderBlockUI} blocking={is_blocking}>
                <div className="zt_cloth">
                    {this.core.generateBreadcrumb()}
                    {ViewTableVisible}
                </div>
            </BlockUi>
        )
    }
}