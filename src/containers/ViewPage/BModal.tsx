import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/ClassTree-api";
import {th} from "date-fns/locale";

import TreeSelect, { TreeNode, SHOW_PARENT } from 'rc-tree-select';
import 'rc-tree-select/assets/index.css';
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import * as ot_api from "../../api/ObjTree-api";

import Select from 'react-select';


interface IBModalProps {
    show:boolean;
    TypeModal: string; //RemoveModal//SortModal//FixModal
    CloseCallback?: () => void,
    SuccessFunction?: () => void,
    SaveSortCallback?: (view_sorts) => void,
    SaveFixCallback?: (view_sorts) => void,


    //del modal
    objectsDel?: any,


    //sort modal
    columnsSort?: any,
    view_sorts?: any,

    //fix modal
    HeadData?: any,


    ToastrStore?: any;
}
interface IBModalState {
    blocking: boolean,
    TypeName?: string,

    isLoadingObj?: boolean,
    objectsDelNames?: any[],
    cacheObjects?: any,

    view_sorts?: any,

    newHeadData?: any,
}

@inject("ToastrStore")
@observer
export default class BModal extends React.Component<IBModalProps,IBModalState> {
    state: Readonly<IBModalState> = {
        blocking: false,
        TypeName: "", //RemoveModal // SortModal
        isLoadingObj: false,

        objectsDelNames: [],
        cacheObjects: {},

        view_sorts: [],
        newHeadData: [],
    };



    _ref_inp_type: HTMLElement;

    constructor(props) {
        super(props);
    }

    events = {
        handleClose: () => {
            this.props.CloseCallback();
        },

        handleDeleteObj: () => {
            let objs:any = [];
            Object.keys(this.state.cacheObjects).forEach( (item_k, key_k) => {
                objs.push(item_k);
            });

            console.log(objs);


            if(objs.length > 0) {
                this.setState({isLoadingObj: true});
                this.core.AJAX.removeObjs(objs, () => {
                    this.setState({isLoadingObj: false, cacheObjects: {}});
                    this.props.SuccessFunction();
                }, () => {
                    this.setState({isLoadingObj: false});
                });
            }
            else {
                this.props.ToastrStore.warning('Warning!', 'No views');
            }
        },

        handleSaveSort: () => {
            this.props.SaveSortCallback(this.state.view_sorts);
        },

        handleSaveFixed: () => {
            this.props.SaveFixCallback(this.state.newHeadData);
        },
    };

    core = {
        AJAX: {
            getNameObjects: (Objs: [], SuccFunc, ErrFunc) => {
                ot_api
                    .getNameObjects(Objs)
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (error) => {
                      console.log(error);
                      ErrFunc();
                    })
            },
            removeObjs: (Objs: [], SuccFunc, ErrFunc) => {
                ot_api
                    .removeObjs(Objs)
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (error) => {
                      console.log(error);
                      ErrFunc();
                    })
            },
        },
        build: {
            removeModal: () => {
                let trsJSX = [];

                let objectsDelNames_2 = {};

                this.state.objectsDelNames.forEach((item, key) => {
                    objectsDelNames_2[item.ID] = item;

                });

                let last_enabled:any = {};
                let enabled_rows = [];
                let disabled_rows = [];

                for( let key_a in this.props.objectsDel) {
                    if(objectsDelNames_2[key_a] != undefined) {
                        if(!this.props.objectsDel[key_a].disabled) {
                            let is_checked = false;
                            if(this.state.cacheObjects[key_a] != undefined) {
                                is_checked = true;
                            }
                            last_enabled = {
                                Name: objectsDelNames_2[key_a].Name,
                                NumOfChildren: objectsDelNames_2[key_a].NumOfChildren,
                                ID: key_a,
                            };

                            enabled_rows.push(<tr key={key_a}>
                                <td>{objectsDelNames_2[key_a].Name}</td>
                                <td>{objectsDelNames_2[key_a].NumOfChildren}</td>
                                <td>
                                    <input className="form-control" type="checkbox" checked={is_checked} onChange={() => {
                                        let new_checked = !is_checked;
                                        if(!new_checked) {
                                            delete this.state.cacheObjects[key_a];
                                        }
                                        else {
                                            this.state.cacheObjects[key_a] = true;
                                        }
                                        this.setState({})
                                    }} />
                                </td>
                            </tr>)
                        }
                        else {
                            disabled_rows.push(<tr key={key_a} className={""}>
                                <td>{objectsDelNames_2[key_a].Name}</td>
                                <td>{objectsDelNames_2[key_a].NumOfChildren}</td>
                                <td>{"No delete permission"}</td>
                            </tr>)
                        }
                    }
                }

                if(enabled_rows.length == 1) {
                    //Are you sure you want to delete "1111111 1111" and 1 nested objects?
                    this.state.cacheObjects[last_enabled.ID] = true;
                    return <div>
                        { 'Are you sure you want to delete "'+ last_enabled.Name +'" and' + last_enabled.NumOfChildren + 'nested objects?' }
                    </div>;
                }
                else {
                    return <div>
                        <table className={"table"}>
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Children</th>
                                <th style={{width: "60px"}}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {enabled_rows}
                            {disabled_rows}
                            </tbody>
                        </table>
                    </div>;
                }
            },

            sortModal: () => {
                let trs_sorts = [];
                let sort_keys = {};
                let names_col = {};
                let selected_uids = {};



                const GenerateSelect = (value_uid) => {
                    let options = [];
                    let _options = [];

                    this.props.columnsSort.forEach((item, index) => {
                        if(selected_uids[item.uid] == undefined || item.uid == value_uid ) {
                        //    options.push(<option key={item.uid} value={item.uid}>{names_col[item.uid]}</option>);
                            _options.push({key:item.uid, value: item.uid, label: names_col[item.uid]});

                        }
                    });

                    //options.push(<option key={"empty_key"} value={""}>Not selected</option>);
                    _options.push({key: "", value: "", label: "Not selected"});


                    let _options_selected;

                    if(value_uid) {
                        _options_selected = {value: value_uid, label: names_col[value_uid]};
                    }
                    else {
                        _options_selected = {value: value_uid, label: "Not selected"};
                    }


                    const onChange = (e) => {
                        let value = e.value;
                        let label = e.label;
                        let key = e.key;

                        if(value == "" && value_uid != "") {
                            //удаление элемента при выборе пустого значения
                            let new_view_sorts = [];
                            if(this.state.view_sorts) {
                                for(let key_s in this.state.view_sorts) {
                                    if( this.state.view_sorts[key_s].uid != value_uid) {
                                        new_view_sorts.push(this.state.view_sorts[key_s]);
                                    }
                                }
                            }
                            this.setState({view_sorts: new_view_sorts})
                        }
                        else {
                            let new_view_sorts = [];
                            if(this.state.view_sorts) {
                                for(let key_s in this.state.view_sorts) {
                                    if( this.state.view_sorts[key_s].uid == value_uid ) {
                                        this.state.view_sorts[key_s].uid = value;
                                    }
                                    new_view_sorts.push(this.state.view_sorts[key_s]);
                                }
                                if(value_uid == "" && value != "") {
                                    new_view_sorts.push({uid: value, order: "asc"});
                                }
                            }
                            else {
                                if(value_uid == "" && value != "") {
                                    new_view_sorts.push({uid: value, order: "asc"});
                                }
                            }
                            this.setState({view_sorts: new_view_sorts})
                        }
                    };


                    return <Select
                        onChange={onChange}
                        styles={{container: styles => ({ ...styles, width: '100%' })}}

                        className="basic-single"
                        classNamePrefix="select"
                        isDisabled={false}
                        isLoading={false}
                        isClearable={false}
                        isRtl={false}
                        isSearchable={false}
                        name="color"

                        value={_options_selected}
                        options={_options}
                    />;


                    /*
                    return <select style={{width:"100%"}}
                                   value={value_uid}
                                   onChange={onChange}
                    >{options}</select>;
                    */
                };

                const GenerateSelect_dir = (value_order, value_uid) => {
                    let sel_val = value_order != "desc" ? "asc" : "desc";
                    let sel_val_label = value_order != "desc" ? "ascending" : "descending";

                    let _options = [
                        {key: "asc", value: "asc", label: "ascending"},
                        {key: "desc", value: "desc", label: "descending"},
                    ];

                    let _options_selected = {value: sel_val, label: sel_val_label};


                    const onChange = (e) => {
                        let value = e.value;
                        let label = e.label;
                        let key = e.key;

                        let new_view_sorts = [];
                        if(this.state.view_sorts) {
                            for(let key_s in this.state.view_sorts) {
                                if( this.state.view_sorts[key_s].uid == value_uid ) {
                                    this.state.view_sorts[key_s].order = value;
                                }
                            }
                            new_view_sorts = this.state.view_sorts;
                        }

                        this.setState({view_sorts: new_view_sorts})
                    };

                    return <Select
                        onChange={onChange}
                        styles={{container: styles => ({ ...styles, width: '100%' })}}
                        className="basic-single"
                        classNamePrefix="select"
                        isDisabled={false}
                        isLoading={false}
                        isClearable={false}
                        isRtl={false}
                        isSearchable={false}
                        name="color"
                        value={_options_selected}
                        options={_options}
                    />;
                };

                this.props.columnsSort.forEach((item, index) => {
                    names_col[item.uid] = item.name;
                });

                if(this.state.view_sorts) {
                    this.state.view_sorts.forEach((item, index) => {
                        selected_uids[item.uid] = names_col[item.uid];
                    });

                    this.state.view_sorts.forEach((item, index) => {
                        trs_sorts.push(
                            <tr key={index}>
                                <td>
                                    <div style={{display:"flex", width:"100%"}}>
                                        <button onClick={() => {
                                            let new_view_sorts = [];
                                            for(let key_s in this.state.view_sorts) {
                                                if( this.state.view_sorts[key_s].uid != item.uid ) {
                                                    new_view_sorts.push(this.state.view_sorts[key_s]);
                                                }
                                            }
                                            this.setState({view_sorts: new_view_sorts})
                                        }} style={{border:"none", background:"transparent", flex: "0 0 auto"}} ><i className="fas fa-times"/></button>
                                        {GenerateSelect(item.uid)}
                                    </div>
                                </td>
                                <td>{GenerateSelect_dir(item.order, item.uid)}</td>
                            </tr>
                        )
                    });
                }


                trs_sorts.push(
                    <tr key={"none"}>
                        <td>{GenerateSelect("")}</td>
                        <td>{GenerateSelect_dir("asc", "")}</td>
                    </tr>
                );



                return <div>
                    <table className={"table table-borderless"}>
                        <thead>
                        <tr>
                            <th>Column</th>
                            <th>Direction</th>
                        </tr>
                        </thead>
                        <tbody>
                        {trs_sorts}
                        </tbody>
                    </table>
                </div>;
            },

            fixModal: () => {
                let trs_columns = [];

                const GenerateCheckbox = (item) => {
                    const onChange = (e) => {
                        item.IsFixed = !!e.target.checked;
                        this.setState({});
                    };
                    return <input
                        type={"checkbox"}
                        onChange={onChange}
                        checked={item.IsFixed == true}
                        className="form-control"
                    />;
                };

                this.state.newHeadData.forEach((item, index) => {
                    if(!item.IsHide) {
                        trs_columns.push(
                            <tr key={index}>
                                <td>{item.name}</td>
                                <td>{GenerateCheckbox(item)}</td>
                            </tr>
                        )
                    }
                });


                return <div style={{overflow: "auto", maxHeight: "calc(100vh - 240px)"}}>
                    <table className={"table table-borderless"}>
                        <thead>
                        <tr>
                            <th>Column</th>
                            <th>Fixed</th>
                        </tr>
                        </thead>
                        <tbody>
                        {trs_columns}
                        </tbody>
                    </table>
                </div>;
            },
        },
    };

    handleCreate = () => {
    };

    onShow = () => {
        if(this.props.TypeModal == "RemoveModal") {
            this.setState({
                isLoadingObj: true,
                objectsDelNames: [],
                cacheObjects: {},
            });
            let objs:any = [];
            Object.keys(this.props.objectsDel).forEach((item, key) => {
                objs.push(item)
            });
            this.core.AJAX.getNameObjects(objs, (answer) => {
                this.setState({isLoadingObj: false, objectsDelNames: answer.data});
            }, (answer) => {
                this.setState({isLoadingObj: false, objectsDelNames: []});
            });
        }
        else if(this.props.TypeModal == "SortModal") {
            this.setState({view_sorts: JSON.parse(JSON.stringify(this.props.view_sorts))})
        }
        else if(this.props.TypeModal == "FixModal") {
            this.setState({newHeadData: JSON.parse(JSON.stringify(this.props.HeadData))})
        }
    };

    helper = {

    };

    render() {
        let Title = "";
        let Body = <div/>;
        let Footer = <div/>;
        let isBlocking = false;

        if(this.props.TypeModal == "RemoveModal") {
            Title = "Delete";
            Body = this.core.build.removeModal();
            Footer = <div>
                <Button variant="success" onClick={this.events.handleDeleteObj}>
                    Delete
                </Button>
                <Button variant="secondary" onClick={this.events.handleClose}>
                    Close
                </Button>
            </div>;

            isBlocking = this.state.isLoadingObj;
        }

        if(this.props.TypeModal == "SortModal") {
            Title = "Multiple sorting";
            Body = this.core.build.sortModal();
            Footer = <div>
                <Button variant="success" onClick={this.events.handleSaveSort}>
                    Apply
                </Button>
                <Button variant="secondary" onClick={this.events.handleClose}>
                    Close
                </Button>
            </div>;
        }

        if(this.props.TypeModal == "FixModal") {
            Title = "Fixed columns";
            Body = this.core.build.fixModal();
            Footer = <div>
                <Button variant="success" onClick={this.events.handleSaveFixed}>
                    Apply
                </Button>
                <Button variant="secondary" onClick={this.events.handleClose}>
                    Close
                </Button>
            </div>;
        }

        return (
            <Modal onShow={this.onShow} animation={true} show={this.props.show} onHide={this.events.handleClose} enforceFocus={false} >
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}