import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import CoolTree from '../../components/CoolTree'
import CoolMarketFilter from "../../components/CoolMarketFilter/CoolMarketFilter";
import {IDataMarketFilter} from "../../stores/ViewStore";
import * as ot_api from "../../api/ObjTree-api";



interface IViewTreeInstanceProps {
    ViewStore?: {
        isLoadMarketFilter: boolean,
        DataMarketFilter: IDataMarketFilter
    }
}
interface IViewTreeInstanceState {

}

@inject("ToastrStore")
@inject("ViewStore")
@observer
export default class ViewTreeInstance extends React.Component<IViewTreeInstanceProps, IViewTreeInstanceState> {
    static defaultProps = {
    };
    state: Readonly<{}> = {
    };
    constructor(props: {}) {
        super(props);
    };
    componentDidMount(): void {
    };
    componentWillUnmount(): void {
        this.props.ViewStore.isLoadMarketFilter = false;
        this.props.ViewStore.DataMarketFilter = {
            TableID: null,
            onClickSubmitBtn: null,
            onClickResetBtn: null,
            Elements: [],
        }
    }

    core = {
    };

    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;

        return (
            <div id="zt_FilterSidebar_body_co">
                <BlockUi className={"cool_t_cloth"} tag="div" loader={LoaderBlockUI} blocking={!this.props.ViewStore.isLoadMarketFilter}>
                    <CoolMarketFilter {...this.props.ViewStore.DataMarketFilter} />
                </BlockUi>
            </div>
        );
    }
}
