import * as React from "react";
import * as ReactDOM from "react-dom";
import {inject, observer, Provider} from "mobx-react";
import { HashRouter as Router, Route, Link, Switch as SwitchRouter, Redirect, RouteProps, RouteComponentProps, useRouteMatch, useParams} from 'react-router-dom'
import axios from 'axios';

import HZTabs from "../../components/HZTabs";
import Loader from "react-loaders";
import BlockUi from "react-block-ui";

import RootStore from '../../stores/RootStore'
import ToastrStore from '../../stores/ToastrStore'
import {IValueFile} from "../../api/ObjTree-api";
import * as v_api from "../../api/View-api";
import ViewStore from "../../stores/ViewStore";
import ObjectsStore from "../../stores/ObjectsStore";
import ImportStore from "../../stores/ImportStore";
import {ViewTableCloth} from "./ViewTableCloth";

import * as ot_api from '../../api/ObjTree-api'
import * as ct_api from '../../api/ClassTree-api'
import * as f_helper from "../../helpers/form_datatype_helper";


//#region material UI
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';

interface UIStyles extends Partial<Record<SwitchClassKey, string>> {
    focusVisible?: string;
}
interface UIProps extends SwitchProps {
    classes: UIStyles;
}
const IOSSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 42,
            height: 26,
            padding: 0,
            margin: theme.spacing(1),
        },
        switchBase: {
            padding: 1,
            '&$checked': {
                transform: 'translateX(16px)',
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#52d869',
                    opacity: 1,
                    border: 'none',
                },
            },
            '&$focusVisible $thumb': {
                color: '#52d869',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 24,
            height: 24,
        },
        track: {
            borderRadius: 26 / 2,
            border: `1px solid ${theme.palette.grey[400]}`,
            backgroundColor: theme.palette.grey[50],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }: UIProps) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});
//#endregion
import ItemAttr from "../ObjectsPage/Interfaces/IitemAttr";

export interface IViewWindowFormViewProps {
    typeModal: "edit" | "read" | "add",
    isAllowEdit: boolean,

    AppUid: string,
    ParentView: string,
    ActiveTabUid?: string, //Вкладка для таблицы по-умолчанию

    name: string,
    tds_form: {
        data_td: any,
        data?: any,
    }[],

    selectData?: {
        ObjID?: string,
        TypeID?: string,
        ParrentObjID?: string,
        ParrentTypeID?: string,
        Name?: string,
    },

    onClose: () => void,
    savedObject?: () => void,
    onChangeTypeModal: (newType: "add" | "edit" | "read") => void,


    IsZaglushka?: boolean,
    match?: any,
    history?: any,
    location?: any,

    SaveFunc_callback?: (callback:(Conf:{SuccFunc:()=>void})=>void) => void,
    onChangeModalName?: (props) => void,
    addedObject?: (config:{newNode:{ObjID:string, Name:string}, selectData}) => void,


    NewModal?: (params:{url_modal:string}) => void,
    RemoveModals?: (ids_arr:string[]) => void, //Удаляем формы через этот метод
}
interface IViewWindowFormViewState {
    nameModal: string,

    isLoadModalContent: boolean,
    isLoadSaveContent: boolean,

    activeFormOrTableTab: "form" | "tables",
    activeDownTablesTab?: string,
    activeTablesTab?: string,
    activeDownTablesTabData?: {uid:string,App:string},
    activeTablesTabData?: {uid:string,App:string},
    tabsDownTablesData: {uid:string,App:string}[], //array for downTables rClickCM
    ZAGLUSHKA_NAME: string,

    attrData: ItemAttr[],
    tree_data: any, //

    attrDataCache: object,
    attrDataFileCache: object,

    activeTabs: {[uid:string]: any}
}

//@inject("ToastrStore")
//@inject("ObjectsStore")
//@observer
export class ViewWindowFormView extends React.Component<IViewWindowFormViewProps, IViewWindowFormViewState> {
    static defaultProps = {
        typeModal: "edit"
    };
    state: Readonly<IViewWindowFormViewState> = {
        nameModal: "Loading...",

        isLoadModalContent: true,
        isLoadSaveContent: false,

        activeFormOrTableTab: "form",
        activeDownTablesTab: null,
        activeTablesTab: null,
        activeDownTablesTabData: null,
        activeTablesTabData: null,
        tabsDownTablesData: [],
        ZAGLUSHKA_NAME: "Tables",

        tree_data: [],
        attrData: [],
        attrDataCache: {},
        attrDataFileCache: {},

        activeTabs: {},
    };
    xhrGenerateModalContent = {cancel: null};

    constructor(props: IViewWindowFormViewProps) {
        super(props);
    };
    componentDidMount(): void {
        this.core.GenerateModalContent();
    };

    shouldComponentUpdate(nextProps: Readonly<IViewWindowFormViewProps>, nextState: Readonly<IViewWindowFormViewState>, nextContext: any): boolean {
        if(nextProps.SaveFunc_callback) {
            nextProps.SaveFunc_callback(this.events.saveObject);
            return false;
        }
        return true
    }

    componentDidUpdate(prevProps: Readonly<IViewWindowFormViewProps>, prevState: Readonly<IViewWindowFormViewState>, snapshot?: any): void {
    };

    events = {
        onClickFormTab: (e, dataTab, dataTabs) => {
            if(dataTab?.uid) {
                dataTabs.forEach((item_view) => {
                    if(this.state.activeTabs[item_view.uid] != undefined) {
                        delete this.state.activeTabs[item_view.uid];
                    }
                });
                this.state.activeTabs[dataTab.uid] = dataTab;
                this.setState({});
            }
        },
        onClickFormOrTableTab: (e, GrTypeForm) => {
            let activeDownTablesTab = this.state.activeDownTablesTab;
            let activeDownTablesTabData = this.state.activeDownTablesTabData;

            let activeTablesTab = this.state.activeTablesTab;
            let activeTablesTabData = this.state.activeTablesTabData;

            if(!this.state.activeDownTablesTab) {
                if(this.state.tabsDownTablesData.length > 0) {
                    activeDownTablesTab = this.state.tabsDownTablesData[0].uid;
                    activeDownTablesTabData = this.state.tabsDownTablesData[0];
                }
            }


            if(this.state.tree_data?.[0]?.tables?.length > 0) {
                if(!this.state.activeTablesTabData) {
                    activeTablesTab = this.state.tree_data[0].tables[0].uid;
                    activeTablesTabData = this.state.tree_data[0].tables[0];
                }
            }

            this.setState({
                activeFormOrTableTab: GrTypeForm,
                activeDownTablesTab: activeDownTablesTab,
                activeDownTablesTabData: activeDownTablesTabData,

                activeTablesTab: activeTablesTab,
                activeTablesTabData: activeTablesTabData,
            });
        },
        onClickTablesTab: (e, GrTypeForm, data) => {
            this.setState({activeTablesTab: GrTypeForm, activeTablesTabData: data});
        },

        saveObject: (Conf) => {
            if(this.state.isLoadModalContent || this.state.isLoadSaveContent) {
                //Всякие кнопки сохранения объекта работать не будут, пока не прогрузится форма
                return false;
            }
            let values = [];
            let Name_obj = "";
            let ValidErrorArr = [];
            this.state.attrData.forEach( (item, index) => {
                let _item = {
                    value: item.value ? item.value.value : item.value,
                    attr: item.uid,
                    key: item.isKey ? 1 : 0,
                    datatype: item.datatype,
                };

                if(item.datatype != "counter") {
                    if(item.datatype == "object") {
                        _item.value = item.value &&  item.value.uid ? item.value.uid : "";
                    }
                    if(this.state.attrDataCache[item.uid] != undefined) {
                        _item.value = this.state.attrDataCache[item.uid].value;
                        if(item.datatype == "object") {
                            _item.value = _item.value && Object.keys( _item.value).length > 0 ? _item.value : "";
                        }
                        values.push(_item);
                    }
                    else if(item.isKey) {
                        values.push(_item);
                    }
                    if(item.isKey) {
                        Name_obj += (_item.value === null ? '' : _item.value) + ' ';
                    }
                    if((item.require && !_item.value) || (item.isKey && !_item.value)) {
                        if(item.datatype == "file") {
                            if((this.state.attrDataFileCache[item.uid] == undefined || this.state.attrDataFileCache[item.uid].files.length == 0) && !item.value  ) {
                                ValidErrorArr.push({name: item.name, text: "Enter value!"})
                            }
                        }
                        else {
                            ValidErrorArr.push({name: item.name, text: "Enter value!"})
                        }
                    }
                }
            });
            if(ValidErrorArr.length > 0) {
                ToastrStore.clear();
                ValidErrorArr.forEach((item_err) => {
                   ToastrStore.error("parameter|" + item_err.name + "| " + item_err.text);
                });
            }
            else {
                if(Name_obj) {
                    Name_obj = Name_obj.slice(0, -1)
                }
                this.setState({isLoadSaveContent: true});
                this.core.AJAX.saveObject(this.props.selectData.ObjID, this.props.selectData.TypeID, this.props.selectData.ParrentObjID, values, (data) => {
                    ToastrStore.success('Success!', 'Success!');
                    this.setState({isLoadSaveContent: false});

                    this.helper.saveCache();

                    this.props.selectData.Name = Name_obj;
                    if(this.props.onChangeModalName) {
                        this.props.onChangeModalName(this.props);
                    }

                    let Files = [];
                    let values:IValueFile[] = [];
                    let iter = 0;
                    if(Object.keys(this.state.attrDataFileCache).length > 0) {
                        RootStore.ProgressIsOpen = true;
                        for( let key_FileCache in this.state.attrDataFileCache) {
                            let File_o = this.state.attrDataFileCache[key_FileCache];
                            let Files_tmp = File_o.files;
                            for( let i = 0; i < Files_tmp.length; i++ ) {
                                Files.push(Files_tmp[i]);
                                values.push({
                                    uid: File_o.data.uid, //uid-параметра
                                    new: 1, //0/1
                                    key: Number(File_o.data.isKey), //0/1
                                    value: iter,
                                    del: 0,
                                });
                                iter ++;
                            }
                        }
                        this.core.AJAX.addFile(this.props.selectData.ObjID, this.props.selectData.TypeID,
                            this.props.selectData.ParrentObjID, Files, values,
                            (answer) => {
                                this.helper.saveFileCache(answer);
                                RootStore.ProgressIsOpen = false;
                                this.events.savedObject(Conf);

                            }, () => {
                                RootStore.ProgressIsOpen = false;
                            });
                    }
                    else {
                        this.events.savedObject(Conf);
                    }

                }, (data) => {
                    ToastrStore.error('Error!', 'ERROR CODE='+data.result);
                    this.setState({isLoadSaveContent: false});
                });
            }
        },
        savedObject: (Conf) => {
            //Сюда перейдет, как только мы сохранили все файлы, либо файлов нет
            if(this.props.savedObject) {
                this.props.savedObject();
            }
            if(Conf) {
                if(Conf.SuccFunc) {
                    Conf.SuccFunc();
                }
            }
        },

        addObject: (Conf) => {
            if(this.state.isLoadModalContent || this.state.isLoadSaveContent) {
                //Всякие кнопки сохранения объекта работать не будут, пока не прогрузится форма
                return false;
            }
            let values = [];
            let Name_obj = "";
            let ValidErrorArr = [];

            this.state.attrData.forEach( (item, index) => {
                let _item = {
                    value: item.value ? item.value.value : item.value,
                    attr: item.uid,
                    key: item.isKey ? 1 : 0,
                    datatype: item.datatype,
                };

                if(_item.datatype != "counter") {
                    if(this.state.attrDataCache[item.uid] != undefined) {
                        _item.value = this.state.attrDataCache[item.uid].value;
                        if(item.datatype == "object") {
                            _item.value = _item.value && Object.keys( _item.value).length > 0 ? _item.value : "";
                        }
                        values.push(_item);
                    }
                    else if(item.isKey) {
                        values.push(_item);
                    }

                    if(item.isKey) {
                        Name_obj += (_item.value === null ? '' : _item.value) + ' ';
                    }

                    if((item.require && !_item.value) || (item.isKey && !_item.value)) {
                        if(item.datatype == "file") {
                            if(this.state.attrDataFileCache[item.uid] == undefined || this.state.attrDataFileCache[item.uid].files.length == 0 ) {
                                ValidErrorArr.push({name: item.name, text: "Enter value!"})
                            }
                        }
                        else {
                            ValidErrorArr.push({name: item.name, text: "Enter value!"})
                        }
                    }
                }
            });

            if(ValidErrorArr.length > 0) {
                ToastrStore.clear();
                ValidErrorArr.forEach((item_err) => {
                    ToastrStore.error("parameter|" + item_err.name + "| " + item_err.text);
                });
            }
            else {
                this.setState({isLoadSaveContent: true});
                if(Name_obj) {
                    Name_obj = Name_obj.slice(0, -1)
                }
                this.core.AJAX.addObject(this.props.selectData.TypeID, this.props.selectData.ParrentObjID, values, (answer) => {
                    ToastrStore.success('Success!', 'Success!');
                    this.setState({isLoadSaveContent: false});
                    this.helper.saveCache();

                    let new_ObjID = answer.data.uid;
                    let new_Name = answer.objName;

                    let Files = [];
                    let values:IValueFile[] = [];
                    let iter = 0;
                    if(Object.keys(this.state.attrDataFileCache).length > 0) {
                        RootStore.ProgressIsOpen = true;
                        for( let key_FileCache in this.state.attrDataFileCache) {
                            let File_o = this.state.attrDataFileCache[key_FileCache];
                            let Files_tmp = File_o.files;
                            for( let i = 0; i < Files_tmp.length; i++ ) {
                                Files.push(Files_tmp[i]);
                                values.push({
                                    uid: File_o.data.uid, //uid-параметра
                                    new: 1, //0/1
                                    key: Number(File_o.data.isKey), //0/1
                                    value: iter,
                                    del: 0,
                                });
                                iter ++;
                            }
                        }

                        this.core.AJAX.addFile(new_ObjID, this.props.selectData.TypeID, this.props.selectData.ParrentObjID, Files, values, () => {
                            this.setState({attrDataFileCache: {}});
                            RootStore.ProgressIsOpen = false;
                            this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}, ...Conf});
                        }, () => {
                            this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}, ...Conf});
                            RootStore.ProgressIsOpen = false;
                        });
                    }
                    else {
                        this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}, ...Conf});
                    }

                }, (data) => {
                    // ToastrStore.error('Error!', 'ERROR CODE='+data.result);
                    this.core.showErrors(data.errors);
                    this.setState({isLoadSaveContent: false});
                });
            }
        },
        addedObject: (Conf?: {SuccFunc?:()=>void, ObjID?:string, Name?:string }) => {
            if(Conf) {
                if(this.props.addedObject) {
                    this.props.addedObject({
                        newNode: {ObjID: Conf.ObjID, Name: Conf.Name},
                        selectData: this.props.selectData,
                    })
                }
                if(Conf.SuccFunc) {
                    Conf.SuccFunc();
                }
            }
        },
    };

    core = {
        AJAX: {
            getAttributesOfGroup: (type:string,group:string, SuccFunc, ErrFunc) => {
                ct_api
                    .getAttributesOfGroup(type, group)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch( (error) => {
                        ErrFunc({result: 0});
                    })
            },
            addObject: function (type:string, parent:string, values: ot_api.IValue[], SuccFunc, ErrFunc) {
                ot_api
                    .addObject(type, parent, values)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch(function (error) {
                        ErrFunc({result: 0});
                    })
            },



            getWindowData:  (data:v_api.IWindowData, SuccFunc, ErrFunc) => {
                if(this.xhrGenerateModalContent.cancel) {
                    this.xhrGenerateModalContent.cancel();
                }
                v_api
                    .getWindowData(data, this.xhrGenerateModalContent)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch( (error) =>{
                        if(axios.isCancel(error)) {
                            console.error("Отмена");
                            //ручная отмена
                        }
                        else {
                            console.log(error);
                            ErrFunc({result: 0});
                        }
                    })
            },

            getAllValues:  (uid:string, SuccFunc, ErrFunc) => {
                if(this.xhrGenerateModalContent.cancel) {
                    this.xhrGenerateModalContent.cancel();
                }
                ot_api
                    .getAllValues(uid, this.xhrGenerateModalContent)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch( (error) =>{
                        if(axios.isCancel(error)) {
                            console.error("Отмена");
                            //ручная отмена
                        }
                        else {
                            console.log(error);
                            ErrFunc({result: 0});
                        }
                    })
            },
            getTablesDown: (SuccFunc) => {
                //Тут мы будем получать наши "виртуальные приложения" для построения вкладок с табличными холстами
                if(this.xhrGenerateModalContent.cancel) {
                    this.xhrGenerateModalContent.cancel();

                    this.core.AJAX.getParrentViews({view:this.props.ActiveTabUid},
                        (answer) => {
                            if(answer.data.length > 0) {
                                this.setState({ZAGLUSHKA_NAME: answer.data[0].name})
                                SuccFunc({
                                    data: [{
                                        App: "Main",
                                        uid: "MainVirtual"
                                    }]
                                })
                            }
                            else {
                                SuccFunc({data:[]});
                            }
                        },
                        (answer) => {
                            SuccFunc({data:[]});
                        });


                }
            },

            getParrentViews: (param:{view:string}, SuccFunc, ErrFunc) => {
                if(this.xhrGenerateModalContent.cancel) {
                    this.xhrGenerateModalContent.cancel();
                }
                v_api.getParrentViews(param, this.xhrGenerateModalContent)
                    .then((res)=>{
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch((er)=>{
                        ErrFunc({result:0});
                    })
            },

            saveObject: function (uid:string, type:string, parent:string, values: ot_api.IValue[], SuccFunc, ErrFunc) {
                ot_api
                    .saveObject(uid, type, parent, values)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrFunc({result: 0});
                    })
            },
            addFile: (uid:string, type:string, parent:string, Files:any, values: IValueFile[], SuccFunc, ErrorFunc) => {
                ot_api
                    .addFile(uid, type, parent, Files, values, (progressEvent) => {
                        let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                        RootStore.ProgressData.completed = percentCompleted;
                    })
                    .then(res => {
                        SuccFunc(res.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrorFunc();
                    })
            },
        },

        GenerateModalContent: () => {
            if(!this.state.isLoadSaveContent) {
                this.setState({isLoadModalContent: true});
            }

            const GenerateDownTablesTab = () => {
                this.core.AJAX.getTablesDown((answer) => {
                    this.setState({
                        tabsDownTablesData: answer.data,
                        isLoadModalContent: false
                    });
                });
            };
            let attrData = [];
            let treeData = [];
            let nameModal = "#loading error#";

            if(this.props.typeModal == "add") {
                let nameModal = "Add in " +  this.props.selectData.Name;
                this.core.AJAX.getWindowData({app: this.props.AppUid, obj: null, view: this.props.ActiveTabUid, is_add: 1, type: this.props.selectData.TypeID }, (answer) => {
                    let attrData = [];
                    if(answer.result == 1) {
                        attrData = answer.window_data;
                        treeData = answer.tree_data;
                    }
                    this.helper.generateTabsOpen(treeData);
                    this.setState({attrData: attrData, tree_data: treeData, attrDataCache: {}, attrDataFileCache: {}, isLoadModalContent: false, nameModal: nameModal})
                }, (answer) => {
                    ToastrStore.error('Server error!', 'ErrorCode='+answer.result);
                    this.setState({ attrDataCache: {}, tree_data: treeData, attrDataFileCache: {}, isLoadModalContent: false, nameModal: nameModal})
                });

            }
            else {
                this.core.AJAX.getWindowData({app: this.props.AppUid, obj: this.props.selectData.ObjID, view: this.props.ActiveTabUid, is_add: 0 }, (data) => {
                    attrData = data.window_data;
                    treeData = data.tree_data;
                    nameModal = this.helper.getNameModal(attrData);
                    this.helper.generateTabsOpen(treeData);
                    if(this.props.typeModal == "edit" || "read") {
                        this.setState({nameModal: nameModal,attrData: attrData, tree_data: treeData, attrDataCache: {}});
                        GenerateDownTablesTab();
                    }
                    else {
                        this.setState({nameModal: nameModal,attrData: attrData, tree_data: treeData, attrDataCache: {}, isLoadModalContent: false})
                    }
                }, (data) => {
                    ToastrStore.error('Server error!', 'ErrorCode='+data.result);
                    this.setState({ attrDataCache: {}, tree_data: treeData, attrDataFileCache: {}, isLoadModalContent: false, nameModal: nameModal})
                });
            }
        },

        build: {
            content: () => {
                let ClothHDR;
                let ClothForm;
                let ClothTables;

                let ClothFormOrTablesTabs;
                let ClothFTR;


                let views = this.state.tree_data?.views ? this.state.tree_data.views : [];
                let tables = this.state.tree_data?.tables ? this.state.tree_data.tables : [];


                ClothFormOrTablesTabs = this.core.build.tabsFormOrTables();

                //Проверка, что это обычная форма
                if(this.state.tabsDownTablesData.length == 0) {
                    ClothForm = <div className={"zt_co_form"}>
                        <form className="zt_form">
                            {this.core.build.form_container(views, {setTableOrForm:2, tables: tables, isRoot: true})}
                        </form>
                    </div>;
                }
                else {
                    ClothForm = <div className={"zt_co_form " + (this.state.activeFormOrTableTab=="tables" ? "hide-el" : "")}>
                        <form className="zt_form">
                            {this.core.build.form_container(views, {setTableOrForm:2, tables: tables, isRoot: true})}
                        </form>
                    </div>;
                    ClothTables = <div className={"zt_co_tables " + (this.state.activeFormOrTableTab=="tables" && this.state.tabsDownTablesData.length > 0?"":"hide-el")}> {this.core.build.downTable()} </div>;
                }

                ClothHDR = <div className={"cof_hdr"}>
                    <div className={"cof_titlebar"}>
                        <div className={"cof_title"}>{this.state.nameModal}</div>
                    </div>
                    <div className="cof_hdr_btns_co">
                        <button className="cof_hdr_btn" onClick={()=>{
                            this.props.onClose();
                        }}><i className="fas fa-times" /></button>
                    </div>
                </div>;

                let FTR_FixedControls = [];
                let FTR_Controls = [];

                if(this.props.typeModal == "add") {
                    FTR_Controls.push(
                        <button key={"add_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                            this.events.addObject({SuccFunc: () => { this.props.onClose(); }} );
                        }} >Add</button>
                    );
                }
                else {
                    if(this.props.isAllowEdit) {
                        FTR_FixedControls.push(
                            <FormControlLabel
                                key={"sw_type_modal"}
                                className={"cof_ftr_switch_label"}
                                control={<IOSSwitch checked={this.props.typeModal != "read"}
                                                    onChange={(e)=>{
                                                        let newTypeModal:"edit"|"read" = this.props.typeModal == "edit" ? "read" : "edit";
                                                        if(this.props.onChangeTypeModal) {
                                                            this.props.onChangeTypeModal(newTypeModal);
                                                        }
                                                    }} name="checkedA" />}
                                label="Edit mode"
                                color="success"
                            />
                        );

                        FTR_Controls.push(
                            <button key={"save_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                this.events.saveObject({});
                            }} >Save</button>
                        );
                        FTR_Controls.push(
                            <button key={"save_ftr_btn_yet"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                this.events.saveObject({SuccFunc: () => { this.props.onClose(); }});
                            }} >Save &amp; Close</button>
                        );
                    }
                }


                ClothFTR = <div className={"cof_ftr"}>
                    <div className={"cof_ftr_fixed_btns_co"}>
                        {FTR_FixedControls}
                    </div>
                    <div className={"cof_ftr_btns_co"}>
                        {FTR_Controls}
                        <button key={"close_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                            this.props.onClose();
                        }}><i className="fas fa-times"/>Close</button>
                    </div>
                </div>;



                return (
                    <div className={"zt_form_content zt_modal_form"}>
                        {ClothHDR}
                        <div className={"zt_form_tabs_bar"}>
                            {ClothFormOrTablesTabs}
                        </div>
                        {ClothForm}
                        {ClothTables}
                        {ClothFTR}
                    </div>
                );
            },

            downTable: () => {
                let ViewTableVisible;
                if(this.state.activeDownTablesTab) {
                    let SelectObjs = [];
                    this.props.tds_form.forEach( (item_td, key_td) => {
                        if(item_td.data_td.o && SelectObjs.indexOf(item_td.data_td.o) == -1 ) {
                            SelectObjs.push(item_td.data_td.o);
                        }
                    });
                    const DataViewTableCloth = {
                        id: "window-down-table-" + this.state.activeDownTablesTabData.uid,

                        ParentApp: this.props.AppUid,
                        ParentView: this.props.ActiveTabUid,

                        AppUid: this.state.activeDownTablesTabData.uid,
                        ActiveTabUid: null,

                        ActiveTabName: this.props.name,
                        tds_form: this.props.tds_form,
                        SelectObjs: SelectObjs,
                    };
                    ViewTableVisible =
                        <Provider ToastrStore={ToastrStore} RootStore={RootStore} ViewStore = {ViewStore} ObjectsStore = {ObjectsStore} ImportStore={ImportStore}>
                            <ViewTableCloth {...DataViewTableCloth} />
                        </Provider>;
                }

                return ViewTableVisible;
            },
            Table: () => {
                let ViewTableVisible;
                if(this.state.activeTablesTab) {
                    let isHideTabs = true;


                    const DataViewTableCloth = {
                        id: "window-down-table-" + this.state.activeTablesTab,

                        ParentApp: this.props.AppUid,
                        ParentView: this.props.ActiveTabUid,

                        AppUid: this.props.AppUid,
                        ViewUid: this.state.activeTablesTabData.uid,
                        ActiveTabUid: null,

                        ActiveTabName: this.props.name,
                        SelectObjs: [],
                        ParentObj: this.props.selectData.ObjID,
                        IsZaglushka: false,
                        IsHideTabs: false,
                    };
                    if(isHideTabs) {
                        DataViewTableCloth.IsHideTabs = true;
                        DataViewTableCloth.ActiveTabUid = this.state.activeTablesTab;
                    }

                    ViewTableVisible =
                        <Provider ToastrStore={ToastrStore} RootStore={RootStore} ViewStore = {ViewStore} ObjectsStore = {ObjectsStore} ImportStore={ImportStore}>
                            <ViewTableCloth {...DataViewTableCloth} />
                        </Provider>;
                }

                return ViewTableVisible;
            },


            //контейнер формы
            form_container: (views_data, parent_view) => {

                let table_tab_content;
                if(parent_view?.setTableOrForm == 2 && parent_view?.tables?.length > 0) {
                    table_tab_content = this.core.build.table_tab_content(parent_view.tables);
                }
                let form_or_table_tabs = this.core.build.form_or_table_tabs(views_data, parent_view);
                let form_tab_content = this.core.build.form_tab_content(views_data, parent_view);

                if(form_or_table_tabs || form_tab_content || table_tab_content) {
                    return <div className={"zt_form_container"}>
                        {form_or_table_tabs}
                        {form_tab_content}
                        {table_tab_content}
                    </div>;
                }
                else {
                    return '';
                }
            },
            //Вкладкии контейнера формы  //Вкладкии для табличного контейнера
            form_or_table_tabs: (views_data, parent_view) => {
                let DataTabs = [];
                let classActive = "active";
                let all_data = views_data;

                if(parent_view?.setTableOrForm == 2 && parent_view?.tables?.length > 0) {
                    all_data = views_data.concat(parent_view.tables);
                }

                all_data.forEach((item_view, key_view) => {
                    let Name = item_view.name;
                    if(item_view.setTableOrForm == 0) {
                        Name = <React.Fragment><i className={"fas fa-table"} style={{fontSize:"8px", marginRight: "4px"}} /> {item_view.name}</React.Fragment>;
                    }

                    DataTabs.push({
                        className: "" + (this.state.activeTabs[item_view.uid] != undefined ? classActive : ""),
                        Name: Name,
                        key: "form"+key_view,
                        onClick: (e) => { this.events.onClickFormTab(e, item_view, all_data); },
                        onContextMenu: (e) => {
                            e.preventDefault();
                            return false;
                        }
                    });
                });


                let is_hide_class = "";
                //Если одна вкладка и корень, то мы скрываем панель вкладок
                if(parent_view?.isRoot && DataTabs.length == 1) {
                    is_hide_class = "hide-el";
                }

                const dataHZTabs = {
                    callbackRecalc: () => {},
                    className: "zt_pp_form_tabs " +is_hide_class,
                    data: DataTabs
                };

                if(DataTabs.length > 0) {
                    return (
                        <HZTabs {...dataHZTabs} />
                    );
                }
                else {
                    return "";
                }
            },
            //Контейнеры для вкладок формы
            form_tab_content: (views_data, parent_view) => {
                let containers = [];
                views_data.forEach((item_view, key_view) => {
                    let table_container;
                    if(item_view.setTableOrForm != 2) {
                        table_container = this.core.build.table_container(item_view.tables);
                    }
                    containers.push(<div key={key_view} className={'zt_form_tab_content ' + (this.state.activeTabs[item_view.uid] == undefined ? "hide-el" : "")}>
                        {this.core.build.form_params(item_view.params_data)}
                        {table_container}
                        {this.core.build.form_container(item_view.views, item_view)}
                    </div>)
                });
                if(containers.length > 0) {
                    return containers;
                }
                else {
                    return "";
                }
            },
            //Параметры контейнера формы
            form_params: (params_data) => {
                let controls = [];
                params_data.forEach( (item, index) => {
                    const paramsControl:f_helper.IFormDatatypeClass = {
                        item: item,
                        key: index,
                        attrData: this.state.attrData,
                        attrDataCache: this.state.attrDataCache,
                        attrDataFileCache: this.state.attrDataFileCache,
                        type: this.props.typeModal,
                        ObjID: this.props.selectData.ObjID,
                        TypeID: this.props.selectData.TypeID,

                        ChangeStateFunc: () => { this.setState({});}, // this.setState();
                    };
                    let itemControl = new f_helper.FormDatatypeClass(paramsControl);
                    controls.push(itemControl.GetControl());
                });

                if(controls.length > 0) {
                    return <div className={"zt_form_params"}>
                        {controls}
                    </div>;
                }
                else {
                    return '';
                }
            },

            //контейнер таблиц внутри контейнера формы
            table_container: (tables_data) => {
                if(tables_data.length > 0) {
                    return <div className={'zt_table_container'}>
                        {this.core.build.form_or_table_tabs(tables_data, {})}
                        {this.core.build.table_tab_content(tables_data)}
                    </div>
                }
                else {
                    return "";
                }
            },
            //контент табличной вкладки
            table_tab_content: (tables_data) => {
                let container;
                tables_data.forEach((item_view, key_view) => {
                    if(this.state.activeTabs[item_view.uid] != undefined) {

                        let isHideTabs = true;
                        const DataViewTableCloth = {
                            id: "window-down-table-" + item_view.uid,

                            ParentApp: this.props.AppUid,
                            ParentView: this.props.ActiveTabUid,

                            AppUid: this.props.AppUid,
                            ViewUid: item_view.uid,
                            ActiveTabUid: item_view.uid,

                            ActiveTabName: this.props.name,
                            SelectObjs: [],
                            ParentObj: this.props.selectData.ObjID,
                            IsZaglushka: false,
                            IsHideTabs: true,
                        };
                        if(isHideTabs) {
                            DataViewTableCloth.IsHideTabs = true;
                            //DataViewTableCloth.ActiveTabUid = this.state.activeTablesTab;
                        }

                        let ViewTableVisible =
                            <Provider ToastrStore={ToastrStore} RootStore={RootStore} ViewStore = {ViewStore} ObjectsStore = {ObjectsStore} ImportStore={ImportStore}>
                                <ViewTableCloth {...DataViewTableCloth} />
                            </Provider>;

                        container = <div className={"zt_table_tab_content"}>{ViewTableVisible}</div>;
                    }
                });

                return container;
            },


            tabsFormOrTables: () => {
                let DataTabs = [];
                let classActive = "active";
                if(this.state.tabsDownTablesData.length > 0) {
                    DataTabs.push({
                        className: this.state.activeFormOrTableTab == "form" ? classActive : "",
                        Name: "form",
                        key: "form",
                        onClick: (e) => { this.events.onClickFormOrTableTab(e, "form"); },
                        onContextMenu: (e) => {
                            e.preventDefault();
                            return false;
                        }
                    });
                    DataTabs.push({
                        className: this.state.activeFormOrTableTab == "tables" ? classActive : "",
                        Name: this.state.ZAGLUSHKA_NAME, //"Tables"
                        key: "tables",
                        onClick: (e) => { this.events.onClickFormOrTableTab(e, "tables"); },
                        onContextMenu: (e) => {
                            e.preventDefault();
                            return false;
                        }
                    });
                }
                let is_hide = this.state.tabsDownTablesData.length == 0;

                const dataHZTabs = {
                    callbackRecalc: () => {},
                    className: "pp_form_or_tables_tabs " + (is_hide ? "hide-el" : ""),
                    data: DataTabs
                };

                return (
                    <HZTabs {...dataHZTabs} />
                );
            },
        },

        showErrors: (errors) => {
            if (errors) {
                let text = [];
                for (let i = 0; i < errors.length; i++) {
                    text.push(errors[i].text);
                }
                ToastrStore.error(text.join("\n"), 'Error');
            } else {
                ToastrStore.error('Server error!', 'Error');
            }
        },
    };

    helper = {
        resetCache: () => {},
        saveCache: () => {

            this.state.attrData.forEach( (item, index) => {
                if(this.state.attrDataCache[item.uid] != undefined) {
                    if(!item.value) {
                        item.value = {};
                    }
                    if (item.isArray) {
                        if (item.datatype == 'object') {
                            item.value = this.state.attrDataCache[item.uid].value.map((v) => {
                                return { uid: v.value, name: v.name };
                            });
                        } else {
                            item.value = this.state.attrDataCache[item.uid].value
                        }
                    } else {
                        item.value.value = this.state.attrDataCache[item.uid].value
                    }
                }
            });

            const GenerateControlsRec = (views) => {
                views.forEach( (item_v, index_v) => {
                    item_v.params_data.forEach( (item, index) => {
                        if(this.state.attrDataCache[item.uid] != undefined) {
                            if(!item.value) {
                                item.value = {};
                            }
                            if (item.isArray) {
                                if (item.datatype == 'object') {
                                    item.value = this.state.attrDataCache[item.uid].value.map((v) => {
                                        return { uid: v.value, name: v.name };
                                    });
                                } else {
                                    item.value = this.state.attrDataCache[item.uid].value
                                }
                            } else {
                                item.value.value = this.state.attrDataCache[item.uid].value
                            }
                        }
                    });
                    if(item_v.views.length > 0) {
                        GenerateControlsRec(item_v.views);
                    }
                });
            };
            GenerateControlsRec(this.state.tree_data.views);

            this.setState({attrDataCache: {}});
        },
        saveFileCache: (answer) => {
            this.state.attrData.forEach( (item, key) => {
                if(answer?.data?.[item.uid] != undefined && answer.data[item.uid].result == 1) {
                    if(!item.value) {
                        item.value = {};
                    }

                    if (item.isArray) {
                        item.value = answer.data[item.uid].attr.value
                    }
                    else {
                        item.value = answer.data[item.uid].attr.value
                    }
                }
            });

            const GenerateControlsRec = (views) => {
                views.forEach( (item_v, index_v) => {
                    item_v.params_data.forEach( (item, index) => {
                        if(answer?.data?.[item.uid] != undefined && answer.data[item.uid].result == 1) {
                            if(!item.value) {
                                item.value = {};
                            }
                            if (item.isArray) {
                                item.value = answer.data[item.uid].attr.value
                            }
                            else {
                                item.value = answer.data[item.uid].attr.value
                            }
                        }
                    });
                    if(item_v.views.length > 0) {
                        GenerateControlsRec(item_v.views);
                    }
                });
            };
            GenerateControlsRec(this.state.tree_data.views);

            this.setState({attrDataFileCache: {} });
        },

        getNameModal: (attrData) => {
            let nameModal = "";

            attrData.forEach((item_a, key_a) => {
                if(item_a.isKey) {
                    if(item_a.datatype == "object") {
                        nameModal += (item_a.value && item_a.value.name ? item_a.value.name : "") + ' ';
                    }
                    else {
                        nameModal += (item_a.value ? item_a.value.value : "") + ' ';
                    }
                }
            });

            if(nameModal) {
                nameModal = nameModal.slice(0, -1)
            }

            return nameModal;
        },

        //генерируем список uid вкладок, которые нужно открыть в самом начале построения формы
        generateTabsOpen: (tree_data) => {
            //Пока мы будем просто открывать первую вкладку и первую таблицу
            const recOpen = (tree_data) => {
                if(tree_data?.views?.length > 0) {
                    let view_o = tree_data.views[0];
                    this.state.activeTabs[view_o.uid] = view_o;

                    //Если это контейнер, то мы его открываем и еще открываем форму
                    if(view_o?.setTableOrForm == 2) {
                        recOpen(view_o);
                    }
                    else {
                        if(view_o?.tables?.length > 0) {
                            this.state.activeTabs[view_o.tables[0].uid] = view_o.tables[0];
                        }
                    }
                }
            };

            recOpen(tree_data);



        }
    };


    render() {
        let p_url = "test123";//this.props.p_url;

        const CheckModalUrlOpen = (params) => {
            return null;
        };
        let isBlocking = this.state.isLoadModalContent || this.state.isLoadSaveContent;
        let content = this.core.build.content();
        return (
            <BlockUi className={"zt_window_cloth"} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                {content}
                <Router>
                    <SwitchRouter>
                        <Route path={`${p_url}`} component={CheckModalUrlOpen}/>
                    </SwitchRouter>
                </Router>
            </BlockUi>);
    }
}