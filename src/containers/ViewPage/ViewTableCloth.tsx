/*
Тут происходит генерация табличного холста вместе со вкладками и прочим остальным. Это будет встраиваться как на главную так и в модалку
* */
import * as React from "react";
import {inject, observer} from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import download from 'js-file-download';

import CoolTable, {IajaxData, ITR_tree_head} from '../../components/CoolTable'
import HZTabs from "../../components/HZTabs";

import * as v_api from '../../api/View-api'
import {IDataMarketFilter} from "../../stores/ViewStore"
import ICoolMarketEl from "../../components/CoolMarketFilter/ICoolMarketEl";
import EnumCoolMarketEl from "../../components/CoolMarketFilter/EnumCoolMarketEl";
import HZContextMenu from "../../components/HZContextMenu";
import {CalendarView} from "../../components/CalendarView";
import {ViewChart} from '../../components/ViewChart'
import {getParrentViews} from "../../api/View-api";

import IModalConfig from "../../components/CoolModalForm/IModalConfig";
import {ViewWindowDownView, IViewWindowDownViewProps} from './ViewWindowDownView'
import {ViewWindowFormView, IViewWindowFormViewProps} from './ViewWindowFormView'
import axios from "axios";



import * as d_helper  from "../../helpers/table_datatype_helper"
import AsyncSelect from 'react-select/async';
//#region material UI
import Switch from "react-switch";
import * as f_helper from "../../helpers/form_datatype_helper";
import ToastrStore from "../../stores/ToastrStore";
import * as ot_api from "../../api/ObjTree-api";
import {format, parse} from "date-fns";
import { WithContext as ReactTags } from 'react-tag-input';
import DatePicker from "react-datepicker";
import {object} from "prop-types";
import BModal from "./BModal";

//#endregion

interface ItrBody {
    key: string,
    name: string,
    o: string,
    v: string,
}
interface ITableFilterData extends ICoolMarketEl{
    key: string,
    TypeEl: EnumCoolMarketEl,
    data: any,
    onChange: (e) => void,

    filter_value: any, //Это значение будет приходить с базу
    filter_value_new: any, //Это значение, которое вводим мы
    filter_sett_new: any, //Это значение, которое вводим мы
}

export interface IModalConfigVState extends IModalConfig {
    //может чего добавится
}

export interface IObjectsPageProps {
    id: string, //Главный идентификатор компоненты, так мы сможем определять
    ToastrStore?: any,
    RootStore?: any,
    ViewStore?: {
        isMainTableOpen: boolean,

        ActiveClothID: string,
        isLoadMarketFilter: boolean,
        DataMarketFilter: IDataMarketFilter

    },

    Sort?: {
        Attr: string,
        Dir: 0 | 1,
    },

    ParentApp?: string,
    AppUid: string,
    ParentObj?: string,
    ViewUid?: string,
    ActiveTabUid?: string,
    ActiveTabName?: string,
    ParentView?: string, //uuid, —айди вышестоящего представления, из которого вызываем нижестоящее
    SelectObjs?: string[],

    IsHideTabs?: boolean,
    IsZaglushka?: boolean,

    isFirstRow?: boolean,



}
interface IObjectsPageState {
    isEditMode: boolean,
    isNewMode: boolean,
    isDisplayChart: boolean,
    isHasCharts:boolean,
    select_cell?: {item_tr, item_td, index_td, item_th, isEdit?:boolean, isNotMove?:boolean, attrCache?: any} | never, //isEdit - редактируется ли данная ячейка //isNotMove - можно ли уйти с данной ячейки через стрелки

    RecalcStyleFunc: () => void,

    isLoadTabs: boolean,
    DataTabs: any[],
    ActiveTabUid: string,

    isLoadHead: boolean,
    HeadDataTree: ITR_tree_head[][],
    HeadData: {
        key: string,
        IsFixedWidth: boolean,
        Sort: {isActive: boolean, dir: 0 | 1, disabled: boolean}
        MinWidth: number,
        IsHide: boolean,

        rowspan?: number,

        data: any,
        data_td?: any,
    }[],
    MainClass?: {
        name: string,
        uid: string,
    },
    FilterData: ITableFilterData[],
    FilterGlobal: string,
    isShowColumnFilters: boolean,

    isLoadBody: boolean,
    ReloadTrig?: () => void,
    BodyData: ItrBody[],
    SelectTr: any,
    customData?: any,

    ObjectModalConfig: {
        show: boolean,
        TypeModal: string,
        data?: any,
        SuccessFunction?: () => void,
    },

    isFirstRow: boolean, // Мы будем выбирать первую строку, пока это не станет истиной

    isRequestRunning: boolean,

    //ContextMenu
    targetCM?: HTMLInputElement,
    contentCM?: object[],

    //forms
    Forms: {[id:string] : IModalConfigVState }; //Массив наших форм

    FSettings: any, //Права


    BModal: {
        show: boolean,
        TypeModal: string,
        objectsDel: any,

        columnsSort?: any,
        view_sorts?: any,


        HeadData?: any,
    }
}
@inject("ToastrStore")
@inject("ViewStore")
@inject("RootStore")
@observer
export class ViewTableCloth extends React.Component<IObjectsPageProps, IObjectsPageState> {
    state: Readonly<IObjectsPageState> = {
        isEditMode: false,
        isNewMode: false,

        isDisplayChart:false,
        isHasCharts:false,

        RecalcStyleFunc: null,

        isLoadTabs: false,
        DataTabs: [],
        ActiveTabUid: "",

        isLoadHead: false,
        HeadData: [],
        HeadDataTree: [],
        FilterData: [],

        FilterGlobal: "",
        isShowColumnFilters: false,

        isLoadBody: false,
        BodyData: [],
        SelectTr: null,
        customData: { sort: 1},

        ObjectModalConfig: {
            show: false,
            TypeModal: "add",
            data: {},
        },

        Forms: {},

        FSettings: {},

        isFirstRow: false,

        isRequestRunning: false,


        BModal: {
            show: false,
            TypeModal: "",
            objectsDel: {},

            columnsSort: [],
            view_sorts: [],
        }
    };
    tableData: any = [];
    headerData: any = [];
    isReadMode: boolean = false; //Если true, то модакли "по умолчанию" открываются в режиме чтения

    isSaveCellToken:boolean = true; //Если false, то мы не даём перемещаться по ячейке пока не сохранится значение кэша
    isRecalcTable:boolean = true; //При обновлении состояния нужно обновить таблицу


    constructor(props: IObjectsPageProps) {
        super(props);
    };

    componentWillMount(): void {
        document.addEventListener("zt_resize", this.core.resizeTable);
    };
    componentWillUnmount(): void {
        document.removeEventListener("zt_resize", this.core.resizeTable);
        document.removeEventListener("keydown", this.events.onKeyDown);

        //Удаляем все формы привязанные к этому модулю
        Object.keys(this.state.Forms).forEach( (item_k, key_k) => {
            if(this.props.RootStore.Forms[item_k] != undefined) {
                delete this.props.RootStore.Forms[item_k];
            }
        });

        this.props.RootStore.UpdateForms();
    }
    componentDidMount(): void {
        if(this.props.IsZaglushka) {
            this.setState({
                isLoadTabs: true,
                DataTabs: [
                    {
                        Name: this.props.ActiveTabName,
                        View: this.props.ActiveTabUid,
                    }
                ],
                ActiveTabUid: this.props.ActiveTabUid, //Пока по умолчанию выбираем первую вкладку
            });

            this.core.getHeadData({
                uid: this.props.ActiveTabUid,
            });
        }
        else if(this.props.AppUid == "MainVirtual") {
            this.core.getParrentViews({view: this.props.ParentView}, (answer) => {
                if(answer.data.length > 0) {
                    let data_tabs_tmp_zagl = [];

                    answer.data.forEach( (item_v, key_v) => {
                        data_tabs_tmp_zagl.push({
                            Name: item_v.name,
                            View: item_v.uid,
                        })
                    });

                    this.setState({
                        isLoadTabs: true,
                        DataTabs: data_tabs_tmp_zagl,
                        ActiveTabUid: data_tabs_tmp_zagl[0].View, //Пока по умолчанию выбираем первую вкладку
                    });

                    this.core.getHeadData({
                        uid: data_tabs_tmp_zagl[0].View,
                    });
                }
                else {

                }
            }, () => {

            })
        }
        else {
            this.core.getTabsData()
        }

        if(this.props.id == "MainTable") {
            this.events.onClickCloth();
        }

        document.addEventListener("keydown", this.events.onKeyDown);
    }
    componentDidUpdate(prevProps: Readonly<IObjectsPageProps>, prevState: Readonly<IObjectsPageState>, snapshot?: any): void {
        if(prevProps.AppUid != this.props.AppUid || prevProps.ActiveTabUid != this.props.ActiveTabUid) {
            this.setState({
                isLoadTabs: false,
                isLoadHead: false,
                isLoadBody: false,
            });
            this.core.getTabsData()
        }
        else if(prevState.isLoadHead !== this.state.isLoadHead) {
            this.core.Filters.GenerateMarketFilterData();
        }
        else if (!this.isRecalcTable) {
            this.isRecalcTable = true;
            this.setState({
                RecalcStyleFunc: () => {
                    this.setState({RecalcStyleFunc: null});
                }
            });
        }
    }



    events = {
        onClickCloth: (e?) => {
            //Клик по самой компоненте, нужно, чтобы построит боковой фильтр
            if(this.props.ViewStore.ActiveClothID != this.props.id) {
                this.props.ViewStore.ActiveClothID = this.props.id;

                this.core.Filters.GenerateMarketFilterData();
            }
        },

        onClickBody: (e?) => {

            //cool_t_gc_tbody
        },

        onClickTab: (e, item_tab) => {
            let isFirstRow = false;
            let isReload = false;

            if(item_tab?.settings?.select_first_row==1) {
                isFirstRow = true
            }

            if(this.state.ActiveTabUid == item_tab.View) {
                //При обновлении шапки, если вкладка не изменялась, то мы и не сбрасываем фильтры
                isReload = true;
            }
            else {
                this.state.customData.attr_filter = [];
            }

            this.setState({ActiveTabUid: item_tab.View, isLoadHead: false, isFirstRow: isFirstRow});

            if(!item_tab.settings.table_or_form || item_tab.settings.table_or_form==0){
                this.core.getHeadData({
                    uid: item_tab.View,
                    isReload: isReload
                });
            }
        },

        onClickAddBtn: (e, data:{Name:string, TypeID:string, ParentObj: string, ParrentTypeID: string }) => {
            this.core.Forms.GenerateWindowFormView({
                typeModal: "add",
                AppUid: this.props.AppUid,
                ActiveTabUid: this.state.ActiveTabUid,
                ParentView: this.props.ParentView,
                name: data.Name,
                tds_form: [],
                selectData: {
                    TypeID: data.TypeID,
                    ParrentObjID: data.ParentObj,
                    ParrentTypeID: data.ParrentTypeID,
                    Name: data.Name,
                },
                isAllowEdit: true,
            });
        },
        onClickRemoveBtn: () => {
            if(this.state.SelectTr && !this.state.isEditMode) {
                let objects = {};
                this.state.SelectTr.tds.forEach((item, key) => {
                    if(item.o && objects[item.o] == undefined) {
                        let is_read = this.state.FSettings?.[this.state.ActiveTabUid]?.[this.state.HeadData[key].data.class.uid]?.["EditDel"] != 1;
                        objects[item.o] = {class: this.state.HeadData[key].data.class.uid, disabled: is_read};
                    }
                });

                this.state.BModal.show = true;
                this.state.BModal.TypeModal = 'RemoveModal';
                this.state.BModal.objectsDel = objects;

                this.setState({})
            }
        },


        onClickDelBtn: (e) => {
            if(this.state.SelectTr && !this.state.isEditMode) {
                this.state.ObjectModalConfig.data.Name = this.state.SelectTr.tds[0].v;
                this.setState({
                    ObjectModalConfig: {
                        show: true,
                        TypeModal: "del",
                        data: this.state.ObjectModalConfig.data,
                        SuccessFunction: () => {
                            this.core.reload(false);
                        },
                    }
                });
            }
        },
        onClickEditBtn: (e) => {
            if(this.state.SelectTr && !this.state.isEditMode) {
                this.state.ObjectModalConfig.data.Name = this.state.SelectTr.tds[0].v;
                this.setState({
                    ObjectModalConfig: {
                        show: true,
                        TypeModal: "edit",
                        data: this.state.ObjectModalConfig.data,
                        SuccessFunction: () => {
                            this.core.reload(false);
                        },
                    }
                });
            }
        },

        onChangeEditMode: (e) => {
            if(!this.state.isEditMode) {
                this.setState({isEditMode: true});
            }
            else {
                this.setState({isEditMode: false, select_cell: null});
            }
        },


        onDrawTable: (params:{readonly tableData, readonly headerData}) => {
            this.tableData = params.tableData;
            this.headerData = params.headerData;

            if(this.state.isFirstRow) {
                let SelectTr = null;

                if(this.tableData.length > 0) {
                    SelectTr = params.tableData[0];
                }
                this.setState({isFirstRow: true, SelectTr: SelectTr})
            }
        },
        onDoubleClickTD: (e, item_tr, item_td) => {
            if(!this.state.isEditMode) {
                const data_form = {
                    o: "",
                    v: "",

                    is_disabled: true,

                    class: {
                        uid: "",
                        parentUID: "",
                    }
                };

                let tds_c = JSON.parse(JSON.stringify(item_tr.tds));

                //#region class_for_open_form

                let class_for_open_form;
                const tab = this.core.getActiveTabData();
                if(tab?.settings?.class_for_open_form) {
                    class_for_open_form = tab.settings.class_for_open_form;
                }

                //Если выбор по строке
                if(class_for_open_form) {
                    //Производим замену выбранной ячейки на первую, где наш класс
                    let item_td_tmp;
                    let f_item_td_this;
                    for( let i = 0; i < this.state.HeadData.length; i++ ) {
                        let item_th_tmp = this.state.HeadData[i];
                        if(item_th_tmp.data.class.uid == class_for_open_form) {
                            f_item_td_this = item_th_tmp;
                            item_td_tmp = item_tr.tds[i];
                            break;
                        }
                    }
                    if(!item_td_tmp) {
                        ToastrStore.warn('Column of this type was not found!');
                        return;
                    }

                    data_form.v = item_td_tmp.v;
                    data_form.o = item_td_tmp.o;
                    data_form.is_disabled = item_td_tmp.is_disabled;
                    data_form.class.uid = f_item_td_this.data.class.uid;
                    data_form.class.parentUID = f_item_td_this.data.class.parentUID;
                }
                //Если выбор по ячейкам
                else {
                    let f_item_td_this = this.helper.GetDataForColumn(item_td.key);

                    data_form.v = item_td.v;
                    data_form.o = item_td.o;
                    data_form.is_disabled = item_td.is_disabled;
                    data_form.class.uid = f_item_td_this.data.class.uid;
                    data_form.class.parentUID = f_item_td_this.data.class.parentUID;

                }
                //#endregion

                let tds_form = [];
                //Собираем всю информацию о строке в массив
                tds_c.forEach((item_td_tmp, key_td) => {
                    let f_item_td = this.helper.GetDataForColumn(item_td_tmp.key);
                    f_item_td.data_td = item_td_tmp;
                    tds_form.push(f_item_td);
                });


                if(data_form.o) {
                    this.state.ObjectModalConfig.data.Name = data_form.v;
                    this.state.ObjectModalConfig.data.ObjID = data_form.o;
                    this.state.ObjectModalConfig.data.TypeID = data_form.class.uid;
                    this.state.ObjectModalConfig.data.ParentTypeID = data_form.class.parentUID;

                    let typeModal : "add" | "edit" | "read" = "edit";
                    if(data_form.is_disabled) {
                        typeModal = "read";
                    }

                    this.core.Forms.GenerateWindowFormView({
                        typeModal: typeModal,
                        AppUid: this.props.AppUid,
                        ActiveTabUid: this.state.ActiveTabUid,
                        ParentView: this.props.ParentView,
                        name: "tmp/-" + data_form.v,
                        tds_form: tds_form,
                        selectData: {
                            ObjID: data_form.o,
                            TypeID: data_form.class.uid,
                            //    ParrentObjID?: string,
                            ParrentTypeID: data_form.class.parentUID,
                            Name: "tmp/-" + data_form.v,
                        },
                        isAllowEdit: !data_form.is_disabled
                    });

                    /*
                    this.setState({
                        ObjectModalConfig: {
                            show: true,
                            TypeModal: "edit",
                            data: this.state.ObjectModalConfig.data,
                            SuccessFunction: () => {
                                this.core.reload();
                            },
                        }
                    });
                     */
                }
            }
        },
        onRightClickTD: (e, item_tr, item_td) => {
            if(!this.state.isEditMode) {
                let tds_c = JSON.parse(JSON.stringify(item_tr.tds));
                let tds_form = [];
                //Собираем всю информацию о строке в массив
                tds_c.forEach((item_td, key_td) => {
                    let f_item_td = this.helper.GetDataForColumn(item_td.key);
                    f_item_td.data_td = item_td;

                    tds_form.push(f_item_td);
                });

                this.core.ContextMenu.CM_td_generate(e, tds_form);
            }
        },
        onClickTD: (e, params) => {
            if(this.state.isEditMode) {
                if(!this.state.select_cell || this.state.select_cell.item_tr.trKey != params.item_tr.trKey || this.state.select_cell.item_td.key != params.item_td.key) {
                    if(!params.item_td.is_disabled) {
                        this.core.Editable.SaveCellBefore(params);
                    }
                }
            }
        },
        onClickTH: (e, item_th) => {
            if(!item_th.Sort.disabled) {
                let order = "asc";
                if(this.state.customData.view_sorts) {
                    for(let key in this.state.customData.view_sorts) {
                        if(this.state.customData.view_sorts[key].uid == item_th.key) {
                            order = this.state.customData.view_sorts[key].order == "asc" ? "desc" : "asc";
                        }
                    }
                }
                this.state.customData.view_sorts = [
                    {uid: item_th.key, order: order}
                ];
                this.state.HeadData.forEach((item_th2, key_th2) => {
                    if( item_th.key == item_th2.key) {
                        item_th2.Sort.isActive = true;
                        item_th2.Sort.dir = order == "asc" ? 1 : 0;
                    }
                    else {
                        item_th2.Sort.isActive = false;
                    }
                });
                this.core.reload(false);
            }
        },

        onClickEInput: (e) => {
            if(!this.state.select_cell.isNotMove) {
                this.state.select_cell.isNotMove = true;
                this.setState({});
            }
        },
        onDoubleClickFocusInput: (e, item_tr, item_td) => {
            if(this.state.select_cell && this.state.select_cell.item_tr.trKey == item_tr.trKey && this.state.select_cell.item_td.key == item_td.key) {
                e.stopPropagation();
                e.preventDefault();

                let element = e.target;

                if (window.getSelection) {
                    window.getSelection().removeAllRanges();

                   // let element = document.getElementsByClassName('cool_t_focus_cell')[0] as HTMLInputElement;
                    let lengthV = element.value.length;
                    element.setSelectionRange(lengthV, lengthV);
                }

                this.state.select_cell.isEdit = true;
                this.state.select_cell.isNotMove = true;
                this.setState({});

                return false;
            }
        },
        onKeyDown: (e) => {
            if(this.state.isEditMode) {
                if(this.state.select_cell && this.isSaveCellToken) {
                    if(e.target.classList.contains('cool_t_focus_cell') || e.target.classList.contains('kt_e_active_control')) {
                        if(!this.state.select_cell.isNotMove) {
                            if(e.keyCode == 39) {
                                //->
                                let h_data = this.headerData;
                                if(h_data.length > this.state.select_cell.index_td) {
                                    for( let i = this.state.select_cell.index_td + 1; i < h_data.length; i++ ) {
                                        if(!h_data[i].IsHide) {
                                            this.core.Editable.SaveCellBefore({
                                                item_td: this.state.select_cell.item_tr.tds[i],
                                                item_tr: this.state.select_cell.item_tr,
                                                item_th: h_data[i],
                                                index_td: i,
                                            });
                                            e.stopPropagation();
                                            e.preventDefault();
                                            break;
                                        }
                                    }
                                }
                            }
                            else if(e.keyCode == 37) {
                                //<-
                                let h_data = this.headerData;
                                if(this.state.select_cell.index_td > 0) {
                                    for( let i = this.state.select_cell.index_td - 1; i >= 0; i-- ) {
                                        if(!h_data[i].IsHide) {
                                            this.core.Editable.SaveCellBefore({
                                                item_td: this.state.select_cell.item_tr.tds[i],
                                                item_tr: this.state.select_cell.item_tr,
                                                item_th: h_data[i],
                                                index_td: i,
                                            });
                                            e.stopPropagation();
                                            e.preventDefault();
                                            break;
                                        }
                                    }
                                }
                            }
                            else if(e.keyCode == 38) {
                                //up
                                if(this.state.select_cell.item_tr.trKey > 0) {
                                    this.core.Editable.SaveCellBefore({
                                        item_td: this.tableData[this.state.select_cell.item_tr.trKey - 1].tds[this.state.select_cell.index_td],
                                        item_tr: this.tableData[this.state.select_cell.item_tr.trKey - 1],
                                        item_th: this.state.select_cell.item_th,
                                        index_td: this.state.select_cell.index_td,
                                    });
                                    e.stopPropagation();
                                    e.preventDefault();
                                }
                            }
                            else if(e.keyCode == 40) {
                                //down
                                let newTrKey = Number(this.state.select_cell.item_tr.trKey) + 1;
                                if(this.tableData[newTrKey] != undefined) {
                                    this.core.Editable.SaveCellBefore({
                                        item_td: this.tableData[newTrKey].tds[this.state.select_cell.index_td],
                                        item_tr: this.tableData[newTrKey],
                                        item_th: this.state.select_cell.item_th,
                                        index_td: this.state.select_cell.index_td,
                                    });
                                    e.stopPropagation();
                                    e.preventDefault();
                                }

                            }
                        }
                        else {

                        }
                        if(e.which == 27) {
                            if(e.target.classList.contains('kt_e_active_control')) {
                                this.state.select_cell.isNotMove = false;
                                this.state.select_cell.isEdit = false;
                                this.state.select_cell.attrCache = {};
                                this.setState({});
                            }
                        }
                    }
                }
            }
        },
        onChangeFocusInput: (e) => {
            //Если это событие происходит, то мы рдактируем ячейку
            if(!this.state.select_cell.isEdit) {
                this.state.select_cell.isEdit = true;
                this.state.select_cell.attrCache = this.state.select_cell.attrCache != undefined ? this.state.select_cell.attrCache : {};
                if(this.state.select_cell.item_th?.data?.datatype == "number" || this.state.select_cell.item_th?.data?.datatype == "text") {
                    if(e.nativeEvent.inputType == "insertText") {
                        this.state.select_cell.attrCache.value = e.nativeEvent.data;
                    }
                }
                else if(this.state.select_cell.item_th?.data?.datatype == "object") {

                }

                this.setState({});
            }
        },
    };
    core = {
        AJAX: {
            GetSelectData: (type:string, filter:string, SuccFunc, ErrFunc) => {
                ot_api
                    .getObjsByTypeForFilter({type:type, filter:filter})
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (error) => {
                        console.log(error);
                        ErrFunc();
                    })
            },
        },

        getTabsData: () => {
            v_api.getTabsData(this.props.AppUid, this.props.ViewUid)
                .then((res)=>{
                    if(res.data.result == 1) {
                        let data_views = res.data.data_views;
                        let data_app = res.data.data_app;
                        let nTabs = [];
                        let ActiveTabUID = null;
                        let isFirstRow = false;
                        let isEditMode = false;

                        data_views.data.forEach((item) => {
                            if(data_app.FSettings?.[item.View]?.["ExRead"] == 1) {
                                if(this.props.ActiveTabUid && this.props.ActiveTabUid == item.View) {
                                    ActiveTabUID = this.props.ActiveTabUid;
                                    if(item?.settings?.table_edit_mode == "1") {
                                        isEditMode = true;
                                    }
                                    if(item?.settings?.select_first_row==1) {
                                        isFirstRow = true
                                    }
                                }
                                nTabs.push(item);
                            }
                        });

                        if(nTabs.length == 0) {
                            ToastrStore.info('You don\'t have permission!');

                            this.setState({
                                isLoadTabs: false,
                                isLoadHead: false,
                                isLoadBody: false,
                            });
                            return;
                        }

                        if(!ActiveTabUID) {
                            if(nTabs[0]?.settings?.select_first_row==1) {
                                isFirstRow = true
                            }
                            ActiveTabUID = nTabs[0].View;
                            if(nTabs[0]?.settings?.table_edit_mode == "1") {
                                isEditMode = true;
                            }
                        }


                        this.setState({
                            isLoadTabs: true,
                            DataTabs: nTabs,
                            ActiveTabUid: ActiveTabUID, //Пока по умолчанию выбираем первую вкладку
                            isFirstRow: isFirstRow,
                            FSettings: data_app.FSettings,
                            isEditMode: isEditMode,
                        });

                        this.core.getHeadData({
                            uid: ActiveTabUID,
                        });
                    }
                })
                .catch(()=>{

                })
        },
        getHeadData: (params:{uid:string, isReload?: boolean}) => {
            const NParams:v_api.IHeadData = {
                uid: params.uid,
                attr_filter: this.state.customData?.attr_filter ? this.state.customData.attr_filter : [],
                ParentView: this.props.ParentView,
                SelectObjs: this.props.SelectObjs,
                ParentObj: this.props.ParentObj,
                isNewMode: Number(this.state.customData.isNewMode == true),
                ForView: 1,
            };



            v_api.getHeadData(NParams)
                .then((res)=>{
                    let answer = res.data;
                    if(res.data.result == 1) {
                        if(answer.data.length == 0) {
                            this.props.ToastrStore.error('Incorrect settings!', 'Not enough columns to display');
                            this.setState({isLoadHead: true, isLoadBody: true, HeadData: []})
                        }
                        else {
                            const tab = this.core.getActiveTabData();
                            const settingsTab = tab?.settings ? tab.settings : {};

                            let ActiveTabUid = params.uid;
                            let HeaderData = [];
                            let HeadDataTree = [];
                            let TDsCard = []; //Колонки для корр кард
                            let FilterData = [];

                            let SortAttr = "";
                            let dir_sort = 1;
                            let MainClassData;
                            let HatID = null;
                            let isHasCharts=false;
                            let settings;

                            //#region others
                            for( let key in this.state.DataTabs) {
                                if(this.state.DataTabs[key].View == ActiveTabUid) {
                                    settings = this.state.DataTabs[key].settings;
                                    break;
                                }
                            }
                            let view_sorts = [];
                            let view_sorts_obj = [];
                            if(params.isReload) {
                                if(this.state.customData.view_sorts) {
                                    view_sorts = this.state.customData.view_sorts;
                                    view_sorts.forEach((item, index) => {
                                        view_sorts_obj[item.uid] = item;
                                    });
                                }
                            }
                            else {
                                if(settings?.view_sorts) {
                                    view_sorts = settings.view_sorts;
                                    view_sorts.forEach((item, index) => {
                                        view_sorts_obj[item.uid] = item;
                                    });
                                }
                            }
                            let exe_data_source_method = "";
                            let block_calcs = "";
                            if(settings?.exe_data_source_method) {
                                exe_data_source_method = settings.exe_data_source_method;
                            }
                            if(settings?.block_calcs) {
                                block_calcs = settings.block_calcs;
                            }
                            //#endregion


                            let Hat1LvName_tmp;
                            let Hat1LvName_colspan = 1;

                            answer.data.forEach( (item, key) => {
                                let HatLv = 0;
                                let Hat1LvName = "";
                                let rowspan = 1;

                                let IsFixedWidth = false;
                                let IsFixed = false;
                                let NameParam = item.name;
                                let MinWidth = 200;
                                let TypeEl: EnumCoolMarketEl;
                                if(item.datatype == "text") {
                                    TypeEl = EnumCoolMarketEl.stringInput;
                                }
                                else if(item.datatype == "number") {
                                    TypeEl = EnumCoolMarketEl.numberInput;
                                    IsFixedWidth = true;
                                    MinWidth = 200;
                                }
                                else if(item.datatype == "datetime") {
                                    TypeEl = EnumCoolMarketEl.datetime;
                                    MinWidth = 200;
                                    IsFixedWidth = true;
                                }
                                else if(item.datatype == "object") {
                                    TypeEl = EnumCoolMarketEl.objInput;
                                }
                                else if(item.datatype == "file") {
                                    IsFixedWidth = true;
                                    MinWidth = 200;
                                    TypeEl = null;
                                }
                                else {
                                    TypeEl = null;
                                }

                                if(item.attr?.dopSett?.width?.value) {
                                    MinWidth = Number(item.attr.dopSett.width.value);
                                    IsFixedWidth = true;
                                }
                                if(item.attr?.dopSett?.alias) {
                                    NameParam = item.attr.dopSett.alias;
                                }
                                if(item.attr?.dopSett?.isFixed) {
                                    IsFixed = item.attr.dopSett.isFixed == 1;
                                }
                                if(item.attr?.dopSett?.HatID) {
                                    HatID = item.attr.dopSett.HatID;
                                }
                                if(item.attr?.dopSett?.HatLv) {
                                    HatLv = item.attr.dopSett.HatLv;
                                }
                                if(item.attr?.dopSett?.Hat1LvName) {
                                    Hat1LvName = item.attr.dopSett.Hat1LvName;
                                }

                                let Sort  = {isActive: false, dir: 0, disabled: false};

                                if(key == 0 && view_sorts.length == 0) {
                                    //Если нет сортировки, то ставим первую колонку по умолчанию
                                    Sort.isActive = true;
                                    SortAttr = item.uid;
                                    if (item.datatype == "datetime") {
                                        dir_sort = 0;
                                    }
                                    Sort.dir = dir_sort;
                                }

                                if(view_sorts_obj[item.uid] != undefined) {
                                    Sort.isActive = true;
                                    Sort.dir = view_sorts_obj[item.uid].order == "asc" ? 1 : 0;
                                }

                                if(item.class?.parentUID == "00000000-0000-0000-0000-000000000000" && !MainClassData) {
                                    MainClassData = {
                                        uid: item.class.uid,
                                        name: item.class.name,
                                    }
                                }


                                //Если есть разворот
                                if(settingsTab.HatAttr) {
                                    //Проверка на кк-колонку с разваротом
                                    if (HatLv == 3) {
                                        if(Hat1LvName_tmp && Hat1LvName_tmp == Hat1LvName) {
                                            Hat1LvName_colspan ++;
                                            TDsCard[TDsCard.length-1].colspan = Hat1LvName_colspan;
                                        }
                                        else {
                                            Hat1LvName_colspan = 1;
                                            TDsCard.push({
                                                key: item.uid,
                                                name: Hat1LvName,
                                                colspan: 1
                                            })
                                        }
                                        Hat1LvName_tmp = Hat1LvName;
                                    }
                                    else {
                                        rowspan = 2;
                                    }
                                }

                                if(HatLv != 2 && HatLv != 1) {
                                    HeaderData.push({
                                        key: item.uid,
                                        IsFixedWidth: IsFixedWidth,
                                        MinWidth: MinWidth,
                                        IsHide: item.hide == 1,
                                        IsFixed: IsFixed,
                                        Sort: Sort,


                                        rowspan: rowspan,
                                        name: NameParam,
                                        uid: item.uid,

                                        data: item
                                    });
                                }

                                if(HatLv != 3) {
                                    FilterData.push({
                                        key: item.uid,
                                        name: NameParam,
                                        uid: item.uid,

                                        IsFixed: IsFixed,
                                        data: item,
                                        TypeEl: TypeEl,
                                        isArray: item.isArray,
                                        filter_value: "",
                                        filter_value_new: "",
                                        filter_sett_new: {},
                                    });
                                }
                            });
                            if(settings?.display_number_of_row !=0) {

                                HeaderData.unshift({
                                    "key": "row_number",
                                    IsFixed: false,
                                    IsFixedWidth: true,
                                    MinWidth: 50,
                                    IsHide: false,
                                    Sort: {isActive: false, dir: 0, disabled: true},

                                    rowspan: (settingsTab.HatAttr?2:1),
                                    name: "#",
                                    uid: "row_number",
                                    isArray: 0,
                                    data: {},
                                });
                                FilterData.unshift({
                                    key: "row_number",
                                    name: "#",
                                    uid: "row_number",
                                    IsFixed: false,
                                    data: {},
                                    isArray: 0,
                                    TypeEl: "row_number",
                                });
                            }

                            if(view_sorts.length == 0) {
                                view_sorts = [{uid: SortAttr, order: (dir_sort==1?"asc":"desc")}];
                            }


                            this.state.customData["block_calcs"] = block_calcs;
                            this.state.customData["exe_data_source_method"] = exe_data_source_method;
                            this.state.customData["HatID"] = HatID;
                            this.state.customData["view_sorts"] = view_sorts;
                            this.state.customData["attr_sort"] = SortAttr;
                            this.state.customData["sort"] = dir_sort;

                            if(params.isReload) {
                                FilterData = this.state.FilterData;
                            }


                            this.state.customData["SelectObjs"] = this.props.SelectObjs;
                            if (this.props.Sort) {
                                if (this.props.Sort.Attr) {
                                    this.state.customData["attr_sort"] = this.props.Sort.Attr;
                                }
                                if (this.props.Sort.Dir !== undefined || this.props.Sort.Dir !== null) {
                                    this.state.customData["sort"] = this.props.Sort.Dir;
                                }
                            }

                            //Смотрим есть ли у данного представления графики
                            let CurViewData=this.state.DataTabs.find((tab)=>{
                                return tab.View==ActiveTabUid?true:false;
                            });
                            if(CurViewData && CurViewData.settings && CurViewData.settings.charts && CurViewData.settings.charts.length){
                                isHasCharts=true;
                            }




                            //Если есть разворот
                            if(settingsTab.HatAttr) {
                                HeadDataTree.push(TDsCard);
                            }


                            this.setState({
                                customData: this.state.customData,
                                isLoadBody: true,
                                isLoadHead: true,
                                HeadData: HeaderData,
                                HeadDataTree: HeadDataTree,
                                FilterData: FilterData,
                                ActiveTabUid: ActiveTabUid,
                                SelectTr: null,
                                MainClass: MainClassData,
                                isHasCharts:isHasCharts,

                                isDisplayChart: isHasCharts
                            })
                        }
                    }
                })
                .catch((er)=>{
                    this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + er);
                    this.setState({
                        isLoadHead: false,
                        isLoadBody: false,
                        BodyData: [],
                        HeadData: [],
                    })
                })
        },

        getActiveTabData: () => {
            let tab = null;
            for (let i = 0; i < this.state.DataTabs.length; i++) {
                if (this.state.DataTabs[i].View == this.state.ActiveTabUid) {
                    tab = this.state.DataTabs[i];
                    break;
                }
            }
            return tab;
        },

        getTableDataAjaxParams: (ajaxData?: IajaxData) => {
            let sort = Number(this.state.customData["sort"]);
            let attr_sort = this.state.customData["attr_sort"];
            let view_sorts = null;

            if(this.state.customData.view_sorts) {
                view_sorts = this.state.customData.view_sorts;
            }

            const params_ajax: v_api.ITableData = {
                start: ajaxData ? ajaxData.start : 0, //  — номер первой записи в блоке
                num: ajaxData ? ajaxData.length : 0, // — количество записей в блоке

                view: this.state.ActiveTabUid,
                ParentObj: this.props.ParentObj,
                attr_sort: attr_sort,
                sort: sort,
                ParentView: this.props.ParentView,
                SelectObjs: this.state.customData["SelectObjs"],
                block_calcs: this.state.customData["block_calcs"],
                exe_data_source_method: this.state.customData["exe_data_source_method"],
                HatID: this.state.customData["HatID"],

                isNewMode: Number(this.state.customData.isNewMode == true),

                attr_filter: this.state.customData["attr_filter"] ? this.state.customData["attr_filter"] : null,
                view_sorts: view_sorts,

                header_data: this.state.HeadData,
            };
            return params_ajax;
        },

        getTableDataCallback: (ajaxData: IajaxData) => {
            const params_ajax: v_api.ITableData = this.core.getTableDataAjaxParams(ajaxData);

            v_api.getTableData(params_ajax, ajaxData.xhr)
                .then((res)=>{
                    if(res.data.result == 1) {
                        let BodyData = [];
                        let start_key_tr = params_ajax.start == 0 ? 1 : params_ajax.start;

                        for( let key_tr in res.data.data) {
                            let item_tr = res.data.data[key_tr];

                            let tr = {
                                trKey: Number(start_key_tr)+Number(key_tr) - 1,
                                tds: [],
                            };

                            this.state.HeadData.forEach((item_th, key_th) => {
                                let item_td = item_tr[item_th.key];
                                let key_td = item_th.key;

                                let td = {
                                    is_merge: 0,
                                    key: key_td,
                                    name: key_td,
                                };
                                if(item_td) {
                                    td["o"] = item_td.o;
                                    td["v"] = item_td.v;
                                    td["is_merge"] = item_td.is_merge;
                                    td["is_disabled"] = item_td.is_disabled;

                                    td["ln"] = item_td.ln ? item_td.ln : "";

                                    if(item_td.list) {
                                        td["list"] = item_td.list;
                                    }

                                    td["css"] = {
                                        background: item_td.background ? item_td.background : null,
                                        color: item_td.color ? item_td.color : null,
                                    };

                                    if(item_td.is_disabled) {
                                        td["css"]["background"] = '#f1f1f1';
                                    }
                                }
                                else {
                                    td["key"] = key_td;
                                    td["name"] = key_td;
                                }

                                tr.tds.push(td);
                            });

                            BodyData.push(tr);
                        }

                        ajaxData.success({
                            getOnlyTotal: 0,
                            isLoadTotal: res.data.isLoadTotal,
                            allCount: res.data.allCount,
                            data: BodyData,
                            result: 1,
                        });
                    }
                    else {
                        this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + res.data.result);
                        ajaxData.error({result: 0});
                    }
                })
                .catch((error)=>{
                    if(axios.isCancel(error)) {
                        console.error("Отмена");
                        //ручная отмена
                    }
                    else {
                        this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + error);
                        ajaxData.error({result: 0});
                    }
                });
        },
        getTableDataCallback_tfooter: (ajaxData: IajaxData) => {
            const params_ajax: v_api.ITableData = this.core.getTableDataAjaxParams(ajaxData);
            v_api.getFooterTableData(params_ajax, ajaxData.xhr)
                .then((res)=>{
                    if(res.data.result == 1) {
                        let data_ans = res.data.data;
                        let BodyData = [];
                        data_ans.forEach((item_tr, index_tr) => {
                            let tr = {
                                trKey: index_tr + "_key_sum",
                                tds: [],
                                noneTR: false,
                            };

                            let i_td = 0;
                            this.state.HeadData.forEach((item_th) => {
                                let key_td = item_th.key;
                                let td = {
                                    v: "",
                                    is_merge: 0,
                                    key: key_td,
                                    name: key_td,
                                };
                                if(item_th.IsHide) {
                                    td.v = "";
                                }
                                else {
                                    //Записываем наше значение в v
                                    if(item_tr[i_td] != undefined) {
                                        td.v = item_tr[i_td].v;
                                    }
                                    i_td ++;
                                }

                                tr.tds.push(td);
                            });

                            BodyData.push(tr);
                        });

                        ajaxData.success({
                            data: BodyData,
                            result: 1,
                        });
                    }
                    else {
                        this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + res.data.result);
                        ajaxData.error({result: 0});
                    }
                })
                .catch((error)=>{
                    if(axios.isCancel(error)) {
                        console.error("Отмена");
                        //ручная отмена
                    }
                    else {
                        this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + error);
                        ajaxData.error({result: 0});
                    }
                });
        },



        getParrentViews: (param:{view:string}, SuccFunc, ErrFunc) => {
            v_api.getParrentViews(param)
                .then((res)=>{
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc(res.data);
                    }
                })
                .catch((er)=>{
                    ErrFunc({result:0});
                })
        },

        reload: (isReloadHead) => {
            if(isReloadHead) {
                //Если таблица в новом режиме, то обновляем шапку при обновлении таблицы
                this.setState({ActiveTabUid: this.state.ActiveTabUid, isLoadHead: false});
                this.core.getHeadData({
                    uid: this.state.ActiveTabUid,
                    isReload: true
                });
            }
            else {
                this.setState({ReloadTrig: () => {this.setState({ReloadTrig: null})}});
            }
        },
        resizeTable: () => {
            this.setState({RecalcStyleFunc: () => {
                    this.setState({RecalcStyleFunc: null});
                }})
        },



        Forms: {
            GenerateWindowDownView: (
                params:{
                    view: string,
                    name: string,
                    type: string,
                    tds_form: any[],
                }
            ) =>{
                let DataCloth:IViewWindowDownViewProps = {
                    AppUid: params.view,
                    ActiveTabUid: params.view,
                    ParentView: this.state.ActiveTabUid,
                    IsZaglushka: true,

                    name: params.name,
                    tds_form: params.tds_form,
                };
                let ClothJSX = <ViewWindowDownView {...DataCloth} />;
                let uidWindow = "|v|"+params.view;
                let ModalConf:IModalConfig = {
                    id: uidWindow,
                    onClose: () => {
                        delete this.state.Forms[uidWindow];
                        this.props.RootStore.CloseForm(uidWindow);
                    },
                    content: {
                        ClassName: "down-table",
                        header: {
                            content: {
                                title: params.name,
                                buttons: [ {type: "close"} ],
                            }
                        },
                        body: {
                            contentJSX: ClothJSX,
                        },
                        footer: {
                            content: {
                                buttons: [  {type: "custom", content: "DropDataTable Click"}, {type: "close"} ],
                            }
                        }
                    }
                };
                if(this.state.Forms[uidWindow] == undefined) {
                    this.state.Forms[uidWindow] = {...ModalConf};
                    this.props.RootStore.OpenForm(uidWindow, ModalConf);
                }
                else {
                }
            },
            GenerateWindowFormView: (
                params:{
                    typeModal: "edit"|"add"|"read",
                    AppUid: string,
                    ActiveTabUid: string,
                    ParentView: string,
                    name: string,
                    tds_form: any[],
                    selectData?: {
                        ObjID?: string,
                        TypeID?: string,
                        ParrentObjID?: string,
                        ParrentTypeID?: string,
                        Name?: string,
                    },

                    isAllowEdit: boolean,
                }
            ) => {
                let uidWindow = "*"+params.selectData.ObjID;

                let isAllowEdit = params.isAllowEdit;

                /*
                //TODO::ZAGL PERMISSION
                if(document.location.host == "arctic.ozoneprogram.ru" && this.props.RootStore.MSettings.UserName != "Administrator") {
                    isAllowEdit = false;
                }
                */

                if(params.typeModal == "add") {
                    if(this.state.FSettings?.[params.ActiveTabUid]?.[params.selectData.TypeID]?.["Add"] == 1) {
                        isAllowEdit = true;
                    }
                    else {
                        ToastrStore.info('You don\'t have permission!');
                        return;
                    }
                }
                else {
                    if(this.state.FSettings?.[params.ActiveTabUid]?.[params.selectData.TypeID]?.["EditDel"] == 1 && isAllowEdit) {
                        isAllowEdit = true;
                    }
                    else if(this.state.FSettings?.[params.ActiveTabUid]?.[params.selectData.TypeID]?.["Read"] == 1) {
                        isAllowEdit = false;
                    }
                    else {
                        ToastrStore.info('You don\'t have permission!');
                        return;
                    }
                }

                //Если нет прав то принудительно режим редактирования
                if(params.typeModal != "read" && !isAllowEdit || this.isReadMode) {
                    params.typeModal = "read";
                }


                let DataCloth:IViewWindowFormViewProps = {
                    onClose: () => {
                        delete this.state.Forms[uidWindow];
                        this.props.RootStore.CloseForm(uidWindow);
                    },
                    savedObject: () => {
                        this.core.reload(this.core.getActiveTabData().settings.HatAttr);
                    },
                    addedObject: () => {
                        this.core.reload(this.core.getActiveTabData().settings.HatAttr);
                    },
                    onChangeTypeModal: (newType: "add" | "edit" | "read") => {
                        this.isReadMode = newType == "read";
                        params.typeModal = newType;
                        this.core.Forms.GenerateWindowFormView(params);
                    },
                    typeModal: params.typeModal,
                    isAllowEdit: isAllowEdit,
                    AppUid: params.AppUid,
                    ActiveTabUid: params.ActiveTabUid,
                    ParentView: params.ParentView,
                    name: params.name,
                    tds_form: params.tds_form,
                    selectData: {
                        ObjID: params.selectData.ObjID,
                        TypeID: params.selectData.TypeID,
                        ParrentObjID: params.selectData.ParrentObjID,
                        ParrentTypeID: params.selectData.ParrentTypeID,
                        Name: params.selectData.Name,
                    }
                };
                let ModalConf:IModalConfig = {
                    id: uidWindow,
                    onClickCloth: () => {},
                    content: {
                        bodyJSX: <ViewWindowFormView {...DataCloth} />,
                    }
                };

                if(this.state.Forms[uidWindow] == undefined) {
                    this.state.Forms[uidWindow] = {...ModalConf};
                    this.props.RootStore.OpenForm(uidWindow, ModalConf);
                }
                else {
                    this.props.RootStore.OpenForm(uidWindow, ModalConf);
                }
            }
        },
        ContextMenu: {
            CM_td_generate: (e, tds_form) => {
                let target_e = e.target;
                this.setState({
                    contentCM: [{
                        "label": 'Loading...',
                        "action": ()=> {
                        },
                        "icon_className": 'fas fa-spinner',
                        "children": false
                    }],
                    targetCM: target_e,
                });

                this.core.getParrentViews({view: this.state.ActiveTabUid}, (answer) => {
                    this.core.ContextMenu.GenerateCM(target_e, tds_form, answer.data);
                }, (data) => {
                    this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
                });
            },

            GenerateCM: (target, tds_form, answer_data) => {
                let contentCM = [];
                let targetCM = target;

                if(answer_data.length == 0) {
                    this.props.ToastrStore.clear();
                    this.props.ToastrStore.warning('Warning!', 'No views');
                    targetCM = null;
                }
                else {
                    let callbackCloseCM = () => {
                        this.setState({targetCM: null});
                    };

                    answer_data.forEach((item_v, key_v) => {
                        contentCM.push({
                            "label": item_v.name,
                            "action": ()=> {
                                this.core.Forms.GenerateWindowDownView({
                                    view: item_v.uid,
                                    name: item_v.name,
                                    type: item_v.type,
                                    tds_form: tds_form,
                                });
                                callbackCloseCM();
                            },
                            "icon_className": 'fas fa-tablet-alt',
                            "children": false
                        });
                    });
                }
                this.setState({
                    targetCM: targetCM,
                    contentCM: contentCM,
                })
            }
        },

        Filters: {
            GetCriteria: () => {
                let attr_filter = [];
                if(this.state.FilterGlobal) {
                    attr_filter.push({
                        "op": "~",
                        "v": this.state.FilterGlobal,
                    })
                }

                this.state.FilterData.forEach((th_f,key_f) => {
                    if(typeof th_f.filter_value_new == "object") {
                        let op = "IN";
                        if(th_f.isArray) {
                            if(th_f.filter_sett_new?.op == "ALL") {
                                op = "ALL";
                            }
                            else {
                                op = "ANY";
                            }
                        }

                        if(th_f.TypeEl == EnumCoolMarketEl.objInput || th_f.isArray) {
                            let values_obj = [];
                            for( let key_fi in th_f.filter_value_new) {
                                let item_fi = th_f.filter_value_new[key_fi];
                                let value_fi = item_fi.value;
                                if(th_f.TypeEl == EnumCoolMarketEl.datetime) {
                                    value_fi = parse(
                                        value_fi,
                                        'dd/MM/yyyy HH:mm:ss',
                                        new Date()
                                    );
                                    value_fi = format(value_fi, 'yyyy/MM/dd HH:mm:ss');
                                }

                                values_obj.push(value_fi);
                            }
                            if(values_obj.length > 0) {
                                attr_filter.push({
                                    "op": op,
                                    "v": values_obj,
                                    "attr": th_f.data.uid,
                                });
                            }
                        }
                        else if(th_f.TypeEl == EnumCoolMarketEl.datetime) {
                            for( let key_fi in th_f.filter_value_new) {
                                let item_fi = th_f.filter_value_new[key_fi];
                                if(item_fi.value1) {
                                    let value1 = item_fi.value1;
                                    value1 = parse(
                                        value1,
                                        'dd/MM/yyyy HH:mm:ss',
                                        new Date()
                                    );
                                    let fvalue1 = format(value1, 'yyyy/MM/dd HH:mm:ss');

                                    attr_filter.push({
                                        "op": ">=",
                                        "v": fvalue1,
                                        "attr": th_f.data.uid,
                                    });
                                }
                                if(item_fi.value2) {
                                    let value2 = item_fi.value2;
                                    value2 = parse(
                                        value2,
                                        'dd/MM/yyyy HH:mm:ss',
                                        new Date()
                                    );
                                    let fvalue2 = format(value2, 'yyyy/MM/dd HH:mm:ss');

                                    attr_filter.push({
                                        "op": "<=",
                                        "v": fvalue2,
                                        "attr": th_f.data.uid,
                                    });
                                }
                            }
                        }
                        else if(th_f.TypeEl == EnumCoolMarketEl.numberInput) {
                            for( let key_fi in th_f.filter_value_new) {
                                let item_fi = th_f.filter_value_new[key_fi];
                                if(item_fi.value1) {
                                    let value1 = item_fi.value1;
                                    attr_filter.push({
                                        "op": ">=",
                                        "v": value1,
                                        "attr": th_f.data.uid,
                                    });
                                }
                                if(item_fi.value2) {
                                    let value2 = item_fi.value2;
                                    attr_filter.push({
                                        "op": "<=",
                                        "v": value2,
                                        "attr": th_f.data.uid,
                                    });
                                }
                            }
                        }
                        else {
                            for( let key_fi in th_f.filter_value_new) {
                                let item_fi = th_f.filter_value_new[key_fi];
                                attr_filter.push({
                                    "op": op,
                                    "v": item_fi.v,
                                    "attr": th_f.data.uid,
                                })
                            }
                        }
                    }
                    else if(th_f.filter_value_new ) {
                        let op = "~";

                        if(th_f.isArray) {
                            if(th_f.filter_sett_new?.op == "ALL") {
                                op = "ALL";
                            }
                            else {
                                op = "ANY";
                            }
                        }

                        attr_filter.push({
                            "op": op,
                            "v": th_f.filter_value_new,
                            "attr": th_f.data.uid,
                        })
                    }
                });

                return attr_filter;
            },
            onSubmit: (e) => {
                this.state.customData.attr_filter = this.core.Filters.GetCriteria();
                this.core.reload(this.core.getActiveTabData().settings.HatAttr);
            },
            onReset: (e) => {
                this.state.FilterData.forEach( (item, key) => {
                    item.filter_value_new = "";
                    item.filter_sett_new = {};
                });
                this.setState({
                    FilterGlobal: "",
                });
                this.core.Filters.GenerateMarketFilterData();

                if(this.state.customData.attr_filter && this.state.customData.attr_filter.length > 0) {
                    this.state.customData.attr_filter = [];
                    this.core.reload(this.core.getActiveTabData().settings.HatAttr);
                }
            },

            onChange: (e, dataTH_f) => {
                if(dataTH_f.TypeEl == EnumCoolMarketEl.stringInput) {
                    this.core.Filters.onChangeTextFilter(e, dataTH_f);
                }
                else if(dataTH_f.TypeEl == EnumCoolMarketEl.numberInput) {
                    console.log('numberCh')
                    this.core.Filters.onChangeNumberFilter(e, dataTH_f);
                }
                else if(dataTH_f.TypeEl == EnumCoolMarketEl.datetime) {
                    this.core.Filters.onChangeDateTimeFilter(e, dataTH_f);
                }
                else if(dataTH_f.TypeEl == EnumCoolMarketEl.objInput) {
                    this.core.Filters.onChangeObjectFilter(e, dataTH_f);
                }
            },
            onChangeTextFilter: (data, dataTH_f) => {
                if(data?.filter_sett_new) {
                    dataTH_f.filter_sett_new = data.filter_sett_new;
                }
                else {
                    if(dataTH_f.isArray) {
                        dataTH_f.filter_value_new = data;
                    }
                    else {
                        if(data?.target) {
                            let value = data.target.value;
                            dataTH_f.filter_value_new = value;
                        }
                    }
                }
                //filter_sett_new

                //dataTH_f мы изменили по ссыке, но состояние не опр

                this.setState({});
                this.core.Filters.GenerateMarketFilterData();
            },
            onChangeNumberFilter: (data, dataTH_f) => {
                if(data?.filter_sett_new) {
                    dataTH_f.filter_sett_new = data.filter_sett_new;
                }
                else {
                    dataTH_f.filter_value_new = data;
                }
                //filter_sett_new

                //dataTH_f мы изменили по ссыке, но состояние не опр

                this.setState({});
                this.core.Filters.GenerateMarketFilterData();
            },

            onChangeDateTimeFilter: (data, dataTH_f) => {
                if(data?.filter_sett_new) {
                    dataTH_f.filter_sett_new = data.filter_sett_new;
                }
                else {
                    dataTH_f.filter_value_new = data;
                }

                //dataTH_f мы изменили по ссыке, но состояние не опр

                this.setState({});
                this.core.Filters.GenerateMarketFilterData();
            },
            onChangeObjectFilter: (data, dataTH_f) => {
                if(data?.filter_sett_new) {
                    dataTH_f.filter_sett_new = data.filter_sett_new;
                }
                else {
                    dataTH_f.filter_value_new = data;
                }
                this.setState({});
                this.core.Filters.GenerateMarketFilterData();
            },

            RenderTHFilter: (dataTH_f) => {
                let ElementFilter;

                let btns_arr = [];
                if(dataTH_f.isArray) {
                    let isActiveBtn = dataTH_f.filter_sett_new?.op == "ALL" ? "ALL" : "ANY";
                    btns_arr.push(
                        <button key={'btn_any'} className={"nf_btn-ellipse vtc_btn_arr_table " + (isActiveBtn == "ANY" ? "active" : "")}
                                onClick={() => {
                                    dataTH_f.filter_sett_new.op = "ANY";
                                    dataTH_f.onChange({filter_sett_new: dataTH_f.filter_sett_new}, dataTH_f);
                                }}
                        >Any</button>
                    );
                    btns_arr.push(
                        <button key={'btn_all'} className={"nf_btn-ellipse vtc_btn_arr_table " + (isActiveBtn == "ALL" ? "active" : "")}
                                onClick={() => {
                                    dataTH_f.filter_sett_new.op = "ALL";
                                    dataTH_f.onChange({filter_sett_new: dataTH_f.filter_sett_new}, dataTH_f);
                                }}
                        >All</button>
                    );
                }

                if(dataTH_f.TypeEl == EnumCoolMarketEl.stringInput) {
                    if(!dataTH_f.isArray) {
                        ElementFilter = <div className={"vtc_co_input_filter"}>
                            <input className={"vtc_input_filter"}
                                   value={dataTH_f.filter_value_new}
                                   onChange={(e) => {
                                       this.core.Filters.onChangeTextFilter(e, dataTH_f);
                                   }}
                                   onKeyDown={(e)=>{
                                       if (e.key === 'Enter') {
                                           this.core.Filters.onSubmit(e);
                                       }
                                   }}
                                   type={"text"} disabled={false} />
                            {btns_arr}
                        </div>
                    }
                    else {
                        ElementFilter = <div className={"vtc_co_input_filter"}>
                            {this.core.Filters.build.tagsInputControl(dataTH_f, dataTH_f.onChange)}
                            {btns_arr}
                        </div>
                    }
                }
                else if(dataTH_f.TypeEl == EnumCoolMarketEl.numberInput) {}
                else if(dataTH_f.TypeEl == EnumCoolMarketEl.objInput) {
                    dataTH_f.ObjCallback = (data:{filter_value, SuccFunc, ErrFunc}) => {
                        this.core.AJAX.GetSelectData(dataTH_f.data.LinkedClass.uid, data.filter_value,
                            (res) => {
                                if(data.SuccFunc) {
                                    let re_arr = [];
                                    let res1 = res?.data?.data?.objs ? res.data.data.objs : [];
                                    for(let key1 in res1) {
                                        let item1 = res1[key1];
                                        if(item1.name) {
                                            re_arr.push({
                                                value: item1.name,
                                                label: item1.name,
                                                name: item1.name,
                                                uid: item1.uid,
                                            });
                                        }
                                    }
                                    data.SuccFunc(re_arr);
                                }
                            },
                            () => {
                                data.ErrFunc()
                            });
                    };

                    ElementFilter = <div className={"vtc_co_input_filter"}>
                        {this.core.Filters.build.selectControl(dataTH_f, (new_value) => {this.core.Filters.onChange(new_value, dataTH_f);})}
                        {btns_arr}
                    </div>
                }
                else if(dataTH_f.TypeEl == EnumCoolMarketEl.datetime) {
                    if(!dataTH_f.isArray) {
                        ElementFilter = <div className={"vtc_co_input_filter"}>
                            {this.core.Filters.build.dateTimeControl(dataTH_f, (new_value) => {this.core.Filters.onChange(new_value, dataTH_f);})}
                        </div>
                    }
                    else {
                        ElementFilter = <div className={"vtc_co_input_filter"}>
                            {this.core.Filters.build.dateTimeControlArray(dataTH_f, (new_value) => {this.core.Filters.onChange(new_value, dataTH_f);})}
                            {btns_arr}
                        </div>
                    }
                }
                else {
                    ElementFilter = <div className={"vtc_co_input_filter"}>
                        <input className={"vtc_input_filter"} type={"text"} disabled={true} />
                        {btns_arr}
                    </div>
                }


                return ElementFilter;
            },
            build: {
                selectControl: (item, onChange) => {
                    const promiseOptions = inputValue =>
                        new Promise(resolve => {
                            if(item.ObjCallback) {
                                item.ObjCallback({filter_value: inputValue, SuccFunc: (data) => {
                                            console.log(data);
                                            resolve(data);

                                        }, ErrFunc: () => {
                                            resolve([]);
                                        }
                                    }
                                );
                            }
                        });
                    //defaultOptions
                    let _options = item.filter_value_new ? item.filter_value_new : [];

                    return <div className={"vtc_co_select_obj"}>
                        <AsyncSelect
                            isMulti
                            cacheOptions
                            defaultOptions
                            options={_options}
                            loadOptions={promiseOptions}

                            menuPortalTarget={document.querySelector("body")}

                            onChange={onChange}

                            className="abc"
                            classNamePrefix="react-select"

                            value={_options}
                            setValue={(ValueType, ActionTypes) => {}}
                            onKeyDown={(e)=>{
                                if (e.key === 'Enter') {
                                    this.core.Filters.onSubmit(e);
                                }
                            }}
                        />
                    </div>;
                },
                tagsInputControl: (item, onChange) => {

                    //suggestions={suggestions}
                    let _options = item.filter_value_new ? item.filter_value_new : [];
                    let _tags = _options.map( (item) => { return {text: item.value, id: item.value}}) ;


                    const KeyCodes = {
                        comma: 188,
                        enter: 13,
                    };
                    const delimiters = [KeyCodes.comma, KeyCodes.enter];

                    return <div className={"vtc_co_tags_arr"}>
                        <ReactTags
                            tags={_tags}
                            allowDragDrop={false}
                            handleDelete={(i) => {
                                let new_value = _options.filter((tag, index) => index !== i);
                                onChange(new_value, item);
                            }}
                            handleAddition={ (tag) => {
                                let new_value = [..._options, {value:tag.id}];
                                onChange(new_value, item);
                            }}
                            delimiters={delimiters}
                            inline={true}
                            placeholder={""}
                        />
                    </div>
                },

                dateTimeControl: (item, onChange) => {
                    let dates = [];
                    let data_item: any = {value1: "", value2: ""};

                    if (typeof item.filter_value_new == "object") {
                        if(item.filter_value_new[0] != undefined) {
                            data_item.value1 = item.filter_value_new[0].value1;
                            data_item.value2 = item.filter_value_new[0].value2;
                        }
                    }
                    else {
                        data_item.value1 = "";
                        data_item.value2 = "";
                    }

                    let value1 = data_item.value1 ? data_item.value1 : "";
                    let value2 = data_item.value2 ? data_item.value2 : "";

                    if(value1) {
                        value1 = parse(
                            value1,
                            'dd/MM/yyyy HH:mm:ss',
                            new Date()
                        );
                    }
                    if(value2) {
                        value2 = parse(
                            value2,
                            'dd/MM/yyyy HH:mm:ss',
                            new Date()
                        );
                    }

                    return <div style={{display:"flex"}}>
                        <DatePicker key={1}
                                    className={"vtc_input_filter vtc_input_text"}
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    dateFormat="dd/MM/yyyy HH:mm"
                                    isClearable
                                    placeholderText={"From"}
                                    showTimeInput
                                    timeInputLabel="Time:"
                                    selected={value1}
                                    title={value1}
                                    onChange={(date) => {
                                        if(date) {
                                            date = format(date, 'dd/MM/yyyy HH:mm:ss')
                                        }
                                        onChange([{value1: date, value2: data_item.value2}]);
                                    }}
                        />
                        <DatePicker key={2}
                                    className={"vtc_input_filter vtc_input_text "}
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    dateFormat="dd/MM/yyyy HH:mm"
                                    isClearable
                                    placeholderText={"To"}
                                    showTimeInput
                                    timeInputLabel="Time:"
                                    selected={value2}
                                    title={value2}
                                    onChange={(date) => {
                                        if(date) {
                                            date = format(date, 'dd/MM/yyyy HH:mm:ss')
                                        }
                                        onChange([{value1: data_item.value1, value2: date}]);
                                    }}
                        />
                    </div>;
                },
                dateTimeControlArray: (item, onChange) => {
                    const dateTime_input = (value, isDisabled, key, onChange) => {
                        return <DatePicker key={key}
                                           className={"date_control"}
                                           timeFormat="HH:mm"
                                           timeIntervals={15}
                                           timeCaption="time"
                                           dateFormat="dd/MM/yyyy HH:mm"
                                           isClearable
                                           placeholderText={""}
                                           showTimeInput
                                           timeInputLabel="Time:"
                                           selected={value}
                                           title={value}
                                           onChange={(date) => {
                                               onChange(date);
                                           }}
                        />;
                    };

                    let _options = item.filter_value_new ? item.filter_value_new : [];
                    let _tags = _options.map( (item) => { return {text: item.value, id: item.value}});
                    const KeyCodes = {
                        comma: 188,
                        enter: 13,
                    };
                    const delimiters = [KeyCodes.comma, KeyCodes.enter];

                    let main_control = dateTime_input('', false, 'one_control', (date) => {
                        let t_value = date;
                        if(t_value) {
                            const index = _options.length === 0 ? 0 : _options.length;
                            if(t_value) {
                                t_value = format(t_value, 'dd/MM/yyyy HH:mm:ss')
                            }
                            _options[index] = { value: t_value };
                            onChange(_options, item);
                        }
                        else {
                            onChange([], item);
                        }
                    });

                    return <div className={"vtc_co_tags_datetime_arr"}>
                        <ReactTags
                            readOnly = {false}
                            tags={_tags}
                            allowDragDrop={false}
                            handleDelete={(i) => {
                                let new_value = _options.filter((tag, index) => index !== i);
                                onChange(new_value, item);
                            }}
                            delimiters={delimiters}
                            inline={true}
                            placeholder={""}
                        />
                        {main_control}
                    </div>;
                },
            },
            GenerateMarketFilterData: () => {
                //Создание спец. данных для фильтров
                let MarketElements:ICoolMarketEl[] = [];
                if(this.state.isLoadHead) {
                    this.state.FilterData.forEach( (item, key) => {
                        let m_elem = item;

                        if(item.TypeEl == EnumCoolMarketEl.stringInput) {
                            m_elem.onChange = (e) => { this.core.Filters.onChangeTextFilter(e, item); };
                        }
                        else if(item.TypeEl == EnumCoolMarketEl.numberInput) {
                            m_elem.onChange = (data) => { this.core.Filters.onChangeNumberFilter(data, item); };
                        }
                        else if(item.TypeEl == EnumCoolMarketEl.datetime) {
                            m_elem.onChange = (data) => { this.core.Filters.onChangeDateTimeFilter(data, item); };
                        }
                        else if(item.TypeEl == EnumCoolMarketEl.objInput) {
                            item.ObjCallback = (data:{filter_value, SuccFunc, ErrFunc}) => {
                                this.core.AJAX.GetSelectData(item.data.LinkedClass.uid, data.filter_value,
                                    (res) => {
                                        if(data.SuccFunc) {
                                            let re_arr = [];
                                            let res1 = res?.data?.data?.objs ? res.data.data.objs : [];
                                            for(let key1 in res1) {
                                                let item1 = res1[key1];
                                                if(item1.name) {
                                                    re_arr.push({
                                                        value: item1.name,
                                                        label: item1.name,
                                                        name: item1.name,
                                                        uid: item1.uid,
                                                    });
                                                }
                                            }
                                            data.SuccFunc(re_arr);
                                        }
                                    },
                                    () => {
                                        data.ErrFunc()
                                    });
                            };
                            m_elem.onChange = (data) => { this.core.Filters.onChangeObjectFilter(data, item); };
                        }

                        MarketElements.push(m_elem);
                    });
                }

                if(this.props.ViewStore.ActiveClothID == this.props.id) {
                    this.props.ViewStore.isLoadMarketFilter = this.state.isLoadHead;
                    this.props.ViewStore.DataMarketFilter = {
                        TableID: this.props.id,
                        Elements: MarketElements,
                        onClickResetBtn: this.core.Filters.onReset,
                        onClickSubmitBtn: this.core.Filters.onSubmit,
                    }
                }
            },
        },

        Editable: {
            //Сохранение ячейки бещ кэша
            AJAX: {
                saveValueCellTable:  (params:{obj:string, type:string, datatype:string, attr:string, value:string}, SuccFunc, ErrFunc) => {
                    v_api.saveValueCellTable(params)
                        .then((res)=>{
                            if(res.data.result == 1) {
                                SuccFunc();
                            }
                            else {
                                ErrFunc(res.data);
                            }
                        })
                        .catch(()=>{
                            ErrFunc({result: 0})
                        })
                },
            },
            SaveCellBefore: (new_select_cell) => {

                if(this.isSaveCellToken) {
                    this.isSaveCellToken = false;

                    const SuccFunc = () => {
                        //Сохраняем предыдущее значение в кэш, если есть
                        this.setState({select_cell: new_select_cell});
                        this.isSaveCellToken = true;
                    };


                    if(this.state.select_cell?.attrCache?.value != undefined) {
                        this.isSaveCellToken = false;
                        this.core.Editable.SaveCell(SuccFunc, () => { this.isSaveCellToken = true; })
                    }
                    else {
                        SuccFunc();
                    }
                }
            },
            SaveCell: (SuccFunc, ErrFunc) => {
                if(this.state.FSettings?.[this.state.ActiveTabUid]?.[this.state.select_cell.item_th.data.class.uid]?.["EditDel"] == 1) {
                    this.core.Editable.AJAX.saveValueCellTable({
                            obj: this.state.select_cell.item_td.o,
                            type: this.state.select_cell.item_th.data.class.uid,
                            datatype: this.state.select_cell.item_th.data.datatype,
                            attr:this.state.select_cell.item_th.data.uid,
                            value: this.state.select_cell.attrCache.value,
                        }, (answer) => {
                            let datatype = this.state.select_cell.item_th.data.datatype;
                            let index_tr = Number(this.state.select_cell.item_tr.trKey);
                            let index_td = this.state.select_cell.index_td;

                            if(this.state.select_cell.item_th.data?.attr?.dopSett.data_source != undefined) {
                                if(datatype == "number" || datatype == "text") {
                                    this.tableData[index_tr].tds[index_td].v = this.state.select_cell.attrCache.value;
                                }
                                else if(datatype == "object") {
                                    this.tableData[index_tr].tds[index_td].v = this.state.select_cell.attrCache.value;
                                    this.tableData[index_tr].tds[index_td].ln = this.state.select_cell.attrCache.label;
                                }
                            }
                            else if(datatype == "number" || datatype == "text") {
                                this.tableData[index_tr].tds[index_td].v = this.state.select_cell.attrCache.value;
                            }
                            else if(datatype == "object") {
                                this.tableData[index_tr].tds[index_td].v = this.state.select_cell.attrCache.value;
                                this.tableData[index_tr].tds[index_td].ln = this.state.select_cell.attrCache.label;
                            }

                            this.state.select_cell.attrCache = null;

                            this.props.ToastrStore.clear();
                            this.props.ToastrStore.success("Success!");

                            if(SuccFunc) {
                                SuccFunc();
                            }
                        },
                        (answer) => {
                            if(ErrFunc) {
                                ErrFunc();
                            }
                            this.props.ToastrStore.clear();
                            this.props.ToastrStore.error("Server error!");
                        });
                }
                else {
                    ToastrStore.info('You don\'t have permission!');
                    if(SuccFunc) {
                        SuccFunc();
                    }
                    return;
                }
            },

            //Обновление ячейки без сохранения в БД (например подгрузки всей ячейки)
            SaveCellWithoutCache: (newValue, SuccFunc) => {

                let datatype = this.state.select_cell.item_th.data.datatype;
                let index_tr = Number(this.state.select_cell.item_tr.trKey);
                let index_td = this.state.select_cell.index_td;

                if(datatype == "number" || datatype == "text") {
                    this.tableData[index_tr].tds[index_td].v = newValue;
                }
                else if(datatype == "object") {
                    this.tableData[index_tr].tds[index_td].v = newValue.v;
                    this.tableData[index_tr].tds[index_td].ln = newValue.ln;
                }

                SuccFunc();

            }
        },


        RenderHZTabs: () => {
            let DataTabs = [];
            this.state.DataTabs.forEach( (item, key) => {
                let classActive = "";
                if(this.state.ActiveTabUid == item.View) {
                    classActive = "active"
                }
                DataTabs.push({
                    className: classActive,
                    Name: item.Name,
                    key: item.View,
                    onClick: (e) => { this.events.onClickTab(e, item); },
                    onContextMenu: (e) => {
                        console.error(e,'cclick');
                        e.preventDefault();
                        return false;
                    }
                });
            });

            DataTabs.push({
                className: "",
                Name: <i className="fa fa-plus"/>,
                key: "add_user_tab",
                onClick: (e)=>{
                },
                onContextMenu: (e)=>{
                    console.error(e,'cclick')
                    e.preventDefault();
                }
            });


            //TODO::ZAGLUSHKA
            let isHideTabs = "";
            if(this.props.AppUid == "MainVirtual" && this.state.DataTabs.length == 1 || this.props.IsHideTabs) {
                isHideTabs = "hide-el";
            }

            const dataHZTabs = {
                callbackRecalc: () => {},
                className: "pp_tollbar_tabs " + isHideTabs,
                data: DataTabs
            };

            return (
                <HZTabs {...dataHZTabs} />
            );
        },
        RenderHZTabsTools: () => {
            /*    let toolsJSX = [

                <button title="Edit" key={"btn_edit"}
                        className={"pp_c pp_c_btn "}
                        onClick={(e)=>{this.events.onClickEditBtn(e);}}
                >
                    <i className="fa fa-save"/>Edit
                </button>,
                <button title="Remove" key={"btn_remove"}
                        onClick={(e)=>{this.events.onClickDelBtn(e);}}
                        className={"pp_c pp_c_btn"}>
                    <i className="fas fa-times"/>Remove
                </button>,
            ];
            */
            let toolsJSX = [];

            if(this.state.MainClass?.uid &&  this.state.FSettings?.[this.state.ActiveTabUid]?.[this.state.MainClass?.uid]?.["Add"] == 1) {
                toolsJSX.push(
                    <button title="Add" type={"button"} key={"btn_add-"+this.state.MainClass.uid}
                            disabled={false}
                            onClick={(e)=>{

                                let ParentObj = this.props.ParentObj ? this.props.ParentObj : "00000000-0000-0000-0000-000000000000";

                                this.events.onClickAddBtn(e, {
                                ParentObj: ParentObj,
                                Name: this.state.MainClass.name,
                                TypeID: this.state.MainClass.uid,
                                ParrentTypeID: "00000000-0000-0000-0000-000000000000",
                            });}}
                            className={"pp_c pp_c_btn "}>
                        <i className="fa fa-plus"/>Add ({this.state.MainClass.name})
                    </button>
                )
            }
            if(this.state.SelectTr && !this.state.isEditMode) {
                //Проходим по всем ID-объектам и находим все типы родители для добавления

                let Types = {};


                const find_parrent = (uid) => {
                    //ищем объект родитель
                    for( let key_h in this.state.HeadData) {
                        let item_h = this.state.HeadData[key_h];
                        if (item_h.data?.class?.uid == uid) {

                            let f_item = this.state.SelectTr.tds[key_h];

                            if(f_item.o) {
                                return {
                                    o: f_item.o,
                                }
                            }
                            else {
                                return null;
                            }
                        }
                    }

                    return  null;
                };


                let is_show_remove_btn = false;

                for( let key_h in this.state.HeadData) {
                    let item_h = this.state.HeadData[key_h];
                    if (item_h.data?.class) {
                        if (Types[item_h.data.class.uid] == undefined) {
                            Types[item_h.data.class.uid] = true;

                            if(item_h.data.class.parentUID != "00000000-0000-0000-0000-000000000000") {
                                let parrent_item = find_parrent(item_h.data.class.parentUID);

                                if(parrent_item) {
                                    if(this.state.FSettings?.[this.state.ActiveTabUid]?.[item_h.data.class?.uid]?.["Add"] == 1) {
                                        toolsJSX.push(
                                            <button title="Add" type={"button"} key={"btn_add-"+item_h.data.class.uid}
                                                    disabled={false}
                                                    onClick={(e)=>{this.events.onClickAddBtn(e, {
                                                        ParentObj: parrent_item.o,
                                                        Name: item_h.data.class.name,
                                                        TypeID: item_h.data.class.uid,
                                                        ParrentTypeID: item_h.data.class.parentUID,
                                                    });}}
                                                    className={"pp_c pp_c_btn "}>
                                                <i className="fa fa-plus"/>Add ({item_h.data.class.name})
                                            </button>
                                        )
                                    }
                                }
                            }
                        }

                        if(!is_show_remove_btn && this.state.FSettings?.[this.state.ActiveTabUid]?.[item_h.data.class.uid]?.["EditDel"] == 1) {
                            is_show_remove_btn = true;
                        }
                    }
                }

                if(is_show_remove_btn) {
                    toolsJSX.push(<button type={"button"} title="Delete" key={"btn_remove"}
                                          disabled={false}
                                          onClick={(e)=>{
                                              this.events.onClickRemoveBtn();
                                          }}
                                          className={"pp_c pp_c_btn "}>
                        <i className="fas fa-times"/>
                    </button>);
                }
            }


            toolsJSX.push(<Switch
                height={26}
                title="Множественный выбор"
                key={"sw_many_mode"}
                className={"pp_c_switch pp_c react-switch"}
                onChange={this.events.onChangeEditMode}
                checked={this.state.isEditMode}
                uncheckedIcon={<i style={{padding: "7px 9px", color: "white"}} className="fas fa-pencil-alt"/>}
                checkedIcon={<i style={{padding: "7px 9px", color: "white"}} className="fas fa-pencil-alt"/>}
            />);
            toolsJSX.push(<Switch
                height={26}
                title="Катина функция"
                key={"sw_many_mode2"}
                className={"pp_c_switch pp_c react-switch"}
                onChange={() => {
                    let mode = !this.state.isNewMode;

                    this.state.customData.isNewMode = mode;
                    this.setState({isNewMode: mode});

                    this.core.reload(true);
                }}
                checked={this.state.isNewMode}
                uncheckedIcon={<i style={{padding: "7px 9px", color: "white"}} className="fab fa-angellist"/>}
                checkedIcon={<i style={{padding: "7px 9px", color: "white"}} className="fab fa-angellist"/>}
            />);
            toolsJSX.push(<input title="Search" key={'pp_FilterGlobal'} disabled={false} className="pp_c_inp pp_c " type="text"
                                  value={this.state.FilterGlobal}
                                  onChange={(e)=>{
                                      let value = e.target.value;
                                      this.setState({FilterGlobal: value})
                                  }}
                                  onKeyDown={(e)=>{
                                      if (e.key === 'Enter') {
                                          this.core.Filters.onSubmit(e);
                                      }
                                  }}
            />);
            toolsJSX.push(<button type={"button"} title="Search" key={"btn_search"}
                                   disabled={false}
                                   onClick={(e)=>{
                                       this.core.Filters.onSubmit(e);
                                   }}
                                   className={"pp_c pp_c_btn "}>
                <i className="fa fa-search"/>
            </button>);
            toolsJSX.push(<button title="Clear filters" key={"btn_clear"} type={"button"}
                                  disabled={false}
                                  onClick={(e)=>{
                                      this.core.Filters.onReset(e);
                                  }}
                                  className={"pp_c pp_c_btn "}>
                <i className="fa fa-eraser"/>
            </button>);
            if(this.state.isHasCharts){
              toolsJSX.push(<button title="Display charts" key={"btn_chart"} type={"button"}
                                    disabled={false}
                                    onClick={(e)=>{
                                        this.setState({isDisplayChart:!this.state.isDisplayChart});
                                    }}
                                    className={"pp_c pp_c_btn "}>
                  <i className="fas fa-chart-area"></i>
              </button>);
            }

            /* Катя вместе с Антоном решили, что не нужны нам фильтры в таблице
            toolsJSX.push(<button title="Show/Hide column filters" key={"btn_show_hide_filters"} type={"button"}
                                  disabled={false}
                                  onClick={(e)=>{
                                      this.setState({
                                          isShowColumnFilters: !this.state.isShowColumnFilters,
                                          RecalcStyleFunc: () => {
                                              this.setState({RecalcStyleFunc: null});
                                          },
                                      })
                                  }}
                                  className={"pp_c pp_c_btn "}>
                <i className="fa fa-filter"/>
            </button>);
            */
            toolsJSX.push(
                <button title="Export view" key={"btn_export"} type={"button"}
                        disabled={false}
                        onClick={(e) => {
                            this.setState({
                                isRequestRunning: true,
                            });
                            const params_ajax: v_api.ITableData = this.core.getTableDataAjaxParams();
                            v_api.exportView(params_ajax)
                                .then((repsponse) => {
                                    this.setState({
                                        isRequestRunning: false,
                                    });
                                    const tab = this.core.getActiveTabData();
                                    download(repsponse.data, (tab ? tab.Name : 'export') + '.xlsx');
                                });

                        }}
                        className={"pp_c pp_c_btn"}>
                    <i className="fa fa-download"/>
                </button>
            );

            toolsJSX.push(
                <button title="Sort" key={"btn_sort"} type={"button"}
                        disabled={false}
                        onClick={(e) => {
                            this.state.BModal.show = true;
                            this.state.BModal.TypeModal = 'SortModal';

                            this.state.BModal.columnsSort = this.state.HeadData;
                            this.state.BModal.view_sorts = this.state.customData.view_sorts;
                            this.setState({})
                        }}
                        className={"pp_c pp_c_btn"}>
                    <span style={{height:"14px", width: "14px", display: "flex"}}>
                        <svg viewBox="0 0 34.761 26.892">
                            <path d="M15.359 9.478l-6.226-6.21v17.557H7.426V3.268L1.2 9.478 0 8.281 8.279 0l8.279 8.281z"/>
                            <path d="M34.761 18.612l-8.279 8.281-8.282-8.281 1.2-1.2 6.226 6.21V6.068h1.707v17.557l6.226-6.21z"/>
                        </svg>
                    </span>
                </button>
            );

            toolsJSX.push(
                <button title="Fixed" key={"btn_fix"} type={"button"}
                        disabled={false}
                        onClick={(e) => {
                            this.state.BModal.show = true;
                            this.state.BModal.TypeModal = 'FixModal';
                            this.state.BModal.HeadData = this.state.HeadData;
                            this.setState({})
                        }}
                        className={"pp_c pp_c_btn"}>
                    <i className="fas fa-columns"/>
                </button>
            );



            const dataHZTabs_tools = {
                callbackRecalc: () => {},
                className: "pp_tollbar_tools",
                dataJSX: toolsJSX
            };

            return (
                <HZTabs {...dataHZTabs_tools} />
            );
        },

        RenderTable: () => {
            let data_trs = [];

            if(this.state.isLoadBody) {
                data_trs = this.state.BodyData;
            }

            let filterData = null;
            if(this.state.isShowColumnFilters) {
                filterData = this.state.FilterData;
            }


            let footer_calback;

            if(this.state.customData.block_calcs) {
                footer_calback = this.core.getTableDataCallback_tfooter;
            }

            const DataForCoolTable = {
                settings: {
                    ajax: {},
                 //   data: data_trs,

                    ajax_callback: this.core.getTableDataCallback,
                    isMergeTable: true,


                    ajax_callback_tfooter: footer_calback,
                },


                filterData: filterData,

                headerDataTree: this.state.HeadDataTree,
                headerData: this.state.HeadData,

                callbackRenderTD: (dataTD, dataTR, dataTH) => {
                    let content_td;
                    if(dataTH && dataTH.data.datatype == "file" && dataTD.v && dataTD.o) {
                        content_td = [
                            <button key={1} style={{marginRight: '2px', border: 'none'}} type="button" title="download" className="file_btns"
                                    onClick={() => {window.open('/Object/downloadFile?obj='+dataTD.o+'&attr='+dataTH.data.uid, '_blank');}}>
                                <i className="fas fa-file-download"/>
                            </button>,
                            <span key={2}> {dataTD.v}</span>]
                    }
                    else if(dataTH && dataTH.data.datatype == "datetime" && dataTD.v && dataTD.o) {
                        content_td = dataTD.v.slice(0,10);
                    }
                    else if(dataTH && dataTH.data.datatype == "object" && dataTD.ln && dataTD.o) {
                        content_td = dataTD.ln;
                    }
                    else if(dataTH && dataTH.data.datatype == "text" && dataTH.data.attr.TextType == "1" && dataTD.v && dataTD.o) {
                        let valid_value = '';
                        valid_value = dataTD.v.replace(/<\/?[^>]+(>|$)/g, "");
                        content_td = valid_value;
                    }
                    else if(dataTH && dataTH.data.datatype == "text" && dataTH.data.attr.TextType == "2" && dataTD.v && dataTD.o) {
                        let valid_value = '#ffffff';
                        if(/^#[0-9A-F]{6}$/i.test(dataTD.v)) {
                            valid_value = dataTD.v;
                        }

                        content_td = [<div key={1} className={"kt_v_color_box"} style={{background: valid_value}} >&nbsp;</div>, <span key={2}> {dataTD.v}</span>]
                    }
                    else if(dataTH && dataTH.data.datatype == "text" && dataTH.data.attr.TextType == "3" && dataTD.v && dataTD.o) {
                        content_td = <a href={dataTD.v} target={"_blank"} >{dataTD.v}</a>;
                    }
                    else {
                        content_td = dataTD.v;
                    }

                    let td;
                    let className = "";

                    //.? sugar es2020
                    if(this.state.isEditMode && dataTH.key == this.state.select_cell?.item_td?.key && dataTR.trKey == this.state.select_cell?.item_tr?.trKey) {
                        className = "cool_t_select_cell ";
                        if(dataTD.o) {
                            if(this.state.select_cell.isEdit || dataTH.data.datatype == "object" || dataTH.data?.attr?.dopSett.data_source != undefined) {
                                className += "cool_t_editable_cell ";
                                let paramsControl = new d_helper.TableDatatypeClass({
                                    item_td: dataTD,
                                    item_th: dataTH,
                                    item_tr: dataTR,
                                    HeadData: this.state.HeadData,

                                    select_cell: this.state.select_cell,
                                    key: dataTD.key,
                                    type: "edit",
                                    onClickEInput: (e) => {this.events.onClickEInput(e);},
                                    ChangeStateFunc: () => { this.setState({});}, // this.setState();
                                    SaveCellWithoutCache: this.core.Editable.SaveCellWithoutCache,
                                });
                                td = paramsControl.GetControl();
                            }
                            else {
                                if(this.state.select_cell.item_th?.data?.datatype == "text" && dataTH.data.attr.TextType == "1") {
                                    let paramsControl = new d_helper.TableDatatypeClass({
                                        item_td: dataTD,
                                        item_th: dataTH,
                                        item_tr: dataTR,
                                        HeadData: this.state.HeadData,

                                        select_cell: this.state.select_cell,
                                        key: dataTD.key,
                                        type: "edit",
                                        onClickEInput: (e) => {this.events.onClickEInput(e);},
                                        ChangeStateFunc: () => { this.setState({});}, // this.setState();
                                        SaveCellWithoutCache: this.core.Editable.SaveCellWithoutCache,
                                    });
                                    td = paramsControl.GetControl();
                                }
                                else {
                                    td = <div className="cool_t_focus_cell_div">
                                        <input className={"cool_t_focus_cell"} autoFocus={true} value={content_td}
                                               onDoubleClick={e=>{this.events.onDoubleClickFocusInput(e, dataTR, dataTD)}}
                                               onChange={(e)=>{this.events.onChangeFocusInput(e)}}
                                        />
                                    </div>;
                                }
                            }
                        }
                        else {
                            td = <div className="cool_t_focus_cell_div">
                                <input className={"cool_t_focus_cell"} readOnly={true} autoFocus={true} value={content_td}
                                      // onDoubleClick={e=>{this.events.onDoubleClickFocusInput(e, dataTR, dataTD)}}
                                      // onChange={(e)=>{this.events.onChangeFocusInput(e)}}
                                />
                            </div>;
                        }
                    }
                    else {
                        td = <div className="cool_t_td_content"><div className="fxc-gc-text">{content_td}</div></div>;
                    }


                    return {contentJSX: td, className: className};
                },
                callbackRenderTH: (dataTH) => {
                    return dataTH.name;
                },
                onClickTH: this.events.onClickTH,

                callbackRenderFilterTH: this.core.Filters.RenderTHFilter,

                ReloadTrig: this.state.ReloadTrig,
                RecalcStyleFunc: this.state.RecalcStyleFunc,
                selectRowKey: this.state.SelectTr && !this.state.isEditMode ? this.state.SelectTr.trKey : null,
                onSelectRow: (item_tr) => {
                    this.setState({
                        SelectTr: item_tr
                    })//is_disabled
                },
                onDoubleClickTD: this.events.onDoubleClickTD,
                onRightClickTD: this.events.onRightClickTD,
                onClickTD: this.events.onClickTD,

                onDrawTable: this.events.onDrawTable,
                //IsResized
            };

            return <CoolTable {...DataForCoolTable} />;
        },
        renderCharts:()=>{
          if(this.state.isDisplayChart && this.state.isHasCharts)
          {
            return <div className="cool_t_chart"><ViewChart viewUid={this.state.ActiveTabUid}/></div>;
          }
          return null;
        }
    };
    helper = {
        GetDataForColumn: (ColumnID) => {
            for( let i = 0; i < this.state.HeadData.length; i++ ) {
                let item_th = this.state.HeadData[i];
                if(item_th["uid"] == ColumnID) {
                    return item_th;
                }
            }
        },
    };

    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        //  LoaderBlockUI = <h4 className="loader-text">Loading...</h4>;


        let TabsJSX;
        let TabsToolsJSX;
        let CoolTableJSX;


        if(this.state.isLoadTabs) {
            TabsJSX = this.core.RenderHZTabs();

            let TabSett= this.state.DataTabs.find((tab_item)=>{
              return tab_item.View==this.state.ActiveTabUid?true:false;
            })

            if(TabSett?.settings?.table_or_form==3){
              TabsToolsJSX = null;
              CoolTableJSX=<CalendarView
              user={{
                name:this.props.RootStore.MSettings.UserName,
                uid:this.props.RootStore.MSettings.UserUID
              }}
              view_sett={TabSett}
              onLoad={()=>{
                this.setState({isLoadHead:true, isLoadBody:true});
              }}
              />;
            }else{
              TabsToolsJSX = this.core.RenderHZTabsTools();
              if(this.state.isLoadHead && this.state.isLoadBody) {
                  if(this.state.HeadData.length > 0) {
                      CoolTableJSX = this.core.RenderTable();
                  }
              }
            }

        }


        let isBlocking = !this.state.isLoadTabs || !this.state.isLoadHead || !this.state.isLoadBody || this.state.isRequestRunning;


        let isActiveCloth = "";
        if(this.props.ViewStore.ActiveClothID == this.props.id) {
            isActiveCloth = "cl_active";
        }


        const dataContextMenu = {
            target: this.state.targetCM,
            content: this.state.contentCM,
            CloseCMFunc: () => { this.setState({targetCM: null}) }
        };

        let edit_mode_class = this.state.isEditMode ? "cool_t_edit_mode" : "";


        const dataBModal = {...this.state.BModal,
            ...{
            CloseCallback: () => {
                this.state.BModal.show = false;
                this.state.BModal.TypeModal = '';
                this.setState({});
            },
            SuccessFunction: () => {
                this.state.BModal.show = false;
                this.state.BModal.TypeModal = '';
                this.setState({});
                this.core.reload(false);
            },
            SaveSortCallback: (view_sorts) => {
                this.state.customData.view_sorts = view_sorts;
                this.state.BModal.show = false;
                this.state.BModal.TypeModal = '';
                let view_sorts_obj = {};
                view_sorts.forEach( (item) => {
                    view_sorts_obj[item.uid] = item;
                });
                this.state.HeadData.forEach((item_th2, key_th2) => {
                    if( view_sorts_obj[item_th2.key] != undefined) {
                        item_th2.Sort.isActive = true;
                        item_th2.Sort.dir = view_sorts_obj[item_th2.key].order == "asc" ? 1 : 0;
                    }
                    else {
                        item_th2.Sort.isActive = false;
                    }
                });
                this.core.reload(false);
            },
            SaveFixCallback: (newHeadData) => {
                this.state.BModal.show = false;
                this.state.BModal.TypeModal = '';
                this.isRecalcTable = false;
                this.setState({
                    //Сначала меняем шапку
                    HeadData: newHeadData,
                });
              //  this.core.reload();
            }

            }};

        return (
            <BlockUi id={this.props.id} className={"cool_t_cloth"} tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                <div className={"cool_t_wrapper_main " + isActiveCloth + " " + edit_mode_class} onClick={this.events.onClickCloth}>
                    <div className="cool_t_tollbar">
                        {TabsJSX}
                        {TabsToolsJSX}
                    </div>
                    <div className="cool_t_wrapper_table">
                      {this.core.renderCharts()}
                      <div className="cool_t_table">
                        {CoolTableJSX}
                      </div>
                    </div>
                </div>
                <HZContextMenu {...dataContextMenu} />

                <BModal {...dataBModal}/>
            </BlockUi>
        )
    }
}
