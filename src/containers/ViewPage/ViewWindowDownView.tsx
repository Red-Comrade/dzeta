import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect, useParams, useRouteMatch } from 'react-router-dom'
import {inject, observer, Provider} from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import CoolTable from '../../components/CoolTable'
import HZTabs from "../../components/HZTabs";

import * as v_api from '../../api/View-api'
import {ViewTableCloth} from "./ViewTableCloth";
import HZContextMenu from "../../components/HZContextMenu";
import RootStore from "../../stores/RootStore";
import ApplicationStore from "../../stores/ApplicationStore";
import ClassesStore from "../../stores/ClassesStore";
import ObjectsStore from "../../stores/ObjectsStore";
import ToastrStore from "../../stores/ToastrStore";
import ViewStore from "../../stores/ViewStore";
import ImportStore from "../../stores/ImportStore";
import UserStepStore from "../../stores/UserStepStore";

export interface IViewWindowDownViewProps {
    match?: any,
    history?: any,
    location?: any,


    IsZaglushka?: boolean,
    ViewUid?: string,
    AppUid: string,
    ParentView: string,
    ActiveTabUid?: string, //Вкладка для таблицы по-умолчанию
    name: string,
    tds_form: {
        data_td: any,
        data?: any,
    }[],
}
interface IViewWindowDownViewState {
    AppUid: string,
    ViewUid: string,

    blocking: boolean;
    RecalcStyleFunc: () => void,
}

export class ViewWindowDownView extends React.Component<IViewWindowDownViewProps, IViewWindowDownViewState> {
    state: Readonly<IViewWindowDownViewState> = {
        blocking: true,
        RecalcStyleFunc: null,
        AppUid: "",
        ViewUid: null,
    };

    constructor(props: IViewWindowDownViewProps) {
        super(props);
    };

    componentDidMount(): void {
        this.setState({
            AppUid: this.props.AppUid,
            ViewUid: this.props.ViewUid ? this.props.ViewUid: null,
            blocking: false
        })
    }

    core = {
    };



    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        //  LoaderBlockUI = <h4 className="loader-text">Loading...</h4>;

        let isBlocking = false;

        let ViewTableVisible;
        if(this.state.AppUid) {
            let SelectObjs = [];

            this.props.tds_form.forEach( (item_td, key_td) => {
                if(item_td.data_td.o && SelectObjs.indexOf(item_td.data_td.o) == -1 ) {
                    SelectObjs.push(item_td.data_td.o);
                }
            });


            const DataViewTableCloth = {
                IsZaglushka: true,

                id: "window-down-table-" + this.state.AppUid,
                AppUid: this.state.AppUid,
                ViewUid: this.state.ViewUid,
                ActiveTabUid: this.state.AppUid,
                ActiveTabName: this.props.name,
                tds_form: this.props.tds_form,
                SelectObjs: SelectObjs,
                ParentView: this.props.ParentView,
            };
            ViewTableVisible =
                <Provider ToastrStore={ToastrStore} RootStore={RootStore} ViewStore = {ViewStore} ObjectsStore = {ObjectsStore} ImportStore={ImportStore}>
                    <ViewTableCloth {...DataViewTableCloth} />
                </Provider>;
        }



        return (
            <BlockUi className="zt_cloth" tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                {ViewTableVisible}
            </BlockUi>
        )
    }
}