import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import { withTranslation } from 'react-i18next';
import Select, { Option } from 'rc-select';
import { InitStepSettings,SaveStepSettings } from '../../api/StepForm-api';
import { Fragment } from 'react';

import {MetkaSettModal} from './MetkaSettModal'
import RootStore from "../../stores/RootStore";



@inject("ToastrStore")
@observer
class UserStepSettPageForm extends React.Component{

	constructor(props) {
        super(props);
        this.state={
            showModalSettMetka:false,
        	data:[],
            dataSettModal:[],
            dataSettMetkaListAll:[],
            curLevel:null
        }        
        this.renderBlock=this.renderBlock.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
        this.handleInit=this.handleInit.bind(this);
        this.handleClickSave=this.handleClickSave.bind(this);
        this.handleClickDopSettMetka=this.handleClickDopSettMetka.bind(this);
        this.saveMetkaSett=this.saveMetkaSett.bind(this);
        this.getStateTreeSett=this.getStateTreeSett.bind(this);
        this.handleClickFillSpec=this.handleClickFillSpec.bind(this);
        this.handleInit();

    }
    componentDidMount(): void {
        document.title = 'User Step Page | ' + RootStore.config.nameSystem;
    }

    saveMetkaSett(level, settings)
    {       
        let newSett=[];
        settings.forEach(function(item, i, arr){
             if(settings[i].selectedValue!='---')
            {
                newSett.push(settings[i]);
            }
        });       
        let stateDate=this.state.data;
        let arr=level.split('_');
        let l1=arr[0];
        arr.shift();  
        let newStateData=this.changeStateTreeSett(stateDate[l1].value,arr,newSett);        
        stateDate[l1].value=newStateData;
        this.setState({
            data:stateDate
        });
       
        
    }
    handleClickDopSettMetka(e)
    {        
        let level=e.currentTarget.dataset.level;
        let stateDate=this.state.data;
        let arr=level.split('_');
        let l1=arr[0];
        arr.shift();  
        let dopSett=this.getStateTreeSett(stateDate[l1].value,arr);
        if(dopSett.dopSettMetka==undefined)
        {
            dopSett.dopSettMetka=[];
        }
        this.setState(
            {
                showModalSettMetka:true,
                dataSettModal: dopSett.dopSettMetka,
                curLevel:level
            });

    }
    handleInputChange(e){
    	
    	let level=e.target.dataset.level;
    	let val=e.target.value;
    	let stateDate=this.state.data;
    	let arr=level.split('_');
    	let l1=arr[0];
    	arr.shift();  
    	let newStateData=this.changeStateTree(stateDate[l1].value,arr,val);    	
    	stateDate[l1].value=newStateData;
    	this.setState({
	        data:stateDate,
            showModalSettMetka:false
	    });
    }
    handleInit()
	{
		InitStepSettings()
        .then((response) => {  
        	    	
          this.setState({
            data: response.data.sett,
            dataSettMetkaListAll: response.data.metks,
          }); 
        })
	}
    handleClickFillSpec()
    {
        let allMetkaforTextForAgreem=[];
        let dataState=this.state.data[0]['value'][4]['value'][0]['value'];
        for(let key1 in dataState)
        {            
            if(dataState[key1]['metka']=='')
            {
                dataState[key1]['metka']=key1;
            }
            allMetkaforTextForAgreem.push(dataState[key1]['metka']);
            for(let key2 in dataState[key1]['value']['Block elements']['value'])
            {
                if(dataState[key1]['value']['Block elements']['value'][key2]['metka']=='')
                {
                    dataState[key1]['value']['Block elements']['value'][key2]['metka']=key1+'.'+key2;
                }
                allMetkaforTextForAgreem.push(dataState[key1]['value']['Block elements']['value'][key2]['metka']);
            }
        }
        let newState=this.state.data;
        newState[0]['value'][4]['value'][0]['value']=dataState;

        let checkBoxs=this.state.data[0]['value'][2]['value'];
        for(let i=0;i<checkBoxs.length;i++)
        {
            for(let j=0;j<checkBoxs[i]['value'].length;j++)
            {
                if(checkBoxs[i]['value'][j]['metka']=='')
                {
                    checkBoxs[i]['value'][j]['metka']='Check.'+checkBoxs[i]['name']+'.'+checkBoxs[i]['value'][j]['name'];
                }
            }
        }
        console.log("checkBoxs");
        console.log(checkBoxs);
        newState[0]['value'][2]['value']=checkBoxs;
        let dopSettTextForAggrement=newState[0]['value'][3]['value'][0]['value'][1]['value'][2]['dopSettMetka'];

        for(let i=0;i<dopSettTextForAggrement.length;i++)
        {
            let key=allMetkaforTextForAgreem.indexOf(dopSettTextForAggrement[i].selectedValue);
            if(key!=-1)
            {
                delete allMetkaforTextForAgreem[key];
            }
        }
        
        let allMetkasList=this.state.dataSettMetkaListAll;
        for(let key1 in allMetkaforTextForAgreem)
        {
            dopSettTextForAggrement.push(
            {
                'selectedValue':allMetkaforTextForAgreem[key1],
                'type':'select'
            });
            allMetkasList.push(allMetkaforTextForAgreem[key1]);
        }
        newState[0]['value'][3]['value'][0]['value'][1]['value'][2]['dopSettMetka']=dopSettTextForAggrement
        this.setState({
            data: newState,
            dataSettMetkaListAll:allMetkasList,
            showModalSettMetka:false
        });
        
    }
	handleClickSave()
	{	
		SaveStepSettings(JSON.stringify(this.state.data))
        .then((response) => {        	
          /*this.setState({
            data: JSON.parse(response.data.data)
          }); */
          console.log(this.props);
          this.props.ToastrStore.success('Save', 'Save');
        })
	}

    changeStateTree(data,arrLevel,val)
    {    	
    	let l1=arrLevel[0];
    	arrLevel.shift();
    	if(arrLevel.length==0)
    	{
    		data[l1].metka=val;
    		return data;
       	}       	
       	let newStateData=this.changeStateTree(data[l1].value,arrLevel,val);
       	data[l1].value=newStateData;
       	return data;
    }
    changeStateTreeSett(data,arrLevel,val)
    {        
        let l1=arrLevel[0];
        arrLevel.shift();
        if(arrLevel.length==0)
        {
            //data.value=val;
            data[l1].dopSettMetka=val;
            return data;
        }           
       let newStateData=this.changeStateTreeSett(data[l1].value,arrLevel,val);
       data[l1].value=newStateData;
       return data;
    }
    getStateTreeSett(data,arrLevel){
        let l1=arrLevel[0];
        arrLevel.shift();
        if(arrLevel.length==0)
        {
            //data.value=val;
            data[l1];
            return data[l1];
        }
       return this.getStateTreeSett(data[l1].value,arrLevel);
    }
    renderBlock(data,level){
    	let idBlockForm;
    	switch (data['type']) {
            case "block":                
                return(
                    Object.keys(data['value']).map((key,index)=>
                    {        
                        let tmp=level+"_"+key;                
                        return(
                            <React.Fragment  key={tmp+"__"}>
                                <div className="row" > 
                                    <div className="col-md-12 col-lg-12 col-xs-12">                                                             
                                        {this.renderBlock(data['value'][key],tmp)}
                                    </div>
                                </div>  
                            </React.Fragment>
                        );                       
                    }
                )
            );                
            break;
            case "blockForm":
            case "blockDB":
            case "blockCheck":
                idBlockForm="identifierbf-"+level;
                let keyss=Object.keys(data['value']);
                var s='',send='';
                console.log(data['value'][keyss[0]]);
                if(data['value'][keyss[0]]==undefined)
                {
                    console.log('Problems');
                    console.log(data['value']);
                    return (<div></div>);
                }
                if(data['value'][keyss[0]]['type']=='elem')
               {
                   return(
                        <div className="row" style={{'marginLeft':'10px'}}>    
                            <input className="toggle-box" id={idBlockForm} type="checkbox" />
                            <label htmlFor={idBlockForm} style={{'fontSize':'20px'}} >&#8661;{data['name']}</label>  
                            <div className="col-md-12 col-lg-12 col-xs-12">
                               <React.Fragment key={level+"__"}> 
                                   <div className="row"> 
                                            {Object.keys(data['value']).map((key,index)=>
                                                {        
                                                    let tmp=level+"_"+key; 
                                                        return(
                                                            <React.Fragment key={tmp+"__"}>                                                                                                     
                                                                {this.renderBlock(data['value'][key],tmp)}                                             
                                                            </React.Fragment>
                                                        );               
                                                }
                                            )}
                                      </div>
                                </React.Fragment>
                                </div>
                          </div>
                         ); 
               }else{
                   return( 

                <div className="row">   
                    <input className="toggle-box" id={idBlockForm} type="checkbox" />
                    <label htmlFor={idBlockForm} style={{'fontSize':'25px'}}>&#8661;{data['name']}</label>
                    <div className="col-md-12 col-lg-12 col-xs-12"> 
                        <React.Fragment key={level+"__"}> 
                            {Object.keys(data['value']).map((key,index)=>
                                {        
                                    let tmp=level+"_"+key;   
                                    let tmp1=tmp+"_";
                                        return(
                                            <React.Fragment key={tmp1}>                                                                                                     
                                                {this.renderBlock(data['value'][key],tmp)}                                             
                                            </React.Fragment>
                                        ); 
                                                 
                                                          
                                }
                            )}
                        </React.Fragment>
                    </div>
                </div>
                 ); 
               }
                
            break;
            case 'blockObjwithChild':
                idBlockForm="identifierboc-"+level;
                let idBlockForm1=idBlockForm+"1";
                return(
                    <div className="row" >
                        <div className="col-md-12 col-lg-12 col-xs-12">
                        <input className="toggle-box" id={idBlockForm} type="checkbox" />
                        <label htmlFor={idBlockForm} style={{'fontSize':'15px'}}>&#8661;{data['name']}</label>
                        <React.Fragment key={idBlockForm1}> 
                            <div className="row" style={{'marginLeft':'30px'}}>
                            <div className="col-md-12 col-lg-12 col-xs-12">
                                {
                                    Object.keys(data['value']).map((key,index)=>
                                    {
                                        let tmp=level+"_"+key;
                                         /**/
                                         let hideBlock='fas fa-plus-square',text='',styleInput={};

                                            if(data['value'][key].dopSettMetka==undefined)
                                            {
                                                data['value'][key].dopSettMetka=[];

                                            }
                                            if(data['value'][key].dopSettMetka.length==0)
                                            {
                                                
                                            }else{
                                                text='('+data['value'][key].dopSettMetka.length+')';
                                                hideBlock='fas fa-plus-square';
                                                styleInput={'backgroundColor':'antiquewhite'};
                                            }
                                        return (
                                            <div key={tmp+"_0"}>
                                                <input className="toggle-box" id={tmp} type="checkbox" />
                                                <label htmlFor={tmp}>{data['value'][key]['name']}
                                                    <span  onClick={this.handleClickDopSettMetka} data-level={tmp}>
                                                     {text}    
                                                        <i  className={hideBlock} style={{"marginLeft":"10px"}}></i>
                                                    </span>

                                                </label>
                                                
                                                <input style={styleInput} className="form-control" data-level={tmp} value={data['value'][key]['metka']} onChange={this.handleInputChange}/>
                                                <div className="row" style={{'marginLeft':'30px'}}>
                                                   
                                                      <div className="col-md-12 col-lg-12 col-xs-12">
                                                        {
                                                            Object.keys(data['value'][key]['value']).map((key1,index)=>
                                                            {        
                                                                let tmp1=tmp+"_"+key1;  
                                                                let tmp2= tmp1+'_' ;                                                             
                                                                if(data['value'][key]['value'][key1]['type']=="elem") 
                                                                {
                                                                    return(
                                                                        <div className="row" key={tmp2+"_0"}>
                                                                            <React.Fragment key={tmp2}>                                                                                                     
                                                                                {this.renderBlock(data['value'][key]['value'][key1],tmp1)}                                                                                                                         
                                                                            </React.Fragment>
                                                                        </div>
                                                                    ); 
                                                                } else{
                                                                    return(
                                                                        <React.Fragment key={tmp2}>                                                                                                     
                                                                            {this.renderBlock(data['value'][key]['value'][key1],tmp1)}                                                                                                                         
                                                                        </React.Fragment>
                                                                    ); 
                                                                }         
                                                                                     
                                                            }
                                                        )}
                                                   </div>                                                    
                                                </div>
                                            </div>
                                        );
                                    })
                                }
                            </div>
                            </div>
                        </React.Fragment> 
                    </div>
                     </div>
                );
            break;
             case "elem": 
                level=level;
                let hideBlock='fas fa-plus-square',text='',styleInput={};

                if(data.dopSettMetka==undefined)
                {
                    data.dopSettMetka=[];

                }
                if(data.dopSettMetka.length==0)
                {
                    
                }else{
                    text='('+data.dopSettMetka.length+')';
                    hideBlock='fas fa-plus-square';
                    styleInput={'backgroundColor':'antiquewhite'};
                }
                //background-color: antiquewhite;
                return(
                        <div className="form-group col-md-4" key={level}> 
                            <label>
                                {data.name}
                                <span  onClick={this.handleClickDopSettMetka} data-level={level}>
                                     {text}    
                                    <i  className={hideBlock} style={{"marginLeft":"10px"}}></i>  
                                                                     
                                </span>
                            </label>
                            <input style={styleInput} className="form-control" data-level={level} value={data.metka} onChange={this.handleInputChange}/>
                        </div> 
                    );  
                break;
        }
    }
    render() {
    	let html=(<div></div>);
    	if(this.state.data.length>0)
		{
			html=this.renderBlock(this.state.data[0],'0');
		}	
        ;  
    	return(
    		<div style={{'margin':'30px'}}>
                <div className="col-md-12 col-lg-12 col-xs-12" style={{'margin':'30px',"paddingBottom": "30px"}}>                
    			    {html}
                </div>
    			<div className="portlet__footer" style={{'backgroundColor': '#e0e0e0',"position": "fixed","left": "0px","bottom": "0px","width": "100%"}}>
                   
    				<div>    					
    					<button type="button" className="btn btn-primary btn btn-success" onClick={this.handleClickSave} style={{'marginRight':'10px','float':'right'}}>Save</button>
    				</div>
                     <div>                       
                        <button type="button" className="btn btn-primary btn btn-success" onClick={this.handleClickFillSpec} style={{'marginRight':'10px','float':'right'}}>Specfill</button>
                    </div>
    			</div>
                <MetkaSettModal show={this.state.showModalSettMetka} dataSelect={this.state.dataSettModal} dataSettMetkaListAll={this.state.dataSettMetkaListAll} level={this.state.curLevel} saveFMetk={this.saveMetkaSett}/>
			</div>	     
			
				  );
    }
}

const UserStepSettPageTranslation = withTranslation('import')(UserStepSettPageForm);

export function UserStepSettPage(props) {
  return (
    <React.Suspense fallback={<Loader active type="ball-pulse"/>}>

      <UserStepSettPageTranslation {...props} />
    </React.Suspense>
  );
}