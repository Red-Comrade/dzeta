import * as React from "react";
import { Modal, Button, Table } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import Select, { Option } from 'rc-select';
import {inject, observer} from "mobx-react";


@inject("ToastrStore")
@observer
export class MetkaSettModal extends React.Component {

	constructor(props) {
        super(props);
        this.state={
           show:false,
           dataSelect:[],
           dataSelectMetka:[]
        };
        this.closeModal= this.closeModal.bind(this);
        this.handleSelectGroupAdd=this.handleSelectGroupAdd.bind(this);
        this.handleChangeSelectMetka=this.handleChangeSelectMetka.bind(this);
        this.removeBlockSelect=this.removeBlockSelect.bind(this);
        this.saveModal=this.saveModal.bind(this);
    }
    saveModal(){
    	
    	this.props.saveFMetk(this.props.level,this.state.dataSelect);
    	this.setState({
           show:false
        });
    }
    removeBlockSelect(event){
    	let dataSelect=this.state.dataSelect;
    	delete dataSelect[event.currentTarget.dataset.key];
    	this.setState({
           dataSelect:dataSelect
        });
    }
    handleChangeSelectMetka(event){
    	
		let dataSelect=this.state.dataSelect;
    	dataSelect[event.target.dataset.key].selectedValue=event.target.value;
    	this.setState({
           dataSelect:dataSelect
        });
    }
    handleSelectGroupAdd(){
    	let dataSelect=this.state.dataSelect;
    	dataSelect.push({'type':'select','selectedValue':0});     
    	this.setState({
           dataSelect:dataSelect
        });
    }
    closeModal(){
    	this.setState({
           show:false
        });
    }
    

    componentDidUpdate(prevProps){
      
     /*  this.setState({
           show:false
        });*/
    }
    componentWillReceiveProps(nextProps) {
    
      console.log(nextProps);
      if(this.state.show==false&&nextProps.show)
      {
      	this.setState({
           show:nextProps.show,
           dataSelect:nextProps.dataSelect,
           dataSelectMetka:nextProps.dataSettMetkaListAll,
        });
      }
       
      
    }

    render() {
        let Title="Définition de l'étiquette";
        console.log(this.state.dataSelect);
        let Body=
        <div>
        	<div className="row" style={{"marginBottom":"15px"}}>
        		<button  onClick={this.handleSelectGroupAdd} className="btn-primary">
                	Ajouter une étiquette
            	</button>
        	</div>	   
          <div style={{"overflowY":"auto","overflowX":"hidden","height":"500px"}}>     
	        	{
	        		Object.keys(this.state.dataSelect).map((key,index)=>
                    {        
                        let tmpKey='selectKey_' +key;  
                          
                        return(
                           <div className="row" key={tmpKey} style={{"marginBottom":"10px","marginLeft":"5px"}}>
								<select defaultValue={this.state.dataSelect[key].selectedValue==undefined? 0:this.state.dataSelect[key].selectedValue} className="form-control" key={key}  data-key={key} style={{"width":"70%"}} onChange={this.handleChangeSelectMetka}>
								  {
								  	Object.keys(this.state.dataSelectMetka).map((key1,index)=>
								  	{                      
								  		return(
								  			<option key={key1} data-key={key}  value={this.state.dataSelectMetka[key1]}>
								  				{this.state.dataSelectMetka[key1]}
								  			</option>
								  		);
								  	})
								  }
								</select>
								<button className="btn-danger" data-key={key} onClick={this.removeBlockSelect}>
					        <i className="fas fa-trash"></i>
					      </button>
            </div>
                        );                       
                    })
	        	}
          </div>  
      </div>;
        let Footer=<div style={{"marginLeft":"-15px"}}>
        	 <button className="btn-danger btn" onClick={this.closeModal} >
                Clode
            </button>
            <button className="btn-success btn"  onClick={this.saveModal}>
                Sauvegarder
            </button>
        </div>;
        return (
            <Modal className="add_operation_modal" show={this.state.show} animation={true} onHide={this.closeModal}>
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} >
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }


}