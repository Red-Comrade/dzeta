import * as React from "react";
import * as ReactDOM from "react-dom";
import {inject, observer} from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import './ApplicationPage.scss'

import * as ct_api from '../../api/AppTree-api'

import {TabeleApp} from '../../components/TabbleApp'
import {ViewSettings} from './ViewSettings'
import {AppSettings} from './AppSettings'
import {ChoseClassApp} from './ChoseClassApp'
import {AppClassesLink} from './AppClassesLink'
import {AppSortModal} from './AppSortModal'
import {IViewWindowFormViewProps} from "../ViewPage/ViewWindowFormView";
import * as ot_api from "../../api/ObjTree-api";
import ToastrStore from "../../stores/ToastrStore";
import RootStore from "../../stores/RootStore";


export interface IApplicationPageProps {
    ApplicationStore: any,
    ToastrStore: any,

    match: any,
}

export interface IApplicationPageState {
    UserGroups: {
        uid: string,
        name: string,
    }[],


    blocking: boolean,
}


@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export class ApplicationPage extends React.Component<IApplicationPageProps, IApplicationPageState> {
    NameModule: "application" | "papplication" = "application"; //Название модуля при котором работает настройка прав
    state: Readonly<IApplicationPageState> = {
        UserGroups: null,
        blocking: false,
    };

    constructor(props: IApplicationPageProps) {
        super(props);
    };

    componentDidMount() {
        this.NameModule = this.props.match.path.indexOf('/application') !== -1 ? "application" : "papplication";
        document.title = this.NameModule[0].toUpperCase() + this.NameModule.slice(1) + ' | ' + RootStore.config.nameSystem;

        this.core.LoadDataForApplication();
    }

    componentWillUnmount(): void {
        console.error('unmount');
    }

    componentDidUpdate(prevProps: Readonly<IApplicationPageProps>, prevState: Readonly<IApplicationPageState>, snapshot?: any): void {
        let PrevNameModule: "application" | "papplication" = prevProps.match.path.indexOf('/application') !== -1 ? "application" : "papplication";
        let NameModule: "application" | "papplication" = this.props.match.path.indexOf('/application') !== -1 ? "application" : "papplication";

        if (PrevNameModule != NameModule) {
            this.NameModule = NameModule;

            if(!this.props.ApplicationStore.ParamFullData) {
                this.core.LoadDataForApplication();
            }
            else {
                if (NameModule == "papplication") {
                    if(!this.state.UserGroups) {
                        this.core.Permission.InitGroupsData({isAjax: true});
                    }
                    else {
                        this.core.Permission.InitGroupsData({UserGroups: this.state.UserGroups});
                    }
                }
            }
        }
    }

    core = {
        AJAX: {
            getFullAttrForApp: (isPerm, SuccFunc) => {
                ct_api
                    .getFullAttrForApp(isPerm)
                    .then(res => {
                        SuccFunc(res.data);
                    })
                    .catch((error) => {
                        console.log(error);
                        this.props.ToastrStore.error('Server error!', 'Error');
                        this.setState({blocking: false});
                    })
            },

            //Получение Групп пользователей
            getChildrenByType: (parent: string, type: string, SuccFunc, ErrFunc) => {
                ot_api
                    .getChildrenByType(parent, type)
                    .then(res => {
                        if (res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrFunc({result: 0});
                    })
            },
        },

        //Загрузка, или перезагрузка модуля приложений
        LoadDataForApplication: () => {
            const GetFullAttrFunc = () => {
                this.core.AJAX.getFullAttrForApp(false, (data) => {
                    if (data.result === 1) {
                        this.props.ApplicationStore.SetFullParamData(data.data);
                    }
                    else {
                        this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
                    }
                });
            };

            if (this.NameModule == "papplication") {
                this.core.Permission.InitGroupsData({isAjax: true, SuccFunc: GetFullAttrFunc});
            }
            else {
                GetFullAttrFunc();
            }
        },


        Application: {
            build: {
                RenderViewSett: () => {
                    if (this.props.ApplicationStore.AppViewSett && this.props.ApplicationStore.AppViewSett.attrs && this.props.ApplicationStore.ParamFullData) {
                        let Data = [];
                        this.props.ApplicationStore.AppViewSett.attrs.forEach((item, key) => {
                            Data.push({...item});
                        });
                        let iddb = this.props.ApplicationStore.AppViewSett.uid;
                        let title = this.props.ApplicationStore.AppViewSett.name;
                        //@ts-ignore
                        return (<ViewSettings viewUid={iddb} viewTitle={title} attrData={Data}/>);
                    }
                    return null;
                },
                RenderAppSett: () => {
                    let AppSett = this.props.ApplicationStore.AppSett;
                    let iddb = AppSett.app;
                    let title = AppSett.name;
                    return (<AppSettings appUid={iddb} appTitle={title}/>);
                },
            }
        },
        Permission: {

            InitGroupsData: (params:{UserGroups?:any, isAjax?:boolean, SuccFunc?:any}) => {

                const SuccInitData = (UserGroups) => {
                    //SelectedUserGroup
                    let PathGroupID = this.props.match.params.GroupID;
                    let isEx = false;

                    const GenerateGroupFunc = () => {
                        this.setState({
                            UserGroups: UserGroups,
                        });
                        if(params.SuccFunc)
                            params.SuccFunc();
                    };

                    for (let key_g in UserGroups) {
                        if (UserGroups[key_g].uid == PathGroupID) {
                            isEx = true;
                            break;
                        }
                    }

                    //Если не нашли группу, то берём самую первую
                    if (!isEx) {
                        for (let key_g in UserGroups) {
                            if (UserGroups[key_g].uid) {
                                GenerateGroupFunc();
                                location.hash = `/papplication/` + UserGroups[key_g].uid;
                                return;
                            }
                        }
                    }

                    //Если первой не нашлось, то выводим ошибку
                    if (!isEx) {
                        ToastrStore.error("No such user group!");
                        location.hash = `/papplication/`;
                    }
                    else {
                        GenerateGroupFunc();
                    }
                };

                if(params.isAjax) {
                    this.core.AJAX.getChildrenByType("00000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-000000000002", (answer) => {
                        SuccInitData(answer.data);
                    }, (answer) => {
                        ToastrStore.error("error", "code=" + answer.result);
                        debugger
                    });
                }
                else {
                    SuccInitData(params.UserGroups);
                }




            },
            build: {
                RenderPermissionPanel: () => {
                    let control;

                    if (this.state.UserGroups === null) {
                        control = <input readOnly={true} className={"form-control"} value={"Loading..."}/>
                    }
                    else {
                        let options = [<option key={"0"} value="0"> --- No groups --- </option>];

                        this.state.UserGroups.forEach((item, key) => {
                            options.push(<option key={item.uid} value={item.uid}>{item.name}</option>);
                        });

                        let PathGroupID = this.props.match.params.GroupID;
                        PathGroupID = PathGroupID ? PathGroupID : "0";


                        control = <select onChange={this.events.onChangeUserGroup} className="form-control"
                                          value={PathGroupID}>{options}</select>;
                    }

                    return <div styleName="permiss_co">
                        <div styleName="permiss_co1"><h5>User Group</h5></div>
                        <div styleName="permiss_co2">{control}</div>
                    </div>;
                },
            }
        }
    };

    events = {
        onChangeUserGroup: (e) => {
            let value = e.target.value;
            value = value && value != 0 ? value : "";

            location.hash = `/papplication/` + value;
        },
    };


    render() {
        let NameModule: "application" | "papplication" = this.props.match.path.indexOf('/application') !== -1 ? "application" : "papplication";
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
        let isBlocking = this.props.ApplicationStore.IsLoad;

        let Content_sett;
        let AppContentJSX = <div className="label_center"><h4 className="loader-text">Select a node in the tree...</h4>
        </div>;
        let PermissPanel;

        if (NameModule == "application") {
            if (this.props.ApplicationStore.AppViewSett) {
                Content_sett = this.core.Application.build.RenderViewSett();
            }
            else if (this.props.ApplicationStore.AppSett) {
                Content_sett = this.core.Application.build.RenderAppSett();
            }
            if (!this.props.ApplicationStore.AppParamData) {
                AppContentJSX = this.props.ApplicationStore.AppContentJSX;
            }
        }
        else if (NameModule == 'papplication') {
            PermissPanel = this.core.Permission.build.RenderPermissionPanel();
            AppContentJSX = this.props.ApplicationStore.AppContentJSX;
        }


        return (
            <BlockUi id="zt_AppPage" tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                {PermissPanel}
                {AppContentJSX}
                {Content_sett}
                <AppClassesLink/>
                <AppSortModal/>
            </BlockUi>
        )
    }
}
