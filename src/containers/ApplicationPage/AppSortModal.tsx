import * as React from "react";
import { Modal, Button, Table } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/AppTree-api";


import {DnDListOrder} from '../../components/DnDListOrder'



interface IAppSortModalProps{
  ToastrStore?:{
    error:(text:string, title:string)=>void,
    info:(text:string, title:string)=>void,
    success:(text:string, title:string)=>void,
  }
  ApplicationStore?:{
    SelectNode:{iddb:string, ParentID:string, isFolder:boolean, Name:string},
    setShowModalSort:(state:boolean, items:[], mode:string, advData:object)=>void,
    UpdParamsTrigAction:(isLoading:boolean, data:object[])=>void,
    sortMode:string,
    sortItems:{uid:string, name:string, class?:{name:string}}[],
    sortAdvData:{
      folderuid:string,
    },
    showSortModal:boolean,
  }

}
interface IAppSortModalState{
  blocking:boolean,
  sorted:{id:string, index:number}[],
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export class AppSortModal extends React.Component<IAppSortModalProps, IAppSortModalState> {


    constructor(props) {
        super(props);
        this.state={
            blocking:false,
            sorted:[],
        };
    }
    onShow=()=>{
        this.setState({sorted:[]});
    };
    handleClose = () => {
       this.props.ApplicationStore.setShowModalSort(false, [], '', null);
    };

    renderBody=()=>{
      let that=this;
      let events={
        onDragEnd:function(result){
          const reorder=(list, startIndex, endIndex)=>{
            const result = Array.from(list);
            const [removed] = result.splice(startIndex, 1);
            result.splice(endIndex, 0, removed);

            return result;
          };

          if(!result.destination) {
             return;
          }
          console.log(this.state);
          const items = reorder(
            this.state.items,
            result.source.index,
            result.destination.index
          );

          this.setState({
            items:items
          }, ()=>{
            let sortedParam=items.map((item:{id:string, index:number}, i)=>{
              return {id:item.id, index:i}
            });
            that.setState({sorted:sortedParam})
          });

        },
      };
      let Items=[];
      let mode =this.props.ApplicationStore.sortMode;
      if(mode==='sortParams'){
          Items=this.props.ApplicationStore.sortItems.map((item:{uid:string, name:string, class:{name:string}})=>{
            return {
              id:item.uid,
              content:<div><span className="pram_name">{item.name}</span><span style={{float:'right',fontSize:13,color:'#988f8f'}} className="type_name">{(item.class?item.class.name:'')}</span></div>
            }
          })

      }else if(mode==='sortFolders' || mode=='sortApp'){
        Items=this.props.ApplicationStore.sortItems.map((item:{uid:string, name:string})=>{
          return {
            id:item.uid,
            content:<div><span className="pram_name">{item.name}</span></div>
          }
        })
      }

      return <DnDListOrder items={Items} events={events}/>
    }
    handleSave=()=>{
      if(!this.state.sorted.length){
        this.props.ToastrStore.info('Please sort params.', 'Information');
        return false;
      }
      let uidView= this.props.ApplicationStore.SelectNode.iddb;
      let orderParam=this.state.sorted.map((item)=>{
        return item.id
      })
      ct_api.sortAttrView(uidView, orderParam).then(res => {
          if(res.data.result===1){
            this.props.ToastrStore.success('Success', 'Params sorted successfully!');
            this.handleClose();
            this.updateDataView(uidView);
          }else{
            this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + res.data.result);
          }
      })
      .catch( (error) => {
          console.log(error);
          this.props.ToastrStore.error('Server error!', 'Error');
      })
    }
    handleSaveFolders=()=>{
      if(!this.state.sorted.length){
        this.props.ToastrStore.info('Please sort params.', 'Information');
        return false;
      }
      let orderFolders=this.state.sorted.map((item, i)=>{
        return {uid:item.id, index:i}
      })
      ct_api.orderFolders(orderFolders).then(res => {
          if(res.data.code===1){
            this.props.ToastrStore.success('Success', 'Folder sorted successfully!');
            this.handleClose();
          }else{
            this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + res.data.code);
          }
      })
      .catch( (error) => {
          console.log(error);
          this.props.ToastrStore.error('Server error!', 'Error');
      })
    }
    handleSaveApps=()=>{
      let folderuid=this.props.ApplicationStore.sortAdvData.folderuid;
      if(!this.state.sorted.length){
        this.props.ToastrStore.info('Please sort params.', 'Information');
        return false;
      }
      let orderApp=this.state.sorted.map((item, i)=>{
        return {uid:item.id, index:i}
      })
      ct_api.orderApp(folderuid, orderApp).then(res => {
          if(res.data.code===1){
            this.props.ToastrStore.success('Success', 'Folder sorted successfully!');
            this.handleClose();
          }else{
            this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + res.data.code);
          }
      })
      .catch( (error) => {
          console.log(error);
          this.props.ToastrStore.error('Server error!', 'Error');
      })
    }

    updateDataView=(uidView)=>{
      ct_api.getAttributesForApp(uidView).then((data) => {
          if(data.data.result === 1) {
              console.log(data)
              this.props.ApplicationStore.UpdParamsTrigAction(false, data.data.data);
          }
          else {
              this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.data.result);
              this.props.ApplicationStore.UpdParamsTrigAction(false, []);

          }
      });
    };

    render() {
        let Body=this.renderBody();
        let mode =this.props.ApplicationStore.sortMode;
        let Title=''
        let Footer=null;
        if(mode==='sortParams'){
           Title='Sort params';
           Footer=<div>
              <Button variant="success" onClick={this.handleSave}>
                  Save
              </Button>
              <Button variant="secondary" onClick={this.handleClose}>
                  Close
              </Button>
          </div>;
        }else if(mode==='sortFolders'){
           Title='Sort folders';
           Footer=<div>
              <Button variant="success" onClick={this.handleSaveFolders}>
                  Save
              </Button>
              <Button variant="secondary" onClick={this.handleClose}>
                  Close
              </Button>
          </div>;
        }else if(mode==='sortApp'){
           Title='Sort App';
           Footer=<div>
              <Button variant="success" onClick={this.handleSaveApps}>
                  Save
              </Button>
              <Button variant="secondary" onClick={this.handleClose}>
                  Close
              </Button>
          </div>;
        }

        return (
            <Modal  size="lg" onShow={this.onShow} animation={true} show={this.props.ApplicationStore.showSortModal} onHide={this.handleClose}>
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}
