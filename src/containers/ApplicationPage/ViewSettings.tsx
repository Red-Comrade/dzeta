import * as React from "react";
import {inject, observer} from "mobx-react";
import CreatableSelect from 'react-select/creatable';
import Select, { components } from 'react-select';
import InputColor from 'react-input-color';

import * as ct_api from '../../api/AppTree-api'
import * as ct_api_class from '../../api/ClassTree-api'
import * as ct_apiobj from "../../api/ObjTree-api";

import {ClassRelation} from '../../components/ClassRelation'
import {ViewDataSettings} from '../../components/ViewDataSettings'
import {FilterBlock, SingleModeBlock} from '../../components/ViewDataFilters'
import {ViewParamSetting} from '../../components/ViewParamSetting'
import CoolTable from '../../components/CoolTable'

//TableControl
import {ChoseViewCtrl} from './ParamComponents/ChoseViewCtrl'
import {ColumnWidthCtrl} from './ParamComponents/ColumnWidthCtrl'
import {ColorCalcCtrl} from './ParamComponents/ColorCalcCtrl'
import {DisplayCtrl} from './ParamComponents/DisplayCtrl'
import {ChoseClass} from './ParamComponents/ChoseClass'
import {FixedColumnCtrl} from './ParamComponents/FixedColumnCtrl'
import {AliasCtrl} from './ParamComponents/AliasCtrl'
import {DataSourceCtrl} from './ParamComponents/DataSourceCtrl'

import { DataTable  } from 'primereact/datatable';
import { MultiSelect } from 'primereact/multiselect';
import { Column } from 'primereact/column';

import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';

import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';


const IOSSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 42,
            height: 26,
            padding: 0,
        },
        switchBase: {
            padding: 1,
            '&$checked': {
                transform: 'translateX(16px)',
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#17a2b8',
                    opacity: 1,
                    border: 'none',
                },
            },
            '&$focusVisible $thumb': {
                color: '#17a2b8',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 24,
            height: 24,
        },
        track: {
            borderRadius: 26 / 2,
            border: `1px solid ${theme.palette.grey[400]}`,
            backgroundColor: theme.palette.grey[50],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }:any) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});


export interface IViewSettingsProps {
    attrData: {
      class:{
        name:string,
        uid:string,
      },
      name:string,
      datatype:string,
      uid:string,
      id:number,
    }[],
    viewUid: string,
    viewTitle:string,
    ToastrStore?:any,
    ApplicationStore?:any,
}

export interface IViewSettingsPropsClass {
  attrData: object[],
  viewUid: string,
  classUid:string,
  classTitle:string,
  ToastrStore?:any,
  ApplicationStore?:any,
}

export interface IViewSettingsPropsParam{
  attrData: {
    class:{
      name:string,
      uid:string,
    },
    name:string,
    datatype:string,
    uid:string,
    id:number,
  },
  viewUid: string,
  ToastrStore?:any,
  ApplicationStore?:any,

}

export interface IViewSettingsPropsAddParam {
  viewUid: string,
  classData:{uid:string, name:string},
  attrData: object[],
  ToastrStore?:any,
  ApplicationStore?:any,
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export class ViewSettings  extends React.Component<IViewSettingsProps> {

  clickView=(e)=>{
    this.props.ApplicationStore.SetWorkAreaMode({
      mode:'viewTable',
      data:this.props.viewUid,
    });
  }

  renderBtn=()=>{
    let classesUid=this.props.attrData.filter((item:any)=>{
      return item.class && item.class.uid?true:false;
    }).map((item:any)=>{
      return item.class.uid;
    }).filter((item, index, arr)=> {
      return arr.indexOf(item) === index;
    });

    let countClasses=classesUid.length;
    let countParams=this.props.attrData.length;
    let icon=<i className="fa fa-table"></i>;
    if(this.props.ApplicationStore.AppViewSett.settings && this.props.ApplicationStore.AppViewSett.settings.table_or_form==1){
       icon=<i className="far fa-window-maximize"></i>;
    }else if(this.props.ApplicationStore.AppViewSett.settings && this.props.ApplicationStore.AppViewSett.settings.table_or_form==2){
       icon=<i className="far fa-object-group"></i>;
    }
    return <div className="viewsett__ctrlpanel ctrlpanel">
      <div className="ctrlpanel__info info">
        <h3>{icon} {this.props.viewTitle}</h3>
        <span className="info__classes">Classes: <span className="count">{countClasses}</span></span>
        <span className="info__params">Params: <span className="count">{countParams}</span></span>
      </div>
      <div className="ctrlpanel__btn">
        <button onClick={this.saveView} className="btn btn-primary btn-sm">Save <i className="fa fa-save" aria-hidden="true"></i></button>
      </div>

    </div>
  }
  saveView=()=>{
    let ViewSett=JSON.parse(JSON.stringify(this.props.ApplicationStore.AppViewSett));
    ViewSett.appUid=this.props.ApplicationStore.SelectNode.appUid;
    console.log(ViewSett);
    ct_api.saveView(ViewSett).then((res)=>{
      if(res.data.result==1){
        this.props.ToastrStore.success('Success', 'View saved successfully!');
        this.updateDataView(ViewSett.uid);
      }
    })

  }
  updateDataView=(uid)=>{
    this.props.ApplicationStore.setLoader(true);
    ct_api.getView(uid).then((res)=>{
      //Данные дял связей типов пердставления
      if(res.data.result==1){
        let data_view=res.data.data;
        //Создания ид для параметров
        data_view.view.attrs=data_view.view.attrs.map((item) => {
            let data= {id:Math.floor(Math.random() * 10000000000), ...item};
            data.class={name:data.parent.name , uid:data.parent.uid};
            return data;
        });

        this.props.ApplicationStore.setDataView(this.props.ApplicationStore.SelectNode, data_view.view, false, {mode:'viewTable', uid}, data_view.ParrentsToRoot);

      }
    });
  }
  render(){
    let type_of_view= this.props.ApplicationStore.AppViewSett.settings && this.props.ApplicationStore.AppViewSett.settings.table_or_form? this.props.ApplicationStore.AppViewSett.settings.table_or_form:0;
    let ViewSettParams_elem= <ViewSettParams viewTitle={this.props.viewTitle} attrData={this.props.attrData} viewUid={this.props.viewUid}/>;
    if(type_of_view==2){
      ViewSettParams_elem=null;
    }
    return <div className="viewsett">
      {this.renderBtn()}
      <div  className="viewsett__workarea workarea">
        {ViewSettParams_elem}
        <ViewSettWorkArea viewTitle={this.props.viewTitle} attrData={this.props.attrData} viewUid={this.props.viewUid}/>
      </div>
    </div>
  }
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewSettParams extends React.Component<IViewSettingsProps>{
  state={
    hiddenClasses:[],
    searchInput:'',
  }
  renderClassList=(ClassData)=>{
    let WorkAreaMode=this.props.ApplicationStore.WorkAreaMode;
    let listParams=ClassData.params.map((item)=>{
      let isSelected_param=WorkAreaMode.mode==='viewParam' && WorkAreaMode.data.id===item.id?'selected':'';
      let isNewAddParam=!item.uid?<span className="isNewParam"><i className="fas fa-plus-circle"></i></span>:null;
      return <li key={'param_list_'+item.id} className={'params__paramitem paramitem '+isSelected_param}>
          <div className="paramitem__title" onClick={()=>{this.setWorkAreaMode('viewParam', item)}}>{item.name}{isNewAddParam}</div>
          <div className="paramitem__action"><a onClick={()=>{this.clickRemoveParam(item.id)}} className="remove"><i className="fas fa-trash"></i></a></div>
      </li>
    });
    let isSelected_class=WorkAreaMode.mode==='viewClass' && WorkAreaMode.data.uid===ClassData.uid?'selected':'';
    return <div key={'class_list_'+ClassData.uid} className={'classes__classitem classitem '+isSelected_class}>
      <div className="classitem__name name">
          <div onClick={()=>{this.clickHideClass(ClassData.uid)}} className="name__arrow"><i className={'fa nested-list-icon '+(this.state.hiddenClasses.indexOf(ClassData.uid)===-1?'fa-caret-down':'fa-caret-right')}></i></div>
          <div onClick={()=>{this.setWorkAreaMode('viewClass', ClassData)}} className="name__title">{ClassData.name}</div>
          <div className="name__actionbtn">
            <a onClick={()=>{this.setWorkAreaMode('viewAddParams', ClassData)}} className="plus"><i className="fas fa-plus-circle"></i></a>
            <a onClick={()=>{this.cliskRemoveClass(ClassData)}} className="remove"><i className="fas fa-trash"></i></a>
          </div>
      </div>
      <ul className={'classitem__params params '+(this.state.hiddenClasses.indexOf(ClassData.uid)===-1?'':'hide')}>
        {listParams}
      </ul>
    </div>
  }
  renderParamsList=()=>{
    let struc_param_data={};
    this.props.attrData.forEach((item:any)=>{
      if(!struc_param_data[item.class.uid]){
        struc_param_data[item.class.uid]={
          name:item.class.name,
          uid:item.class.uid,
          params:[],
        };
      }
      //Строка поиска
      if(item.name.toLowerCase().includes(this.state.searchInput.toLowerCase())){
        struc_param_data[item.class.uid].params.push(item);
      }
    })
    let classes_list= [], classes_uid=Object.keys(struc_param_data);
    classes_list=classes_uid.map((key)=>{
        return this.renderClassList(struc_param_data[key]);
    });
    return <div className="params__classes classes">{classes_list}</div>
  }

  clickHideClass=(uid:string)=>{
    let new_hide_class=this.state.hiddenClasses;
    if(this.state.hiddenClasses.indexOf(uid)===-1){
      new_hide_class.push(uid);
    }else{
      new_hide_class=this.state.hiddenClasses.filter((item:any)=>{
        return item===uid?false:true;
      });
    }
    this.setState({hiddenClasses:new_hide_class});
  }
  clickRemoveParam=(id:number)=>{
    this.props.ApplicationStore.RemoveParamData(id, this.props.viewUid);
  }
  cliskRemoveClass=(class_data:{params:[], uid:string})=>{
    let ids=class_data.params.map((item:any)=>{
      return item.id;
    })
    this.props.ApplicationStore.RemoveArrParamData(ids, this.props.viewUid);
  }
  findParams=(e:any)=>{
    let {value} = e.currentTarget;
    this.setState({searchInput:value});
  }

  setWorkAreaMode=(mode, data)=>{
    //mode=['viewParam', 'viewClass'];
    let mode_data={
      mode:mode,
      data:data,
    }
    this.props.ApplicationStore.SetWorkAreaMode(mode_data);
  }


  render(){
    return <div className="workarea__params params">
      <div className="params__addbtn">
        <button onClick={()=>{this.setWorkAreaMode('viewAddParams', null)}} className="btn btn-info btn-sm"><i className="fa fa-plus" aria-hidden="true"></i> Add Params</button>
      </div>
      <div className="params__searchctrl">
        <label><i className="fa fa-search" aria-hidden="true"></i><input value={this.state.searchInput} onChange={this.findParams} type="text" placeholder="SEARCHPARAM"/></label>
      </div>
      {this.renderParamsList()}
    </div>
  }
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewSettWorkArea extends React.Component<IViewSettingsProps>{
  render(){
    let WorkAreaMode=this.props.ApplicationStore.WorkAreaMode;
    let Content;
    if(WorkAreaMode.mode==='viewTable'){
      Content=<ViewTable viewTitle={this.props.viewTitle} attrData={this.props.attrData} viewUid={this.props.viewUid}/>;
    }else if(WorkAreaMode.mode==='viewClass'){
      let attr_data=this.props.attrData.filter((item:any)=>{
        if(item.class && item.class.uid && item.class.uid===WorkAreaMode.data.uid){
          return true;
        }
        return false;
      });
      Content=<ViewClass classUid={WorkAreaMode.data.uid} classTitle={WorkAreaMode.data.name} attrData={attr_data} viewUid={this.props.viewUid}/>;
    }else if(WorkAreaMode.mode==='viewParam'){
      let param_data=this.props.attrData.find((item:any)=>{
        return item.id===WorkAreaMode.data.id?true:false;
      })
      Content=<ViewParam attrData={param_data} viewUid={this.props.viewUid}/>;
    }
    else if(WorkAreaMode.mode==='viewAddParams'){
        Content=<ViewAddParams classData={WorkAreaMode.data} attrData={this.props.attrData} viewUid={this.props.viewUid}/>;
    }
    return <div className="workarea__settings settings">
      {Content}
    </div>
  }
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewTable extends React.Component<IViewSettingsProps>{
  state={
    tabs:'params',
  }

  clickTabs=(id)=>{
    this.setState({tabs:id});
  }

  renderTabs=()=>{
    let type_of_view= this.props.ApplicationStore.AppViewSett.settings && this.props.ApplicationStore.AppViewSett.settings.table_or_form? this.props.ApplicationStore.AppViewSett.settings.table_or_form:0;
    let tabs=[];
    if(type_of_view==0){
      tabs=[
        {name:'Params', id:'params'},
        {name:'Class Link', id:'links'},
        {name:'Filters', id:'view_filters'},
        {name:'Charts', id:'chart'},
        {name:'Calculated block', id:'calc_block'}
      ];
  }else if(type_of_view==1){
    tabs=[
      {name:'Params', id:'params'},
      {name:'Class Link', id:'links'},
    ];
  }else if(type_of_view==3){
    tabs=[
      {name:'Params', id:'params'},
      {name:'Calendar', id:'calendar'},
    ];
  }
    tabs.push({name:'Settings', id:'settings'});
    let li_items=tabs.map((item)=>{
        return <li key={'tabs_'+item.id} onClick={()=>{this.clickTabs(item.id)}} className={'tabs__item item '+(this.state.tabs==item.id?'selected':'')}>{item.name}</li>
    });
    return <ul className="viewtable__tabs tabs">
      {li_items}
    </ul>
  }
  renderBody=()=>{
    let Content;
    if(this.state.tabs==='links'){
      let DataFunction={
        onChange:(e:any, uid:string)=>{
          let {value} = e.currentTarget
          let AppViewSett=this.props.ApplicationStore.AppViewSett;
          let chosenClass=AppViewSett.ClasseLinks.find((item)=>{
            return item.DependentClass.uid==value?true:false;
          });
          if(chosenClass){
            chosenClass={name:chosenClass.DependentClass.name,uid:chosenClass.DependentClass.uid};
          }else{
            chosenClass={name:'',uid:value};
          }
          AppViewSett.ClasseLinks= AppViewSett.ClasseLinks.map((item)=>{
              if(item.DependentClass.uid==uid){
                item.MainClass=chosenClass;
              }
              return item;
          });

          this.props.ApplicationStore.setDataAppViewSett(AppViewSett);
        },
        onClassclick:(class_data:any)=>{
          this.props.ApplicationStore.SetWorkAreaMode({
            mode:'viewClass',
            data:{
              uid:class_data.uid,
              name:class_data.name,
            },
          });
        },
      }
      let classesUid=this.props.ApplicationStore.AppViewSett.ClasseLinks;
      Content= <ClassRelation viewUid={this.props.viewUid} DataFunction={DataFunction}  Classes={classesUid} />
    }
    else if(this.state.tabs==='settings'){
      let AppViewSett=this.props.ApplicationStore.AppViewSett;
      let DataFunction={
        onChange:(data:object)=>{
          this.props.ApplicationStore.setDataAppViewSett(data);
        }
      }
      Content= <ViewDataSettings viewUid={this.props.viewUid} DataFunction={DataFunction}  Settings={AppViewSett} />
    }else if(this.state.tabs==='params'){
      let data_table=this.props.attrData.map((item:any)=>{
        return {
            name: item.name,
            data_type: item.datatype,
            class: item.class.name,
            trCustomData: item,
        };
      })
      let type_of_view= this.props.ApplicationStore.AppViewSett.settings && this.props.ApplicationStore.AppViewSett.settings.table_or_form? this.props.ApplicationStore.AppViewSett.settings.table_or_form:0;
      Content= 	<PrimeTable
                    type_of_view={type_of_view}
                    mode={'view'}
                    changeView={(data)=>{
                      this.props.ApplicationStore.SetWorkAreaMode({
                      mode:'viewParam',
                      data:data.trCustomData,
                    })}}
                    sortParam={(data)=>{
                      this.props.ApplicationStore.sortParamData(data);
                    }}
                    viewUid={this.props.viewUid}
                    attrData={data_table}/>;
    }else if(this.state.tabs==='table_costructor'){
      Content='Coming soon';
    }else if(this.state.tabs==='form_costructor'){
      Content='Coming soon';
    }else if(this.state.tabs==='view_filters'){
      Content=<ViewFilters />
    }else if(this.state.tabs==='chart'){
      let AppViewSett=this.props.ApplicationStore.AppViewSett;
      Content=<ViewChartsSett class_count={AppViewSett.ClasseLinks.length} AppViewSett={AppViewSett} viewUid={this.props.viewUid} />
    }else if(this.state.tabs==='calc_block'){
      let AppViewSett=this.props.ApplicationStore.AppViewSett;
      Content=<ViewCalcBlockSett class_count={AppViewSett.ClasseLinks.length} AppViewSett={AppViewSett} viewUid={this.props.viewUid} />
    }
    else if(this.state.tabs==='calendar'){
      let AppViewSett=this.props.ApplicationStore.AppViewSett;
      Content=<ViewCalendarSett AppViewSett={AppViewSett} viewUid={this.props.viewUid} />
    }
    return <div key={'table_params'+this.props.viewUid} className="viewtable__body body">
      {Content}
    </div>
  }

  render(){
    return <div className="viewtable">
      {this.renderTabs()}
      {this.renderBody()}
    </div>
  }
}

export interface IPrimeTableProps {
    attrData:{
        name: string,
        data_type: string,
        class: string,
        trCustomData: any,
    }[],
    viewUid:string,
    type_of_view:number,
    mode:string,
    changeView:(data:{
        name: string,
        data_type: string,
        class: string,
        trCustomData: object,
    })=>void,
    sortParam:(data)=>void,
}
export interface IPrimeTableSate{
  data:{
      name: string,
      data_type: string,
      class: string,
      trCustomData: any,
  }[],
  lazyTotalRecords:number,
  selected:string,
  loading:boolean,
  selectedColumns:{field: string, header: string}[],
  allColumns:{field: string, header: string}[],
  isSortedParams:boolean,
}
class PrimeTable extends React.Component<IPrimeTableProps, IPrimeTableSate>{
  constructor(props) {
      super(props);
      this.state = {
          data: [],
          lazyTotalRecords: 0,
          selected:null,
          loading:false,
          selectedColumns:[],
          allColumns:[],
          isSortedParams:false,
      }
  }
  componentDidMount() {
      this.setState({data:[], loading:true}, ()=>{
        setTimeout(()=>{
            let allColumns=[
              {field: 'data_type', header: 'Data type'},
              {field: 'class', header: 'Class'},
              {field: 'alias', header: 'Alias'},
              {field: 'view', header: 'View'},
              {field: 'data_source', header: 'Data Source'},
              {field: 'color', header: 'Color'},
              {field: 'width', header: 'Width'},
              {field: 'fixed', header: 'isFixed'},
              {field: 'hide', header: 'isHide'}
            ];
            if(this.props.type_of_view==1){
              allColumns=[
                {field: 'data_type', header: 'Data type'},
                {field: 'class', header: 'Class'},
                {field: 'alias', header: 'Alias'},
                {field: 'hide', header: 'isHide'}
              ];
            }
            if(this.props.type_of_view==3){
              allColumns=[
                {field: 'data_type', header: 'Data type'},
                {field: 'class', header: 'Class'},
                {field: 'alias', header: 'Alias'},
                {field: 'hide', header: 'isHide'}
              ];
            }
            let selectedColumns=allColumns;
            this.setState({data:this.props.attrData, loading:false, selectedColumns:selectedColumns, allColumns:allColumns})
        }, 100)

      });
  }
  componentDidUpdate(prevProps){
    /*let sort_prev=prevProps.attrData.map((item)=>{return item.trCustomData.uid}).join(',');
    let sort_cur=this.props.attrData.map((item)=>{return item.trCustomData.uid}).join(',');
    let changeSort=sort_prev!=sort_cur?true:false;*/
    let oldIDs=prevProps.attrData.map((item)=>{
      return item.trCustomData.id;
    });
    let newIDs=this.props.attrData.map((item)=>{
      return item.trCustomData.id;
    });
    if(prevProps.attrData.length!=this.props.attrData.length || JSON.stringify(oldIDs)!=JSON.stringify(newIDs)){
      this.setState({data:this.props.attrData})
    }
  }

  tablechange=(NewData, column)=>{
    let updatedData = [...this.state.data];
    updatedData[column.rowIndex].trCustomData = NewData;
    this.setState({data:updatedData});
  }
  onColumnToggle=(e)=>{
    let selectedColumns = e.value;
    let orderedSelectedColumns = this.state.allColumns.filter(col => selectedColumns.some(sCol => sCol.field === col.field));
    this.setState({ selectedColumns: orderedSelectedColumns });
  }
  onRowReorder=(e)=>{
    this.setState({data:e.value}, ()=>{
      let data= this.state.data.map((item)=>{
        return item.trCustomData.id;
      })
      this.props.sortParam(data);
    })
  }

  RenderTD = (row_data, column) => {
    if(column.field==='view'){
     return <span className="link_edit_td table_text">{(row_data.trCustomData.dopSett.view?row_data.trCustomData.dopSett.view.name:'')}</span>;
    }else if(column.field==='filter'){
      //unused
    /*  let filterValue=row_data.trCustomData.Filter && row_data.trCustomData.Filter.length?row_data.trCustomData.Filter[0]:{op:'=',v:''};
      return <span className="link_edit_td table_text">{(filterValue.v?filterValue.op+filterValue.v:'')}</span>;*/
    }else if(column.field==='color'){

      return <span className="link_edit_td table_text">{(row_data.trCustomData.dopSett.calc_color?row_data.trCustomData.dopSett.calc_color.name:'')}</span>;
    //  return <ColorCalcCtrl Data={row_data.trCustomData} />
    }else if(column.field==='hide'){
      return <DisplayCtrl tablechange={(Data)=>{this.tablechange(Data, column)}} Data={row_data.trCustomData} />;
    }
    else if(column.field==='width'){
      return <span className="link_edit_td table_text">{(row_data.trCustomData.dopSett.width?row_data.trCustomData.dopSett.width.value+' '+row_data.trCustomData.dopSett.width.unit:'')}</span>;
    }
    else if(column.field==='fixed'){
      return <span className="link_edit_td table_text">{(row_data.trCustomData.dopSett.isFixed==1?'Fixed':'')}</span>;
    }
    else if(column.field==='alias'){
      return <span className="link_edit_td table_text">{(row_data.trCustomData.dopSett.alias?row_data.trCustomData.dopSett.alias:'')}</span>;
    }else if(column.field==='data_source'){
      return <span className="link_edit_td table_text">{(row_data.trCustomData.dopSett.data_source?row_data.trCustomData.dopSett.data_source.name:'')}</span>;
    }
    return <span className="link_edit_td table_text">{row_data[column.field]}</span>
  };

  renderHeader=()=>{

    return(<div className="prime_header">
              <div className='left'>
                <MultiSelect
                value={this.state.selectedColumns}
                options={this.state.allColumns}
                optionLabel="header"
                onChange={this.onColumnToggle}
                style={{width:'200px'}}
                selectedItemsLabel={'{0} columns'}
                 />
              </div>
              <div className='right'>
                {this.props.mode=='view' && <div className="switch_cont">
                  <i style={{marginRight:5}} className="fas fa-sort-alpha-down" aria-hidden="true"></i>
                  <IOSSwitch checked={this.state.isSortedParams}
                             onChange={(e)=>{
                                 this.setState({isSortedParams:!this.state.isSortedParams})
                             }} />
                </div>}
              </div>
            </div>);
  }

  render() {
    const column_key={
      data_type:<Column key={'data_type'} style={{width:150}} field="data_type" body={this.RenderTD} header="Data type"></Column>,
      class:<Column key={'class'} style={{width:150}} field="class" body={this.RenderTD} header="Class"></Column>,
      view:<Column key={'view'} editor={(column) => {return <ChoseViewCtrl tablechange={(Data)=>{this.tablechange(Data, column)}} Data={column.rowData.trCustomData}/>}} style={{width:200}} field="view" body={this.RenderTD} header="View"></Column>,
      color:<Column key={'color'} editor={(column) => {return <ColorCalcCtrl tablechange={(Data)=>{this.tablechange(Data, column)}} Data={column.rowData.trCustomData}/>}} style={{width:200}} field="color" body={this.RenderTD} header="Color"></Column>,
      hide:<Column key={'hide'} style={{width:70}} field="hide" body={this.RenderTD} header="isHide"></Column>,
      width:<Column key={'width'} editor={(column) => {return <ColumnWidthCtrl tablechange={(Data)=>{this.tablechange(Data, column)}} Data={column.rowData.trCustomData}/>}} style={{width:200}} field="width" body={this.RenderTD} header="Width"></Column>,
      fixed:<Column key={'fixed'} editor={(column) => {return <FixedColumnCtrl tablechange={(Data)=>{this.tablechange(Data, column)}} Data={column.rowData.trCustomData}/>}} style={{width:200}} field="fixed" body={this.RenderTD} header="IsFixed"></Column>,
      alias:<Column key={'alias'} editor={(column) => {return <AliasCtrl tablechange={(Data)=>{this.tablechange(Data, column)}} Data={column.rowData.trCustomData}/>}} style={{width:200}} field="alias" body={this.RenderTD} header="Alias"></Column>,
      data_source:<Column key={'data_source'} editor={(column) => {return <DataSourceCtrl tablechange={(Data)=>{this.tablechange(Data, column)}} Data={column.rowData.trCustomData}/>}} style={{width:200}} field="data_source" body={this.RenderTD} header="Data Source"></Column>
    };

    let ColumnSett=this.state.selectedColumns.map((item)=>{
      return column_key[item.field];
    })

    const header =this.renderHeader();

    return(
      <DataTable
      value={this.state.data}
      scrollable
      lazy
      scrollHeight="100%"
      loading={this.state.loading}
      selection={this.state.selected}
      onSelectionChange={e => this.setState({ selected: e.value })}
      selectionMode="single"
      resizableColumns
      columnResizeMode="fit"
      editMode="cell"
      onRowDoubleClick={(e)=>{if (e.originalEvent.target instanceof HTMLInputElement){return false; }this.props.changeView(e.data)}}
      header={header}
      reorderableColumns
      onRowReorder={this.onRowReorder}
    >
            {this.state.isSortedParams && <Column className={'order_td'} rowReorder style={{cursor: 'move', textAlign:'center',maxWidth:45, width: 45}} />}
            <Column style={{width:150}} field="name" body={this.RenderTD} header="Name"></Column>
            {ColumnSett}
      </DataTable>
    );
  }

}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewClass extends React.Component<IViewSettingsPropsClass>{
  clickBack=(e)=>{
    this.props.ApplicationStore.SetWorkAreaMode({
      mode:'viewTable',
      data:this.props.viewUid,
    });
  }
  render(){
    let data_table=this.props.attrData.map((item:any)=>{
      return {
          name: item.name,
          data_type: item.datatype,
          class: item.class.name,
          trCustomData: item,
      };
    })
    let type_of_view= this.props.ApplicationStore.AppViewSett.settings && this.props.ApplicationStore.AppViewSett.settings.table_or_form? this.props.ApplicationStore.AppViewSett.settings.table_or_form:0;
    return <div key={'vie_class_uid'+this.props.classUid} className="viewclass">
        <div className="viewclass__header headr">
            <div className="headr__title"><i onClick={this.clickBack} className="fas fa-undo-alt back"></i>{this.props.classTitle}</div>
            <div className="headr__info"><span className="info__params">Params: <span className="count">{this.props.attrData.length}</span></span></div>
        </div>
        <div className="viewclass__table table">
        <PrimeTable
              type_of_view={type_of_view}
              mode={'class'}
              changeView={(data)=>{
                this.props.ApplicationStore.SetWorkAreaMode({
                mode:'viewParam',
                data:data.trCustomData,
              })}}
              sortParam={(data)=>{
                this.props.ApplicationStore.sortParamData(data);
              }}
              viewUid={this.props.viewUid}
              attrData={data_table}/>
        </div>
    </div>
  }
}


@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewParam extends React.Component<IViewSettingsPropsParam>{
  clickBack=(e)=>{
    this.props.ApplicationStore.SetWorkAreaMode({
      mode:'viewTable',
      data:this.props.viewUid,
    });
  }
  render(){
    let view_settings=this.props.ApplicationStore.AppViewSett;
    return <div key={'vie_param_uid'+this.props.attrData.id} className="viewparam">
        <div className="viewparam__header headr">
            <div className="headr__title"><i onClick={this.clickBack} className="fas fa-undo-alt back"></i>{this.props.attrData.class.name+'.'+this.props.attrData.name}</div>
            <div className="headr__info">
              <span className="info__class">Class: <span className="count">{this.props.attrData.class.name}</span></span>
              <span className="info__dataType">DataType: <span className="count">{this.props.attrData.datatype}</span></span>
            </div>
        </div>
        <ViewParamSetting view_settings={view_settings} DataFunction={null} paramUid={this.props.attrData.uid} Settings={this.props.attrData} />
    </div>
  }
}


@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewAddParams extends React.Component<IViewSettingsPropsAddParam>{
  state={
    chosen_params:[],
    datatype:'text',
    class:null,
    fixedClassData:null,
    inputValue:'',
    showClassSelect:false,
    valid_error:[],
  }

  componentDidMount(){
    if(this.props.classData){
      const AllParams=this.props.ApplicationStore.ParamFullData;
      let fixedClassData=AllParams.filter((item)=>{
        return item.class.uid==this.props.classData.uid?true:false;
      });
      this.setState({class:this.props.classData, fixedClassData:fixedClassData});
    }
  }
  componentDidUpdate(prevProps){
    if((!prevProps.classData && this.props.classData) ||
      (prevProps.classData && !this.props.classData) ||
      (prevProps.classData && this.props.classData && prevProps.classData.uid!==this.props.classData.uid)
    ){
      let fixedClassData=null;
      if(this.props.classData){
        const AllParams=this.props.ApplicationStore.ParamFullData;
        fixedClassData=AllParams.filter((item)=>{
          return item.class.uid==this.props.classData.uid?true:false;
        });
      }
      this.setState({class:this.props.classData, chosen_params:[], inputValue:'', fixedClassData:fixedClassData});
    }
  }

  //Name
  changeName=(opt_data)=>{
    if(opt_data){
      let param, class_param=this.state.class;
      if(opt_data.length && opt_data.length>1){
        opt_data =opt_data.filter((item)=>{
          return item.uid?true:false;
        })
        class_param=opt_data[0] && opt_data[0].class?opt_data[0].class:null;
      }else if(opt_data.length && opt_data.length==1 && opt_data[0].uid){
        param=this.props.ApplicationStore.ParamFullData.find((item)=>{
          return item.uid==opt_data[0].uid?true:false;
        });

      }
      if(param){
        this.setState({chosen_params:opt_data, class:param.class, datatype:param.datatype});
      }else{
        this.setState({chosen_params:opt_data, class:class_param})
      }
    }else{
      this.setState({chosen_params:[]});
    }
  }

  //DateType
  changeDataType=(e)=>{
    let {value}=e.currentTarget;
    this.setState({datatype:value});
  }

  //Class
  focusHandler=(e)=>{
    if(this.state.fixedClassData || (this.state.chosen_params && this.state.chosen_params.length>1)){
      return false;
    }
    this.setState({showClassSelect:true});
  }
  blurHandler=(e)=>{
    if(this.state.fixedClassData || (this.state.chosen_params && this.state.chosen_params.length>1)){
      return false;
    }
    var el=e.relatedTarget, parentsClasses=[];
    while(el){
      parentsClasses.push(el.className);
      el=el.parentNode;
    }
    if(parentsClasses.indexOf('ClassChoseTree')!==-1){
      return false;
    }
    this.setState({showClassSelect:false});
  }
  removeClass=(e)=>{
    if(this.state.fixedClassData || (this.state.chosen_params && this.state.chosen_params.length>1)){
      return false;
    }
    this.setState({class:null});
  }

  addParam=(e)=>{
      if(!this.state.chosen_params || !this.state.chosen_params.length || !this.state.class){
        return false;
      }
      const AllParams=this.props.ApplicationStore.ParamFullData;
      let newParams =this.state.chosen_params.map((item)=>{
        let param_obj={
          id:Math.floor(Math.random() * 10000000000),
          uid:item.uid?item.uid:null,
          name:item.label,
          datatype:this.state.datatype,
          class:{name:this.state.class.name, uid:this.state.class.uid},
          dopSett:{
            calc_color:null,
            Hide:0,
            view:null,
            Filter:[],
          }
        };
        if(item.uid){
          let param=AllParams.find((item_all)=>{
            return item_all.uid==item.uid?true:false;
          })
          if(param){
            param_obj.class=param.class;
            param_obj.datatype=param.datatype;
          }
        }
        return param_obj;
      })
      this.props.ApplicationStore.AddAppArrParamData(newParams, false);
      this.setState({
        chosen_params:[],
        datatype:'text',
        class:this.props.classData,
        inputValue:'',
      });
  }

  renderNameCtrl=()=>{
    const getOptions=()=>{
      let AllParams=[];
      AllParams=this.props.ApplicationStore.ParamFullData;
      let existParams=this.props.attrData.map((item:any)=>{
        return item.uid;
      })
      let opt=AllParams.filter((item:any)=>{
        return existParams.indexOf(item.uid)==-1?true:false;
      }).map((item)=>{
          return {label:item.name, value:item.uid, uid:item.uid, class:item.class}
      });
      if(this.state.class){
        opt=opt.filter((item)=>{
          return item.class.uid==this.state.class.uid?true:false;
        })
      }
      if(this.state.class || (this.state.inputValue && this.state.inputValue.length>=2)){
        if(this.state.inputValue){
          opt =opt.filter((item)=>{
            return item.label.toLowerCase().includes(this.state.inputValue.toLowerCase());
          })
        }
        return opt;
      }

      return [];
    }
    let data_opt=getOptions();
    return <div className="form-group">
            <label>Name</label>
            <CreatableSelect
              components={{
                Option: (data) =>{
                  let {children, ...props } =data;
                  let class_span=data.data.class?<span className="class_name" title={data.data.class.name}>{data.data.class.name}</span>:null;
                  return  <components.Option {...props}>
                            {children}
                            {class_span}
                        </components.Option>
                },
              }}
              options={data_opt}
              placeholder={'Enter Name'}
              autoFocus={true}
              isMulti={true}
              value={this.state.chosen_params}
              createOptionPosition={'first'}
              isValidNewOption={(inputValue)=>{
                return inputValue && this.state.chosen_params && this.state.chosen_params.length==0? true:false;
              }}
              onInputChange={(inputValue, {action})=>{
                if (action !== "set-value") this.setState({inputValue: inputValue});
              }}
              blurInputOnSelect={true}
              inputValue={this.state.inputValue}
              noOptionsMessage={(data) => null}
              onChange={this.changeName}
            />
          </div>
  }
  renderDataTypeCtrl=()=>{
    const DataType=[
      {name:'text', value:'text'},
      {name:'number', value:'number'},
      {name:'datetime', value:'datetime'},
      {name:'object', value:'object'},
      {name:'file', value:'file'},
      {name:'counter', value:'counter'},
    ];
    const Options= DataType.map((item, index)=>{
       return  <option key={'datatypeapp_'+index} value={item.value}>{item.name}</option>
    });
    let isDisabled=false;
    if(this.state.chosen_params && this.state.chosen_params.length && this.state.chosen_params.length==1){
      isDisabled=this.state.chosen_params[0].uid?true:false;
    }
    return <div className={'form-group '+(this.state.chosen_params && this.state.chosen_params.length>1?'hide':'')}>
            <label>Data type</label>
            <select disabled={isDisabled} onChange={this.changeDataType} className="form-control" value={this.state.datatype}>
            {Options}
            </select>
          </div>
  }
  renderClassCtrl=()=>{
    let value=this.state.class?this.state.class.name:'';
    return <div className="form-group">
            <label>Class</label>
            <div tabIndex={0} onFocus={this.focusHandler} onBlur={this.blurHandler} className={'form-control classSelectCtrl'+(this.state.fixedClassData || (this.state.chosen_params && this.state.chosen_params.length>1)?' disabled':'')}>
              {value}
              {this.state.class && <a onClick={this.removeClass} className="remove_class_ctrl"><i className="fa fa-times"></i></a>}
            </div>
          </div>
  }


  clickBack=(e)=>{
    this.props.ApplicationStore.SetWorkAreaMode({
      mode:'viewTable',
      data:this.props.viewUid,
    });
  }
  render(){
    let key=this.props.classData?'view_add_uid'+this.props.classData.uid+this.props.attrData.length:'view_add_uid'+this.props.viewUid+this.props.attrData.length;
    let isExist=false;
    if(this.state.chosen_params && this.state.chosen_params.length){
      this.state.chosen_params.forEach((param)=>{
        if(param.uid){
          isExist=true;
        }
      });
    }
    let isNewparamIcon=<span className="isNewParam"><i className={'fas '+(isExist?'fa-check-circle':'fa-plus-circle')}></i></span>;
    let className=this.props.classData?'('+this.props.classData.name+')':null;
    let ClassFunction={
      onBlur:()=>{
        this.setState({showClassSelect:false});
      },
      onChose:(data)=>{
      /*  let AllParams=this.state.fixedClassData || this.props.ApplicationStore.ParamFullData;
        let param=AllParams.find((item)=>{
          return item.class.uid==data.uid && this.state.name==item.name?true:false;
        })
        if(param){
          this.setState({showClassSelect:false, name:param.name, uid:param.uid, datatype:param.datatype, class:param.class});
        }else{

        }*/
        this.setState({showClassSelect:false, chosen_params:[], class:data});

      },
    };
    return <div key={key} className="viewaddparam">
      <div className="viewaddparam__header headr">
            <div className="headr__title">
                <i onClick={this.clickBack} className="fas fa-undo-alt back"></i>Adding new params {className} {isNewparamIcon}
            </div>
      </div>
      <div className="viewaddparam__body body">
        <div className="form-wrapper">
          {this.renderNameCtrl()}
          {this.renderDataTypeCtrl()}
          {this.renderClassCtrl()}
          <div className="btn__cont"><button onClick={this.addParam} className="btn btn-info">Add <i className="fa fa-plus"></i></button></div>
        </div>
        {this.state.showClassSelect && <ChoseClass classFunction={ClassFunction} />}
      </div>
    </div>
  }
}


interface IViewFiltersProps {
  ApplicationStore?:any,
  ToastrStore?:any,
}
interface IViewFiltersState {
  tabs:string,
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewFilters extends React.Component<IViewFiltersProps, IViewFiltersState>{
  constructor(props){
    super(props);
    this.state={
      tabs:'filters',
    };
  }

  clickTabs=(tabs)=>{
    this.setState({tabs:tabs});
  }

  changeParamKK=(value)=>{
    let AppParamData=this.props.ApplicationStore.AppViewSett.attrs;
    let AppViewSett=this.props.ApplicationStore.AppViewSett;
    let chosen_param=AppParamData.find((item)=>{
      return item.uid==value?true:false;
    });
    if(chosen_param){
        AppViewSett.settings.HatAttr=chosen_param.uid;
        AppViewSett.settings.HatClassAttr=chosen_param.class.uid;
    }else{
      AppViewSett.settings.HatAttr=null;
      AppViewSett.settings.HatClassAttr=null;
    }
    this.props.ApplicationStore.setDataAppViewSett(AppViewSett);
  }
  renderKKParamsSet=(AppViewSett, AppParamData)=>{
    let Options=AppParamData.filter((item)=>{
      return item.isArray || item.datatype=='file'?false:true;
    }).map((item)=>{
      return {value:item.uid , label:item.class.name+'.'+item.name};
    });
    Options=[{value:null, label:'-Chose param-'}, ...Options];

    let chosen_param=AppViewSett.settings && AppViewSett.settings.HatAttr?AppViewSett.settings.HatAttr:null;

    let chosen_value=Options.find((item)=>{
      return  chosen_param && chosen_param==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }
    return(<div className="settings__setcont setcont">
    <div className="row"><label className="col-lg-12">Pivot table by column</label></div>
    <div className="param_value">
      <Select
          menuPosition={'fixed'}
          options={Options}
          value={chosen_value}
          onChange={(opt_data)=>{this.changeParamKK(opt_data.value);}}
        />
      </div>
    </div>);
  }

  render(){
    let tabs=[
      {title:'Filters', tabs:'filters'},
      {title:'Single line mode', tabs:'single_mode'},
    ].map((item)=>{
      return <li onClick={()=>{this.clickTabs(item.tabs)}} className={'tabs__item item '+(item.tabs===this.state.tabs?'selected':'')} key={'sett_key_'+item.tabs}>{item.title}</li>;
    })
    let Content=null;
    let AppViewSett=this.props.ApplicationStore.AppViewSett;
    let AppParamData=this.props.ApplicationStore.AppViewSett.attrs;
    let DataFunction={
      onChange:(data:object)=>{
        this.props.ApplicationStore.setDataAppViewSett(data);
      }
    }
  /*  if(this.state.tabs=='filters'){
      Content=<FilterBlock DataFunction={DataFunction} AppViewSett={AppViewSett} AppParamData={AppParamData}/>
    }else if(this.state.tabs=='single_mode'){
      Content=<SingleModeBlock DataFunction={DataFunction} AppViewSett={AppViewSett} AppParamData={AppParamData}/>
    }*/
    return(
      <div className="viewfilters">
        <div className="viewfilters__body">
          <SingleModeBlock DataFunction={DataFunction} AppViewSett={AppViewSett} AppParamData={AppParamData}/>
          {this.renderKKParamsSet(AppViewSett, AppParamData)}
          <FilterBlock DataFunction={DataFunction} AppViewSett={AppViewSett} AppParamData={AppParamData}/>
        </div>
      </div>
    );
  }

}

interface IViewChartsSettProps {
  ApplicationStore?:any,
  ToastrStore?:any,
  class_count:number,
  viewUid:string,
  AppViewSett:any,

}
interface IViewChartsSettState {
  calcData:{uid:string, name:string, class_uid:string}[],
  chartData:{uid:string, name:string, class_uid:string, id?:number}[]
}
@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewChartsSett extends React.Component<IViewChartsSettProps, IViewChartsSettState>{
  constructor(props){
    super(props);
    this.state={
      calcData:[],
      chartData:[],
    };
  }
  componentDidMount(){
    let chartData=this.props.AppViewSett && this.props.AppViewSett.settings &&  this.props.AppViewSett.settings.charts?this.props.AppViewSett.settings.charts:[];
    chartData=chartData.map((item)=>{
      item.id=Math.floor(Math.random() * 10000000000);
      return item;
    })
    this.setState({chartData:chartData}, ()=>{
      this.loadCalc();
    })
  }
  componentDidUpdate(prevProps, prevSate){
    if(this.props.viewUid!=prevProps.viewUid || this.props.class_count!=prevProps.class_count){
      let chartData=this.props.AppViewSett && this.props.AppViewSett.settings &&  this.props.AppViewSett.settings.charts?this.props.AppViewSett.settings.charts:[];
      chartData=chartData.map((item)=>{
        item.id=Math.floor(Math.random() * 10000000000);
        return item;
      })
      this.setState({chartData:chartData}, ()=>{
        this.loadCalc();
      })
    }
  }

  loadCalc=()=>{
    let class_uids=this.props.AppViewSett.ClasseLinks.map((item)=>{
      return item.DependentClass.uid;
    });
    let classes_promise=class_uids.map((uid)=>{
      return ct_api_class.getCalcs(uid)
    });
    Promise.all(classes_promise).then((res)=>{
      let all_calc=[];
      res.forEach((res_i:any, i)=>{
        if(res_i.data.result==1){
          let data_calc=res_i.data.data.map((item)=>{
            item.class_uid=class_uids[i];
            return item;
          })
          all_calc=[...all_calc, ...res_i.data.data]
        }

      });
      this.setState({calcData:all_calc});
    });

  }
  renderCharts=()=>{
    if(this.state.chartData.length){
      return this.state.chartData.map((item, i)=>{
        return this.renderChartSelect(item, i);
      })
    }
    return null;
  }
  renderChartSelect=(data, i)=>{
      let options=this.state.calcData.map((item)=>{
        return {label:item.name, value:item.uid, class_uid:item.class_uid}
      });
      options=[{label:'-Chose Chart-', value:null, class_uid:null}, ...options]
      let chosen_value={label:data.name, value:data.uid, class_uid:data.class_uid};
      return(
        <div key={'chart_ctrl_'+i} className="chart_item">
          <div className="chart_select_ctrl">
              <Select
                  menuPosition={'fixed'}
                  options={options}
                  value={chosen_value}
                  onChange={(data_opt)=>{this.changeChart(data.id, data_opt)}}
                />
            </div>
            <button onClick={()=>{this.removeChart(data.id)}} className="btn btn-danger btn-sm"><i className="fa fa-times" aria-hidden="true"></i></button>
        </div>
      );
  }


  addChart=()=>{
    let chartData=this.state.chartData;
    chartData.push({id:Math.floor(Math.random() * 10000000000), name:'-Chose Chart-', uid:null, class_uid:null});
    this.setState({chartData:chartData});
  }
  changeChart=(id, data)=>{
    let chartData=this.state.chartData.map((item)=>{
      if(item.id==id){
        item.name=data.label;
        item.uid=data.value;
        item.class_uid=data.class_uid;
      }
      return item;
    });
    this.setState({chartData:chartData}, this.saveStore);
  }
  removeChart=(id)=>{
    let chartData=this.state.chartData.filter((item)=>{
      return item.id==id?false:true;
    });
    this.setState({chartData:chartData}, this.saveStore);
  }

  saveStore=()=>{
    let AppViewSett=this.props.AppViewSett;
    let chartData=this.state.chartData.filter((item)=>{
      return item.uid?true:false;
    }).map((item)=>{
      return {
        uid:item.uid,
        name:item.name,
        class_uid:item.class_uid,
      }
    });
    AppViewSett.settings.charts=chartData && chartData.length?chartData:null;
    this.props.ApplicationStore.setDataAppViewSett(AppViewSett);
  }


  render(){
    let Charts=this.renderCharts();
    return(
    <div className="viewcharts">
      <div className="viewcharts__body">
        <div className="settings__setcont setcont">
          <div className="row"><label className="col-lg-12">Charts</label></div>
          {Charts}
          <div className="chartctrl_btn"><button onClick={this.addChart} className="btn btn-sm" style={{color:'rgb(118, 102, 102)'}}><i className="fa fa-plus" aria-hidden="true"></i> Add Chart</button></div>
        </div>
      </div>
    </div>);
  }
}

interface IViewCalcBlockSettProps {
  ApplicationStore?:any,
  ToastrStore?:any,
  class_count:number,
  viewUid:string,
  AppViewSett:any,

}
interface IViewCalcBlockSettState {
  calcData:{uid:string, name:string, class_uid:string}[],
  calcBlockData:{uid:string, name:string, class_uid:string, id?:number}[]
}
@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewCalcBlockSett extends React.Component<IViewCalcBlockSettProps, IViewCalcBlockSettState>{
  constructor(props){
    super(props);
    this.state={
      calcData:[],
      calcBlockData:[],
    };
  }
  componentDidMount(){
    let calcBlockData=this.props.AppViewSett && this.props.AppViewSett.settings &&  this.props.AppViewSett.settings.block_calcs?this.props.AppViewSett.settings.block_calcs:[];
    calcBlockData=calcBlockData.map((item)=>{
      item.id=Math.floor(Math.random() * 10000000000);
      return item;
    })
    this.setState({calcBlockData:calcBlockData}, ()=>{
      this.loadCalc();
    })
  }
  componentDidUpdate(prevProps, prevSate){
    if(this.props.viewUid!=prevProps.viewUid || this.props.class_count!=prevProps.class_count){
      let calcBlockData=this.props.AppViewSett && this.props.AppViewSett.settings &&  this.props.AppViewSett.settings.block_calcs?this.props.AppViewSett.settings.block_calcs:[];
      calcBlockData=calcBlockData.map((item)=>{
        item.id=Math.floor(Math.random() * 10000000000);
        return item;
      })
      this.setState({calcBlockData:calcBlockData}, ()=>{
        this.loadCalc();
      })
    }
  }

  loadCalc=()=>{
    let class_uids=this.props.AppViewSett.ClasseLinks.map((item)=>{
      return item.DependentClass.uid;
    });
    let classes_promise=class_uids.map((uid)=>{
      return ct_api_class.getCalcs(uid)
    });
    Promise.all(classes_promise).then((res)=>{
      let all_calc=[];
      res.forEach((res_i:any, i)=>{
        if(res_i.data.result==1){
          let data_calc=res_i.data.data.map((item)=>{
            item.class_uid=class_uids[i];
            return item;
          })
          all_calc=[...all_calc, ...res_i.data.data]
        }

      });
      this.setState({calcData:all_calc});
    });

  }
  renderCalculations=()=>{
    if(this.state.calcBlockData.length){
      return this.state.calcBlockData.map((item, i)=>{
        return this.renderCalcSelect(item, i);
      })
    }
    return null;
  }
  renderCalcSelect=(data, i)=>{
      let options=this.state.calcData.map((item)=>{
        return {label:item.name, value:item.uid, class_uid:item.class_uid}
      });
      options=[{label:'-Chose Calc-', value:null, class_uid:null}, ...options]
      let chosen_value={label:data.name, value:data.uid, class_uid:data.class_uid};
      return(
        <div key={'chart_ctrl_'+i} className="calcblock_item">
          <div className="calcblock_select_ctrl">
              <Select
                  menuPosition={'fixed'}
                  options={options}
                  value={chosen_value}
                  onChange={(data_opt)=>{this.changeChart(data.id, data_opt)}}
                />
            </div>
            <button onClick={()=>{this.removeChart(data.id)}} className="btn btn-danger btn-sm"><i className="fa fa-times" aria-hidden="true"></i></button>
        </div>
      );
  }


  addCalc=()=>{
    let calcBlockData=this.state.calcBlockData;
    calcBlockData.push({id:Math.floor(Math.random() * 10000000000), name:'-Chose Calc-', uid:null, class_uid:null});
    this.setState({calcBlockData:calcBlockData});
  }
  changeChart=(id, data)=>{
    let calcBlockData=this.state.calcBlockData.map((item)=>{
      if(item.id==id){
        item.name=data.label;
        item.uid=data.value;
        item.class_uid=data.class_uid;
      }
      return item;
    });
    this.setState({calcBlockData:calcBlockData}, this.saveStore);
  }
  removeChart=(id)=>{
    let calcBlockData=this.state.calcBlockData.filter((item)=>{
      return item.id==id?false:true;
    });
    this.setState({calcBlockData:calcBlockData}, this.saveStore);
  }

  saveStore=()=>{
    let AppViewSett=this.props.AppViewSett;
    let calcBlockData=this.state.calcBlockData.filter((item)=>{
      return item.uid?true:false;
    }).map((item)=>{
      return {
        uid:item.uid,
        name:item.name,
        class_uid:item.class_uid,
      }
    });
    AppViewSett.settings.block_calcs=calcBlockData && calcBlockData.length?calcBlockData:null;
    this.props.ApplicationStore.setDataAppViewSett(AppViewSett);
  }


  render(){
    let Calculations=this.renderCalculations();
    return(
    <div className="viewcalcblock">
      <div className="viewcalcblock__body">
        <div className="settings__setcont setcont">
          <div className="row"><label className="col-lg-12">Сalculations</label></div>
          {Calculations}
          <div className="calcblockctrl_btn"><button onClick={this.addCalc} className="btn btn-sm" style={{color:'rgb(118, 102, 102)'}}><i className="fa fa-plus" aria-hidden="true"></i> Add Сalculation</button></div>
        </div>
      </div>
    </div>);
  }
}


interface IViewCalendarSettProps {
  ApplicationStore?:any,
  ToastrStore?:any,
  viewUid:string,
  AppViewSett:any,

}
interface IViewCalendarSettState {
  tab:string,
  view_data:{name:string, uid:string, app_uid:string, type_of_view:number}[],
  view_sett:any,
  user_group:{value:any, label:string}[],
}
@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewCalendarSett extends React.Component<IViewCalendarSettProps, IViewCalendarSettState>{

  constructor(props){
    super(props);
    this.state={
      tab:'event',
      view_data:this.getView(),
      view_sett:{},
      user_group:[],
    }
  }
  componentDidMount(){
    let parrent='00000000-0000-0000-0000-000000000000';
    let classGroupOfusers='00000000-0000-0000-0000-000000000002'
    ct_apiobj.getChildrenByType(parrent, classGroupOfusers).then((res)=>{
        if(res.data.result===1){
          let user_group= res.data.data.map((item)=>{
            return {
              value:item.uid,
              label:item.name,
            }
          });
          user_group=[{value:'-1', label:'-Chose group-'},...user_group]
          this.setState({user_group:user_group});
        }
    });

    let CalendarSett= this.props.AppViewSett && this.props.AppViewSett.settings && this.props.AppViewSett.settings.calendar_sett?this.props.AppViewSett.settings.calendar_sett:{};
    if(CalendarSett.tasks && CalendarSett.tasks.task_view){
      this.loadViewData(CalendarSett.tasks.task_view);
    }
  }

  loadViewData=(view_uid)=>{
      ct_api.getView(view_uid).then((res) => {
        if (res.data.result == 1) {
            let data_view = res.data.data;
            data_view.view.attrs = data_view.view.attrs.map((item) => {
                let data = {id: Math.floor(Math.random() * 10000000000), ...item};
                data.class = {name: data.parent.name, uid: data.parent.uid};
                return data;
            });
            let view_sett = this.state.view_sett;
            view_sett[view_uid]=data_view;
            this.setState({view_sett:view_sett});
        }

      });
  }

  getView=()=>{
    let treeData = this.props.ApplicationStore.treeData[0].children;
    let view_data=[];
    const loopRec= (data)=>{
       data.forEach((item)=>{
         if(item.isApp && item.children){
           item.children.forEach((view_item)=>{
             view_data.push({
               name:item.Name+'.'+view_item.Name,
               uid:view_item.iddb,
               app_uid:view_item.appUid,
               type_of_view:view_item.table_or_form,
             })
           });
         }
         if(!item.isApp && item.children){
           loopRec(item.children);
         }
       })
    }
    loopRec(JSON.parse(JSON.stringify(treeData)));
    console.log(view_data);
    return view_data;
  }


  clickOnTab=(id)=>{
    this.setState({tab:id});
  }

  changeSett=(id, value, key)=>{
    let AppViewSett = this.props.AppViewSett;
    let CalendarSett= this.props.AppViewSett && this.props.AppViewSett.settings && this.props.AppViewSett.settings.calendar_sett?this.props.AppViewSett.settings.calendar_sett:{};
    if(key){
      if(!CalendarSett[key]){
        CalendarSett[key]={};
      }
      CalendarSett[key][id]=value;
    }else{
      CalendarSett[id]=value;
    }
    AppViewSett.settings.calendar_sett=CalendarSett;
    this.props.ApplicationStore.setDataAppViewSett(AppViewSett);

  }




  renderTabs=()=>{
    let Tabs=[
      {label:'Events', id:'event'},
      {label:'Tasks', id:'task'},
    ];
    return(<ul className="viewcalendarblock__tabs tabs">
        {Tabs.map((item)=>{
            return <li onClick={(e)=>{this.clickOnTab(item.id)}}  key={'cal_tab_'+item.id} className={'tabs__item item '+(this.state.tab==item.id?'selected':'')}>{item.label}</li>
        })}
    </ul>)
  }
  renderBodyTab=(id)=>{
    let Content_body=null;
    switch (id) {
      case 'event':
        Content_body=this.renderEventSett();
        break;
      case 'task':
        Content_body=this.renderTasksSett();
        break;
    }
    return Content_body;
  }

  renderSettCtrl=(item, key)=>{
    let ctrl=<input className="form-control" value={item.value} id={item.id} onChange={(e)=>{
        let {id, value}=e.currentTarget;
        this.changeSett(id, value, key);
    }} />;
    if(item.type=='select'){
      ctrl=this.renderSelect(item.id, key, item.value, item.options);
    }
    if(item.type=='select_param'){
      ctrl = this.renderParamSelect(item.id, key, item.value, item.options_attr, item.isUser)
    }
    if(item.type=='select_view'){
      ctrl = this.renderViewSelect(item.id, key, item.value, item.options_view, item.type_of_view)
    }
    if(item.type=='select_restriction'){
      ctrl = this.renderRestriction(item.id, key, item.value, item.options_attr, item.isUser)
    }
    if(item.type=='color_picker'){
      ctrl=<div className="colorpick_ctrl">
          <input  id={item.id} className="form-control" value={item.value} onChange={(e)=>{
              let {id, value}=e.currentTarget;
              this.changeSett(id, value, key);
          }}/>
          <InputColor initialValue={item.value} onChange={(setColor)=>{
                this.changeSett(item.id, setColor.hex, key);
          }} />
      </div>
    }
    if(item.type=='switcher'){
      ctrl=<div className="switch_cont">
        <span className="switch_label">{(item.value==1?'On':'Off')}</span>
        <IOSSwitch checked={item.value==1?true:false}
                   onChange={(e)=>{
                        let sw_value=item.value==1?0:1;
                        this.changeSett(item.id, sw_value, key);
                   }} />
      </div>
    }
    return ctrl;
  }
  renderSelect=(id, key, value, options_attr)=>{
    let Options=options_attr.map((item)=>{
      return {value:item.value , label:item.label};
    });
    let chosen_value=Options.find((item)=>{
      return value==item.value?true:false;
    })
  /*  if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }*/
    return (<React.Fragment>
      <Select
        menuPosition={'fixed'}
        options={Options}
        value={chosen_value}
        onChange={(opt_data)=>{
          this.changeSett(id, opt_data.value, key);
        }}
      />
      </React.Fragment>
    )
  }
  renderParamSelect=(id, key, value, options_attr, isUser)=>{
    let Options=options_attr.filter((item)=>{
        return (!isUser || isUser && item.LinkType && item.LinkType.uid=='00000000-0000-0000-0000-000000000001')?true:false;
    }).map((item)=>{
      return {value:item.uid , label:item.class.name+'.'+item.name};
    });
    Options=[{value:null, label:'-Chose param-'}, ...Options];
    let chosen_value=Options.find((item)=>{
      return value==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }
    return (<React.Fragment>
      <Select
        menuPosition={'fixed'}
        options={Options}
        value={chosen_value}
        onChange={(opt_data)=>{
          this.changeSett(id, opt_data.value, key);
        }}
      />
      </React.Fragment>
    )
  }
  renderViewSelect=(id, key, value, options_attr, type_of_view)=>{
    let Options=options_attr.filter((item)=>{
      return type_of_view.indexOf(item.type_of_view)!==-1?true:false;
    }).map((item)=>{
      return {value:item.uid , label:item.name};
    });
    Options=[{value:null, label:'-Chose view-'}, ...Options];
    let chosen_value=Options.find((item)=>{
      return value==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose view-'};
    }
    return (<React.Fragment>
      <Select
        menuPosition={'fixed'}
        options={Options}
        value={chosen_value}
        onChange={(opt_data)=>{
          this.changeSett(id, opt_data.value, key);
          this.loadViewData(opt_data.value);
        }}
      />
      </React.Fragment>
    )
  }
  renderRestriction=(id, key, value, options_attr, isUser)=>{
    let Options=options_attr.filter((item)=>{
        return (!isUser || isUser && item.LinkType && item.LinkType.uid=='00000000-0000-0000-0000-000000000001')?true:false;
    }).map((item)=>{
      return {value:item.uid , label:item.class.name+'.'+item.name};
    });
    Options=[{value:null, label:'None'}, {value:-1, label:'Parent'}, ...Options];
    let chosen_value=Options.find((item)=>{
      return value==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }
    return (<React.Fragment>
      <Select
        menuPosition={'fixed'}
        options={Options}
        value={chosen_value}
        onChange={(opt_data)=>{
          this.changeSett(id, opt_data.value, key);
        }}
      />
      </React.Fragment>
    )
  }

  renderMainSett=(CalendarSett)=>{
    let calendar_type=CalendarSett.calendar_type?CalendarSett.calendar_type:0;
    let Settings=[
      {label:'Calendar type', id:'calendar_type', value:calendar_type, type:'select', options:[{value:0, label:'Auditor Calendar'}, {value:1, label:'Manager Calendar'}]},
    ];
    if(calendar_type==1){
      Settings.push(
        {label:'Auditor group', id:'auditor_group', value:(CalendarSett.auditor_group?CalendarSett.auditor_group:'-1'), type:'select', options:this.state.user_group}
      )
    }
    return(
      <React.Fragment>
        {
          Settings.map((item)=>{
            let ctrl=this.renderSettCtrl(item, null);
            return(
              <div key={'cal_event_sett_'+item.id} className="settings__setcont setcont">
                <div className="row">
                  <label className="col-lg-6">{item.label}</label>
                  <div className="col-lg-6">
                    {ctrl}
                  </div>
                </div>
              </div>
            );
          })
        }
      </React.Fragment>
    );
  }
  renderEventSett=()=>{
    let CalendarSett= this.props.AppViewSett && this.props.AppViewSett.settings && this.props.AppViewSett.settings.calendar_sett?this.props.AppViewSett.settings.calendar_sett:{};
    let EventSett= CalendarSett.events?CalendarSett.events:{};
    let Settings=[
      {label:'Event Date', id:'start_date', value:(EventSett.start_date?EventSett.start_date:null), type:'select_param', options_attr:this.props.AppViewSett.attrs, isUser:false},
      //{label:'End Event Date', id:'end_date', value:(EventSett.end_date?EventSett.end_date:null), type:'select_param', options_attr:this.props.AppViewSett.attrs},
      {label:'Event Restriction', id:'event_restriction', value:(EventSett.event_restriction?EventSett.event_restriction:null), type:'select_restriction', options_attr:this.props.AppViewSett.attrs, isUser:false},
      {label:'Type of Event', id:'type_event', value:(EventSett.type_event?EventSett.type_event:0), type:'select', options:[{value:0, label:'Normal'}, {value:1, label:'Background Event'}]},
      {label:'Event Text', id:'text_event', value:(EventSett.text_event?EventSett.text_event:null), type:'select_param', options_attr:this.props.AppViewSett.attrs, isUser:false},
      {label:'Event Background Color', id:'background_color', value:(EventSett.background_color?EventSett.background_color:'#ffffff'), type:'color_picker'},
      {label:'Event Text Color', id:'text_color', value:(EventSett.text_color?EventSett.text_color:'#5d6e74'), type:'color_picker'},
      {label:'Event Border Color', id:'border_color', value:(EventSett.border_color?EventSett.border_color:'#05c0fa'), type:'color_picker'},
    ];
    let Elem_sett=Settings.map((item)=>{
      let ctrl=this.renderSettCtrl(item, 'events');
      return(
        <div key={'cal_event_sett_'+item.id} className="settings__setcont setcont">
          <div className="row">
            <label className="col-lg-6">{item.label}</label>
            <div className="col-lg-6">
              {ctrl}
            </div>
          </div>
        </div>
      );
    })
    return(
      <div className="event__settings settings">
          {Elem_sett}
      </div>
    );
  }
  renderTasksSett=()=>{
    let CalendarSett= this.props.AppViewSett && this.props.AppViewSett.settings && this.props.AppViewSett.settings.calendar_sett?this.props.AppViewSett.settings.calendar_sett:{};
    let TaskSett= CalendarSett.tasks?CalendarSett.tasks:{};
    let Settings:any[]=[
      {label:'Enable Task List', id:'enable_task', value:(TaskSett.enable_task?TaskSett.enable_task:null), type:'switcher'},
    ];
    if(TaskSett.enable_task==1){
      Settings=[
        ...Settings,
        {label:'Chose Task View', id:'task_view', value:(TaskSett.task_view?TaskSett.task_view:null), type:'select_view', options_view:this.state.view_data, type_of_view:[0]},
      ];
    }
    if(TaskSett.enable_task==1 && TaskSett.task_view){
      let attr_data= this.state.view_sett[TaskSett.task_view] && this.state.view_sett[TaskSett.task_view].view &&  this.state.view_sett[TaskSett.task_view].view.attrs?this.state.view_sett[TaskSett.task_view].view.attrs:[];
      Settings=[
        ...Settings,
        {label:'Task Date', id:'start_date', value:(TaskSett.start_date?TaskSett.start_date:null), type:'select_param', options_attr:attr_data, isUser:false},
      //  {label:'End Task Date', id:'end_date', value:(TaskSett.end_date?TaskSett.end_date:null), type:'select_param', options_attr:attr_data},
        {label:'Date limite', id:'date_limit_param', value:(TaskSett.date_limit_param?TaskSett.date_limit_param:null), type:'select_param', options_attr:attr_data, isUser:false},
        {label:'Implementer param', id:'implementer_param', value:(TaskSett.implementer_param?TaskSett.implementer_param:null), type:'select_param', options_attr:attr_data, isUser:false},
        {label:'Task Text', id:'text_task', value:(TaskSett.text_task?TaskSett.text_task:null), type:'select_param', options_attr:attr_data, isUser:false},
        {label:'Task Background Color', id:'background_color', value:(TaskSett.background_color?TaskSett.background_color:'#ffffff'), type:'color_picker'},
        {label:'Task Text Color', id:'text_color', value:(TaskSett.text_color?TaskSett.text_color:'#5d6e74'), type:'color_picker'},
        {label:'Task Border Color', id:'border_color', value:(TaskSett.border_color?TaskSett.border_color:'#05c0fa'), type:'color_picker'},
      ];
    }
    let Elem_sett=Settings.map((item)=>{
      let ctrl=this.renderSettCtrl(item, 'tasks');
      return(
        <div key={'cal_event_sett_'+item.id} className="settings__setcont setcont">
          <div className="row">
            <label className="col-lg-6">{item.label}</label>
            <div className="col-lg-6">
              {ctrl}
            </div>
          </div>
        </div>
      );
    })
    return(
      <div className="task__settings settings">
        {Elem_sett}
      </div>
    );
  }


  render(){
    let CalendarSett= this.props.AppViewSett && this.props.AppViewSett.settings && this.props.AppViewSett.settings.calendar_sett?this.props.AppViewSett.settings.calendar_sett:{};
    return(
    <div className="viewcalendarblock">
        {this.renderMainSett(CalendarSett)}
        {this.renderTabs()}
        {this.renderBodyTab(this.state.tab)}
    </div>);
  }
}
