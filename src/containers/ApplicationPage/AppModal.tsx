import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/AppTree-api";

interface IAppModalProps {
    ApplicationStore?: {
        SelectNode: {
            iddb: string,
            ParentID: string,
            isFolder: boolean,
            isView: boolean,
            isApp: boolean,
            Name: string
            appUid: string
        },
    },
    CloseCallback: () => void,
    TypeModal: string,
    show: boolean,
}
interface IModalTreeState {
    blocking: boolean
    TypeName?: string
    settings?:object,
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export default class AppModal extends React.Component<IAppModalProps> {

    constructor(props) {
        super(props);
    }

    handleClose = () => {
        this.props.CloseCallback();
    };


    onShow = () => {

    };

    render() {
      let Content=null;
      if(this.props.ApplicationStore.SelectNode){
        let TypeModal=this.props.TypeModal;
        if(TypeModal == 'CreateApp' || TypeModal == 'EditApp' || (TypeModal=='Remove' && this.props.ApplicationStore.SelectNode.isApp)){
          Content=<AppModalComponent TypeModal={this.props.TypeModal} CloseCallback={this.props.CloseCallback} />;
        }else if(TypeModal == 'CreateView' || TypeModal == 'EditView' || (TypeModal=='Remove' && this.props.ApplicationStore.SelectNode.isView)){
          Content=<ViewModalComponent TypeModal={this.props.TypeModal} CloseCallback={this.props.CloseCallback} />;
        }else if(TypeModal == 'CreateFolder' || TypeModal == 'EditFolder' || (TypeModal=='Remove' && this.props.ApplicationStore.SelectNode.isFolder)){
          Content=<FolderModalComponent TypeModal={this.props.TypeModal} CloseCallback={this.props.CloseCallback} />;
        }
    }
      return (
            <Modal onShow={this.onShow} animation={true} show={this.props.show} onHide={this.handleClose}>
              {Content}
            </Modal>
        );
    }
}




interface IAppModalComponentProps {
  ToastrStore?:{
    error:(text:string, title:string)=>void,
    success:(text:string, title:string)=>void,
  },
  ApplicationStore?:{
    SelectNode:{iddb:string, ParentID:string, isFolder:boolean, Name:string},
    AddNode:(data:{})=>void,
    DeleteNode:(data:{})=>void,
    EditNode:(data:{})=>void,
  },
  CloseCallback:()=>void,
  TypeModal:string,
}
interface IAppModalComponentState {
    blocking: boolean
    Name?: string
    settings?:{},
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class AppModalComponent extends React.Component<IAppModalComponentProps>{
  state: Readonly<IAppModalComponentState> = {
      blocking: false,
      Name: "",
      settings:null,
  };
  core = {
      addApp:(name:string, parent:string, folder:string, SuccFunc) => {
          ct_api
              .addApp(name, parent, folder)
              .then(res => {
                  SuccFunc(res.data);
              })
              .catch( (error) => {
                  console.log(error);
                  this.props.ToastrStore.error('Server error!', 'Error');
                  this.setState({blocking: false});
              })
      },
      editApp:(uid:string, name:string, SuccFunc) => {
          ct_api
              .editApp(uid,name)
              .then(res => {
                  SuccFunc(res.data);
              })
              .catch( (error) => {
                  console.log(error);
                  this.props.ToastrStore.error('Server error!', 'Error');
                  this.setState({blocking: false});
              })
      },
      removeApp: (uid, SuccFunc) => {
          ct_api
              .removeApp(uid)
              .then(res => {
                  SuccFunc(res.data);
              })
              .catch( (error) => {
                  console.log(error);
                  this.props.ToastrStore.error('Server error!', 'Error');
                  this.setState({blocking: false});
              })
      },
  };

  componentDidMount(){
    if(this.props.TypeModal == "EditApp"){
      let Name=this.props.ApplicationStore.SelectNode.Name;
      this.setState({Name:Name})
    }
  }

  onChangeInput = (e) => {
     let {id, value}=e.currentTarget;
     let sett_key=['display_pers_obj'];
     if(sett_key.indexOf(id)!=-1){
       this.setState({settings:{...this.state.settings, [id]:value}});
     }else{
       this.setState({
           [id]: value,
       });
     }

  };


  ButtonClick=()=>{
  if (this.props.TypeModal == "CreateApp") {
    this.handleCreate();
  }else if(this.props.TypeModal == "EditApp"){
    this.handleEdit();
  }else if(this.props.TypeModal == "Remove"){
    this.handleDelete();
  }
}

  handleCreate = () => {
      let TypeName = this.state.Name;
      if(TypeName.trim()) {
          var ParentID = this.props.ApplicationStore.SelectNode.iddb;
          var _root_parent=this.props.ApplicationStore.SelectNode.ParentID;
          let folder=null;
          if(this.props.ApplicationStore.SelectNode.isFolder){
            folder=this.props.ApplicationStore.SelectNode.iddb;
          }
          this.setState({blocking: true})
          this.core.addApp(TypeName, _root_parent, folder, (data) => {
              if(data.result === 1) {
                  this.props.ToastrStore.success('Success', 'App added successfully!');
                  let iddb = data.data.uid;
                  this.props.ApplicationStore.AddNode({
                      Name: TypeName,
                      icon: 'fas fa-tablet-alt',

                      ParentID: ParentID,
                      iddb: iddb,
                      isApp:true,

                      ParrentTypeID: ParentID,
                      TypeID: iddb,
                      children: null,
                  });

                  this.props.CloseCallback();
              }
              else {
                  this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
              }
              this.setState({blocking: false})
          });
      }
  };
  handleEdit = () => {
      let newName = this.state.Name;
      if(newName.trim()) {
          let iddb = this.props.ApplicationStore.SelectNode.iddb;
          this.setState({blocking: true}, ()=>{
            this.core.editApp(iddb, newName,(data) => {
                if(data.result === 1) {
                    this.props.ToastrStore.success('Success', 'App renamed successfully!');
                    this.props.ApplicationStore.EditNode({
                        iddb: iddb,
                        name: newName
                    });

                    this.props.CloseCallback();
                }
                else {
                    this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
                }
                this.setState({blocking: false})
            });
          })

      }
  };
  handleDelete = () => {
      let iddb = this.props.ApplicationStore.SelectNode.iddb;
      this.setState({blocking: true}, ()=>{
        this.core.removeApp(iddb, (data) => {
            if(data.result === 1) {
                this.props.ToastrStore.success('Success', 'App removed successfully!');
                this.props.ApplicationStore.DeleteNode({
                    iddb: iddb
                });

                this.props.CloseCallback();
            }
            else {
                this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
            }
            this.setState({blocking: false})
        });
      })

  };

  renderBody=()=>{
    let Body =<div className="form-group">
        Enter Name
        <input
            id="Name"
            onChange={this.onChangeInput}
            value={this.state.Name}
            className="form-control"/>
    </div>
    if(this.props.TypeModal == "Remove"){
      Body=<div>
          Are you sure you want to delete "{this.props.ApplicationStore.SelectNode.Name}"
      </div>;
    }
    return Body;
  }

  render(){

      let Title = "Create Application";
      let BtnTitle='Create';
      if (this.props.TypeModal == "EditApp") {
          Title = "Edit Application";
          BtnTitle='Edit';
      }
      if(this.props.TypeModal == "Remove") {
          Title = 'Delete \"' + this.props.ApplicationStore.SelectNode.Name +'\"';
          BtnTitle='Delete';
      }

    return(
      <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
          <Modal.Header closeButton>
              <Modal.Title>{Title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.renderBody()}
          </Modal.Body>
          <Modal.Footer>
            <div>
                <Button variant="success" onClick={this.ButtonClick}>
                    {BtnTitle}
                </Button>
                <Button variant="secondary" onClick={this.props.CloseCallback}>
                    Close
                </Button>
            </div>
          </Modal.Footer>
      </BlockUi>
    );
  }
}


interface IViewModalComponentProps {
  ToastrStore?:{
    error:(text:string, title:string)=>void,
    success:(text:string, title:string)=>void,
  },
  ApplicationStore?:{
    SelectNode:{
      iddb:string,
      ParentID:string,
      isFolder:boolean,
      isView:boolean,
      isApp:boolean,
      Name:string
      appUid:string
     },
    AddNode:(data:{})=>void,
    DeleteNode:(data:{})=>void,
    EditNode:(data:{})=>void,
  },
  CloseCallback:()=>void,
  TypeModal:string,
}
interface IViewModalComponentState {
    blocking: boolean
    Name?: string
    settings?:{
      display_pers_obj:number,
      table_or_form:number,
    },
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class ViewModalComponent extends React.Component<IViewModalComponentProps>{
  state: Readonly<IViewModalComponentState> = {
      blocking: false,
      Name: "",
      settings:{
        display_pers_obj:0,
        table_or_form:0,
      },
  };
  core = {
    addView: (name:string, uidApp:string, settings:object, uidView:string, SuccFunc) => {
        ct_api
            .addView(name, uidApp, settings, uidView)
            .then(res => {
                SuccFunc(res.data);
            })
            .catch( (error) => {
                console.log(error);
                this.props.ToastrStore.error('Server error!', 'Error');
                this.setState({blocking: false});
            })
    },
    editView: (uid:string, name:string, settings:object, SuccFunc) => {
        ct_api
            .editView(uid,name, settings)
            .then(res => {
                SuccFunc(res.data);
            })
            .catch( (error) => {
                console.log(error);
                this.props.ToastrStore.error('Server error!', 'Error');
                this.setState({blocking: false});
            })
    },
    removeView: (uid, SuccFunc) => {
        ct_api
            .removeView(uid)
            .then(res => {
                SuccFunc(res.data);
            })
            .catch( (error) => {
                console.log(error);
                this.props.ToastrStore.error('Server error!', 'Error');
                this.setState({blocking: false});
            })
    },
    getInfoView:(uid, SuccFunc)=>{
      ct_api
          .getInfoView(uid)
          .then(res => {
              SuccFunc(res.data);
          })
          .catch( (error) => {
              console.log(error);
              this.props.ToastrStore.error('Server error!', 'Error');
              this.setState({blocking: false});
          })
    },
  };

  componentDidMount(){
    if(this.props.TypeModal == "EditView"){
      let uid=this.props.ApplicationStore.SelectNode.iddb;
      this.setState({blocking:true}, ()=>{
        this.core.getInfoView(uid, (res)=>{
            if(res.result==1){
              this.setState({Name: res.data.name, settings:res.data.settings, blocking: false});
            }else{
              this.setState({blocking: false});
            }

          });
      })

    }
  }

  onChangeInput = (e) => {
     let {id, value}=e.currentTarget;
     let sett_key=['display_pers_obj', 'table_or_form'];
     if(sett_key.indexOf(id)!=-1){
       this.setState({settings:{...this.state.settings, [id]:value}});
     }else{
       this.setState({
           [id]: value,
       });
     }

  };


  ButtonClick=()=>{
  if (this.props.TypeModal == "CreateView") {
    this.handleCreate();
  }else if(this.props.TypeModal == "EditView"){
    this.handleEdit();
  }else if(this.props.TypeModal == "Remove"){
    this.handleDelete();
  }
}

  handleCreate = () => {
    let TypeName = this.state.Name;
    if(TypeName.trim()) {
        var UidView='';
        var UidApp='';
        var ParentID=this.props.ApplicationStore.SelectNode.iddb;
        if(this.props.ApplicationStore.SelectNode.isView){
            UidView = this.props.ApplicationStore.SelectNode.iddb;
            UidApp= this.props.ApplicationStore.SelectNode.appUid;
        }else if(this.props.ApplicationStore.SelectNode.isApp){
            UidView = '00000000-0000-0000-0000-000000000000';
            UidApp= this.props.ApplicationStore.SelectNode.iddb;
        }
        this.setState({blocking: true}, ()=>{
          this.core.addView(TypeName, UidApp, this.state.settings, UidView, (data) => {
              if(data.result === 1) {
                  this.props.ToastrStore.success('Success', 'View added successfully!');
                  let iddb = data.data.uid;
                  let icon= 'fa fa-table';
                  if(this.state.settings.table_or_form==1){
                    icon='far fa-window-maximize';
                  }else if(this.state.settings.table_or_form==2){
                    icon='far fa-object-group';
                  }
                  this.props.ApplicationStore.AddNode({
                      Name: TypeName,
                      icon: icon,

                      ParentID: ParentID,
                      iddb: iddb,
                      isView:true,
                      appUid:UidApp,

                      ParrentTypeID: ParentID,
                      TypeID: iddb,
                      children: null,
                  });

                  this.props.CloseCallback();
              }
              else {
                  this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
              }
              this.setState({blocking: false})
          });
        })

    }
  };
  handleEdit = () => {
    let newName = this.state.Name;
    if(newName.trim()) {
        let iddb = this.props.ApplicationStore.SelectNode.iddb;
        this.setState({blocking: true}, ()=>{
          this.core.editView(iddb, newName, this.state.settings, (data) => {
              if(data.result === 1) {
                  this.props.ToastrStore.success('Success', 'App renamed successfully!');
                  this.props.ApplicationStore.EditNode({
                      iddb: iddb,
                      name: newName
                  });

                  this.props.CloseCallback();
              }
              else {
                  this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
              }
              this.setState({blocking: false})
          });
        })

    }
  };
  handleDelete = () => {
      let iddb = this.props.ApplicationStore.SelectNode.iddb;
      this.setState({blocking: true}, ()=>{
        this.core.removeView(iddb, (data) => {
            if(data.result === 1) {
                this.props.ToastrStore.success('Success', 'View removed successfully!');
                this.props.ApplicationStore.DeleteNode({
                    iddb: iddb
                });

                this.props.CloseCallback();
            }
            else {
                this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.result);
            }
            this.setState({blocking: false})
        });
      })

  };

  renderBody=()=>{
    let Body = <React.Fragment><div className="form-group">
          Enter Name
          <input
              id="Name"
              onChange={this.onChangeInput}
              value={this.state.Name}
              className="form-control"/>
      </div>
      <div className="form-group">
          Permission output
          <select value={(this.state.settings && this.state.settings.display_pers_obj?this.state.settings.display_pers_obj:0)} id="display_pers_obj" onChange={this.onChangeInput} className="form-control">
              <option value="0">Not limited</option>
              <option value="1">Owner</option>
              <option value="2">Owner group</option>
              <option value="4">Edit by owner</option>
              <option value="3">Edit by owner group</option>
          </select>
      </div><div className="form-group">
          Type of View
          <select value={(this.state.settings && this.state.settings.table_or_form?this.state.settings.table_or_form:0)} id="table_or_form" onChange={this.onChangeInput} className="form-control">
            <option value="0">Table</option>
            <option value="1">Form</option>
            <option value="2">Tab</option>
            <option value="3">Calendar</option>
          </select>
      </div></React.Fragment>;
      if(this.props.TypeModal == "Remove"){
        Body=<div>
            Are you sure you want to delete "{this.props.ApplicationStore.SelectNode.Name}"
        </div>;
      }
      return Body;
  }

  render(){

      let Title = "Create View";
      let BtnTitle='Create';
      if (this.props.TypeModal == "EditView") {
          Title = "Edit View";
          BtnTitle='Edit';
      }
      if(this.props.TypeModal == "Remove") {
          Title = 'Delete \"' + this.props.ApplicationStore.SelectNode.Name +'\"';
          BtnTitle='Delete';
      }

    return(
      <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
          <Modal.Header closeButton>
              <Modal.Title>{Title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              {this.renderBody()}
          </Modal.Body>
          <Modal.Footer>
            <div>
                <Button variant="success" onClick={this.ButtonClick}>
                    {BtnTitle}
                </Button>
                <Button variant="secondary" onClick={this.props.CloseCallback}>
                    Close
                </Button>
            </div>
          </Modal.Footer>
      </BlockUi>
    );
  }
}


interface IFolderModalComponentProps {
  ToastrStore?:{
    error:(text:string, title:string)=>void,
    success:(text:string, title:string)=>void,
  }
  ApplicationStore?:{
    SelectNode:{
      iddb:string,
      ParentID:string,
      isFolder:boolean,
      isView:boolean,
      isApp:boolean,
      Name:string
      appUid:string
     },
    AddNode:(data:{})=>void,
    DeleteNode:(data:{})=>void,
    EditNode:(data:{})=>void,
  }
  CloseCallback:()=>void,
  TypeModal:string,
}
interface IFolderModalComponentState {
    blocking: boolean
    Name?: string
    settings?:{},
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class FolderModalComponent extends React.Component<IFolderModalComponentProps>{
  state: Readonly<IFolderModalComponentState> = {
      blocking: false,
      Name: "",
      settings:null,
  };
  core = {
     addFolder:(name:string, settings:{}, SuccFunc) => {
        ct_api
            .addFolder(name, settings)
            .then(res => {
                SuccFunc(res.data);
            })
            .catch( (error) => {
                console.log(error);
                this.props.ToastrStore.error('Server error!', 'Error');
                this.setState({blocking: false});
            })
    },
     editFolder:(uid:string, name:string, SuccFunc) => {
        ct_api
            .editFolder(uid, name)
            .then(res => {
                SuccFunc(res.data);
            })
            .catch( (error) => {
                console.log(error);
                this.props.ToastrStore.error('Server error!', 'Error');
                this.setState({blocking: false});
            })
    },
     removeFolder: (uid, SuccFunc) => {
        ct_api
            .removeFolder(uid)
            .then(res => {
                SuccFunc(res.data);
            })
            .catch( (error) => {
                console.log(error);
                this.props.ToastrStore.error('Server error!', 'Error');
                this.setState({blocking: false});
            })
    },
  };

  onChangeInput = (e) => {
     let {id, value}=e.currentTarget;
     let sett_key=['display_pers_obj'];
     if(sett_key.indexOf(id)!=-1){
       this.setState({settings:{...this.state.settings, [id]:value}});
     }else{
       this.setState({
           [id]: value,
       });
     }

  };

  componentDidMount(){
    if(this.props.TypeModal == "EditFolder"){
      let Name=this.props.ApplicationStore.SelectNode.Name;
      this.setState({Name:Name})
    }
  }


  ButtonClick=()=>{
  if (this.props.TypeModal == "CreateFolder") {
    this.handleCreate();
  }else if(this.props.TypeModal == "EditFolder"){
    this.handleEdit();
  }else if(this.props.TypeModal == "Remove"){
    this.handleDelete();
  }
}

  handleCreate = () => {
    let TypeName = this.state.Name;
    if(TypeName.trim()){
        var ParentID = this.props.ApplicationStore.SelectNode.iddb;
        this.setState({blocking: true}, ()=>{
          this.core.addFolder(TypeName, null, (data)=>{
            if(data.code === 1) {
              this.props.ToastrStore.success('Success', 'Folder added successfully!');
              let iddb =data.data.uid;
              this.props.ApplicationStore.AddNode({
                  Name: TypeName,
                  icon: 'fas fa-folder',

                  ParentID: ParentID,
                  iddb: iddb,
                  isFolder:true,

                  ParrentTypeID: ParentID,
                  TypeID: iddb,
                  children: null,
              });

              this.props.CloseCallback();
            }else{
              this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.code);
            }
            this.setState({blocking: false})
          })
        })

    }
  };
  handleEdit = () => {
    let newName = this.state.Name;
    if(newName.trim()) {
      let iddb = this.props.ApplicationStore.SelectNode.iddb;
      this.setState({blocking: true}, ()=>{
        this.core.editFolder(iddb, newName, (data)=>{
              if(data.code === 1) {
                this.props.ToastrStore.success('Success', 'Folder renamed successfully!');
                this.props.ApplicationStore.EditNode({
                    iddb: iddb,
                    name: newName
                });
                this.props.CloseCallback();
              }else{
                this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.code);
              }
              this.setState({blocking: false});
        })
      })

    }
  };
  handleDelete = () => {
      let iddb = this.props.ApplicationStore.SelectNode.iddb;
      this.setState({blocking: true}, ()=>{
        this.core.removeFolder(iddb, (data) => {
            if(data.code === 1) {
                this.props.ToastrStore.success('Success', 'Folder removed successfully!');
                this.props.ApplicationStore.DeleteNode({
                    iddb: iddb
                });

                this.props.CloseCallback();
            }
            else {
                this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.code);
            }
            this.setState({blocking: false})
        });
      })

  };

  renderBody=()=>{
    let Body =<div className="form-group">
        Enter Name
        <input
            id="Name"
            onChange={this.onChangeInput}
            value={this.state.Name}
            className="form-control"/>
    </div>
    if(this.props.TypeModal == "Remove"){
      Body=<div>
          Are you sure you want to delete "{this.props.ApplicationStore.SelectNode.Name}"
      </div>;
    }
    return Body;
  }

  render(){

      let Title = "Create Folder";
      let BtnTitle='Create';
      if (this.props.TypeModal == "EditFolder") {
          Title = "Edit Folder";
          BtnTitle='Edit';
      }
      if(this.props.TypeModal == "Remove") {
          Title = 'Delete \"' + this.props.ApplicationStore.SelectNode.Name +'\"';
          BtnTitle='Delete';
      }
    return(
      <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
          <Modal.Header closeButton>
              <Modal.Title>{Title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.renderBody()}
          </Modal.Body>
          <Modal.Footer>
            <div>
                <Button variant="success" onClick={this.ButtonClick}>
                    {BtnTitle}
                </Button>
                <Button variant="secondary" onClick={this.props.CloseCallback}>
                    Close
                </Button>
            </div>
          </Modal.Footer>
      </BlockUi>
    );
  }
}
