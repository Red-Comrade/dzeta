import * as React from "react";
import * as ReactDOM from "react-dom";
import {inject, observer, Provider} from "mobx-react";

import CoolTree from '../../components/CoolTree'
import AppModal from './AppModal'
import * as ct_api from '../../api/AppTree-api'
import {IApplicationPageProps, IApplicationPageState} from "./ApplicationPage";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import INodeTemplate from "../ObjectsPage/Interfaces/INodeTemplate";
import RootStore from "../../stores/RootStore";
import {IObjectModalCloth} from "../ObjectsPage/ObjectModalCloth";
import ObjectsStore from "../../stores/ObjectsStore";
import IModalConfig from "../../components/CoolModalForm/IModalConfig";
import {IModalConfigState} from "../ObjectsPage/ObjectTreeClass";
import EditWindowCloth from "./EditWindowCloth/EditWindowCloth";

import INodeAppTree from "./Interfaces/INodeAppTree"
import ToastrStore from "../../stores/ToastrStore";
import ViewStore from "../../stores/ViewStore";
import ApplicationStore from "../../stores/ApplicationStore";
import ImportStore from "../../stores/ImportStore";


export interface IAppTreeInstanceProps {
    ApplicationStore: any,
    match: any,
}
export interface IAppTreeInstanceState {
    Settings: any,
    CM_target: any,
    CM_content: any,
    ShowFromLevelFunc: any,

    ModalTreeConfig: any,

    AddBtnDisabled: boolean,
    EditBtnDisabled: boolean,
    DelBtnDisabled: boolean,

    expandNodeFunc?: any,
    RedrafFunc?: () => void,
    buildTree: boolean,

    Forms: {[id:string] : IModalConfigState }; //Массив наших форм
}

@inject("ApplicationStore")
@observer
export class AppTreeInstance extends React.Component<IAppTreeInstanceProps, IAppTreeInstanceState> {
    NameModule: "application" | "papplication" = "application"; //Название модуля при котором работает настройка прав
    cancel_axios = {cancel: null};


    static defaultProps = {
        isOpen: null,
        isFullLoad: true, //
        treeData: [],
    };
    state: Readonly<IAppTreeInstanceState> = {
        Settings: null,
        CM_target: null,
        CM_content: [
            {
                "label": "Создать",
                "action": () => {
                },
                "icon_className": 'fa fa-plus',
                "children": [
                    {
                        "label": "Create",
                        "action": () => {
                        },
                        "icon_className": 'glyphicon glyphicon-th',
                        "children": false,
                    },
                    {
                        "label": "Edit",
                        "action": () => {
                        },
                        "icon_className": 'glyphicon glyphicon-th',
                        "children": false
                    },
                    {
                        "label": "Delete",
                        "action": () => {
                        },
                        "icon_className": 'glyphicon glyphicon-th',
                        "children": false
                    }
                ]
            }
        ],
        ShowFromLevelFunc: null,

        RedrafFunc: null,

        ModalTreeConfig: {
            show: false,
            TypeModal: "create",
            data: {}
        },

        AddBtnDisabled: false,
        EditBtnDisabled: false,
        DelBtnDisabled: false,
        buildTree: false,

        Forms: {},
    };

    constructor(props: IAppTreeInstanceProps) {
        super(props);
    };

    componentDidMount(): void {
        this.NameModule = this.props.match.path.indexOf('/application') !== -1 ? "application" : "papplication";
        this.core.GenerateTree();
    };

    componentDidUpdate(prevProps: Readonly<IAppTreeInstanceProps>, prevState: Readonly<IAppTreeInstanceState>, snapshot?: any): void {
        let PrevNameModule: "application" | "papplication" = prevProps.match.path.indexOf('/application') !== -1 ? "application" : "papplication";
        let NameModule: "application" | "papplication" = this.props.match.path.indexOf('/application') !== -1 ? "application" : "papplication";

        if(PrevNameModule != NameModule) {
            this.core.ClearContent();
            this.NameModule = this.props.match.path.indexOf('/application') !== -1 ? "application" : "papplication";
            this.setState({buildTree: false, Forms: {}});
            this.core.GenerateTree();
        }
        else if(NameModule == "papplication") {
            let PrevPathGroupID = prevProps.match.params.GroupID;
            let PathGroupID = this.props.match.params.GroupID;

            if(PrevPathGroupID != PathGroupID) {
                this.core.ClearContent();
                if(this.props.ApplicationStore.SelectNode?.iddb) {
                    this.props.ApplicationStore.IsGetDataForTab = true;
                }
                this.setState({buildTree: false});
                this.core.GenerateTree();
            }
        }
    }

    componentWillUnmount(): void {
        this.props.ApplicationStore.AppContentJSX = null;
        Object.keys(this.state.Forms).forEach( (item_k, key_k) => {
            if(RootStore.Forms[item_k] != undefined) {
                delete RootStore.Forms[item_k];
            }
        });
        RootStore.UpdateForms();
    }

    protected FormCore = {
        //В будущем редактирование переедет сюда
        GenerateEditFormModal: (params: {
            appUid: string,
            name: string,
            isMain?: boolean, //Если открываем не в модальной форме, а в контейнере
            selectNode?: INodeAppTree
        }) => {
            let PathGroupID = this.props.match.params.GroupID;

            let uid_form = 'permis-' + params.appUid;
            if (this.state.Forms[uid_form] == undefined) {
                let PermData = {
                    url_modal: uid_form,
                    appUid: params.appUid,
                    onClose: () => {
                        delete this.state.Forms[uid_form];
                        RootStore.CloseForm(uid_form);
                    },
                    name: params.name,
                    selectNode: params.selectNode,
                    isMain: params.isMain,
                    Group: PathGroupID,
                };



                if(params.isMain) {
                    this.props.ApplicationStore.AppContentJSX =  <EditWindowCloth {...PermData} />;
                }
                else {
                    const Forms = this.state.Forms as any;

                    let ModalConf: IModalConfig = {
                        id: uid_form,
                        onClickCloth: () => {
                        },
                        content: {
                            bodyJSX: <Provider ToastrStore={ToastrStore} RootStore={RootStore} ViewStore = {ViewStore} ObjectsStore = {ObjectsStore} ApplicationStore={ApplicationStore}>
                                <EditWindowCloth {...PermData} />
                            </Provider>,
                        }
                    };

                    Forms[uid_form] = {
                        ...ModalConf,
                        ...{RootNode: null, url_modal: uid_form, type: "permis"},
                    };
                    RootStore.OpenForm(uid_form, ModalConf);
                }

            }
        },
    };

    protected TreeHelper = {
        fillAttrFuncRec: (Children, parrentNode: INodeAppTree) => {
            //Классы для отображения цвета в зависимости от прав
            let block_class = "app_perm_block";
            let allow_class = "app_perm_allow";
            let interm_class = "app_perm_interm";

            for (let key in Children) {
                let Param = Children[key];

                Param.ParentIDTree = parrentNode.id;
                Param.id = parrentNode.id + '|' + Param.uid;

                Param.ParentID = parrentNode.iddb;
                Param.iddb = Param.uid;

                Param.isLoaded = false;
                Param.loaded = false;

                Param.icon = 'fas fa-cube ';

                if (Param.isApp) {
                    Param.icon = 'fas fa-tablet-alt ';
                } else if (Param.isView) {
                    Param.icon = 'fa fa-table ';
                    if(Param.table_or_form==1){
                      Param.icon ='far fa-window-maximize ';
                    }else if(Param.table_or_form==2){
                      Param.icon ='far fa-object-group ' ;
                    }else if(Param.table_or_form==3){
                      Param.icon ='far fa-calendar ' ;
                    }
                }else if (Param.isFolder) {
                    Param.icon = 'fas fa-folder ';
                }

                if(this.NameModule == "papplication" && this.props.match.params.GroupID && this.props.match.params.GroupID != 0) {
                    if (Param.isApp) {
                        if(Param.Settings) {

                        }
                    }
                    else {
                        if(Param.permissionStatus == "intermediate") {
                            Param.icon += interm_class;
                        }
                        else if(Param.permissionStatus == "block") {
                            Param.icon += block_class;
                        }
                        else {
                            Param.icon += allow_class;
                        }
                    }
                }

                if (Param.children.length > 0) {
                    this.TreeHelper.fillAttrFuncRec(Param.children, Param)
                } else {
                    Param.children = null;
                }
            }
        },

        //Для сохранения массива по значению
        extend_rec: (obj1, obj2) => {
            //obj1 наследует свойства obj2, если такие есть
            const extend_rec = function (obj1, obj2) {
                Object.keys(obj1).forEach( (key) => {
                    if( obj2 && typeof obj2 === "object" && obj2[key] !== undefined ) {
                        if( obj1 && typeof obj1[key] === "object" && obj1[key] && Object.keys(obj1[key]).length > 0 ) {
                            //Значит можно еще глубже
                            extend_rec(obj1[key], obj2[key]);
                        }
                        else {
                            obj1[key] = obj2[key];
                        }
                    }
                });
            };
            extend_rec(obj1, obj2);
        },

        h_sort: (data, key, dir) => {
            if(dir === 1) {
                data.sort(function(a, b) {
                    if (a[key] > b[key]) {
                        return 1; }
                    if (a[key] < b[key]) {
                        return -1; }
                    return 0;
                });
            }
            else {
                data.sort(function(a, b) {
                    if (a[key] < b[key]) {
                        return 1; }
                    if (a[key] > b[key]) {
                        return -1; }
                    return 0;
                });
            }
            return data;
        }
    }

    core = {
        AJAX:{
            GetTree: (isPerm, GroupID, SuccFunc) => {
                if(this.cancel_axios.cancel) {
                    this.cancel_axios.cancel();
                }
                else {
                    this.cancel_axios.cancel = null;
                }
                ct_api
                    .getTree(isPerm, GroupID, this.cancel_axios)
                    .then(res => {
                        console.error(res);
                        SuccFunc(res.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            },
        },

        GenerateTree: () => {
            let isPerm = false;
            let PathGroupID = this.props.match.params.GroupID;

            if(this.NameModule == "papplication" && PathGroupID != 0 && PathGroupID) {
                isPerm = true;
            }

            let RootNode:INodeAppTree = {
                Name: "Root",
                id: '|0',
                key: '|0',

                isApp:  true,
                isView: false,

                ParentIDTree: "",
                ParentID: "",

                iddb: "00000000-0000-0000-0000-000000000000",

                icon: 'fas fa-sitemap',

                children: [],
            };
            this.core.AJAX.GetTree(isPerm, PathGroupID,(data_tree) => {

                //МНЕ Проще написать функцию преобразования, чем разбираться в коде на PHP, нихрена не понятно...

                //Классы для отображения цвета в зависимости от прав
                let block_class = "app_perm_block";
                let allow_class = "app_perm_allow";
                let interm_class = "app_perm_interm";

                let expandedKeys = [RootNode.id];

                RootNode.children = data_tree[0]?.children ? data_tree[0].children : [];

                this.TreeHelper.fillAttrFuncRec(RootNode.children, RootNode);

                this.props.ApplicationStore.UpdTreeDataAction([RootNode]);


                this.setState({
                    buildTree: false,
                    expandNodeFunc:  () => {
                        this.setState({expandNodeFunc: null});
                        return expandedKeys;
                    },
                    RedrafFunc: () => {
                        this.setState({RedrafFunc: null, buildTree: true})
                    },
                });
            });
        },

        ClearContent: () => {
            this.props.ApplicationStore.AppContentJSX = null;
            Object.keys(this.state.Forms).forEach( (item_k, key_k) => {
                if(RootStore.Forms[item_k] != undefined) {
                    delete RootStore.Forms[item_k];
                }
            });
            RootStore.UpdateForms();
        }
    };

    render() {
        const DataCoolTree = {
            RootTab: {
                ID: "T00000000-0000-0000-0000-000000000000",
                Name: "Applications",
            },

            treeData: this.props.ApplicationStore.treeData,
            isFullLoad: true,
            selectNodeFunc: null,
            expandNodeFunc: this.state.expandNodeFunc,
            ShowFromLevelFunc: this.state.ShowFromLevelFunc,

            //Кнопки
            AddBtnDisabled: this.state.AddBtnDisabled,
            EditBtnDisabled: this.state.EditBtnDisabled,
            DelBtnDisabled: this.state.DelBtnDisabled,
            OnAddBtnFunc: (selectData) => {
                if (selectData.id === '|0') {
                    this.setState({
                        ModalTreeConfig: {
                            show: true,
                            TypeModal: "CreateApp",
                            data: {},
                            SuccessFunction: () => {
                            }
                        }
                    });
                } else {
                    this.setState({
                        ModalTreeConfig: {
                            show: true,
                            TypeModal: "CreateView",
                            data: {},
                            SuccessFunction: () => {
                            }
                        }
                    });
                }
            },
            OnEditBtnFunc: (selectData) => {
                if (selectData.isApp) {
                    this.setState({
                        ModalTreeConfig: {
                            show: true,
                            TypeModal: "EditApp",
                            data: {},
                            SuccessFunction: () => {
                            }
                        }
                    });
                } else if (selectData.isView) {
                    this.setState({
                        ModalTreeConfig: {
                            show: true,
                            TypeModal: "EditView",
                            data: {},
                            SuccessFunction: () => {
                            }
                        }
                    });
                }
            },
            OnDelBtnFunc: (selectData) => {
                this.setState({
                    ModalTreeConfig: {
                        show: true,
                        TypeModal: "Delete",
                        data: {},
                        SuccessFunction: () => {
                        }
                    }
                });
            },

            onSelect: (selectData) => {
                if (selectData) {
                    if (this.NameModule == "papplication") {
                        this.props.ApplicationStore.setDataView(selectData, [], false, null, null, []);
                        this.FormCore.GenerateEditFormModal({
                            appUid: selectData.appUid,
                            name: selectData.name,
                            isMain: true,
                            selectNode: selectData,
                        });
                    }
                    else {
                        if (selectData.isView) {
                            this.props.ApplicationStore.setLoader(true);
                            /*Promise.all([
                              ct_api.getAttributesForApp(selectData.iddb),
                              ct_api.getInfoView(selectData.iddb),
                              ct_api.getViewClass(selectData.iddb),
                              ct_api.getParentToRoot(selectData.iddb),
                            ])*/
                            ct_api.getView(selectData.iddb).then((res) => {
                                //Данные дял связей типов пердставления
                                if (res.data.result == 1) {
                                    let data_view = res.data.data;
                                    //Создания ид для параметров
                                    data_view.view.attrs = data_view.view.attrs.map((item) => {
                                        let data = {id: Math.floor(Math.random() * 10000000000), ...item};
                                        data.class = {name: data.parent.name, uid: data.parent.uid};
                                        return data;
                                    });

                                    this.props.ApplicationStore.setDataView(selectData, data_view.view, false, {
                                        mode: 'viewTable',
                                        data: selectData.iddb
                                    }, data_view.ParrentsToRoot);

                                }

                            })

                        }
                        else if (selectData.isApp) {
                            ct_api.getSettings(selectData.iddb).then((res) => {
                                if (res.data.result == 1) {
                                    let AppSett = res.data.data;
                                    this.props.ApplicationStore.setDataApp(selectData, AppSett, false);
                                }
                            })

                        }
                        else {

                        }
                    }
                    if (selectData.id === "|0") {
                        //root
                        this.setState({
                            AddBtnDisabled: false,
                            EditBtnDisabled: true,
                            DelBtnDisabled: true,
                        });
                    } else {
                        this.setState({
                            AddBtnDisabled: false,
                            EditBtnDisabled: false,
                            DelBtnDisabled: false,
                        });
                    }

                } else {
                  this.props.ApplicationStore.setDataView(null, [], false, null, null, []);
                }
            },
            onSelectR: (selectData) => {
                if (selectData) {
                    this.props.ApplicationStore.setSelectNode(selectData);
                }
            },
            onOpenCM: (params: { SelectData: any, callbackCloseCM: () => void, IsBtnCM?: boolean }, callback) => {
                let contentCM = [];
                const loopGetFolderNode = (children) => {
                    var Folder=[];
                    children.forEach((item) => {
                        if(item.isFolder) {
                          Folder.push(item);
                        }
                        if(item.children) {
                            let chilfFolder=loopGetFolderNode(item.children);
                            Folder=Folder.concat(chilfFolder);
                        }
                    });
                    return Folder;
                };
                let DataFolde=loopGetFolderNode(this.props.ApplicationStore.treeData);
                let Items_Folders=DataFolde.map((item)=>{
                  return {
                    uid:item.iddb,
                    name:item.Name,
                  }
                })
                if (params.IsBtnCM) {
                    //Контекстное меню от кнопки
                } else {
                    if (params.SelectData.id === '|0') {
                        contentCM.push({
                            "label": "Create application",
                            "action": () => {
                                this.setState({
                                    ModalTreeConfig: {
                                        show: true,
                                        TypeModal: "CreateApp",
                                        data: {},
                                        SuccessFunction: () => {
                                        }
                                    }
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-tablet-alt',
                            "children": false
                        });
                        contentCM.push({
                            "label": "Create folder",
                            "action": () => {
                                this.setState({
                                    ModalTreeConfig: {
                                        show: true,
                                        TypeModal: "CreateFolder",
                                        data: {},
                                        SuccessFunction: () => {
                                        }
                                    }
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-folder',
                            "children": false
                        });
                        contentCM.push({
                            "label": "Sort folder",
                            "action": () => {
                                this.props.ApplicationStore.setShowModalSort(true, Items_Folders, 'sortFolders', null);
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-sort-alpha-down',
                            "children": false
                        });
                    }
                    if (params.SelectData.id !== '|0') {
                      if(params.SelectData.isFolder){
                        contentCM.push({
                            "label": "Create application",
                            "action": () => {
                                this.setState({
                                    ModalTreeConfig: {
                                        show: true,
                                        TypeModal: "CreateApp",
                                        data: {},
                                        SuccessFunction: () => {
                                        }
                                    }
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-tablet-alt',
                            "children": false
                        });
                      }
                      if(params.SelectData.isApp || params.SelectData.isView){
                        contentCM.push({
                            "label": "Create view",
                            "action": () => {
                                this.setState({
                                    ModalTreeConfig: {
                                        show: true,
                                        TypeModal: "CreateView",
                                        data: {},
                                        SuccessFunction: () => {
                                        }
                                    }
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa fa-table',
                            "children": false
                        });
                      }
                      contentCM.push({
                            "label": "Edit",
                            "action": () => {
                                if (params.SelectData.isApp) {
                                    this.setState({
                                        ModalTreeConfig: {
                                            show: true,
                                            TypeModal: "EditApp",
                                            data: {},
                                            SuccessFunction: () => {
                                            }
                                        }
                                    });
                                } else if (params.SelectData.isView) {
                                    this.setState({
                                        ModalTreeConfig: {
                                            show: true,
                                            TypeModal: "EditView",
                                            data: {},
                                            SuccessFunction: () => {
                                            }
                                        }
                                    });
                                }else if (params.SelectData.isFolder) {
                                  this.setState({
                                      ModalTreeConfig: {
                                          show: true,
                                          TypeModal: "EditFolder",
                                          data: {},
                                          SuccessFunction: () => {
                                          }
                                      }
                                  });
                                }
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-edit',
                            "children": false
                        });

                      if(params.SelectData.isApp) {

                            contentCM.push({
                                "label": "Edit permission",
                                "action": () => {
                                    this.FormCore.GenerateEditFormModal({
                                        appUid: params.SelectData.appUid,
                                        name: params.SelectData.name,
                                        selectNode: params.SelectData,
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-user-shield TypeTreeCM_a_i',
                                "children": false
                            });
                            let contentSubFolder = Items_Folders.map((item)=>{
                              return {
                                  "label": item.name,
                                  "action": ()=> {
                                    ct_api.addAppToFolder(item.uid, params.SelectData.iddb).then(res => {
                                        if(res.data.code===1){
                                          ToastrStore.success('Success', 'App moved successfully!');
                                          this.props.ApplicationStore.MoveNode(params.SelectData.iddb, item.uid);
                                          params.callbackCloseCM();
                                        }else{
                                          ToastrStore.error('Server error!', 'ERROR CODE:' + res.data.result);
                                        }
                                    })
                                    .catch( (error) => {
                                        console.log(error);
                                        ToastrStore.error('Server error!', 'Error');
                                    })

                                  },
                                  "icon_className": 'fa fa-folder',
                                  "children": false,
                              }
                            });
                            if(params.SelectData.ParentID!=='00000000-0000-0000-0000-000000000000'){
                              contentSubFolder=[{
                                  "label": 'Root',
                                  "action": ()=> {
                                    ct_api.addAppToFolder(null, params.SelectData.iddb).then(res => {
                                        if(res.data.code===1){
                                          ToastrStore.success('Success', 'App moved successfully!');
                                          this.props.ApplicationStore.MoveNode(params.SelectData.iddb, '00000000-0000-0000-0000-000000000000');
                                          params.callbackCloseCM();
                                        }else{
                                          ToastrStore.error('Server error!', 'ERROR CODE:' + res.data.result);
                                        }
                                    })
                                    .catch( (error) => {
                                        console.log(error);
                                        ToastrStore.error('Server error!', 'Error');
                                    })

                                  },
                                  "icon_className": 'fas fa-sitemap',
                                  "children": false,
                              }, ...contentSubFolder];
                            }
                            if(contentSubFolder.length){
                              contentCM.push({
                                  "label": "Move to",
                                  "action": ()=> {},
                                  "icon_className": 'far fa-object-group',
                                  "children": contentSubFolder
                              });
                            }

                        }
                      if(params.SelectData.isFolder){
                        contentCM.push({
                            "label": "Sort App",
                            "action": () => {
                                var Items_app=params.SelectData.children.filter((item)=>{
                                  return item.isApp?true:false;
                                }).map((item)=>{
                                  return {
                                    uid:item.iddb,
                                    name:item.Name,
                                  }
                                })
                                this.props.ApplicationStore.setShowModalSort(true, Items_app, 'sortApp', {folderuid:params.SelectData.iddb});
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-sort-alpha-down',
                            "children": false
                        });
                      }


                        contentCM.push({
                            "label": "Delete",
                            "action": () => {
                                this.setState({
                                    ModalTreeConfig: {
                                        show: true,
                                        TypeModal: "Remove",
                                        data: {},
                                        SuccessFunction: () => {
                                        }
                                    }
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": 'fas fa-times',
                            "children": false
                        });
                    }

                }
                contentCM.push({
                    "label": "Display from current level",
                    "action": () => {
                        this.setState({
                            ShowFromLevelFunc: (
                                (callback) => {
                                    this.setState({ShowFromLevelFunc: null});
                                    callback(params.SelectData.id);
                                })
                        });
                        params.callbackCloseCM();
                    },
                    "icon_className": 'far fa-eye',
                    "children": false
                });
                return contentCM;
            },
            onExpandNode: (node_info) => {
            },
            RedrafFunc: this.state.RedrafFunc,
        };
        const dataModalTree = {
            CloseCallback: () => {
                this.state.ModalTreeConfig.show = false;
                this.setState({ModalTreeConfig: this.state.ModalTreeConfig})
            },
            SuccessFunction: this.state.ModalTreeConfig.SuccessFunction,
            show: this.state.ModalTreeConfig.show,
            TypeModal: this.state.ModalTreeConfig.TypeModal,
            data: this.state.ModalTreeConfig.data,
        };
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;

        let isBlocking = !this.state.buildTree;

        return (
            <BlockUi id="zt_FilterSidebar_body_co" tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                <div id="zt_FilterSidebar_body_co">
                    <CoolTree {...DataCoolTree} />
                    <AppModal {...dataModalTree} />
                </div>
            </BlockUi>
        );
    }
}
