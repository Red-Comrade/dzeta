export default interface INodeAppTree {
    permissionStatus?: "allow" | "block" | "intermediate",
    permission?: {
        FSettings: any,
        Settings: {
            Add: string, EditDel: string, Read: string, Class: string, View: string, Object?: string,
        }[],
        result: number,
    },

    classes?: { Class: string, ClassName: string, MainClass: string, MainClassName: string } []

    isLoaded?: boolean,
    loaded?: boolean,

    Name: string,
    id: string,
    key?: string,


    uid?: string,


    isApp?:  boolean,
    isView?: boolean,
    ParentIDTree: string,
    ParentID: string,
    icon: string,

    iddb: string,

    children?: INodeAppTree[],

}