import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import CoolTree, {ICoolTreeProps} from '../../../components/CoolTree'
import INodeAppTree from '../Interfaces/INodeAppTree'
import './EditWindowCloth.scss'
import Tree, { TreeNode } from 'rc-tree';


import { makeStyles, Theme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

export interface IViewsPermTreeProps {
    treeData: INodeAppTree[],
    onChangeState: (new_state?) => void,
    get_attrDataCache: () => {[id:string] : any},
}
export interface IViewsPermTreeState {
    CM_target: any,
    CM_content: any,
    expandNodeFunc?: any,
    RedrafFunc?: () => void,
    ShowFromLevelFunc: any,

    SearchString?: string,
    selectedKey: string,
    expandedKeys: string[],
    checkedKeys: string[],
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export class ViewsPermContent extends React.Component<IViewsPermTreeProps, IViewsPermTreeState>{
    static defaultProps = {
        isOpen: null,
        isFullLoad: true, //
        treeData:[],
    };
    state: Readonly<IViewsPermTreeState> = {
        CM_target: null,
        CM_content: [],
        ShowFromLevelFunc: null,
        expandNodeFunc: null,

        SearchString: "",
        selectedKey: "",
        expandedKeys: [],
        checkedKeys: [],

    };

    constructor(props: IViewsPermTreeProps) {
        super(props);
    };
    componentDidMount(): void {

    };

    core = {

        build: {
            checkboxPermTableAll: (attrDataCache, data, name) => {
                let isChecked = true;

                let row_data = [];
                if(this.state.checkedKeys?.length > 0) {
                    this.state.checkedKeys.forEach((item_key, key) => {
                        let item_data = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, item_key);
                        if(item_data?.isView && (item_data?.table_or_form == 1 || item_data?.table_or_form == 0 || item_data?.table_or_form == 3) ) {
                            row_data.push(item_data);
                        }
                    });

                }
                else if(this.state.selectedKey) {
                    let item_data = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, this.state.selectedKey);
                    if(item_data?.isView && (item_data?.table_or_form == 1 || item_data?.table_or_form == 0 || item_data?.table_or_form == 3)) {
                        row_data.push(item_data);
                    }
                }

                for(let key_it in row_data) {
                    let item = row_data[key_it];
                    let clases_tmp = item.classes;
                    for( let key_cl in clases_tmp) {
                        let tmp_is_ch = false;
                        let item_tmp_cl = clases_tmp[key_cl];
                        if(attrDataCache[item.uid]?.[item_tmp_cl.Class]?.[name] != undefined) {
                            tmp_is_ch = attrDataCache[item.uid]?.[item_tmp_cl.Class]?.[name] == 1;
                        }
                        else if(data.AppItem?.permission?.FSettings?.[item.uid]?.[item_tmp_cl.Class]?.[name] != undefined){
                            tmp_is_ch =  data.AppItem?.permission?.FSettings?.[item.uid]?.[item_tmp_cl.Class]?.[name] == 1;
                        }
                        else {
                            tmp_is_ch = false;
                        }
                        if(!tmp_is_ch) {
                            isChecked = false;
                            break;
                        }
                    }
                    if(!isChecked) {
                        break;
                    }
                }

                return <Checkbox checked={isChecked} onChange={(e) => {
                    let attrDataCache = this.props.get_attrDataCache();
                    let isChecked = e.target.checked;
                    let row_data = [];

                    if(this.state.checkedKeys?.length > 0) {
                        this.state.checkedKeys.forEach((item_key, key) => {
                            let item_data = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, item_key);
                            if(item_data?.isView && (item_data?.table_or_form == 1 || item_data?.table_or_form == 0 || item_data?.table_or_form == 3)) {
                                row_data.push(item_data);
                            }
                        });

                    }
                    else if(this.state.selectedKey) {
                        let item_data = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, this.state.selectedKey);
                        if(item_data?.isView && (item_data?.table_or_form == 1 || item_data?.table_or_form == 0 || item_data?.table_or_form == 3)) {
                            row_data.push(item_data);
                        }
                    }

                    row_data.forEach((item, key) => {
                        attrDataCache[item.uid] = attrDataCache[item.uid] == undefined ? {} : attrDataCache[item.uid];
                        item.classes.forEach( (item_cl) => {
                            attrDataCache[item.uid][item_cl.Class] = attrDataCache[item.uid][item_cl.Class] == undefined ? {} : attrDataCache[item.uid][item_cl.Class];
                            attrDataCache[item.uid][item_cl.Class] [name] = isChecked;
                        });
                    });

                    this.props.onChangeState();
                }} />;
            },
            checkboxPermTable: (attrDataCache, data, name) => {
                let isChecked = true;
                if(data?.item_cl != undefined) {
                    //Значит мы записываем кэш для класса
                    if(attrDataCache?.[data.item.uid]?.[data.item_cl.Class]?.[name] == undefined) {
                        isChecked = data.AppItem?.permission?.FSettings?.[data.item.uid]?.[data.item_cl.Class]?.[name] == 1;
                    }
                    else {
                        isChecked = !!attrDataCache[data.item.uid][data.item_cl.Class][name];
                    }
                }
                else {
                    //Чекбокс активируется в зависимости от выборки
                    let clases_tmp = data.item.classes;
                    for( let key_cl in clases_tmp) {
                        let tmp_is_ch = false;
                        let item_tmp_cl = clases_tmp[key_cl];
                        if(attrDataCache[data.item.uid]?.[item_tmp_cl.Class]?.[name] != undefined) {
                            tmp_is_ch = attrDataCache[data.item.uid]?.[item_tmp_cl.Class]?.[name] == 1;
                        }
                        else if(data.AppItem?.permission?.FSettings?.[data.item.uid]?.[item_tmp_cl.Class]?.[name] != undefined){
                            tmp_is_ch =  data.AppItem?.permission?.FSettings?.[data.item.uid]?.[item_tmp_cl.Class]?.[name] == 1;
                        }
                        else {
                            tmp_is_ch = false;
                        }
                        if(!tmp_is_ch) {
                            isChecked = false;
                            break;
                        }
                    }
                }

                return <Checkbox checked={isChecked} onChange={(e) => {
                    let attrDataCache = this.props.get_attrDataCache();
                    let isChecked = e.target.checked;

                    //Добавляем вложенность, если нет
                    attrDataCache[data.item.uid] = attrDataCache[data.item.uid] == undefined ? {} : attrDataCache[data.item.uid];

                    if(data.item_cl) {
                        attrDataCache[data.item.uid][data.item_cl.Class] = attrDataCache[data.item.uid][data.item_cl.Class] == undefined ? {} : attrDataCache[data.item.uid][data.item_cl.Class];
                        attrDataCache[data.item.uid][data.item_cl.Class][name] = isChecked;
                    }
                    else {
                        //если выбираем, либо снимаем выделение для всего представлеиния
                        data.item.classes.forEach( (item_cl) => {
                            attrDataCache[data.item.uid][item_cl.Class] = attrDataCache[data.item.uid][item_cl.Class] == undefined ? {} : attrDataCache[data.item.uid][item_cl.Class];
                            attrDataCache[data.item.uid][item_cl.Class][name] = isChecked;
                        })
                    }

                    this.props.onChangeState();
                }} />;
            },
            btnHidePermTable: (is_active, item) => {
                let type_ch = "view_btn";

                return <button className={"nf_btn-ellipse"} styleName="hide_show_btn" onClick={ () => {
                    let attrDataCache = this.props.get_attrDataCache();

                    if(attrDataCache[item.uid] == undefined) {
                        attrDataCache[item.uid] = {} as any;
                    }
                    if(attrDataCache[item.uid][type_ch] == undefined) {
                        attrDataCache[item.uid][type_ch] = {} as any;
                    }

                    if(attrDataCache[item.uid][type_ch] == true) {
                        delete attrDataCache[item.uid][type_ch];
                    }
                    else {
                        attrDataCache[item.uid][type_ch] = true;
                    }

                    this.props.onChangeState();

                }} >{is_active?"+":"-"}</button>
            },

            PermTable: () => {
                let rows = [];
                let row_data = [];

                if(this.state.checkedKeys?.length > 0) {
                    this.state.checkedKeys.forEach((item_key, key) => {
                        let item_data = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, item_key);
                        if(item_data?.isView && (item_data?.table_or_form == 1 || item_data?.table_or_form == 0 || item_data?.table_or_form == 3)) {
                            row_data.push(item_data);
                        }
                    });
                }
                else if(this.state.selectedKey) {
                    let item_data = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, this.state.selectedKey);
                    if(item_data?.isView && (item_data?.table_or_form == 1 || item_data?.table_or_form == 0 || item_data?.table_or_form == 3)) {
                        row_data.push(item_data);
                    }
                }
                else {
                    return null;
                }

                let attrDataCache = this.props.get_attrDataCache();


                //Временное решение, что мы выбираем только одно приложение
                let AppItem = this.props.treeData[0];

                row_data.forEach((item, key) => {
                    let isActiveBtnHideClasses = attrDataCache?.[item.uid]?.["view_btn"] == true;
                    rows.push(<tr key={item.uid} styleName={"permiss_v_row_view"}>
                        <th scope="col">{this.core.build.btnHidePermTable(isActiveBtnHideClasses, item)}</th>
                        <th scope="col">{item.Name}</th>
                        <td scope="col">{this.core.build.checkboxPermTable(attrDataCache, { item: item, AppItem: AppItem }, "Add")}</td>
                        <td scope="col">{this.core.build.checkboxPermTable(attrDataCache, { item: item, AppItem: AppItem }, "Read")}</td>
                        <td scope="col">{this.core.build.checkboxPermTable(attrDataCache, { item: item, AppItem: AppItem }, "EditDel")}</td>
                    </tr>);

                    if(!isActiveBtnHideClasses && item.classes) {
                        item.classes.forEach( (item_cl, key_cl) => {
                            rows.push(<tr key={item.uid+'cl'+item_cl.Class}>
                                <th scope="col">{item_cl.ClassName}</th>
                                <th scope="col">{item.Name}</th>
                                <td scope="col">{this.core.build.checkboxPermTable(attrDataCache, { item: item, item_cl: item_cl, AppItem: AppItem} , "Add")}</td>
                                <td scope="col">{this.core.build.checkboxPermTable(attrDataCache, { item: item, item_cl: item_cl, AppItem: AppItem}, "Read")}</td>
                                <td scope="col">{this.core.build.checkboxPermTable(attrDataCache, { item: item, item_cl: item_cl, AppItem: AppItem}, "EditDel")}</td>
                            </tr>);
                        });
                    }
                });

                return <div styleName="co_v_table">
                    <table className="table table-sm table-bordered" >
                        <thead>
                        <tr>
                            <th scope="col" rowSpan={2}>Class</th>
                            <th scope="col" rowSpan={2}>View</th>
                            <th scope="col">Add</th>
                            <th scope="col">Read</th>
                            <th scope="col">EditDel</th>
                        </tr>
                        <tr>
                            <th scope="col">{this.core.build.checkboxPermTableAll(attrDataCache, {AppItem: AppItem}, "Add")}</th>
                            <th scope="col">{this.core.build.checkboxPermTableAll(attrDataCache,{AppItem: AppItem}, "Read")}</th>
                            <th scope="col">{this.core.build.checkboxPermTableAll(attrDataCache, {AppItem: AppItem}, "EditDel")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {rows}
                        </tbody>
                    </table>
                </div>;
            }
        }
    };


    TreeCore = {
        events: {
            onSelect: (info, e, selectedNodes) => {
                let sel_id_node = e.node.props.eventKey;
                if(sel_id_node) {
                    const selectData = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, sel_id_node);
                    if( selectData?.isApp || (selectData.isView && (selectData?.table_or_form == 1 || selectData?.table_or_form == 0 || selectData?.table_or_form == 3)) )
                        this.setState({selectedKey: selectData.id})
                }
            },
            onExpand: (expandNode) => {
                this.setState({expandedKeys: expandNode});
            },
            onCheck: (checkedKeys, info) => {

                let chk_key = info?.node?.props?.eventKey;

                if(chk_key) {
                    const selectData = this.TreeCore.helpers.loopFindDataNode(this.props.treeData, chk_key);

                    if(selectData.isApp) {
                        //Если выбираем приложение, то автоматом выбираем все нижестоящие
                        let all_keys = [];
                        if(info.checked) {
                            let loopCh = (children) => {
                                children.forEach((item, key) => {
                                    all_keys.push(item.id);
                                    if(item.children && item.children.length > 0) {
                                        loopCh(item.children);
                                    }
                                });
                            };
                            loopCh(this.props.treeData);
                        }

                        this.setState({checkedKeys: all_keys});
                    }
                    else {
                        this.setState({checkedKeys: checkedKeys.checked});
                    }
                }

                console.log('onCheck', checkedKeys, info);
            },
        },

        helpers: {
            //Для получение всех данных узла
            loopFindDataNode: (children, id) => {
                if(typeof children == "object") {
                    for (let i = 0; i < children.length; i++) {
                        if(children[i].id == id) {
                            return children[i];
                        }
                        else if(typeof children[i].children == "object" && children[i].children!== null && children[i].children.length > 0) {
                            let sel_item = this.TreeCore.helpers.loopFindDataNode(children[i].children, id);
                            if(sel_item !== null) {
                                return sel_item;
                            }
                        }
                        else if(typeof children[i].children != "object" ) {
                            debugger
                        }
                    }
                }
                return null;
            }
        }
    };

    render() {
        let treeData = this.props.treeData ? this.props.treeData : [];

        let expandedKeys = [];
        const loop = (data, SearchString) => {
            let children = [];
            if(data.length > 0) {
                data.forEach( (item) => {
                    let icon = item.icon ? item.icon : 'rc-tree-iconEle rc-tree-icon__open';

                    let is_founded = item.Name.toLocaleLowerCase().indexOf(SearchString) !== -1;
                    let ClassName = is_founded && SearchString != "" ? "ttp_tree_node_founded" : "";
                    const DateTreeNode = {
                        active: false,
                        icon: <span className={icon}/>,
                        title: item.Name,
                        key: item.id,
                        className: ClassName,
                        isLeaf: false,
                        disabled: false
                    };

                    if (item.children) {
                        let childrenJSX = loop(item.children, SearchString);
                        if( is_founded || childrenJSX.length > 0 ) {
                            if(SearchString) {
                                expandedKeys.push(item.id);
                            }
                            children.push(
                                <TreeNode {...DateTreeNode}>
                                    {childrenJSX}
                                </TreeNode>
                            );
                        }
                    }
                    else {
                        if( item.Name.toLocaleLowerCase().indexOf(SearchString) !== -1) {
                            DateTreeNode.isLeaf = true;
                            DateTreeNode.disabled = item.key === '0-0-0';
                            children.push(<TreeNode {...DateTreeNode} />);
                        }
                    }
                });
            }
            return children;
        };
        const treeNodes = loop(treeData, this.state.SearchString);

        let selectedKeys = [];
        if(this.state.selectedKey) {
            selectedKeys.push(this.state.selectedKey+"");
        }
        expandedKeys = this.state.expandedKeys;
        let checkedKeys = this.state.checkedKeys;


        const DataTree = {
            onSelect: this.TreeCore.events.onSelect,
            //onExpand: this.TreeCore.events.onExpand,
            onCheck: this.TreeCore.events.onCheck,

            defaultExpandAll: true,

            selectedKeys: selectedKeys,
           // expandedKeys: expandedKeys,
            checkedKeys: checkedKeys,
        };

        return (
            <div className={"co_views"}>
                <div className={"co_tree"}>
                    <Tree
                        {...DataTree}
                        checkStrictly
                        showLine
                        checkable
                    >
                        {treeNodes}
                    </Tree>
                </div>
                <div className={"co_perm"}>
                    {this.core.build.PermTable()}
                </div>
            </div>)
    }
}
