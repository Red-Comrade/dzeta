import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import { HashRouter as Router, Route, Link, Switch, Redirect, RouteProps, RouteComponentProps} from 'react-router-dom'
import axios from 'axios';

import HZTabs from "../../../components/HZTabs";
import * as ot_api from '../../../api/ObjTree-api'
import * as ct_api from '../../../api/ClassTree-api'
import * as ap_api from '../../../api/AppTree-api'

import { useRouteMatch, useParams } from 'react-router-dom'
import Loader from "react-loaders";
import BlockUi from "react-block-ui";
import * as f_helper from "../../../helpers/form_datatype_helper";

import RootStore from '../../../stores/RootStore'
import {IValueFile} from "../../../api/ObjTree-api";


import { makeStyles, Theme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import './EditWindowCloth.scss'

import ToastrStore from '../../../stores/ToastrStore'
import INodeAppTree from "../Interfaces/INodeAppTree";
import {ViewsPermContent} from "./ViewsPermContent";

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}


interface IPermisProps {
    url_modal: string,
    appUid: string,
    name: string,
    isMain?: boolean,


    Group: string, //Группа пользователя
    selectNode?: INodeAppTree,

    onClose: () => void,
}
interface IPermisState {
    isLoadModalContent: boolean,
    TabsSett: number,

    modules: { allow: 0 | 1, module: string }[],



    redrawWindow?: boolean,


    attrDataCache: {[id:string] : any},
}

export default class EditWindowCloth extends React.Component<IPermisProps, IPermisState> {
    static defaultProps = {
    };
    state: Readonly<IPermisState> = {
        isLoadModalContent: true,
        TabsSett: 0,
        modules: [],
        attrDataCache: {},
        redrawWindow: false,
    };

    xhrGenerateModalContent = {cancel: null};

    constructor(props: IPermisProps) {
        super(props);
    };
    componentDidMount(): void {
        this.core.GenerateModalContent();
    };

    shouldComponentUpdate(nextProps: Readonly<IPermisProps>, nextState: Readonly<IPermisState>, nextContext: any): boolean {
        if(nextProps.selectNode?.id != this.props.selectNode?.id) {
            this.setState({redrawWindow: true, attrDataCache: {}});
            return false;
        }
        return true;
    }

    componentDidUpdate(prevProps: Readonly<IPermisProps>, prevState: Readonly<IPermisState>, snapshot?: any): void {
        if(this.state.redrawWindow) {
            this.setState({redrawWindow: false});
        }
    }


    events = {
        onChangeTab: (event: React.ChangeEvent<{}>, newValue: number) => {
            this.setState({TabsSett: newValue });
        },
        onSave: (event: React.ChangeEvent<{}>, isCloseForm: boolean) => {
            this.core.SavePermission(isCloseForm);
        },
        onClose: () => {
            if(this.props.onClose) {
                this.props.onClose();
            }
        }
    };

    core = {
        AJAX: {
            addAccessForApp: function (params:{App:string, Group:string, Settings: {View:string, Add: number, EditDel: number, Read: number }[] }, SuccFunc, ErrFunc) {
                ap_api
                    .addAccessForApp(params)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch(function (error) {
                        ErrFunc({result: 0});
                        console.log(error);
                    })
            },

        },

        GenerateModalContent: () => {
            this.setState({isLoadModalContent: false});
        },


        SavePermission: (isCloseForm: boolean) => {
            this.setState({isLoadModalContent: false});


            //Тут мы проходим по всему кэшу, удаляем его

            if(Object.keys(this.state.attrDataCache).length > 0) {
                //Проходим по всем представлениям
                if(this.props.selectNode.isApp) {
                    if(this.props.selectNode.children && this.props.selectNode.children.length > 0) {

                        let all_keys = [];
                        let views = [];

                        let loopCh = (children) => {
                            children.forEach((item, key) => {
                                all_keys.push(item.id);
                                const selectData = this.helper.loopFindDataNode(this.props.selectNode.children, item.id);
                                if(selectData) {
                                    views.push(selectData);
                                }
                                if(item.children && item.children.length > 0) {
                                    loopCh(item.children);
                                }
                            });
                        };
                        loopCh(this.props.selectNode.children);



                        let Settings_s = [];
                        let FSettings_s = {};

                        views.forEach( (item) => {
                            if(item.classes && item.classes.length > 0) {
                                item.classes.forEach( (item_cl) => {
                                    let ch_tmp = {Add: 0, EditDel: 0, Read: 0};


                                    if(this.state.attrDataCache[item.uid]?.[item_cl.Class] != undefined) {
                                        let ch_tmp2 = this.state.attrDataCache[item.uid][item_cl.Class];
                                        ch_tmp.Add = ch_tmp2.Add != undefined ? Number(ch_tmp2.Add) : Number(this.props.selectNode?.permission?.FSettings?.[item.uid]?.[item_cl.Class]?.["Add"] == 1);
                                        ch_tmp.EditDel = ch_tmp2.EditDel != undefined ? Number(ch_tmp2.EditDel) : Number(this.props.selectNode?.permission?.FSettings?.[item.uid]?.[item_cl.Class]?.["EditDel"] == 1);
                                        ch_tmp.Read = ch_tmp2.Read != undefined ? Number(ch_tmp2.Read) : Number(this.props.selectNode?.permission?.FSettings?.[item.uid]?.[item_cl.Class]?.["Read"] == 1);
                                    }
                                    else {
                                        ch_tmp.Add = Number(this.props.selectNode?.permission?.FSettings?.[item.uid]?.[item_cl.Class]?.["Add"] == 1);
                                        ch_tmp.EditDel = Number(this.props.selectNode?.permission?.FSettings?.[item.uid]?.[item_cl.Class]?.["EditDel"] == 1);
                                        ch_tmp.Read = Number(this.props.selectNode?.permission?.FSettings?.[item.uid]?.[item_cl.Class]?.["Read"] == 1);
                                    }

                                    let item_s = {
                                        View: item.uid,
                                        Class: item_cl.Class,

                                        Add: ch_tmp.Add,
                                        EditDel: ch_tmp.EditDel,
                                        Read: ch_tmp.Read,
                                    };

                                    Settings_s.push(item_s);

                                    if(FSettings_s[item.uid] == undefined) {
                                        FSettings_s[item.uid] = {};
                                    }

                                    FSettings_s[item.uid][item_cl.Class] = item_s;
                                })
                            }
                        });


                        console.log(Settings_s);

                        this.core.AJAX.addAccessForApp({
                            App: this.props.selectNode.uid,
                            Group: this.props.Group,
                            Settings: Settings_s,
                        }, () => {
                            //Тут мы проходим по всем узлам и расставляем по ссылке актуальные значения
                            this.props.selectNode.permission.Settings = Settings_s;
                            this.props.selectNode.permission.FSettings = FSettings_s;

                            this.setState({attrDataCache: {}});


                            ToastrStore.success('Success!');
                        }, () => {
                            ToastrStore.error('Error!');
                        });
                    }
                }
            }

        },

        build: {
            content: () => {
                let ClothHDR;
                let ClothForm;
                let ClothTables;
                let ClothFormTabs;
                let ClothTablesTabs;
                let ClothFTR;

                ClothForm = <div className={"zt_co_form"}>{this.core.build.form()}</div>;

                ClothHDR = <div className={"cof_hdr"}>
                    <div className={"cof_titlebar"}>
                        <div className={"cof_title"}>{this.props.name}</div>
                    </div>
                    <div className="cof_hdr_btns_co">
                        {!this.props.isMain ? (
                            <button className="cof_hdr_btn" onClick={()=>{
                                this.props.onClose();
                            }}><i className="fas fa-times" /></button>
                        ) : ""}
                    </div>
                </div>;
                ClothFTR = <div className={"cof_ftr"}>
                    <div className={"cof_ftr_fixed_btns_co"}>
                    </div>
                    <div className={"cof_ftr_btns_co"}>
                        <button className="cof_ftr_btn nf_btn-ellipse" onClick={(e)=>{
                            this.events.onSave(e, false);
                        }} >Save</button>
                        {!this.props.isMain ? (<React.Fragment>
                            <button className="cof_ftr_btn nf_btn-ellipse" onClick={(e)=>{
                                this.events.onSave(e, true);
                            }} >Save &amp; Close</button>
                            <button className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                this.props.onClose();
                            }}><i className="fas fa-times"/>Close</button>
                        </React.Fragment>):""}
                    </div>
                </div>;


                return (
                    <div className={"zt_form_content zt_modal_form"}>
                        {ClothHDR}
                        <div className={"zt_form_tabs_bar"}>
                            {ClothFormTabs}
                            {ClothTablesTabs}
                        </div>
                        {ClothForm}
                        {ClothTables}
                        {ClothFTR}
                    </div>
                );
            },
            form: () => {
                return this.core.build.PermViewsArea();

                //tabs functional

                function a11yProps(index: any) {
                    return {
                        id: `vertical-tab-${index}`,
                        'aria-controls': `vertical-tabpanel-${index}`,
                    };
                }
                return (
                    <div className={"zt_co_perm_form"}>
                        <Tabs
                            orientation="vertical"
                            variant="scrollable"
                            value={this.state.TabsSett}
                            onChange={this.events.onChangeTab}
                            aria-label="Vertical tabs example"
                            className={""}
                        >
                            <Tab label="Views" {...a11yProps(0)} />
                            <Tab label="Objects" {...a11yProps(1)} />

                        </Tabs>

                        {this.core.build.TabPanel({
                            children: this.core.build.PermViewsArea(),
                            index: 0,
                            value: this.state.TabsSett
                        })}
                        {this.core.build.TabPanel({
                            children: "",
                            index: 1,
                            value: this.state.TabsSett
                        })}
                    </div>);
            },

            TabPanel: (props) => {
                const { children, value, index, ...other } = props;

                return (
                    <div
                        className={"zt_permis_tab_content"}
                        role="tabpanel"
                        hidden={value !== index}
                        id={`vertical-tabpanel-${index}`}
                        aria-labelledby={`vertical-tab-${index}`}
                        {...other}
                    >
                        {value === index && children}
                    </div>);
            },

            PermViewsArea: () => {
                let PanelJSX;

                let treeData = this.props.selectNode ? [this.props.selectNode] :[];
                const dataViews = {
                    treeData: treeData,
                    onChangeState: (new_state) => {
                        if(new_state) {
                            this.setState(new_state)
                        }
                        else {
                            this.setState({});
                        }
                    },

                    get_attrDataCache: () => { return this.state.attrDataCache }
                };

                return (
                    <ViewsPermContent {...dataViews}/>
                );
            },
        }
    };

    helper = {
        resetCache: () => {},
        saveCache: () => {
        },
        //Для получение всех данных узла
        loopFindDataNode: (children, id) => {
            if(typeof children == "object") {
                for (let i = 0; i < children.length; i++) {
                    if(children[i].id == id) {
                        return children[i];
                    }
                    else if(typeof children[i].children == "object" && children[i].children!== null && children[i].children.length > 0) {
                        let sel_item = this.helper.loopFindDataNode(children[i].children, id);
                        if(sel_item !== null) {
                            return sel_item;
                        }
                    }
                    else if(typeof children[i].children != "object" ) {
                        debugger
                    }
                }
            }
            return null;
        }
    };


    render() {
        let isBlocking = this.state.isLoadModalContent || this.state.redrawWindow;

        let content;

        if(this.state.redrawWindow) {
            content = "";
        }
        else {
            content = this.core.build.content()
        }

        let Class_form = "";
        if(this.props.isMain) {
            Class_form += " app_window_cloth"
        }

        return (
            <BlockUi className={"zt_window_cloth zt_app_cloth" + Class_form} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                {content}
            </BlockUi>);
    }
}