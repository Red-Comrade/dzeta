import * as React from "react";
import { inject, observer } from "mobx-react";
import Select from 'react-select';
import * as ct_api_class from '../../../api/ClassTree-api'
import IViewParamProps from './IViewParamProps'

@inject("ApplicationStore")
@observer
export class DataSourceCtrl extends React.Component<IViewParamProps>{
  state={
    Options:[],
    cache:1,
    loaded:false,
  }
  loadCalc=()=>{
      if(this.props.Data.class && (this.state.cache!=this.props.Data.class.uid || !this.state.loaded)){
        ct_api_class.getCalcs(this.props.Data.class.uid).then((res)=>{
          let Options=res.data.data.map((item)=>{
            return {value:item.uid , label:item.name}
          });
          Options=[{value:null, label:'-Chose Calc-'}, ...Options];
          this.setState({Options:Options, cache:this.props.Data.class.uid, loaded:true})
        });
      }
    }
  changeCalc=(opt_data)=>{
    let ParamData={...this.props.Data};
    if(opt_data.value){
      ParamData.dopSett.data_source={uid:opt_data.value, name:opt_data.label};
    }else{
      ParamData.dopSett.data_source=null;
    }
    this.props.ApplicationStore.UpdateParamData(ParamData);
    if(this.props.tablechange){
      this.props.tablechange(ParamData);
    }
  }

  render(){
    let chosen_value={value:null, label:'-Chose Calc-'};
    if(this.props.Data.dopSett.data_source){
      chosen_value={value:this.props.Data.dopSett.data_source.uid, label:this.props.Data.dopSett.data_source.name};
    }
    return(
      <div className="data_source">
      <Select
          menuPosition={'fixed'}
          options={this.state.Options}
          value={chosen_value}
          onMenuOpen={this.loadCalc}
          onChange={this.changeCalc}
        />
      </div>
    );
  }
}
