
export default interface IViewParamProps {
    Data: {
      id:number,
      uid:string,
      name:string,
      datatype:string,
      class:{
        name:string,
        uid:string
      },
      dopSett:any,
    },
    tablechange?:(Data:object)=>void,
    ApplicationStore?:any,
}
