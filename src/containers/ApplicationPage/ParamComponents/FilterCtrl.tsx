import * as React from "react";
import { inject, observer } from "mobx-react";
import IViewParamProps from './IViewParamProps'

@inject("ApplicationStore")
@observer
export class FilterCtrl extends React.Component<IViewParamProps>{
  filterChange=(e)=>{
    let {id, value}=e.currentTarget;
    let ParamData={...this.props.Data};
    if(!ParamData.Filter || !ParamData.Filter.length){
      ParamData.Filter=[{op:'=',v:''}]
    }
    ParamData.Filter[0][id]=value;
    this.props.ApplicationStore.UpdateParamData(ParamData);
    if(this.props.tablechange){
      this.props.tablechange(ParamData);
    }
  }
  render(){
    const operation=['=', '!=', '>=', '<=', '~'];
    let operOpt=operation.map((item, index)=>{
      return <option key={'filter_'+index+'_'+this.props.Data.id} value={item}>{item}</option>;
    });
    let FilterData=this.props.Data.Filter && this.props.Data.Filter.length?this.props.Data.Filter[0]:{op:'=',v:''};//Only firs Data
    return(
      <div key={'filter_ctr_'+this.props.Data.uid} className="filter">
        <select id="op" onChange={this.filterChange} value={FilterData.op} className="form-control">{operOpt}</select>
        <input  id="v" onChange={this.filterChange} value={FilterData.v} className="form-control"/>
      </div>
    );
  }
}
