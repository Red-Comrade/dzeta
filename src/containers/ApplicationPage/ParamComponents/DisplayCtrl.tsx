import * as React from "react";
import { inject, observer } from "mobx-react";
import IViewParamProps from './IViewParamProps'

@inject("ApplicationStore")
@observer
export class DisplayCtrl extends React.Component<IViewParamProps>{
  isHideChange=(e)=>{
    let isCheck = e.currentTarget.checked;
    let newData={...this.props.Data};
    newData.dopSett.Hide=Number(isCheck);
    this.props.ApplicationStore.UpdateParamData(newData);
    if(this.props.tablechange){
      this.props.tablechange(newData);
    }
  }
  render(){
    return(
      <div className="isHide">
        <input checked={this.props.Data.dopSett.Hide}  onChange={this.isHideChange} type="checkbox" className="form-control"/>
      </div>
    );
  }
}
