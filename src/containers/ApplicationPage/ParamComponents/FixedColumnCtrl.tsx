import * as React from "react";
import { inject, observer } from "mobx-react";
import IViewParamProps from './IViewParamProps'

@inject("ApplicationStore")
@observer
export class FixedColumnCtrl extends React.Component<IViewParamProps>{
  fixedChange=(e)=>{
    let {id, value}=e.currentTarget;
    let ParamData={...this.props.Data};
    ParamData.dopSett[id]=value;
    this.props.ApplicationStore.UpdateParamData(ParamData);
    if(this.props.tablechange){
      this.props.tablechange(ParamData);
    }
  }
  render(){
    const Fixed=[{text:'None', value:0}, {text:'Fixed', value:1}];
    let fixedOpt=Fixed.map((item, index)=>{
      return <option key={'fixed_'+index+'_'+this.props.Data.id} value={item.value}>{item.text}</option>;
    });
    let FixedData=this.props.Data.dopSett.isFixed?this.props.Data.dopSett.isFixed:0;//Only firs Data
    return(
      <div key={'fixed_ctrl_'+this.props.Data.uid} className="isFixed">
        <select id="isFixed" onChange={this.fixedChange} value={FixedData} className="form-control">{fixedOpt}</select>
      </div>
    );
  }
}
