import * as React from "react";
import { inject, observer } from "mobx-react";
import IViewParamProps from './IViewParamProps'

@inject("ApplicationStore")
@observer
export class AliasCtrl extends React.Component<IViewParamProps>{
  fixedChange=(e)=>{
    let {id, value}=e.currentTarget;
    let ParamData={...this.props.Data};
    ParamData.dopSett[id]=value;
    this.props.ApplicationStore.UpdateParamData(ParamData);
    if(this.props.tablechange){
      this.props.tablechange(ParamData);
    }
  }
  render(){
    let AliasData=this.props.Data.dopSett.alias?this.props.Data.dopSett.alias:'';//Only firs Data
    return(
      <div key={'alias_ctrl_'+this.props.Data.uid} className="Alias">
        <input id="alias" onChange={this.fixedChange} value={AliasData} className="form-control"/>
      </div>
    );
  }
}
