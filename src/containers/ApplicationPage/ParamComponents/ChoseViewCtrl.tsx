import * as React from "react";
import { inject, observer } from "mobx-react";
import Select from 'react-select';
import IViewParamProps from './IViewParamProps'

@inject("ApplicationStore")
@observer
export class ChoseViewCtrl extends React.Component<IViewParamProps>{
  viewChange=(opt_data)=>{
    let ParamData={...this.props.Data};
    if(opt_data.value){
      ParamData.dopSett.view={uid:opt_data.value, name:opt_data.label};
    }else{
      ParamData.dopSett.view=null;
    }
    this.props.ApplicationStore.UpdateParamData(ParamData);
    if(this.props.tablechange){
      this.props.tablechange(ParamData);
    }
  }
  render(){
    let uid= this.props.ApplicationStore.AppViewSett.uid;
    let parrentView=this.props.ApplicationStore.parrentsView.filter((item)=>{
      return item.type!=='view' || uid==item.uid?false:true;
    }).map((item)=>{
      return {...item};
    })
    let chosen_value={value:null, label:'-Chose View-'};
    let operOpt=parrentView.map((item:any, index)=>{
      if(this.props.Data.dopSett.view && this.props.Data.dopSett.view.uid===item.uid){
        chosen_value={value:item.uid, label:item.name};
      }
      return {value:item.uid, label:item.name};
    });
    operOpt=[{value:null, label:'-Chose View-'}, ...operOpt];
    return(
      <div className="view">
      <Select
          menuPosition={'fixed'}
          options={operOpt}
          value={chosen_value}
          onChange={this.viewChange}
        />
      </div>
    );
  }
};
