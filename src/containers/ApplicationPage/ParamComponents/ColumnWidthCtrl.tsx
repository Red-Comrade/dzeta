import * as React from "react";
import { inject, observer } from "mobx-react";
import IViewParamProps from './IViewParamProps'

@inject("ApplicationStore")
@observer
export class ColumnWidthCtrl extends React.Component<IViewParamProps>{
  widthChange=(e)=>{
    let {id, value}=e.currentTarget;
    if(id=='unit'){
      return false;
    }
    let ParamData={...this.props.Data};
    if(!ParamData.dopSett.width){
      ParamData.dopSett.width={value:'', unit:'px'};
    }
    ParamData.dopSett.width[id]=value;
    if(!ParamData.dopSett.width.value){
      ParamData.dopSett.width=null;
    }
    this.props.ApplicationStore.UpdateParamData(ParamData);
    if(this.props.tablechange){
      this.props.tablechange(ParamData);
    }
  }
  render(){
    const units=['px'];
    let unitsOpt=units.map((item, index)=>{
      return <option key={'width_'+index+'_'+this.props.Data.id} value={item}>{item}</option>;
    });
    let WidthData=this.props.Data.dopSett.width?this.props.Data.dopSett.width:{value:'', unit:'px'};//Only firs Data
    return(
      <div key={'width_ctr_'+this.props.Data.uid} className="width">
        <input  id="value" onChange={this.widthChange} value={WidthData.value} className="form-control"/>
        <select id="unit" onChange={this.widthChange} value={WidthData.unit} className="form-control">{unitsOpt}</select>
      </div>
    );
  }
}
