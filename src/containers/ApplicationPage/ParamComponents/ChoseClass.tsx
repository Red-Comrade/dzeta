import * as React from "react";
import { inject, observer } from "mobx-react";

import CoolTree from '../../../components/CoolTree'
import * as ct_api from '../../../api/ClassTree-api'

interface IClassesTreeInstanceState {
    Settings: object;
    CM_target: HTMLElement;
    CM_content: object[];
    ShowFromLevelFunc?: (callback:(NodeID?)=>void)=> void,
    expandNodeFunc?: (callback:()=>void)=> string[],

    ModalTreeConfig: {
        show: boolean,
        TypeModal: string,
        data?: object,
        SuccessFunction?: () => void
    },

    AddBtnDisabled?: boolean;
    EditBtnDisabled?: boolean;
    DelBtnDisabled?: boolean;
}
interface IClassesTreeInstanceProps {
    ClassesStore?: {
        treeData: object[],
        UpdTreeDataAction: (newTreeData:object[]) => void,
        AddNode: (payload:object) => void,
        SelectNode?: any,
        SelectNodeForParamsAction: () => void,
        ActiveGroupID: any,
        IsOpenTable: boolean,
    },
}
interface IClassChoseProps{
  classFunction:{
    onBlur:()=>void
    onChose:(data:any)=>void
  }
}




@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export class ChoseClass extends React.Component<IClassChoseProps>{
  static defaultProps = {
      isOpen: null,
      isFullLoad: true, //
      treeData:[],
  };
  state: Readonly<IClassesTreeInstanceState> = {
      Settings: null,
      CM_target: null,
      CM_content: [
          {
              "label": "Создать",
              "action": ()=> {},
              "icon_className": 'fa fa-plus',
              "children": [
                  {
                      "label": "Create",
                      "action": ()=> {},
                      "icon_className": 'glyphicon glyphicon-th',
                      "children": false,
                  },
                  {
                      "label": "Edit",
                      "action": ()=> {},
                      "icon_className": 'glyphicon glyphicon-th',
                      "children": false
                  },
                  {
                      "label": "Delete",
                      "action": ()=> {},
                      "icon_className": 'glyphicon glyphicon-th',
                      "children": false
                  }
              ]
          }
      ],
      ShowFromLevelFunc: null,

      ModalTreeConfig: {
          show: false,
          TypeModal: "create",
          data: {}
      },

      AddBtnDisabled: false,
      EditBtnDisabled: false,
      DelBtnDisabled: false,
  };

  constructor(props: IClassesTreeInstanceProps) {
      super(props);
  };
  componentDidMount(): void {
      this.core.GetTree((data_tree) => {

          //МНЕ Проще написать функцию преобразования, чем разбираться в коде на PHP, нихрена не понятно...

          let expandedKeys = [];
          const fillAttrFuncRec = (Children, ParentIDTree, ParentID) => {
              for( let key in Children) {
                  let Param = Children[key];

                  Param.Name = Param.name;
                  Param.id = ParentIDTree + '|' + Param.uid;
                  Param.ParentIDTree = ParentIDTree;
                  Param.key = Param.id;
                  if(ParentIDTree) {
                      //проверка на корень
                      Param.icon = 'fas fa-file-invoice';
                  }
                  else {
                      Param.key = '|0';
                      Param.id = '|0';
                      expandedKeys.push(Param.id);
                      Param.icon = 'fas fa-sitemap';
                  }

                  Param.ParentID = ParentID;
                  Param.iddb = Param.uid;
                  Param.ParrentTypeID = ParentID;
                  Param.TypeID = Param.uid;

                  if(Param.children.length > 0) {
                      fillAttrFuncRec(Param.children, Param.id, Param.iddb);
                  }
                  else {
                      Param.children = null;
                  }
              }
          };
          fillAttrFuncRec(data_tree, "", "");

          this.props.ApplicationStore.UpdTreeDataClassAction(data_tree);

          let FuncExpand = () => {
              this.setState({expandNodeFunc: null});
              return expandedKeys;
          };

          this.setState({expandNodeFunc: FuncExpand});
      });
  };


  core = {
      GetTree: function (SuccFunc) {
          ct_api
              .getTree()
              .then(res => {
                  console.error(res);
                  SuccFunc(res.data);
              })
              .catch(function (error) {
                  console.log(error);
              })
      },
  };

  blurHandler=(e)=>{
    var el=e.relatedTarget, parentsClasses=[];
    while(el){
      parentsClasses.push(el.className);
      el=el.parentNode;
    }
    if(parentsClasses.indexOf('ClassChoseTree')!==-1){
      return false;
    }
    this.props.classFunction.onBlur();

  }
  render(){
    const DataCoolTree = {
        RootTab: {
            ID: "T00000000-0000-0000-0000-000000000000",
            Name: "Classes",
        },

        treeData: this.props.ApplicationStore.treeDataClass,
        isFullLoad: true,
        selectNodeFunc: null,
        expandNodeFunc: this.state.expandNodeFunc,
        ShowFromLevelFunc: this.state.ShowFromLevelFunc,

        //Кнопки
        AddBtnDisabled: true,
        EditBtnDisabled: true,
        DelBtnDisabled: true,
        CustomBtn:[
        /*  {
            classIcon:'fa fa-check',
            events:{
              onClick:(e)=>{
                  if(this.props.ApplicationStore.SelectNodeClass){
                      let data ={
                        name:this.props.ApplicationStore.SelectNodeClass.name,
                        uid:this.props.ApplicationStore.SelectNodeClass.uid
                      };
                      this.props.classFunction.onChose(data);
                  }

              }
            },
          }*/
        ],

        onSelect: (selectData) => {
          if(selectData.id!=='|0'){
            let data ={
              name:selectData.name,
              uid:selectData.uid
            };
            this.props.classFunction.onChose(data);
          }else{
            this.props.ApplicationStore.setSelectedClassTree(null);
          }
        },
        onSelectR: (selectData) => {

        },
        onOpenCM: (params: {SelectData:any, callbackCloseCM:()=>void, IsBtnCM?:boolean}, callback) => {
            let contentCM = [];
            return contentCM;
        },
        onExpandNode: (node_info) => {},
    };

    return(<div onBlur={this.blurHandler} tabIndex="0" className="ClassChoseTree">
            <CoolTree {...DataCoolTree} />
      </div>);

  }
}
