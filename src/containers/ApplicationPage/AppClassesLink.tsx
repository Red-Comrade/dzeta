import * as React from "react";
import { Modal, Button, Table } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/AppTree-api";

import {ClassRelation} from '../../components/ClassRelation'


@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export class AppClassesLink extends React.Component {


    constructor(props) {
        super(props);
        this.state={
            blocking:false,
        };
    }

    onShow=()=>{

    };
    handleClose = () => {
       this.props.ApplicationStore.setShowModalSave(false);
    };
    handleSave= ()=>{
        let uidView=this.props.ApplicationStore.SelectNode.iddb;
        let uidApp=this.props.ApplicationStore.SelectNode.appUid;
        let classesLink= this.props.ApplicationStore.classLinkData.map((item)=>{
          return {MainClass:item.chose, Class:item.uid};
        });
        let DataParam=this.props.ApplicationStore.AppParamData.map((item)=>{
          if(item.Filter && item.Filter.length && !item.Filter[0].v){
            item.Filter=[];
          }
          return {...item, TypeOfData:item.datatype, class:{...item.class}};
        });

        Promise.all([ct_api.addViewClass(uidView, uidApp, classesLink), ct_api.attrAdd(uidView, DataParam)]).then(([linkResult, paramResult])=>{
            if(linkResult.data.result===1 && paramResult.data.result===1 ){
              this.props.ToastrStore.success('Success', 'View saved successfully!');
              this.updateDataView(uidView);
              let newParam=DataParam.find((item)=>{
                return !item.uid?true:false;
              })
              if(newParam){
                this.updateFullParam();
              }
              this.handleClose();
            }else{
                this.props.ToastrStore.error('Error!', 'Error');
            }

        }).catch( (error) => {
            console.log(error);
            this.props.ToastrStore.error('Server error!', 'Error');
        });

    };

    updateDataView=(uidView)=>{
      ct_api.getAttributesForApp(uidView).then((data) => {
          if(data.data.result === 1) {
              console.log(data)
              this.props.ApplicationStore.UpdParamsTrigAction(false, data.data.data);
          }
          else {
              this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.data.result);
              this.props.ApplicationStore.UpdParamsTrigAction(false, []);

          }
      });
    };
    updateFullParam=()=>{
      ct_api.getFullAttrForApp().then((data) => {
          if(data.data.result === 1) {
              this.props.ApplicationStore.SetFullParamData(data.data.data);
          }
          else {
              this.props.ToastrStore.error('Server error!', 'ERROR CODE:' + data.data.result);
          }
      });
    };

    renderBody=()=>{
        if(this.props.ApplicationStore.AppParamData){
          let Classes=this.props.ApplicationStore.AppParamData.map((item)=>{
            return item.class;
          });
          let uniqClass=[];
          Classes=Classes.filter((item)=>{
            if(item && uniqClass.indexOf(item.uid)===-1){
              uniqClass.push(item.uid);
              return true;
            }
            return false;
          });

          let that=this;
          let DataFunction={
            onLoad:function(){
              var uidView=that.props.ApplicationStore.SelectNode.iddb;
              ct_api.getViewClass(uidView).then(res => {
                  let newData= this.state.Data.map((item)=>{return {...item}});
                  for (let i = 0; i < res.data.data.length; i++) {
                     let elem= newData.find((item)=>{
                       return item.uid===res.data.data[i].Class?true:false;
                     })
                     if(elem){
                       elem.chose=res.data.data[i].MainClass;
                     }
                  }
                  this.setState({Data:newData}, ()=>{
                    that.props.ApplicationStore.setClassLinkData(newData);
                  })
              })
              .catch( (error) => {
                  console.log(error);
                  that.props.ToastrStore.error('Server error!', 'Error');
              })
            },
            onChange:function(e, uid){
              let {value} = e.currentTarget
              let newData= this.state.Data.map((item)=>{
                  if(item.uid===uid){
                    item.chose=value;
                  }
                  return item;
              });
              this.setState({Data:newData}, ()=>{
                that.props.ApplicationStore.setClassLinkData(this.state.Data);
              });
            }
          }
          return <ClassRelation DataFunction={DataFunction}  Classes={Classes} Data={null} />
        }
        return null;

    }

    render() {
        let Title='Classes relation';
        let Body=this.renderBody();
        let Footer=<div>
            <Button variant="success" onClick={this.handleSave}>
                Save
            </Button>
            <Button variant="secondary" onClick={this.handleClose}>
                Close
            </Button>
        </div>;
        return (
            <Modal  size="lg" onShow={this.onShow} animation={true} show={this.props.ApplicationStore.showSaveModal} onHide={this.handleClose}>
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}
