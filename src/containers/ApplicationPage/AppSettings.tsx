import * as React from "react";
import {inject, observer} from "mobx-react";
import CreatableSelect from 'react-select/creatable';
import Select, { components } from 'react-select';

import * as ct_api from '../../api/AppTree-api'
import * as ct_api_class from '../../api/ClassTree-api'


import InputColor from 'react-input-color';



interface IAppSettingsProps {
  appUid:string,
  appTitle:string
  ApplicationStore?:any,
  ToastrStore?:any,
}

@inject("ApplicationStore")
@inject("ToastrStore")
@observer
export class AppSettings  extends React.Component<IAppSettingsProps> {

  renderBtn=()=>{

    let icon=<i className="fas fa-tablet-alt"></i>;

    return <div className="appsett__ctrlpanel ctrlpanel">
      <div className="ctrlpanel__info info">
        <h3>{icon} {this.props.appTitle}</h3>
      </div>
      <div className="ctrlpanel__btn">
        <button onClick={this.saveApp} className="btn btn-primary btn-sm">Save <i className="fa fa-save" aria-hidden="true"></i></button>
      </div>

    </div>
  }
  saveApp=()=>{
    let AppSett=JSON.parse(JSON.stringify(this.props.ApplicationStore.AppSett));
    console.log(AppSett);
    const buildFormData =  function(formData, data, parentKey) {
        if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
          Object.keys(data).forEach(key => {
            buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
          });
        } else {
          const value = data == null ? '' : data;

          formData.append(parentKey, value);
        }
    }
    var AppSettFormData= new FormData();
    buildFormData(AppSettFormData, AppSett, null);
    if(this.props.ApplicationStore.AppSett.app_settings.icon_settings && this.props.ApplicationStore.AppSett.app_settings.icon_settings.file){
      AppSettFormData.append('file', this.props.ApplicationStore.AppSett.app_settings.icon_settings.file);
    }

    ct_api.editSettings(AppSettFormData).then((res)=>{
      if(res.data.result==1){
        this.props.ToastrStore.success('Success', 'View saved successfully!');
      //  this.updateDataView(ViewSett.uid);
      }
    })

  }

  render(){
    return <div className="appsett">
      {this.renderBtn()}
      <div  className="appsett__workarea workarea">
          <AppSettWorkArea appUid={this.props.appUid}/>
      </div>
    </div>
  }
}

interface IAppSettWorkAreaProps {
  appUid:string,
  ApplicationStore?:any,
}

interface IAppSettWorkAreaState {
  tabs:string,
}
@inject("ApplicationStore")
@inject("ToastrStore")
@observer
class AppSettWorkArea extends React.Component<IAppSettWorkAreaProps, IAppSettWorkAreaState>{
  state={
    tabs:'main_sett',
  }

  clickTabs=(id)=>{
    this.setState({tabs:id});
  }


  changeSett=(e)=>{
    let {name, value}= e.currentTarget;
    let AppSett= this.props.ApplicationStore.AppSett;
    if(name=='app_color' && value.length==6){
      let valid_value_color = '#ffffff';
      if(!/^#[0-9A-F]{6}$/i.test(value)) {
          value = valid_value_color;
      }
    }
    AppSett.app_settings[name]=value;
    this.props.ApplicationStore.setDataAppSett(AppSett);
  }

  changeIcon=(file)=>{
    let AppSett= this.props.ApplicationStore.AppSett;
    if(file){
      AppSett.app_settings.icon_settings={
          file:file,
          RealName:file.name,
      };

    }else{
      AppSett.app_settings.icon_settings=null;
    }
    this.props.ApplicationStore.setDataAppSett(AppSett);
  }

  getMainSett=(AppSett)=>{
    let color=AppSett.app_settings.app_color?AppSett.app_settings.app_color:'#ffffff';
    return(<React.Fragment>
        <div className="settings__setcont setcont">
          <div className="row">
            <label className="col-lg-6">Color</label>
            <div className="col-lg-6">
              <div className="colorpick_ctrl">
                  <input  name="app_color" className="form-control" value={color} onChange={this.changeSett}/>
                  <InputColor initialValue={color} onChange={(setColor)=>{
                      this.changeSett({currentTarget:{name:'app_color', value:setColor.hex}})
                  }} />
              </div>
            </div>
          </div>
        </div>

        <div className="settings__setcont setcont">
          <div className="row">
            <label className="col-lg-6">Icon</label>
            <div className="col-lg-6">
              <IconFileInput data={AppSett.app_settings.icon_settings} onChange={this.changeIcon} onRemove={this.changeIcon} />
            </div>
          </div>
        </div>

      </React.Fragment>)

  }

  renderTabs=()=>{
    const tabs=[
      {name:'Common Settings', id:'main_sett'},
    ];
    let li_items=tabs.map((item)=>{
        return <li key={'tabs_'+item.id} onClick={()=>{this.clickTabs(item.id)}} className={'tabs__item item '+(this.state.tabs==item.id?'selected':'')}>{item.name}</li>
    });
    return <ul className="app__tabs tabs">
      {li_items}
    </ul>
  }
  renderBody=()=>{
    let Content;
    let AppSett= this.props.ApplicationStore.AppSett;
    if(this.state.tabs==='main_sett'){
      Content=this.getMainSett(AppSett);
    }
    return <div key={'table_params'+this.props.appUid} className="app__body body">
      {Content}
    </div>
  }

  render(){
    return <div className="workarea__settings settings"><div className="app_work_sett">
      {this.renderTabs()}
      {this.renderBody()}
    </div></div>
  }
}

interface IIconFileInputProps {
  data:{RealName:string, GenName:string, file?:any},
  onChange:(file)=>void,
  onRemove:(file)=>void,
}
class IconFileInput extends React.Component<IIconFileInputProps>{
    private iconInput: React.RefObject<HTMLInputElement>;
    private canvas: React.RefObject<HTMLCanvasElement>;
    constructor(props) {
       super(props);
       this.iconInput = React.createRef();
       this.canvas = React.createRef();
   }

  componentDidMount(){
    var ctx = this.canvas.current.getContext('2d');
    ctx.clearRect(0,0,100,100);
    if(this.props.data && this.props.data.file){
      var img = new Image;
      img.src = URL.createObjectURL(this.props.data.file);
      img.onload = function() {
          ctx.drawImage(img, 0, 0, 100, 100);
      }
    }
    if(this.props.data && this.props.data.GenName){
      var img = new Image;
      img.src='/object/getPreviewFile/'+ this.props.data.GenName;
      img.onload = function() {
          ctx.drawImage(img, 0, 0, 100, 100);
      }
    }
  }

  componentDidUpdate(){
    var ctx = this.canvas.current.getContext('2d');
    ctx.clearRect(0,0,100,100);
    if(this.props.data && this.props.data.file){
      var img = new Image;
      img.src = URL.createObjectURL(this.props.data.file);
      img.onload = function() {
          ctx.drawImage(img, 0, 0, 100, 100);
      }
    }
    if(this.props.data && this.props.data.GenName){
      var img = new Image;
      img.src='/object/getPreviewFile/'+ this.props.data.GenName;
      img.onload = function() {
          ctx.drawImage(img, 0, 0, 100, 100);
      }
    }
  }

  changeIcon=(e)=>{
    var ctx = this.canvas.current.getContext('2d');
    var img = new Image;
    img.src = URL.createObjectURL(e.currentTarget.files[0]);
    img.onload = function() {
        ctx.clearRect(0,0,100,100);
        ctx.drawImage(img, 0, 0, 100, 100);
    }
    this.props.onChange(e.currentTarget.files[0]);
  }
  removeFile=(e)=>{
    var ctx = this.canvas.current.getContext('2d');
    this.iconInput.current.value='';
    ctx.clearRect(0,0,100,100);
    this.props.onRemove(null);
  }

  render(){
    let data= this.props.data;
    return(<div className="iconfile_ctrl">
          <input style={{display:'none'}} onChange={this.changeIcon} accept="image/*" type="file" ref={this.iconInput}/>
          <div className="upload_btn">
            <button onClick={()=>{this.iconInput.current.click()}} className="btn btn-primary btn-sm">{(data && data.RealName?'Change':'Upload')} <i className="fa fa-upload"></i></button>
          </div>
          <div className="file_name">
            <label>{(data && data.RealName?data.RealName:'')}</label>
          </div>
          <div className="icon_preview">
            <canvas ref={this.canvas} width={100} height={100}/>
          </div>
          <div className="remove_btn">
            {data && <button onClick={this.removeFile} className="btn btn-danger btn-sm"><i className="fa fa-times"></i></button>}
          </div>
    </div>)
  }
}
