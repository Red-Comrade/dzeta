import * as React from "react";
import { Modal, ProgressBar } from 'react-bootstrap';

import { getProgress } from '../api/Progress-api';

interface IProgressModalState {
    progress: number,
    timerId?: number,
    animated: boolean,
}

interface IProgressModalProps {
    uid: string,
    interval: number,
    show: boolean,
    title: string,
    onShow?:() => any,
    onClose?: () => any,
}

export default class ProgressModal extends React.Component<IProgressModalProps, IProgressModalState> {
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            timerId: null,
            animated: true,
        };
    }

	render() {
        let Body;
        if (this.props.body) {
            Body = (
                <>
                    <ProgressBar animated={this.state.animated} now={this.state.progress} label={`${this.state.progress}%`} />
                    {this.props.body}
                </>
            );
        } else {
    		Body = (
    			<ProgressBar animated={this.state.animated} now={this.state.progress} label={`${this.state.progress}%`} />
    		);
        }
        let Footer;
        if (this.props.footer) {
            Footer = this.props.footer;
        }
        return (
            <Modal
                onShow={
                    () => {
                        const timerId = setInterval(() => {
                            getProgress(this.props.uid)
                                .then((response) => {
                                    console.log(response);
                                    if (response.data) {
                                        this.setState({
                                            progress: response.data.progress > 100 ? 100 : response.data.progress,
                                            animated: response.data.progress >= 100 ? false : true,
                                        });
                                        if (response.data.progress >= 100) {
                                            clearInterval(this.state.timerId);
                                        }
                                    }
                                })
                        }, this.props.interval);
                        this.setState({
                            timerId: timerId
                        });
                        if (typeof this.props.onShow === 'function') {
                            this.props.onShow();
                        }
                    }
                }
                animation={true}
                show={this.props.show}
                onExit={
                    () => {
                        clearInterval(this.state.timerId);
                        this.setState({
                            progress: 0
                        });
                        if (typeof this.props.onClose === 'function') {
                            this.props.onClose();
                        }
                    }
                }
            >
                <Modal.Header>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>{Body}</Modal.Body>
                <Modal.Footer>{Footer}</Modal.Footer>
            </Modal>
        );
	}
}