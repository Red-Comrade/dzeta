import React from 'react';
import ReactCustomFlagSelect from 'react-custom-flag-select';
import "react-custom-flag-select/lib/react-custom-flag-select.min.css";
import STYLES from 'react-custom-flag-select/lib/react-custom-flag-select.css';
import {FLAGS} from './img.js';
import i18next from 'i18next';
import Cookies from 'universal-cookie';

const FLAG_SELECTOR_OPTION_LIST = [
  {
    id: "0",
    name: "ENG",
    displayText: "ENG",
    locale:'en',
    flag: FLAGS.ENG
  },
  {
    id: "1",
    name: "FRA",
    displayText: "FRA",
    locale:'fr',
    flag: FLAGS.FRA
  },
  {
    id: "2",
    name: "RUS",
    displayText: "RUS",
    locale:'ru',
    flag: FLAGS.RUS
  },
];

class Lang extends React.Component {
  constructor(props) {
    super(props);
    const curLang=(i18next.language ||(typeof window !== 'undefined' && window.localStorage.i18nextLng) || ( (document.location.host == "form.enemat.fr" || document.location.host == "enemat.misnik.by") ?"fr" : "en") ||'en');
    const idLang=FLAG_SELECTOR_OPTION_LIST.find(lng => lng.locale === curLang);
    this.state={langValue:idLang?idLang.id:'0'};


      i18next.on('languageChanged', (lng)=>
      {
          const idLang=FLAG_SELECTOR_OPTION_LIST.find(lng1 => lng1.locale === lng);
          this.setState({
              langValue: idLang?idLang.id:'0'
          });
          const cookies = new Cookies();

          localStorage.setItem('i18nextLng', idLang?idLang.locale:'fr');
          cookies.set('dzetalang', idLang?idLang.locale:'en', { path: '/' });
      });
  }

    changeLang = (event) => {
      i18next.changeLanguage(FLAG_SELECTOR_OPTION_LIST[event].locale);
    };

  render() {

		return (
              <ReactCustomFlagSelect
               value={this.state.langValue}
               showArrow={true}
               animate={true}
               selectHtml={
                 <div className={STYLES['select__dropdown']}>
                   <div className={STYLES['select__dropdown-flag']}>
                     <img src={FLAG_SELECTOR_OPTION_LIST[this.state.langValue].flag} style={{ width: '100%', height: '100%', verticalAlign: 'middle' }} />
                   </div>
                 </div>
               }
               customStyleContainer={{ border: "none", fontSize: "12px" }}
               customStyleOptionListItem={{height:"35px"}}
               customStyleSelect={{ width: '100px' }}
               customStyleOptionListContainer={{ maxHeight: '150px', overflow: 'auto', width: '100px', marginTop: '8px' }}
               optionList={FLAG_SELECTOR_OPTION_LIST}
               onChange={this.changeLang}
             />
 	  );
	}

}

export default Lang
