import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import { withTranslation } from 'react-i18next';
import Select, { Option } from 'rc-select';
import download from 'js-file-download';
import 'rc-select/assets/index.less';

import '../ImportPage/ImportPage.scss';

import { getExcelData } from '../../api/Import-api';
import { verifyTaxes } from '../../api/StepForm-api';
import { generateUid } from '../../utils';

import CoolTable from '../../components/CoolTable';
import ImportFileModal from '../ImportPage/ImportFileModal';
import ProgressModal from '../ProgressModal';
import RootStore from "../../stores/RootStore";

@inject("TaxesStore")
@inject("ToastrStore")
@observer
class TaxesPageForm extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            blocking: false,
            showFileModal: false,
            showProgressModal: false,
			importUid: null,
        };
    }
    componentDidMount(): void {
		document.title = 'Taxes | ' + RootStore.config.nameSystem;
	}

	loadFileHandler = () => {
		this.setState({
			showFileModal: true,
		});
	};

	verifyHandler = () => {
		const uid = generateUid();
		this.setState({
			blocking: true,
			showProgressModal: true,
			importUid: uid,
		});
		verifyTaxes(uid, this.props.TaxesStore.file, this.props.TaxesStore.settings)
	        .then((repsponse) => {
	            this.setState({
					blocking: false,
					showProgressModal: false,
				});
	            download(repsponse.data, 'verify.xlsx');
	        });
	};

	selectParam = (column, attribute) => {
		this.props.TaxesStore.updateColumnAttribute(column, attribute);
	}

	renderToolbar = () => {
		return 	(
			<div className="pp_tollbar_tools" style={{padding: "10px"}}>
				<button onClick={this.loadFileHandler}  title="Choose file" className="nf_btn-ellipse">
					<i className="fa fa-file"></i> Choose
				</button>
				<button onClick={this.verifyHandler} disabled={this.props.TaxesStore.file ? false : true}  title="Run import" className="nf_btn-ellipse">
					<i className="fa fa-file-import"></i> Import
				</button>
			</div>
		);
	};

	renderTable = () => {
		if (this.props.TaxesStore.settings.length) {
			const columns = [];
			const paramsRow = { trKey: -1, trCustomData: { selectParam: true }, tds: [] };
			for (let i = 0; i < this.props.TaxesStore.settings.length; i++) {
				columns.push({
					key: this.props.TaxesStore.settings[i].column,
					IsFixedWidth: false,
					MinWidth: 100,
					name: this.props.TaxesStore.settings[i].column
				});
				paramsRow.tds.push({ name: '', column: this.props.TaxesStore.settings[i].column });
			}
			const data = [];
			data.push(paramsRow);
			for (let i = 0; i < this.props.TaxesStore.data.length; i++) {
				const tds = [];
				for (let j = 0; j < this.props.TaxesStore.data[i].length; j++) {
					tds.push({ name: this.props.TaxesStore.data[i][j] })
				}
				data.push({
                    trKey: i,
                    trCustomData: this.props.TaxesStore.data[i],
                    tds: tds
                });
			}
			const tableData = {
	            settings: {
	                data: data,
	            },
	            headerData: columns,
	            callbackRenderTD: (dataTD, dataTR) => {
	            	if (dataTR.trCustomData.selectParam) {
	            		return (
	            			<Select
	            				dropdownClassName="import-param-select-dropdown"
	            				optionLabelProp="inputvalue"
	            				styleName="import-param-select"
	            				showSearch={true}
	            				onSelect={(value) => { this.selectParam(dataTD.column, value) }}
	            			>
	            				<Option value={null} title="Select attribute">Select attribute</Option>
	            				{
	            					this.props.TaxesStore.params.map((param) => {
		            					return (
		            						<Option key={param} inputvalue={param} value={param} title={param}>
		            							<span className="param-name">{param}</span>
		            						</Option>
		            					);
		            				})
	            				}
	            			</Select>
	            		)
	            	} else {
		                return dataTD.name;
	            	}
	            },
	            callbackRenderTH: (dataTH) => {
	                return dataTH.name;
	            },
	        };
	        return (
	        	<CoolTable {...tableData} />
	        );
		}
		return null;
	};

	render() {
		const { t } = this.props;
    	const loader = <h4 className="loader-text">Load file...</h4>; //<Loader active type="ball-pulse"/>;

		return (
	        <BlockUi id="zt_TaxesPage" tag="div" loader={loader} blocking={false}>
	        	{this.renderToolbar()}
	            {this.renderTable()}
	            <ImportFileModal
	            	show={this.state.showFileModal}
	            	filename={this.props.TaxesStore.filename}
	            	onClose={() => { this.setState({ showFileModal: false }) }}
	            	onLoad={() => {
	    				this.props.TaxesStore.updateDataAndSettings([], []);
	            		getExcelData(this.props.TaxesStore.file)
							.then((data) => {
		            			if (data.result != 1) {
		            				this.props.ToastrStore.error(t('errors.' + data.result), 'Error!');
		            				this.props.TaxesStore.updateDataAndSettings([], []);
								} else {
									this.props.TaxesStore.updateDataAndSettings(data.data.columns, data.data.sheet);
									this.setState({ showFileModal: false });
								}
		            		});
	            	}}
	            	onChoose={(file) => {
	            		this.props.TaxesStore.updateFile(file);
	            	}}
	            	onRemove={() => {
	            		this.props.TaxesStore.updateFile(null);
	            	}}
	            />
	            <ProgressModal
	            	show={this.state.showProgressModal}
	            	title="Import file"
	            	uid={this.state.importUid}
	            	interval={1000}
	            />
	        </BlockUi>
		);
	}
}

const TaxesPageTranslation = withTranslation('import')(TaxesPageForm);

export function TaxesPage(props) {
  return (
    <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
      <TaxesPageTranslation {...props} />
    </React.Suspense>
  );
}