import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect, useParams, useRouteMatch, NavLink } from 'react-router-dom'
import { inject, observer } from "mobx-react";

import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import * as h_api from '../../api/Home-api'

export interface IObjectsPageProps {
    ToastrStore?: any,
    RootStore?: any,
}
interface IObjectsPageState {
    blocking: boolean;

    Applications: any[],
}
@inject("ToastrStore")
@inject("RootStore")
@observer
export class HomePage extends React.Component<IObjectsPageProps, IObjectsPageState> {
    state: Readonly<IObjectsPageState> = {
        blocking: true,
        Applications: []
    };

    constructor(props: IObjectsPageProps) {
        super(props);
    };

    componentDidMount(): void {
        document.title = 'Home | ' + this.props.RootStore.config.nameSystem;
        this.core.GenerateContentHomeAjax();
    }

    core = {
        GenerateContentHomeAjax: () => {
            this.setState({blocking:true});
            this.setState({
                blocking:false,
            });

            return;

            h_api.getHomeApp()
                .then(res => {
                    let Applications = [];

                    if(res.data[0] != undefined && res.data[0].children.length > 0) {
                        res.data[0].children.forEach( (item) => {
                            if(item.Read) {
                                if(document.location.host == "form.enemat.fr" && this.props.RootStore.MSettings?.UserName != "Administrator") {
                                    if(item.uid == "39033974-54e5-4b65-9304-21866be48550") {
                                        Applications.push(item);
                                    }
                                }
                                else {
                                    Applications.push(item);
                                }
                            }
                        });
                    }

                    this.setState({
                        blocking:false,
                        Applications: Applications,
                    });
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                    this.setState({blocking: false, Applications:[]});
                })
        },


        RenderListApplications: () => {
            let JSX_appl = [];
            let folders = [];
            this.props.RootStore.Applications.forEach( (item_f, index_f) => {
                if(item_f.isFolder) {
                    let JSX_appl_folder = [];
                    item_f.children.forEach( (item, index) => {
                        if(item.Read) {
                            let img = <i className="fas fa-tablet-alt"/>;
                            let style = {};
                            if(item?.Settings?.icon_settings?.GenName) {
                                img = <img className={"img-icon"} alt={""} src={"/object/getPreviewFile/" + item.Settings.icon_settings.GenName} />
                            }
                            if(item?.Settings?.app_color) {
                                style["backgroundColor"] = item.Settings.app_color;
                            }
                            JSX_appl_folder.push(<li key={index} className={"home-feature-tile"}>
                                <NavLink style={style} to={"/view/"+item.uid} role="link" title={item.name} className={"home-feature-link home-feature"}>
                                    <div className={"home-resource-icon"}>{img}</div>
                                    <div className={"home-feature-name"}>{item.name}</div>
                                </NavLink>
                            </li>)
                        }
                    });

                    folders.push(
                        <React.Fragment key={index_f}>
                            <header className="home-title">{item_f.name}</header>
                            <ul className={"home-section home-feature-tile-container"}>{JSX_appl_folder}</ul>
                        </React.Fragment>
                    )
                }
                else {
                    let img = <i className="fas fa-tablet-alt"/>;
                    let style = {};
                    if(item_f?.Settings?.icon_settings?.GenName) {
                        img = <img className={"img-icon"} alt={""} src={"/object/getPreviewFile/" + item_f.Settings.icon_settings.GenName} />
                    }
                    if(item_f?.Settings?.app_color) {
                        style["backgroundColor"] = item_f.Settings.app_color;
                    }
                    JSX_appl.push(<li key={index_f} className={"home-feature-tile"}>
                        <NavLink style={style} to={"/view/"+item_f.uid} role="link" title={item_f.name} className={"home-feature-link home-feature"}>
                            <div className={"home-resource-icon"}>{img}</div>
                            <div className={"home-feature-name"}>{item_f.name}</div>
                        </NavLink>
                    </li>)
                }
            });

            let ApplJSX;
            if(JSX_appl.length > 0) {
                ApplJSX = <React.Fragment>
                    <header className={"home-title"}>Applications</header>
                    <ul className={"home-section home-feature-tile-container"}>
                        {JSX_appl}
                    </ul>
                </React.Fragment>;
            }

            return <React.Fragment>
                {folders}
                {ApplJSX}
            </React.Fragment>;
        },
    };

    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
      //  LoaderBlockUI = <h4 className="loader-text">Loading...</h4>;

        return (
            <BlockUi id="zt_HomePage" tag="div" loader={LoaderBlockUI} blocking={this.state.blocking}>
                <div className={"HomeContainer"}>
                    {this.core.RenderListApplications()}
                </div>
            </BlockUi>
        )
    }
}