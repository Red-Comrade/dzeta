import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/ClassTree-api";
import * as ot_api from "../../api/ObjTree-api";
import INodeTemplate from "./Interfaces/INodeTemplate";
import {IValue} from "../../api/ObjTree-api";
import {IValueFile} from "../../api/ObjTree-api";
import {parse, format} from "date-fns";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as f_helper from "../../helpers/form_datatype_helper"
import ToastrStore from "../../stores/ToastrStore";
import IitemAttr from "./Interfaces/IitemAttr";
import axios from "axios";
import ItemAttr from "./Interfaces/IitemAttr";
import RootStore from "../../stores/RootStore";


interface IHelperModalProps {
    show:boolean;
    TypeModal: "del" | "GenPassword" | "copy" | "inf",

    ObjID?: string,
    TypeID?: string,
    Name?: string,

    ToNode?: any,

    ToastrStore?: any;
    CloseCallback: () => void,
    onDeleteNode?: (ObjID:string) => void,
    addedObject?: (config:{newNode:{ObjID: string, Name:string}, selectData}) => void,
}
interface IHelperModalState {
    blocking: boolean,

    //copy obj
    attrData: ItemAttr[],
    attrDataCache: object,
    attrDataFileCache: object,
    isCopyChild: boolean,

    CountDelObj: any,
    GenPassword: {
        value?: string
    },

    OwnerName: string,
    CreationDate: string,
    attrDataInf?: {[keyAttr:string] : any }
}

const ContainerModal = document.getElementById('root_theme');

export default class HelperModal extends React.Component<IHelperModalProps, IHelperModalState> {
    state: Readonly<IHelperModalState> = {
        blocking: false,
        attrData: [],
        attrDataCache: {},
        attrDataFileCache: {},
        isCopyChild: true,

        CountDelObj: 0,
        GenPassword: {
            value: ""
        },

        OwnerName: null,
        CreationDate: null,
        attrDataInf: {},
    };
    xhrGenerateModalContent = {cancel: null};


    constructor(props) {
        super(props);
    }


    core = {
        delObject: function (uid:string, SuccFunc) {
            ot_api
                .delObject(uid)
                .then(res => {
                    console.error(res);
                    SuccFunc(res.data);
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        getDeleteCount: function (uid:string, SuccFunc) {
            ot_api
                .getDeleteCount(uid)
                .then(res => {
                    console.error(res);
                    SuccFunc(res.data);
                })
                .catch(function (error) {
                    console.log(error);
                })
        },

        getInfObject: function (uid:string, SuccFunc, ErrFunc) {
            ot_api
                .getInfObject(uid)
                .then(res => {
                    console.error(res);
                    SuccFunc(res.data);
                })
                .catch(function (error) {
                    console.log(error);
                })
        },

        generatePassword: function (uid:string, SuccFunc) {
            ot_api
                .changePassword(uid)
                .then(res => {
                    console.error(res);
                    SuccFunc(res.data);
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        getKeysForObject: (uid:string, SuccFunc, ErrFunc) => {
            if(this.xhrGenerateModalContent.cancel) {
                this.xhrGenerateModalContent.cancel();
            }
            ot_api
                .getKeysValues(uid, this.xhrGenerateModalContent)
                .then(res => {
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        let text = [];
                        if(res?.data?.errors) {
                            let errors = res.data.errors;
                            for (let i = 0; i < errors.length; i++) {
                                text.push(errors[i].text);
                            }
                        }
                        ToastrStore.error(text.join("\n"), 'Error');
                        ErrFunc(res.data)
                    }
                })
                .catch( (error) =>{
                    if(axios.isCancel(error)) {
                        console.error("Отмена");
                        //ручная отмена
                    }
                    else {
                        console.log(error);
                        ToastrStore.error('Server Error', 'Error');
                        ErrFunc({result: 0});
                    }
                })
        },
        copyObjectAJAX: (data:{obj_parent:string, objs_copy: {obj:string, attrs:{attr:string,datatype:string,value:string}[]}[], copyChildren:number}, SuccFunc, ErrFunc) => {
            ot_api
                .copyObject(data)
                .then(res => {
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        let text = [];
                        if(res?.data?.errors) {
                            let errors = res.data.errors;
                            for (let i = 0; i < errors.length; i++) {
                                text.push(errors[i].text);
                            }
                        }
                        ToastrStore.error(text.join("\n"), 'Error');
                        ErrFunc(res.data)
                    }
                })
                .catch( (error) =>{
                    if(axios.isCancel(error)) {
                        console.error("Отмена");
                        //ручная отмена
                    }
                    else {
                        console.log(error);
                        ToastrStore.error('Server Error', 'Error');
                        ErrFunc({result: 0});
                    }
                })
        },



        build: {
            copyModal: {
                content: () => {
                  return <div>
                      <h5>Enter key parameters</h5>
                      <div style={{padding:"20px"}}>
                          <div className="form-check">
                              <input type="checkbox"
                                     className="form-check-input"
                                     id="exampleCheck1"
                                     onChange={(e) => {this.setState({isCopyChild: e.target.checked});}}
                                     checked={this.state.isCopyChild}
                              />
                              <label className="form-check-label" htmlFor="exampleCheck1">Copy children</label>
                          </div>
                      </div>
                      {this.core.build.copyModal.form()}
                  </div>;
                },
                form: () => {
                    let controls = [];
                    let typeModal = "edit";

                    this.state.attrData.forEach( (item, index) => {
                        const paramsControl:f_helper.IFormDatatypeClass = {
                            item: item,
                            key: index,
                            attrData: this.state.attrData,
                            attrDataCache: this.state.attrDataCache,
                            attrDataFileCache: this.state.attrDataFileCache,
                            type: typeModal as ("add" | "edit" | "read"),
                            ObjID: this.props.ObjID,
                            TypeID: this.props.TypeID,

                            ChangeStateFunc: () => { this.setState({});}, // this.setState();
                        };
                        let itemControl = new f_helper.FormDatatypeClass(paramsControl);
                        controls.push(itemControl.GetControl());
                    });


                    return <form className="zt_form">{controls}</form>;
                }
            },

            infModal: () => {
                let OwnerName = this.state.OwnerName ? this.state.OwnerName : "empty";
                let CreationDate = this.state.CreationDate ? this.state.CreationDate : "empty";

                let attr_table;

                if(Object.keys(this.state.attrDataInf).length > 0) {
                    let trs = [];

                    Object.keys(this.state.attrDataInf).forEach((item_key) => {
                        let item_attr = this.state.attrDataInf[item_key];

                        let LastUpdated = item_attr.attr.LastUpdated;

                        if(LastUpdated) {
                            let CreationDate2 = parse(
                                LastUpdated.split('.')[0],
                                'yyyy-MM-dd HH:mm:ss',
                                new Date()
                            );
                            LastUpdated = format(CreationDate2, 'dd-MM-yyyy HH:mm:ss')
                        }

                        trs.push(
                            <tr>
                                <td>{item_attr.attr.AttrName}</td>
                                <td>{LastUpdated}</td>
                                <td>{item_attr.attr.Login}</td>
                            </tr>
                        )
                    });

                    attr_table = (
                      <table className={"table"}>
                          <thead>
                          <tr>
                              <th>Name</th>
                              <th>Last updated</th>
                              <th>Author</th>
                          </tr>
                          </thead>
                          <tbody>
                          {trs}
                          </tbody>
                      </table>
                    );
                }

                return (
                    <div>
                        <h3>{"Owner name: " +OwnerName}</h3>
                        <br/>
                        <h3>{"Creation date: " + CreationDate}</h3>
                        <br/>
                        {attr_table}
                    </div>
                );
            }
        }
    };

    events = {
        copyModal: {
            handleCopy: () => {
                this.setState({blocking: true });

                let values = [];
                let Name_obj = "";
                let ValidErrorArr = [];

                this.state.attrData.forEach( (item, index) => {
                    let _item = {
                        value: item.value ? item.value.value : item.value,
                        attr: item.uid,
                        key: item.isKey ? 1 : 0,
                        datatype: item.datatype,
                    };
                    if(_item.datatype != "counter") {
                        if(this.state.attrDataCache[item.uid] != undefined) {
                            _item.value = this.state.attrDataCache[item.uid].value;
                            if(item.datatype == "object") {
                                _item.value = _item.value && Object.keys( _item.value).length > 0 ? _item.value : "";
                            }
                            values.push(_item);
                        }
                        else if(item.isKey) {
                            values.push(_item);
                        }

                        if(item.isKey) {
                            Name_obj += (_item.value === null ? '' : _item.value) + ' ';
                        }

                        if((item.require && !_item.value) || (item.isKey && !_item.value)) {
                            if(item.datatype == "file") {
                                if(this.state.attrDataFileCache[item.uid] == undefined || this.state.attrDataFileCache[item.uid].files.length == 0 ) {
                                    ValidErrorArr.push({name: item.name, text: "Enter value!"})
                                }
                            }
                            else {
                                ValidErrorArr.push({name: item.name, text: "Enter value!"})
                            }
                        }
                    }
                });

                if(ValidErrorArr.length > 0) {
                    ToastrStore.clear();
                    ValidErrorArr.forEach((item_err) => {
                        ToastrStore.error("parameter|" + item_err.name + "| " + item_err.text);
                    });
                }
                else {
                    if(Name_obj) {
                        Name_obj = Name_obj.slice(0, -1)
                    }
                }


                const data_param = {
                    copyChildren: Number(this.state.isCopyChild),
                    obj_parent: this.props.ToNode.ObjID,

                    objs_copy: [
                        {
                            obj: this.props.ObjID,
                            attrs: values
                        }
                    ]
                };

                this.core.copyObjectAJAX(data_param, (answer) => {
                    ToastrStore.success('Success!', 'Success!');
                    this.setState({blocking: false });
                    console.log(answer);
                    answer.CreatedObjs.forEach((item_a) => {
                        this.props.addedObject({
                            selectData: this.props.ToNode,
                            newNode:{ObjID: item_a.ID, Name: item_a.Name} });
                    });
                    this.props.CloseCallback();
                }, () => {
                    this.setState({blocking: false });
                });
            },
        },

    };


    handleCopySave = (e, ref_pass, isClose) => {
        var textAcode = ref_pass;
        textAcode.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copy email command was ' + msg);
            ToastrStore.success('Copy in buffer!', 'Success!');
        } catch (err) {
            console.log('Oops, unable to copy');
        }
        window.getSelection().removeAllRanges();
        if(isClose) {
            this.props.CloseCallback();
        }
    };
    handeleDelete = () => {
        this.setState({blocking: true})
        this.core.delObject(this.props.ObjID, (data) => {
            if(data.result === 1) 
            {
                ToastrStore.success('Success', 'Object deleted successfully!');
                this.props.onDeleteNode(this.props.ObjID);
                this.props.CloseCallback();
            }
            else 
            {
                for(let i=0;i<data.errors.length;i++)
                {
                    ToastrStore.error(data.errors[i].text);
                }
                
            }
            this.setState({blocking: false})
        });
    };

    handleClose = () => {
        this.props.CloseCallback();
    };

    onShow = () => {
        this.setState({blocking: true, attrDataCache: {}, OwnerName: null, CreationDate: null, attrDataInf: {} });

        if(this.props.TypeModal == "del") {
            this.setState({blocking: true});
            this.core.getDeleteCount(this.props.ObjID, (data) => {
                this.setState({blocking: false, CountDelObj: data.data});
            });
            //getDeleteCount
        }
        else if(this.props.TypeModal == "GenPassword") {
            this.core.generatePassword(this.props.ObjID, (data) => {
                if(data.result == 1) {
                    this.state.GenPassword.value = data.data;
                }
                else {
                    ToastrStore.error('Server error!', 'Error');
                }
                this.setState({blocking: false});
            })
        }
        else if(this.props.TypeModal == "copy") {
            this.core.getKeysForObject(this.props.ObjID, (answer) => {
                let values = answer.data[this.props.ObjID];

                let keys_param_values = [];

                values.forEach((item) => {
                   if(item.isKey) {
                       keys_param_values.push(item);
                   }
                });


                this.setState({
                    blocking: false,
                    attrData: keys_param_values
                });

                console.log(answer);
            }, (err) => {
                this.setState({blocking: false, attrData: []});
            })

        }
        else if(this.props.TypeModal == "inf") {
            this.core.getInfObject(this.props.ObjID, (answer) => {

                let CreationDate = answer.data?.CreationDate ? answer.data.CreationDate : "";
                let OwnerName = answer.data?.OwnerName ? answer.data.OwnerName : "";

                if(CreationDate) {
                    let CreationDate2 = parse(
                        CreationDate.split('.')[0],
                        'yyyy-MM-dd HH:mm:ss',
                        new Date()
                    );
                    CreationDate = format(CreationDate2, 'dd-MM-yyyy HH:mm:ss')
                }

                this.setState({
                    OwnerName: OwnerName,
                    CreationDate: CreationDate,
                    attrDataInf: answer.data.attrs,
                    blocking: false,
                });
            }, (err) => {
                this.setState({blocking: false, attrDataInf: {}});
            })

        }
    };


    render() {
        let Title = "";
        let Body = <div/>;
        let Footer = <div/>;

        if (this.props.TypeModal == "del") {
            Title = "Delete object \"" + this.props.Name + "\"";
            Body = <div>
                Are you sure you want to delete "{this.props.Name}" and {(this.state.CountDelObj-1)} nested objects?
            </div>;
            Footer = <div>
                <Button variant="danger" onClick={this.handeleDelete}>
                    Delete
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    Close
                </Button>
            </div>;
        }
        else if (this.props.TypeModal == "GenPassword") {
            Title = "New Password for " + this.props.Name;
            let ref_pass;
            Body = <input className="form-control" value={this.state.GenPassword.value} onChange={()=>{}} onClick={
                (e)=> {this.handleCopySave(e, ref_pass, false)}
            } ref={(ref)=>(ref_pass=ref)}/>;
            Footer = <div>
                <Button variant="success" onClick={(e)=>{this.handleCopySave(e, ref_pass, true)}} >
                    Copy & Close
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    Close
                </Button>
            </div>;
        }
        else if(this.props.TypeModal == "copy") {
            Title = "Copy " + this.props.Name;
            Body = this.core.build.copyModal.content();
            Footer = <div>
                <Button variant="success" onClick={(e)=>{this.events.copyModal.handleCopy()}} >
                    Copy
                </Button>
                <Button variant="secondary" onClick={this.handleClose}>
                    Close
                </Button>
            </div>;
        }
        else if(this.props.TypeModal == "inf") {
            Title = "Information about " + this.props.Name;
            Body = this.core.build.infModal();
            Footer = <div>
                <Button variant="secondary" onClick={this.handleClose}>
                    Close
                </Button>
            </div>;
        }

        return (
            <Modal onShow={this.onShow}
                   animation={true} show={this.props.show}
                   onHide={this.handleClose} size="lg"
                   container={document.getElementById('root_theme')}>
                <BlockUi className={"Obj_HelperModal"} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{Title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}
