//Экземпляр дерева,
//При этом этот экземпляр будет генерировать совершенно разные деревья в зависимости от того места, от которого нужно строить
//Это может быть как основное дерево, так и форма 3-го уровня, тогда дерево начинает строится с определённого типа, или объекта
//При этом все деревья являются независимыми
import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";

import { HashRouter as Router, Route, Link, Switch, Redirect, RouteProps, RouteComponentProps} from 'react-router-dom'

import Loader from "react-loaders";
import BlockUi from "react-block-ui";

import CoolTree from '../../components/CoolTree'
import * as ot_api from '../../api/ObjTree-api'
import * as ct_api from '../../api/ClassTree-api'

import HelperModal from './HelperModal'
import ImportModal from './ImportModal'
import INodeTemplate from './Interfaces/INodeTemplate'

import { useRouteMatch, useParams } from 'react-router-dom'
import IModalConfig from "../../components/CoolModalForm/IModalConfig";
import ToastrStore from "../../stores/ToastrStore";
import PermissionWindowCloth from "./PermissionWindowCloth";
import RootStore from "../../stores/RootStore";


export interface IModalConfigState extends IModalConfig {
    Tree?: object[], //Ветка дерева для открытого модального окна
    SelectNode?: object,
    expandedKeys?: string[],
    selectedKey?: string,
    url_modal?: string,
    RootNode: any,
    type: "permis" | "object"
}

export interface IClassesTreeInstanceProps {
    match?: any,
    history?: any,
    location?: any,
    ToastrStore?: any,
    ObjectsStore?: {
        JSXTree: JSX.Element,
        isLoadTree: boolean,

        treeData: any,
        UpdTreeDataAction: any,
        SelectNode: any,
        AddChildrenInNode: any,
        CutNodeAction: any,
        PageContent: any,
        PageTrig: boolean,
        UpdNameNode: any,
    },
    RootStore?: any,
}
interface IClassesTreeInstanceState {
}

@inject("ToastrStore")
@inject("ObjectsStore")
@inject("RootStore")
@observer
export default class ObjectsTreeInstance extends React.Component<IClassesTreeInstanceProps, IClassesTreeInstanceState> {
    static defaultProps = {
        isOpen: null,
        isFullLoad: true, //
        treeData:[],
    };
    state: Readonly<IClassesTreeInstanceState> = {
    };

    constructor(props: IClassesTreeInstanceProps) {
        super(props);
    };
    componentDidMount(): void {

    };
    componentDidUpdate(prevProps: Readonly<IClassesTreeInstanceProps>, prevState: Readonly<IClassesTreeInstanceState>, snapshot?: any): void {

    };

    componentWillUnmount(): void {
    };

    core = {

    };

    render() {
        let isBlocking =  false;
        if(this.props.ObjectsStore.isLoadTree) {
            isBlocking = true;
        }

        return (
            <BlockUi id={"zt_FilterSidebar_body_co"} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                {this.props.ObjectsStore.JSXTree}
            </BlockUi>
        );
    }
}