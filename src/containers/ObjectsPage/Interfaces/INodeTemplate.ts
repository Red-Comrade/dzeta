export default interface INodeTemplate {
    isRoot?: boolean;
    loaded?: boolean,
    isLoaded?: boolean;


    ParentIDTree: string; //
    id: string; //Асболютно уникальный ID
    key?: string,

    ParentID: string;
    iddb: string; //спец ID, если тип, то это TypeID, папка - FolderID, объект - ObjID

    Name: string;
    icon?: any;

    children?: INodeTemplate[];

    isObject?: boolean;
    isClass?: boolean;
    IsFolder?: boolean;

    TypeID: string;
    ObjID: string;
    FolderID: string;

    //Дополнительные
    ParrentTypeID?: string;
    ParrentFolderID?: string;
    ParrentObjID?: string;
}