export default interface ItemAttr {
    datatype: string;
    isKey: boolean;
    isArray: boolean;
    isName: string;
    index: number;
    unitGroup: any;
    unit: any;
    parent?: any;
    group?:any;
    value?: {
        convertedValue?: number,
        value?: any,
        name?: string,
        preview?: string,
        size?: number,
        uid?: string,
    };
    convertedValue?: string;
    uid: string;
    name: string;
    require: boolean,

    calc_triggers: {
        manual: any,
    },

    LinkType?: object;

    SelectData?: any,
}