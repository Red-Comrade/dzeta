import * as React from "react";
import {HashRouter as Router, Route, Link, Switch as SwitchRouter, Redirect, useParams, useRouteMatch} from 'react-router-dom'
import {inject, observer} from "mobx-react";
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import CoolTable from '../../components/CoolTable'
import HZTabs from "../../components/HZTabs";
import HZContextMenu from "../../components/HZContextMenu";
import * as ct_api from '../../api/ClassTree-api'
import ObjectTreeClass, {IModalConfigState, IStateObjectTree} from "./ObjectTreeClass";
import CoolTree from "../../components/CoolTree";
import IModalConfig from "../../components/CoolModalForm/IModalConfig";
import PermissionWindowCloth from "./PermissionWindowCloth";
import {IViewWindowFormViewProps, ViewWindowFormView} from "../ViewPage/ViewWindowFormView";
import ObjectModalCloth, {IObjectModalCloth} from "./ObjectModalCloth";
import INodeTemplate from "./Interfaces/INodeTemplate";
import RootStore from "../../stores/RootStore";
import ObjectsStore from "../../stores/ObjectsStore";
import HelperModal from "./HelperModal";
import ImportModal from "./ImportModal";
import ToastrStore from "../../stores/ToastrStore";



export interface IObjectsPageProps {
    match?: any,
    history?: any,
    location?: any,
    ToastrStore?: any,
    RootStore?: any,
    ObjectsStore?: {
        JSXTree: JSX.Element,
        isLoadTree: boolean,
        PageContent: any,
        PageTrig: boolean,
    }
}

export interface IObjectsPageState extends IStateObjectTree {

}

//React.Component<IObjectsPageProps, IObjectsPageState>
@inject("ToastrStore")
@inject("ObjectsStore")
@inject("RootStore")
@observer
export class ObjectsPage extends ObjectTreeClass<IObjectsPageProps, IObjectsPageState> {
    NameModule: "users" | "objects" = "objects";
    getInitialState() {
        let initialStates = super.getInitialState();
        return initialStates;
    }

    constructor(props: IObjectsPageProps) {
        super(props);
        this.state = this.getInitialState();
    };


    componentDidMount(): void {
        this.NameModule = this.props.match.path.indexOf('/users') !== -1 ? "users" : "objects";
        document.title = this.NameModule[0].toUpperCase() + this.NameModule.slice(1) + ' | ' + RootStore.config.nameSystem;

        let PathNodeID = this.props.match.params.NodeID;
        this.core.GenerateTree(PathNodeID);
    }
    componentDidUpdate(prevProps: Readonly<IObjectsPageProps>, prevState: Readonly<IObjectsPageState>, snapshot?: any): void {
        let PrevPathNode = prevProps.match.params.NodeID;
        let PathNodeID = this.props.match.params.NodeID;
        PathNodeID = PathNodeID ? PathNodeID : "";
        PrevPathNode = PrevPathNode ? PrevPathNode : "";
        let hasA = PathNodeID.indexOf('@') !== -1;

        if (this.state.buildTree && !this.state.isLoadTree) {
            this.setState({buildTree: false, buildCleanTree: false});
            //дерево будет построено только в том случае, если по УРЛ не нужно строить модальную форму т.е. Либо пустой урл, либо присутствует вначале @
            if(hasA || !PathNodeID) {
                this.props.ObjectsStore.isLoadTree = false;
                this.props.ObjectsStore.JSXTree = this.core.Tree.build();
            }
        }
        else if(this.state.buildCleanTree) {
            this.setState({buildCleanTree: false});
            if(hasA || !PathNodeID) {
                this.props.ObjectsStore.isLoadTree = false;
                this.props.ObjectsStore.JSXTree = this.core.Tree.build();
            }
        }

        let nextNameModule: "users" | "objects" = this.props.match.path.indexOf('/users') !== -1 ? "users" : "objects";

        if (this.NameModule != nextNameModule) {
            this.NameModule = nextNameModule;
            if(!PathNodeID || hasA) {
                this.setState({buildCleanTree: true, treeData: []});
            }

            this.setState({isLoadTree: true});
            this.core.GenerateTree(PathNodeID);
        }
        else if (!this.state.isLoadTree) {
            //Делаем проверку, если изменился узел в дереве по урл
            if (PathNodeID != PrevPathNode) {
                if(hasA) {
                    PathNodeID = PathNodeID.slice(1);
                }
                let selectRoot = this.state.treeData[0];

                if(!PathNodeID || selectRoot.id == PathNodeID) {
                    let AddBtnDisabled = true;
                    if(selectRoot.iddb == "00000000-0000-0000-0000-000000000002") {
                        AddBtnDisabled = false;
                    }
                    //root
                    this.setState({
                        SelectNode: selectRoot,
                        buildTree: true,

                        AddBtnDisabled: AddBtnDisabled,
                        EditBtnDisabled: true,
                        DelBtnDisabled: true,

                        selectNodeFunc: () => {
                            this.setState({selectNodeFunc: null, buildTree: true });
                            return selectRoot.id;
                        }
                    });
                    this.core.GenerateMainContent(selectRoot);
                }
                else {
                    this.setState({isLoadTree: true});
                    this.TreeCore.OpenTreeNode({Node: this.state.treeData[0], PathNodeID: PathNodeID, isSelect: true},
                        (config: { expandedKeys: string[], SelectNode? }) => {

                            let expandedKeys = this.state.expandedKeys;
                            if(this.state.expandedKeys) {
                                for(let i = 0; i < config.expandedKeys.length; i++) {
                                    if( expandedKeys.indexOf(config.expandedKeys[i]) === -1 ) {
                                        expandedKeys.push(config.expandedKeys[i]);
                                    }
                                }
                            }

                            let
                                AddBtnDisabled = true,
                                EditBtnDisabled = true,
                                DelBtnDisabled= true;

                            if(config.SelectNode.isObject) {
                                EditBtnDisabled = false;
                                DelBtnDisabled =  false
                            }
                            else if(config.SelectNode.isClass) {
                                AddBtnDisabled = false;
                            }


                            this.setState( {
                                isLoadTree: false,
                                buildTree: true,
                                expandedKeys: expandedKeys,
                                SelectNode: config.SelectNode,
                                expandNodeFunc: () => {
                                    this.setState({expandNodeFunc: null, buildTree: true});
                                    return expandedKeys;
                                },
                                selectNodeFunc: () => {
                                    this.setState({selectNodeFunc: null, buildTree: true });
                                    return PathNodeID;
                                },
                                AddBtnDisabled: AddBtnDisabled,
                                EditBtnDisabled: EditBtnDisabled,
                                DelBtnDisabled: DelBtnDisabled,
                            });

                            this.core.GenerateMainContent(config.SelectNode);


                        }, () => {
                            this.setState({isLoadTree: false});
                            location.hash = `/${this.NameModule}/`;
                        });
                }
            }
        }
    }
    componentWillUnmount(): void {
        Object.keys(this.state.Forms).forEach( (item_k, key_k) => {
            if(this.props.RootStore.Forms[item_k] != undefined) {
                delete this.props.RootStore.Forms[item_k];
            }
        });
        this.props.RootStore.UpdateForms();
    }

    events: {};

    //Не получилось наследовать
    protected FormCore = {
        GenerateObjectModal: (params: {
            url_modal?: string,
            url_modal_to?: string,
            url_modal_prev?: string,
            isMain?: boolean,
            typeModal: "types" | "edit" | "read" | "add",
            selectData: INodeTemplate,
        }) => {
            params.selectData = JSON.parse(JSON.stringify(params.selectData));
            params.selectData.children = [];

            let uid_form = "tmp";
            if (params.typeModal == "edit" || params.typeModal == "read") {
                uid_form = '*' + params.selectData.ObjID;
            }
            else if(params.typeModal == "add") {
                uid_form = 'add';
            }

            //TODO::ZAGL PERMISSION
            let isAllowEdit = true;
            if (document.location.host == "arctic.ozoneprogram.ru" && RootStore.MSettings.UserName != "Administrator") {
                isAllowEdit = false;
            }

            //Если нет прав то принудительно режим редактирования
            if (params.typeModal != "read" && !isAllowEdit || this.isReadMode) {
                params.typeModal = "read";
            }

            let DataCloth: IObjectModalCloth = {
                url_modal: params.url_modal,
                url_modal_to: params.url_modal_to,
                url_modal_prev: params.url_modal_prev,

                isMain: params.isMain,

                onClose: () => {
                    delete this.state.Forms[uid_form];
                    RootStore.CloseForm(uid_form);
                },
                savedObject: () => {
                    //this.core.reload();
                },
                addedObject: (config:{newNode:{ObjID: string, Name:string}, selectData}) => {
                    let ChildNode:INodeTemplate = {
                        ParentIDTree: config.selectData.id,
                        id: config.selectData.id + '*' + config.newNode.ObjID,
                        ParentID: config.selectData.iddb,
                        iddb: config.newNode.ObjID,
                        Name: config.newNode.Name,
                        icon: 'fas fa-cube',
                        children: [],
                        isObject: true,
                        isClass: false,
                        IsFolder: false,
                        TypeID: config.selectData.TypeID,
                        ObjID: config.newNode.ObjID,
                        FolderID: config.selectData.FolderID,
                        ParrentTypeID: config.selectData.ParrentTypeID,
                        ParrentObjID: config.selectData.ObjID,
                        isLoaded: false
                    };
                    this.TreeCore.AddNode(config.selectData, ChildNode)
                },
                onChangeModalName: (config:{Name: string, selectData: any}) => {
                    this.TreeCore.UpdNameNode(config.selectData.id, config.Name);
                },

                onChangeTypeModal: (newType: "add" | "edit" | "read") => {
                    this.isReadMode = newType == "read";
                    params.typeModal = newType;
                    this.FormCore.GenerateObjectModal(params);
                },

                typeModal: params.typeModal,
                isAllowEdit: isAllowEdit,
                selectData: params.selectData,
            };


            if(params.isMain) {
                ObjectsStore.PageContent.contentJSX = <ObjectModalCloth {...DataCloth} />;
            }
            else {
                let ModalConf: IModalConfig = {
                    id: uid_form,
                    onClickCloth: () => {
                    },
                    content: {
                        bodyJSX: <ObjectModalCloth {...DataCloth} />,
                    },
                };

                if(params.typeModal != "add") {
                    ObjectsStore.isLoadTree = true;
                }

                const Forms = this.state.Forms as any;

                if (Forms[uid_form] == undefined) {
                    Forms[uid_form] = {
                        ...ModalConf,
                        ...{
                            url_modal_to: params.url_modal_to,
                            url_modal: uid_form,
                            type: "object"},
                    } as any;
                    RootStore.OpenForm(uid_form, ModalConf);
                } else {
                    Forms[uid_form].url_modal_to = params.url_modal_to;
                    Forms[uid_form].url_modal = uid_form;
                    RootStore.OpenForm(uid_form, ModalConf);
                }
            }
        },

        GeneratePermissionModal: (params: {
            ObjID: string,
            Name: string,
        }) => {
            let uid_form = 'permis-' + params.ObjID;
            if (this.state.Forms[uid_form] == undefined) {
                let PermData = {
                    url_modal: uid_form,
                    ObjID: params.ObjID,
                    onClose: () => {
                        delete this.state.Forms[uid_form];
                        RootStore.CloseForm(uid_form);
                    },
                    UserName: params.Name,
                };

                let ModalConf: IModalConfig = {
                    id: uid_form,
                    onClickCloth: () => {
                    },
                    content: {
                        bodyJSX: <PermissionWindowCloth {...PermData} />,
                    }
                };

                const Forms = this.state.Forms as any;

                Forms[uid_form] = {
                    ...ModalConf,
                    ...{RootNode: null, url_modal: uid_form, type: "permis"},
                };
                RootStore.OpenForm(uid_form, ModalConf);
            }
        },
    };

    core = {
        AJAX: {},
        //Прогрузка данных для основного дерева
        GenerateTree: (PathNodeID: string) => {
            PathNodeID = PathNodeID ? PathNodeID : "";
            let hasA = PathNodeID.indexOf('@') !== -1;
            if(hasA) {
                PathNodeID = PathNodeID.slice(1);
            }
            //Перед вызовом этого метода вызываем isLoadTree=true
            this.TreeCore.GenerateTreeData({
                PathNodeID: PathNodeID,
                module: this.NameModule
            }, (config: { expandedKeys: string[], treeData: any, SelectNode? }) => {
                let treeData:INodeTemplate[] = [config.treeData];

                let StateCh = {
                    treeData: treeData,
                    isLoadTree: false,
                    buildTree: true,
                    expandedKeys: config.expandedKeys,
                    RedrafFunc: () => {
                        this.setState({RedrafFunc: null, buildTree: true})
                    },
                    expandNodeFunc: () => {
                        this.setState({expandNodeFunc: null, buildTree: true });
                        return config.expandedKeys;
                    },
                    AddBtnDisabled: true,
                    EditBtnDisabled: true,
                    DelBtnDisabled: true,
                };

                if (config.SelectNode) {
                    if(config.SelectNode.isObject) {
                        StateCh.EditBtnDisabled = false;
                        StateCh.DelBtnDisabled =  false
                    }
                    else if(config.SelectNode.isClass) {
                        StateCh.AddBtnDisabled = false;
                    }

                    Object.assign(StateCh, {
                        SelectNode: config.SelectNode,
                        selectNodeFunc: () => {
                            this.setState({selectNodeFunc: null, buildTree: true });
                            return PathNodeID;
                        }
                    });
                    this.setState(StateCh);

                    this.core.GenerateMainContent(config.SelectNode);
                }
                else {
                    this.setState(StateCh);
                    //Если нет такого узла в дереве, то мы возвращаем наше дерево в базовое состояние

                    if(!PathNodeID) {
                        let selectRoot = this.state.treeData[0];
                        this.core.GenerateMainContent(selectRoot);
                    }
                    else {
                        location.hash = `/${this.NameModule}/`;
                    }
                }
            });
        },
        GenerateMainContent: (selectData) => {
            let typeModal: "types" | "edit" | "read" | "add" = "edit";
            if(selectData.isClass || selectData.isRoot) {
                typeModal = "types";
            }

            let url_modal = '/' + this.NameModule + '/@' + selectData.id;
            this.FormCore.GenerateObjectModal({
                url_modal: url_modal,
                typeModal: typeModal,
                isMain: true,
                selectData: selectData,
            })
        },

        Tree: {
            build: () => {
                let IDTab = '*0';
                let NameTab = "Objects";

                if (this.NameModule == "users") {
                    IDTab = '|0';
                    NameTab = "Groups";
                }

                const DataCoolTree = {
                    RootTab: {
                        ID: IDTab,
                        Name: NameTab,
                    },

                    search_function: (node, SearchString) => {
                        let is_founded = node.Name.toLocaleLowerCase().indexOf(SearchString) !== -1;
                        if(is_founded ) {
                            return true;
                        }
                        if(node.isClass && node.children) {
                            return true;
                        }
                        return false;
                    },
                    treeData: this.state.treeData,
                    isFullLoad: true,
                    selectNodeFunc: this.state.selectNodeFunc,
                    expandNodeFunc: this.state.expandNodeFunc,
                    expandNodeTabFunc: this.state.expandNodeTabFunc,
                    ShowFromLevelFunc: this.state.ShowFromLevelFunc,
                    RedrafFunc: this.state.RedrafFunc,
                    UpdateGlobalTree: this.state.UpdateGlobalTree,

                    //Кнопки
                    AddBtnDisabled: this.state.AddBtnDisabled,
                    EditBtnDisabled: this.state.EditBtnDisabled,
                    DelBtnDisabled: this.state.DelBtnDisabled,

                    OnAddBtnFunc: (selectData) => {
                        if(selectData) {
                            if(selectData.isClass) {
                                this.FormCore.GenerateObjectModal({
                                    url_modal: '/' + this.NameModule + '/@' + selectData.id,
                                    typeModal: "add",
                                    selectData: this.state.SelectNode,
                                });
                            }
                        }
                    },
                    OnEditBtnFunc: (selectData) => {
                        location.hash = '/' + this.NameModule + '/' + selectData.id;
                    },
                    OnDelBtnFunc: (selectData) => {
                        this.setState({
                            HelperModalData: {
                                show: true,
                                TypeModal: "del",
                                ObjID: selectData.ObjID,
                                Name: selectData.Name,
                            }
                        });
                    },

                    onSelect: (selectData) => {
                        if(selectData) {
                            let url_modal = "";

                            this.setState({SelectNode: selectData});

                            if(this.NameModule == "users") {
                                if(selectData.id == "*0") {
                                    location.hash = `/users/*0`;
                                }
                                else {
                                    location.hash = `/users/@${selectData.id}`;
                                }
                            }
                            else {
                                if(selectData.id == "|00000000-0000-0000-0000-000000000000") {
                                    //Проверка на корень
                                    location.hash = `/objects/*0`;
                                }
                                else {
                                    location.hash = `/objects/@${selectData.id}`;
                                }
                            }

                        }
                    },
                    onSelectR: (selectData) => {
                    },
                    onOpenCM: (params: {SelectData:any, callbackCloseCM:()=>void, IsBtnCM?:boolean}, callback) => {
                        let contentCM = [];

                        if(!params.SelectData.isRoot) {
                            let contentCMothers = [];
                            if(params.IsBtnCM) {
                                //Контекстное меню от кнопки
                            }
                            else {
                                if(params.SelectData.isClass) {
                                    contentCM.push({
                                        "label": "Create",
                                        "action": ()=> {
                                            this.FormCore.GenerateObjectModal({
                                                url_modal: '/' + this.NameModule + '/@' + params.SelectData.id,
                                                typeModal: "add",
                                                selectData: params.SelectData,
                                            });
                                            params.callbackCloseCM();
                                        },
                                        "icon_className": 'fas fa-cube',
                                        "children": false
                                    });
                                }
                                if(params.SelectData.isObject) {
                                    contentCM.push({
                                        "label": "Edit",
                                        "action": ()=> {
                                            location.hash = '/' + this.NameModule + '/' + params.SelectData.id;
                                            params.callbackCloseCM();
                                        },
                                        "icon_className": 'fas fa-edit',
                                        "children": false
                                    });

                                    if(params.SelectData.TypeID == "00000000-0000-0000-0000-000000000002") {
                                        contentCM.push({
                                            "label": "Permission",
                                            "action": ()=> {
                                                this.FormCore.GeneratePermissionModal({
                                                    ObjID: params.SelectData.ObjID,
                                                    Name: params.SelectData.Name,
                                                });
                                                params.callbackCloseCM();
                                            },
                                            "icon_className": 'fas fa-user-shield',
                                            "children": false
                                        });

                                        contentCM.push({
                                            "label": "Application permission",
                                            "action": ()=> {
                                                window.open('#/papplication/'+params.SelectData.ObjID, '_blank');
                                                params.callbackCloseCM();
                                            },
                                            "icon_className": 'fas fa-user-shield',
                                            "children": false
                                        });
                                    }
                                    if(params.SelectData.TypeID == "00000000-0000-0000-0000-000000000001") {
                                        contentCM.push({
                                            "label": "Generate Password",
                                            "action": ()=> {
                                                this.setState({
                                                    HelperModalData: {
                                                        show: true,
                                                        TypeModal: "GenPassword",
                                                        ObjID: params.SelectData.ObjID,
                                                        Name: params.SelectData.Name,
                                                    }
                                                });
                                                params.callbackCloseCM();
                                            },
                                            "icon_className": 'fas fa-edit',
                                            "children": false
                                        });
                                    }

                                    contentCM.push({
                                        "label": "Delete",
                                        "action": ()=> {
                                            this.setState({
                                                HelperModalData: {
                                                    show: true,
                                                    TypeModal: "del",
                                                    ObjID: params.SelectData.ObjID,
                                                    Name: params.SelectData.Name,
                                                }
                                            });
                                            params.callbackCloseCM();
                                        },
                                        "icon_className": 'fas fa-times',
                                        "children": false
                                    });

                                }
                            }
                            contentCM.push({
                                "label": "Display from current level",
                                "action": ()=> {
                                    this.setState({
                                        buildTree: true,
                                        ShowFromLevelFunc: (
                                            (callback) => {
                                                this.setState({
                                                    ShowFromLevelFunc: null,
                                                    buildTree: true,
                                                });
                                                callback(params.SelectData.id);
                                            })
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'far fa-eye',
                                "children": false
                            });

                            if(params.SelectData.isClass) {
                                let isDisabledPaste = true;
                                let NamePaste = "Paste";
                                if(this.state.CutNode || this.state.CopyNode) {
                                    if(this.state.CutNode) {
                                        NamePaste = "Paste(cut) (\'" + this.state.CutNode.Name + "\')";
                                        if( params.SelectData.TypeID == this.state.CutNode.TypeID) {
                                            isDisabledPaste = false;
                                        }
                                    }
                                    if(this.state.CopyNode) {
                                        NamePaste = "Paste(copy) (\'" + this.state.CopyNode.Name + "\')";
                                        if( params.SelectData.TypeID == this.state.CopyNode.TypeID) {
                                            isDisabledPaste = false;
                                        }
                                    }
                                }
                                contentCMothers.push({
                                    "label": NamePaste,
                                    "disabled" : isDisabledPaste,
                                    "action": ()=> {
                                        if(!isDisabledPaste) {
                                            if(this.state.CopyNode) {
                                                this.setState({
                                                    HelperModalData: {
                                                        show: true,
                                                        TypeModal: "copy",
                                                        ObjID: this.state.CopyNode.ObjID,
                                                        TypeID: this.state.CopyNode.TypeID,
                                                        Name: this.state.CopyNode.Name,
                                                        ToNode: params.SelectData,
                                                    }
                                                });

                                                params.callbackCloseCM();
                                            }
                                            else if(this.state.CutNode) {
                                                this.TreeCore.AJAX.changeLinkObj(this.state.CutNode.ObjID, this.state.CutNode.ParrentObjID, params.SelectData.ObjID, () => {
                                                    this.TreeCore.CutNodeAction({
                                                        Node: this.state.CutNode,
                                                        ToNode: params.SelectData,
                                                    });
                                                    params.callbackCloseCM();
                                                }, (answer) =>
                                                { 
                                                    for(let i=0;i<answer.errors.length;i++)
                                                    {
                                                        ToastrStore.error(answer.errors[i].text);
                                                    } 
                                                });
                                            }
                                        }
                                    },
                                    "icon_className": 'fas fa-paste',
                                    "children": false
                                });

                                let NameImport = "Import";
                                contentCMothers.push({
                                    "label": NameImport,
                                    "action": (a, b, c, d) => {
                                        console.log(params);
                                        this.setState({
                                            ImportModalConfig: {
                                                show: true,
                                                class: params.SelectData.TypeID,
                                                parent: params.SelectData.ParentID,
                                            }
                                        });
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-upload',
                                    "children": false
                                });
                            }

                            if(params.SelectData.isObject) {
                                contentCMothers.push({
                                    "label": "Copy",
                                    "action": ()=> {
                                        this.setState({
                                            CopyNode: params.SelectData,
                                            CutNode: null,
                                        });
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-copy',
                                    "children": false
                                });
                                contentCMothers.push({
                                    "label": "Cut",
                                    "action": ()=> {
                                        this.setState({
                                            CutNode: params.SelectData,
                                            CopyNode: null,
                                        });
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-cut',
                                    "children": false
                                });

                                contentCMothers.push({
                                    "label": "Information",
                                    "action": ()=> {
                                        this.setState({
                                            HelperModalData: {
                                                show: true,
                                                TypeModal: "inf",
                                                ObjID: params.SelectData.ObjID,
                                                Name: params.SelectData.Name,
                                            }
                                        });
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-info-circle',
                                    "children": false
                                });
                            }

                            if(contentCMothers.length > 0) {
                                contentCM.push({
                                    "label": "Others",
                                    "action": ()=> {},
                                    "icon_className": 'fa fa-ellipsis-h',
                                    "children": contentCMothers
                                });
                            }
                        }
                        else if (params.SelectData.iddb == "00000000-0000-0000-0000-000000000002") {
                            //Добавление группы
                            contentCM.push({
                                "label": "Create",
                                "action": ()=> {
                                    this.FormCore.GenerateObjectModal({
                                        url_modal: '/' + this.NameModule + '/@' + params.SelectData.id,
                                        typeModal: "add",
                                        selectData: params.SelectData,
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-cube',
                                "children": false
                            });
                        }

                        return contentCM;
                    },
                    onExpandNode: this.TreeCore.onExpandNode,
                    onLoadData: this.TreeCore.onLoadData,
                };

                return <CoolTree {...DataCoolTree} />;
            }
        },
    };


    render() {
        let LoaderBlockUI = <Loader active type="ball-pulse"/>;
      //  LoaderBlockUI = <h4 className="loader-text">Select a node in the tree...</h4>;

        let Trig = this.props.ObjectsStore.PageTrig;
        let contentJSX = this.props.ObjectsStore.PageContent.contentJSX;


        let isBlocking = false;
        if (this.state.isLoadTree || !this.props.ObjectsStore.PageContent.contentJSX) {
            isBlocking = true;
        }

        const ImportModalData = {
            show: this.state.ImportModalConfig.show,
            class: this.state.ImportModalConfig.class,
            parent: this.state.ImportModalConfig.parent,
            onClose: () => { this.state.ImportModalConfig.show = false; this.setState({}) }
        };
        const HelperModalData = {
            ...{
                onDeleteNode: (ObjID:string) => { this.TreeCore.DeleteNode({iddb: ObjID})},
                CloseCallback: () => { this.state.HelperModalData.show = false; this.setState({})},
                addedObject: (config:{newNode:{ObjID: string, Name:string}, selectData}) => {

                    const selectData = this.state.HelperModalData.ToNode;

                    let ChildNode:INodeTemplate = {
                        ParentIDTree: config.selectData.id,
                        id: config.selectData.id + '*' + config.newNode.ObjID,
                        ParentID: config.selectData.iddb,
                        iddb: config.newNode.ObjID,
                        Name: config.newNode.Name,
                        icon: 'fas fa-cube',
                        children: [],
                        isObject: true,
                        isClass: false,
                        IsFolder: false,
                        TypeID: config.selectData.TypeID,
                        ObjID: config.newNode.ObjID,
                        FolderID: config.selectData.FolderID,
                        ParrentTypeID: config.selectData.ParrentTypeID,
                        ParrentObjID: config.selectData.ObjID,
                        isLoaded: false
                    };
                    this.TreeCore.AddNode(config.selectData, ChildNode);
                },
            }, ...this.state.HelperModalData
        };


        let url_modal = this.props.match.url + '/';

        const CheckModalUrlOpen = (params) => {
            //делаем проход по дереву на наличие такого узла, чтобы открыть
            //Если мы находим такой узел, то открываем модальную форму
            if(!this.state.isLoadTree)  {
                let PathNodeID = this.props.match.params.NodeID;
                PathNodeID = PathNodeID ? PathNodeID : "";

                if(PathNodeID && PathNodeID.indexOf('@') == -1 ) {
                    let ObjID_A = PathNodeID.split('*');

                    //Проверка, чтобы не был открыт тип, так как типы мы не открываем
                    if(!ObjID_A[ObjID_A.length - 1].match(/\|/)) {
                        let _ObjID = ObjID_A[ObjID_A.length - 1];
                        let uid_form = '*' + _ObjID;

                        let url_modal_to = params.match.params && params.match.params.NodeID != undefined ? params.match.params.NodeID : "";
                        //Проверка, чтобы форму уже не была открыта
                        if(this.state.Forms[uid_form] == undefined || (this.state.Forms[uid_form].url_modal_to != url_modal_to)) {
                            //let Node = this.state.treeData[0];
                            //Также, чтобы открыть данную форму нужно выбрать в дереве узел, чтобы из него получить данные
                            //В будущем, как появятся таблицы будет еще SelectNodeTable - активная таблица,
                            //Из которой будем брать данные для построения формы, но это будет в другом условии
                            if(this.state.SelectNode.ObjID == _ObjID) {
                                let url_modal = '/' + this.NameModule + '/' + this.state.SelectNode.id;
                                let url_modal_prev = '/' + this.NameModule + '/@' + this.state.SelectNode.id;
                                this.FormCore.GenerateObjectModal({
                                    url_modal_to: url_modal_to,
                                    url_modal: url_modal,
                                    url_modal_prev: url_modal_prev,
                                    typeModal: "edit",
                                    selectData: this.state.SelectNode,
                                });
                            }
                        }
                    }
                }
            }

            return null;
        };

        return (
            <BlockUi id="zt_ClassPage" tag="div" loader={LoaderBlockUI} blocking={isBlocking}>
                {contentJSX}
                <Router>
                    <SwitchRouter>
                        <Route path={[`${url_modal}:NodeID`, `${url_modal}`]} component={CheckModalUrlOpen}/>
                    </SwitchRouter>
                </Router>
                <HelperModal {...HelperModalData}  />
                <ImportModal {...ImportModalData} />
            </BlockUi>
        )
    }
}