import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import { HashRouter as Router, Route, Link, Switch, Redirect, RouteProps, RouteComponentProps} from 'react-router-dom'
import axios from 'axios';

import HZTabs from "../../components/HZTabs";
import * as ot_api from '../../api/ObjTree-api'
import * as ct_api from '../../api/ClassTree-api'

import INodeTemplate from './Interfaces/INodeTemplate'

import { useRouteMatch, useParams } from 'react-router-dom'
import Loader from "react-loaders";
import BlockUi from "react-block-ui";
import * as f_helper from "../../helpers/form_datatype_helper";

import RootStore from '../../stores/RootStore'
import {IValueFile} from "../../api/ObjTree-api";


import { makeStyles, Theme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';



import ToastrStore from '../../stores/ToastrStore'

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}


interface IPermisProps {
    url_modal: string,
    ObjID: string,
    UserName: string,
    onClose: () => void,
}
interface IPermisState {
    isLoadModalContent: boolean,
    TabsSett: number,

    modules: { allow: 0 | 1, module: string }[],

    attrDataCache: {[id:string] : {value: any}},
}

export default class PermissionWindowCloth extends React.Component<IPermisProps, IPermisState> {
    static defaultProps = {
    };
    state: Readonly<IPermisState> = {
        isLoadModalContent: true,
        TabsSett: 0,
        modules: [],
        attrDataCache: {}
    };

    xhrGenerateModalContent = {cancel: null};

    constructor(props: IPermisProps) {
        super(props);
    };
    componentDidMount(): void {
        this.core.GenerateModalContent();
    };


    events = {
        onChangeTab: (event: React.ChangeEvent<{}>, newValue: number) => {
            this.setState({TabsSett: newValue });
        },
        onSaveGroups: (event: React.ChangeEvent<{}>, isCloseForm: boolean) => {
            this.core.SavePermissionGroup(isCloseForm);
        },
        onClose: () => {
            if(this.props.onClose) {
                this.props.onClose();
            }
        }
    };

    core = {
        AJAX: {
            getModulesSettGroup: (group:string, SuccFunc:(answer) => void, ErrFunc:(answer) => void) => {
                ot_api
                    .getModulesSettGroup({group: group})
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch( (error) => {
                        ErrFunc({result: 0, errors: [{text:"Server error"}]});
                        //
                    })

            },
            addPermissionGroup: (param:{group:string, modules: {module: string, allow: 0 | 1 }[] }, SuccFunc:(answer) => void, ErrFunc:(answer) => void) => {
                ot_api
                    .addPermissionGroup(param)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch( (error) => {
                        ErrFunc({result: 0, errors: [{text:"Server error"}]});
                    })

            },
        },
        GenerateModalContent: () => {
            this.setState({isLoadModalContent: true});
            this.core.AJAX.getModulesSettGroup(this.props.ObjID, (answer) => {
                let modules = answer.data.modules;
                this.setState({
                    isLoadModalContent: false,
                    modules: modules,
                });
            }, (answer) => {
                answer.errors.forEach( (item_err, key_err) => {
                    ToastrStore.error('Error!', item_err.text);
                });
                this.setState({isLoadModalContent: false});
            });
        },
        SavePermissionGroup: (isCloseForm: boolean) => {
            this.setState({isLoadModalContent: true});

            let new_modules = [];

            this.state.modules.forEach( (item_m, key_m) => {
                let item_out = {
                    module: item_m.module,
                    allow: item_m.allow,
                };
                if(this.state.attrDataCache[item_m.module] != undefined) {
                    item_out.allow = this.state.attrDataCache[item_m.module].value;
                }
                new_modules.push(item_out);
            });

            this.core.AJAX.addPermissionGroup({group: this.props.ObjID, modules: new_modules}, (answer) => {
                ToastrStore.success('Success!');
                if(isCloseForm) {
                    this.events.onClose();
                }
                else {
                    this.setState({
                        attrDataCache: {},
                        isLoadModalContent: false,
                        modules: new_modules,
                    });
                }
            }, (answer) => {
                answer.errors.forEach( (item_err, key_err) => {
                    ToastrStore.error('Error!', item_err.text);
                });
                this.setState({isLoadModalContent: false});
            });
        },

        build: {
            content: () => {
                let ClothHDR;
                let ClothForm;
                let ClothTables;
                let ClothFormTabs;
                let ClothTablesTabs;
                let ClothFTR;

                ClothForm = <div className={"zt_co_form"}>{this.core.build.form()}</div>;

                ClothHDR = <div className={"cof_hdr"}>
                    <div className={"cof_titlebar"}>
                        <div className={"cof_title"}>{this.props.UserName}</div>
                    </div>
                    <div className="cof_hdr_btns_co">
                        <button className="cof_hdr_btn" onClick={()=>{
                            this.props.onClose();
                        }}><i className="fas fa-times" /></button>
                    </div>
                </div>;
                ClothFTR = <div className={"cof_ftr"}>
                    <div className={"cof_ftr_fixed_btns_co"}>
                    </div>
                    <div className={"cof_ftr_btns_co"}>
                        <button className="cof_ftr_btn nf_btn-ellipse" onClick={(e)=>{
                            this.events.onSaveGroups(e, false);
                        }} >Save</button>
                        <button className="cof_ftr_btn nf_btn-ellipse" onClick={(e)=>{
                            this.events.onSaveGroups(e, true);
                        }} >Save &amp; Close</button>
                        <button className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                            this.props.onClose();
                        }}><i className="fas fa-times"/>Close</button>
                    </div>
                </div>;


                return (
                    <div className={"zt_form_content zt_modal_form"}>
                        {ClothHDR}
                        <div className={"zt_form_tabs_bar"}>
                            {ClothFormTabs}
                            {ClothTablesTabs}
                        </div>
                        {ClothForm}
                        {ClothTables}
                        {ClothFTR}
                    </div>
                );
            },
            form: () => {
                function a11yProps(index: any) {
                    return {
                        id: `vertical-tab-${index}`,
                        'aria-controls': `vertical-tabpanel-${index}`,
                    };
                }
                function TabPanel(props: TabPanelProps) {
                    const { children, value, index, ...other } = props;

                    return (
                        <div
                            className={"zt_permis_tab_content"}
                            role="tabpanel"
                            hidden={value !== index}
                            id={`vertical-tabpanel-${index}`}
                            aria-labelledby={`vertical-tab-${index}`}
                            {...other}
                        >
                            {value === index && (
                                <Box p={2}>
                                    {children}
                                </Box>
                            )}
                        </div>
                    );
                }
                return (
                    <div className={"zt_co_perm_form"}>
                        <Tabs
                            orientation="vertical"
                            variant="scrollable"
                            value={this.state.TabsSett}
                            onChange={this.events.onChangeTab}
                            aria-label="Vertical tabs example"
                            className={""}
                        >
                            <Tab label="Modules" {...a11yProps(0)} />
                            <Tab label="Item Two" {...a11yProps(1)} />

                        </Tabs>
                        <TabPanel value={this.state.TabsSett} index={0}>
                            <h5>Modules</h5>
                            {this.core.build.modulesArea()}
                        </TabPanel>
                        <TabPanel value={this.state.TabsSett} index={1}>
                            Item Two
                        </TabPanel>
                    </div>);
            },

            modulesArea: () => {
                let ModulesJSX = [];
                this.state.modules.forEach( (item_m, key_m) => {
                    let isChecked = item_m.allow == 1;
                    let name = item_m.module;

                    if(this.state.attrDataCache[name] != undefined) {
                        isChecked = this.state.attrDataCache[name].value;
                    }

                    ModulesJSX.push(
                        <FormControlLabel key={key_m}
                            control={<Checkbox checked={!!isChecked} onChange={(e) => {
                                if(this.state.attrDataCache[name] == undefined) {
                                    this.state.attrDataCache[name] = {value: 0};
                                }
                                this.state.attrDataCache[name].value = Number(!isChecked);
                                this.setState({});
                            }} name={name} />}
                            label={name}
                        />
                    );
                });

                return (
                    <FormGroup row>
                        {ModulesJSX}
                    </FormGroup>
                );
            },
        }
    };

    helper = {
        resetCache: () => {},
        saveCache: () => {
        },
    };


    render() {
        let isBlocking = this.state.isLoadModalContent;
        let content = this.core.build.content();
        return (
            <BlockUi className={"zt_window_cloth"} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                {content}
            </BlockUi>);
    }
}