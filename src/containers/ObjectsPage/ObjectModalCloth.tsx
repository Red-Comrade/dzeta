import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import { HashRouter as Router, Route, Link, Switch as SwitchRouter, Redirect, RouteProps, RouteComponentProps} from 'react-router-dom'
import axios from 'axios';

import HZTabs from "../../components/HZTabs";

import * as ot_api from '../../api/ObjTree-api'
import * as ct_api from '../../api/ClassTree-api'

import INodeTemplate from './Interfaces/INodeTemplate'

import { useRouteMatch, useParams } from 'react-router-dom'
import Loader from "react-loaders";
import BlockUi from "react-block-ui";
import * as f_helper from "../../helpers/form_datatype_helper";

import RootStore from '../../stores/RootStore'
import ToastrStore from '../../stores/ToastrStore'
import {IValueFile} from "../../api/ObjTree-api";
import {IModalConfigState} from './ObjectsTreeInstance'

import ItemAttr from "./Interfaces/IitemAttr";

//#region material UI
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';
import ObjectTreeClass, {IStateObjectTree} from "./ObjectTreeClass";
import {IObjectsPageProps, IObjectsPageState} from "./ObjectsPage";

interface UIStyles extends Partial<Record<SwitchClassKey, string>> {
    focusVisible?: string;
}
interface UIProps extends SwitchProps {
    classes: UIStyles;
}
const IOSSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 42,
            height: 26,
            padding: 0,
            margin: theme.spacing(1),
        },
        switchBase: {
            padding: 1,
            '&$checked': {
                transform: 'translateX(16px)',
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#52d869',
                    opacity: 1,
                    border: 'none',
                },
            },
            '&$focusVisible $thumb': {
                color: '#52d869',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 24,
            height: 24,
        },
        track: {
            borderRadius: 26 / 2,
            border: `1px solid ${theme.palette.grey[400]}`,
            backgroundColor: theme.palette.grey[50],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }: UIProps) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});
//#endregion

import ObjectsStore from "../../stores/ObjectsStore";
import CoolTree from "../../components/CoolTree";
import IModalConfig from "../../components/CoolModalForm/IModalConfig";
import PermissionWindowCloth from "./PermissionWindowCloth";

export interface IObjectModalCloth {
    url_modal?: string,
    url_modal_to?: string,
    url_modal_prev?: string,

    typeModal: "types" | "edit" | "read" | "add",
    isAllowEdit: boolean,

    isMain?: boolean, //Указывает, что мы открываем главный контейнер, значит не строим кнопки "закрыть", также не строим дерево
    SaveFunc_callback?: (callback:(Conf:{SuccFunc:()=>void})=>void) => void,
    onChangeModalName?: (params:{Name: string, selectData: any}) => void,

    onClose?: () => void,
    savedObject?: () => void,
    addedObject?: (config:{newNode:{ObjID:string, Name:string}, selectData}) => void,
    onChangeTypeModal: (newType: "add" | "edit" | "read") => void,

    selectData?: {
        ObjID?: string,
        TypeID?: string,
        ParrentObjID?: string,
        ParrentTypeID?: string,
        Name?: string,
        isRoot?: boolean,
    }
}
interface IObjectWindowClothState extends IStateObjectTree {
    nameModal: string,

    isLoadModalContent: boolean,
    isLoadSaveContent: boolean,


    activeTypeFormTab?: 'form' | 'tables',
    activeTypesTab?: string,

    typesData: { uid: string, name: string }[],

    attrData: ItemAttr[],

    attrDataCache: any,
    attrDataFileCache: any,
}

//@inject("ToastrStore")
//@inject("ObjectsStore")
//@observer

export default class ObjectModalCloth extends ObjectTreeClass<IObjectModalCloth, IObjectWindowClothState> {
    static defaultProps = {
        typeModal: "edit",
        url_modal: "tmp",
    };

    getInitialState() {
        let initialStates = super.getInitialState();
        initialStates.nameModal = "Loading...";
        initialStates.isLoadModalContent = true;
        initialStates.isLoadSaveContent = false;
        initialStates.activeTypeFormTab = "form";
        initialStates.activeTypesTab = null;
        initialStates.typesData = [];
        initialStates.attrData = [];
        initialStates.attrDataCache = {};
        initialStates.attrDataFileCache = {};
        return initialStates;
    }


    xhrGenerateModalContent = {cancel: null};

    constructor(props: IObjectModalCloth) {
        super(props);
        this.state = this.getInitialState();
    };
    componentDidMount(): void {
        this.core.GenerateModalContent();
        if(!this.props.isMain && this.props.typeModal != "add") {
            let PathNodeID = this.props.url_modal_to;
            this.core.GenerateTree(PathNodeID);
        }
    };

    componentWillUnmount(): void {
        if(!this.props.isMain && this.props.typeModal != "add") {
            location.hash = this.props.url_modal_prev;
        }
    }

    shouldComponentUpdate(nextProps: Readonly<IObjectModalCloth>, nextState: Readonly<IObjectWindowClothState>, nextContext: any): boolean {
        if(nextProps.SaveFunc_callback) {
            nextProps.SaveFunc_callback(this.events.saveObject);
            return false;
        }

        return true
    }

    componentDidUpdate(prevProps: Readonly<IObjectModalCloth>, prevState: Readonly<IObjectWindowClothState>, snapshot?: any): void {
        if(prevProps.selectData.ObjID != this.props.selectData.ObjID || prevProps.selectData.TypeID != this.props.selectData.TypeID) {
            this.core.GenerateModalContent();
        }
        if(!this.props.isMain && this.props.typeModal != "add") {
            let PrevPathNode = prevProps.url_modal_to;
            let PathNodeID = this.props.url_modal_to;
            PathNodeID = PathNodeID ? PathNodeID : "";
            PrevPathNode = PrevPathNode ? PrevPathNode : "";
            let hasA = PathNodeID.indexOf('@') !== -1;


            if (this.state.buildTree && !this.state.isLoadTree) {
                this.setState({buildTree: false, buildCleanTree: false});
                //дерево будет построено только в том случае, если по УРЛ не нужно строить модальную форму т.е. Либо пустой урл, либо присутствует вначале @
                if(hasA || !PathNodeID) {
                    ObjectsStore.isLoadTree = false;
                    ObjectsStore.JSXTree = this.core.Tree.build();
                }
            }
            else if(this.state.buildCleanTree) {
                this.setState({buildCleanTree: false});
                if(hasA || !PathNodeID) {
                    ObjectsStore.isLoadTree = false;
                    ObjectsStore.JSXTree = this.core.Tree.build();
                }
            }

            if (!this.state.isLoadTree) {
                //Делаем проверку, если изменился узел в дереве по урл
                if (PathNodeID != PrevPathNode) {
                    if(hasA) {
                        PathNodeID = PathNodeID.slice(1);
                    }
                    let selectRoot = this.state.treeData[0];

                    if(!PathNodeID || selectRoot.id == PathNodeID) {
                        //root
                        this.setState({
                            SelectNode: selectRoot,
                            buildTree: true,

                            AddBtnDisabled: true,
                            EditBtnDisabled: true,
                            DelBtnDisabled: true,

                            selectNodeFunc: () => {
                                this.setState({selectNodeFunc: null, buildTree: true });
                                return selectRoot.id;
                            }
                        });
                    }
                    else {
                        this.setState({isLoadTree: true});
                        this.TreeCore.OpenTreeNode({Node: this.state.treeData[0], PathNodeID: PathNodeID, isSelect: true},
                            (config: { expandedKeys: string[], SelectNode? }) => {

                                let expandedKeys = this.state.expandedKeys;
                                if(this.state.expandedKeys) {
                                    for(let i = 0; i < config.expandedKeys.length; i++) {
                                        if( expandedKeys.indexOf(config.expandedKeys[i]) === -1 ) {
                                            expandedKeys.push(config.expandedKeys[i]);
                                        }
                                    }
                                }

                                let
                                    AddBtnDisabled = true,
                                    EditBtnDisabled = true,
                                    DelBtnDisabled= true;

                                if(config.SelectNode.isObject) {
                                    EditBtnDisabled = false;
                                    DelBtnDisabled =  false
                                }
                                else if(config.SelectNode.isClass) {
                                    AddBtnDisabled = false;
                                }


                                this.setState( {
                                    isLoadTree: false,
                                    buildTree: true,
                                    expandedKeys: expandedKeys,
                                    SelectNode: config.SelectNode,
                                    expandNodeFunc: () => {
                                        this.setState({expandNodeFunc: null, buildTree: true});
                                        return expandedKeys;
                                    },
                                    selectNodeFunc: () => {
                                        this.setState({selectNodeFunc: null, buildTree: true });
                                        return PathNodeID;
                                    },
                                    AddBtnDisabled: AddBtnDisabled,
                                    EditBtnDisabled: EditBtnDisabled,
                                    DelBtnDisabled: DelBtnDisabled,
                                });

                            }, () => {
                                this.setState({isLoadTree: false});
                                location.hash = this.props.url_modal;
                            });
                    }
                }
            }
        }
    };

    events = {
        onClickCloth: (e) => {
            if(this.props.url_modal_to) {
                location.hash = this.props.url_modal + '/' + this.props.url_modal_to;
            }
            else {
                location.hash = this.props.url_modal;
            }
        },

        saveObject: (Conf) => {
            if(this.state.isLoadModalContent || this.state.isLoadSaveContent) {
                //Всякие кнопки сохранения объекта работать не будут, пока не прогрузится форма
                return false;
            }
            let values = [];
            let Name_obj = "";
            let ValidErrorArr = [];

            this.state.attrData.forEach( (item, index) => {
                let _item = {
                    value: item.value ? item.value.value : item.value,
                    attr: item.uid,
                    key: item.isKey ? 1 : 0,
                    datatype: item.datatype,
                };

                if(item.datatype != "counter") {
                    if(item.datatype == "object") {
                        _item.value = item.value &&  item.value.uid ? item.value.uid : "";
                    }

                    if(this.state.attrDataCache[item.uid] != undefined) {
                        _item.value = this.state.attrDataCache[item.uid].value;
                        if(item.datatype == "object") {
                            _item.value = _item.value && Object.keys( _item.value).length > 0 ? _item.value : "";
                        }

                        values.push(_item);
                    }
                    else if(item.isKey) {
                        values.push(_item);
                    }

                    if(item.isKey) {
                        Name_obj += (_item.value === null ? '' : _item.value) + ' ';
                    }

                    if((item.require && !_item.value) || (item.isKey && !_item.value)) {
                        if(item.datatype == "file") {
                            if(
                                (this.state.attrDataFileCache[item.uid] == undefined || this.state.attrDataFileCache[item.uid].files.length == 0) && !item.value  ) {
                                ValidErrorArr.push({name: item.name, text: "Enter value!"})
                            }
                        }
                        else {
                            ValidErrorArr.push({name: item.name, text: "Enter value!"})
                        }
                    }
                }
            });

            if(ValidErrorArr.length > 0) {
                ToastrStore.clear();
                ValidErrorArr.forEach((item_err) => {
                    ToastrStore.error("parameter|" + item_err.name + "| " + item_err.text);
                });
            }
            else {
                this.setState({isLoadSaveContent: true});
                if(Name_obj) {
                    Name_obj = Name_obj.slice(0, -1)
                }
                this.core.AJAX.saveObjectAJAX(this.props.selectData.ObjID, this.props.selectData.TypeID, this.props.selectData.ParrentObjID, values, (data) => {
                    ToastrStore.success('Success!', 'Success!');
                    this.setState({isLoadSaveContent: false});

                    this.helper.saveCache();

                    this.props.selectData.Name = Name_obj;

                    let new_Name = data.objName;


                    if(this.props.onChangeModalName) {
                        this.props.onChangeModalName({Name: new_Name, selectData: this.props.selectData});
                    }

                    let Files = [];
                    let values:IValueFile[] = [];
                    let iter = 0;
                    if(Object.keys(this.state.attrDataFileCache).length > 0) {
                        RootStore.ProgressIsOpen = true;
                        for( let key_FileCache in this.state.attrDataFileCache) {
                            let File_o = this.state.attrDataFileCache[key_FileCache];
                            let Files_tmp = File_o.files;
                            for( let i = 0; i < Files_tmp.length; i++ ) {
                                Files.push(Files_tmp[i]);
                                values.push({
                                    uid: File_o.data.uid, //uid-параметра
                                    new: 1, //0/1
                                    key: Number(File_o.data.isKey), //0/1
                                    value: iter,
                                    del: 0,
                                });
                                iter ++;
                            }
                        }
                        this.core.AJAX.addFileAJAX(this.props.selectData.ObjID, this.props.selectData.TypeID,
                            this.props.selectData.ParrentObjID, Files, values,
                            () => {
                                this.setState({attrDataFileCache: {} });
                                RootStore.ProgressIsOpen = false;
                                this.events.savedObject(Conf);

                            }, () => {
                                RootStore.ProgressIsOpen = false;
                            });
                    }
                    else {
                        this.events.savedObject(Conf);
                    }

                }, (data) => {
                    ToastrStore.error('Error!', 'ERROR CODE='+data.result);
                    this.setState({isLoadSaveContent: false});
                });
            }
        },
        savedObject: (Conf) => {
            //Сюда перейдет, как только мы сохранили все файлы, либо файлов нет
            if(this.props.isMain) {
                //если у нас форма объекта, то мы её не закрываем, а обновляем
                this.helper.resetCache();
                this.core.GenerateModalContent();

                if(Conf) {
                    if(Conf.SuccFunc) {
                        Conf.SuccFunc();
                    }
                }
            }

        },

        addObject: (Conf) => {
            if(this.state.isLoadModalContent || this.state.isLoadSaveContent) {
                //Всякие кнопки сохранения объекта работать не будут, пока не прогрузится форма
                return false;
            }
            let values = [];
            let Name_obj = "";
            let ValidErrorArr = [];

            this.state.attrData.forEach( (item, index) => {
                let _item = {
                    value: item.value ? item.value.value : item.value,
                    attr: item.uid,
                    key: item.isKey ? 1 : 0,
                    datatype: item.datatype,
                };
                if(_item.datatype != "counter") {
                    if(this.state.attrDataCache[item.uid] != undefined) {
                        _item.value = this.state.attrDataCache[item.uid].value;
                        if(item.datatype == "object") {
                            _item.value = _item.value && Object.keys( _item.value).length > 0 ? _item.value : "";
                        }
                        values.push(_item);
                    }
                    else if(item.isKey) {
                        values.push(_item);
                    }

                    if(item.isKey) {
                        Name_obj += (_item.value === null ? '' : _item.value) + ' ';
                    }

                    if((item.require && !_item.value) || (item.isKey && !_item.value)) {
                        if(item.datatype == "file") {
                            if(this.state.attrDataFileCache[item.uid] == undefined || this.state.attrDataFileCache[item.uid].files.length == 0 ) {
                                ValidErrorArr.push({name: item.name, text: "Enter value!"})
                            }
                        }
                        else {
                            ValidErrorArr.push({name: item.name, text: "Enter value!"})
                        }
                    }
                }
            });

            if(ValidErrorArr.length > 0) {
                ToastrStore.clear();
                ValidErrorArr.forEach((item_err) => {
                    ToastrStore.error("parameter|" + item_err.name + "| " + item_err.text);
                });
            }
            else {
                this.setState({isLoadSaveContent: true});
                if(Name_obj) {
                    Name_obj = Name_obj.slice(0, -1)
                }
                this.core.AJAX.addObject(this.props.selectData.TypeID, this.props.selectData.ObjID, values, (answer) => {
                    ToastrStore.success('Success!', 'Success!');
                    this.setState({isLoadSaveContent: false});
                    this.helper.saveCache();

                    let new_ObjID = answer.data.uid;
                    let new_Name = answer.objName;

                    let Files = [];
                    let values:IValueFile[] = [];
                    let iter = 0;
                    if(Object.keys(this.state.attrDataFileCache).length > 0) {
                        RootStore.ProgressIsOpen = true;
                        for( let key_FileCache in this.state.attrDataFileCache) {
                            let File_o = this.state.attrDataFileCache[key_FileCache];
                            let Files_tmp = File_o.files;
                            for( let i = 0; i < Files_tmp.length; i++ ) {
                                Files.push(Files_tmp[i]);
                                values.push({
                                    uid: File_o.data.uid, //uid-параметра
                                    new: 1, //0/1
                                    key: Number(File_o.data.isKey), //0/1
                                    value: iter,
                                    del: 0,
                                });
                                iter ++;
                            }
                        }

                        this.core.AJAX.addFileAJAX(new_ObjID, this.props.selectData.TypeID, this.props.selectData.ObjID, Files, values, () => {
                            this.setState({attrDataFileCache: {}});
                            RootStore.ProgressIsOpen = false;
                            this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}, ...Conf});
                        }, () => {
                            this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}, ...Conf});
                            RootStore.ProgressIsOpen = false;
                        });
                    }
                    else {
                        this.events.addedObject({...{ObjID: new_ObjID, Name: new_Name}, ...Conf});
                    }

                }, (data) => {
                    // ToastrStore.error('Error!', 'ERROR CODE='+data.result);
                    this.core.showErrors(data.errors);
                    this.setState({isLoadSaveContent: false});
                });
            }
        },
        addedObject: (Conf?: {SuccFunc?:()=>void, ObjID?:string, Name?:string }) => {
            if(Conf) {
                if(this.props.addedObject) {
                    this.props.addedObject({
                        newNode: {ObjID: Conf.ObjID, Name: Conf.Name},
                        selectData: this.props.selectData,
                    })
                }
                if(Conf.SuccFunc) {
                    Conf.SuccFunc();
                }
            }
        },

        onClickTypeFormTab: (e, GrTypeForm) => {
            this.setState({activeTypeFormTab: GrTypeForm});
        },
        onClickTypesTab: (e, GroupID) => {
            this.setState({activeTypesTab: GroupID});
        },
    };

    //Не получилось наследовать
    protected FormCore = {
        GenerateObjectModal: (params: {
            url_modal?: string,
            url_modal_to?: string,
            url_modal_prev?: string,
            isMain?: boolean,
            typeModal: "types" | "edit" | "read" | "add",
            selectData: {
                ObjID?: string,
                TypeID?: string,
                ParrentObjID?: string,
                ParrentTypeID?: string,
                Name?: string,
                isRoot?: boolean,
            },
        }) => {
            let uid_form = "tmp";
            if (params.typeModal == "edit" || params.typeModal == "read") {
                uid_form = '*' + params.selectData.ObjID;
            }

            //TODO::ZAGL PERMISSION
            let isAllowEdit = true;
            if (document.location.host == "arctic.ozoneprogram.ru" && RootStore.MSettings.UserName != "Administrator") {
                isAllowEdit = false;
            }

            //Если нет прав то принудительно режим редактирования
            if (params.typeModal != "read" && !isAllowEdit || this.isReadMode) {
                params.typeModal = "read";
            }

            let DataCloth: IObjectModalCloth = {
                url_modal: params.url_modal,
                url_modal_to: params.url_modal_to,
                url_modal_prev: params.url_modal_prev,

                isMain: params.isMain,

                onClose: () => {
                    delete this.state.Forms[uid_form];
                    RootStore.CloseForm(uid_form);
                },
                savedObject: () => {
                    //this.core.reload();
                },
                onChangeTypeModal: (newType: "add" | "edit" | "read") => {
                    this.isReadMode = newType == "read";
                    params.typeModal = newType;
                    this.FormCore.GenerateObjectModal(params);
                },

                typeModal: params.typeModal,
                isAllowEdit: isAllowEdit,
                selectData: {
                    ObjID: params.selectData.ObjID,
                    TypeID: params.selectData.TypeID,
                    ParrentObjID: params.selectData.ParrentObjID,
                    ParrentTypeID: params.selectData.ParrentTypeID,
                    Name: params.selectData.Name,
                    isRoot: params.selectData.isRoot,
                },
            };


            if(params.isMain) {
                ObjectsStore.PageContent.contentJSX = <ObjectModalCloth {...DataCloth} />;
            }
            else {
                let ModalConf: IModalConfig = {
                    id: uid_form,
                    onClickCloth: () => {
                    },
                    content: {
                        bodyJSX: <ObjectModalCloth {...DataCloth} />,
                    },
                };

                ObjectsStore.isLoadTree = true;

                const Forms = this.state.Forms as any;

                if (Forms[uid_form] == undefined) {
                    Forms[uid_form] = {
                        ...ModalConf,
                        ...{RootNode: null,
                            url_modal_to: params.url_modal_to,
                            url_modal: uid_form,
                            type: "object"},
                    } as IModalConfigState;
                    RootStore.OpenForm(uid_form, ModalConf);
                } else {
                    Forms[uid_form].url_modal_to = params.url_modal_to;
                    Forms[uid_form].url_modal = uid_form;
                    RootStore.OpenForm(uid_form, ModalConf);
                }
            }
        },

        GeneratePermissionModal: (params: {
            ObjID: string,
            Name: string,
        }) => {
            let uid_form = 'permis-' + params.ObjID;
            if (this.state.Forms[uid_form] == undefined) {
                let PermData = {
                    url_modal: uid_form,
                    ObjID: params.ObjID,
                    onClose: () => {
                        delete this.state.Forms[uid_form];
                        RootStore.CloseForm(uid_form);
                    },
                    UserName: params.Name,
                };

                let ModalConf: IModalConfig = {
                    id: uid_form,
                    onClickCloth: () => {
                    },
                    content: {
                        bodyJSX: <PermissionWindowCloth {...PermData} />,
                    }
                };

                const Forms = this.state.Forms as any;

                Forms[uid_form] = {
                    ...ModalConf,
                    ...{RootNode: null, url_modal: uid_form, type: "permis"},
                };
                RootStore.OpenForm(uid_form, ModalConf);
            }
        },
    };
    core = {
        AJAX: {
            getAttributesOfGroup: (type:string,group:string, SuccFunc, ErrFunc) => {
                ct_api
                    .getAttributesOfGroup(type, group)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch( (error) => {
                        ErrFunc({result: 0});
                    })
            },
            addObject: function (type:string, parent:string, values: ot_api.IValue[], SuccFunc, ErrFunc) {
                ot_api
                    .addObject(type, parent, values)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        ErrFunc({result: 0});
                    })
            },

            getAllValuesAJAX:  (uid:string, SuccFunc, ErrFunc) => {
                if(this.xhrGenerateModalContent.cancel) {
                    this.xhrGenerateModalContent.cancel();
                }
                ot_api
                    .getAllValues(uid, this.xhrGenerateModalContent)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch( (error) =>{
                        if(axios.isCancel(error)) {
                            console.error("Отмена");
                            //ручная отмена
                        }
                        else {
                            console.log(error);
                            ErrFunc({result: 0});
                        }
                    })
            },
            getTypesChildrenAJAX: (type:string, SuccFunc, ErrFunc) => {
                if(this.xhrGenerateModalContent.cancel) {
                    this.xhrGenerateModalContent.cancel();
                }
                ct_api
                    .getChildren(type, this.xhrGenerateModalContent)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);

                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        if(axios.isCancel(error)) {
                            console.error("Отмена");
                            //ручная отмена
                        }
                        else {
                            console.log(error);
                            ErrFunc({result: 0});
                        }
                    })
            },
            saveObjectAJAX: function (uid:string, type:string, parent:string, values: ot_api.IValue[], SuccFunc, ErrFunc) {
                ot_api
                    .saveObject(uid, type, parent, values)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);

                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrFunc({result: 0});
                    })
            },
            addFileAJAX: (uid:string, type:string, parent:string, Files:any, values: IValueFile[], SuccFunc, ErrorFunc) => {
                ot_api
                    .addFile(uid, type, parent, Files, values, (progressEvent) => {
                        let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                        RootStore.ProgressData.completed = percentCompleted;
                    })
                    .then(res => {
                        SuccFunc(res.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrorFunc();
                    })
            },
        },

        //Прогрузка данных для основного дерева
        GenerateTree: (PathNodeID: string) => {
            let rootNode:INodeTemplate = {
                isRoot: true,
                ParentIDTree: "",
                id: "",
                key: "",
                ParentID: "",
                iddb: "",

                Name: "",
                icon: "fas fa-sitemap",

                children: [],

                isObject: true,
                isClass: false,
                IsFolder: false,

                TypeID: "",
                ObjID: "",
                FolderID: "",

                ParrentTypeID: "",
                ParrentFolderID: "",
                ParrentObjID: "",
            };
            this.TreeHelper.extend_rec(rootNode, this.props.selectData);
            rootNode.children = [];
            rootNode.id = '*'+rootNode.ObjID;
            rootNode.key = '*'+rootNode.ObjID;

            PathNodeID = PathNodeID ? PathNodeID : "";
            let hasA = PathNodeID.indexOf('@') !== -1;
            if(hasA) {
                PathNodeID = PathNodeID.slice(1);
            }

            this.TreeCore.GenerateRecOpen({Node: rootNode, PathNodeID: PathNodeID }, (config:{expandedKeys:string[], treeData: INodeTemplate, SelectNode?}) => {
                let treeData:INodeTemplate[] = [config.treeData];// [config.treeData] ;

                if(config.expandedKeys.indexOf(rootNode.id) === -1) {
                    config.expandedKeys.push(rootNode.id);
                }

                let StateCh = {
                    treeData: treeData,
                    isLoadTree: false,
                    buildTree: true,
                    expandedKeys: config.expandedKeys,
                    RedrafFunc: () => {
                        this.setState({RedrafFunc: null, buildTree: true})
                    },
                    expandNodeFunc: () => {
                        this.setState({expandNodeFunc: null, buildTree: true });
                        return config.expandedKeys;
                    },
                    AddBtnDisabled: true,
                    EditBtnDisabled: true,
                    DelBtnDisabled: true,
                };

                if (config.SelectNode) {
                    if(config.SelectNode.isObject) {
                        StateCh.EditBtnDisabled = false;
                        StateCh.DelBtnDisabled =  false
                    }
                    else if(config.SelectNode.isClass) {
                        StateCh.AddBtnDisabled = false;
                    }

                    Object.assign(StateCh, {
                        SelectNode: config.SelectNode,
                        selectNodeFunc: () => {
                            this.setState({selectNodeFunc: null, buildTree: true });
                            return PathNodeID;
                        }
                    });
                    this.setState(StateCh);
                }
                else {
                    this.setState(StateCh);
                    //Если нет такого узла в дереве, то мы возвращаем наше дерево в базовое состояние
                    location.hash = this.props.url_modal;
                }
            });
        },


        GenerateModalContent: () => {
            this.setState({isLoadModalContent: true, attrData: [], attrDataCache: {}, attrDataFileCache: {}});

            const GenerateTypes = (ParrentTypeID) => {
                this.core.AJAX.getTypesChildrenAJAX(ParrentTypeID, (data) => {
                    this.setState({typesData: data.data, isLoadModalContent: false});
                }, (data) => {
                    ToastrStore.error('Server error!', 'ErrorCode='+data.result);
                    this.setState({isLoadModalContent: false});
                });
            };

            if(this.props.typeModal == "types") {
                //Значит мы открываем начальную форму корня
                let ParrentTypeID = this.props.selectData.ParrentTypeID;

                if(this.props.selectData.isRoot) {
                    ParrentTypeID = this.props.selectData.TypeID;
                }
                GenerateTypes(ParrentTypeID);
            }
            else if(this.props.typeModal == "edit" || this.props.typeModal == "read") {
                let attrData = [];
                let nameModal = "#loading error#";
                this.core.AJAX.getAllValuesAJAX(this.props.selectData.ObjID, (data)=>{
                    attrData = data.data[this.props.selectData.ObjID];
                    nameModal = this.helper.getNameModal(attrData);
                    GenerateTypes(this.props.selectData.TypeID);

                    this.setState({attrData: attrData, attrDataCache: {}, attrDataFileCache: {}, isLoadModalContent: false, nameModal: nameModal});
                }, (data)=>{
                    ToastrStore.error('Server error!', 'ErrorCode='+data.result);
                    this.setState({attrData: attrData, attrDataCache: {}, attrDataFileCache: {}, isLoadModalContent: false, nameModal: nameModal})
                });
            }
            else if(this.props.typeModal == "add") {
                let nameModal = "Add in " +  this.props.selectData.Name;
                this.core.AJAX.getAttributesOfGroup(this.props.selectData.TypeID, '00000000-0000-0000-0000-000000000000', (answer)=> {
                    let attrData = [];
                    if(answer.result == 1) {
                        attrData = answer.data;
                    }
                    this.setState({attrData: attrData, attrDataCache: {}, attrDataFileCache: {}, isLoadModalContent: false, nameModal: nameModal})
                }, (answer) => {
                    ToastrStore.error('Server error!', 'ErrorCode='+answer.result);
                    this.setState({ attrDataCache: {}, attrDataFileCache: {}, isLoadModalContent: false, nameModal: nameModal})
                });
            }
        },

        Tree: {
            build: () => {
                const DataCoolTree = {
                    RootTab: {
                        ID: this.props.selectData.ObjID,
                        Name: this.props.selectData.Name,
                    },

                    treeData: this.state.treeData,
                    isFullLoad: true,
                    selectNodeFunc: this.state.selectNodeFunc,
                    expandNodeFunc: this.state.expandNodeFunc,
                    expandNodeTabFunc: this.state.expandNodeTabFunc,
                    ShowFromLevelFunc: this.state.ShowFromLevelFunc,
                    RedrafFunc: this.state.RedrafFunc,
                    UpdateGlobalTree: this.state.UpdateGlobalTree,

                    //Кнопки
                    AddBtnDisabled: this.state.AddBtnDisabled,
                    EditBtnDisabled: this.state.EditBtnDisabled,
                    DelBtnDisabled: this.state.DelBtnDisabled,

                    OnAddBtnFunc: (selectData) => {
                    },
                    OnEditBtnFunc: (selectData) => {
                        if(selectData) {
                            location.hash = this.props.url_modal + '/' + selectData.id;
                        }
                    },
                    OnDelBtnFunc: (selectData) => {
                    },

                    onSelect: (selectData) => {
                        if(selectData) {
                            //let url_modal = "";
                            //this.setState({SelectNode: selectData});
                            location.hash = this.props.url_modal+'/@'+selectData.id;
                        }
                    },
                    onSelectR: (selectData) => {
                    },
                    onOpenCM: (params: {SelectData:any, callbackCloseCM:()=>void, IsBtnCM?:boolean}, callback) => {
                        let contentCM = [];

                        let contentCMothers = [];
                        if(params.IsBtnCM) {
                            //Контекстное меню от кнопки
                        }
                        else {
                            if(params.SelectData.isClass) {
                                contentCM.push({
                                    "label": "Create",
                                    "action": ()=> {
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-cube',
                                    "children": false
                                });
                            }
                            if(params.SelectData.isObject) {
                                contentCM.push({
                                    "label": "Edit",
                                    "action": ()=> {
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-edit',
                                    "children": false
                                });


                                if(params.SelectData.TypeID == "00000000-0000-0000-0000-000000000002") {
                                    contentCM.push({
                                        "label": "Permission",
                                        "action": ()=> {
                                            /*
                                            this.core.Modal.GeneratePermissionModal({
                                                ObjID: params.SelectData.ObjID,
                                                Name: params.SelectData.Name,
                                            });

                                             */
                                            params.callbackCloseCM();
                                        },
                                        "icon_className": 'fas fa-user-shield',
                                        "children": false
                                    });
                                }
                                if(params.SelectData.TypeID == "00000000-0000-0000-0000-000000000001") {
                                    contentCM.push({
                                        "label": "Generate Password",
                                        "action": ()=> {
                                            /*
                                            this.setState({
                                                ObjectModalConfig: {
                                                    show: true,
                                                    TypeModal: "GenPassword",
                                                    data: {}
                                                }
                                            });

                                             */
                                            params.callbackCloseCM();
                                        },
                                        "icon_className": 'fas fa-edit',
                                        "children": false
                                    });
                                }


                                contentCM.push({
                                    "label": "Delete",
                                    "action": ()=> {
                                        /*
                                        this.setState({
                                            ObjectModalConfig: {
                                                show: true,
                                                TypeModal: "del",
                                                data: {}
                                            }
                                        });

                                         */
                                        params.callbackCloseCM();
                                    },
                                    "icon_className": 'fas fa-times',
                                    "children": false
                                });

                            }
                        }
                        contentCM.push({
                            "label": "Display from current level",
                            "action": ()=> {
                                this.setState({
                                    buildTree: true,
                                    ShowFromLevelFunc: (
                                        (callback) => {
                                            this.setState({
                                                ShowFromLevelFunc: null,
                                                buildTree: true,
                                            });
                                            callback(params.SelectData.id);
                                        })
                                });
                                params.callbackCloseCM();
                            },
                            "icon_className": 'far fa-eye',
                            "children": false
                        });


                        /*
                        if(params.SelectData.isClass) {
                            let isDisabledPaste = true;
                            let NamePaste = "Paste";
                            if(this.state.CutNode || this.state.CopyNode) {
                                if(this.state.CutNode) {
                                    NamePaste = "Paste(cut) (\'" + this.state.CutNode.Name + "\')";
                                    if( params.SelectData.TypeID == this.state.CutNode.TypeID) {
                                        isDisabledPaste = false;
                                    }
                                }
                                if(this.state.CopyNode) {
                                    NamePaste = "Paste(copy) (\'" + this.state.CopyNode.Name + "\')";
                                    if( params.SelectData.TypeID == this.state.CopyNode.TypeID) {
                                        isDisabledPaste = false;
                                    }
                                }
                            }
                            contentCMothers.push({
                                "label": NamePaste,
                                "disabled" : isDisabledPaste,
                                "action": ()=> {
                                    if(!isDisabledPaste) {
                                        if(this.state.CutNode) {
                                            this.core.changeLinkObj(this.state.CutNode.ObjID, this.state.CutNode.ParrentObjID, params.SelectData.ObjID, () => {
                                                this.props.ObjectsStore.CutNodeAction({
                                                    Node: this.state.CutNode,
                                                    ToNode: params.SelectData,
                                                });
                                                params.callbackCloseCM();
                                            });
                                        }
                                    }
                                },
                                "icon_className": 'fas fa-paste',
                                "children": false
                            });

                            let NameImport = "Import";
                            contentCMothers.push({
                                "label": NameImport,
                                "action": (a, b, c, d) => {
                                    console.log(params);
                                    this.setState({
                                        ImportModalConfig: {
                                            show: true,
                                            class: params.SelectData.TypeID,
                                            parent: params.SelectData.ParentID,
                                        }
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-upload',
                                "children": false
                            });
                        }



                        if(params.SelectData.isObject) {
                            contentCMothers.push({
                                "label": "Copy",
                                "action": ()=> {
                                    this.setState({
                                        CopyNode: params.SelectData,
                                        CutNode: null,
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-copy',
                                "children": false
                            });
                            contentCMothers.push({
                                "label": "Cut",
                                "action": ()=> {
                                    this.setState({
                                        CutNode: params.SelectData,
                                        CopyNode: null,
                                    });
                                    params.callbackCloseCM();
                                },
                                "icon_className": 'fas fa-cut',
                                "children": false
                            });
                        }

                         */

                        if(contentCMothers.length > 0) {
                            contentCM.push({
                                "label": "Others",
                                "action": ()=> {},
                                "icon_className": 'fa fa-ellipsis-h',
                                "children": contentCMothers
                            });
                        }
                        return contentCM;
                    },
                    onExpandNode: this.TreeCore.onExpandNode,
                    onLoadData: this.TreeCore.onLoadData,
                };

                return <CoolTree {...DataCoolTree} />;
            }
        },

        build: {
            content: () => {
                let ClothHDR;
                let ClothForm;
                let ClothTables;
                let ClothFormTabs;
                let ClothTablesTabs;
                let ClothFTR;
                if(this.props.typeModal == "types") {
                    ClothTablesTabs = <div>{this.core.build.tabsTables()}</div>;
                }
                else {
                    ClothForm = <div className={"zt_co_form " + (this.state.activeTypeFormTab!="form"?"hide-el":"")}>{this.core.build.form()}</div>;
                    ClothTables = <div className={"zt_co_tables " + (this.state.activeTypeFormTab!="tables"?"hide-el":"")}> </div>;
                    ClothFormTabs = this.core.build.tabsTablesOrForm();
                    ClothTablesTabs = <div className={this.state.activeTypeFormTab!="tables"?"hide-el":""}>{this.core.build.tabsTables()}</div>;


                    //header
                    if(this.props.isMain) {
                        ClothHDR = <div className={"cof_hdr"}>
                            <div className={"cof_titlebar"}>
                                <div className={"cof_title"}>{this.state.nameModal}</div>
                            </div>
                        </div>;
                    }
                    else {
                        ClothHDR = <div className={"cof_hdr"}>
                            <div className={"cof_titlebar"}>
                                <div className={"cof_title"}>{this.state.nameModal}</div>
                            </div>
                            <div className="cof_hdr_btns_co">
                                <button className="cof_hdr_btn" onClick={()=>{
                                    this.props.onClose();
                                }}><i className="fas fa-times" /></button>
                            </div>
                        </div>;
                    }


                    let FTR_FixedControls = [];
                    let FTR_Controls = [];

                    if(this.props.typeModal == "read" || this.props.typeModal == "edit") {
                        if(this.props.isAllowEdit) {
                            FTR_FixedControls.push(
                                <FormControlLabel
                                    key={"sw_type_modal"}
                                    className={"cof_ftr_switch_label"}
                                    control={<IOSSwitch checked={this.props.typeModal != "read"}
                                                        onChange={(e)=>{
                                                            let newTypeModal:"edit"|"read" = this.props.typeModal == "edit" ? "read" : "edit";
                                                            if(this.props.onChangeTypeModal) {
                                                                this.props.onChangeTypeModal(newTypeModal);
                                                            }
                                                        }} name="checkedA" />}
                                    label="Edit mode"
                                    color="success"
                                />
                            );

                            FTR_Controls.push(
                                <button key={"save_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                    this.events.saveObject({});
                                }} >Save</button>
                            );

                            if(!this.props.isMain) {
                                FTR_Controls.push(
                                    <button key={"save_ftr_btn_yet"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                        this.events.saveObject({SuccFunc: () => { this.props.onClose(); }} );
                                    }} >Save &amp; Close</button>
                                );
                                FTR_Controls.push(
                                    <button key={"close_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                        this.props.onClose();
                                    }} ><i className="fas fa-times"/> Close</button>
                                );
                            }
                        }
                    }
                    else if(this.props.typeModal == "add") {
                        FTR_Controls.push(
                            <button key={"add_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                this.events.addObject({SuccFunc: () => { this.props.onClose(); }} );
                            }} >Add</button>
                        );
                        FTR_Controls.push(
                            <button key={"close_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                this.props.onClose();
                            }} ><i className="fas fa-times"/> Close</button>
                        );
                    }
                    else {
                        //types
                        if(!this.props.isMain) {
                            FTR_Controls.push(
                                <button key={"close_ftr_btn"} className="cof_ftr_btn nf_btn-ellipse" onClick={()=>{
                                    this.props.onClose();
                                }} ><i className="fas fa-times"/> Close</button>
                            );
                        }
                    }

                    ClothFTR = (
                        <div className={"cof_ftr"}>
                            <div className={"cof_ftr_fixed_btns_co"}>
                                {FTR_FixedControls}
                            </div>
                            <div className={"cof_ftr_btns_co"}>
                                {FTR_Controls}
                            </div>
                        </div>
                    );

                }

                return (
                    <div className={"zt_form_content "+ (this.props.isMain ? "zt_root_form" : "zt_modal_form")}>
                        {ClothHDR}
                        <div className={"zt_form_tabs_bar " + (this.props.typeModal=="add"?"hide-el":"")}>
                            {ClothFormTabs}
                            {ClothTablesTabs}
                        </div>
                        {ClothForm}
                        {ClothTables}
                        {ClothFTR}
                    </div>
                );
            },


            form: () => {
                let controls = [];
                if(this.props.typeModal == "edit" || this.props.typeModal == "add" || this.props.typeModal == "read") {
                    this.state.attrData.forEach( (item, index) => {
                        const paramsControl:f_helper.IFormDatatypeClass = {
                            item: item,
                            key: index,
                            attrData: this.state.attrData,
                            attrDataCache: this.state.attrDataCache,
                            attrDataFileCache: this.state.attrDataFileCache,
                            type: this.props.typeModal as ("add" | "edit" | "read"),
                            ObjID: this.props.selectData.ObjID,
                            TypeID: this.props.selectData.TypeID,

                            ChangeStateFunc: () => { this.setState({});}, // this.setState();
                        };
                        let itemControl = new f_helper.FormDatatypeClass(paramsControl);
                        controls.push(itemControl.GetControl());
                    });
                }
                return <form className="zt_form">{controls}</form>;
            },

            tabsTablesOrForm: () => {
                let DataTabs = [];
                let classActive = "active";

                DataTabs.push({
                    className: this.state.activeTypeFormTab == "form" ? classActive : "",
                    Name: "Form",
                    key: "form",
                    onClick: (e) => { this.events.onClickTypeFormTab(e, "form"); },
                    onContextMenu: (e) => {
                        e.preventDefault();
                        return false;
                    }
                });
                DataTabs.push({
                    className: this.state.activeTypeFormTab == "tables" ? classActive : "",
                    Name: "Tables",
                    key: "tables",
                    onClick: (e) => { this.events.onClickTypeFormTab(e, "tables"); },
                    onContextMenu: (e) => {
                        e.preventDefault();
                        return false;
                    }
                });
                const dataHZTabs = {
                    callbackRecalc: () => {},
                    className: "pp_type_form_tabs",
                    data: DataTabs
                };

                return (
                    <HZTabs {...dataHZTabs} />
                );
            },
            tabsTables: () => {
                let DataTabs = [];
                let classActive = "active";
                let isExActive = false;

                this.state.typesData.forEach( (itemType, key_type) => {
                    if(this.state.activeTypesTab == itemType.uid) {
                        isExActive = true;
                    }

                    DataTabs.push({
                        className: this.state.activeTypesTab == itemType.uid ? classActive : "",
                        Name: itemType.name,
                        key: itemType.uid,
                        onClick: (e) => { this.events.onClickTypesTab(e, itemType.uid); },
                        onContextMenu: (e) => {
                            console.error(e,'cclick');
                            e.preventDefault();
                            return false;
                        }
                    });
                });

                const dataHZTabs = {
                    callbackRecalc: () => {},
                    className: "pp_types_bar_tabs",
                    data: DataTabs
                };

                return (
                    <HZTabs {...dataHZTabs} />
                );
            },
        },

        showErrors: (errors) => {
            if (errors) {
                let text = [];
                for (let i = 0; i < errors.length; i++) {
                    text.push(errors[i].text);
                }
                ToastrStore.error(text.join("\n"), 'Error');
            } else {
                ToastrStore.error('Server error!', 'Error');
            }
        },
    };

    helper = {
        resetCache: () => {
            this.setState({attrDataCache: {}, attrDataFileCache:{} });
        },
        saveCache: () => {
            this.state.attrData.forEach( (item, index) => {
                if(this.state.attrDataCache[item.uid] != undefined) {
                    if(!item.value) {
                        item.value = {};
                    }
                    if (item.isArray) {
                        if (item.datatype == 'object') {
                            item.value = this.state.attrDataCache[item.uid].value.map((v) => {
                                return { uid: v.value, name: v.name };
                            });
                        } else {
                            item.value = this.state.attrDataCache[item.uid].value
                        }
                    } else {
                        item.value.value = this.state.attrDataCache[item.uid].value
                    }
                }
            });

            this.setState({attrDataCache: {}});
        },

        getNameModal: (attrData) => {
            let nameModal = "";

            attrData.forEach((item_a, key_a) => {
                if(item_a.isKey) {
                    if(item_a.datatype == "object") {
                        nameModal += (item_a.value &&  item_a.value.name ? item_a.value.name : "") + ' ';
                    }
                    else {
                        nameModal += (item_a.value ? item_a.value.value : "") + ' ';
                    }
                }
            });

            if(nameModal) {
                nameModal = nameModal.slice(0, -1)
            }

            return nameModal;
        },
    };


    render() {
        let isBlocking = this.state.isLoadModalContent || this.state.isLoadSaveContent;
        let content = this.core.build.content();

        let url_modal = this.props.url_modal;
        if(this.props.url_modal_to) {
            url_modal += '/' + this.props.url_modal_to + '/';
        }
        const CheckModalUrlOpen = (params) => {
            //делаем проход по дереву на наличие такого узла, чтобы открыть
            //Если мы находим такой узел, то открываем модальную форму
            if(!this.state.isLoadTree)  {
                let PathNodeID = this.props.url_modal_to;
                PathNodeID = PathNodeID ? PathNodeID : "";

                if(PathNodeID && PathNodeID.indexOf('@') == -1 ) {
                    let ObjID_A = PathNodeID.split('*');

                    //Проверка, чтобы не был открыт тип, так как типы мы не открываем
                    if(!ObjID_A[ObjID_A.length - 1].match(/\|/)) {
                        let _ObjID = ObjID_A[ObjID_A.length - 1];
                        let uid_form = '*' + _ObjID;

                        let url_modal_to = params.match.params && params.match.params.NodeID != undefined ? params.match.params.NodeID : "";
                        //Проверка, чтобы форму уже не была открыта
                        if(this.state.Forms[uid_form] == undefined || (this.state.Forms[uid_form].url_modal_to != url_modal_to)) {
                            //let Node = this.state.treeData[0];
                            //Также, чтобы открыть данную форму нужно выбрать в дереве узел, чтобы из него получить данные
                            //В будущем, как появятся таблицы будет еще SelectNodeTable - активная таблица,
                            //Из которой будем брать данные для построения формы, но это будет в другом условии
                            if(this.state.SelectNode.ObjID == _ObjID) {
                                let url_modal = this.props.url_modal + '/' + this.state.SelectNode.id;
                                let url_modal_prev = this.props.url_modal + '/@' + this.state.SelectNode.id;
                                this.FormCore.GenerateObjectModal({
                                    url_modal_to: url_modal_to,
                                    url_modal: url_modal,
                                    url_modal_prev: url_modal_prev,
                                    typeModal: "edit",
                                    selectData: {
                                        ObjID: this.state.SelectNode.ObjID,
                                        TypeID: this.state.SelectNode.TypeID,
                                        ParrentObjID: this.state.SelectNode.ParrentObjID,
                                        ParrentTypeID: this.state.SelectNode.ParrentTypeID,
                                        Name: this.state.SelectNode.Name,
                                        isRoot: this.state.SelectNode.isRoot,
                                    },
                                });
                            }
                        }
                    }
                }
            }

            return null;
        };
        return (
            <BlockUi onClick={this.events.onClickCloth} className={"zt_window_cloth"} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                {content}
                <Router>
                    <SwitchRouter>
                        <Route path={[`${url_modal}:NodeID`, `${url_modal}`]} component={CheckModalUrlOpen}/>
                    </SwitchRouter>
                </Router>
            </BlockUi>);
    }
}