import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";

import { ImportPage } from '../ImportPage/ImportPage';

interface IImportModalProps {
    parent?: string,
    class?: string,
    show: boolean,
    onClose?: () => any,
}

export default class ImportModal extends React.Component<IImportModalProps, any> {
    constructor(props) {
        super(props);
        this.state = {
            entered: false,
        };
    }

	render() {
        let Body = null;
        if (this.state.entered) {
            Body = (
                <ImportPage class={this.props.class} parent={this.props.parent} />
            );
        }
        return (
            <Modal animation={true} size="xl" show={this.props.show}
                onEntered={
                    () => {
                        this.setState({ entered: true })
                    }
                }
                onHide={this.props.onClose}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Import file</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{ height: 'calc(100vh - 127px)'}}>{Body}</Modal.Body>
            </Modal>
        );
	}
}