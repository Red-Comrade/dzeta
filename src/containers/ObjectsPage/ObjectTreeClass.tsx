//Специальный класс с набором методов для дерева для наследования
import * as React from "react";
import * as ReactDOM from "react-dom";
import ToastrStore from "../../stores/ToastrStore";
import INodeTemplate from "./Interfaces/INodeTemplate";
import * as ct_api from "../../api/ClassTree-api";
import * as ot_api from "../../api/ObjTree-api";
// import ObjectModalCloth, {IObjectModalCloth} from "./ObjectModalCloth";
import IModalConfig from "../../components/CoolModalForm/IModalConfig";
import PermissionWindowCloth from "./PermissionWindowCloth";
import RootStore from "../../stores/RootStore";
import ObjectsStore from "../../stores/ObjectsStore";
import {action} from "mobx";

export enum EnumTypeModal {
    "permis",
    "object",
}
export interface IModalConfigState extends IModalConfig {
    url_modal?: string, //url чтобы открыть модалку
    url_modal_to?: string, //url самой модалки
    type: EnumTypeModal
}

export interface IStateObjectTree {
    treeData: INodeTemplate[],
    isLoadTree: boolean,
    buildTree: boolean, //по завершению загрузки дерева мы должны его построить, с помощью данного свойства мы иницилизируем построение дерева
    buildCleanTree: boolean,

    SelectNode?: INodeTemplate,
    expandedKeys?: string[],

    ShowFromLevelFunc?: (callback:(NodeID?)=>void)=> void,
    expandNodeFunc?: (callback:()=>void)=> string[],
    expandNodeTabFunc?: (callback?:()=>void) => {TabID: string,  expandedKeys: string[]}; //Работает и с обычными вкладками
    selectNodeFunc?: () => string,
    RedrafFunc?: () => void,
    UpdateGlobalTree?: () => void,

    AddBtnDisabled?: boolean;
    EditBtnDisabled?: boolean;
    DelBtnDisabled?: boolean;

    CutNode: INodeTemplate,
    CopyNode: INodeTemplate,


    Forms: {[id:string] : IModalConfigState }; //Массив наших форм


    HelperModalData: {
        show: boolean,
        TypeModal: "del" | "GenPassword" | "copy" | "inf",
        ObjID?: string,
        Name?: string,
        TypeID?: string,
        ToNode?: any,
    },
    ImportModalConfig: {
        show: boolean,
        class: string,
        parent: string,
    },
}

export default class ObjectTreeClass <p, s extends IStateObjectTree> extends React.Component<p, s> {
    protected isReadMode: boolean = false; //Если true, то модакли "по умолчанию" открываются в режиме чтения
    protected getInitialState() {
        return {
            Forms: {},
            treeData: [],
            isLoadTree: true,
            expandedKeys: [],

            HelperModalData: {
                show: false,
                TypeModal: "del",
                ObjID: null,
                Name: null,
                TypeID: null,
                ToNode: null
            },
            ImportModalConfig: {
                show: false,
                class: null,
                parent: null,
            },
        } as IStateObjectTree as s;
    }
    constructor(p:p) {
        super(p);
        this.state = this.getInitialState();
    }

    protected TreeCore = {
        AJAX: {
            getChildren: function (uid:string, SuccFunc, ErrFunc) {
                ct_api
                    .getChildren(uid)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data)
                        }
                    })
                    .catch(function (error) {
                        ErrFunc({result: 0});
                        console.log(error);
                    })
            },
            getChildrenByType: function (parent:string, type:string, SuccFunc, ErrFunc) {
                ot_api
                    .getChildrenByType(parent, type)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrFunc({result: 0});
                    })
            },

            changeLinkObj: (uidObj:string, uidOldObj:string, uidNewObj: string, SuccFunc: (res) => void, ErrFunc ) => {
                ot_api
                    .changeLinkObj(uidObj, uidOldObj, uidNewObj)
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch( (error) => {
                        ErrFunc({result: 0,errors:[{text:'Server ERROR'}]});
                    })
            },
        },

        //Построение основного дерева с нулевой точки
        GenerateTreeData: (params:{PathNodeID:string, module: "users" | "objects"},
                           SuccFunc: (config:{expandedKeys:string[], treeData: any[], SelectNode?}) => void) => {

            //this.setState({treeData: []});

            if(params.module == "users") {
                let uidType = "00000000-0000-0000-0000-000000000002";
                let rootNode = {
                    isRoot: true,
                    isLoaded: true,

                    ParentIDTree: "",
                    id: "|0",
                    key: "|0",


                    ParentID: "",
                    iddb: uidType,

                    Name: "Groups",
                    icon: "fas fa-sitemap",

                    children: [],

                    isObject: false,
                    isClass: true,
                    IsFolder: false,

                    TypeID: uidType,
                    ObjID: "00000000-0000-0000-0000-000000000000",
                    FolderID: "",

                    ParrentTypeID: "",
                    ParrentFolderID: "",
                    ParrentObjID: "",
                };
                this.TreeCore.GenerateRecOpen({Node: rootNode, PathNodeID: params.PathNodeID }, SuccFunc);
            }
            else {
                let uid = "00000000-0000-0000-0000-000000000000";
                let rootNode = {
                    isRoot: true,
                    isLoaded: true,

                    ParentIDTree: "",
                    id: "*0",
                    key: "*0",


                    ParentID: "",
                    iddb: uid,

                    Name: "Root",
                    icon: "fas fa-sitemap",

                    children: [],

                    isObject: true,
                    isClass: false,
                    IsFolder: false,

                    TypeID: uid,
                    ObjID: "00000000-0000-0000-0000-000000000000",
                    FolderID: "",

                    ParrentTypeID: "",
                    ParrentFolderID: "",
                    ParrentObjID: "",
                };

                this.TreeCore.GenerateRecOpen({Node: rootNode, PathNodeID: params.PathNodeID }, SuccFunc);
            }
        },

        //Открытие и выбор узла через сервер
        GenerateRecOpen: (
            params:{Node: INodeTemplate, PathNodeID?: string, PathParams?: {arr_url_path, index_url_path, node_id_path}},
            SuccFunc: (config:{expandedKeys:string[], treeData: any, SelectNode?}) => void, //treeData: any[] в будущем будет по ссылке
        ) => {
            let arr_url_path = [];
            let index_url_path = 0;
            let node_id_path = "";

            if(params.PathNodeID) {
                if(params.PathParams) {
                    //Если мы открываем с какой-то определённой ветки, то нам нужно знать параметры пути
                    arr_url_path = params.PathParams.arr_url_path;
                    index_url_path = params.PathParams.index_url_path;
                    node_id_path = params.PathParams.node_id_path;
                }
                else {
                    let Sep1 = "*";
                    let Sep2 = "|";
                    if(params.Node.isClass) {
                        Sep1 = "|";
                        Sep2 = "*";
                    }
                    else if(params.Node.isObject) {
                        Sep1 = "*";
                        Sep2 = "|";
                    }
                    let spl_node = params.PathNodeID.split(Sep1);
                    for( let i = 0; i < spl_node.length; i++ ) {
                        if(spl_node[i]) {
                            let spl2_node = spl_node[i].split(Sep2);
                            arr_url_path.push(Sep1 + spl2_node[0]);
                            if(spl2_node[1] != undefined) {
                                arr_url_path.push(Sep2 + spl2_node[1]);
                            }
                        }
                    }
                    if(arr_url_path[1] != undefined) {
                        index_url_path = 1;
                        node_id_path = arr_url_path[0] + arr_url_path[1];
                    }
                }
            }


            //На вход передаём узел дерева, на выходе получаем SuccFunc с детьми этого нода
            const OpenTreeRec = (params:{Node: any, expandedKeys: string[], arr_url_path, index_url_path, node_id_path, SuccFunc }) => {
                let data_tree = [];
                const SuccFuncIns = () => {
                    params.Node.isLoaded = true;
                    //Будет вызвана по успешному завершению AJAX-запроса
                    params.expandedKeys.push(params.Node.id);
                    params.Node.children = data_tree;
                    this.TreeHelper.fillAttrFuncRec(params.Node.children, params.Node);
                    params.Node.children = (params.Node.children.length > 0 ? params.Node.children : null);

                    if(params.node_id_path) {
                        //Открытие веток по url

                        if(params.Node.children && params.Node.children.length > 0) {
                            for (let i = 0; i < params.Node.children.length; i++ ) {
                                let itemNode = params.Node.children[i];
                                if(itemNode.id == params.node_id_path) {
                                    if(params.arr_url_path[params.index_url_path + 1] == undefined) {
                                        //Достигли нашего узла
                                        params.SuccFunc({
                                            expandedKeys: params.expandedKeys,
                                            treeData: params.Node,
                                            SelectNode: itemNode,
                                        });
                                    }
                                    else {
                                        params.expandedKeys.push(params.node_id_path);
                                        let n_param = {
                                            Node: itemNode,
                                            expandedKeys: params.expandedKeys,
                                            arr_url_path: params.arr_url_path,
                                            index_url_path: params.index_url_path + 1,
                                            node_id_path: params.node_id_path + params.arr_url_path[params.index_url_path + 1],
                                            SuccFunc: (config:{expandedKeys:string[], treeData: INodeTemplate, SelectNode?}) => {
                                                //params.Node у нас меняется по ссылке
                                                params.SuccFunc({
                                                    expandedKeys: config.expandedKeys,
                                                    treeData: params.Node,
                                                    SelectNode: config.SelectNode,
                                                });
                                            }
                                        };
                                        OpenTreeRec(n_param);
                                    }
                                    return;
                                }
                            }
                            console.log('err')
                            ToastrStore.error('Could not find the node, it may have been deleted(online)');
                            params.SuccFunc({
                                expandedKeys: params.expandedKeys,
                                treeData: params.Node,
                                SelectNode: null,
                            });
                        }
                        else {
                            console.log('err')
                            ToastrStore.error('Could not find the node, it may have been deleted!(online)');
                            params.SuccFunc({
                                expandedKeys: params.expandedKeys,
                                treeData: params.Node,
                                SelectNode: null,
                            });
                        }
                    }
                    else {
                        params.SuccFunc({
                            expandedKeys: params.expandedKeys,
                            treeData: params.Node,
                            SelectNode: null,
                        });
                    }
                };

                if(params.Node.isObject) {
                    this.TreeCore.AJAX.getChildren(params.Node.TypeID,(data) => {
                        //Отсекаем системные объекты
                        for (let i = 0; i < data.data.length; i++) {
                            if(data.data[i].uid != "00000000-0000-0000-0000-000000000002") {
                                data_tree.push(data.data[i]);
                            }
                        }
                        SuccFuncIns();
                    }, (data) => {
                        debugger
                    });
                }
                else if(params.Node.isClass) {
                    this.TreeCore.AJAX.getChildrenByType(params.Node.ObjID, params.Node.TypeID,(data) => {
                        data_tree = data.data;
                        SuccFuncIns();
                    }, (data) => {
                        ToastrStore.error("error", "code="+data.result);
                        debugger
                    });

                }
            };

            OpenTreeRec({
                Node: params.Node,
                expandedKeys: [],
                arr_url_path: arr_url_path,
                index_url_path: index_url_path,
                node_id_path: node_id_path,
                SuccFunc: SuccFunc
            });
        },
        //Открытие и выбор узла оффлайн, если не нашли оффлайн, то обращаемся к GenerateRecOpen
        OpenTreeNode: (
            params:{Node: any, PathNodeID:string, isSelect: boolean},
            SuccFunc: (config:{expandedKeys:string[], SelectNode?}) => void,
            ErrFunc: () => void
        ) => {
            let arr_url_path = [];
            let index_url_path = 0;
            let node_id_path = "";

            let expandedKeys = [];

            if(params.PathNodeID) {
                let Sep1 = "*";
                let Sep2 = "|";
                if(params.Node.isClass) {
                    Sep1 = "|";
                    Sep2 = "*";
                }
                else if(params.Node.isObject) {
                    Sep1 = "*";
                    Sep2 = "|";
                }
                let spl_node = params.PathNodeID.split(Sep1);
                for( let i = 0; i < spl_node.length; i++ ) {
                    if(spl_node[i]) {
                        let spl2_node = spl_node[i].split(Sep2);
                        arr_url_path.push(Sep1 + spl2_node[0]);
                        if(spl2_node[1] != undefined) {
                            arr_url_path.push(Sep2 + spl2_node[1]);
                        }
                    }
                }
                if(arr_url_path[1] != undefined) {
                    expandedKeys.push(arr_url_path[0]);
                    index_url_path = 1;
                    node_id_path = arr_url_path[0] + arr_url_path[1];
                }


                let CutNodeID = params.Node.id;
                let str_id = "";
                let tmp_last_id = 0;
                for( let i = 0; i < arr_url_path.length; i++ ) {
                    str_id += arr_url_path[i];
                    if( CutNodeID == str_id ) {
                        tmp_last_id = i;
                        break;
                    }
                }

                if(arr_url_path[tmp_last_id + 1] != undefined) {
                    expandedKeys.push(arr_url_path[tmp_last_id]);
                    index_url_path = tmp_last_id + 1;
                    node_id_path = str_id + arr_url_path[tmp_last_id + 1];
                }

                //Тут мы отсекаем наш узел,
            }

            let lastIndex = index_url_path;
            let last_node_id_path = index_url_path;
            let lastNode = params.Node;

            let SelectNode = null;
            let isNeedLoad = false; //if true, then need request

            const FindNodeRec = (params:{Node: any,  arr_url_path, index_url_path, node_id_path }) => {

                if(params.Node.children && params.Node.children.length > 0) {
                    for (let i = 0; i < params.Node.children.length; i++ ) {
                        let itemNode = params.Node.children[i];
                        if(itemNode.id == params.node_id_path) {
                            if(params.arr_url_path[params.index_url_path + 1] == undefined) {
                                //Достигли нашего узла
                                SelectNode = itemNode;
                                console.log('offline node');
                                return true;
                            }
                            else {
                                expandedKeys.push(params.node_id_path);
                                let n_param = {
                                    Node: itemNode,
                                    arr_url_path: params.arr_url_path,
                                    index_url_path: params.index_url_path + 1,
                                    node_id_path: params.node_id_path + params.arr_url_path[params.index_url_path + 1],
                                };
                                return FindNodeRec(n_param);
                            }
                        }
                    }
                    console.log('err')
                    ToastrStore.error('Could not find the node, it may have been deleted');
                    return false;
                }
                else if(params.Node.children == null) {
                    console.log('err');
                    ToastrStore.error('Could not find the node, it may have been deleted!');
                    return false;
                }
                else {
                    lastNode = params.Node;
                    lastIndex = params.index_url_path;
                    last_node_id_path = params.node_id_path;
                    isNeedLoad = true;
                    //need load
                    return true;
                }
            };
            let isEx = FindNodeRec({
                Node: params.Node,
                arr_url_path: arr_url_path,
                index_url_path: index_url_path,
                node_id_path: node_id_path,
            });

            if(isEx && !isNeedLoad) {
                SuccFunc({expandedKeys:expandedKeys, SelectNode:SelectNode});
            }
            else if(isEx){
                this.TreeCore.GenerateRecOpen({
                    Node: lastNode,
                    PathNodeID: params.PathNodeID,
                    PathParams: {
                        arr_url_path: arr_url_path,
                        index_url_path: lastIndex,
                        node_id_path: last_node_id_path,
                    }
                }, (config:{expandedKeys:string[], treeData: INodeTemplate, SelectNode?}) => {

                    if(this.state.expandedKeys) {
                        for(let i = 0; i < config.expandedKeys.length; i++) {
                            if( expandedKeys.indexOf(config.expandedKeys[i]) === -1 ) {
                                expandedKeys.push(config.expandedKeys[i]);
                            }
                        }
                    }

                   // let expandedKeys1 = expandedKeys.concat(config.expandedKeys);
                    SuccFunc({expandedKeys:expandedKeys, SelectNode: config.SelectNode})
                });
                //loaded ajax
            }
            else {
                ErrFunc();
            }
        },


        onLoadData: (selectData, callbackResolve) => {
            if(selectData.children !== null && selectData.children.length == 0) {
                if(selectData.isClass) {
                    //Если открываемый узел является классом
                    this.setState({isLoadTree: true});
                    this.TreeCore.AJAX.getChildrenByType(selectData.ObjID, selectData.uid, (data) => {
                        let _children;
                        selectData.isLoaded = true;

                        if(data.data.length > 0) {
                            selectData.children = [];
                            this.TreeHelper.fillAttrFuncRec(data.data, selectData);
                            _children = data.data;

                            //selectData.children = data.data;
                        }
                        else {
                            _children = null;
                            //selectData.children = null;
                        }

                        this.TreeCore.AddChildrenInNode({iddb: selectData.iddb}, _children);
                        callbackResolve();
                    }, () => {
                        callbackResolve();
                    })
                }
                else if(selectData.isObject) {
                    //Если открываемый узел является объектом//пока без папок
                    this.setState({isLoadTree: true});
                    this.TreeCore.AJAX.getChildren(selectData.TypeID, (data) => {
                        let _children;
                        selectData.isLoaded = true;
                        if(data.data.length > 0) {
                            selectData.children = [];
                            this.TreeHelper.fillAttrFuncRec(data.data, selectData);
                            _children = data.data;

                            //selectData.children = data.data;
                        }
                        else {
                            _children = null;
                            //selectData.children = null;
                        }
                        this.TreeCore.AddChildrenInNode({iddb: selectData.iddb}, _children);
                        callbackResolve();
                    }, (data) => {
                        ToastrStore.error('Error', "errorCode="+data.result);
                        callbackResolve();
                    })

                }
                else {
                    //Тут возможо будет папка
                }
            }
            else {
                callbackResolve();

                this.setState({buildTree: true});
            }
        },
        onExpandNode: (expandedKeys) => {
            this.setState({
                buildTree: true,
                expandedKeys: expandedKeys,
            })
        },



        AddNode: (PanentNode: INodeTemplate, ChildNode: INodeTemplate) => {
            if(PanentNode.isLoaded) {
                const loopFindAndCreateNode = (children) => {
                    children.forEach((item) => {
                        if(item.children) {
                            loopFindAndCreateNode(item.children);
                        }
                        if(item.iddb == PanentNode.iddb) {
                            if(!Array.isArray( item.children )) {
                                item.children = [ChildNode]
                            }
                            else {
                                //Тут можно добавить с учетом сортировки
                                item.children.push(ChildNode);
                                this.TreeHelper.h_sort(item.children, "Name", 1);
                            }
                        }
                    });
                };
                loopFindAndCreateNode(this.state.treeData);

                this.setState({buildTree: true});
            }
        },
        AddChildrenInNode: (payload:{iddb:string}, _children) => {
            let newTreeData = [];
            newTreeData = this.state.treeData;
            const loopFindAndCreateNode = (children) => {
                children.forEach((item) => {
                    if(item.iddb == payload.iddb) {
                        item.children = _children;
                        item.isLoaded = true;
                    }
                    else if(item.children) {
                        loopFindAndCreateNode(item.children);
                    }
                });
            };
            loopFindAndCreateNode(newTreeData);

            this.setState({treeData: newTreeData, isLoadTree: false, buildTree: true, UpdateGlobalTree: () => {this.setState({UpdateGlobalTree: null})}  });
        },

        DeleteNode: (payload: {iddb: string, }) => {
            let newTreeData = [];
            const loopFindAndCreateNode = (children) => {
                let NewChildren = [];
                let isRemove = false;
                children.forEach((item) => {
                    if(item.children) {
                        item.children = loopFindAndCreateNode(item.children);
                    }
                    if(item.iddb != payload.iddb) {
                        NewChildren.push(item);
                    }
                    else {
                        isRemove = true;
                    }
                });
                return NewChildren.length > 0 || !isRemove ? NewChildren : null;
            };
            newTreeData = loopFindAndCreateNode(this.state.treeData);
            this.setState({treeData: newTreeData, isLoadTree: false, buildTree: true, UpdateGlobalTree: () => {this.setState({UpdateGlobalTree: null})}  });
        },
        CutNodeAction: (payload: {Node: any, ToNode: any}) => {
            let ParentID = payload.Node.ParentID;
            let iddb = payload.Node.iddb;
            let newTreeData = this.state.treeData;

            let Node = payload.Node;


            //Генерируем дерево без нашего узла ребёнка
            const loopFindAndCutNode = (children, ParentID_tmp) => {
                let NewChildren = [];
                children.forEach((item) => {
                    if(item.children) {
                        item.children = loopFindAndCutNode(item.children, item.iddb);
                    }
                    if(ParentID_tmp != ParentID || item.iddb != iddb) {
                        NewChildren.push(item);
                    }
                });
                return NewChildren;
            };
            newTreeData = loopFindAndCutNode(this.state.treeData, null);


            //Переделываем ID node под наши ID
            payload.Node.ParentIDTree = payload.ToNode.id;
            payload.Node.id = payload.ToNode.id + '*' + payload.Node.ObjID;
            payload.Node.FolderID = payload.ToNode.FolderID;
            //Param.TypeID = parrentNode.TypeID;
            payload.Node.ParentID = payload.ToNode.iddb;
            //Param.ObjID = Param.ObjID;
            payload.Node.ParrentTypeID = payload.ToNode.TypeID;
            payload.Node.ParrentFolderID = payload.ToNode.ParrentFolderID;
            payload.Node.ParrentObjID = payload.ToNode.ObjID;


            const loopFindAndPasteNode = (children, ParentID_tmp) => {
                let NewChildren = [];
                children.forEach((item) => {
                    if(item.children) {
                        item.children = loopFindAndPasteNode(item.children, item.iddb);
                    }
                    NewChildren.push(item);

                    if(item.id == payload.ToNode.id) {
                        if(item.children !== null) {
                            item.children.unshift(payload.Node);
                        }
                        else {
                            item.children = [payload.Node];
                        }
                    }
                });
                return NewChildren;
            };
            newTreeData = loopFindAndPasteNode(this.state.treeData, null);


            this.setState({treeData: newTreeData, isLoadTree: false, buildTree: true, UpdateGlobalTree: () => {this.setState({UpdateGlobalTree: null})}  });
        },
        UpdNameNode: (id_node, newName) => {
            let newTreeData = this.state.treeData;
            const loopFindAndCreateNode = (children) => {
                children.forEach((item) => {
                    if(item.id == id_node) {
                        item.Name = newName;
                        return true;
                    }
                    else if(item.children) {
                        let isEx = loopFindAndCreateNode(item.children);
                        if(isEx) {
                            return true;
                        }
                    }
                });
                return false;
            };
            loopFindAndCreateNode(newTreeData);

            this.setState({treeData: newTreeData, isLoadTree: false, buildTree: true, UpdateGlobalTree: () => {this.setState({UpdateGlobalTree: null})}  });
        },
    };

    protected TreeHelper = {
        fillAttrFuncRec: (children, parrentNode: INodeTemplate) => {
            for( let key in children) {
                let Param = children[key];

                if(parrentNode.isClass) {
                    //Так как в классе могут быть только объекты
                    Param.isObject = true;
                }
                else if(parrentNode.isObject) {
                    //Могут быть только попки, или типы, но мы поставим пока типы
                    Param.isClass = true;
                }


                Param.Name = Param.name;


                if(Param.isObject) {
                    //То родитель является типом и мы проходимся по объектам
                    Param.ParentIDTree = parrentNode.id;
                    Param.id = parrentNode.id + '*' + Param.uid;

                    Param.ParentID = parrentNode.iddb;
                    Param.iddb = Param.uid;

                    Param.ObjID = Param.uid;
                    Param.TypeID = parrentNode.TypeID;
                    Param.FolderID = parrentNode.FolderID;

                    Param.ParrentTypeID = parrentNode.ParrentTypeID;
                    Param.ParrentFolderID = parrentNode.ParrentFolderID;
                    Param.ParrentObjID = parrentNode.ObjID;

                    Param.children = [];
                    Param.isLoaded = false;
                    Param.loaded = false;

                    Param.icon = 'fas fa-cube';
                }
                else if(Param.isClass) {
                    //То родитель является объектом, или папкой
                    Param.ParentIDTree = parrentNode.id;
                    Param.id = parrentNode.id + '|' + Param.uid;

                    Param.ParentID = parrentNode.iddb;
                    Param.iddb = parrentNode.ObjID + '|' + Param.uid;

                    Param.ObjID = parrentNode.ObjID;
                    Param.TypeID = Param.uid;
                    Param.FolderID = parrentNode.FolderID;

                    Param.ParrentTypeID = parrentNode.TypeID;
                    Param.ParrentFolderID = parrentNode.ParrentFolderID;
                    Param.ParrentObjID = parrentNode.ParrentObjID;

                    Param.icon = 'fas fa-file-invoice';

                    Param.children = [];
                    Param.isLoaded = false;
                    Param.loaded = false;
                }

                Param.key = Param.id;
            }
        },

        //Для сохранения массива по значению
        extend_rec: (obj1, obj2) => {
            //obj1 наследует свойства obj2, если такие есть
            const extend_rec = function (obj1, obj2) {
                Object.keys(obj1).forEach( (key) => {
                    if( obj2 && typeof obj2 === "object" && obj2[key] !== undefined ) {
                        if( obj1 && typeof obj1[key] === "object" && obj1[key] && Object.keys(obj1[key]).length > 0 ) {
                            //Значит можно еще глубже
                            extend_rec(obj1[key], obj2[key]);
                        }
                        else {
                            obj1[key] = obj2[key];
                        }
                    }
                });
            };
            extend_rec(obj1, obj2);
        },

        h_sort: (data, key, dir) => {
            if(dir === 1) {
                data.sort(function(a, b) {
                    if (a[key] > b[key]) {
                        return 1; }
                    if (a[key] < b[key]) {
                        return -1; }
                    return 0;
                });
            }
            else {
                data.sort(function(a, b) {
                    if (a[key] < b[key]) {
                        return 1; }
                    if (a[key] > b[key]) {
                        return -1; }
                    return 0;
                });
            }
            return data;
        }
    }
}