import * as React from "react";
import { inject, observer } from "mobx-react";

import Loader from "react-loaders";
import BlockUi from "react-block-ui";

import EnematLoader from '../../components/svg/EnematLoader'

import RootStore from "../../stores/RootStore";
import * as ct_apiview from "../../api/View-api";
import {Icon} from '../UserStepPage/Components/Icon'
import DeleteModal from "./DeleteModal";
import NUploadDocumentModal from "./NUploadDocumentModal";
import archive from "../UserStepPage/Icons/archive";
import {createRef} from "react";
import ToastrStore from "../../stores/ToastrStore";
import nt_check from "../UserStepPage/Icons/nt_check";
import {string} from "prop-types";



interface ITreeTablePageState {
    isFullLoad: boolean, //флаг, что всё загружено
    data: any[],
    dataKeys: {[id:string]: any},
    dataOpen: {[key:string]:any},


    isAjaxDel: boolean,
    isReloadBlocking: boolean,


    DeleteModal: {
        name: string,
        obj: string,
        show: boolean,
    },

    UploadFileModal: {
        show: boolean,
        data: any,
        dkey: string,
    },

    hoverTr?: {
        ref_tr: HTMLDivElement,
        ref_co_hover: HTMLDivElement,
        tr_item: any,
        tr_index: number,
    },
    many_mode: 0 | 1 | 2 //0 - uncheck, 1 - intermediate, 2 - check all,
    many_mode_selected: {[uid:string] : any},
    many_mode_unselected: {[uid:string] : any},
    many_mode_opt?: {
        lot_name?: string,
        lot_name_cache?: string,
        is_edit?: boolean,
    }
}
interface ITreeTablePageProps{
    ToastrStore?:any,
    NameModule: "treetable" | "archivetreetable",
    config: any,

    isCompletedFilterTable: boolean,
    isAdminViewTable: boolean,
    searchText: string,
    date_parsesr: any,
    file_parsesr: any,

    CheckSiret: (SuccFunc:()=>void, ErrFunc:()=>void) => void,
    onEdit: (data: {}, table_state: any, full_data:any) => void,
    onSign: (data: {}) => void,
    onUpload: (data: {}, doc_view: any) => void,
    onCreateDevis: (table_state: any) => void,
    onCreateAHFull: (table_state: any) => void,
    onCreateFacture: (data: {}, table_state: any) => void,
    onCreateAH: (data: {}, table_state: any) => void,

    onSearch: (e:any) => void,

    table_state: any,


    dataOpen?: {[key:string]:any},
    scrollPositionY?: number,
    countData?: number
}

export default class NTreeTable extends React.Component<ITreeTablePageProps,ITreeTablePageState> {
    state: Readonly<ITreeTablePageState> = {
        isAjaxDel: false,
        isFullLoad: false,
        data: [],
        dataKeys: {},
        dataOpen: {},

        isReloadBlocking: false,

        DeleteModal: {
            name: "",
            show: false,
            obj: "",
        },
        UploadFileModal: {
            show: false,
            dkey: "",
            data: {}
        },
        many_mode: 0,
        many_mode_selected: {},
        many_mode_unselected: {},
        many_mode_opt: {}
    };
    ID_Table:string = "cool_t-" + Math.floor(Math.random() * 1000000); //Внутренний ID, нужен для правильной стилизации
    _ref_gc_tbody: HTMLElement;
    _ref_trs_content: HTMLElement;
    _ref_gc_thead: HTMLElement;
    _ref_tbody_scroller: HTMLElement;
    _ref_tbody_scroller_div: HTMLElement;
    _ref_footer_cont: HTMLElement;
    DelayRecalcTimer: any;
    settings = {
        length_load: 20, //количество строк при выгрузке
        region_load: 2000, //за сколько пикселей до конца начинаем подгрузку данных

        isLoadAjaxToken: true,
        isNeedRecalc: false, //Обновление стилей компонента без задействия смены состояния реакта
        isNeedRecalcHoverTR: false,
        isNeedRecalcFooter: false,
        xhr: null,


        scrollPositionY: 0,
        countData: 0,

        is_disable_scroll: false,
        footer_space_heightPX: 74, //90

        lastCheckedIndex: null
    };

    isAdmin: boolean = false;


    constructor(props:any) {
        super(props);
    }



    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.settings.isNeedRecalcFooter) {
            this.settings.isNeedRecalcFooter = false;
            //Если появился футер
            //Иницилизируем его высоту
            this.core.InitHeightFooter();
        }

        if(this.settings.isNeedRecalc) {
            this.settings.isNeedRecalc = false;
            this.core.RecalcStyle();

            this._ref_gc_tbody.scrollTop = this.settings.scrollPositionY;
        }

        if(this.props.searchText != prevProps.searchText
            || this.props.isCompletedFilterTable != prevProps.isCompletedFilterTable
            || this.props.NameModule != prevProps.NameModule
            || this.props.isAdminViewTable != prevProps.isAdminViewTable
        ) {
            this.settings.countData = this.settings.length_load;
            this.settings.scrollPositionY = 0;
            this.core.Reload();
        }

        if(this.state.hoverTr && (this.state.hoverTr?.tr_index != prevState.hoverTr?.tr_index || this.settings.isNeedRecalcHoverTR)) {
            this.settings.isNeedRecalcHoverTR = false;
            this.core.RecalcHoverTR();
        }
    }


    componentDidMount(): void {

        //Делаем обход на проверку, являемся ли мы админом
        for(let index_group in RootStore.MSettings.UserGroups) {
            let item_group = RootStore.MSettings.UserGroups[index_group];
            let uid_group = item_group.IDo;

            if(this.props.config.adminGroups[uid_group] != undefined) {
                this.isAdmin = true;
                break;
            }
        }

        console.log(this.props.NameModule);
        //При первой иницилизации мы загружаем первичные данные
        window.addEventListener('resize', this.events.onResizeDoc , false);

        let dataOpen = {};
        let scrollPositionY = 0;
        let countData = 0;


        if(this.props.dataOpen) {
            dataOpen = this.props.dataOpen;
        }
        if(this.props.scrollPositionY) {
            scrollPositionY = this.props.scrollPositionY;
        }
        if(this.props.countData) {
            countData = this.props.countData
        }

        this.core.GoAjax({
            dataOpen: dataOpen,
            scrollPositionY: scrollPositionY,
            countData: countData,
        });
    }

    componentWillUnmount(): void {
        if(this.settings.xhr) {
            if(this.settings.xhr.cancel) {
                this.settings.xhr.cancel();
            }
        }
        let style_el = document.getElementById('cool_t_style-'+this.ID_Table);
        if(style_el) {
            style_el.remove();
        }

        window.removeEventListener('resize', this.events.onResizeDoc , false);
    }


    events = {
        onScrollBody: (e) => {
            this._ref_tbody_scroller.scrollTop = this._ref_gc_tbody.scrollTop;

            this._ref_gc_thead.scrollLeft = this._ref_gc_tbody.scrollLeft;
            //Иницилизируем событие, когда почти прокрутили до конца
            this.core.RecalcHoverTR();
            this.settings.scrollPositionY = this._ref_gc_tbody.scrollTop;
            let scroll_pog = this._ref_gc_tbody.scrollHeight - this._ref_gc_tbody.scrollTop - this._ref_gc_tbody.clientHeight;
            if(scroll_pog <= this.settings.region_load && !this.state.isFullLoad && this.settings.isLoadAjaxToken) {
                this.core.GoAjax({});
            }
        },

        onScrollScroller: (e) => {
            if(!this.settings.is_disable_scroll) {
                this._ref_gc_tbody.scrollTop = this._ref_tbody_scroller.scrollTop;
            }
        },

        onResizeDoc: () => {
            this.core.DelayRecalc( () =>{
                this.core.InitHeightFooter();
                this.core.RecalcStyle();
            }, )
        },

        toggle_open_tr: (tr_index, e) => {
            if(e.target.classList.contains('nt_toggle_open_tr')) {
                this.settings.isNeedRecalcHoverTR = true;
                if(this.state.dataOpen[tr_index] != undefined) {
                    delete this.state.dataOpen[tr_index];
                    this.setState({});
                }
                else {
                    let data_open = {};
                    data_open[tr_index] = true;
                    this.setState({dataOpen: data_open });
                }
            }
            e.preventDefault();
            e.stopPropagation();
        },

        onRemoveTr: (tr_item, tr_index) => {
            console.log(tr_item);
            this.state.DeleteModal.name = tr_item.name;
            this.state.DeleteModal.obj = tr_item.key;
            this.state.DeleteModal.show = true;
            this.setState({});
        },

        handleRemove: (obj_del,  action : "delete" | "archive" | "unarchive") => {
            this.setState({isAjaxDel: true});
            this.core.AJAX.removeODTEnemat(obj_del, action, (answer) => {
                this.setState({isAjaxDel: false});
                this.core.removeTR(obj_del);
            }, () => {
                this.setState({isAjaxDel: false});
            });
        },

        onCheckHandle: (e, tr_index) => {
            const data_tr = this.state.data[tr_index];
            let checked = this.state.many_mode_selected[data_tr.key] == undefined;
            const CheckFunc = (tr_index, checked:boolean) => {
                const data_tr = this.state.data[tr_index];

                if(checked) {
                    this.state.many_mode_selected[data_tr.key] = data_tr;
                    delete this.state.many_mode_unselected[data_tr.key];
                }
                else {
                    if(this.state.dataKeys[data_tr.key] != undefined) {
                        this.state.many_mode_unselected[data_tr.key] = data_tr;
                    }
                    delete this.state.many_mode_selected[data_tr.key];
                }
            };
            let hasSelectedPrev = Object.keys(this.state.many_mode_selected).length > 0;
            if(e.shiftKey && this.settings.lastCheckedIndex!==null && this.settings.lastCheckedIndex != tr_index) {
                if(tr_index > this.settings.lastCheckedIndex) {
                    for(let i = this.settings.lastCheckedIndex; i <= tr_index; i++) {
                        CheckFunc(i, checked);
                    }
                }
                else {
                    for(let i = this.settings.lastCheckedIndex; i >= tr_index; i--) {
                        CheckFunc(i, checked);
                    }
                }
            }
            else {
                CheckFunc(tr_index, checked);
            }
            this.settings.lastCheckedIndex = tr_index;
            let hasSelected = Object.keys(this.state.many_mode_selected).length > 0;
            if(!hasSelectedPrev && hasSelected) {
                this.settings.isNeedRecalcFooter = true;
            }
            this.setState({});
        },
        onCheckAllHandle: (e) => {
            let icon_check = this.helper.statusCkeckAll();
            if(icon_check == 'nt_uncheck') {
                //ничего не выбрано
                //то значит мы выбираем всё
                let many_mode_unselected = {};

                if(this.state.isFullLoad) {
                    let hasSelectedPrev = Object.keys(this.state.many_mode_selected).length > 0;
                    //Значит все данные прогружены и мы просто выбираем
                    this.state.data.forEach((item_tr) => {
                       this.state.many_mode_selected[item_tr.key] = item_tr;
                    });
                    let hasSelected = Object.keys(this.state.many_mode_selected).length > 0;
                    if(!hasSelectedPrev && hasSelected) {
                        this.settings.isNeedRecalcFooter = true;
                    }
                    this.setState({many_mode_unselected: many_mode_unselected});
                }
                else {
                    //Значит данные не прогружены и мы снимаем выбор
                    this.core.GoAjax({countData:99999, isCheckAll: true});
                }
            }
            else {
                let many_mode_unselected = {};
                Object.keys(this.state.many_mode_selected).forEach((key_item) => {
                   let item = this.state.many_mode_selected[key_item];
                    many_mode_unselected[key_item] = item;
                });
                //значит мы снимаем всё
                this.setState({
                    many_mode_selected: {},
                });
            }
        },
        onCloseCheckMode: () => {
            let many_mode_unselected = {};
            Object.keys(this.state.many_mode_selected).forEach((key_item) => {
                let item = this.state.many_mode_selected[key_item];
                many_mode_unselected[key_item] = item;
            });
            //значит мы снимаем всё
            this.setState({
                many_mode_selected: {},
            });
        },

        onClickShowCheckedList: () => {
            let textSelection = "#Revoir la sélection#";
            if(this.props.searchText == textSelection) {
                textSelection = "";
            }
            this.props.onSearch({target: {value:textSelection}});
        },
    };

    core = {
        RecalcStyle: () => {
            let width_scroll = this._ref_gc_tbody.offsetWidth - this._ref_gc_tbody.clientWidth;

            let styleID = 'nt_t_style-'+this.ID_Table;
            let css = '',
                style = document.getElementById(styleID);


            if(!style) {
                let
                    head = document.head || document.getElementsByTagName('head')[0];
                style = document.createElement('style');
                head.appendChild(style);

                style.setAttribute('type', 'text/css');
                style.setAttribute('id', styleID);
            }

            //Задаём пространство для скрола для шапки
            css += '#' + this.ID_Table + ' .nt_thead { padding-right: '+width_scroll+'px; } ';
            //Задаём скроллер
            css += '#' + this.ID_Table + ' .nt_tbody_scroller_div { height: ' + this._ref_trs_content.offsetHeight + 'px; } ' ;
            css += '#' + this.ID_Table + ' .nt_tbody_scroller { height: calc(100% - ' + this.settings.footer_space_heightPX + 'px); } ' ;


            style.innerHTML = css;
        },
        RecalcHoverTR: () => {
            if(this.state.hoverTr?.ref_co_hover) {
                let sizes_target = this.state.hoverTr.ref_tr.getBoundingClientRect();
                this.state.hoverTr.ref_co_hover.style.top = sizes_target.y + sizes_target.height/2 - 42 + 'px';
                this.state.hoverTr.ref_co_hover.style.left = sizes_target.x + sizes_target.width - 18 + 'px';
            }
        },
        InitHeightFooter: () => {
            console.log("recalcFooter")
            if(this._ref_footer_cont) {
                this.settings.footer_space_heightPX = this._ref_footer_cont.offsetHeight - 12;
            }
        },

        AJAX: {
            getTableData: (params:{start, num, searchText}, SuccFunc, ErrFunc) => {
                this.settings.xhr = {cancel: null};

                let view_uid;
                if(this.isAdmin && this.props.isAdminViewTable) {
                    view_uid = this.props.NameModule == "treetable" ? this.props.config.rootView.uid_admin : this.props.config.rootView.uid_archive_admin;
                }
                else {
                    view_uid = this.props.NameModule == "treetable" ? this.props.config.rootView.uid : this.props.config.rootView.uid_archive;
                }

                let attr_filter = [
                    {op:'~', v: params.searchText, attr: ""}
                ];

                if(this.props.isCompletedFilterTable) {
                    attr_filter.push( {
                        op:'~',
                        v: "1",
                        attr: this.props.config.rootView.assoc_data.Completed
                    });
                }

                ct_apiview.getTableDataEnemat({
                    view: view_uid,
                    attr_sort: '',
                    sort: 0,
                    start: (params.start+1),
                    num: params.num,
                    NumOfObjOfMainClass: params.num,
                    attr_filter: attr_filter,
                }, this.settings.xhr).then((res)=>{
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc();
                    }
                }).catch( (res) => {

                });
            },
            removeODTEnemat: (odt, action:"delete" | "archive" | "unarchive", SuccFunc, ErrFunc) => {
                ct_apiview
                    .removeODTEnemat(odt, action)
                    .then((res) => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ToastrStore.error('Server error!', 'ErrorCode=' + res.data.result);
                            ErrFunc();
                        }
                    })
                    .catch( (res) => {
                        ToastrStore.error('Server error!', 'Error');
                        ErrFunc();
                    });
            },
        },

        Reload: () => {
            if(this.settings.xhr) {
                if(this.settings.xhr.cancel) {
                    this.settings.xhr.cancel();
                }
            }
            this.settings.isLoadAjaxToken = true;
            let countData = this.settings.countData;
            let scrollPositionY = this.settings.scrollPositionY;
            this.core.GoAjax({isReload: true, countData: countData, scrollPositionY: scrollPositionY});
        },

        //Подгрузка данных
        GoAjax: (param:{isReload?, dataOpen?, countData?, scrollPositionY?, isCheckAll?}) => {
            let params = {
                start: this.state.data.length,
                num: this.settings.length_load,
                searchText: this.props.searchText,
            };

            if(param.isReload) {
                params.start = 0;
            }
            if(param.countData) {
                params.num = param.countData;
            }
            if(param.dataOpen) {
                for(let key_i in param.dataOpen) {
                    this.state.dataOpen[key_i] = param.dataOpen[key_i];
                }
            }
            if(param.scrollPositionY) {
                this.settings.scrollPositionY = param.scrollPositionY;
            }

            this.settings.isLoadAjaxToken = false;
            this.setState({isReloadBlocking: param.isReload == true || param.isCheckAll});
            const SuccFunc = (answer) => {
                let data = [];
                let dataKeys = {};
                if(!param.isReload) {
                    data = this.state.data;
                }
                let many_mode_selected = this.state.many_mode_selected;
                let hasSelectedPrev = Object.keys(many_mode_selected).length > 0;
                let many_mode_unselected = {};

                let data_format = answer.data_format;
                let isFullLoad = false;

                data_format.forEach( (item) => {
                    let child_nodes = this.helper.convertData(item.data_child, 'row', item);

                    let full_data = item.data;
                    for (let index = 0; index < child_nodes.length; index++) {
                        full_data[child_nodes[index].type] = child_nodes[index];
                    }
                    full_data.data_child = item.data_child;
                    full_data.key = item.key;
                    data.push(full_data);


                });

                data.forEach((item_data) => {
                    dataKeys[item_data.key] = true;
                    if(many_mode_selected[item_data.key] == undefined && !param.isCheckAll) {
                        many_mode_unselected[item_data.key] = item_data;
                    }
                });


                if(data_format.length < params.num) {
                    isFullLoad = true;
                }

                this.settings.countData = data.length;
                this.settings.isLoadAjaxToken = true;
                this.settings.isNeedRecalc = true;

                if(param.isReload) {
                    //  many_mode_selected = {}; //убираем сброс выбранного при обновлении
                }
                if(param.isCheckAll) {
                    //То мы проходимся по всей дате и выбираем всё
                    data.forEach((item_tr) => {
                        this.state.many_mode_selected[item_tr.key] = item_tr;
                    });
                }

                let hasSelected = Object.keys(many_mode_selected).length > 0;
                if(!hasSelectedPrev && hasSelected) {
                    this.settings.isNeedRecalcFooter = true;
                }

                this.setState({
                    data: data,
                    isFullLoad: isFullLoad,
                    isReloadBlocking: false,
                    many_mode_selected: many_mode_selected,
                    many_mode_unselected: many_mode_unselected,
                    dataKeys: dataKeys,
                });
            };


            if(this.props.searchText == '#Revoir la sélection#') {
                let data = [];
                Object.keys(this.state.many_mode_selected).forEach((item_key) => {
                   data.push(this.state.many_mode_selected[item_key]);
                });
                this.settings.isLoadAjaxToken = true;
                this.settings.isNeedRecalc = true;

                this.setState({
                    data: data,
                    isReloadBlocking: false,
                    isFullLoad: true
                });

            }
            else {
                this.core.AJAX.getTableData(params, SuccFunc, () => {
                    this.settings.isLoadAjaxToken = true;
                    this.events.onScrollBody(null);
                    this.settings.isNeedRecalc = true;
                });
            }

        },


        DelayRecalc: (Func) => {
            clearTimeout(this.DelayRecalcTimer);
            this.DelayRecalcTimer = setTimeout(Func, 200); // Will do the ajax stuff after 1000 ms, or 1 s
        },



        removeTR: (obj_del) => {
            let new_data = [];

            this.state.data.forEach((item) => {
                if(item.key != obj_del) {
                    new_data.push(item);
                }
            });

            this.settings.countData = new_data.length;
            this.settings.isNeedRecalc = true;

            this.setState({data: new_data});
        },
    };


    helper = {
        convertData: (data, lvl, parentNode) => {
            let data_converted = [];
            let getDateHelper = (Data) => {
                let DateIn = Data.split(' ')[0];
                let DateArr = DateIn.split('/');
                return new Date(DateArr[2], (Number(DateArr[1]) - 1), DateArr[0]);
            };
            let getValue = (attr) => {
                if (attr.datatype == 'datetime' && attr.value) {
                    let tmp_date = getDateHelper(attr.value.value);
                    return tmp_date.toISOString();
                }
                if (attr.datatype == 'file') {
                    if (attr.value) {
                        attr.value.p_uid = attr.uid;
                    }
                    return attr.value;
                }
                if (attr.LinkType) {

                    if(attr?.value?.name) {
                        return attr.value.name;
                    }
                    else {
                        return "";
                    }
                }
                if (attr.value) {
                    return attr.value.value;
                }
                return '';
            };
            if (lvl == 0) {
                let assoc_data = this.props.config.rootView.assoc_data;
                data.forEach((element) => {
                    let key = '';
                    let obj_data = {
                        key: null,
                        data: {},
                        leaf: false,
                        lvl: 1,
                    };
                    Object.keys(assoc_data).map((column) => {
                        let uid = assoc_data[column];
                        if (uid && element[uid] && element[uid].v) {
                            obj_data.data[column] = element[uid].v;
                            if (!key) {
                                key = element[uid].o;
                            }
                        } else {
                            obj_data.data[column] = '';
                        }
                    });
                    obj_data.key = key;

                    data_converted.push(obj_data);
                });
            }
            if (lvl == 1) {
                data.forEach((element) => {
                    let obj_data = {
                        key: element.uid,
                        parent_key: parentNode.key,
                        data: {
                            name: element.type.name,
                        },
                        type: this.props.config.docView[element.type.uid].type,
                        leaf: false,
                        lvl: 2,
                        isSigned: false,
                        hasStatus: false,
                        class: element.type.uid
                    };
                    let assoc_data = this.props.config.docView[element.type.uid].assoc_data;
                    Object.keys(assoc_data).map((column) => {
                        let uid = assoc_data[column];
                        let attr = element.attributes.find((item) => {
                            return item.uid == uid ? true : false;
                        })
                        if (attr) {
                            let value = getValue(attr);
                            if (column == 'date_signature' && value) {
                                obj_data.isSigned = true;
                            }
                            if (column == 'status' && value) {
                                obj_data.hasStatus = true;
                            }
                            obj_data.data[column] = value;
                        } else {
                            obj_data.data[column] = '';
                        }
                    });
                    data_converted.push(obj_data);
                });
                let sort_index = ['devis', 'facture', 'AH'];
                data_converted.sort((itemA, itemB) => {
                    return sort_index.indexOf(itemA.type) - sort_index.indexOf(itemB.type);
                });
                let devis = data_converted.find((item) => {
                    return item.type == 'devis' ? true : false
                });
                parentNode.data.status = {create_facture: devis.key};
                let facture = data_converted.find((item) => {
                    return item.type == 'facture' ? true : false
                });
                if (facture) {
                    parentNode.data.status = {create_ah: facture.key};
                }
                let ah = data_converted.find((item) => {
                    return item.type == 'AH' ? true : false
                });
                if (ah) {
                    parentNode.data.status = 'Terminé';
                }

            }
            if (lvl == 2) {
                let assoc_data = this.props.config.operView.assoc_data;
                data.forEach((element) => {
                    let obj_data = {
                        key: element.uid,
                        data: {},
                        leaf: true,
                        lvl: 3,
                    };
                    Object.keys(assoc_data).map((column) => {
                        let uid = assoc_data[column];
                        let attr = element.attributes.find((item) => {
                            return item.uid == uid ? true : false;
                        })
                        if (attr) {
                            obj_data.data[column] = getValue(attr);
                        } else {
                            obj_data.data[column] = '';
                        }
                    });
                    data_converted.push(obj_data);
                });
            }
            if (lvl == 'row') {
                data.forEach((element) => {
                    let obj_data = {
                        key: element.uid,
                        parent_key: parentNode.key,
                        data: {},
                        type: this.props.config.docView[element.type.uid].type,
                        isSigned: false,
                        hasStatus: false,
                        class: element.type.uid,

                        data_child: element.data_child ? element.data_child : []
                    };
                    let assoc_data = this.props.config.docView[element.type.uid].assoc_data;

                    let signed_file; //наличие подписанного файла
                    let date_signature; //Наличие записи о дате подписания

                    Object.keys(assoc_data).map((column) => {
                        let uid = assoc_data[column];
                        let attr = element.attributes.find((item) => {
                            return item.uid == uid ? true : false;
                        });
                        if (attr) {
                            let value = getValue(attr);
                            if (column == 'date_signature' && value) {
                                date_signature = value;
                            }
                            if (column == 'signed_file' && value) {
                                signed_file = value;
                            }
                            if (column == 'status' && value) {
                                obj_data.hasStatus = true;
                            }
                            obj_data.data[column] = value;
                        }
                        else {
                            obj_data.data[column] = '';
                        }
                    });
                    if(signed_file && date_signature) {
                        obj_data.isSigned = true;
                    }
                    data_converted.push(obj_data);
                });
            }

            return data_converted;
        },

        childParser: (key, data, column, tr_index) => {
            let table_state = this.props.table_state;
            table_state.scrollPositionY = this.settings.scrollPositionY;
            table_state.dataOpen = this.state.dataOpen;
            table_state.countData = this.settings.countData;

            const lang_conf = {
                Completed: "Terminé",
                Processing: "En traitement",
                Generate: "Generate"
            };
            const btns_conf = {
                Completed: (onClick) => {
                    let is_sign_class = data[key].isSigned ? "nt_btn_signed" : "";

                    return <button onClick={onClick} className={'nt_btn nt_btn_completed '+is_sign_class+' nt_toggle_open_tr'}><i className={"fas fa-check nt_toggle_open_tr"}/> {lang_conf.Completed}</button>
                },
                Processing: (onClick) => {
                    return <button onClick={onClick} className={'nt_btn nt_btn_processing nt_toggle_open_tr'}><i className={"far fa-clock"}/> {lang_conf.Processing}</button>
                },
                Generate: (onClick) => {
                    return <button onClick={onClick} className={'nt_btn nt_btn_generate'}><i className={"fa fa-plus"}/> {lang_conf.Generate}</button>
                },
            };
            //fas fa-pencil-alt
            //far fa-edit
            //'fas fa-pencil'
            let isOwner = true;
            let OwnerName = "";
            if(this.isAdmin && this.props.isAdminViewTable) {
                OwnerName = data.Owner?.name ? data.Owner.name : "";
                if(OwnerName != RootStore.MSettings.UserName) {
                    isOwner = false;
                }
            }

            if(key == "pre_visite" || key == "devis" || key == "facture" || key == "AH") {
                let btn_status;
                let label_date;
                let btns_editors = [];

                //Если есть объект
                if(data[key]) {
                    //То мы выводим кнопку со статусом объекта
                    if(data[key].hasStatus) {
                        btn_status = btns_conf.Processing(() => {});
                    }
                    else {
                        btn_status = btns_conf.Completed(() => {});
                    }
                    label_date = <div className={'nt_co_date_label nt_toggle_open_tr'}> <button className={"btn nt_toggle_open_tr"}> {this.props.date_parsesr(data[key], {field: 'date_create'}) } </button> </div>;

                    if(this.state.dataOpen[tr_index]!=undefined) {

                        let btnName = data[key].isSigned || !isOwner ? 'Vue' : 'Éditer';
                        if (data[key].type == 'facture' && isOwner) {
                            btnName = 'Éditer';
                        }

                        btns_editors.push(
                            <div className={"nt_btn_o_cont"} key={"edit"}>
                                <button onClick={() => {
                                    let isAllowEdit = isOwner;

                                    data[key].isAllowEdit = isAllowEdit;
                                    this.props.onEdit(data[key], table_state, data);
                                }} className={'btn nt_btn_ed'}><Icon name="edit" color="#0069A5" size={18}/>{" " + btnName}</button>
                            </div>
                        );

                        let file_div;
                        let file_signed_div;

                        if (data[key]['data']['file']) {
                            let file = data[key]['data']['file'];
                            file_div = <div className="file_cont nt_btn_o_cont">
                                <button title={'Download ' + file.name} onClick={() => {
                                    window.open('/Object/downloadFile?obj=' + data[key].key + '&attr=' + file.p_uid, '_blank');
                                }} className={'btn nt_btn_ed'}><Icon name="download" color="#0069A5" size={24}/> Télécharger</button>
                            </div>;
                        }


                        //подписанные файлы начинаем показывать
                        if (data[key]['data']['signed_file']) {
                            let file = data[key]['data']['signed_file'];
                            file_signed_div = <div className="file_cont nt_btn_o_cont">
                                <button title={'Download ' + file.name} onClick={() => {
                                    window.open('/Object/downloadFile?obj=' + data[key].key + '&attr=' + file.p_uid, '_blank');
                                }} className={'btn nt_btn_ed'}><Icon name="sign" color="#0069A5" size={24}/>{" Document signé"}</button>
                            </div>;
                        }


                        let file_facture_excel_div;
                        //Сохранение ексель файла с таблицей для фактуры
                        if(key == "facture" && data[key]['data']['excel_file']) {
                            let file = data[key]['data']['excel_file'];
                            file_facture_excel_div = <div className="file_cont nt_btn_o_cont">
                                <button title={'Download ' + file.name} onClick={() => {
                                    window.open('/Object/downloadFile?obj=' + data[key].key + '&attr=' + file.p_uid, '_blank');
                                }} className={'btn nt_btn_ed'}><i style={{fontSize:"26px"}} className={"far fa-file-excel custom_icon"} /> {" Télécharger"}</button>
                            </div>;
                        }



                        if(!data[key].isSigned && !data[key].hasStatus && isOwner) {
                            //Разрешить подписывание документа для ЮРИДИЧЕСКОГО лица только если* Organization=ESSO*

                            let is_allow_sign = data['devis']?.data?.organisation?.name == "Esso" || data["persone"] == "Personne physique";

                            if (key == 'devis' && data['devis'] && is_allow_sign) {
                                    btns_editors.push(
                                        <React.Fragment key={"sign"}>
                                            <div className={"nt_btn_o_cont"}>
                                                <button onClick={() => {
                                                    //this.props.onUpload(data['devis'], this.props.config.docView)
                                                    this.props.CheckSiret(
                                                        () => {
                                                            this.state.UploadFileModal.show = true;
                                                            this.state.UploadFileModal.data = data;
                                                            this.state.UploadFileModal.dkey = key;
                                                            this.setState({});
                                                        },
                                                        () => {}
                                                    );

                                                }} className={'btn nt_btn_ed'}><Icon name="sign" color="#0069A5" size={24}/> Signer</button>
                                            </div>
                                            {file_div}
                                            {file_signed_div}
                                        </React.Fragment>
                                    )

                            }
                            else if(key == 'AH' && data['AH'] && is_allow_sign) {
                                btns_editors.push(
                                    <React.Fragment key={"sign"}>
                                        <div className={"nt_btn_o_cont"}>
                                            <button onClick={() => {
                                                //this.props.onUpload(data['AH'], this.props.config.docView)
                                                this.props.CheckSiret(
                                                    () => {
                                                        this.state.UploadFileModal.show = true;
                                                        this.state.UploadFileModal.data = data;
                                                        this.state.UploadFileModal.dkey = key;
                                                        this.setState({});
                                                    },
                                                    () => {}
                                                );
                                            }} className={'btn nt_btn_ed'}><Icon name="sign" color="#0069A5" size={24}/> Signer</button>
                                        </div>
                                        {file_div}
                                        {file_signed_div}
                                        {file_facture_excel_div}
                                    </React.Fragment>
                                );
                            }
                            else {
                                btns_editors.push(
                                    <React.Fragment key={"sign"}>
                                        {file_div}
                                        {file_signed_div}
                                        {file_facture_excel_div}
                                    </React.Fragment>
                                )
                            }
                        }
                        else {
                            //{file_signed_div}
                            btns_editors.push(
                                <React.Fragment key={"sign"}>
                                    {file_div}
                                    {file_signed_div}
                                    {file_facture_excel_div}
                                </React.Fragment>
                            )
                        }
                    }
                }
                else if(isOwner){
                    if (key == 'devis') {
                        btn_status = btns_conf.Generate(() => {this.props.onCreateDevis(data['devis']); });
                    }
                    if (key == 'facture' && data['devis']) {
                        btn_status = btns_conf.Generate(() => {this.props.onCreateFacture(data['devis'], table_state); });
                    }
                    if (key == 'AH' && data['facture']) {
                        btn_status = btns_conf.Generate(() => {this.props.onCreateAH(data['facture'], table_state); });
                    }
                }

                return <div className={"nt_co_inf_1 nt_toggle_open_tr"}>
                    <div className={'nt_co_btn'}> {btn_status} </div>
                    {label_date}
                    {btns_editors}
                </div>;
            }
            else if(key == "main_td") {
                let btns_operations = [];
                let adress_arr = [];


                if(data["devis"]) {
                    if(Object.keys(data["devis"]["operations"]).length > 0) {
                        Object.keys(data["devis"]["operations"]).forEach((item_key_op, index_btn) => {
                            let item = data["devis"]["operations"][item_key_op];
                            if(this.state.dataOpen[tr_index]!=undefined || btns_operations.length  <=3) {
                                let name = item?.data?.name.name ? item.data.name.name : "";
                                if(!name.trim()) {
                                    name = "#empty#";
                                }
                                else {
                                    btns_operations.push(<button key={index_btn} className={"nt_btn_inf nt_toggle_open_tr"}>{name}</button>)
                                }
                            }
                        });

                        if(data["devis"]["operations"].length > 3 && !this.state.dataOpen[tr_index]) {
                            btns_operations.push(<button key={"+1"} className={"nt_btn_inf nt_toggle_open_tr"}>+{(data["devis"]["operations"].length - 3)}</button>)
                        }
                    }

                    if(data.devis.data?.adress_0) {
                        adress_arr.push(data.devis.data.adress_0);
                    }
                    if(data.devis.data?.adress_1) {
                        adress_arr.push(data.devis.data.adress_1);
                    }
                    if(data.devis.data?.adress_2) {
                        adress_arr.push(data.devis.data.adress_2);
                    }
                }
                let adress = adress_arr.join(', ');


                let operation_name = "Classique";
                if (data.TableAflag == 1) {
                    operation_name = "Grand Précaire";
                }
                else if (data.TableBflag == 1) {
                    operation_name = "Précaire";
                }

                if(data["persone"] == "Personne morale" ) {
                    operation_name = data.TypeOperation;
                }


                let operation_type = <button className={"nt_btn_operation nt_toggle_open_tr"}>{operation_name}</button>;

                let OwnerNameEl;
                if(this.isAdmin && this.props.isAdminViewTable) {
                    OwnerNameEl =  <div className={"nt_inf_owner nt_toggle_open_tr " + (isOwner ? "nt_owner_my" : "")}>
                        {OwnerName}
                    </div>;
                }


                /*
                * <button className={'nt_btn_ch nt_toggle_open_tr'}>
                            <i className={"nt_toggle_open_tr " + (this.state.dataOpen[tr_index]!=undefined?"fas fa-chevron-down nt_ch_open":"fas fa-chevron-right nt_ch_open")}  />
                        </button>
                *
                * */


//nt_cst_chev

                let icon_check = "nt_uncheck";
                if(this.state.many_mode_selected[data.key] != undefined) {
                    icon_check = "nt_check";
                }

                return <div className={'nt_co_0 nt_toggle_open_tr'}>
                    <div className={"nt_co_chev nt_toggle_open_tr"}>
                        <button className={'nt_cst_ckeckbox'}
                                onClick={(e)=>{this.events.onCheckHandle(e, tr_index)}}
                        ><Icon name={icon_check} color="#0069A5" size={26}/></button>
                        <div className={'nt_cst_chev nt_toggle_open_tr ' + (this.state.dataOpen[tr_index]!=undefined?"nt_ch_open":"")}/>
                    </div>
                    <div className={"nt_co_inf nt_toggle_open_tr"}>
                        {OwnerNameEl}
                        <div className={'nt_inf_0 nt_toggle_open_tr'}><span className={"nt_toggle_open_tr"}>{data.name}</span>{operation_type}</div>
                        <div className={'nt_inf_1 nt_toggle_open_tr'}><span className={"nt_toggle_open_tr"}>{"Adresse des travaux: " + adress}</span></div>
                        <div className={'nt_inf_2 nt_toggle_open_tr'}>{btns_operations}</div>
                    </div>
                </div>;
            }

            return '';
        },

        statusCkeckAll: () => {
            let has_check = Object.keys(this.state.many_mode_selected).length > 0;
            let is_check_all = Object.keys(this.state.many_mode_unselected).length == 0;

            let icon_check = "nt_uncheck";

            if(!has_check) {
                icon_check = "nt_uncheck";
            }
            else if(is_check_all && has_check) {
                icon_check = "nt_check";
            }
            else {
                icon_check = "nt_intermediate";
            }

            return icon_check;
        }
    };




    render() {
        let trs = [];

        //region generate trs

        let data;
        data = this.state.data;





        data.forEach((tr_item, tr_index) => {
            let ref_tr:HTMLDivElement;
            let ref_co_hover:HTMLDivElement;

            let ref_nt_btn_remove_tr:HTMLDivElement;

            const onClickRemoveBtn = (target) => {
                if(target.classList.contains('nt_open_del_pop')) {
                    target.classList.remove('nt_open_del_pop')
                }
                else {
                    target.classList.add('nt_open_del_pop')
                }
            };



            let co_hover = <div className={"nt_co_hover"} ref={(ref) => {ref_co_hover = ref}}>
                <div className={"nt_btn_archive_tr"} onClick={()=>{this.events.handleRemove(tr_item.key, (tr_item.Deleted==2?"unarchive":"archive"));}}>
                    <div className={"nt_div_inf_t"}>{(tr_item.Deleted==2?"Désarchiver":"Archiver")}</div>
                    {(tr_item.Deleted==2?<Icon name="unarchive" color="#0069A5" size={16}/>:<Icon name="archive" color="#0069A5" size={16}/>)}
                </div>
                <div ref={(ref) => {ref_nt_btn_remove_tr = ref;}} className={"nt_btn_remove_tr"} onClick={(e) => {
                    onClickRemoveBtn(e.currentTarget);
                }}>
                    <div className={"nt_div_inf_t"}>Supprimer</div>
                    <div className={"nt_div_inf_t_del"}>
                        <div className={"nt_div_inf_t_del_c1"}>Cette action est inévitable. Etes-vous sûr que vous voulez supprimer?</div>
                        <div className={"nt_div_inf_t_del_c2"}>
                            <button className={"nt_div_inf_t_del_b1"} onClick={()=>{this.events.handleRemove(tr_item.key, 'delete');}}>Oui</button>
                            <button className={"nt_div_inf_t_del_b2"} onClick={(e)=>{
                                e.stopPropagation();
                                onClickRemoveBtn(ref_nt_btn_remove_tr);
                            }}>Non</button>
                        </div>
                    </div>
                    <Icon name="trash" color="#FF2E00" size={16}/>
                </div>
            </div>;

            let is_hover_tr_class = "";
            if(this.state.hoverTr?.tr_index != undefined && this.state.hoverTr?.tr_index == tr_index) {
                is_hover_tr_class = " nt_tr_hover";
            }

            let icon_check = "nt_uncheck";
            let isCheck = false;
            if(this.state.many_mode_selected[tr_item.key] != undefined) {
                icon_check = "nt_check";
                isCheck = true;
            }

            trs.push(
                <React.Fragment key={tr_index}>
                    <div className={"nt_tr_mobile_header" + (isCheck?" nt_tr_active_check":"")}>
                        <div className={"nt_tr_mh_co0"}>
                            <button className={'nt_cst_ckeckbox'}
                                    onClick={(e)=>{this.events.onCheckHandle(e, tr_index)}}
                            ><Icon name={icon_check} color="#0069A5" size={26}/></button>
                        </div>
                        <div className={"nt_tr_mh_co1"}>
                            <button className={"nt_btn_mobile"} onClick={()=>{this.events.handleRemove(tr_item.key, (tr_item.Deleted==2?"unarchive":"archive"));}} >
                                {(tr_item.Deleted==2?<Icon name="unarchive" color="#0069A5" size={16}/>:<Icon name="archive" color="#0069A5" size={16}/>)}
                            </button>
                            <span>{(tr_item.Deleted==2?"Désarchiver":"Archiver")}</span>
                        </div>
                        <div className={"nt_tr_mh_co2"}>
                            <button className={"nt_btn_mobile"} onClick={() => {
                                this.events.onRemoveTr(tr_item, tr_index);
                            }} >
                                <Icon name="trash" color="#FF2E00" size={16} />
                            </button>
                            <span>Supprimer</span>
                        </div>
                    </div>
                    <div ref={(ref)=>{ref_tr=ref;}} className={'nt_tr nt_toggle_open_tr ' + (isCheck?" nt_tr_active_check ":"") + (this.state.dataOpen[tr_index]!=undefined?" nt_tr_open":"") + is_hover_tr_class}  onClick={(e) => {this.events.toggle_open_tr(tr_index, e)}}
                         onMouseEnter={(e) => {
                            // console.log('enter');
                             this.setState({
                                 hoverTr: {
                                     ref_tr: ref_tr,
                                     tr_item: tr_item,
                                     tr_index: tr_index,
                                     ref_co_hover: ref_co_hover,
                                 }
                             });
                         }}
                         onMouseLeave={(e) => {
                            // console.log('end');
                             this.setState({
                                 hoverTr: null
                             });
                         }}
                    >
                        <div className={'nt_td nt_td_0 nt_toggle_open_tr'} >
                            <div className={'nt_td_content nt_toggle_open_tr'}>
                                {this.helper.childParser('main_td', tr_item, {field: 'main_td'}, tr_index)}
                            </div>
                        </div>
                        <div className={'nt_td nt_td_2 nt_toggle_open_tr'}>
                            <div className={'nt_td_content nt_toggle_open_tr'}>
                                {this.helper.childParser('devis', tr_item, {field: 'devis_column'}, tr_index)}
                            </div>
                        </div>
                        <div className={'nt_td nt_td_3 nt_toggle_open_tr'}>
                            <div className={'nt_td_content nt_toggle_open_tr'}>
                                {this.helper.childParser('facture', tr_item, {field: 'facture_column'}, tr_index)}
                            </div>
                        </div>
                        <div className={'nt_td nt_td_4 nt_toggle_open_tr'}>
                            <div className={'nt_td_content nt_toggle_open_tr'}>
                                {this.helper.childParser('AH', tr_item, {field: 'AH_column'}, tr_index)}
                            </div>
                        </div>
                        <div className={'nt_co_mobile_chev nt_toggle_open_tr'}>
                            <button className={'nt_btn_ch nt_toggle_open_tr'}>
                                <i className={"nt_toggle_open_tr " + (this.state.dataOpen[tr_index]?"fas fa-chevron-up nt_ch_open":"fas fa-chevron-down nt_ch_open")}  />
                            </button>
                        </div>
                        {co_hover}
                    </div>
                </React.Fragment>
            )
        });
        //endregion


        let footer_space;
        let fixed_footer;
        let isShowCustomScroll;
        if(Object.keys(this.state.many_mode_selected).length > 0) {
            isShowCustomScroll = true;
            footer_space = <div style={{height:this.settings.footer_space_heightPX+"px"}}/>;


            let s2_edit_content;
            if(this.state.many_mode_opt.is_edit) {
                let value_text_cache = this.state.many_mode_opt?.lot_name_cache ? this.state.many_mode_opt.lot_name_cache : "";
                s2_edit_content = (
                    <React.Fragment>
                        <div className={"nt_form_group"}>
                            <input autoFocus={true} type={"text"} className={"nt_input"} value={value_text_cache} onChange={(e) => { this.state.many_mode_opt.lot_name_cache = e.target.value; this.setState({}) }} />
                            <label className={"nt_label"}>
                                <span className={"nt_span_text"}>Nom du lot</span>
                                <span className={"nt_label_ph"}/>
                            </label>
                        </div>
                        <button className={"nt_footer_cont_btn"} onClick={() => {
                            this.state.many_mode_opt.lot_name = this.state.many_mode_opt.lot_name_cache;
                            this.state.many_mode_opt.is_edit = false;
                            this.setState({})

                        }}><i className="fas fa-check"/><span className={"hide-el-mobile"}> Renommer</span></button>
                        <button className={"nt_footer_pure_btn btn"} onClick={() => {
                            this.state.many_mode_opt.is_edit = false;
                            this.setState({})
                        }}>Annuler</button>
                    </React.Fragment>
                );
            }
            else {
                s2_edit_content = (
                    <React.Fragment>
                        <div className={"nt_form_group"}>
                            <span className={'nt_span_lot_name'}>{(this.state.many_mode_opt.lot_name?this.state.many_mode_opt.lot_name:"Lot Enemat #1")}</span>
                            <label className={"nt_label nt_label_read"}>
                                <span className={"nt_span_text_read"}>Nom du lot</span>
                                <span className={"nt_label_ph"}/>
                            </label>
                        </div>
                        <button className={"nt_footer_pure_btn btn"} onClick={() => {
                            this.state.many_mode_opt.lot_name_cache = this.state.many_mode_opt.lot_name;
                            this.state.many_mode_opt.is_edit = true;
                            this.setState({})
                        }}>
                            <Icon name="edit" color="#0069A5" size={18}/><span className={""}> Éditer</span>
                        </button>
                    </React.Fragment>
                );
            }

            fixed_footer = <div className="nt_footer_cont" ref={(_ref)=>{this._ref_footer_cont = _ref}}>
                <div className="nt_footer_cont_content">
                    <div className="nt_footer_cont_s1">
                        {Object.keys(this.state.many_mode_selected).length} sélectionnés
                    </div>
                    <div className="nt_footer_cont_s2">
                        {s2_edit_content}
                    </div>
                    <div className="nt_footer_cont_s3">
                        <button className={"nt_footer_pure_btn btn"} onClick={this.events.onClickShowCheckedList} >Revoir la sélection</button>
                        <button className="nt_footer_cont_btn nt_footer_generate_lot_btn">Générer Lot</button>
                    </div>
                </div>
            </div>;
        }




        let isBlocking = false;
        if(this.state.isReloadBlocking || this.state.isAjaxDel) {
            isBlocking = true;
        }

        let icon_check = this.helper.statusCkeckAll();

        return (
            <BlockUi id={this.ID_Table} className={"nt_table_co"} tag="div" loader={<EnematLoader />} blocking={isBlocking}>
                <div className={"nt_wrapper_table"}>
                    <div className={"hide-el-desktop nt_mobile_thead"} >
                        <button onClick={this.events.onCheckAllHandle} className={'nt_cst_ckeckbox'}><Icon name={icon_check} color="#0069A5" size={26}/></button>
                        <span className={"nt_cst_span"}>Tout sélectionner</span>
                    </div>
                    <div className={'nt_thead'} ref={(ref)=>{this._ref_gc_thead = ref}} >
                        <div className={'nt_tr_thead'}>
                            <div className={'nt_th nt_th_0'}>
                                <button onClick={this.events.onCheckAllHandle} className={'nt_cst_ckeckbox'}><Icon name={icon_check} color="#0069A5" size={26}/></button>
                                <span className={"nt_cst_span"}>Tout sélectionner</span>
                            </div>
                            <div className={'nt_th nt_th_2'}>Devis</div>
                            <div className={'nt_th nt_th_3'}>Facture</div>
                            <div className={'nt_th nt_th_4'}>AH</div>
                        </div>
                    </div>
                    <div className={"nt_wrapper_tbody"}>
                        <div className={'nt_tbody '+(Object.keys(this.state.dataOpen).length>0?" nt_is_open_tr":"") +(isShowCustomScroll?' hide-scrollbar':'')} ref={(ref)=>{this._ref_gc_tbody = ref}}
                             onScroll={this.events.onScrollBody}
                             onTouchStart={() => {
                                 //Чтобы не дублировались события скролов
                                 this.settings.is_disable_scroll = true;
                             }}
                             onTouchEnd={() => {this.settings.is_disable_scroll = false;}}
                             onMouseEnter={() => {this.settings.is_disable_scroll = true;}}
                             onMouseLeave={() => {this.settings.is_disable_scroll = false;}}
                        >
                            <div className={"nt_trs_content"} ref={(ref)=>{this._ref_trs_content = ref}}>
                                {trs}
                                <div className={"nt_loading_div " + (this.settings.isLoadAjaxToken ? "hide-el" : "")}>Chargement en cours...</div>
                            </div>
                            {footer_space}


                        </div>
                        <div className={'nt_tbody_scroller ' +(!isShowCustomScroll?'hide-el':"")} ref={(ref)=>{this._ref_tbody_scroller = ref}}  onScroll={this.events.onScrollScroller}>
                            <div className={"nt_tbody_scroller_div"} ref={(ref)=>{this._ref_tbody_scroller_div = ref}} />
                        </div>
                    </div>
                </div>
                {fixed_footer}
                <DeleteModal
                    {
                        ...{ ...{
                                CloseCallback: () => {
                                    this.state.DeleteModal.show = false;
                                    this.setState({})
                                },
                                SuccessFunction: () => {
                                    let obj_del = this.state.DeleteModal.obj;
                                    this.state.DeleteModal.obj = "";
                                    this.state.DeleteModal.name = "";
                                    this.state.DeleteModal.show = false;
                                    this.core.removeTR(obj_del);
                                },
                            },
                            ...this.state.DeleteModal}
                    }
                />
                <NUploadDocumentModal {
                    ...{ ...{
                            CloseCallback: () => {
                                this.state.UploadFileModal.show = false;
                                this.setState({})
                            },
                            Reload: () => {
                                this.core.Reload();
                            },
                            config: this.props.config,
                        },
                        ...this.state.UploadFileModal}}/>
            </BlockUi>
        )
    }
}
