import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/ClassTree-api";
import {th} from "date-fns/locale";

import TreeSelect, { TreeNode, SHOW_PARENT } from 'rc-tree-select';
import 'rc-tree-select/assets/index.css';
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import * as ot_api from "../../api/ObjTree-api";

import Select from 'react-select';
import * as ct_apiview from "../../api/View-api";
import {IValueFile} from "../../api/ObjTree-api";
import RootStore from "../../stores/RootStore";
import {Icon} from "../UserStepPage/Components/Icon";
import * as ct_apistep from "../../api/StepForm-api";
import upload_file from "../UserStepPage/Icons/upload_file";
import DatePicker from "react-datepicker";
import {format, parse} from "date-fns";
import ToastrStore from "../../stores/ToastrStore";


interface IBModalProps {
    show:boolean;

    CloseCallback?: () => void,
    Reload?: () => void,
    SuccessFunction?: () => void,


    config: any,
    data: any,
    dkey: string,

    ToastrStore?: any;
}
interface IBModalState {
    drag_file_class: boolean,
    drag_file_class2: boolean,

    FileCache: any,
    DateCache: any,
    FileCache2: any, //файл для штампа

    ProgressData: {
        completed: number, //для загрузки пдф
        completed2: number, //для загрузки штампов
    },

    isLoadingSign: boolean,
    isLoadingSaveObj: boolean,
    isLoadingFile: boolean,
    isLoadingFile2: boolean

}

@inject("ToastrStore")
@observer
export default class NUploadDocumentModal extends React.Component<IBModalProps,IBModalState> {
    state: Readonly<IBModalState> = {
        FileCache: null,
        FileCache2: null,
        DateCache: "",
        drag_file_class: false,
        drag_file_class2: false,

        ProgressData: {
            completed: 0,
            completed2: 0
        },
        isLoadingSign: false,
        isLoadingSaveObj: false,
        isLoadingFile: false,
        isLoadingFile2: false,

    };
    _ref_inp_type: HTMLElement;
    src_image: any;
    src_image2: any;

    constructor(props) {
        super(props);
    }

    events = {
        handleClose: () => {
            this.props.CloseCallback();
        },



        sfile: {
            handleFiles: (files) => {
                console.log(files);

                if(files.length > 0) {
                    if(this.helper.validateFilePDF(files[0])) {
                        /*
                        let reader = new FileReader();
                        reader.onload = (e) => {
                            this.src_image = e.target.result;
                            this.setState({})
                        };
                        reader.readAsDataURL(files[0]); // convert to base64 string
                        */
                        this.setState({FileCache: files[0]});
                    }
                }
                else {
                    //this.setState({FileCache: null});
                }
            },

            dragOver: (e) => {
                this.setState({drag_file_class: true});
                e.preventDefault();
            },
            dragEnter: (e) => {
                this.setState({drag_file_class: true});
                e.preventDefault();
            },

            dragLeave: (e) => {
                this.setState({drag_file_class: false});
                e.preventDefault();
            },
            fileDrop: (e) => {
                this.setState({drag_file_class: false});
                e.preventDefault();
                console.log(e);
                const files = e.dataTransfer.files;
                if (files.length) {
                    this.events.sfile.handleFiles(files);
                }
            },
        },


        stamp: {
            handleFiles: (files) => {
                console.log(files);

                if(files.length > 0) {
                    if(this.helper.validateFileSTAMP(files[0])) {
                        let reader = new FileReader();
                        reader.onload = (e) => {
                            this.src_image2 = e.target.result;
                            this.setState({})
                        };
                        reader.readAsDataURL(files[0]); // convert to base64 string
                        this.setState({FileCache2: files[0]});
                    }
                }
                else {
                    //this.setState({FileCache: null});
                }
            },

            dragOver: (e) => {
                this.setState({drag_file_class2: true});
                e.preventDefault();
            },
            dragEnter: (e) => {
                this.setState({drag_file_class2: true});
                e.preventDefault();
            },

            dragLeave: (e) => {
                this.setState({drag_file_class2: false});
                e.preventDefault();
            },
            fileDrop: (e) => {
                this.setState({drag_file_class2: false});
                e.preventDefault();
                console.log(e);
                const files = e.dataTransfer.files;
                if (files.length) {
                    this.events.stamp.handleFiles(files);
                }
            },
        },


        //подпись документа
        onSignPhis: () => {
            this.setState({isLoadingSign: true});
            this.core.AJAX.signDoc(this.props.data[this.props.dkey].key, this.props.data[this.props.dkey].type, () => {
                this.setState({isLoadingSign: false});
                this.props.Reload();
                this.props.CloseCallback();
            }, () => {
                this.setState({isLoadingSign: false});
            });
        },
        //сохранение штампа и подпись документа
        onSaveSignStamp: () => {
            this.setState({isLoadingFile2: true});
            let file = this.state.FileCache2;
            this.core.AJAX.signDocStamp(this.props.data[this.props.dkey].key, this.props.data[this.props.dkey].type, file, () => {
                this.setState({isLoadingFile2: false});
                this.props.Reload();
                this.props.CloseCallback();
            }, () => {
                this.setState({isLoadingFile2: false});
            });
        },


        //Сохранение подписанного документа
        onSaveSignedFile: () => {
            let param_data = {
                obj: this.props.data[this.props.dkey].key,
                type: this.props.data[this.props.dkey].class,
                obj_parent: this.props.data[this.props.dkey].parent_key,
            };

            let values = [
                {
                    attr: this.props.config.docView[param_data.type].assoc_data.date_signature, //uid-параметра
                    key: this.props.data[this.props.dkey].type == 'facture' ? 1 : 0,
                    value: this.state.DateCache,
                    datatype: "datetime",
                }
            ];

            this.setState({isLoadingSaveObj: true});
            this.core.AJAX.saveObject(param_data.obj, param_data.type, param_data.obj_parent, values, (data) => {
                ToastrStore.success('Success!', 'Success!');
                this.setState({
                    isLoadingSaveObj: false,
                    isLoadingFile: true,
                });

                let Files = [
                    this.state.FileCache
                ];
                let values:ot_api.IValueFile[] = [
                    {
                        uid: this.props.config.docView[param_data.type].assoc_data.signed_file, //uid-параметра
                        new: 1,
                        key: 0,
                        value: 0,
                        del: 0,
                    }
                ];

                this.core.AJAX.addFile(param_data.obj, param_data.type,
                    param_data.obj_parent, Files, values, () => {
                        this.setState({isLoadingFile: false});
                        this.props.Reload();
                        this.props.CloseCallback();

                    }, () => {
                        this.setState({isLoadingFile: false});
                    });


            }, (data) => {
                ToastrStore.error('Error!', 'ERROR CODE='+data.result);
                this.setState({isLoadingFile: false});
            });

        },
    };

    core = {
        AJAX: {
            addFile: (uid:string, type:string, parent:string, Files:any, values: IValueFile[], SuccFunc, ErrorFunc) => {
                ot_api
                    .addFile(uid, type, parent, Files, values, (progressEvent) => {
                        let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                        this.state.ProgressData.completed = percentCompleted;
                        this.setState({});
                    })
                    .then(res => {
                        SuccFunc(res.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrorFunc();
                    })
            },

            signDoc: (key:string, type:string, SuccFunc, ErrorFunc) => {
                ct_apistep.signDoc(
                    key,
                    type,
                    null
                ).then((res) => {
                    let result = res.data;
                    if (result.result != 1) {
                        this.props.ToastrStore.error(result.errors.join("\n"), 'Error!');
                        ErrorFunc();
                    }
                    else {
                        this.props.ToastrStore.success('Success!', 'Success!');
                        SuccFunc();
                    }
                }).catch((ex) => {
                    this.props.ToastrStore.error('Server error!');
                    ErrorFunc();
                });
            },

            //загрузка штампа и подпись
            signDocStamp: (key:string, type:string, file:any, SuccFunc, ErrorFunc) => {
                ct_apistep.signDoc(
                    key,
                    type,
                    file,
                    (progressEvent) => {
                        let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                        this.state.ProgressData.completed2 = percentCompleted;
                        this.setState({});
                    }
                ).then((res) => {
                    let result = res.data;
                    if (result.result != 1) {
                        this.props.ToastrStore.error(result.errors.join("\n"), 'Error!');
                        ErrorFunc();
                    }
                    else {
                        this.props.ToastrStore.success('Success!', 'Success!');
                        SuccFunc();
                    }
                }).catch((ex) => {
                    this.props.ToastrStore.error('Server error!');
                    ErrorFunc();
                });
            },

            saveObject: (uid:string, type:string, parent:string, values: ot_api.IValue[], SuccFunc, ErrFunc) => {
                ot_api
                    .saveObject(uid, type, parent, values, true)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        ErrFunc({result: 0});
                    })
            },
        },

        build: {
            body: () => {
                let panel_sign = this.core.build.panel_sign_file();
                let panel_stamp = this.core.build.panel_stamp_file();

                if(panel_sign && panel_stamp) {
                    //Убираем загрузку подписанных документов в принципе (так сказали)

                    /*
                    * <div className={"nt_w_d1"}>{panel_sign}</div>
                        <div className={"nt_w_or"}>ou</div>
                    * */

                    return <div className={"nt_wrapper_panels"}>
                        <div className={"nt_w_d2"}>{panel_stamp}</div>
                    </div>
                }
                else {
                    return <div className={"nt_wrapper_panels"}>
                        <div className={"nt_w_d1"}>{"#No File#"}</div>
                    </div>
                    /*
                    return <div className={"nt_wrapper_panels"}>
                        <div className={"nt_w_d1"}>{panel_sign}</div>
                    </div>
                    */
                }
            },

            //панель загрузки подписанного файла
            panel_sign_file: () => {
                let data_key = this.props.data[this.props.dkey];
                let upl_div;
                let file_name = "";
                let d_btn_save;
                let d_date;
                let img_div;


                if(this.state.FileCache) {
                    let value_date = this.state.DateCache;
                    if(value_date) {
                        value_date = parse(
                            value_date,
                            'dd/MM/yyyy HH:mm:ss',
                            new Date()
                        );
                        //2010-04-12 00:00:00+02
                    }
                    d_date = <div className={"nt_co_center_padding"}>
                        <DatePicker key={"datepicker"}
                                    className={"form-control"}
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    dateFormat="dd/MM/yyyy HH:mm"
                                    isClearable
                                    placeholderText={"Enter date"}
                                    showTimeInput
                                    timeInputLabel="Time:"
                                    selected={value_date}
                                    title={value_date}
                                    onChange={(date) => {
                                        let t_value = date;
                                        if(t_value) {
                                            t_value = format(t_value, 'dd/MM/yyyy HH:mm:ss')
                                        }
                                        this.setState({DateCache: t_value});
                                    }}
                        />
                    </div>;
                    if(this.state.DateCache) {
                        d_btn_save = <div className={"nt_co_center_padding"}><button onClick={() => {this.events.onSaveSignedFile()}} type={"button"} className={"nt_btn_def"}>{"Signer le document"}</button></div>;
                    }
                    file_name = this.state.FileCache.name;
                    if(this.state.drag_file_class) {
                        upl_div = <div className={"nt_file_div_upl"}><Icon name="upload_file" color="#0069A5" size={20}/>&nbsp;{'Glissez et déposez le fichier'}</div>;
                    }
                    else {
                        upl_div = <div className={"nt_file_div_upl"}><Icon name="upload_file" color="#0069A5" size={20}/>&nbsp;{file_name}</div>;
                    }
                    img_div = <div className={"nt_img_div"}><img src={this.src_image} alt={""}/></div>;
                }
                else {
                    upl_div = <div className={"nt_file_div_upl"}><Icon name="upload" color="#0069A5" size={20}/>&nbsp;{'Télécharger un document signé'}</div>;
                }

                return <div className={this.state.DateCache?"nt_not_file":""}>
                    <div className={"nt_box_file_dnd " + (this.state.drag_file_class ? "nt_dnd" : "")}
                         onDragOver={this.events.sfile.dragOver}
                         onDragEnter={this.events.sfile.dragEnter}
                         onDragLeave={this.events.sfile.dragLeave}
                         onDrop={this.events.sfile.fileDrop}
                    >
                        {upl_div}
                        <div className={"nt_file_div_upl_line " + (this.state.isLoadingFile?"open":"")}>
                            <div style={{width: this.state.ProgressData.completed + "%"}} className={"nt_file_div_upl_line_perc"} />
                        </div>
                        {img_div}
                        <input className="nt_file" type="file" name="nt_file" accept={'.pdf'} onChange={(e) => {
                            this.events.sfile.handleFiles(e.target.files);
                        }} />
                    </div>
                    {d_date}
                    {d_btn_save}
                </div>
                ;
            },
            //Панель загрузки штампа
            panel_stamp_file: () => {
                let data_key = this.props.data[this.props.dkey];
                if(data_key.data.file) {
                    //Для физической персоны нет штампа, но можно подписать
                    if(this.props.data.persone == "Personne physique") {
                        //Если мы не загружаем никакой файл, то модалка будет состоять только из кнопки "подписать"
                        return <div className={"nt_co_sign"}>
                            <button className={"nt_btn_def"} onClick={()=>{this.events.onSignPhis()}}><Icon name="sign" color="#0069A5" size={24}/>Signer le document</button>
                        </div>;
                    }
                    //Для юридической персоны подписание работает только со штампом
                    else {
                        let upl_div;
                        let d_btn_save;
                        let img_div;

                        if(this.state.FileCache2) {
                            d_btn_save = <div className={"nt_co_center_padding"}><button onClick={() => {this.events.onSaveSignStamp()}} type={"button"} className={"nt_btn_def"}>{"Signer le document"}</button></div>;
                            let file_name = this.state.FileCache2.name;

                            if(this.state.drag_file_class2) {
                                upl_div = <div className={"nt_file_div_upl"}><Icon name="upload_file" color="#0069A5" size={20}/>&nbsp;{'Drag’n’drop la numérisation du sceau'}</div>;
                            }
                            else {
                                upl_div = <div className={"nt_file_div_upl"}><Icon name="upload_file" color="#0069A5" size={20}/>&nbsp;{file_name}</div>;
                            }
                            img_div = <div className={"nt_img_div"}><img src={this.src_image2} alt={""}/></div>;
                        }
                        else {
                            upl_div = <div className={"nt_file_div_upl"}><Icon name="upload" color="#0069A5" size={20}/>&nbsp;{'Merci d\'importer le tampon du bénéficiaire sous format image'}</div>;
                        }

                        return <React.Fragment>
                            <div className={"nt_box_file_dnd " + (this.state.drag_file_class2 ? "nt_dnd" : "")}
                                 onDragOver={this.events.stamp.dragOver}
                                 onDragEnter={this.events.stamp.dragEnter}
                                 onDragLeave={this.events.stamp.dragLeave}
                                 onDrop={this.events.stamp.fileDrop}
                            >
                                {upl_div}
                                <div className={"nt_file_div_upl_line " + (this.state.isLoadingFile?"open":"")}>
                                    <div style={{width: this.state.ProgressData.completed2 + "%"}} className={"nt_file_div_upl_line_perc"} />
                                </div>
                                {img_div}
                                <input className="nt_file" type="file" name="nt_file" accept={'.jpg, .png, jpeg'} onChange={(e) => {
                                    this.events.stamp.handleFiles(e.target.files);
                                }} />
                            </div>
                            {d_btn_save}
                        </React.Fragment>;
                    }
                }
                else {
                    return null;
                }
            },
        }
    };

    handleCreate = () => {
    };

    onShow = () => {
        this.src_image = "";
        this.src_image2 = "";
        this.setState({
            FileCache: null,
            FileCache2: null,
            DateCache: null,
        });
    };

    helper = {
        validateFilePDF: (file) => {
            const validTypes = ['application/pdf'];
            if (validTypes.indexOf(file.type) === -1) {
                return false;
            }
            return true;
        },
        validateFileSTAMP: (file) => {
            const validTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/x-icon'];
            if (validTypes.indexOf(file.type) === -1) {
                return false;
            }
            return true;
        }
    };



    render() {
        let Title;
        let Body = <div/>;
        let Footer = <div/>;
        let isBlocking = false;

        if(this.state.isLoadingSign || this.state.isLoadingSaveObj || this.state.isLoadingFile|| this.state.isLoadingFile2) {
            isBlocking = true;
        }

        if(this.props.show) {
            Footer = <div>
                <button className={"nt_btn_def"} onClick={this.events.handleClose}>
                    Close
                </button>
            </div>;


            Title = <React.Fragment>{this.props.dkey.toLowerCase() + ": "}<span className={'nt_title_span_normal'}>{this.props.data.name}</span></React.Fragment>;


            Body = this.core.build.body();

            //Если файлик залит, то мы его можем подписать
            //Если не залит, то подписать нельзя
        }
        return (
            <Modal className={"nt_modal_nud nt_modal"} onShow={this.onShow} animation={true} show={this.props.show} onHide={this.events.handleClose} enforceFocus={false} >
                <BlockUi className={""} tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                    <Modal.Header>
                        <button type="button" className="close nt_close_btn" onClick={this.events.handleClose}>
                            <span aria-hidden="true">×</span>
                            <span className="sr-only">Close</span>
                        </button>
                        <div className={"nt_title"}>{Title}</div>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                </BlockUi>
            </Modal>
        );
    }
}