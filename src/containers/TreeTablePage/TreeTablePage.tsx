import * as React from "react";
import {inject, observer} from "mobx-react";

import * as moment from 'moment';
import Loader from "react-loaders";
import EnematLoader from '../../components/svg/EnematLoader'
import BlockUi from "react-block-ui";
import {DebounceInput} from 'react-debounce-input';

import axios from 'axios';
import qs from 'qs';
import Cookies from 'universal-cookie';

import * as ct_apiobj from "../../api/ObjTree-api";
import * as ct_apiview from "../../api/View-api";
import * as ct_apistep from "../../api/StepForm-api";

import {TreeTable} from 'primereact/treetable';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {ColumnGroup} from 'primereact/columngroup';
import {Row} from 'primereact/row';
import NTreeTable from './NTreeTable'

import {UserStepPage} from '../UserStepPage/UserStepPage'
import {IOSSwitchGreen} from '../UserStepPage/Components/IOSSwitch'
import {ConfirmModal} from '../UserStepPage/ConfirmModal'
import UploadDocumentModal from './UploadDocumentModal';
import SignDocumentModal from './SignDocumentModal';

import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import './TreeTablePage.scss'
import RootStore from "../../stores/RootStore";

const DateFormat = 'DD/MM/yyyy';
const rounded = function (number) {
    return +number.toFixed(2);
}

interface ITreeTablePageState {
    mode: string,
    data_mode: {
        config?: {},
        initData?: {}
    },
    selectedRow: {},
    isBlocking: boolean,
    refreshTableTree: boolean,
    table_state: {},
    signDocumentStamp: object,
    signDocumentData: object,
    showSignDocumentModal: boolean,
    showUploadDocumentModal: boolean,
    uploadDocumentModalAttrs: object[],
    uploadDocumentModalObject?: string,
    uploadDocumentModalParent?: string,
    uploadDocumentModalClass?: string,

    isLoadConfig: boolean,
    configTable: any
}

interface ITreeTablePageProps {
    ToastrStore?: any,
    UserStepStore?:any,
    TreeTableUpdate?: any,
    match: any,
}

interface IOperation {
    id: number,
    value: number,
    operation: { uid: string, name: string, oper_options: [], min_price: number },
    surface: number,
    isolant: [],
    objUid: [],
    previsite: string,
    bureaudecontrole: string,
    referencerapport: string,
    cost: number,
    cost_client: number,
    prix_unitaire: number,
    coef: number,
    cost_pro: number,
    reste_a_charge: number,
    linked_obj?: string,
    additional_blocks: [],
    disableSurface?: boolean,
    lockquantity?: boolean,
    adv_attr_name: { name: string, value: string }[],
    min_prix: number,
    max_prix:number,
    cumac: number,
    def_quant: number,
    prix_material:number,
    prix_work:number,
    day_between_date:number,
    options_adv_size:{uid:string, name:string}[],
}

interface IIsolant {
    name: string,
    value: string,
    adv_size: [],
    advParams: any,
    surface: number,
    id: number,
    linked_obj?: string,
}

interface IPhys_data {
    id: number,
    numero: string,
    reference: string,
    linked_obj?: string,
    'JSON'?: string,

}

@inject("ToastrStore")
@inject("UserStepStore")
export class TreeTablePage extends React.Component<ITreeTablePageProps, ITreeTablePageState> {
    NameModule: string;

    constructor(props: any) {
        super(props);
        this.state = {
            mode: 'table',
            data_mode: null,
            selectedRow: null,
            isBlocking: false,
            refreshTableTree: false,
            table_state: null,
            signDocumentStamp: null,
            signDocumentData: null,
            showSignDocumentModal: false,
            showUploadDocumentModal: false,
            uploadDocumentModalAttrs: [],
            uploadDocumentModalObject: null,
            uploadDocumentModalParent: null,
            uploadDocumentModalClass: null,

            isLoadConfig: false,
            configTable: {},
        }
    }

    componentDidUpdate(prevProps) {
        let PrevNameModule :  "treetable" | "archivetreetable" = prevProps.match.path.indexOf('/treetable') !== -1 ? "treetable" : "archivetreetable";
        let NameModule :  "treetable" | "archivetreetable" = this.props.match.path.indexOf('/treetable') !== -1 ? "treetable" : "archivetreetable";

        if(PrevNameModule != NameModule) {
            this.NameModule = NameModule;
            document.title = this.NameModule[0].toUpperCase() + this.NameModule.slice(1) + ' | ' + RootStore.config.nameSystem;

            this.setState({});
        }


        if (this.props.TreeTableUpdate != prevProps.TreeTableUpdate) {
            this.setState({
                mode: 'table',
                data_mode: null,
                selectedRow: null,
                isBlocking: false,
                refreshTableTree: false,
                table_state: null,
                signDocumentStamp: null,
                signDocumentData: null,
                showSignDocumentModal: false,
                showUploadDocumentModal: false,
                uploadDocumentModalAttrs: [],
                uploadDocumentModalObject: null,
                uploadDocumentModalParent: null,
                uploadDocumentModalClass: null,
            });
        }

    }

    componentDidMount(): void {
        this.NameModule = this.props.match.path.indexOf('/treetable') !== -1 ? "treetable" : "archivetreetable";
        document.title = this.NameModule[0].toUpperCase() + this.NameModule.slice(1) + ' | ' + RootStore.config.nameSystem;


        ct_apiview.getConfigEnemat().then((res)=>{
            if(res.data.result == 1) {
                let data_res = JSON.parse(JSON.stringify(res.data.data));
                this.setState({configTable: data_res, isLoadConfig: true})
            }
            else {
                let text_err = '';
                if(res.data.errors) {
                    text_err = res.data.errors.map((item) => { return item.text}).join(', ');
                }
                this.props.ToastrStore.error(text_err, 'ERROR CODE:' + res.data.result + ' ');
            }
        })
            .catch((ex) =>{
                this.props.ToastrStore.error('Error', 'Error!');
            });
    }

    CheckSiret = (SuccFunc, ErrFunc) => {
        let SIRET_value;
        let uid_SIRET = this.state.configTable.uid_SIRET;

        for(let index_value in RootStore.MSettings.UserObjData) {
            let item_value = RootStore.MSettings.UserObjData[index_value];
            if(uid_SIRET == item_value.uid) {
                SIRET_value = item_value?.value?.value ? item_value.value.value : "";
                break;
            }
        }
        if(SIRET_value) {
            const cookies = new Cookies();
            let SIRET_RESULT = cookies.get('SIRET_RESULT_C');
            let SIRET_VALUE_C = cookies.get('SIRET_VALUE_C');

            const setCookiesSIRET = (result) => {
                let now = new Date();
                let duration = 1000 * 60 * 60 * 12; //ms * s * m * h
                let expireTime = (now.getTime() + duration);
                now.setTime(expireTime);
                cookies.set('SIRET_RESULT_C', result+"", { path: '/', expires: now });
                cookies.set('SIRET_VALUE_C', SIRET_value+"", { path: '/', expires: now });
            };

            if(SIRET_RESULT === undefined || SIRET_VALUE_C != SIRET_value ) {
                ct_apistep
                    .stepFormGetInfoPoSiren(SIRET_value)
                    .then((res)=>{
                        if(res.data.result==1){
                            setCookiesSIRET(res.data.result);
                            SuccFunc();
                        }
                        else {
                            setCookiesSIRET(res.data.result);
                            this.props.ToastrStore.error('','LE SIRET de votre entreprise n\'est plus actif - merci de soumettre votre nouvelle immatriculation à ENEMAT');
                            ErrFunc();
                        }
                    })
                    .catch(()=>{
                        setCookiesSIRET(0);
                        this.props.ToastrStore.error('','LE SIRET de votre entreprise n\'est plus actif - merci de soumettre votre nouvelle immatriculation à ENEMAT');
                        ErrFunc();
                    });
            }
            else if(SIRET_RESULT == "1") {
                SuccFunc();
            }
            else {
                this.props.ToastrStore.error('','LE SIRET de votre entreprise n\'est plus actif - merci de soumettre votre nouvelle immatriculation à ENEMAT');
                ErrFunc();
            }
        }
        else {
            ErrFunc();
        }
    };


    downloadFile=(uids_file)=>{
      uids_file=uids_file.map((uid)=>{
        return 'files[]='+uid;
      })
      window.open('/dop/downloadFileSpec?'+uids_file.join('&'), '_self');
    };


    onCreateDevis = (table_state) => {
        //Делаем проверку на SIRET
        this.CheckSiret(
            () => {
                let config = this.getConfig('devis', null);
                config.finishFunc = (FullData) => {
                    FullData.created_document = 'devis';
                    console.log(JSON.parse(JSON.stringify(FullData)));
                    this.setState({isBlocking: true}, () => {
                        ct_apiobj.stepFormEniTwo(FullData).then((res) => {
                            let result = res.data;
                            if (result.result == 1) {
                                this.setState({mode: 'table', data_mode: null, isBlocking: false}, ()=>{
                                    this.downloadFile(result.uidsFile);
                                });
                            } else {
                                this.setState({isBlocking: false});
                                this.props.ToastrStore.error('Error', 'Error!');
                            }
                        }).catch(() => {
                            this.setState({isBlocking: false});
                            this.props.ToastrStore.error('Error', 'Error!');
                        });
                    })

                };
                let initData = {
                    dataClaculator: {
                        persone: 'Personne morale',
                        organization: {
                            name: 'Esso',
                            uid: 'b63dd855-2b40-4cef-ba4a-201e76966136',
                        },
                        date: {
                            date_de_signature: moment().format(DateFormat),
                            date_de_debut: '',
                            finDeTravaux: '',
                            date_facture: '',
                            numero_facture: '',
                        },
                        information_lieu_travaux: {
                            operationavec: 'Operation classique',
                            nom_travaux: '',
                            adresse_travaux: '',
                            code_postale_travaux: '',
                            ville_travaux: '',
                            parcelle_cadastrale:'',
                            siern_travaux: '',
                            same_adress: 0,
                            adv_field: {
                                //qpv
                                code_quartier: '',
                                //Bailleur Social
                                raison_sociale: '',
                                siern: '',
                                nom: '',
                                prenom: '',
                                fonction: '',
                                phone: '',
                                adresse: '',
                                code_postale: '',
                                ville: '',

                                nombre_total: '',
                                nombre_total_menages: '',

                                //Operation avec precarite
                                adv_fiscal_data: [{
                                    numero_fiscal: '',
                                    reference_fiscal: '',
                                }],
                            }
                        },
                        beneficiaire: {
                            previsite: '',
                            nom: '',
                            prenom: '',
                            fonction: '',
                            phone: '',
                            email: '',
                            raison_sociale: '',
                            siern: '',
                            adresse: '',
                            code_postale: '',
                            ville: '',
                            persone_phys_data: [],
                            number_de_perosne: '',
                            revenue: '',
                        },
                        sous_traitance: {
                            sous_traitance: 1,
                            siret: '',
                            nom: '',
                            prenom: '',
                            raison_sociale: '',
                        },
                    },
                    dataOperation: [],
                };
                ct_apistep.getTypeOfPersone().then((res) => {
                    if (res.data.defBeniaciare) {
                        initData.dataClaculator.persone = res.data.defBeniaciare;
                    }
                    this.setState({
                        mode: 'formStep',
                        data_mode: {config: config, initData: initData},
                        table_state: table_state
                    });
                })
            },
            () => {},
        );
    };
    onCreateAHFull = (table_state) => {
        this.CheckSiret(
            () => {
                let config = this.getConfig('AHFull', null);
                config.finishFunc = (FullData) => {
                    FullData.created_document = 'devisah';
                    this.setState({isBlocking: true}, () => {
                        ct_apiobj.stepFormEniTwo(FullData).then((res) => {
                            let result = res.data;
                            if (result.result == 1) {
                                this.setState({mode: 'table', data_mode: null, isBlocking: false}, ()=>{
                                    this.downloadFile(result.uidsFile);
                                });
                            } else {
                                this.setState({isBlocking: false});
                                this.props.ToastrStore.error('Error', 'Error!');
                            }
                        }).catch(() => {
                            this.setState({isBlocking: false});
                            this.props.ToastrStore.error('Error', 'Error!');
                        });
                    })
                }
                let initData = {
                    dataClaculator: {
                        persone: 'Personne morale',
                        organization: {
                            name: 'Esso',
                            uid: 'b63dd855-2b40-4cef-ba4a-201e76966136',
                        },
                        date: {
                            date_de_signature: moment().format(DateFormat),
                            date_de_debut: '',
                            finDeTravaux: '',
                            date_facture: '',
                            numero_facture: '',
                        },
                        information_lieu_travaux: {
                            operationavec: 'Operation classique',
                            nom_travaux: '',
                            adresse_travaux: '',
                            code_postale_travaux: '',
                            ville_travaux: '',
                            parcelle_cadastrale:'',
                            siern_travaux: '',
                            same_adress: 0,
                            adv_field: {
                                //qpv
                                code_quartier: '',
                                //Bailleur Social
                                raison_sociale: '',
                                siern: '',
                                nom: '',
                                prenom: '',
                                fonction: '',
                                phone: '',
                                adresse: '',
                                code_postale: '',
                                ville: '',

                                nombre_total: '',
                                nombre_total_menages: '',

                                //Operation avec precarite
                                adv_fiscal_data: [{
                                    numero_fiscal: '',
                                    reference_fiscal: '',
                                }],
                            }
                        },
                        beneficiaire: {
                            previsite: '',
                            nom: '',
                            prenom: '',
                            fonction: '',
                            phone: '',
                            email: '',
                            raison_sociale: '',
                            siern: '',
                            adresse: '',
                            code_postale: '',
                            ville: '',
                            persone_phys_data: [],
                            number_de_perosne: '',
                            revenue: '',
                        },
                        sous_traitance: {
                            sous_traitance: 1,
                            siret: '',
                            nom: '',
                            prenom: '',
                            raison_sociale: '',
                        },
                    },
                    dataOperation: [],
                };
                ct_apistep.getTypeOfPersone().then((res) => {
                    console.log(res.data);
                    if (res.data.defBeniaciare) {
                        initData.dataClaculator.persone = res.data.defBeniaciare;
                    }
                    this.setState({
                        mode: 'formStep',
                        data_mode: {config: config, initData: initData},
                        table_state: table_state
                    });
                })
            },
            () => {}
        );
    };
    onCreateFacture = (data, table_state) => {
        this.CheckSiret(
            () => {
                let config = this.getConfig('facture', data);
                config.finishFunc = (FullData) => {
                    FullData.object_id = data.key;
                    FullData.created_document = 'facture';
                    console.log(JSON.parse(JSON.stringify(FullData)));
                    this.setState({isBlocking: true}, () => {
                        ct_apiobj.stepFormEniTwo(FullData).then((res) => {
                            let result = res.data;
                            if (result.result == 1) {
                                this.setState({mode: 'table', data_mode: null, isBlocking: false}, ()=>{
                                    this.downloadFile(result.uidsFile);
                                });
                            } else {
                                this.setState({isBlocking: false});
                                this.props.ToastrStore.error('Error', 'Error!');
                            }
                        }).catch(() => {
                            this.setState({isBlocking: false});
                            this.props.ToastrStore.error('Error', 'Error!');
                        });
                    })

                };
                this.getIntnData(data.key, 'devis', data.parent_key, false).then((initData) => {
                    console.log(initData);
                    this.setState({
                        mode: 'formStep',
                        data_mode: {config: config, initData: initData},
                        table_state: table_state
                    });
                });
            },
            ()=>{}
            );

    };
    onCreateAH = (data, table_state) => {
        this.CheckSiret(
            () => {
                let config = this.getConfig('AH', data);
                config.finishFunc = (FullData) => {
                    FullData.object_id = data.key;
                    FullData.created_document = 'ah';
                    console.log(JSON.parse(JSON.stringify(FullData)));
                    this.setState({isBlocking: true}, () => {
                        ct_apiobj.stepFormEniTwo(FullData).then((res) => {
                            let result = res.data;
                            if (result.result == 1) {
                                this.setState({mode: 'table', data_mode: null, isBlocking: false}, ()=>{
                                    this.downloadFile(result.uidsFile);
                                });
                            } else {
                                this.setState({isBlocking: false});
                                this.props.ToastrStore.error('Error', 'Error!');
                            }
                        }).catch(() => {
                            this.setState({isBlocking: false});
                            this.props.ToastrStore.error('Error', 'Error!');
                        });
                    });

                };
                this.getIntnData(data.key, 'facture', data.parent_key, false).then((initData) => {
                    console.log(initData);
                    this.setState({
                        mode: 'formStep',
                        data_mode: {config: config, initData: initData},
                        table_state: table_state
                    });
                });
            },
            ()=>{}
        );
    };
    onEdit = (data, table_state, full_data) => {
        console.log(data);
        let config = this.getConfig(data.type, data);
      //  config.finishLabel = 'Générer';
        config.finishFunc = (FullData) => {
            FullData.object_id = data.key;
            FullData.edited_document = data.type.toLocaleLowerCase();
            console.log(JSON.parse(JSON.stringify(FullData)));
            const cahnge_func=()=>{
                  ct_apiobj.stepFormEniTwo(FullData).then((res) => {
                    let result = res.data;
                    if (result.result == 1) {
                        this.setState({mode: 'table', data_mode: null, isBlocking: false}, ()=>{
                          this.downloadFile(result.uidsFile);
                        });
                    } else {
                        this.setState({isBlocking: false});
                        this.props.ToastrStore.error('Error', 'Error!');
                    }
                }).catch(() => {
                    this.setState({isBlocking: false});
                    this.props.ToastrStore.error('Error', 'Error!');
                });
            }
            if((data.type=='devis' && (full_data.facture || full_data.AH)) || (data.type=='facture' && full_data.AH)){
              let text='La modification nécessite de re générer l’attestation sur l’honneur';
              if(data.type=='devis'){
                text='La génération du devis nécessite de régénérer la facture et l\'attestation sur l\'honneur';
              }
              let locked_oper= FullData.operation.filter((item)=>{
                return item.lockquantity?true:false;
              })
              if(data.type=='facture' && locked_oper.length==FullData.operation.length){
                text='La facture ET l’attestation sur l’honneur vont être régénérées , merci de télécharger les nouveaux documents';
              }
              this.props.UserStepStore.setVisibleConfirmModal({
                  show:true,
                  text:text,
                  title:'Confirm',
                  YesTitle:'Oui',
                  NonTitle:'Non',
                  callbackYes:()=>{
                    this.setState({isBlocking: true}, () => {
                        cahnge_func();
                    });
                  },
                  callbackNo:()=>{
                    this.setState({mode: 'table', data_mode: null, isBlocking: false});
                  }
                });
            }else{
              this.setState({isBlocking: true}, () => {
                  cahnge_func();
              });
            }


        };
        if ((data.type == 'devis' || data.type == 'AH') && (data.isSigned || data.hasStatus) || (data.isAllowEdit !== undefined && !data.isAllowEdit)) {
            console.warn('READ MODE');
            config.finishLabel = 'Retour';
            config.finishFunc = (FullData) => {
                this.setState({mode: 'table', data_mode: null});
            };
            if (data.type == 'AH') {
                config.cancelBtn = false;
            }
        }

        this.setState({isBlocking: true});

        this.getIntnData(data.key, data.type, data.parent_key, true).then((initData) => {
            console.log(initData);
            if (data.type == 'facture') {
                if (initData.dataClaculator.date.has_date_signature) {
                    config.disable_DateSignatureAH = true;
                }
            }
            this.setState({
                mode: 'formStep',
                data_mode: {config: config, initData: initData},
                table_state: table_state,
                isBlocking: false
            });
        }).catch(() => {
            this.setState({isBlocking: false});
        });
    }
    onSign = (data) => {
        this.setState({
            // showSignDocumentModal: true,
            // signDocumentData: data,
            isBlocking: true,
        });
        ct_apistep.signDoc(
            data.key,
            data.type,
            null
        ).then((res) => {
            let result = res.data;
            if (result.result != 1) {
                this.props.ToastrStore.error(result.errors.join("\n"), 'Error!');
            } else {
                this.props.ToastrStore.success('Success!', 'Success!');
            }
            this.setState({
                isBlocking: false,
                refreshTableTree: !this.state.refreshTableTree
            });
        });
    }
    onUpload = (data, docView) => {
        let attrs = [
            {
                objId: data.key,
                typeId: data.class,
                attr: {
                    datatype: 'datetime',
                    calc_triggers: {},
                    isKey: data.type == 'facture' ? true : false,
                    isArray: false,
                    isName: false,
                    name: 'Date de signature',
                    uid: docView[data.class].assoc_data.date_signature,
                    require: true,
                    value: null,
                }
            },
            {
                objId: data.key,
                typeId: data.class,
                attr: {
                    datatype: 'file',
                    calc_triggers: {},
                    isKey: false,
                    isArray: false,
                    isName: false,
                    name: 'Signed file',
                    uid: docView[data.class].assoc_data.signed_file,
                    require: false,
                    value: null,
                }
            }
        ];
        this.setState({
            uploadDocumentModalAttrs: attrs,
            uploadDocumentModalObject: data.key,
            uploadDocumentModalClass: data.class,
            uploadDocumentModalParent: data.parent_key,
            showUploadDocumentModal: true,
        });
    }

    getConfig = (mode, data) => {
        let config;
        if (mode == 'devis') {
            config = {
                mode:'devis',
                Data: [
                    {title: 'Bénéficiaire', step: 1},
                    {title: 'Adresse', step: 2},
                    {title: 'Sous-traitance', step: 3},
                    {title: 'Opérations', step: 4, showOrganization: true},
                ],
                cancelBtn: () => {
                    console.log(this.state);
                    this.setState({mode: 'table', data_mode: null, refreshTableTree: !this.state.refreshTableTree});
                },
                finishLabel: 'Générer Devis',
                showStepper: true,
                showDate_de_debut: false,
                showDate_de_finDeTravaux: false,
                oper_action: ['add', 'remove', 'change_previsite', 'change_bureaudecontrole', 'change_referencerapport', 'change_oper'],
                oper_adv_column: [],
                isolant_action: ['change', 'change_param', 'change_surf', 'change_prime', 'change_prix', 'change_adv_size'],
                blockOrganization: false,
            };
        }
        if (mode == 'facture') {
            config = {
                mode:'facture',
                Data: [
                    {
                        title: 'Opérations',
                        step: 4,
                        showOrganization: true,
                        showDate_de_debut: true,
                        showDate_de_finDeTravaux: true,
                        showDate_previsite: true,
                        showDateSignatureAH: true
                    },
                ],
                cancelBtn: () => {
                    this.setState({mode: 'table', data_mode: null});
                },
                finishLabel: 'Générer Facture',
                showStepper: false,
                showDate_de_debut: true,
                showDate_de_finDeTravaux: true,
                oper_action: ['remove', 'change_bureaudecontrole', 'change_referencerapport', 'change_oper'],
                oper_adv_column: [],
                isolant_action: ['change', 'change_param', 'change_surf', 'change_adv_size'],
                blockOrganization: true,

            };
        }
        if (mode == 'AH') {
            config = {
                mode:'AH',
                Data: [
                    {title: 'Opérations', step: 4, showOrganization: true},
                ],
                cancelBtn: () => {
                    this.setState({mode: 'table', data_mode: null});
                },
                finishLabel: 'Générer AH',
                showStepper: false,
                showDate_de_debut: false,
                showDate_de_finDeTravaux: false,
                oper_action: ['change_bureaudecontrole', 'change_referencerapport'],
                oper_adv_column: ['bureaudecontrole', 'referencerapport'],
                isolant_action: ['change_surf'],
                blockOrganization: true,

            };
        }
        if (mode == 'AHFull') {
            config = {
                mode:'AHFull',
                Data: [
                    {title: 'Date lieés au projet', step: 0, showOrganization: true},
                    {title: 'Bénéficiaire', step: 1},
                    {title: 'Adresse', step: 2},
                    {title: 'Sous-traitance', step: 3},
                    {title: 'Opérations', step: 4},
                ],
                cancelBtn: () => {
                    this.setState({mode: 'table', data_mode: null});
                },
                finishLabel: 'Générer AH',
                showStepper: true,
                showDate_de_debut: true,
                showDate_de_finDeTravaux: true,
                oper_action: ['add', 'remove', 'change_previsite', 'change_bureaudecontrole', 'change_referencerapport', 'change_oper'],
                oper_adv_column: ['bureaudecontrole', 'referencerapport'],
                isolant_action: ['change', 'change_param', 'change_surf', 'change_prime', 'change_prix', 'change_adv_size'],
                blockOrganization: false,

            }
        }
        return config;
    }


    getValueFromParam = (param) => {
        if (param.datatype == 'text' && param.value) {
            return param.value.value;
        }
        if (param.datatype == 'object' && param.value) {
            return param.value;
        }
        if (param.datatype == 'datetime' && param.value) {
            return param.value.value.split(' ')[0];
        }
        if (param.datatype == 'number' && param.value) {
            return Number(param.value.value);
        }

        return '';
    }

    getIntnData = (object_id: string, mode: string, parent_id: string, isEdit: boolean) => {
        let initData = {
            dataClaculator: {
                persone: 'Personne morale',
                organization: {
                    name: 'Esso',
                    uid: 'b63dd855-2b40-4cef-ba4a-201e76966136',
                },
                date: {
                    date_de_signature: moment().format(DateFormat),
                    date_facture: '',
                    date_de_debut: '',
                    finDeTravaux: '',
                    numero_facture: '',
                },
                information_lieu_travaux: {
                    operationavec: 'Operation classique',
                    nom_travaux: '',
                    adresse_travaux: '',
                    code_postale_travaux: '',
                    ville_travaux: '',
                    parcelle_cadastrale:'',
                    siern_travaux: '',
                    same_adress: 0,
                    adv_field: {
                        //qpv
                        code_quartier: '',
                        //Bailleur Social
                        raison_sociale: '',
                        siern: '',
                        nom: '',
                        prenom: '',
                        fonction: '',
                        phone: '',
                        adresse: '',
                        code_postale: '',
                        ville: '',

                        nombre_total: '',
                        nombre_total_menages: '',

                        //Operation avec precarite
                        adv_fiscal_data: [{
                            numero_fiscal: '',
                            reference_fiscal: '',
                        }],
                    }
                },
                beneficiaire: {
                    previsite: '',
                    nom: '',
                    prenom: '',
                    fonction: '',
                    phone: '',
                    email: '',
                    raison_sociale: '',
                    siern: '',
                    adresse: '',
                    code_postale: '',
                    ville: '',
                    persone_phys_data: [],
                    number_de_perosne: '',
                    revenue: '',
                },
                sous_traitance: {
                    sous_traitance: 0,
                    siret: '',
                    nom: '',
                    prenom: '',
                    raison_sociale: '',
                },
            },
            dataOperation: [],
        };
        let config = [];
        let obj_data = null;
        let empty_coef_data = [];

        let promise_data = [
            ct_apiobj.getConfigEni(),
            ct_apiobj.getFullChildObjValues([parent_id]),
            ct_apiobj.getAllValuesMany([parent_id], null)
        ];
        return Promise.all(promise_data).then((res) => {
            //res[0] - config
            //res[1] - все данные объекта(строки в таблице)
            //res[2] - данные главного объекта
            config = res[0].data;
            let full_data = res[1].data.data[parent_id];
            let main_obj_data = res[2].data.data[parent_id];
            //Получения нужного объекта.
            let obj_data_devis = full_data.find((item) => {
                return item.type.uid == '5a6364ee-e0de-4772-983f-9f992d57a8fb' ? true : false;
            })
            obj_data = obj_data_devis;
            let isDevis = true;
            let obj_data_facture = null, obj_data_ah = null;

            obj_data_facture = full_data.find((item) => {
                return item.type.uid == 'cc056b7b-b1ba-45b0-a7e3-446bd321abf9' ? true : false;
            })
            obj_data_ah = full_data.find((item) => {
                return item.type.uid == '0207572e-779a-46eb-953f-90c64f98b697' ? true : false;
            })

            if (mode == 'facture') {
                obj_data = obj_data_facture;
            } else if (mode == 'AH') {
                obj_data = obj_data_ah;
            }

            if ((mode == 'facture' && isEdit) || (mode == 'devis' && !isEdit)) {
                initData.dataClaculator.devis_id = obj_data_devis.uid;
            }

            //заполняем шаг 1-4;
            this.fillFormData(initData, config, main_obj_data, obj_data_devis, obj_data_facture, obj_data_ah, mode, isEdit);
            let operations = this.getObjsByType([obj_data], '9887b9f5-81f9-4a40-9b9b-c0458701fa9d', obj_data.uid);

            if (operations.length) {
                let data_operations_uid = operations.filter((item) => {
                    let oper_param = item.attributes.find((param_oper) => {
                        return param_oper.uid == '3e35af51-6b93-4d75-9b55-859a2cdd315a' ? true : false;
                    })
                    return oper_param.value && oper_param.value.uid ? true : false;
                }).map((item) => {
                    let oper_param = item.attributes.find((param_oper) => {
                        return param_oper.uid == '3e35af51-6b93-4d75-9b55-859a2cdd315a' ? true : false;
                    })
                    return oper_param.value.uid;
                }).filter((item, i, self) => {
                    return self.indexOf(item) === i;
                });
                return Promise.all([ct_apiobj.getFullChildObjValues(data_operations_uid), ct_apiobj.getAllValuesMany(data_operations_uid, null)])
            } else {
                return false;
            }
        }).then((res) => {
            if (res) {
                let adv_oper_data = res[0].data.data;
                let adv_oper_value = res[1].data.data;
                this.fillOperations(initData, empty_coef_data, config, obj_data, adv_oper_data, adv_oper_value, isEdit, mode);
            }
            console.log(empty_coef_data);
            if (empty_coef_data.length) {
                return ct_apiobj.stepFormGetCoefsUser(empty_coef_data)
            }
            return false;

        }).then((res) => {
            if (res) {
                if (res.data.result == 1) {
                    res.data.data.coef.forEach((coef_item, i) => {
                        let data = empty_coef_data[i];
                        let oper = initData.dataOperation.find((operation) => {
                            return operation.operation.uid == data.scenario ? true : false;
                        });
                        if (oper) {
                            oper.coef = coef_item;
                        }

                    })
                }
            }
            if(initData.dataOperation.length){
              let calc_data_promise=initData.dataOperation.map((item_oper)=>{
                  let data_calc={
                    form:initData.dataClaculator,
                    oper_data:item_oper.objUid,
                  };
                  return ct_apistep.getOperCalcData('c960cdcb-59d7-4494-8c8a-1e60a7910068', data_calc)
              })
              return Promise.all(calc_data_promise);
            }
            return false;
        }).then((res_arr)=>{
          if(res_arr){
              res_arr.forEach((res, i)=>{
                  if(res.data.result==1){
                    initData.dataOperation[i].max_prix=res.data.data.value;
                    initData.dataOperation[i].min_prix = !initData.dataOperation[i].min_prix ? (initData.dataOperation[i].max_prix / 2) : initData.dataOperation[i].min_prix;
                  }
              })

          }
          return initData;
        });
    }

    //helpers
    getObjsByType = (data: any, type_uid: string, parent_uid: string) => {
        let objs = null;
        data.forEach((item) => {
            if (item.uid == parent_uid) {
                objs = [];
                if (item.children && item.children.length) {
                    objs = item.children.filter((child_item) => {
                        return child_item.type.uid == type_uid ? true : false;
                    });
                }
            }
            if (item.children && item.children.length) {
                let childObjs = this.getObjsByType(item.children, type_uid, parent_uid);
                if (childObjs) {
                    objs = childObjs;
                }
            }
        })
        return objs;
    }
    getObjsByUID = (data: any, uid: string) => {
        let obj = null;
        data.forEach((item) => {
            if (item.uid == uid) {
                obj = item;
            }
            if (item.children && item.children.length) {
                let childObj = this.getObjsByUID(item.children, uid);
                if (childObj) {
                    obj = childObj;
                }
            }
        })
        return obj;
    }
    //

    fillFormData = (initData, config, main_obj_data, obj_data_devis, obj_data_facture, obj_data_ah, mode, isEdit) => {
        //заполняем шаг 1-4;
        obj_data_devis.attributes.map((param) => {
            if (config[param.uid]) {
                let keys = config[param.uid].keys;
                let name = config[param.uid].name;
                let value = this.getValueFromParam(param);
                let data_record = initData.dataClaculator;
                keys.forEach((key, i, self) => {
                    if (i == (self.length - 1)) {
                        if (name == 'devis-org') {
                            if (value) {
                                data_record['uid'] = value.uid;
                                data_record['name'] = value.name;
                            }
                        } else {
                            data_record[key] = value;
                        }
                    } else {
                        data_record = data_record[key];
                    }
                });

            }
        });
        main_obj_data.map((param) => {
            if (config[param.uid]) {
                let keys = config[param.uid].keys;
                let name = config[param.uid].name;
                let value = this.getValueFromParam(param);
                let data_record = initData.dataClaculator;
                keys.forEach((key, i, self) => {
                    if (i == (self.length - 1)) {
                        if (name == 'Error addres') {
                            value = value == 1 ? true : false;
                            data_record[key] = value;
                        } else {
                            data_record[key] = value;
                        }
                    } else {
                        data_record = data_record[key];
                    }
                });

            }
        });
        if (obj_data_facture) {
            obj_data_facture.attributes.map((param) => {
                if (config[param.uid]) {
                    let keys = config[param.uid].keys;
                    let name = config[param.uid].name;
                    let value = this.getValueFromParam(param);
                    let data_record = initData.dataClaculator;
                    keys.forEach((key, i, self) => {
                        if (i == (self.length - 1)) {
                            data_record[key] = value;
                        } else {
                            data_record = data_record[key];
                        }
                    });

                }
            });
        }
        if (obj_data_ah) {
            obj_data_ah.attributes.map((param) => {
                if (config[param.uid]) {
                    let keys = config[param.uid].keys;
                    let name = config[param.uid].name;
                    let value = this.getValueFromParam(param);
                    let data_record = initData.dataClaculator;
                    keys.forEach((key, i, self) => {
                        if (i == (self.length - 1)) {
                            data_record[key] = value;
                        } else {
                            data_record = data_record[key];
                        }
                    });

                }
            });
        }
        if (initData.dataClaculator.sous_traitance.siret) {
            initData.dataClaculator.sous_traitance.sous_traitance = 1;
        }
        if (initData.dataClaculator.date.date_de_signature) {
            initData.dataClaculator.date.date_de_signature_AH = initData.dataClaculator.date.date_de_signature;
            initData.dataClaculator.date.has_date_signature = true;
        } else if (initData.dataClaculator.date.date_de_signature_AH) {
            initData.dataClaculator.date.date_de_signature = initData.dataClaculator.date.date_de_signature_AH;
        }
        if (obj_data_facture && (mode == 'AH' || (mode == 'facture' && !isEdit))) {
            //если нет подписи берем previsiste
            let uid_create = 'a205f5ac-d703-408d-9d5a-e6ab27009734';
            let attr = obj_data_facture.attributes.find((param) => {
                return param.uid == uid_create ? true : false
            });
            let value = this.getValueFromParam(attr);
            if (attr && value) {
                initData.dataClaculator.date.date_de_signature = value;
            }
        }
        if (!initData.dataClaculator.date.date_de_signature && !((mode == 'devis' && !isEdit) || (mode == 'facture' && isEdit))) {
            //если нет подписи берем previsiste
            let uid_create = 'c85b5886-90fb-481c-a367-f8f9f62759b4';
            let attr = obj_data_devis.attributes.find((param) => {
                return param.uid == uid_create ? true : false
            });
            let value = this.getValueFromParam(attr);
            if (attr && value) {
                initData.dataClaculator.date.date_de_signature = value;
            }
        }
        if (!initData.dataClaculator.persone) {
            initData.dataClaculator.persone = 'Personne morale';
        }
        if (!initData.dataClaculator.information_lieu_travaux.same_adress) {
            initData.dataClaculator.information_lieu_travaux.same_adress = 0;
        }
        this.fillPersonePhys(initData, config, obj_data_devis, isEdit);
    }
    fillOperations = (initData, empty_coef_data, config, obj_data, adv_oper_data, adv_oper_value, isEdit, mode) => {
        let operations = this.getObjsByType([obj_data], '9887b9f5-81f9-4a40-9b9b-c0458701fa9d', obj_data.uid);
        initData.dataOperation = operations.filter((item) => {
            let oper_param = item.attributes.find((param_oper) => {
                return param_oper.uid == '3e35af51-6b93-4d75-9b55-859a2cdd315a' ? true : false;
            })
            return oper_param.value && oper_param.value.uid ? true : false;
        }).map((item) => {
            let oper_obj: IOperation = {
                id: Math.round(Math.random() * 1000000000),
                value: 0,
                operation: {uid: '', name: '', oper_options: [], min_price: 0},
                surface: 0,
                isolant: [],
                objUid: [],
                previsite: '',
                bureaudecontrole: '',
                referencerapport: '',
                cost: 0,
                cost_client: 0,
                prix_unitaire: 0,
                prix_material:0,
                prix_work:0,
                cost_pro: 0,
                reste_a_charge: 0,
                coef: 0,
                additional_blocks: [],
                disableSurface: false,
                lockquantity: false,
                adv_attr_name: [],
                min_prix: 0,
                max_prix:0,
                cumac: 0,
                def_quant: 0,
                day_between_date:0,
                options_adv_size:[],
            };
            if (isEdit) {
                oper_obj.linked_obj = item.uid;
            }
            let params = item.attributes;
            params.map((param) => {
                if (config[param.uid]) {
                    let keys = config[param.uid].keys;
                    let name = config[param.uid].name;
                    let value = this.getValueFromParam(param);
                    let data_record = oper_obj;
                    keys.forEach((key, i, self) => {
                        if (i == (self.length - 1)) {
                            if (name == 'oper-Operation') {
                                if (value) {
                                    data_record['uid'] = value.uid;
                                    data_record['name'] = value.name;
                                }
                            } else if (name == 'oper-bure') {
                                if (value) {
                                    data_record[key] = value.uid;
                                }
                            } else if (name == 'objuidJson') {
                                if (value) {
                                    data_record[key] = JSON.parse(value);
                                }
                            } else {
                                data_record[key] = value;
                            }
                        } else {
                            data_record = data_record[key];
                        }
                    });

                }
            });
            /*if(oper_obj.surface){
        oper_obj.prix_unitaire=rounded(oper_obj.cost_client/oper_obj.surface);
      }*/

            let adv_oper_obj = adv_oper_data[oper_obj.operation.uid];
            let adv_oper_value_params = adv_oper_value[oper_obj.operation.uid];

            //Мин цена
            let max_prix = 0;
            let min_prix = null;
            let cumac = Number(oper_obj.surface) * Number(oper_obj.value);
            if ((initData.dataClaculator.persone == 'Personne physique' && initData.dataClaculator.beneficiaire.TableAflag == 1) || (initData.dataClaculator.persone == 'Personne morale' && initData.dataClaculator.information_lieu_travaux.operationavec != 'Operation classique')) {
                let min_price_GP_attr = adv_oper_value_params.find((item) => {
                    return item.uid == '74e4af68-d6f4-4abd-b2ff-fd51f316f4f3' ? true : false;
                });
                if (min_price_GP_attr && min_price_GP_attr.value && min_price_GP_attr.value.value) {
                    min_prix = Number(min_price_GP_attr.value.value);
                }
            } else if (initData.dataClaculator.persone == 'Personne physique' && initData.dataClaculator.beneficiaire.TableBflag == 1) {
                let min_price_P_attr = adv_oper_value_params.find((item) => {
                    return item.uid == '5898dc36-7152-4153-ae8a-c1a1ee2e49ed' ? true : false;
                });
                if (min_price_P_attr && min_price_P_attr.value && min_price_P_attr.value.value) {
                    min_prix = Number(min_price_P_attr.value.value);
                }
            } else {
                let min_price_attr = adv_oper_value_params.find((item) => {
                    return item.uid == '3dc7b442-0228-4ddc-868a-f4220e6b47b7' ? true : false;
                });
                if (min_price_attr && min_price_attr.value && min_price_attr.value.value) {
                    min_prix = Number(min_price_attr.value.value);
                }
            }
            min_prix = !min_prix ? (max_prix / 2) : min_prix;
            oper_obj.min_prix = min_prix;
            oper_obj.cumac = cumac;

            //данные об отображаемых полях
            let option_param= adv_oper_value_params.find((item)=>{
              return item.uid=='d0045cb0-5576-4edc-a13d-9a08d5b57831'?true:false;
            })
            if(option_param && option_param.value){
              oper_obj.options_adv_size=option_param.value.map((opt_item)=>{
                return {uid:opt_item.uid, name:opt_item.name}
              })
            }

            //заполнение данных о доп параметрах
            oper_obj.adv_attr_name = adv_oper_value_params.filter((item) => {
                return item.name.indexOf('Attribute') !== -1 && item.value && item.value.value ? true : false;
            }).map((item) => {
                return {name: item.name, value: item.value.value};
            });
            //заполнение данных о дефолтном значении площади
            let def_quant_param = adv_oper_value_params.find((item) => {
                return item.uid == 'a176581e-e030-42ca-9f56-a9f207703b03' ? true : false;
            })
            if (def_quant_param && def_quant_param.value && def_quant_param.value.value) {
                oper_obj.def_quant = def_quant_param.value.value;
            }
            //заполняем изолянты
            this.fillIsolants(oper_obj, config, item, isEdit);
            //заполнение данных о доп блоках
            this.fillAdditBlocks(oper_obj, config, item, adv_oper_obj, isEdit);
            //устанавливаем можно ли менять Surface
            //(только при редактировании/создании ah/facture)
            if (mode == 'facture' || mode == 'AH') {
                let lock_param = adv_oper_value_params.find((param) => {
                    return param.uid == 'f23e965a-1493-4de1-8a49-dec86103de36' ? true : false;
                })
                if (lock_param && lock_param.value && lock_param.value.name == 'Yes') {
                    if((mode == 'facture' && !isEdit) || mode == 'AH'){
                      oper_obj.disableSurface = true;
                    }
                    oper_obj.lockquantity = true;
                }
            }

            //колв-во дней между датам
            let date_param=adv_oper_value_params.find((item)=>{
              return item.uid=='db32bbd2-d1ec-441f-961e-77b8f8b9e72e'?true:false;
            })
            if(date_param && date_param.value){
              oper_obj.day_between_date= date_param.value.value;
            }

            oper_obj.cost = oper_obj.cost_client + oper_obj.cost_pro;
            if (!oper_obj.coef) {
                empty_coef_data.push({
                    organization: initData.dataClaculator.organization.uid,
                    scenario: oper_obj.operation.uid,
                    persone: initData.dataClaculator.persone == 'Personne morale' ? 0 : 1,
                    date: initData.dataClaculator.date.date_de_signature,
                });

            }
            //установка значений по умолчанию для prix_material и prix_work(если пустые в объекте)
            if(!oper_obj.prix_work && !oper_obj.prix_material){
              oper_obj.prix_work=rounded(oper_obj.prix_unitaire*0.6);
              oper_obj.prix_material=rounded(oper_obj.prix_unitaire*0.4);
            }
            return oper_obj;
        });
    }
    fillIsolants = (data, config, obj_data, isEdit) => {
        //child_isolant
        let isolants = this.getObjsByType([obj_data], '38e56551-7b85-47a2-985d-21589255b3de', obj_data.uid);
        let total_surface = 0;
        data.isolant = isolants.map((isolant) => {
            let adv_attr_data = {};
            if (data.adv_attr_name.length) {
                data.adv_attr_name.forEach((item) => {
                    let key = item.name.replaceAll(/name/ig, '').trim();
                    adv_attr_data[key] = {title: item.value, value: ''};
                })
            }
            let isolant_obj: IIsolant = {
                name: '-Ajouter un isolant-',
                value: '-2',
                adv_size: [],
                advParams: {
                    'Marque': {title: 'Marque', value: ''},
                    'Reference': {title: 'Reference', value: ''},
                    ...adv_attr_data
                },
                surface: 0,
                id: Math.round(Math.random() * 1000000000),
            };
            if (isEdit) {
                isolant_obj.linked_obj = isolant.uid;
            }
            let params = isolant.attributes;
            let exist_in_db = false;
            params.map((param) => {
                if (config[param.uid]) {
                    let keys = config[param.uid].keys;
                    let name = config[param.uid].name;
                    let value = this.getValueFromParam(param);
                    let data_record = isolant_obj;
                    for (let i = 0; i < keys.length; i++) {
                        let key = keys[i];
                        if (i == (keys.length - 1)) {
                            if (name == 'isol-isolant') {
                                if (value) {
                                    data_record['value'] = value.uid;
                                    exist_in_db = true;
                                }
                            } else {
                                data_record[key] = value;
                            }
                        } else {
                            if (name == 'isol-adv_params') {
                                if (data_record[key] != undefined) {
                                    data_record = data_record[key];
                                } else {
                                    break;
                                }
                            } else {
                                data_record = data_record[key];
                            }
                        }
                    }

                }
            });
            if (exist_in_db) {
                isolant_obj.name = isolant_obj.advParams.Marque.value + ' ' + isolant_obj.advParams.Reference.value;
            }
            total_surface += isolant_obj.surface;

            //заполнение данных о доп. диаметров
            this.fillAdvSize(isolant_obj, config, isolant, data, isEdit);

            return isolant_obj;
        });
        data.surface = total_surface;
    }
    fillAdvSize = (data, config, obj_data, oper_data, isEdit) => {
        let advSizes = this.getObjsByType([obj_data], '10db6093-56c2-4c43-af67-afa8e6436317', obj_data.uid);
        data.adv_size = advSizes.map((diametr_obj) => {
            let l = 0;
            let l_attr = diametr_obj.attributes.find((attr) => {
                return attr.uid == 'dc083dde-18be-460f-a2d9-0fff88803d6a' ? true : false;
            });
            if (l_attr && l_attr.value) {
                l = l_attr.value.value;
            }
            let d = 0;
            let d_attr = diametr_obj.attributes.find((attr) => {
                return attr.uid == '6bf51662-c812-4bd8-ab4c-2b79d0d50d10' ? true : false;
            });
            if (d_attr && d_attr.value) {
                d = d_attr.value.value;
            }
            let ep = 0;
            let ep_attr = diametr_obj.attributes.find((attr) => {
                return attr.uid == 'c3305c7a-ad74-4eb9-ab8b-4f67371910f3' ? true : false;
            });
            if (ep_attr && ep_attr.value) {
                ep = ep_attr.value.value;
            }
            let adv_size = {l: l, d: d, ep:ep};
            return adv_size;
        })
    }
    fillAdditBlocks = (data, config, obj_data, adv_oper_data, isEdit) => {
        let additional_blocks = this.getObjsByType([obj_data], '9a31a433-8b11-41dc-b480-4bee8ec00e57', obj_data.uid);
        let existing_blocks = [];
        //блоки которые сохранены пользователем
        data.additional_blocks = additional_blocks.filter((addit_block) => {
            let attr_data = addit_block.attributes.find((attr) => {
                return attr.uid == '6bd102b8-e366-4985-971b-7cdcbc851778' ? true : false;
            })
            return attr_data && attr_data.value ? true : false;
        }).map((addit_block) => {
            let attr_data = addit_block.attributes.find((attr) => {
                return attr.uid == '6bd102b8-e366-4985-971b-7cdcbc851778' ? true : false;
            })
            let exist_obj = this.getObjsByUID(adv_oper_data, attr_data.value.uid);
            let attr_name = exist_obj.attributes.find((attr_item) => {
                return attr_item.uid == '0fd7cb64-29cf-4232-85b1-e81d78682a42' ? true : false;
            })
            let attr_type = exist_obj.attributes.find((attr_item) => {
                return attr_item.uid == '5736390f-272e-46ff-a186-89e474a4d910' ? true : false;
            })
            let attr_deleted = exist_obj.attributes.find((attr_item) => {
                return attr_item.uid == 'b741480b-4ed7-48a2-a7bd-54d5efdf25cc' ? true : false;
            })
            let addit_block_obj = {
                uid: exist_obj.uid,
                name: attr_name.value ? attr_name.value.value : '',
                data: [],
                type: {
                    uid: attr_type.value.uid,
                    name: attr_type.value.name,
                },
                isDeleted: attr_deleted.value,
            };
            if (isEdit) {
                addit_block_obj.linked_obj = addit_block.uid;
            }
            //заполнение данными addit_block
            this.fillAdditElemets(addit_block_obj, config, addit_block, adv_oper_data, isEdit);
            existing_blocks.push(exist_obj.uid);

            return addit_block_obj;
        });
        //блоки которые есть в бд
        let additional_blocks_exist = adv_oper_data.filter((exits_block) => {
            return exits_block.type.uid == 'bc85f46f-7fdd-496e-bfe0-1ac578cb66c1' ? true : false;
        });
        let existing = additional_blocks_exist.filter((addit_block_exist) => {
            let attr_deleted = addit_block_exist.attributes.find((attr_item) => {
                return attr_item.uid == 'b741480b-4ed7-48a2-a7bd-54d5efdf25cc' ? true : false;
            })
            return existing_blocks.indexOf(addit_block_exist.uid) == -1 && !attr_deleted.value ? true : false;
        }).map((addit_block_exist) => {
            let attr_type = addit_block_exist.attributes.find((attr_item) => {
                return attr_item.uid == '5736390f-272e-46ff-a186-89e474a4d910' ? true : false;
            })
            let attr_deleted = addit_block_exist.attributes.find((attr_item) => {
                return attr_item.uid == 'b741480b-4ed7-48a2-a7bd-54d5efdf25cc' ? true : false;
            })
            let attr_name = addit_block_exist.attributes.find((attr_item) => {
                return attr_item.uid == '0fd7cb64-29cf-4232-85b1-e81d78682a42' ? true : false;
            })
            return {
                uid: addit_block_exist.uid,
                name: attr_name.value ? attr_name.value.value : '',
                data: [],
                type: {
                    uid: attr_type.value.uid,
                    name: attr_type.value.name,
                },
                isDeleted: attr_deleted.value,
            };
        });
        data.additional_blocks = [...data.additional_blocks, ...existing];

    }
    fillAdditElemets = (data, config, obj_data, adv_oper_data, isEdit) => {
        let additional_elemtns = this.getObjsByType([obj_data], 'd7f886f1-0ae0-4e8b-9f2d-66b6b77e07e2', obj_data.uid);
        data.data = additional_elemtns.filter((addit_element) => {
            let attr_data = addit_element.attributes.find((attr) => {
                return attr.uid == '6395d1c2-3ba7-4a71-881f-f20f88e0efa9' ? true : false;
            })
            return attr_data && attr_data.value ? true : false;
        }).map((addit_element) => {
            let attr_data = addit_element.attributes.find((attr) => {
                return attr.uid == '6395d1c2-3ba7-4a71-881f-f20f88e0efa9' ? true : false;
            })
            let attr_user_data = addit_element.attributes.find((attr) => {
                return attr.uid == '7f256362-eef7-4e56-be6d-0479f17bde86' && attr.value ? true : false;
            })
            let exist_obj = this.getObjsByUID(adv_oper_data, attr_data.value.uid);
            let attr_name = exist_obj.attributes.find((attr_item) => {
                return attr_item.uid == '9f661741-ad54-4a20-a62f-1b84eaae4076' ? true : false;
            })
            let addit_element_obj = {
                uid: exist_obj.uid,
                name: attr_name.value ? attr_name.value.value : '',
            };
            if (attr_user_data) {
                addit_element_obj.userData = attr_user_data.value.value;
            }
            if (isEdit) {
                addit_element_obj.linked_obj = addit_element.uid;
            }
            return addit_element_obj;
        });

    }
    fillPersonePhys = (initData, config, obj_data, isEdit) => {
        let phys_data = this.getObjsByType([obj_data], 'e883e744-8718-4da3-8d09-d4fc500b55a4', obj_data.uid);
        initData.dataClaculator.beneficiaire.persone_phys_data = phys_data.map((phys_item) => {
            let phys_obj: IPhys_data = {
                id: Math.round(Math.random() * 1000000000),
                numero: '',
                reference: '',
            };
            let params = phys_item.attributes;
            params.map((param) => {
                if (config[param.uid]) {
                    let keys = config[param.uid].keys;
                    let name = config[param.uid].name;
                    let value = this.getValueFromParam(param);
                    let data_record = phys_obj;
                    keys.forEach((key, i, self) => {
                        if (i == (self.length - 1)) {
                            data_record[key] = value;
                        } else {
                            data_record = data_record[key];
                        }
                    });
                }
            });
            if (isEdit) {
                phys_obj.linked_obj = phys_item.uid;
            }
            return phys_obj;
        });
    }


    render() {
        let isBlocking = this.state.isBlocking;
        let contentJSX;
        if(this.state.isLoadConfig) {

            contentJSX = <React.Fragment>
                {
                    this.state.mode == 'table' ?
                        <TreeTableModule
                            NameModule={this.NameModule}

                            configTable={this.state.configTable}

                            initialState={this.state.table_state}
                            CheckSiret={this.CheckSiret}
                            onEdit={this.onEdit}
                            onSign={this.onSign}
                            onUpload={this.onUpload}
                            onCreateDevis={this.onCreateDevis}
                            onCreateAHFull={this.onCreateAHFull}
                            onCreateFacture={this.onCreateFacture}
                            onCreateAH={this.onCreateAH}
                            refresh={this.state.refreshTableTree}
                        /> : null
                }
                {
                    this.state.mode == 'table' ?
                        <UploadDocumentModal
                            show={this.state.showUploadDocumentModal}
                            attrs={this.state.uploadDocumentModalAttrs}
                            object={this.state.uploadDocumentModalObject}
                            parent={this.state.uploadDocumentModalParent}
                            class={this.state.uploadDocumentModalClass}
                            title="Upload signed file"
                            onClose={() => {
                                this.setState({showUploadDocumentModal: false})
                            }}
                            onUpload={() => {
                                this.setState({
                                    showUploadDocumentModal: false,
                                    refreshTableTree: !this.state.refreshTableTree
                                })
                            }}
                        /> : null
                }
                {
                    this.state.mode == 'table' ?
                        <SignDocumentModal
                            show={this.state.showSignDocumentModal}
                            onClose={() => {
                                this.setState({showSignDocumentModal: false})
                            }}
                            onLoad={() => {
                                this.setState({
                                    showSignDocumentModal: false,
                                    isBlocking: true,
                                });
                                ct_apistep.signDoc(
                                    this.state.signDocumentData.key,
                                    this.state.signDocumentData.type,
                                    this.state.signDocumentStamp
                                ).then((res) => {
                                    let result = res.data;
                                    if (result.result != 1) {
                                        this.props.ToastrStore.error(result.errors.join("\n"), 'Error!');
                                    } else {
                                        this.props.ToastrStore.success('Success!', 'Success!');
                                    }
                                    this.setState({
                                        isBlocking: false,
                                        refreshTableTree: !this.state.refreshTableTree
                                    });
                                });
                            }
                            }
                            onChoose={(file) => {
                                this.setState({signDocumentStamp: file});
                            }
                            }
                            onRemove={() => {
                                this.setState({signDocumentStamp: null});
                            }
                            }
                        /> : null
                }
                {
                    this.state.mode == 'formStep' ?
                        <UserStepPage
                            config={this.state.data_mode.config}
                            initialStore={this.state.data_mode.initData}
                        /> : null
                }
            </React.Fragment>;
        }
        else {
            isBlocking = true;
        }

        return (
            <BlockUi className={"zt_window_cloth zt_tree_table_page"} tag="div" loader={<EnematLoader/>}
                     blocking={isBlocking}>
                {contentJSX}
                <ConfirmModal />
            </BlockUi>
        )
    }
}


interface ITreeTableState {
    nodes: [],
    first: number,
    rows: number,
    totalRecords: number,
    loading: boolean,
    selectedNodeKeys: {},
    selectedData: {},
    createFactureBtn: boolean,
    createAHBtn: boolean,
    editBtn: boolean,
    signBtn: boolean,
    uploadBtn: boolean,
    expandedKeys: {},
    searchText: string,
    isTreeTable: boolean,

    dataOpen?: {[key:string]:any},
    scrollPositionY?: number,
    countData?: number,

    isCompletedFilterTable: boolean,
    isAdminViewTable: boolean,
}

interface ITreeTableProps {
    initialState: {},
    CheckSiret: (SuccFunc:()=>void, ErrFunc:()=>void) => void,
    onEdit: (data: {}, table_state: any, full_data:any) => void,
    onSign: (data: {}) => void,
    onUpload: (data: {}, doc_view: any) => void,
    onCreateDevis: (table_state: any) => void,
    onCreateAHFull: (table_state: any) => void,
    onCreateFacture: (data: {}, table_state: any) => void,
    onCreateAH: (data: {}, table_state: any) => void,
    refresh: boolean,

    configTable: any,

    match: any,

    NameModule: "treetable" | "archivetreetable",
}

class TreeTableModule extends React.Component<ITreeTableProps> {
    ajax:any = {

    }; // Теперь это загружается через сервер
    state: Readonly<ITreeTableState> = {
        nodes: [],
        first: 0,
        rows: 10,
        totalRecords: 0,
        loading: true,
        selectedNodeKeys: null,
        selectedData: null,
        createFactureBtn: false,
        createAHBtn: false,
        editBtn: false,
        signBtn: false,
        uploadBtn: false,
        expandedKeys: {},
        searchText: '',
        isTreeTable: false,

        isCompletedFilterTable: false,
        isAdminViewTable: false,
    };

    isAdmin = false;

    constructor(props) {
        super(props);
        this.ajax = this.props.configTable;
        if (this.props.initialState) {
            this.state = {
                ...this.state,
                ...this.props.initialState,
                nodes: [],
            };
        }
        this.onExpand = this.onExpand.bind(this);
        this.onPage = this.onPage.bind(this);


        //Делаем обход на проверку, являемся ли мы админом
        for(let index_group in RootStore.MSettings.UserGroups) {
            let item_group = RootStore.MSettings.UserGroups[index_group];
            let uid_group = item_group.IDo;

            if(this.ajax.adminGroups[uid_group] != undefined) {
                this.isAdmin = true;
                break;
            }
        }
    }


    convertData = (data, lvl, parentNode) => {
        let data_converted = [];
        let getDateHelper = (Data) => {
            let DateIn = Data.split(' ')[0];
            let DateArr = DateIn.split('/');
            return new Date(DateArr[2], (Number(DateArr[1]) - 1), DateArr[0]);
        };
        let getValue = (attr) => {
            if (attr.datatype == 'datetime' && attr.value) {
                let tmp_date = getDateHelper(attr.value.value);
                return tmp_date.toISOString();
            }
            if (attr.datatype == 'file') {
                if (attr.value) {
                    attr.value.p_uid = attr.uid;
                }
                return attr.value;
            }
            if (attr.LinkType) {
                return attr.value.name;
            }
            if (attr.value) {
                return attr.value.value;
            }
            return '';
        }
        if (lvl == 0) {
            let assoc_data = this.ajax.rootView.assoc_data;
            data.forEach((element) => {
                let key = '';
                let obj_data = {
                    key: null,
                    data: {},
                    leaf: false,
                    lvl: 1,
                };
                Object.keys(assoc_data).map((column) => {
                    let uid = assoc_data[column];
                    if (uid && element[uid] && element[uid].v) {
                        obj_data.data[column] = element[uid].v;
                        if (!key) {
                            key = element[uid].o;
                        }
                    } else {
                        obj_data.data[column] = '';
                    }
                });
                obj_data.key = key;

                data_converted.push(obj_data);
            });
        }
        if (lvl == 1) {
            data.forEach((element) => {
                let obj_data = {
                    key: element.uid,
                    parent_key: parentNode.key,
                    data: {
                        name: element.type.name,
                    },
                    type: this.ajax.docView[element.type.uid].type,
                    leaf: false,
                    lvl: 2,
                    isSigned: false,
                    hasStatus: false,
                    class: element.type.uid
                };
                let assoc_data = this.ajax.docView[element.type.uid].assoc_data;
                Object.keys(assoc_data).map((column) => {
                    let uid = assoc_data[column];
                    let attr = element.attributes.find((item) => {
                        return item.uid == uid ? true : false;
                    })
                    if (attr) {
                        let value = getValue(attr);
                        if (column == 'date_signature' && value) {
                            obj_data.isSigned = true;
                        }
                        if (column == 'status' && value) {
                            obj_data.hasStatus = true;
                        }
                        obj_data.data[column] = value;
                    } else {
                        obj_data.data[column] = '';
                    }
                });
                data_converted.push(obj_data);
            });
            let sort_index = ['devis', 'facture', 'AH'];
            data_converted.sort((itemA, itemB) => {
                return sort_index.indexOf(itemA.type) - sort_index.indexOf(itemB.type);
            });
            let devis = data_converted.find((item) => {
                return item.type == 'devis' ? true : false
            });
            parentNode.data.status = {create_facture: devis.key};
            let facture = data_converted.find((item) => {
                return item.type == 'facture' ? true : false
            });
            if (facture) {
                parentNode.data.status = {create_ah: facture.key};
            }
            let ah = data_converted.find((item) => {
                return item.type == 'AH' ? true : false
            });
            if (ah) {
                parentNode.data.status = 'Terminé';
            }

        }
        if (lvl == 2) {
            let assoc_data = this.ajax.operView.assoc_data;
            data.forEach((element) => {
                let obj_data = {
                    key: element.uid,
                    data: {},
                    leaf: true,
                    lvl: 3,
                };
                Object.keys(assoc_data).map((column) => {
                    let uid = assoc_data[column];
                    let attr = element.attributes.find((item) => {
                        return item.uid == uid ? true : false;
                    })
                    if (attr) {
                        obj_data.data[column] = getValue(attr);
                    } else {
                        obj_data.data[column] = '';
                    }
                });
                data_converted.push(obj_data);
            });
        }
        if (lvl == 'row') {
            data.forEach((element) => {
                let obj_data = {
                    key: element.uid,
                    parent_key: parentNode.key,
                    data: {},
                    type: this.ajax.docView[element.type.uid].type,
                    isSigned: false,
                    hasStatus: false,
                    class: element.type.uid
                };
                let assoc_data = this.ajax.docView[element.type.uid].assoc_data;
                Object.keys(assoc_data).map((column) => {
                    let uid = assoc_data[column];
                    let attr = element.attributes.find((item) => {
                        return item.uid == uid ? true : false;
                    })
                    if (attr) {
                        let value = getValue(attr);
                        if (column == 'date_signature' && value) {
                            obj_data.isSigned = true;
                        }
                        if (column == 'status' && value) {
                            obj_data.hasStatus = true;
                        }
                        obj_data.data[column] = value;
                    } else {
                        obj_data.data[column] = '';
                    }
                });
                data_converted.push(obj_data);
            });
        }

        return data_converted;
    }
    getRecursiveNode = (key, data) => {
        let node;
        for (let i = 0; i < data.length; i++) {
            if (data[i].key == key) {
                return data[i];
            }
            if (data[i].children && data[i].children.length) {
                node = this.getRecursiveNode(key, data[i].children);
                if (node) {
                    return node;
                }
            }

        }
        return node;
    }
    selectNode = (e) => {
        let stateNode = this.getRecursiveNode(e.value, this.state.nodes);
        let createFactureBtn = false;
        let createAHBtn = false;
        let editBtn = false;
        let signBtn = false;
        let uploadBtn = false;
        if (stateNode.lvl == 2 && stateNode.type == 'devis') {
            let ParentNode = this.getRecursiveNode(stateNode.parent_key, this.state.nodes);
            if (ParentNode) {
                let facture = ParentNode.children.find((item) => {
                    return item.type == 'facture' ? true : false
                });
                if (!facture) {
                    createFactureBtn = true;
                }
            }
            //
            editBtn = true;
            let par_data = this.getRecursiveNode(stateNode.parent_key, this.state.nodes);
            if (!stateNode.isSigned && !stateNode.hasStatus) {
                if (par_data.data.persone == 'Personne physique') {
                    signBtn = true;
                }
                uploadBtn = true;
            }
        }
        if (stateNode.lvl == 2 && stateNode.type == 'facture') {
            let ParentNode = this.getRecursiveNode(stateNode.parent_key, this.state.nodes);
            if (ParentNode) {
                let ah = ParentNode.children.find((item) => {
                    return item.type == 'AH' ? true : false
                });
                if (!ah) {
                    createAHBtn = true;
                }
            }
            //  createAHBtn=true;
            editBtn = true;
        }
        if (stateNode.lvl == 2 && stateNode.type == 'AH') {
            editBtn = true;
            let par_data = this.getRecursiveNode(stateNode.parent_key, this.state.nodes);
            if (!stateNode.isSigned && !stateNode.hasStatus) {
                if (par_data.data.persone == 'Personne physique') {
                    signBtn = true;
                }
                uploadBtn = true;
            }

        }
        this.setState({
            selectedNodeKeys: e.value,
            selectedData: stateNode,
            createFactureBtn: createFactureBtn,
            createAHBtn: createAHBtn,
            editBtn: editBtn,
            signBtn: signBtn,
            uploadBtn: uploadBtn
        });
    }
    refreshTable = () => {
        this.loadRootData(this.state.first, this.state.rows, this.state.searchText, {});
    };

    componentDidMount(): void {
        this.refreshTable();
    }
    componentDidUpdate(prevProps: Readonly<ITreeTableProps>, prevState: Readonly<ITreeTableProps>, snapshot?: any): void {
        if(this.props.NameModule != prevProps.NameModule) {
            this.refreshTable();
        }
    }

    componentWillReceiveProps(props) {
        const {refresh} = this.props;
        if (props.refresh !== refresh) {
            this.refreshTable();
        }
    }

    onExpand(e) {
        let stateNode = this.getRecursiveNode(e.node.key, this.state.nodes);
        if (stateNode && !stateNode.children) {
            this.setState({loading: true}, () => {
                ct_apiobj.getAllChildObjValues(stateNode.key).then((res) => {
                    let result = res.data;
                    if (result.result) {
                        let nodes = this.convertData(result.data, stateNode.lvl, stateNode);
                        stateNode.children = nodes;
                        if (!nodes.length) {
                            stateNode.leaf = true;
                        }
                        this.setState({
                            nodes: this.state.nodes,
                            loading: false,
                        });
                    }
                })
            })
        }
    }
    onPage(e) {
        this.loadRootData(e.first, e.rows, this.state.searchText, {expandedKeys: {}, first: e.first});
    }

    onChangeIsCompletedFilterTable = (e) => {
        this.setState({isCompletedFilterTable: !this.state.isCompletedFilterTable});
    };
    onChangeIsAdminViewTable = (e) => {
        this.setState({isAdminViewTable: !this.state.isAdminViewTable});
    };

    onSearch = (e) => {
        let {value} = e.target;
        if (this.state.searchText != value) {
            this.setState({searchText: value}, () => {
                this.loadRootData(0, this.state.rows, this.state.searchText, {expandedKeys: {}});
            })
        }
    }
    loadRootData = (start, num, search, defFinalState) => {
        if (this.state.isTreeTable) {
            ct_apiview.getTableData({
                view: this.ajax.rootView.uid,
                attr_sort: '',
                sort: 0,
                start: (start + 1),
                num: num,
                attr_filter: [{op: '~', v: search}],
            }).then((res) => {
                let result = res.data;
                if (result.result) {
                    let nodes = this.convertData(result.data, 0, null);
                    //обновляем если только получаем 1 страниц- в иных случаях в запросе allCount = -1
                    let totalRecords = start == 0 ? result.allCount : this.state.totalRecords;
                    this.setState({
                        loading: false,
                        totalRecords: totalRecords,
                        nodes: nodes,
                        ...defFinalState
                    }, () => {
                        let uids = this.state.nodes.map((item) => {
                            return item.key
                        });
                        Object.keys(this.state.expandedKeys).forEach((key) => {
                            if (uids.indexOf(key) == -1) {
                                uids.push(key);
                            }
                        });
                        this.expandNodes(uids);
                        /*  if (uids.length) {
              for (let i = 0; i < uids.length; i++) {
                this.onExpand({
                  node: {
                    key: uids[i]
                  }
                });
              }
            }*/
                    });
                }
            })
        } else {




            this.setState({
                loading: false});
            return;



            ct_apiview.getTableDataEnemat({
                view: this.ajax.rootView.uid,
                attr_sort: '',
                sort: 0,
                start: (start + 1),
                num: num,
                attr_filter: [{op: '~', v: search}],
            }).then((res) => {
                let result = res.data;
                if (result.result) {
                    let nodes = this.convertData(result.data, 0, null);
                    //обновляем если только получаем 1 страниц- в иных случаях в запросе allCount = -1

                    let totalRecords = start == 0 ? result.allCount : this.state.totalRecords;
                    let uids = nodes.map((item) => {
                        return item.key
                    });
                    this.loadChildDataRow(uids, nodes, totalRecords, defFinalState);
                }
            })
        }
    }
    expandNodes = (uids) => {
        this.setState({loading: true}, () => {
            let promises_arr = uids.map((uid) => {
                return ct_apiobj.getAllChildObjValues(uid)
            });
            Promise.all(promises_arr).then((result_arr) => {
                //res[1].data.data
                let newState = this.state.nodes;
                console.log(result_arr);
                result_arr.forEach((res, i) => {
                    console.log(res);
                    let result = res.data;
                    let stateNode = this.getRecursiveNode(uids[i], newState);
                    if (result.result) {
                        let nodes = this.convertData(result.data, stateNode.lvl, stateNode);
                        stateNode.children = nodes;
                        if (!nodes.length) {
                            stateNode.leaf = true;
                        }
                    }
                });
                this.setState({
                    nodes: newState,
                    loading: false,
                });
            })

        });
    }
    loadChildDataRow = (uids, par_nodes_data, totalRecords, defFinalState) => {
        let promises_arr = uids.map((uid) => {
            return ct_apiobj.getAllChildObjValues(uid)
        });
        Promise.all(promises_arr).then((result_arr) => {
            let nodes_row = [];
            result_arr.forEach((res, i) => {
                let result = res.data;
                let child_nodes = this.convertData(result.data, 'row', par_nodes_data[i]);

                let full_data = par_nodes_data[i].data;
                for (let index = 0; index < child_nodes.length; index++) {
                    full_data[child_nodes[index].type] = child_nodes[index];
                }
                nodes_row.push(full_data);
            });
            console.log(nodes_row);
            this.setState({
                loading: false,
                totalRecords: totalRecords,
                nodes: nodes_row,
                ...defFinalState
            });
        })
    }

    changeViewTable = () => {
        this.setState({
            nodes: [],
            first: 0,
            rows: 10,
            totalRecords: 0,
            loading: true,
            selectedNodeKeys: null,
            selectedData: null,
            createFactureBtn: false,
            createAHBtn: false,
            editBtn: false,
            signBtn: false,
            uploadBtn: false,
            expandedKeys: {},
            searchText: '',
            isTreeTable: !this.state.isTreeTable,
        }, () => {
            this.refreshTable();
        })
    }

    treeTableRender = (table_state, date_parsesr, file_parsesr, statu_parser) => {
        return (
            <div className="table_tree_cont">
                <div className="col-md-12 table_tree_control">
                    <div className="right_ctrl">

                        <div className="p-input-icon-left"><i className="fas fa-search"></i><DebounceInput
                            value={this.state.searchText} debounceTimeout={1500} onChange={this.onSearch} type="search"
                            placeholder="Lancer la recherche ici" className="p-inputtext p-component"/></div>
                    </div>
                    <div className="left_ctrl">
                        <div>
                            <button onClick={() => {
                                this.props.onCreateDevis(table_state)
                            }} className="btn btn-add-oper"><i className="fa fa-plus"></i> Créer devis
                            </button>
                        </div>
                        <div style={{flex: "1 1 auto"}}>
                            <button onClick={() => {
                                this.props.onCreateAHFull(table_state)
                            }} className="btn btn-add-oper"><i className="fa fa-plus"></i> Créer AH
                            </button>
                        </div>
                        {this.state.createFactureBtn && <button onClick={() => {
                            this.props.onCreateFacture(this.state.selectedData, table_state)
                        }} disabled={!this.state.createFactureBtn}
                                                                className={'btn btn-add-oper' + (!this.state.createFactureBtn ? ' disabled' : '')}>
                            <i className="fa fa-plus"></i> Créer facture</button>}
                        {this.state.createAHBtn && <button onClick={() => {
                            this.props.onCreateAH(this.state.selectedData, table_state)
                        }} disabled={!this.state.createAHBtn}
                                                           className={'btn btn-add-oper' + (!this.state.createAHBtn ? ' disabled' : '')}>
                            <i className="fa fa-plus"></i> Créer AH</button>}
                        {this.state.editBtn && <button onClick={() => {
                            this.props.onEdit(this.state.selectedData, table_state, null)
                        }} disabled={!this.state.editBtn}
                                                       className={'btn btn-add-oper' + (!this.state.editBtn ? ' disabled' : '')}>
                            <i className="fa fa-edit"></i> Éditer</button>}
                        {this.state.signBtn && <button onClick={() => {
                            this.props.onSign(this.state.selectedData)
                        }} disabled={!this.state.signBtn}
                                                       className={'btn btn-add-oper' + (!this.state.signBtn ? ' disabled' : '')}>
                            <i className="fa fa-signature"></i> Signer</button>}
                        {this.state.uploadBtn && <button onClick={() => {
                            this.props.onUpload(this.state.selectedData, this.ajax.docView)
                        }} disabled={!this.state.uploadBtn}
                                                         className={'btn btn-add-oper' + (!this.state.uploadBtn ? ' disabled' : '')}>
                            <i className="fa fa-upload"></i> Télécharger</button>}
                        <div style={{flex: "0 0 auto"}}>
                            <div style={{display: 'inline-block'}}>
                            <span className="switch_label">{this.state.isTreeTable ?
                                <i className="fas fa-sitemap"></i> : <i className="fas fa-table"></i>}</span>
                                <IOSSwitchGreen
                                    checked={this.state.isTreeTable}
                                    onChange={this.changeViewTable}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card table_tree">
                    <TreeTable
                        value={this.state.nodes}
                        lazy
                        paginator
                        totalRecords={this.state.totalRecords}
                        first={this.state.first}
                        onExpand={this.onExpand}
                        expandedKeys={this.state.expandedKeys}
                        onToggle={e => this.setState({expandedKeys: e.value})}
                        onPage={this.onPage}
                        rows={this.state.rows}
                        loading={this.state.loading}
                        selectionMode="single"
                        selectionKeys={this.state.selectedNodeKeys}
                        onSelectionChange={this.selectNode}
                        resizableColumns
                        columnResizeMode="fit"

                    >
                        <Column field="name" style={{width: 350}} header="Nom" expander></Column>
                        <Column field="file" body={file_parsesr} style={{width: 200}} header="File"></Column>
                        <Column field="signed_file" body={file_parsesr} style={{width: 200}}
                                header="Signed file"></Column>
                        <Column field="status" body={statu_parser} style={{textAlign: 'center', width: 160}}
                                header="Statut"></Column>
                        <Column field="date_create" body={date_parsesr} style={{textAlign: 'center', width: 165}}
                                header="Date de création"></Column>
                        <Column field="date_signature" body={date_parsesr} style={{textAlign: 'center', width: 165}}
                                header="Date de signature"></Column>
                    </TreeTable>


                </div>
            </div>
        )
    }
    colGroupTableRender = (table_state, date_parsesr, file_parsesr, statu_parser) => {
        const headerGroup = <ColumnGroup>
            {/*  <Row>
                                <Column style={{minWidth: 350}} header="Nom" rowSpan={2} />
                                <Column style={{borderLeft:'1px solid #e9ecef', textAlign:'center'}} header="Devis" colSpan={5} />
                                <Column style={{borderLeft:'1px solid #e9ecef', textAlign:'center'}}  header="Facture" colSpan={3} />
                                <Column style={{borderLeft:'1px solid #e9ecef', textAlign:'center'}} header="AH" colSpan={5} />
                            </Row>*/}
            <Row>
                <Column style={{minWidth: 350}} header="Nom"/>
                <Column field="devis_date_create"
                        style={{textAlign: 'center', minWidth: 90, borderLeft: '1px solid #e9ecef'}} header="Date"/>
                <Column field="devis_date_signature" style={{textAlign: 'center', minWidth: 90}} header="Signature"/>
                <Column field="devis_file" style={{minWidth: 50}} header="Devis"/>
                <Column field="devis_signed_file" style={{minWidth: 50}} header="Devis signé"/>
                <Column field="devis_edit"/>


                <Column field="facture_date_create"
                        style={{textAlign: 'center', minWidth: 90, borderLeft: '1px solid #e9ecef'}}
                        header="Date de facture"/>
                <Column field="facture_file" style={{minWidth: 50}} header="Facture"/>
                <Column field="facture_edit"/>


                <Column field="ah_date_create"
                        style={{textAlign: 'center', minWidth: 90, borderLeft: '1px solid #e9ecef'}}
                        header="Date de AH"/>
                <Column field="ah_date_signature" style={{textAlign: 'center', minWidth: 90}} header="Signature"/>
                <Column field="ah_file" style={{minWidth: 50}} header="AH"/>
                <Column field="ah_signed_file" style={{minWidth: 50}} header="AH signé"/>
                <Column field="ah_edit"/>

            </Row>
        </ColumnGroup>;
        const childParser = (key, data, column, parser) => {
            if (data[key]) {
                let column_tmp = column.field.replaceAll(key + '_', '');
                if (column_tmp == 'date_signature' && !data[key].isSigned) {
                    if (key == 'devis' && data['devis']) {
                        return (<React.Fragment>
                            <div>
                                {data.persone == 'Personne physique' && <button onClick={() => {
                                    this.props.onSign(data['devis'])
                                }} className={'btn btn_min'}>Signer <i className="fa fa-signature"></i></button>}
                            </div>
                            <div>
                                <button onClick={() => {
                                    this.props.onUpload(data['devis'], this.ajax.docView)
                                }} className={'btn btn_min'}>Télécharger <i className="fa fa-upload"></i></button>
                            </div>
                        </React.Fragment>);
                    }
                    if (key == 'AH' && data['AH']) {
                        return (<React.Fragment>
                            <div>
                                {data.persone == 'Personne physique' && <button onClick={() => {
                                    this.props.onSign(data['AH'])
                                }} className={'btn btn_min'}>Signer <i className="fa fa-signature"></i></button>}
                            </div>
                            <div>
                                <button onClick={() => {
                                    this.props.onUpload(data['AH'], this.ajax.docView)
                                }} className={'btn btn_min'}>Télécharger <i className="fa fa-upload"></i></button>
                            </div>
                        </React.Fragment>);
                    }

                }
                if (column_tmp == 'edit') {
                    let btnName = data[key].isSigned ? 'Vue' : 'Éditer';
                    if (data[key].type == 'facture') {
                        btnName = 'Éditer';
                    }
                    return (<React.Fragment>
                        <button onClick={() => {
                            this.props.onEdit(data[key], table_state, data)
                        }} className={'btn btn-add-oper btn_min'}><i className="fa fa-edit"></i>{btnName}</button>
                    </React.Fragment>);
                }
                return parser(data[key], {field: column_tmp});
            }
            if (key == 'facture' && !data[key] && data['devis'] && column.field == 'facture_date_create') {
                return (<button onClick={() => {
                    this.props.onCreateFacture(data['devis'], table_state)
                }} className={'btn btn-add-oper btn_min'}><i className="fa fa-plus"></i> Créer facture</button>);
            }
            if (key == 'AH' && !data[key] && data['facture'] && column.field == 'AH_date_create') {
                return (<button onClick={() => {
                    this.props.onCreateAH(data['facture'], table_state)
                }} className={'btn btn-add-oper btn_min'}><i className="fa fa-plus"></i> Créer AH</button>);
            }
            return '';
        };

        /*

        let table_t =  <div className="card table_tree table_tree_row">

            <DataTable
                className="p-datatable-sm"
                value={this.state.nodes}
                headerColumnGroup={headerGroup}
                lazy
                paginator
                totalRecords={this.state.totalRecords}
                first={this.state.first}
                onPage={this.onPage}
                rows={this.state.rows}
                loading={this.state.loading}
                resizableColumns
                columnResizeMode="fit"
                selectionMode="single"
                selection={this.state.selectedNodeKeys}
                onSelectionChange={(e) => {
                    this.setState({selectedNodeKeys: e.value});
                }}

            >
                <Column field="name"/>

                <Column style={{borderLeft: '1px solid #e9ecef'}} field="devis_date_create"
                        body={(data, column) => {
                            return childParser('devis', data, column, date_parsesr)
                        }}/>
                <Column style={{whiteSpace: 'nowrap'}} field="devis_date_signature" body={(data, column) => {
                    return childParser('devis', data, column, date_parsesr)
                }}/>
                <Column field="devis_file" body={(data, column) => {
                    return childParser('devis', data, column, file_parsesr)
                }}/>
                <Column field="devis_signed_file" body={(data, column) => {
                    return childParser('devis', data, column, file_parsesr)
                }}/>
                <Column style={{whiteSpace: 'nowrap'}} field="devis_edit" body={(data, column) => {
                    return childParser('devis', data, column, file_parsesr)
                }}/>

                <Column style={{borderLeft: '1px solid #e9ecef'}} field="facture_date_create"
                        body={(data, column) => {
                            return childParser('facture', data, column, date_parsesr)
                        }}/>
                <Column field="facture_file" body={(data, column) => {
                    return childParser('facture', data, column, file_parsesr)
                }}/>
                <Column style={{whiteSpace: 'nowrap'}} field="facture_edit" body={(data, column) => {
                    return childParser('facture', data, column, file_parsesr)
                }}/>


                <Column style={{borderLeft: '1px solid #e9ecef'}} field="AH_date_create"
                        body={(data, column) => {
                            return childParser('AH', data, column, date_parsesr)
                        }}/>
                <Column style={{whiteSpace: 'nowrap'}} field="AH_date_signature" body={(data, column) => {
                    return childParser('AH', data, column, date_parsesr)
                }}/>
                <Column field="AH_file" body={(data, column) => {
                    return childParser('AH', data, column, file_parsesr)
                }}/>
                <Column field="AH_signed_file" body={(data, column) => {
                    return childParser('AH', data, column, file_parsesr)
                }}/>
                <Column style={{whiteSpace: 'nowrap'}} field="AH_edit" body={(data, column) => {
                    return childParser('AH', data, column, file_parsesr)
                }}/>

            </DataTable>
        </div>;
        */

        let dataOpen = {};
        let scrollPositionY = 0;
        let countData = 0;

        const data_ntable = {
            NameModule: this.props.NameModule,
            config: this.ajax,
            date_parsesr: date_parsesr,
            file_parsesr: file_parsesr,

            isCompletedFilterTable: this.state.isCompletedFilterTable,
            isAdminViewTable: this.state.isAdminViewTable,
            searchText: this.state.searchText,
            CheckSiret: this.props.CheckSiret,
            onEdit: this.props.onEdit,
            onSign: this.props.onSign,
            onUpload: this.props.onUpload,
            onCreateDevis: this.props.onCreateDevis,
            onCreateAHFull: this.props.onCreateAHFull,
            onCreateFacture: this.props.onCreateFacture,
            onCreateAH: this.props.onCreateAH,

            onSearch: this.onSearch,


            table_state: table_state,


            scrollPositionY: 0,
            dataOpen: {},
            countData: null
        };


        if(this.state.scrollPositionY) {
            data_ntable.scrollPositionY = this.state.scrollPositionY;
        }
        if(this.state.dataOpen) {
            data_ntable.dataOpen = this.state.dataOpen;
        }
        if(this.state.countData) {
            data_ntable.countData = this.state.countData;
        }


        let table_t = <NTreeTable
            {...data_ntable}
        />;


        /*
        *
        * <div style={{flex: "0 0 auto"}}>
                            <div style={{display: 'inline-block'}}>
                            <span className="switch_label">{this.state.isTreeTable ?
                                <i className="fas fa-sitemap"></i> : <i className="fas fa-table"></i>}</span>
                                <IOSSwitch
                                    checked={this.state.isTreeTable}
                                    onChange={this.changeViewTable}/>
                            </div>
                        </div>
        * */


        let left_co;

        if(this.props.NameModule == "treetable") {

            //Делаем обход на проверку, являемся ли мы админом
            let swAdmin;
            if(this.isAdmin) {
                swAdmin = <div style={{display: 'inline-block', marginLeft: "10px"}}>
                    <span className="switch_label" style={{
                        color: "#B7B7B7"
                    }}>Personnel</span>
                    <IOSSwitchGreen
                        checked={this.state.isAdminViewTable}
                        onChange={this.onChangeIsAdminViewTable}/>
                    <span className="switch_label" style={{
                        color: "#000000"
                    }}>Tout</span>
                </div>;
            }

            left_co = <div className="left_ctrl">
                <div>
                    <button onClick={() => {
                        this.props.onCreateDevis(table_state)
                    }} className="btn btn-add-oper"><i className="fa fa-plus"/> <span className={"hide-el-mobile"}>Créer </span>devis
                    </button>
                </div>
                <div style={{flex: "1 1 auto"}}>
                    <button onClick={() => {
                        this.props.onCreateAHFull(table_state)
                    }} className="btn btn-add-oper"><i className="fa fa-plus"/> <span className={"hide-el-mobile"}>Créer </span>AH
                    </button>
                </div>
                <div style={{flex: "0 0 auto"}}>
                    <div style={{display: 'inline-block'}}>
                        <span className="switch_label hide-el-mobile" style={{
                            color: "#000000",
                            marginRight: "10px"
                        }}>Afficher les documents terminés:</span>
                        <span className="switch_label" style={{
                            color: "#B7B7B7"
                        }}>Non</span>
                        <IOSSwitchGreen
                            checked={this.state.isCompletedFilterTable}
                            onChange={this.onChangeIsCompletedFilterTable}/>
                        <span className="switch_label" style={{
                            color: "#000000"
                        }}>Oui</span>
                    </div>
                    {swAdmin}
                </div>
            </div>;
        }
        else {
        }


        return (
            <div className="table_tree_cont table_tree_row_cont">
                <div className="col-md-12 table_tree_control">
                    <div className="right_ctrl">

                        <div className="p-input-icon-left"><i className="fas fa-search"></i><DebounceInput
                            value={this.state.searchText} debounceTimeout={1500} onChange={this.onSearch} type="search"
                            placeholder="Lancer la recherche ici" className="p-inputtext p-component"/></div>
                    </div>
                    {left_co}
                </div>
                {table_t}
            </div>
        )
    }

    render() {
        let table_state = {
            ...this.state,
            nodes: [],
        };
        const date_parsesr = (data, column) => {
            if (data.data[column.field]) {
                var date_js = new Date(data.data[column.field]);
                return moment(date_js).format(DateFormat);
            }
            ;
            return '';
        }
        const file_parsesr = (data, column) => {
            if (data.data[column.field]) {
                let file = data.data[column.field];
                return (<div className="file_cont">
                    <button title={'Download ' + file.name} onClick={() => {
                        window.open('/Object/downloadFile?obj=' + data.key + '&attr=' + file.p_uid, '_blank');
                    }} type="button" className="file_download_btn"><i className="fas fa-file-download"></i></button>
                </div>);
            }
            ;
            return '';
        }
        const statu_parser = (data, column) => {
            if (data.data[column.field]) {
                let value = data.data[column.field];
                if (typeof value == 'object') {
                    if (value.create_facture) {
                        let data_node = this.getRecursiveNode(value.create_facture, this.state.nodes);
                        return (<button onClick={() => {
                            this.props.onCreateFacture(data_node, table_state)
                        }} className={'btn btn-add-oper btn_min'}><i className="fa fa-plus"></i> Créer facture
                        </button>);
                    }
                    if (value.create_ah) {
                        let data_node = this.getRecursiveNode(value.create_ah, this.state.nodes);
                        return (<button onClick={() => {
                            this.props.onCreateAH(data_node, table_state)
                        }} className={'btn btn-add-oper btn_min'}><i className="fa fa-plus"></i> Créer AH</button>);
                    }
                }
                return value;

            }
            ;
            return '';
        }
        let TableRender = this.state.isTreeTable ? this.treeTableRender(table_state, date_parsesr, file_parsesr, statu_parser) : this.colGroupTableRender(table_state, date_parsesr, file_parsesr, statu_parser);
        return (
            <React.Fragment>
                {TableRender}
            </React.Fragment>
        )
    }
}
