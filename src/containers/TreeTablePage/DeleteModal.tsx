import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";
import * as ct_api from "../../api/ClassTree-api";
import {th} from "date-fns/locale";

import TreeSelect, { TreeNode, SHOW_PARENT } from 'rc-tree-select';
import 'rc-tree-select/assets/index.css';
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import * as ot_api from "../../api/ObjTree-api";

import Select from 'react-select';
import * as ct_apiview from "../../api/View-api";


interface IBModalProps {
    name?: string,
    obj?: string,

    show:boolean;
    CloseCallback?: () => void,
    SuccessFunction?: () => void,



    ToastrStore?: any;
}
interface IBModalState {
    isAjaxDel?: boolean
}

@inject("ToastrStore")
@observer
export default class DeleteModal extends React.Component<IBModalProps,IBModalState> {
    state: Readonly<IBModalState> = {
        isAjaxDel: false,
    };



    _ref_inp_type: HTMLElement;

    constructor(props) {
        super(props);
    }

    events = {
        handleRemove: () => {
            this.setState({isAjaxDel: true});
            this.core.AJAX.removeODTEnemat(this.props.obj, (answer) => {
                this.setState({isAjaxDel: false});
                this.props.SuccessFunction();
            }, () => {
                this.setState({isAjaxDel: false});
            });
        },
        handleClose: () => {
            this.props.CloseCallback();
        },
    };

    core = {
        AJAX: {
            removeODTEnemat: (odt, SuccFunc, ErrFunc) => {
                ct_apiview
                    .removeODTEnemat(odt, "delete")
                    .then((res) => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (res) => {
                    });
            }
        },
    };

    handleCreate = () => {
    };

    onShow = () => {
    };

    helper = {

    };

    render() {
        let Title = "";
        let Body = "";
        let Footer = <div/>;
        let isBlocking = false;
        if(this.props.show) {
            Title = 'Supprimer un ' + this.props.name;
            Body = 'Etes-vous sûr que vous voulez supprimer?';

            Footer = <div>
                <button className={"nt_btn_dang"} onClick={this.events.handleRemove}>
                    Delete
                </button>
                <button className={"nt_btn_def"} onClick={this.events.handleClose}>
                    Close
                </button>
            </div>;

            if(this.state.isAjaxDel) {
                isBlocking = true;
            }
        }
        return (
            <Modal className={"nt_modal"} onShow={this.onShow} animation={true} show={this.props.show} onHide={this.events.handleClose} enforceFocus={false} >
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={isBlocking}>
                    <Modal.Header>
                        <button onClick={this.events.handleClose} type="button" className="close nt_close_btn">
                            <span aria-hidden="true">×</span>
                            <span className="sr-only">Close</span>
                        </button>
                        <div className={"nt_title"}>{Title}</div>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
    }
}