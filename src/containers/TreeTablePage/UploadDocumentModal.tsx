import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import ItemAttr from "../ObjectsPage/Interfaces/IitemAttr";

import RootStore from '../../stores/RootStore'
import ToastrStore from '../../stores/ToastrStore'

import * as f_helper from "../../helpers/form_datatype_helper";
import * as ot_api from '../../api/ObjTree-api'

interface IUploadDocumentModalState {
    blocking: boolean,
    attrData: ItemAttr[],
    attrDataCache: any,
    attrDataFileCache: any,
}

interface IUploadDocumentModalProps {
    ToastrStore?: any,
    show: boolean,
    title: string,
    attrs: object[],
    object: string,
    parent: string,
    class: string,
    onClose?: () => any,
    onShow?: () => any,
    onUpload?: () => any,
}


export default class UploadDocumentModal extends React.Component<IUploadDocumentModalProps, IUploadDocumentModalState> {
    constructor(props) {
        super(props);

        this.state = {
            blocking: false,
            attrData: [],
            attrDataCache: {},
            attrDataFileCache: {},
        };
    }

    componentWillReceiveProps(nextProps) {
        let attrsData = [];
        for (let i = 0; i < nextProps.attrs.length; i++) {
          attrsData.push(nextProps.attrs[i].attr);
        }
        this.setState({ attrData: attrsData });
    }

    ajax = {
        saveObject: (uid:string, type:string, parent:string, values: ot_api.IValue[], SuccFunc, ErrFunc) => {
            ot_api
                .saveObject(uid, type, parent, values, true)
                .then(res => {
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                        ErrFunc(res.data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    ErrFunc({result: 0});
                })
        },
        addFile: (uid:string, type:string, parent:string, Files:any, values: ot_api.IValueFile[], SuccFunc, ErrorFunc) => {
            ot_api
                .addFile(uid, type, parent, Files, values, (progressEvent) => {
                    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    RootStore.ProgressData.completed = percentCompleted;
                })
                .then(res => {
                    SuccFunc(res.data);
                })
                .catch(function (error) {
                    console.log(error);
                    ErrorFunc();
                })
        },
    }

    saveObject = () => {
        let values = [];
        let Name_obj = "";
        let ValidErrorArr = [];

        this.state.attrData.forEach( (item, index) => {
            let _item = {
                value: item.value ? item.value.value : item.value,
                attr: item.uid,
                key: item.isKey ? 1 : 0,
                datatype: item.datatype,
            };

            if(item.datatype != "counter") {
                if(item.datatype == "object") {
                    _item.value = item.value &&  item.value.uid ? item.value.uid : "";
                }

                if(this.state.attrDataCache[item.uid] != undefined) {
                    _item.value = this.state.attrDataCache[item.uid].value;

                    values.push(_item);
                }
                else if(item.isKey) {
                    values.push(_item);
                }

                if(item.isKey) {
                    Name_obj += (_item.value === null ? '' : _item.value) + ' ';
                }

                if(item.require && !_item.value) {
                    if(item.datatype == "file") {
                        if(
                            (this.state.attrDataFileCache[item.uid] == undefined || this.state.attrDataFileCache[item.uid].files.length == 0) && !item.value  ) {
                            ValidErrorArr.push({name: item.name, text: "Enter value!"})
                        }
                    }
                    else {
                        ValidErrorArr.push({name: item.name, text: "Enter value!"})
                    }
                }
            }
        });

        if(ValidErrorArr.length > 0) {
            ToastrStore.clear();
            ValidErrorArr.forEach((item_err) => {
                ToastrStore.error("parameter|" + item_err.name + "| " + item_err.text);
            });
        }
        else {
            this.setState({blocking: true});
            if(Name_obj) {
                Name_obj = Name_obj.slice(0, -1)
            }
            this.ajax.saveObject(this.props.object, this.props.class, this.props.parent, values, (data) => {
                ToastrStore.success('Success!', 'Success!');
                this.setState({blocking: false});

                this.saveCache();

                let new_Name = data.objName;

                let Files = [];
                let values:ot_api.IValueFile[] = [];
                let iter = 0;
                if(Object.keys(this.state.attrDataFileCache).length > 0) {
                    RootStore.ProgressIsOpen = true;
                    for( let key_FileCache in this.state.attrDataFileCache) {
                        let File_o = this.state.attrDataFileCache[key_FileCache];
                        let Files_tmp = File_o.files;
                        for( let i = 0; i < Files_tmp.length; i++ ) {
                            Files.push(Files_tmp[i]);
                            values.push({
                                uid: File_o.data.uid, //uid-параметра
                                new: 1, //0/1
                                key: Number(File_o.data.isKey), //0/1
                                value: iter,
                                del: 0,
                            });
                            iter ++;
                        }
                    }
                    this.ajax.addFile(this.props.object, this.props.class,
                        this.props.parent, Files, values,
                        () => {
                            this.setState({ attrDataFileCache: {} });
                            RootStore.ProgressIsOpen = false;
                            this.props.onUpload();
                        }, () => {
                            RootStore.ProgressIsOpen = false;
                        });
                } else {
                    this.props.onUpload();
                }

            }, (data) => {
                ToastrStore.error('Error!', 'ERROR CODE='+data.result);
                this.setState({blocking: false});
            });
        }
    }

    saveCache = () => {
        this.state.attrData.forEach( (item, index) => {
            if(this.state.attrDataCache[item.uid] != undefined) {
                if(!item.value) {
                    item.value = {};
                }
                if (item.isArray) {
                    if (item.datatype == 'object') {
                        item.value = this.state.attrDataCache[item.uid].value.map((v) => {
                            return { uid: v.value, name: v.name };
                        });
                    } else {
                        item.value = this.state.attrDataCache[item.uid].value
                    }
                } else {
                    item.value.value = this.state.attrDataCache[item.uid].value
                }
            }
        });

        this.setState({attrDataCache: {}});
    }

	render() {
        let controls = [];
        this.props.attrs.forEach( (item, index) => {
            const paramsControl:f_helper.IFormDatatypeClass = {
                item: item.attr,
                key: index,
                attrData: this.state.attrData,
                attrDataCache: this.state.attrDataCache,
                attrDataFileCache: this.state.attrDataFileCache,
                type: "edit" as ("add" | "edit" | "read"),
                ObjID: item.objId,
                TypeID: item.typeId,

                ChangeStateFunc: () => { this.setState({}) },
            };
            let itemControl = new f_helper.FormDatatypeClass(paramsControl);
            controls.push(itemControl.GetControl());
        });
		const Footer = (
			<div>
                <Button variant="light" className="nf_btn-ellipse" onClick={() => {this.saveObject()}}>
                    Upload
                </Button>
                <Button variant="light" className="nf_btn-ellipse" onClick={this.props.onClose}>
                    <i className="fas fa-times"></i> Close
                </Button>
            </div>
		);
        return (
            <Modal onShow={this.props.onShow} animation={true} show={this.props.show} onHide={this.props.onClose}>
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{controls}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
	}
}