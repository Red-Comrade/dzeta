import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";

interface ISignDocumentModalState {
    blocking: boolean,
    filename: string,
}

interface ISignDocumentModalProps {
    ToastrStore?: any,
    show: boolean,
    onClose?: () => any,
    onShow?: () => any,
    onLoad?: () => any,
}

@inject("ToastrStore")
@observer
export default class SignDocumentModal extends React.Component<ISignDocumentModalProps, ISignDocumentModalState> {
    constructor(props) {
        super(props);
        this.state = {
            blocking: false,
            filename: null,
        };
    }

	render() {
		const Footer = (
			<div>
                <Button variant="light" className="nf_btn-ellipse" onClick={this.props.onLoad}>
                    Choose
                </Button>
                <Button variant="light" className="nf_btn-ellipse" onClick={this.props.onClose}>
                    <i className="fas fa-times"></i> Close
                </Button>
            </div>
		);
        let _ref_file;
		const Body = (
			<div className="form-group zt_form_control" key="stamp-file">
                <div className="hover-label-parent">
                    <div className="hover-label_wrap1">
                        <label className="hover-label">File</label>
                    </div>
                </div>
                <div className="zt_input-group">
                    <div className="form-control file_control">
                        <span className="file_span" title={this.state.filename} ><i className="fas fa-file-alt"/> {this.state.filename}</span>
                    </div>
                    <input ref={ref => (_ref_file = ref)} className="file_input" type="file" accept="image/*"
                    	onChange={
                    		(e) => {
                                this.setState({
                                    filename: e.target.files[0] ? e.target.files[0].name : null
                                });
                                this.props.onChoose(e.target.files.length ? e.target.files[0] : null)
                    		}
                    	}
                    />
                    <div className="file_btn_control">
                        <button type="button" title="upload" className="file_btns nf_btn-ellipse"
                            onClick={
                            	(e) => {
                                    _ref_file.click();
                                }
                            }
                        >
                        	<i className="fas fa-file-upload"/>
                        </button>
                        <button type="button" title="remove" className="file_btns nf_btn-ellipse" onClick={() => { this.props.onRemove() }}><i className="fas fa-times"/></button>
                    </div>
                </div>
            </div>
		);
        return (
            <Modal onShow={this.props.onShow} animation={true} show={this.props.show} onHide={this.props.onClose}>
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>Choose stamp image</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{Body}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
	}
}