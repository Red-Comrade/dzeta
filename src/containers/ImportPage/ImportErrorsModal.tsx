import * as React from "react";
import { Modal, Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import {inject, observer} from "mobx-react";

import CoolTable from '../../components/CoolTable'

interface IImportErrorsModalState {
    blocking: boolean
}

interface IImportErrorsModalProps {
    ToastrStore?: any,
    show: boolean,
    onClose?: () => any,
    onShow?: () => any,
    onLoad?: () => any,
}

@inject("ToastrStore")
@observer
export default class ImportErrorsModal extends React.Component<IImportErrorsModalProps, IImportErrorsModalState> {
    constructor(props) {
        super(props);
        this.state = {
            blocking: false,
            errors: [],
        };
    }

    renderTable = () => {
        if (this.state.errors.length) {
            const columns = [
                {
                    key: 'row',
                    IsFixedWidth: true,
                    MinWidth: 75,
                    name: 'Row',
                },
                {
                    key: 'error',
                    IsFixedWidth: false,
                    MinWidth: 500,
                    name: 'Error',
                }
            ];
            const data = [];
            for (let i = 0; i < this.state.errors.length; i++) {
                const tds = [];
                tds.push(this.state.errors[i].n);
                if (this.state.errors[i].attr_name) {
                    tds.push(this.state.errors[i].info + ' "' + this.state.errors[i].attr_name + '"');
                } else {
                    tds.push(this.state.errors[i].info);
                }
                data.push({
                    trKey: i,
                    trCustomData: this.state.errors[i],
                    tds: tds
                });
            }
            const tableData = {
                settings: {
                    data: data,
                },
                headerData: columns,
                callbackRenderTH: (dataTH) => {
                    return dataTH.name;
                },
            };
            return (
                <CoolTable {...tableData} />
            );
        }
        return null;
    };

	render() {
		const Footer = (
			<div>
                <Button variant="light" className="nf_btn-ellipse" onClick={this.props.onClose}>
                    <i className="fas fa-times"></i> Close
                </Button>
            </div>
		);
        const modalStyle = {
            overflow: 'hidden',
            height: 'calc(100vh - 200px)',
        };
        return (
            <Modal
                onEntered={
                    () => {
                        this.setState({
                            errors: this.props.errors
                        });
                    }
                }
                onExiting={
                    (e) => {
                        this.setState({
                            errors: []
                        });
                        this.props.onClose(e)
                    }
                }
                size="xl" animation={true} show={this.props.show}
            >
                <BlockUi tag="div" loader={<Loader active type="ball-pulse"/>} blocking={this.state.blocking}>
                    <Modal.Header closeButton>
                        <Modal.Title>Import errors</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={modalStyle}>{this.renderTable()}</Modal.Body>
                    <Modal.Footer>{Footer}</Modal.Footer>
                </BlockUi>
            </Modal>
        );
	}
}