import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import { Button } from 'react-bootstrap';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import { withTranslation } from 'react-i18next';
import Select, { components } from 'react-select';

import './ImportPage.scss';

import { getExcelData, runImport } from '../../api/Import-api';
import { getFullAttrForApp as getAllAttributes } from '../../api/AppTree-api';
import { generateUid } from '../../utils';

import CoolTable from '../../components/CoolTable'
import ImportErrorsModal from './ImportErrorsModal';
import ProgressModal from '../ProgressModal';
import RootStore from "../../stores/RootStore";

interface IImportPageState {
    blocking: boolean,
    showProgressModal: boolean,
    showErrorsModal: boolean,
    results?: object,
    errors: object[],
    params: {
		uid: string,
		name: string,
		class: {
			uid: string,
			name: string
		},
	}[],
    loadingParams: boolean,
    isLoadParams: boolean,
    importUid?: string,
    recalcStyleFunc?: void
}

interface IImportPageProps {
    ToastrStore: any,
    ImportStore: any,
    parent?: string,
    class?: string,
}

@inject("ImportStore")
@inject("ToastrStore")
@observer
class ImportPageForm extends React.Component<IImportPageProps, IImportPageState> {
	resizeObserver = null;
	resizeElement = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            blocking: false,
            showProgressModal: false,
            showErrorsModal: false,
			results: null,
			errors: [],
			params: [],
			loadingParams: false,
			isLoadParams: false,
			importUid: null,
        };
    }

    componentDidMount(): void {
    	const { t } = this.props;
    	if (!this.props.class && !this.props.parent) {
			document.title = t("init:general.entities.import") + ' | ' + RootStore.config.nameSystem;
    	}
    	this.resizeObserver = new ResizeObserver((entries) => {
    		window.requestAnimationFrame(() => {
                if (!Array.isArray(entries) || !entries.length) {
                    return;
                }
                console.log('resize');
		      	this.setState({
	                recalcStyleFunc: () => {
	                    this.setState({ recalcStyleFunc: null });
	                }
	            });
            });
	    });
	    this.resizeObserver.observe(this.resizeElement.current);
		getAllAttributes()
			.then((response) => {
				this.setState({
					loadingParams: false
				});
				return response.data;
			})
			.then((data) => {
				if (data.result == 1) {
					this.setState({
						params: data.data,
						isLoadParams: true
					});
				}
			});
	}

	componentWillUnmount() {
    	if (this.resizeObserver) {
      		this.resizeObserver.disconnect();
    	}
  	}

	importHandler = () => {
		const uid = generateUid();
		this.setState({
			blocking: true,
			showProgressModal: true,
			importUid: uid,
			results: null,
			errors: []
		});
		runImport(uid, this.props.ImportStore.file, this.props.class, this.props.ImportStore.settings, this.props.parent, this.props.ImportStore.rewrite)
			.then((response) => {
				console.log(response);
				this.setState({
					blocking: false,
				});
				return response.data;
			})
			.then((data) => {
				if (data.results.errors.length) {
					this.setState({
						showErrorsModal: true,
						showProgressModal: false,
						errors: data.results.errors,
					});
				} else {
					this.setState({
						results: data.results.result
					})
				}
			});
	};

	selectParam = (column, attribute) => {
		this.props.ImportStore.updateColumnAttribute(column, attribute);
	}

	renderToolbar = () => {
		const { t } = this.props;
        let _ref_file;
		return 	(
			<div className="pp_tollbar_tools" ref={this.resizeElement} style={{padding: "10px", display: 'flex'}}>
				<div className="form-group zt_form_control" key="import-file">
	                <div className="zt_input-group">
	                    <div className="form-control file_control">
	                        <span className="file_span" title={this.props.ImportStore.filename} ><i className="fas fa-file-alt"/> {this.props.ImportStore.filename}</span>
	                    </div>
	                    <input ref={ref => (_ref_file = ref)} className="file_input" type="file" accept=".xls, .xlsx, .xlsm"
	                    	onChange={
	                    		(e) => {
	                                this.props.ImportStore.updateFile(e.target.files.length ? e.target.files[0] : null);
	                                this.props.ImportStore.updateDataAndSettings([], []);
	                                if (this.props.ImportStore.file) {
	                                	getExcelData(this.props.ImportStore.file)
											.then((data) => {
						            			if (data.result != 1) {
						            				this.props.ToastrStore.error(t('errors.' + data.result), 'Error!');
						            				this.props.ImportStore.updateDataAndSettings([], []);
												} else {
													this.props.ImportStore.updateDataAndSettings(data.data.columns, data.data.sheet);
												}
						            		});
	                                }
	                    		}
	                    	}
	                    />
	                    <div className="file_btn_control">
	                        <button type="button" title={t('init:general.operations.choose')} className="file_btns nf_btn-ellipse"
	                            onClick={
	                            	(e) => {
	                                    _ref_file.click();
	                                }
	                            }
	                        >
	                        	<i className="fas fa-file-upload"/>
	                        </button>
	                    </div>
	                </div>
	            </div>
				<div className="form-group zt_form_control" key="import-rewrite" style={{ width: '160px' }}>
                    <select
                    	className="form-control"
                    	value={this.props.ImportStore.rewrite}
                    	onChange={(e) => this.props.ImportStore.updateRewrite(e.target.value)}
                    >
                    	<option value="0">{t("import:labels.rewrite.not")}</option>
                    	<option value="1">{t("import:labels.rewrite.empty")}</option>
                    	<option value="2">{t("import:labels.rewrite.always")}</option>
                    </select>
	            </div>
				<div className="form-group zt_form_control" style={{width: 'auto'}}>
					<button onClick={this.importHandler} disabled={this.props.ImportStore.file ? false : true}  title={t('import:labels.run_import')} className="nf_btn-ellipse">
						<i className="fa fa-file-import"></i> {t('import:labels.import')}
					</button>
				</div>
			</div>
		);
	};

	renderTable = () => {
		const { t } = this.props;
		if (this.props.ImportStore.settings.length) {
			const columns = [];
			const paramsRow = { trKey: -1, trCustomData: { selectParam: true }, tds: [] };
			for (let i = 0; i < this.props.ImportStore.settings.length; i++) {
				columns.push({
					key: this.props.ImportStore.settings[i].column,
					IsFixedWidth: false,
					MinWidth: 100,
					name: this.props.ImportStore.settings[i].column
				});
				paramsRow.tds.push({ name: '', column: this.props.ImportStore.settings[i].column });
			}
			const data = [];
			data.push(paramsRow);
			for (let i = 0; i < this.props.ImportStore.data.length; i++) {
				const tds = [];
				for (let j = 0; j < this.props.ImportStore.data[i].length; j++) {
					tds.push({ name: this.props.ImportStore.data[i][j] })
				}
				data.push({
                    trKey: i,
                    trCustomData: this.props.ImportStore.data[i],
                    tds: tds
                });
			}
			const tableData = {
	            settings: {
	                data: data,
	            },
	            headerData: columns,
	            callbackRenderTD: (dataTD, dataTR) => {
	            	if (dataTR.trCustomData.selectParam) {
	            		const options = [{key: "none", value: "", label: t("init:general.labels.not_selected")}];
						this.state.params.map((param) => {
							let includeParam = true;
							if (this.props.class && this.props.class != param.class.uid) {
								includeParam = false;
							}
							if (includeParam) {
								includeParam = param.datatype == "counter" ? false : true;
							}
							if (includeParam) {
								includeParam = this.props.ImportStore.selectedAttributes.indexOf(param.uid) !== -1 && this.props.ImportStore.columnAttribute(dataTD.column) != param.uid ? false : true;
							}
							if (includeParam) {
								options.push({ key: param.uid, value: param.uid, label: param.name, class: param.class.name });
							}
						});

				        const SingleValue = props => (
						  	<components.SingleValue {...props}>
						    	{props.data.label + (props.data.class ? ' (' + props.data.class + ')' : '')}
						  	</components.SingleValue>
						);

						const customStyles = {
						  	control: base => ({
						    	...base,
						    	height: 30,
						    	minHeight: 30
						  	}),
						  	valueContainer: (base) => ({
						      	...base,
						      	height: 30,
						      	padding: '0 6px'
						    }),
						    input: (base) => ({
						      	...base,
						      	margin: '0px',
						      	marginBottom: '3px',
						    }),
						    placeholder: (base) => ({
						    	...base,
						    	position: 'relative',
						    	top: 'unset',
						    	transform: 'none',
						    	display: 'flex',
						    	marginBottom: '3px',
						    }),
						    indicatorsContainer: (base) => ({
						      	...base,
						      	height: 30,
						    }),
						    singleValue: (base) => ({
						    	...base,
						    	position: 'relative',
						    	top: 'unset',
						    	transform: 'none',
						    	display: 'flex',
						    	marginBottom: '3px',
						    }),
						    menuPortal: (base) => ({
						    	...base,
						    	zIndex: 1751
						    })
						};

				        return <Select
				                menuPortalTarget={document.querySelector("body")}
				                options={options}

				                isLoading={this.state.loadingParams}
				                placeholder={t("import:labels.select_attribute")}
				                onChange={(option) => this.selectParam(dataTD.column, option.value)}
				                filterOption={
				                	(option, inputValue) => {
				                		let includeParam = true;
				                		if (inputValue != '') {
				                			const paramName = option.data.label.toLowerCase();
				                			const className = option.data.class ? ' ' + option.data.class.toLowerCase() : '';
											includeParam = (paramName + className).indexOf(inputValue.toLowerCase()) !== -1 ? true : false;
										}
										return includeParam;
				                	}
				                }

				                className={"zt_select_control"}
				                classNamePrefix="react-select"

				                formatOptionLabel={
				                	(option) => {
				                		return (
				                			<span title={option.label + ' (' + option.class + ')'} style={{ width: '100%', display: 'flex'}}>
				                				<span className="param-name">{option.label}</span>
												<span className="class-name">{option.class}</span>
				                			</span>
				                		)
				                	}
				            	}

				            	components={{ SingleValue }}
				            	styles={customStyles}

				            	getOptionLabel={
				            		(option) => {
				            			return option.label + (option.class ? ' (' + option.class + ')' : '')
				            		}
				            	}
				            />;
	            	} else {
		                return dataTD.name;
	            	}
	            },
	            callbackRenderTH: (dataTH) => {
	                return dataTH.name;
	            },
                RecalcStyleFunc: this.state.recalcStyleFunc,
	        };
	        return (
	        	<CoolTable {...tableData} />
	        );
		}
		return null;
	};

	render() {
		const { t } = this.props;
    	const loader = <h4 className="loader-text">Load file...</h4>;
    	const attrs = this.props.ImportStore.selectedAttributes;
    	let resultsBody = null;
    	let resultsFooter = null;
    	if (this.state.results) {
    		resultsBody = (
    			<div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
    				<div style={{ alignSelf: 'center' }}>{t('import:labels.objects_imported')}: {this.state.results.created}</div>
    				<div style={{ alignSelf: 'center' }}>{t('import:labels.objects_updated')}: {this.state.results.updated}</div>
    			</div>
    		);
    		resultsFooter = (
    			<Button variant="light" className="nf_btn-ellipse"
    				onClick={
    					() => {
    						this.setState({ showProgressModal: false })
    					}
    				}
    			>
                    {t('init:general.operations.close')}
                </Button>
    		);
    	}

		return (
	        <BlockUi id="zt_ImportPage" tag="div" loader={loader} blocking={this.state.loadingParams}>
	        	{this.renderToolbar()}
	            {this.renderTable()}
	            <ImportErrorsModal
	            	show={this.state.showErrorsModal}
	            	errors={this.state.errors}
	            	onClose={() => { this.setState({ showErrorsModal: false }) }}
	            />
	            <ProgressModal
	            	show={this.state.showProgressModal}
	            	title={t('import:labels.import')}
	            	uid={this.state.importUid}
	            	interval={1000}
	            	body={resultsBody}
	            	footer={resultsFooter}
	            />
	        </BlockUi>
		);
	}
}

const ImportPageTranslation = withTranslation(['init', 'import'])(ImportPageForm);

export function ImportPage(props) {
  return (
    <React.Suspense fallback={<Loader active type="ball-pulse"/>}>
      <ImportPageTranslation {...props} />
    </React.Suspense>
  );
}