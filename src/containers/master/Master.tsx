import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom'

import {Header} from "./Header";
import {IMenuSidebarProps, MenuSidebar} from "./MenuSidebar";
import {IFilterSidebarProps, FilterSidebar} from "./FilterSidebar";
import {IMSidebarProps, MSidebar} from "./MSidebar";
import {ProgressPanel} from "./ProgressPanel";
import {GalleryPanel} from "./GalleryPanel";

/*
import {ClassesPage} from "../ClassesPage/ClassesPage";
import {ObjectsPage} from "../ObjectsPage/ObjectsPage";
import {ApplicationPage} from '../ApplicationPage/ApplicationPage';
import {HomePage} from '../HomePage/HomePage';
import {ViewPage} from '../ViewPage/ViewPage';
import {UserStepPage} from '../UserStepPage/UserStepPage'
import {TaxesPage} from '../TaxesPage/TaxesPage'
import {ImportPage} from '../ImportPage/ImportPage'
import {UserStepSettPage} from '../UserStepSettPage/UserStepSettPage'
import {TreeTablePage} from '../TreeTablePage/TreeTablePage'
import {LoginPage} from "../LoginPage/LoginPage";

//*/
import {inject, observer} from "mobx-react";

import * as r_api from '../../api/Root-api'
import * as h_api from "../../api/Home-api";
import {FormsPanel} from "./FormsPanel";
import Loader from 'react-loaders';



const LoginFormPage = React.lazy(() =>
    import('../LoginPage/LoginFormPage')
        .then(({ LoginFormPage }) => ({ default: LoginFormPage })),
);
const RegistrationPage = React.lazy(() =>
    import('../LoginPage/RegistrationPage')
        .then(({ RegistrationPage }) => ({ default: RegistrationPage })),
);
const LoginPage = React.lazy(() =>
    import('../LoginPage/LoginPage')
        .then(({ LoginPage }) => ({ default: LoginPage })),
);
const ClassesPage = React.lazy(() =>
    import('../ClassesPage/ClassesPage')
        .then(({ ClassesPage }) => ({ default: ClassesPage })),
);
const ObjectsPage = React.lazy(() =>
    import('../ObjectsPage/ObjectsPage').then(({ ObjectsPage }) => ({ default: ObjectsPage })),
);
const ApplicationPage = React.lazy(() =>
    import('../ApplicationPage/ApplicationPage').then(({ ApplicationPage }) => ({ default: ApplicationPage })),
);
const HomePage = React.lazy(() =>
    import('../HomePage/HomePage').then(({ HomePage }) => ({ default: HomePage })),
);
const ViewPage = React.lazy(() =>
    import('../ViewPage/ViewPage').then(({ ViewPage }) => ({ default: ViewPage })),
);
const UserStepPage = React.lazy(() =>
    import('../UserStepPage/UserStepPage').then(({ UserStepPage }) => ({ default: UserStepPage })),
);
const TaxesPage = React.lazy(() =>
    import('../TaxesPage/TaxesPage').then(({ TaxesPage }) => ({ default: TaxesPage })),
);
const ImportPage = React.lazy(() =>
    import('../ImportPage/ImportPage').then(({ ImportPage }) => ({ default: ImportPage })),
);
const UserStepSettPage = React.lazy(() =>
    import('../UserStepSettPage/UserStepSettPage').then(({ UserStepSettPage }) => ({ default: UserStepSettPage })),
);
const TreeTablePage = React.lazy(() =>
    import('../TreeTablePage/TreeTablePage').then(({ TreeTablePage }) => ({ default: TreeTablePage })),
);


/*
const ClassesPage = loadable(
    () => import('../ClassesPage/ClassesPage'),
);
const ObjectsPage = loadable(
    () => import('../ObjectsPage/ObjectsPage'),
);
const ApplicationPage = loadable(
    () => import('../ApplicationPage/ApplicationPage'),
);
const HomePage = loadable(
    () => import('../HomePage/HomePage'),
);
const ViewPage = loadable(
    () => import('../ViewPage/ViewPage'),
);
const UserStepPage = loadable(
    () => import('../UserStepPage/UserStepPage'),
);
const TaxesPage = loadable(
    () => import('../TaxesPage/TaxesPage'),
);
const ImportPage = loadable(
    () => import('../ImportPage/ImportPage'),
);
const UserStepSettPage = loadable(
    () => import('../UserStepSettPage/UserStepSettPage'),
);
const TreeTablePage = loadable(
    () => import('../TreeTablePage/TreeTablePage'),
);
const LoginPage = loadable(
    () => import('../LoginPage/LoginTest'),
);
*/

export interface MasterProps {
    compiler: string;
    framework: string;
    ToastrStore?: any,
    RootStore?: {
        isLoadMSettings: boolean,
        IsResized?: boolean,
        Settings: {
            HoverMenuSidebar: boolean,
            HoverFilterSidebar: boolean,

            isOpenMenuSidebar?: boolean;
            isOpenMenuSidebarMin?: boolean;
            isOpenFilterSidebar?: boolean;

            RightMenuPanel: boolean,

            WidthFilterSidebar: number,
        }
        saveSettings?: () => void,
        MSettings: any,
        Applications: any,
        modules: string[],
        home_url: string,
        TreeTableUpdate:string,
        config?: any,
    },
}

interface IMasterState {
    isOpenMenuSidebarMobile?: boolean;
    isOpenFilterSidebarMobile?: boolean;

    isOpenMSidebar: boolean;
    TypeMSidebar: string,

    width: number;
    is_desktop: boolean;

    statesClass?: {
        name?: string,
        value?: string
    };
}

export const TYPES = () => <div>I am TYPES page</div>;
export const NoMatch = () => <div>Sorry, No Such PAGE!!!!</div>;

//Если вдруг захотим при загрузке содержимого показывать анимацию загрузки
function FallbackLoad(props) {
    React.useEffect(() => {
        let is_token = document.getElementById('spinner-splash-screen').style.visibility == "hidden";
        let is_need_fade = false;
        let is_unfading = false;


        let splash_screen = document.getElementById('splash-screen');
        function _fade(element) {
            var op = 1;  // initial opacity
            var timer = setInterval(function () {
                if (op <= 0.1){
                    clearInterval(timer);
                    element.style.display = 'none';
                }
                element.style.opacity = op;
                element.style.filter = 'alpha(opacity=' + op * 100 + ")";
                op -= op * 0.1;
            }, 10);
        }
        function _unfade(element) {
            var op = 0.1;  // initial opacity
            element.style.display = 'flex';
            var timer = setInterval(function () {
                if (op >= 1){
                    clearInterval(timer);

                    if(is_need_fade) {
                        is_need_fade = false;
                        is_unfading = false;
                        fade();
                    }
                }
                element.style.opacity = op;
                element.style.filter = 'alpha(opacity=' + op * 100 + ")";
                op += op * 0.1;
            }, 10);
        }


        function fade() {
            if(is_token) {
                if(is_unfading) {
                    is_need_fade = true;
                }
                else {
                    document.getElementById('spinner-splash-screen').style.visibility = 'hidden';
                    setTimeout(() => {
                        _fade(splash_screen);
                    }, 150)
                }
            }
        }
        function unfade() {
            if(is_token) {
                is_unfading = true;
                document.getElementById('spinner-splash-screen').style.visibility = 'visible';
                setTimeout(() => {
                    _unfade(splash_screen);
                }, 150)
            }
        }

        console.log('mount');
        unfade();

        return () => {
            console.log('un mount');
            fade();
        };
    }, []);
    return <div>Loading...</div>;
}

@inject("ToastrStore")
@inject("RootStore")
@observer
export class Master extends React.Component<MasterProps, IMasterState> {
    state: Readonly<IMasterState> = {
        isOpenMSidebar: false,
        TypeMSidebar: "",
        width: window.innerWidth,
        is_desktop: window.innerWidth > 800,
        isOpenFilterSidebarMobile: false,
        isOpenMenuSidebarMobile: false,
        statesClass: {
            value: "",
            name: ""
        }
    };

    _ref_MenuSidebar_wrap: HTMLElement;
    _ref_FilterSidebar_wrap: HTMLElement;

    _ref: HTMLElement;

    isShowSplashScreen = false;

    custom_style_str: string = "";

    constructor(props) {
        super(props);
    }
    componentWillMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }
    componentDidMount(): void {
        if(this._ref_MenuSidebar_wrap) {
            this._ref_MenuSidebar_wrap.addEventListener('transitionend', this.transitionedFuncMenuSidebar, false);
        }

        this.props.RootStore.isLoadMSettings = false;

        if(document.location.pathname != "/login") {
            this.core.GetMainInfo();

        }
        else {
            this.props.RootStore.isLoadMSettings = true;
        }
    }

    componentDidUpdate(prevProps: Readonly<MasterProps>, prevState: Readonly<IMasterState>, snapshot?: any): void {
        if(!this.props.RootStore.IsResized) {
            this.props.RootStore.IsResized = true;

            this.handleWindowSizeChange();
            document.dispatchEvent(new CustomEvent("zt_resize", {detail: {}}));
            console.log('resized');
        }

        if(this.props.RootStore.isLoadMSettings && !this.isShowSplashScreen) {
            this.isShowSplashScreen = true;
            this.core.HideSplashScreen();
        }
    }



    core = {
        HideSplashScreen: () => {
            //#region SplashScreen
            let splashScreenInit = function()  {
                function fade(element) {
                    var op = 1;  // initial opacity
                    var timer = setInterval(function () {
                        if (op <= 0.1){
                            clearInterval(timer);
                            element.style.display = 'none';
                        }
                        element.style.opacity = op;
                        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
                        op -= op * 0.1;
                    }, 10);
                }
                function unfade(element) {
                    var op = 0.1;  // initial opacity
                    element.style.display = 'block';
                    var timer = setInterval(function () {
                        if (op >= 1){
                            clearInterval(timer);
                        }
                        element.style.opacity = op;
                        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
                        op += op * 0.1;
                    }, 10);
                }
                let splash_screen = document.getElementById('splash-screen'); //.classList.add("hidden")

                setTimeout(() => {
                    fade(splash_screen);
                }, 150)

            }();
            //#endregion
        },
        GetMainInfo: () => {
            r_api
                .getMainInfo()
                .then(res => {
                    if(res.data.result == 1) {


                        this.props.RootStore.MSettings.UserObjData = res.data.UserObjData;
                        this.props.RootStore.MSettings.UserName = res.data.nameUser;
                        this.props.RootStore.MSettings.UserGroups = res.data.UserGroups;
                        this.props.RootStore.MSettings.UserUID = res.data.data.userUID;
                        this.props.RootStore.modules = res.data.data.module;
                        let home_url = '/home';
                        if(this.props.RootStore.modules.indexOf('home') === -1) {
                            if(this.props.RootStore.modules.length > 0) {
                                let nameModule = this.props.RootStore.modules[0];
                                if(nameModule == "classes") {
                                    home_url = "/classes"
                                }
                                else if(nameModule == "objects") {
                                    home_url = "/objects"
                                }
                                else if(nameModule == "users") {
                                    home_url = "/users"
                                }
                                else if(nameModule == "settApp") {
                                    home_url = "/application"
                                }
                                else if(nameModule == "import") {
                                    home_url = "/import"
                                }
                                else if(nameModule == "stepFromEnemat") {
                                    home_url = "/stepform"
                                } else if(nameModule == 'taxesEnemat') {
                                    home_url = "/taxes"
                                }
                                else {
                                    home_url = '/'+nameModule
                                }
                            }
                            else {
                                home_url = '/'
                            }
                        }
                        this.props.RootStore.home_url = home_url;

                        if(document.location.host == "form.enemat.fr" || document.location.host == "enemat.misnik.by") {
                            this.props.RootStore.home_url = '/treetable';
                        }

                        this.core.GetAppAjax( () => {
                            this.props.RootStore.isLoadMSettings = true;
                        });
                    }
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                })
        },

        GetAppAjax: (SuccFunc) => {
            h_api.getHomeApp()
                .then(res => {
                    let Applications = [];
                    if(res.data[0] != undefined && res.data[0].children.length > 0) {
                        res.data[0].children.forEach( (item) => {
                            if(item.Read) {
                                Applications.push(item);
                            }
                        });
                    }
                    this.props.RootStore.Applications = Applications;
                    SuccFunc();
                })
                .catch( (error) => {
                    console.log(error);
                    this.props.ToastrStore.error('Server error!', 'Error');
                });
        },

        recalcStyle: () => {
            let styleID = "zt_main_style1";
            let css = '',
                style = document.getElementById(styleID);

            if(this.state.is_desktop) {
                if(this.props.RootStore.Settings.WidthFilterSidebar) {
                    if(this.props.RootStore.Settings.RightMenuPanel) {
                        css += '.desktop-dis.RightMenuSidebar.OpenFilterSidebar #zt_ContainerPage_wrapper { padding-left:'+this.props.RootStore.Settings.WidthFilterSidebar+'px!important; } ';
                    }
                    else {
                        css += '.desktop-dis.LeftMenuSidebar.OpenFilterSidebar #zt_ContainerPage_wrapper { padding-right:'+this.props.RootStore.Settings.WidthFilterSidebar+'px!important; } ';
                    }
                }
            }

            if(!style) {
                let
                    head = document.head || document.getElementsByTagName('head')[0];
                style = document.createElement('style');
                head.appendChild(style);

                style.setAttribute('type', 'text/css');
                style.setAttribute('id', styleID);
            }

            if(this.custom_style_str != css) {
                this.custom_style_str = css;
                style.innerHTML = css;
            }
        },
    };

    handleWindowSizeChange = () => {
        //Запрещаем смену состояния по ресайзу, если мы на панели логина, иначе у нас снимается фокус с элементов при смене состояния
        if(document.location.pathname != "/login") {
            let is_desktop = window.innerWidth > 800;
            this.setState({ width: window.innerWidth, is_desktop: is_desktop });
        }
    };

    PropMenuSidebar = (e) => {
        let isOpenMenuSidebar = false;
        let isOpenMenuSidebarMin = false;

        if (this.state.is_desktop) {
            if(this.state.is_desktop  && !this.props.RootStore.Settings.HoverMenuSidebar) {
                if(this.props.RootStore.Settings.isOpenMenuSidebar) {
                    if(!this.props.RootStore.Settings.isOpenMenuSidebarMin) {
                        isOpenMenuSidebar = true;
                        isOpenMenuSidebarMin = true;
                    }
                }
                else {
                    isOpenMenuSidebar = true;
                }
            }
            else {
                if(!this.props.RootStore.Settings.isOpenMenuSidebar) {
                    isOpenMenuSidebar = true
                }
            }
            this.props.RootStore.Settings.isOpenMenuSidebar = isOpenMenuSidebar;
            this.props.RootStore.Settings.isOpenMenuSidebarMin = isOpenMenuSidebarMin
            if(!this.props.RootStore.Settings.HoverMenuSidebar) {
                this.props.RootStore.IsResized = false;
            }
        }
        else {
            this.setState({
                isOpenMenuSidebarMobile: !this.state.isOpenMenuSidebarMobile
            })
        }
        this.props.RootStore.saveSettings();
    };
    PropFilterSidebar = (e) => {
        if (this.state.is_desktop) {
            this.props.RootStore.Settings.isOpenFilterSidebar = !this.props.RootStore.Settings.isOpenFilterSidebar;
            if(!this.props.RootStore.Settings.HoverFilterSidebar) {
                this.props.RootStore.IsResized = false;
            }
        }
        else {
            this.setState({
                isOpenFilterSidebarMobile: !this.state.isOpenFilterSidebarMobile
            })
        }
        this.props.RootStore.saveSettings();
    };


    transitionedFuncMenuSidebar = (e) => {
    };


    render() {

        /* #region Theme */
        let display_state = ""; // Наши состояния открытости

        //Меню-которое закрывается при нажатии мышкой рядом с меню (AND)
        let HoverMenuSidebar = " hover-MenuSidebar"; //Наличие этого класса говорит о том, что меню открывается поверх, не схлопывая контент// В мобильной версии всегда поверх
        let HoverFilterSidebar = " hover-FilterSidebar"; //Наличие этого класса говорит о том, что меню открывается поверх, не схлопывая контент// В мобильной версии всегда поверх
        //Противоположность ховер-меню (AND)
        let FixedMenuSidebar = " FixedMenuSidebar"; //Наличие этого класса говорит о том, что меню открывается c уменьшением контента и фиксируясь// В мобильной версии всегда поверх
        let FixedFilterSidebar = " FixedFilterSidebar"; //Наличие этого класса говорит о том, что меню открывается c уменьшением контента и фиксируясь// В мобильной версии всегда поверх

        //Расположение меню (Поменяется местами с фильтром справа) (OR)
        let LeftMenuSidebar = " LeftMenuSidebar";
        let RightMenuSidebar = " RightMenuSidebar";

        let MinMenuSidebar = " minMenuSidebar"; //Наличие этого класса говорит о том, что меню расхлопывается лишь частично, отображая только иконки

        let OpenMenuSidebar = " OpenMenuSidebar";
        let OpenFilterSidebar = " OpenFilterSidebar";

        let isOpenMenuSidebar = false;
        let isOpenFilterSidebar = false;

        if(this.state.is_desktop) {
            //Значит перед нами Десктоп отображение
            display_state += " desktop-dis";
            if(this.props.RootStore.Settings.RightMenuPanel) {
                display_state += RightMenuSidebar;
            }
            else {
                display_state += LeftMenuSidebar;
            }

            //Класс, что меню зафиксировано имеет смысл только, когда оно открыто
            if(this.props.RootStore.Settings.HoverMenuSidebar) {
                display_state += HoverMenuSidebar;
            }
            else {
                display_state += FixedMenuSidebar;
            }
            if(this.props.RootStore.Settings.HoverFilterSidebar) {
                display_state += HoverFilterSidebar;
            }
            else {
                display_state += FixedFilterSidebar;
            }

            if(this.props.RootStore.Settings.isOpenMenuSidebar) {
                isOpenMenuSidebar = true;
                display_state += OpenMenuSidebar;
                if( this.props.RootStore.Settings.isOpenMenuSidebarMin) {
                    display_state += MinMenuSidebar;
                }
            }
            if(this.props.RootStore.Settings.isOpenFilterSidebar) {
                display_state += OpenFilterSidebar;
                isOpenFilterSidebar = true;
            }
        }
        else {
            display_state += " mobile-dis";

            if(this.state.isOpenMenuSidebarMobile) {
                isOpenMenuSidebar = true;
                display_state += OpenMenuSidebar;
            }
            if(this.state.isOpenFilterSidebarMobile) {
                isOpenFilterSidebar = true;
                display_state += OpenFilterSidebar;
            }
        }

        this.core.recalcStyle();


        const propsHeader = {
            onClickBtnMenu: this.PropMenuSidebar,
            onClickBtnFilter: this.PropFilterSidebar,
            onClickBtnMSidebar: (isOpen:boolean, TypeSidebar: string) => { this.setState({ isOpenMSidebar: isOpen, TypeMSidebar: TypeSidebar })},
            isOpenMSidebar: this.state.isOpenMSidebar,
            TypeMSidebar: this.state.TypeMSidebar,
        };
        const DataMenuSidebar = {
            _ref: (ref) => { this._ref_MenuSidebar_wrap = ref},
            isOpen: isOpenMenuSidebar,
            pullRight: this.props.RootStore.Settings.RightMenuPanel,
            is_desktop: this.state.is_desktop,
            onClickBtnMenu: this.PropMenuSidebar
        };
        const DataFilterSidebar = {
            _ref: (ref) => { this._ref_FilterSidebar_wrap = ref},
            isOpen: isOpenFilterSidebar,
            pullRight: !this.props.RootStore.Settings.RightMenuPanel,
            is_desktop: this.state.is_desktop,
            onClickBtnFilter: this.PropFilterSidebar,
        };
        const DataMSidebar = {
            isOpen: this.state.isOpenMSidebar,
            TypeMSidebar: this.state.TypeMSidebar,
            onClickBtnMSidebar: (isOpen:boolean, TypeSidebar: string) => { this.setState({ isOpenMSidebar: isOpen, TypeMSidebar: TypeSidebar })},
        };


        let RedirectURL = '/home';
        let isArctic = false;
        if(document.location.host == "arctic.ozoneprogram.ru") {
            //Значит это арктика
            //TODO::arctic
            RedirectURL = '/objects/*0|00fc72bf-7e41-48e4-a85d-be29aea22e5f/0';
            isArctic = true;
        }
        this.props.RootStore.isLoadMSettings; //Чтобы обсервер видел


        let JSX_content;

        if(document.location.pathname == "/login") {
            //Для логина мы строим страницу логина

            let def_route;
            let reg_route;

            if(this.props.RootStore.config.registration_group) {
                reg_route = <Route exact path={["/registration/:params", "/registration"]} component={RegistrationPage}/>
            }

            if(this.props.RootStore.config.isAppLogin) {
                def_route = <Switch>
                    <Route exact path="/index" component={LoginPage}/>
                    <Route path={["/login/:params","/login"]} component={LoginFormPage}/>
                    {reg_route}
                    <Route >
                        <Redirect to="/index" />
                    </Route>
                </Switch>;
            }
            else {
                def_route = <Switch>
                    <Route exact path={["/login/:params","/login"]} component={LoginFormPage}/>
                    {reg_route}
                    <Route >
                        <Redirect to="/login" />
                    </Route>
                </Switch>;
            }

            JSX_content = <React.Suspense fallback={<div className={"zt_loader_suspense_container"}><Loader active type="ball-pulse"/></div>}><Router>
                <div ref={(ref) => (this._ref = ref)} id="root_theme">
                    <div id="zt_LoginContainerPage_wrapper">
                        <div id="zt_LoginContainerPage">
                            {def_route}
                            <FormsPanel />
                        </div>
                    </div>
                    <ProgressPanel />
                    <GalleryPanel />
                </div>
            </Router></React.Suspense>
        }
        else {
            JSX_content =
                <Router>
                    <div ref={(ref) => (this._ref = ref)} className={display_state} id="root_theme">
                        <Header {...propsHeader} />
                        <MSidebar {...DataMSidebar}/>
                        <MenuSidebar  {...DataMenuSidebar} />
                        <FilterSidebar  {...DataFilterSidebar} />
                        <div id="zt_ContainerPage_wrapper">
                            <div id="zt_ContainerPage">
                                <React.Suspense fallback={<div className={"zt_loader_suspense_container"}><Loader active type="ball-pulse"/></div>}>
                                <Switch>
                                    {this.props.RootStore.modules.indexOf('home') !== -1 ? <Route exact path="/home" component={HomePage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('classes') !== -1 ? <Route path="/classes" component={ClassesPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('objects') !== -1 ? <Route path={["/objects/:NodeID", "/objects"]} component={ObjectsPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('users') !== -1 ? <Route path={["/users/:NodeID", "/users"]} component={ObjectsPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('users') !== -1 ? <Route path={["/papplication/:GroupID", "/papplication"]} component={ApplicationPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('settApp') !== -1 ? <Route path="/application" component={ApplicationPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('import') !== -1 ? <Route path="/import" component={ImportPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('stepFromEnemat') !== -1 ?  <Route path="/stepform" component={UserStepPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('taxesEnemat') !== -1 ?  <Route path="/taxes" component={TaxesPage}/> : ""}
                                    {this.props.RootStore.modules.indexOf('treeTableEnemat') !== -1 ?  <Route path="/treetable" render={(props) => <TreeTablePage TreeTableUpdate={this.props.RootStore.TreeTableUpdate} {...props} />} /> : ""}
                                    {this.props.RootStore.modules.indexOf('treeTableEnemat') !== -1 ?  <Route path="/archivetreetable" render={(props) => <TreeTablePage TreeTableUpdate={this.props.RootStore.TreeTableUpdate} {...props} />} /> : ""}
                                    {this.props.RootStore.modules.indexOf('stepFromEnematSettings') !== -1 ?  <Route path="/stepformSetting" component={UserStepSettPage}/> : ""}
                                    <Route path={["/view/:NodeID"]} component={ViewPage}/>
                                    {this.props.RootStore.isLoadMSettings ? <Route path="/"><Redirect to={this.props.RootStore.home_url} /></Route> : ""}
                                    <Route component={NoMatch} />
                                </Switch>
                                <FormsPanel />
                                </React.Suspense>
                            </div>
                        </div>
                        <ProgressPanel />
                        <GalleryPanel />
                    </div>
                </Router>
            ;
        }

        return JSX_content;
    }
}
