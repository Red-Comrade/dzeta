import * as React from "react";
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom'
import ClassesTreeInstance from '../ClassesPage/ClassesTreeInstance'
import ObjectsTreeInstance from '../ObjectsPage/ObjectsTreeInstance'
import {AppTreeInstance} from '../ApplicationPage/AppTreeInstance'
import ViewTreeInstance from '../ViewPage/ViewTreeInstance'
import {inject} from "mobx-react";
import {ApplicationPage} from "../ApplicationPage/ApplicationPage";


export const Home = () => <div></div>;
//export const Classes = () => <ClassesTreeInstance/>;
//export const Objects = () => <ObjectsTreeInstance/>;



export interface IFilterSidebarProps {
    RootStore?: any,
    onClickBtnFilter?: (e?) => void;
    children?: React.ReactNode;
    isOpen?: boolean;
    is_desktop?: boolean;
    _ref?: (ref :HTMLElement) => void;


    pullRight? : boolean;
    transitions? : boolean;
    touch? : any;
    touchHandleWidth: number;
    dragToggleDistance: number;
    onSetOpen: (isOpen:boolean) => void;
}
interface IFilterSidebarState {
    dragSupported: boolean;
    touch: boolean;
    touchIdentifier: any;

    touchStartX: number;
    touchCurrentX: number;

    sidebarWidth: any;

    splitterConf: {
        is_resize: boolean,
        start_x_drg: number,
        move_x_drg: number,
    },
}

@inject("RootStore")
export class FilterSidebar extends React.Component<IFilterSidebarProps, IFilterSidebarState> {
    static defaultProps = {
        isOpen: null,
        is_desktop: false,
        pullRight: false,
        transitions: true,
        touch: true,
        touchHandleWidth: 20,
        dragToggleDistance: 40,
        onSetOpen: (isOpen:boolean) => {},
    };
    state: Readonly<IFilterSidebarState> = {
        dragSupported: true,
        touch: true,
        sidebarWidth: 300,

        touchIdentifier: null,
        touchStartX: 0,
        touchCurrentX: 0,

        splitterConf: {
            is_resize: false,
            start_x_drg: 0,
            move_x_drg: 0,
        },
    };
    sidebar = {
        zIndex: 2,
        position: "absolute",
        top: 0,
        bottom: 0,
        transition: "transform .3s ease-out",
        WebkitTransition: "-webkit-transform .3s ease-out",
        willChange: "transform",
        overflowY: "auto",
        offsetWidth: 0
    };
    ref_splitter:HTMLElement = null;

    cleanup_no_match = null;
    prev_status_filter = null;

    isDispathEventResize = false;

    constructor(props: IFilterSidebarProps) {
        super(props);
    };

    componentDidUpdate() {
        // filter out the updates when we're touching
        if (!this.isTouching()) {
            this.saveSidebarWidth();
        }
        if(this.isDispathEventResize) {
            this.isDispathEventResize = false;
            document.dispatchEvent(new CustomEvent("zt_resize", {detail: {}}));
        }
    }
    componentDidMount(): void {
        document.addEventListener('mousedown', this.core.splitter.mousedown);
        document.addEventListener('mousemove', this.core.splitter.mousemove);
        document.addEventListener('mouseup', this.core.splitter.mouseup);
    }
    componentWillUnmount(): void {
        document.removeEventListener('mousedown', this.core.splitter.mousedown);
        document.removeEventListener('mousemove', this.core.splitter.mousemove);
        document.removeEventListener('mouseup', this.core.splitter.mouseup);
    }


    core = {
        splitter: {
            mousedown: (e) => {
                if(this.ref_splitter) {
                    if(this.ref_splitter.contains(e.target)) {
                        this.state.splitterConf.is_resize = true;
                        this.state.splitterConf.start_x_drg = e.clientX;
                        this.state.splitterConf.move_x_drg = e.clientX;

                        this.setState({});
                        e.preventDefault();
                    }
                }
            },
            mousemove: (e) => {
                if(this.state.splitterConf.is_resize) {
                    this.state.splitterConf.move_x_drg = e.clientX;
                    this.setState({});
                }
            },
            mouseup: (e) => {
                if(this.state.splitterConf.is_resize) {
                    this.state.splitterConf.is_resize = false;

                    this.state.splitterConf.move_x_drg = e.clientX;


                    let WidthPanel = this.sidebar.offsetWidth;

                    if(this.props.pullRight) {
                        //Правое расположение панели
                        if(this.state.splitterConf.start_x_drg < this.state.splitterConf.move_x_drg) {
                            //Уменьшение панели
                            this.props.RootStore.Settings.WidthFilterSidebar = WidthPanel - (this.state.splitterConf.move_x_drg - this.state.splitterConf.start_x_drg);
                        }
                        else {
                            //Увеличение панели
                            this.props.RootStore.Settings.WidthFilterSidebar = WidthPanel + (this.state.splitterConf.start_x_drg - this.state.splitterConf.move_x_drg);
                        }
                    }
                    else {
                        //Левое расположение панели
                        if(this.state.splitterConf.start_x_drg < this.state.splitterConf.move_x_drg) {
                            //Увеличение панели
                            this.props.RootStore.Settings.WidthFilterSidebar = WidthPanel + (this.state.splitterConf.move_x_drg - this.state.splitterConf.start_x_drg);
                        }
                        else {
                            //Уменьшение панели
                            this.props.RootStore.Settings.WidthFilterSidebar = WidthPanel - (this.state.splitterConf.start_x_drg - this.state.splitterConf.move_x_drg);
                        }
                    }

                    if(this.props.RootStore.Settings.WidthFilterSidebar  <= 0) {
                        this.props.RootStore.Settings.WidthFilterSidebar = 0;
                    }
                    else if(this.props.RootStore.Settings.WidthFilterSidebar < 20) {
                        this.props.RootStore.Settings.WidthFilterSidebar = 20;
                    }

                    this.isDispathEventResize = true;
                    this.setState({});
                    this.props.RootStore.saveSettings();
                }
            },
        }
    };



    isTouching = () => {
        return this.state.touchIdentifier !== null;
    };
    saveSidebarWidth() {
        const width = this.sidebar.offsetWidth;

        if (width !== this.state.sidebarWidth) {
            this.setState({ sidebarWidth: width });
        }
    }
    saveSidebarRef(node) {
        this.sidebar = node;
    }


    onTouchStart = (e) => {
        // filter out if a user starts swiping with a second finger
        if (!this.isTouching()) {
            const touch = e.targetTouches[0];
            this.setState({
                touchIdentifier: touch.identifier,
                touchStartX: touch.clientX,
                touchCurrentX: touch.clientX
            });
        }
    };
    onTouchMove = (ev) => {
        if (this.isTouching()) {
            for (let ind = 0; ind < ev.targetTouches.length; ind++) {
                // we only care about the finger that we are tracking
                if (ev.targetTouches[ind].identifier === this.state.touchIdentifier) {
                    console.log( ev.targetTouches[ind].clientX)
                    this.setState({
                        touchCurrentX: ev.targetTouches[ind].clientX
                    });
                    break;
                }
            }
        }

    };
    onTouchEnd = (ev) => {
        if (this.isTouching()) {
            // trigger a change to open if sidebar has been dragged beyond dragToggleDistance
            const touchWidth = this.touchSidebarWidth();

            if (
                (this.props.isOpen &&
                    touchWidth  <
                    this.state.sidebarWidth - this.props.dragToggleDistance) ||
                (!this.props.isOpen && touchWidth > this.props.dragToggleDistance)
            ) {
                this.props.onSetOpen(!this.props.isOpen);
                this.props.onClickBtnFilter();
            }

            this.setState({
                touchIdentifier: null,
                touchStartX: null,
                touchCurrentX: null
            });
        }
    };
    onScroll = (e) => {

    };

    touchSidebarWidth = () => {
        // if the sidebar is open and start point of drag is inside the sidebar
        // we will only drag the distance they moved their finger
        // otherwise we will move the sidebar to be below the finger.
        if (this.props.pullRight) {
            if (
                this.props.isOpen &&
                window.innerWidth - this.state.touchStartX < this.state.sidebarWidth
            ) {
                if (this.state.touchCurrentX > this.state.touchStartX) {
                    return (
                        this.state.sidebarWidth +
                        this.state.touchStartX -
                        this.state.touchCurrentX
                    );
                }
                return this.state.sidebarWidth;
            }
            return Math.min(
                window.innerWidth - this.state.touchCurrentX,
                this.state.sidebarWidth
            );
        }

        if (this.props.isOpen && this.state.touchStartX < this.state.sidebarWidth) {
            if (this.state.touchCurrentX > this.state.touchStartX) {
                return this.state.sidebarWidth;
            }
            return (
                this.state.sidebarWidth -
                this.state.touchStartX +
                this.state.touchCurrentX
            );
        }
        return Math.min(this.state.touchCurrentX, this.state.sidebarWidth);
    };

    render() {
        const sidebarStyle:any = {};
        const overlayStyle:any = {};
        const splitterStyle:any = {};

        const useTouch = this.state.dragSupported && this.props.touch;
        const isTouching = this.isTouching();
        const rootProps = {
            className: "",
            role: "navigation",

            onTouchStart: null,
            onTouchMove: null,
            onTouchEnd: null,
            onTouchCancel: null,
            onScroll: null,
        };
        let dragHandle;
        let SplitterClass = "";

        if(this.state.splitterConf.is_resize) {
            if (this.props.pullRight) {
                splitterStyle.left = this.state.splitterConf.move_x_drg - this.state.splitterConf.start_x_drg  + 'px';
            }
            else {
                splitterStyle.right = this.state.splitterConf.start_x_drg - this.state.splitterConf.move_x_drg + 'px';
            }
            SplitterClass +=" resizing";
        }

        if(this.props.RootStore.Settings.WidthFilterSidebar && this.props.is_desktop) {
            sidebarStyle.width = this.props.RootStore.Settings.WidthFilterSidebar + 'px';
        }


        if (this.props.pullRight) {
            sidebarStyle.right = 0;
            sidebarStyle.transform = "translateX(100%)";
            sidebarStyle.WebkitTransform = "translateX(100%)";
        }
        else {
            sidebarStyle.left = 0;
            sidebarStyle.transform = "translateX(-100%)";
            sidebarStyle.WebkitTransform = "translateX(-100%)";
        }

        if (isTouching) {
            const percentage = this.touchSidebarWidth() / this.state.sidebarWidth;

            // slide open to what we dragged
            if (this.props.pullRight) {
                sidebarStyle.transform = `translateX(${(1 - percentage) * 100}%)`;
                sidebarStyle.WebkitTransform = `translateX(${(1 - percentage) * 100}%)`;
            } else {
                sidebarStyle.transform = `translateX(-${(1 - percentage) * 100}%)`;
                sidebarStyle.WebkitTransform = `translateX(-${(1 - percentage) *
                100}%)`;
            }

            // fade overlay to match distance of drag
            overlayStyle.opacity = percentage;
            overlayStyle.visibility = "visible";
        }
        else if (this.props.isOpen) {
            // slide open sidebar
            sidebarStyle.transform = `translateX(0%)`;
            sidebarStyle.WebkitTransform = `translateX(0%)`;

            // show overlay
            if (!this.props.is_desktop) {
                overlayStyle.opacity = 1;
                overlayStyle.visibility = "visible";
            }



        }

        if (isTouching || !this.props.transitions) {
            sidebarStyle.transition = "none";
            sidebarStyle.WebkitTransition = "none";
            overlayStyle.transition = "none";
        }

        if (useTouch && !this.props.is_desktop) {
            if (this.props.isOpen) {
                rootProps.onTouchStart = this.onTouchStart;
                rootProps.onTouchMove = this.onTouchMove;
                rootProps.onTouchEnd = this.onTouchEnd;
                rootProps.onTouchCancel = this.onTouchEnd;
                rootProps.onScroll = this.onScroll;
            }
            else {
                const dragHandleStyle: any = {};
                dragHandleStyle.width = this.props.touchHandleWidth;

                // dragHandleStyle right/left
                if (this.props.pullRight) {
                    dragHandleStyle.right = 0;
                } else {
                    dragHandleStyle.left = 0;
                }


                dragHandle = (
                    <div
                        style={dragHandleStyle}
                        className="zt_drag_handle"
                        onTouchStart={this.onTouchStart}
                        onTouchMove={this.onTouchMove}
                        onTouchEnd={this.onTouchEnd}
                        onTouchCancel={this.onTouchEnd}
                    />
                );
            }
        }

        let $this = this;

        const NoMatch = function (params) {
            React.useEffect(() => {
                if(!$this.cleanup_no_match && $this.prev_status_filter === null) {
                    $this.cleanup_no_match = true;
                    //Запоминаем предыдущее состояние фильтра
                    //Закрываем фильтр
                    $this.prev_status_filter = $this.props.RootStore.Settings.isOpenFilterSidebar;
                    $this.props.RootStore.Settings.isOpenFilterSidebar = false;
                }
                return function cleanup() {
                    //Если смена состояния при иницилизации cleanup, то мы тут ничего не делаем

                    if(!$this.cleanup_no_match) {
                        let prev_status_filter = $this.prev_status_filter;
                        $this.prev_status_filter = null;
                        $this.props.RootStore.Settings.isOpenFilterSidebar = prev_status_filter;
                    }
                    else {
                        $this.cleanup_no_match = false;
                    }
                };
            });

            return <h1></h1>
        };

        let img_logo;
        if(this.props.RootStore.config.img_logo_sidebar_url) {
            img_logo = <img className="zt_img_logo_min" alt={this.props.RootStore.config.img_logo_sidebar_alt} src={this.props.RootStore.config.img_logo_sidebar_url} />
        }



        return (
            <div id="zt_FilterSidebar" {...rootProps} className={((this.props.isOpen)?"open":"") + " " + (this.props.pullRight ? "right-Sidebar" : "left-Sidebar")} >
                <div id="zt_FilterSidebar_overlay" style={overlayStyle} onClick={this.props.onClickBtnFilter} >
                </div>
                {dragHandle}
                <div id="zt_FilterSidebar_wrap" style={sidebarStyle}  ref={(ref) => {this.props._ref(ref); this.saveSidebarRef(ref); }}>
                    <div id="zt_FilterSidebar_splitter" className={SplitterClass} style={splitterStyle} ref={ref=>{this.ref_splitter = ref}} />
                    <div id="zt_FilterSidebar_wrap_wrap">
                        <div id="zt_FilterSidebar_header" className="zt_MenuSidebar_header" style={{backgroundColor:"#e4e4e4"}} >
                            <div className="zt_co_logo_min"><a href="">{img_logo}</a></div>
                            <button title="MenuSidebar" className="zt_btn_FilterSidebar zt_header-btn" style={{backgroundColor:"#e4e4e4", borderLeft: "1px solid white"}} onClick={this.props.onClickBtnFilter}><i className="fas fa-times"/></button>
                        </div>
                        <div id="zt_FilterSidebar_body" onTouchStart={(e) => { e.stopPropagation();}}  >
                            <Switch>
                                <Route path="/view" component={ViewTreeInstance}/>
                                <Route path="/classes" component={ClassesTreeInstance}/>
                                <Route path={["/objects/:NodeID", "/objects", "/users/:NodeID", "/users"]} component={ObjectsTreeInstance}/>
                                <Route path="/application" component={AppTreeInstance}/>
                                <Route path={["/papplication/:GroupID", "/papplication"]} component={AppTreeInstance}/>

                                <Route component={NoMatch} />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
