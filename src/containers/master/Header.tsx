import * as React from "react";
import * as ReactDOM from "react-dom";
import {inject} from "mobx-react";
//import './Header.scss'
import IModalConfig from "../../components/CoolModalForm/IModalConfig";

export interface HeaderProps {
    onClickBtnMenu: (e?) => void;
    onClickBtnFilter: (e?) => void;
    onClickBtnMSidebar: (isOpen:boolean, TypeSidebar: string) => void,

    isOpenMSidebar: boolean;
    TypeMSidebar: string,

    title?: string;
    children?: React.ReactNode;
    time?: Date;

    RootStore?: {
        MSettings: any,
        config: any,

        OpenForm: any,
        home_url: any,

        Settings: {
            RightMenuPanel: boolean,
        }
    }
}
interface HeaderState {
    time?: Date;
    isLeftMenu: boolean; //if false, then menu right

    isOpenPanelAccount: boolean;
}
@inject("RootStore")
export class Header extends React.Component<HeaderProps, HeaderState> {
    state: Readonly<HeaderState> = {
        isLeftMenu: true,
        isOpenPanelAccount: false
    };

    private PA_ref: HTMLDivElement;
    private PA: JSX.Element[] | JSX.Element;
    private PAContainer: HTMLDivElement;

    constructor(props: HeaderProps) {
        super(props);
    }

    tick() {
        this.setState({
            time: new Date()
        });
    }
    componentWillMount() {
        this.tick();
        document.addEventListener('mousedown', this.handleClick, false);
    }
    componentDidMount() {
        setInterval(() => this.tick(), 1000);
    }
    componentWillUnmount(): void {
        document.removeEventListener('mousedown', this.handleClick, false);
    }


    handleClick = (e) => {
        if(this.PA_ref) {
            if(!this.PA_ref.contains(e.target)) {
                //close
                this.setState({isOpenPanelAccount: false});
            }
        }
    };

    //<div id="zt_header_co_logo" className="zt_co_logo_min"><a href=""><img className="zt_img_logo_min" alt="Dzeta-logo" src="/public/assets/img/logo-mini.png" /></a></div>
    // render will know everything!
    //{this.state.time.toLocaleTimeString()}

    render() {
        const PersonalAccount = (
            <div className="dropdown-menu pull-right fxs-avatarmenu-content fxs_dropdown_menu">
                <div className="fxs-avatarmenu-content-header">
                    <div className="fxs-avatarmenu-content-header-logo dropdown dropdown-user fxs_dropdown">
                        <a className="fxs_dropdown_a fxs_db_a fxs_dropdown_a">
                                        <span className="username db hide-el">
                                            <img src="" alt="" className="fxs_db_img"/>
                                                <span className="fxs_db_name"> DataBase Name <i className="fa fa-angle-down"/></span>
                                        </span>
                        </a>
                        <div className="fxs_dropdown_menu dropdown-menu dropdown-menu-default dbs-select">
                            <a className="fxs_db_a select-db">
                                <img src="/config_assets/favicon-blue.png" alt="" className="fxs_db_img"/>
                                <span className="fxs_db_name">WorkDB</span>
                            </a>
                            <a className="fxs_db_a select-db active_fxs_db" >
                                <img src="/config_assets/favicon-blue.png" alt="" className="fxs_db_img"/>
                                <span className="fxs_db_name">МНПЗ 8484</span>
                            </a>
                            <a className=" fxs_db_a select-db " data-name="mnpz84841">
                                <img src="/config_assets/favicon-blue.png" alt="" className="fxs_db_img"/>
                                <span className="fxs_db_name">МНПЗ 8484new</span>
                            </a>
                            <a className=" fxs_db_a select-db " data-name="tnpz8484">
                                <img src="/config_assets/favicon-blue.png" alt="" className="fxs_db_img"/>
                                <span className="fxs_db_name">TНПЗ 8484</span>
                            </a>
                        </div>
                    </div>
                    <a className="fxs-logout" href="./login/logout" onClick={() => { window.location.href = "./login/logout";}} >Log out</a>
                </div>
                <div className="fxs-avatarmenu-content-currentAccount">
                    <a className="fxs-avatarmenu-content-currentAccount-picture-a" aria-label="Ваш аватар"
                       title="" target="_blank">
                        <div className="fxs-avatarmenu-content-currentAccount_profile-div mectrl_profilepic glyph_account_circle dropdown-pic" />
                    </a>
                    <div className="fxs-avatarmenu-content-accountInfo">
                        <div className="fxs-avatarmenu-content-accountInfo-name">{this.props.RootStore.MSettings.UserName}</div>
                    </div>
                </div>
                <div className="fxs_co_3">
                </div>
            </div>
        );


        let MenuBtnJSX = <button id="zt_header_btn_MenuSidebar" className="zt_header-btn" title="MenuSidebar" onClick={this.props.onClickBtnMenu}><i className="fas fa-bars" /></button>;
        let FilterBtnJSX = <button id="zt_header_btn_FilterSidebar" className="zt_header-btn" title="FilterSidebar" onClick={this.props.onClickBtnFilter}><i className="fas fa-tree" /></button>;


        return (
            <div id="zt_header" className="zt_header">
                <div id="zt_header_co_left_panel">
                    {this.props.RootStore.Settings.RightMenuPanel ? FilterBtnJSX : MenuBtnJSX}
                    <div id="zt_header_co_logo" className="zt_co_logo_min">
                        <a className={'zt_header_a_logo'} href={'#' + this.props.RootStore.home_url}>
                            <img className="zt_img_logo_min" src={this.props.RootStore.config.img_logo_min_url} alt={this.props.RootStore.config.img_logo_min_alt} />
                            <div className={'zt_version_div'}>{this.props.RootStore.config.version}</div>
                        </a>
                    </div>
                </div>
                <div id="zt_header_reserve"><div id="zt_header_clock" style={{color: "black"}}></div></div>
                <div id="zt_header_co_btns" ></div>
                {this.props.children}
                <button title="notification" className={"zt_header-btn "+(this.props.isOpenMSidebar && this.props.TypeMSidebar =="notification"? "active" : "")} onClick={() => {
                    let isOpen = this.props.TypeMSidebar != "notification" || !this.props.isOpenMSidebar;
                    this.props.onClickBtnMSidebar(isOpen, "notification");
                }} >
                    <i className="far fa-bell"/>
                </button>
                <button className={"zt_header-btn "+(this.props.isOpenMSidebar && this.props.TypeMSidebar =="settings"? "active" : "")} onClick={() => {
                    let isOpen = this.props.TypeMSidebar != "settings" || !this.props.isOpenMSidebar;
                    this.props.onClickBtnMSidebar(isOpen, "settings");
                }} >
                    <i className="fas fa-cog"/>
                </button>
                <div className="fxs-topbar-avatarmenu" ref={(ref)=>(this.PA_ref = ref)}>
                    <div className={"fxs_dropdown " + (this.state.isOpenPanelAccount?"fxs_open":"")}>
                        <button className="fxs_dropdown_a fxs-dropmenu-button fxs-menu-account" onClick={()=>{ this.setState({isOpenPanelAccount: !this.state.isOpenPanelAccount})}}>
                            <div className="fxs-avatarmenu-header">
                                <div className="fxs-avatarmenu-tenant-container hidden-custom-700">
                                    <div className="fxs-avatarmenu-username">
                                        {this.props.RootStore.MSettings.UserName}
                                    </div>
                                    <div className="fxs-avatarmenu-tenant hide-el" data-bind="text: currentTenantDisplayName">
                                        <i className="fas fa-database"/> DataBase Name
                                    </div>
                                </div>
                                <div className="fxs-avatarmenu-tenant-image-container">
                                    <div className="mectrl_profilepic">
                                        <i className="far fa-user" style={{fontSize: "20px"}}/>
                                    </div>
                                </div>
                            </div>
                        </button>
                        {PersonalAccount}
                    </div>
                </div>
                {this.props.RootStore.Settings.RightMenuPanel ? MenuBtnJSX : FilterBtnJSX}
            </div>
        );
    }
}