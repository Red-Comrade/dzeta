import * as React from "react";
import {inject, observer} from "mobx-react";
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

export interface IMSidebarProps {
    isOpen: boolean,
    RootStore: {
        ProgressIsOpen: boolean,
        ProgressData: any,
    }
}
interface IMSidebarState {
    TabsSett: number;
}
@inject("RootStore")
@observer
export class ProgressPanel extends React.Component<IMSidebarProps, IMSidebarState> {
    static defaultProps = {
        isOpen: false,
        RootStore: {
            Settings: {},
        }
    };
    state: Readonly<IMSidebarState> = {
        TabsSett: 0
    };
    private ref_ProgressBar: HTMLDivElement;



    constructor(props: IMSidebarProps) {
        super(props);
    };

    componentDidUpdate(prevProps: Readonly<IMSidebarProps>, prevState: Readonly<IMSidebarState>, snapshot?: any): void {
        setTimeout(() => {
            if(this.props.RootStore.ProgressIsOpen) {
                this.ref_ProgressBar.classList.add('open_show_opacity');
            }
            else {
                this.ref_ProgressBar.classList.remove('open_show_opacity');
            }
        }, 100);
    }

    render() {
        let completed = this.props.RootStore.ProgressData.completed;
        let buffer = 100;

        //<LinearProgress variant="buffer" value={completed} valueBuffer={buffer} color="secondary" />

        let text_loading = "";
        if(completed >= 20) {
            text_loading = 'Loading '+ completed + '%';
        }

        return (
            <div ref={(ref) => {this.ref_ProgressBar = ref}} id="zt_ProgressBar" className={((this.props.RootStore.ProgressIsOpen)?"open ":"") } >
                <div className="zt-splash-loading-panel">
                    <div style={{width: completed + "%"}} className="zt-splash-loading-line">{text_loading}</div>
                </div>
            </div>
        )
    }
}