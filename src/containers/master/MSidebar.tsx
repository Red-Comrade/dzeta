import * as React from "react";
import { HashRouter as Router, Route, Link } from 'react-router-dom'

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import {inject, observer} from "mobx-react";



const useStyles = makeStyles({
    root: {
        flexGrow: 1,
    },
});



export interface IMSidebarProps {
    isOpen: boolean,
    TypeMSidebar?: string //settings//notification
    onClickBtnMSidebar: (isOpen:boolean, TypeSidebar: string) => void,

    RootStore: {
        Settings: {
            HoverMenuSidebar: boolean,
            HoverFilterSidebar: boolean,
            isOpenMenuSidebar: boolean,
            isOpenMenuSidebarMin: boolean,
            isOpenFilterSidebar: boolean,
            RightMenuPanel: boolean,
        },
        saveSettings: () => void,
    },
}
interface IMSidebarState {
    TabsSett: number;
}
@inject("RootStore")
@observer
export class MSidebar extends React.Component<IMSidebarProps, IMSidebarState> {
    static defaultProps = {
        isOpen: false,
        RootStore: {
            Settings: {},
        }
    };
    state: Readonly<IMSidebarState> = {
        TabsSett: 0
    };
    constructor(props: IMSidebarProps) {
        super(props);
    };

    handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        this.setState({TabsSett: newValue });
    };


    renderSettings = () => {
        function TabPanel(props) {
            const { children, value, index, ...other } = props;

            return (
                <Typography
                    component="div"
                    role="tabpanel"
                    hidden={value !== index}
                    id={`scrollable-auto-tabpanel-${index}`}
                    aria-labelledby={`scrollable-auto-tab-${index}`}
                    {...other}
                >
                    {value === index && <Box p={3}>{children}</Box>}
                </Typography>
            );
        }

        return (
            <div>
                <Paper className={"123"}>
                    <Tabs
                        value={this.state.TabsSett}
                        onChange={this.handleChange}

                        indicatorColor="primary"
                        textColor="primary"
                        centered
                    >
                        <Tab label="General" />
                        <Tab label="Other" />
                    </Tabs>
                    <TabPanel value={this.state.TabsSett} index={0}>
                        <FormGroup row>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={this.props.RootStore.Settings.isOpenMenuSidebar}
                                        onChange={(e)=> {
                                            this.props.RootStore.Settings.isOpenMenuSidebar = e.target.checked;
                                            this.props.RootStore.saveSettings();
                                        }}
                                        value="checkedB"
                                        color="primary"
                                    />
                                }
                                label="Open MenuPanel"
                            />
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={this.props.RootStore.Settings.isOpenFilterSidebar}
                                        onChange={(e)=> {
                                            this.props.RootStore.Settings.isOpenFilterSidebar = e.target.checked;
                                            this.props.RootStore.saveSettings();
                                        }}
                                        value="checkedB"
                                        color="primary"
                                    />
                                }
                                label="Open FilterPanel"
                            />

                        </FormGroup>
                        <FormGroup row>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={this.props.RootStore.Settings.HoverMenuSidebar}
                                        onChange={(e)=> {
                                            this.props.RootStore.Settings.HoverMenuSidebar = e.target.checked;
                                            this.props.RootStore.saveSettings();
                                        }}
                                        value="checkedB"
                                        color="primary"
                                    />
                                }
                                label="FlyOut MenuPanel"
                            />
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={this.props.RootStore.Settings.HoverFilterSidebar}
                                        onChange={(e)=> {
                                            this.props.RootStore.Settings.HoverFilterSidebar = e.target.checked;
                                            this.props.RootStore.saveSettings();
                                        }}
                                        value="checkedB"
                                        color="primary"
                                    />
                                }
                                label="FlyOut FilterPanel"
                            />

                        </FormGroup>
                        <FormGroup row>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={this.props.RootStore.Settings.RightMenuPanel}
                                        onChange={(e)=> {
                                            this.props.RootStore.Settings.RightMenuPanel = e.target.checked;
                                            this.props.RootStore.saveSettings();
                                        }}
                                        value="checkedB"
                                        color="primary"
                                    />
                                }
                                label="Right MenuPanel"
                            />
                        </FormGroup>

                        <div className="form-group">
                            Choose a theme
                            <select
                                defaultValue={(localStorage.Theme?localStorage.Theme:"light")}
                                className="form-control" onChange={
                                (e)=>{
                                    document.body.className = "";
                                    document.body.classList.add(e.target.value);
                                    localStorage.setItem('Theme', e.target.value)
                                }}>
                                <option value="light">Light</option>
                                <option value="dark">Dark</option>
                                <option value="fr_blue">fr_blue</option>
                            </select>
                        </div>
                    </TabPanel>
                    <TabPanel value={this.state.TabsSett} index={1}>
                        Item Two
                    </TabPanel>
                </Paper>
            </div>
        );
    };

    render() {
        let Title = ""
        let Content;
        if(this.props.TypeMSidebar == "settings") {
            Title = "Settings";
            Content = this.renderSettings();
        }
        else if(this.props.TypeMSidebar == "notifications") {
            Title = "notifications"
        }


        let aa = this.props.RootStore.Settings;

        return (
            <div id="zt_MSidebar" className={((this.props.isOpen)?"open":"")} >
                <div id="zt_MSidebar_wrap">
                    <button className="zt_btn_close" onClick={()=>{this.props.onClickBtnMSidebar(false, "");}}><i className="fas fa-times"/></button>
                    <div className="zt_M_header" >
                        <div className="zt_MenuSidebar_title">{Title}</div>
                    </div>
                    <div className="zt_M_body">
                        {Content}
                    </div>
                </div>
            </div>
        )
    }
}