//Специальная панель для модальных форм
//В данном случае мы строим тут массив модальных форм
import * as React from "react";
import {inject, observer} from "mobx-react";
import CoolModalForm from "../../components/CoolModalForm/CoolModalForm";
import IModalConfig from "../../components/CoolModalForm/IModalConfig";
import IModalConfigDictionary from "../../components/CoolModalForm/IModalConfigDictionary";
import CoolModalFormProps from "../../components/CoolModalForm/ICoolModalFormProps";

export interface IFormsPanelProps {
    RootStore?: {
        Forms: any
        TrigForm: any,
    },
}
interface IFormsPanelState {
}
@inject("RootStore")
@observer
export class FormsPanel extends React.Component<IFormsPanelProps, IFormsPanelState> {
    static defaultProps = {
    };
    state: Readonly<IFormsPanelState> = {
    };
    constructor(props: IFormsPanelProps) {
        super(props);
    };


    render() {

        let Trig = this.props.RootStore.TrigForm;

        let CoolModalFormConfig:CoolModalFormProps = {
            Modals: this.props.RootStore.Forms,
        };

        return <CoolModalForm {...CoolModalFormConfig} />;
    }
}