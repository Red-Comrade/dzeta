//import ImageGallery from 'react-image-gallery';

import Lightbox from 'react-image-lightbox';
import * as React from "react";
import {inject, observer} from "mobx-react";


import { SRLWrapper } from "simple-react-lightbox"; // Import SRLWrapper
import SimpleReactLightbox from "simple-react-lightbox"; // Import Simple React Lightbox


export interface IMSidebarProps {
    RootStore: {
        GalleryIsOpen: boolean,
        GalleryData: any,
    }
}
interface IMSidebarState {
    TabsSett: number;
    photoIndex: number;
}
@inject("RootStore")
@observer
export class GalleryPanel extends React.Component<IMSidebarProps, IMSidebarState> {
    static defaultProps = {
        RootStore: {
            GalleryData: [],
            Settings: {},
        }
    };
    state: Readonly<IMSidebarState> = {
        TabsSett: 0,
        photoIndex: 0
    };
    constructor(props: IMSidebarProps) {
        super(props);
    };

    render() {

        const imagesTMP = [
            'https://picsum.photos/id/1018/1000/600/',
            'https://picsum.photos/id/1015/1000/600/',
            'https://picsum.photos/id/1019/1000/600/',
        ];

        let images = [];
        if(this.props.RootStore.GalleryData.Images) {
            images = this.props.RootStore.GalleryData.Images;
        }
        else {
            images = imagesTMP;
        }


        let renderGallery = <div></div>;
        let photoIndex = this.state.photoIndex;
        if(this.props.RootStore.GalleryIsOpen) {
            //renderGallery = <ImageGallery items={images} />;
            renderGallery = <Lightbox
                mainSrc={images[photoIndex]}
                nextSrc={images[(photoIndex + 1) % images.length]}
                prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                onCloseRequest={() => { this.props.RootStore.GalleryIsOpen = false; } }
                onMovePrevRequest={() =>
                    this.setState({
                        photoIndex: (photoIndex + images.length - 1) % images.length,
                    })
                }
                onMoveNextRequest={() =>
                    this.setState({
                        photoIndex: (photoIndex + 1) % images.length,
                    })
                }
            />;
        }

        return (

            <div id="zt_GalleryPanel" className={((this.props.RootStore.GalleryIsOpen)?"open ":"") } >
                <button type="button" id="zt_closeGalleryPanelBtn" onClick={()=>{
                    this.props.RootStore.GalleryIsOpen = false;
                }}><i className="fas fa-times"/></button>
                {renderGallery}
            </div>
        )
    }
}