import * as React from "react";
import { HashRouter as Router, Route, Link, Switch, NavLink } from 'react-router-dom'
import {ClassesPage} from "../ClassesPage/ClassesPage";
import {ObjectsPage} from "../ObjectsPage/ObjectsPage";
import {NoMatch} from "./Master";
import {inject} from "mobx-react";
import * as h_api from "../../api/Home-api";
import {ApplicationPage} from "../ApplicationPage/ApplicationPage";
import {ImportPage} from "../ImportPage/ImportPage";
import {UserStepPage} from "../UserStepPage/UserStepPage";

import {Icon} from '../UserStepPage/Components/Icon'


export interface IMenuSidebarProps {
    RootStore?: any;
    location?: any;

    onClickBtnMenu?: (e?) => void;
    children?: React.ReactNode;
    isOpen?: boolean;
    is_desktop?: boolean;
    _ref?: (ref :HTMLElement) => void;

    pullRight? : boolean;
    transitions? : boolean;
    touch? : any;
    touchHandleWidth: number;
    dragToggleDistance: number;
    onSetOpen: (isOpen:boolean) => void;
}
interface IMenuSidebarState {
    dragSupported: boolean;
    touch: boolean;
    touchIdentifier: any;

    touchStartX: number;
    touchCurrentX: number;

    sidebarWidth: any;
}
@inject("RootStore")
export class MenuSidebar extends React.Component<IMenuSidebarProps, IMenuSidebarState> {
    static defaultProps = {
        isOpen: null,
        is_desktop: false,
        pullRight: true,
        transitions: true,
        touch: true,
        touchHandleWidth: 20,
        dragToggleDistance: 40,
        onSetOpen: (isOpen:boolean) => {},
    };
    state: Readonly<IMenuSidebarState> = {
        dragSupported: true,
        touch: true,
        sidebarWidth: 300,


        touchIdentifier: null,
        touchStartX: 0,
        touchCurrentX: 0,
    };

    sidebar = {
        zIndex: 2,
        position: "absolute",
        top: 0,
        bottom: 0,
        transition: "transform .3s ease-out",
        WebkitTransition: "-webkit-transform .3s ease-out",
        willChange: "transform",
        overflowY: "auto",
        offsetWidth: 0
    };


    constructor(props: IMenuSidebarProps) {
        super(props);
    };

    componentDidMount(): void {
        let isAdmin = true;
        if(document.location.host == "arctic.ozoneprogram.ru") {
            if(this.props.RootStore.MSettings.UserName != "Administrator") {
                isAdmin = false;
            }
        }

        if(!isAdmin) {

        }
    };

    componentDidUpdate() {
        // filter out the updates when we're touching
        if (!this.isTouching()) {
            this.saveSidebarWidth();
        }
    }


    core = {
        RenderListApplications: () => {
            let JSX_appl = [];
            this.props.RootStore.Applications.forEach( (item, index) => {
                if(item.isFolder) {
                    item.children.forEach( (item_in, index_in) => {
                        if(item_in.Read) {
                            JSX_appl.push(<li key={index + "tile"+index_in} className={"zt_li"}>
                                <NavLink className="zt_li_a" activeClassName="active" title={item_in.name} to={"/view/" + item_in.uid}
                                         role="link">
                                    <i className="zt_li_icon fas fa-tablet-alt"/>
                                    <span className="zt_li_span">{item_in.name}</span>
                                </NavLink>
                            </li>);
                        }
                    });
                }
                else if(item.Read){
                    JSX_appl.push(<li key={index + "tile"} className={"zt_li"}>
                        <NavLink className="zt_li_a" activeClassName="active" title={item.name} to={"/view/" + item.uid}
                                 role="link">
                            <i className="zt_li_icon fas fa-tablet-alt"/>
                            <span className="zt_li_span">{item.name}</span>
                        </NavLink>
                    </li>);
                }
            });

            return JSX_appl;
        },

    };


    isTouching = () => {
        return this.state.touchIdentifier !== null;
    };
    saveSidebarWidth() {
        const width = this.sidebar.offsetWidth;

        if (width !== this.state.sidebarWidth) {
            this.setState({ sidebarWidth: width });
        }
    }
    saveSidebarRef(node) {
        this.sidebar = node;
    }


    onTouchStart = (e) => {
        // filter out if a user starts swiping with a second finger
        if (!this.isTouching()) {
            const touch = e.targetTouches[0];
            this.setState({
                touchIdentifier: touch.identifier,
                touchStartX: touch.clientX,
                touchCurrentX: touch.clientX
            });
        }
    };
    onTouchMove = (ev) => {
        if (this.isTouching()) {
            for (let ind = 0; ind < ev.targetTouches.length; ind++) {
                // we only care about the finger that we are tracking
                if (ev.targetTouches[ind].identifier === this.state.touchIdentifier) {
                    console.log( ev.targetTouches[ind].clientX)
                    this.setState({
                        touchCurrentX: ev.targetTouches[ind].clientX
                    });
                    break;
                }
            }
        }

    };
    onTouchEnd = (ev) => {
        if (this.isTouching()) {
            // trigger a change to open if sidebar has been dragged beyond dragToggleDistance
            const touchWidth = this.touchSidebarWidth();

            if (
                (this.props.isOpen &&
                    touchWidth <
                    this.state.sidebarWidth - this.props.dragToggleDistance) ||
                (!this.props.isOpen && touchWidth > this.props.dragToggleDistance)
            ) {
                this.props.onSetOpen(!this.props.isOpen);
                this.props.onClickBtnMenu();
            }

            this.setState({
                touchIdentifier: null,
                touchStartX: null,
                touchCurrentX: null
            });
        }
    };
    onScroll = (e) => {

    };

    touchSidebarWidth = () => {
        // if the sidebar is open and start point of drag is inside the sidebar
        // we will only drag the distance they moved their finger
        // otherwise we will move the sidebar to be below the finger.
        if (this.props.pullRight) {
            if (
                this.props.isOpen &&
                window.innerWidth - this.state.touchStartX < this.state.sidebarWidth
            ) {
                if (this.state.touchCurrentX > this.state.touchStartX) {
                    return (
                        this.state.sidebarWidth +
                        this.state.touchStartX -
                        this.state.touchCurrentX
                    );
                }
                return this.state.sidebarWidth;
            }
            return Math.min(
                window.innerWidth - this.state.touchCurrentX,
                this.state.sidebarWidth
            );
        }

        if (this.props.isOpen && this.state.touchStartX < this.state.sidebarWidth) {
            if (this.state.touchCurrentX > this.state.touchStartX) {
                return this.state.sidebarWidth;
            }
            return (
                this.state.sidebarWidth -
                this.state.touchStartX +
                this.state.touchCurrentX
            );
        }
        return Math.min(this.state.touchCurrentX, this.state.sidebarWidth);
    };


//<img className="zt_img_logo_min" alt="Dzeta-logo" src="/public/assets/img/logo.png" />



    render() {
        const sidebarStyle:any = {};
        const overlayStyle:any = {};

        const useTouch = this.state.dragSupported && this.props.touch;
        const isTouching = this.isTouching();
        const rootProps = {
            className: "",
            role: "navigation",

            onTouchStart: null,
            onTouchMove: null,
            onTouchEnd: null,
            onTouchCancel: null,
            onScroll: null,
        };
        let dragHandle;


        if (this.props.pullRight) {
            sidebarStyle.right = 0;
            sidebarStyle.transform = "translateX(100%)";
            sidebarStyle.WebkitTransform = "translateX(100%)";
            sidebarStyle.boxShadow = "-2px 2px 4px rgba(0, 0, 0, 0.15)";
        }
        else {
            sidebarStyle.left = 0;
            sidebarStyle.transform = "translateX(-100%)";
            sidebarStyle.WebkitTransform = "translateX(-100%)";
        }

        if (isTouching) {
            const percentage = this.touchSidebarWidth() / this.state.sidebarWidth;

            // slide open to what we dragged
            if (this.props.pullRight) {
                sidebarStyle.transform = `translateX(${(1 - percentage) * 100}%)`;
                sidebarStyle.WebkitTransform = `translateX(${(1 - percentage) * 100}%)`;
            } else {
                sidebarStyle.transform = `translateX(-${(1 - percentage) * 100}%)`;
                sidebarStyle.WebkitTransform = `translateX(-${(1 - percentage) *
                100}%)`;
            }

            // fade overlay to match distance of drag
            overlayStyle.opacity = percentage;
            overlayStyle.visibility = "visible";
        }
        else if (this.props.isOpen) {
            // slide open sidebar
            sidebarStyle.transform = `translateX(0%)`;
            sidebarStyle.WebkitTransform = `translateX(0%)`;

            // show overlay
            if (!this.props.is_desktop) {
                overlayStyle.opacity = 1;
                overlayStyle.visibility = "visible";
            }

        }

        if (isTouching || !this.props.transitions) {
            sidebarStyle.transition = "none";
            sidebarStyle.WebkitTransition = "none";
            overlayStyle.transition = "none";
        }
        if (useTouch && !this.props.is_desktop) {
            if (this.props.isOpen) {
                rootProps.onTouchStart = this.onTouchStart;
                rootProps.onTouchMove = this.onTouchMove;
                rootProps.onTouchEnd = this.onTouchEnd;
                rootProps.onTouchCancel = this.onTouchEnd;
                rootProps.onScroll = this.onScroll;
            }
            else {
                const dragHandleStyle: any = {};
                dragHandleStyle.width = this.props.touchHandleWidth;

                // dragHandleStyle right/left
                if (this.props.pullRight) {
                    dragHandleStyle.right = 0;
                } else {
                    dragHandleStyle.left = 0;
                }


                dragHandle = (
                    <div
                        style={dragHandleStyle}
                        className="zt_drag_handle"
                        onTouchStart={this.onTouchStart}
                        onTouchMove={this.onTouchMove}
                        onTouchEnd={this.onTouchEnd}
                        onTouchCancel={this.onTouchEnd}
                    />
                );
            }
        }
        let isArctic = false, isEnemat=false;
        let isAdmin = true;
        if(document.location.host == "arctic.ozoneprogram.ru") {
            if(this.props.RootStore.MSettings.UserName != "Administrator") {
                isAdmin = false;
            }
            //Значит это арктика
            //TODO::arctic
            isArctic = true;
        }
        if(document.location.host == "form.enemat.fr") {
            //Значит это арктика
            //TODO::arctic
            isEnemat = true;
        }

        let ListMenu = <ul key={0} id="zt_ul_left_menu">
            {this.props.RootStore.modules.indexOf('home') !== -1 ?
                <li className={"zt_li"}><NavLink className="zt_li_a" activeClassName="active" to="/home"><i className="zt_li_icon fas fa-home" /><span className="zt_li_span">Home</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('classes') !== -1 ?
                <li className={"zt_li"+(isArctic?" hide-el":"")}><NavLink className="zt_li_a" activeClassName="active" to="/classes"><i className="zt_li_icon fas fa-file-invoice" /><span className="zt_li_span">Classes</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('objects') !== -1 ?
                <li className={"zt_li "+(!isAdmin?"hide-el":"")}><NavLink className="zt_li_a" activeClassName="active" to="/objects"><i className="zt_li_icon fas fa-cubes" /><span className="zt_li_span">{(isArctic?"Documents":"Objects")}</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('users') !== -1 ?
                <li className={"zt_li "+(!isAdmin?"hide-el":"")}><NavLink className="zt_li_a" activeClassName="active" to="/users"><i className="zt_li_icon fas fa-users-cog" /><span className="zt_li_span">Users</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('users') !== -1 ?
                <li className={"zt_li "+(!isAdmin?"hide-el":"")}><NavLink className="zt_li_a" activeClassName="active" to="/papplication"><i className="zt_li_icon fas fa-user-shield" /><span className="zt_li_span">Applications permission</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('settApp') !== -1 ?
                <li className={"zt_li"+(isArctic?" hide-el":"")}><NavLink className="zt_li_a" activeClassName="active" to="/application"><i className="zt_li_icon fas fa-tablet-alt"/><span className="zt_li_span">Applications</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('import') !== -1 ?
                <li className={"zt_li " + (isArctic || isEnemat ? " hide-el" : "")}><NavLink className="zt_li_a" activeClassName="active" to="/import"><i className="zt_li_icon fas fa-upload" /><span className="zt_li_span">Import</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('taxesEnemat') !== -1 /*&& isEnemat*/ ?
                <li className={"zt_li"}><NavLink className="zt_li_a" activeClassName="active" to="/taxes"><i className="zt_li_icon fa fa-money-bill-wave" /><span className="zt_li_span">Taxes</span></NavLink></li>
                : ""}
            {this.props.RootStore.modules.indexOf('treeTableEnemat') !== -1 /*&& isEnemat*/ ?
                  <li className={"zt_li"}><NavLink onClick={(e)=>{this.props.RootStore.updateTreeTable()}} className="zt_li_a" activeClassName="active" to="/treetable"><i className="zt_li_icon fa fa-table" /><span className="zt_li_span">CEE</span></NavLink></li>
                  : ""}
            {this.props.RootStore.modules.indexOf('treeTableEnemat') !== -1 /*&& isEnemat*/ ?
                <li className={"zt_li"}><NavLink onClick={(e)=>{this.props.RootStore.updateTreeTable()}} className="zt_li_a" activeClassName="active" to="/archivetreetable"><div className={"zt_li_icon"} ><Icon name="unarchive" color="white" size={24}/></div><span className="zt_li_span">CEE</span></NavLink></li>
                  : ""}
             {this.props.RootStore.modules.indexOf('stepFromEnematSettings') !== -1 /*&& isEnemat*/ ?
                <li className={"zt_li"}><NavLink className="zt_li_a" activeClassName="active" to="/stepformSetting"><i className="zt_li_icon fa fa-wrench" /><span className="zt_li_span">Merge settings</span></NavLink></li>
                : ""}
            {!isAdmin?this.core.RenderListApplications():""}
        </ul>;


        let img_logo;
        if(this.props.RootStore.config.img_logo_sidebar_url) {
            img_logo = <img className="zt_img_logo_min" alt={this.props.RootStore.config.img_logo_sidebar_alt} src={this.props.RootStore.config.img_logo_sidebar_url} />
        }

        return (
            <div id="zt_MenuSidebar"  {...rootProps}  className={((this.props.isOpen)?"open":"") + " " + (!this.props.pullRight ? "left-Sidebar" : "right-Sidebar")} >
                <div id="zt_MenuSidebar_overlay" style={overlayStyle} onClick={this.props.onClickBtnMenu} />
                {dragHandle}
                <div id="zt_MenuSidebar_wrap" style={sidebarStyle} ref={(ref) => { this.props._ref(ref); this.saveSidebarRef(ref);}}>
                    <div id="zt_MenuSidebar_wrap_wrap">
                        <div id="zt_MenuSidebar_header" className="zt_MenuSidebar_header"  style={{backgroundColor:"#e4e4e4"}}>
                            <button title="MenuSidebar" className="zt_btn_MenuSidebar zt_header-btn" style={{backgroundColor:"#e4e4e4", borderRight: "1px solid white"}} onClick={this.props.onClickBtnMenu}><i className="fas fa-times"/></button>
                            <div className="zt_co_logo_min"><a href="">{img_logo}</a></div>
                        </div>
                        <div id="zt_MenuSidebar_body" >{ListMenu}</div>
                    </div>
                </div>
            </div>
        )
    }
}
