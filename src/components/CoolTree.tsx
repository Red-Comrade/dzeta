/**
 * Данный модуль предназначен для объединения компонента со вкладками с компонентом деревомъ
 * Добавляет возможность открывать несколько экземпляров дерева на разных вкладках
 */

import * as React from "react";
import * as ReactDOM from "react-dom";

import CoolTreeInstance from "./CoolTreeInstance";
import HZContextMenu from "./HZContextMenu";
import HZTabs from "./HZTabs";
import {CoolTreeTabs} from "./CoolTreeTabs";

//Интерфейс атрибутов вкладки
export interface ITabsData {
    ID:string; //ID-с которого начинается интерфейс
    Name: string; //Название вкладки(узла)
    isMain: boolean; //Определяет, что вкладку нельзя закрыть
    isActive: boolean; //Если вкладка выбрана
    treeDataNode?: object[]; //Массив узлов
    Width?: number;
    data?: {
        selectedKey?: string,
        expandedKeys?: string[],
        isFullLoad?: boolean;
        treeDataNode?: object;
        Settings?: object;
        ID?: string;
    }
}

export interface ICoolTreeProps {
    treeData: any[]; //данные по которым строить дерево
    isFullLoad: boolean; //Указывает, что дерево уже загружено true и данных для загрузки больше нет(бесполезный параметр, но пусть будет)
    RootTab: {
        Name: string;
        ID: string;
    };

    CustomBtn?: {events: any, classIcon: string }[], //TODO::Это еще недоделанный функционал, доделается, когда понадобится необходимость в других кнопках

    ShowFromLevelFunc?: (callback: (NodeID?)=> void ) => void;
    selectNodeFunc?: () => string; //В каллбеке будет функция, которая обнулит selectNodeFunc
    expandNodeFunc?: (callback?:()=>void) => string[]; //Работает только с корневым деревом
    RedrafFunc?: () => void; //Закрываем все вкладки и убираем выбор в дереве

    UpdateGlobalTree?: () => void; //Актуализация дерева, есть проблема, если мы клонируем дерево и пришлём его сюда снова,
    // то мы потеряем все ссылки на узлы, по сути у нас останутся мертвые вкладки, вызвать этот метод, чтобы синхронизировать узлы.

    AddBtnDisabled?: boolean;
    EditBtnDisabled?: boolean;
    DelBtnDisabled?: boolean;
    OnAddBtnFunc?: (selectData) => void;
    OnEditBtnFunc?: (selectData) => void;
    OnDelBtnFunc?: (selectData) => void;

    onSelect?: (selectData) => void;
    onSelectR?: (selectData) => void;
    onOpenCM?: (Params: {SelectData?:object, callbackCloseCM?:()=>void, IsBtnCM?:boolean}, callback?: (content) => void) => object[] | void ;
    onExpandNode?: (all_expandedKeys) =>  void;
    onLoadData?: (selectData, CallbackResolve:()=>void) =>  void;

    search_function?: (node:any, SearchString: string) => boolean
}
interface ICoolTreeState {
    Settings?: object,

    SearchString?: string, //Строка для поиска
    Tabs?: ITabsData[];
    RootTab: {
        isActive: boolean,
        Width?: number,
        data?: {
            selectedKey?: string,
            expandedKeys?: string[],
            isFullLoad?: boolean;
            treeDataNode?: object;
            Settings?: object;
            ID?: string;
        }
    }

    SelectNode?: {
        ID?: string,
        isSelected?: boolean,

        SelectData?: object,
    };

    //Вкладки
    isRecalcRender?: boolean; //Событие, которое говорит о том, что нужно обновить размеры(ширину вкладок), так как растянули окно, либо еще чего-нибудь
    TabsSett?: {
        WidthCo?: number, //Ширина контейнера для вкладок
        DropDownOpen?: boolean, //Флаг, что нужно открыть дропдовн вкладок
        TabsArrWidth?: number[]
    }

    //ContextMenu
    targetCM?: HTMLInputElement,
    contentCM?: object[],
}
export default class CoolTree extends React.Component<ICoolTreeProps, ICoolTreeState> {
    static defaultProps = {
        RootTab: {
            ID: "0",
            Name: "root",
            isActive: true
        }
    };
    state: Readonly<ICoolTreeState> = {
        Tabs: [], //По умолчанию еще будет приходить корневое дерево
        Settings: null,
        isRecalcRender: false,
        SelectNode: {
            isSelected: false,
            SelectData: {}
        },

        RootTab: {
            isActive: true,
            Width: 20,
            data: {
                selectedKey: null,
                expandedKeys: [],
                treeDataNode: {},
            },
        },
        TabsSett: {
            WidthCo: 0,
            DropDownOpen: false,
            TabsArrWidth: []
        },
        SearchString: ""
    };

    constructor(props: ICoolTreeProps) {
        super(props);
    };

    shouldComponentUpdate(nextProps: Readonly<ICoolTreeProps>, nextState: Readonly<ICoolTreeState>, nextContext: any): boolean {
        if(nextProps.RedrafFunc) {
            let newTabs = [];
            this.state.RootTab.isActive = true;
            this.state.RootTab.data.selectedKey = null;
            this.state.RootTab.data.expandedKeys = [];
            this.state.RootTab.data.treeDataNode = {};
            this.state.SelectNode.isSelected = false;


            this.setState({
                Tabs: newTabs,
                RootTab: this.state.RootTab,
                isRecalcRender: false,
            });
            nextProps.RedrafFunc();
            return false;
        }
        if(nextProps.selectNodeFunc) {
            let NodeID = nextProps.selectNodeFunc();
            //Обновим состояние новым выбранным узлом

            if( NodeID && (!this.state.SelectNode.isSelected || NodeID != this.state.SelectNode.ID) ) {
            //    debugger
                let SelectData = this.loopFindDataNode(this.props.treeData, NodeID);
                if(SelectData) {
                    this.state.Tabs.forEach((item, i) => {
                        if(item.isActive) {
                            item.data.selectedKey = NodeID;
                        }
                        else {
                            item.data.selectedKey = null;
                        }
                    });
                    if(this.state.RootTab.isActive) {
                        this.state.RootTab.data.selectedKey = NodeID;
                    }

                    this.setState({
                        SelectNode: {
                            SelectData: SelectData,
                            isSelected: true,
                            ID: SelectData.id
                        },
                        RootTab: this.state.RootTab,
                        Tabs: this.state.Tabs,
                    });
                }
            }

            return false;
        }
        if(nextProps.expandNodeFunc) {
            let expandedIDs = nextProps.expandNodeFunc();
            //Обновим состояние с новыми открытыми узлами на всех деревьях
            this.state.RootTab.data.expandedKeys = expandedIDs;
            this.setState({});
            return false;
        }
        if(nextProps.ShowFromLevelFunc) {
            nextProps.ShowFromLevelFunc((NodeID) => {
                this.ShowFromLevelFunc(NodeID);
            });
            return false;
        }
        return true;
    }

    componentDidUpdate(prevProps: Readonly<ICoolTreeProps>, prevState: Readonly<ICoolTreeState>, snapshot?: any): void {
        if(this.props.UpdateGlobalTree) {
            this.props.UpdateGlobalTree();

            for(let i = 0; i < this.state.Tabs.length; i++) {
                let SelectData = this.loopFindDataNode(this.props.treeData, this.state.Tabs[i].ID);
                if(SelectData) {
                    this.state.Tabs[i].data.treeDataNode = [SelectData];
                }
                else {
                    this.onCloseTab(this.state.Tabs[i].ID);
                }
            }
        }
    }


    componentDidMount = () => {
    };
    componentWillMount = () =>  {
    };
    componentWillUnmount = () => {
    };

    //Для получение всех данных узла
    loopFindDataNode = (children, id) => {
        for (let i = 0; i < children.length; i++) {
            if(children[i].id == id) {
                return children[i];
            }
            else if(children[i].children!== null && children[i].children.length > 0) {
                let sel_item = this.loopFindDataNode(children[i].children, id);
                if(sel_item !== null) {
                    return sel_item;
                }
            }
        }
        return null;
    };

    //Вкладки
    ShowFromLevelFunc = (NodeID) => {
        console.log('newLevel');

        if(!NodeID && this.state.SelectNode.isSelected) {
            NodeID = this.state.SelectNode.ID;
        }

        let SelectData = this.loopFindDataNode(this.props.treeData, NodeID);

        if(SelectData) {
            let isEX = false;
            this.state.Tabs.forEach((item, i)=> {
                if(item["ID"] == NodeID) {
                    isEX = true;
                    item["isActive"] = true;
                }
                else {
                    item["isActive"] = false;
                }
            });

            if(this.props.RootTab.ID == NodeID) {
                isEX = true;
                this.state.RootTab.isActive = true;
            }
            else {
                this.state.RootTab.isActive = false;
            }

            if(!isEX) {
                let NewTabConfig = {
                    ID: NodeID,
                    Name: SelectData.Name,
                    isMain: false,
                    isActive: true,
                    Close: true,

                    Width: 20,
                    data: {
                        selectedKey: null,
                        treeDataNode: [SelectData],
                        ID: NodeID
                    }
                };

                this.state.Tabs.push(NewTabConfig);
            }

            this.setState({
                Tabs: this.state.Tabs,
                RootTab: this.state.RootTab,
                isRecalcRender: false,
            })
        }
    };
    onDropdownChange = (isOpen) => {
        this.state.TabsSett.DropDownOpen = isOpen;
        this.setState({})
    };
    onActiveTab = (TabID:string) => {
        this.state.Tabs.forEach((item, i)=> {
            if(item["ID"] == TabID) {
                item["isActive"] = true;
            }
            else {
                item["isActive"] = false;
            }
        });
        if(this.props.RootTab.ID == TabID) {
            this.state.RootTab.isActive = true;
        }
        else {
            this.state.RootTab.isActive = false;
        }
    };
    onCloseTab = (TabID:string) => {
        let newTabs = [];
        // tabs_new.splice();
        let isActiveClosingTab = false;
        this.state.Tabs.forEach((item, i)=> {
            if(item["ID"] == TabID && !item["isMain"]) {
                isActiveClosingTab = item["isActive"]
            }
            else {
                newTabs.push(item);
            }
        });
        if(isActiveClosingTab) {
            this.state.RootTab.isActive = true;
        }

        this.setState({
            Tabs:newTabs,
            RootTab: this.state.RootTab,
            isRecalcRender: false,
        });
    };
    RecalcCoTab = (TabsSett: {TabsArrWidth: number[], WidthCo: number }) => {
        let newTabs = [];
        this.state.Tabs.forEach((item, i)=> {
            if(TabsSett.TabsArrWidth[item["ID"]] != undefined) {
                item.Width = TabsSett.TabsArrWidth[item["ID"]].Width
            }
            newTabs.push(item);
        });

        if(TabsSett.TabsArrWidth[this.props.RootTab.ID] != undefined) {
            this.state.RootTab.Width = TabsSett.TabsArrWidth[this.props.RootTab.ID].Width
        }

        this.state.TabsSett.TabsArrWidth = TabsSett.TabsArrWidth;
        this.state.TabsSett.WidthCo = TabsSett.WidthCo;


        this.setState({
            Tabs: newTabs,
            TabsSett: this.state.TabsSett,
            RootTab: this.state.RootTab,
            isRecalcRender: true
        })
    };


    //Дерево
    onRightClickNode = (e) => {
        const openCM = (contentCM) => {
            this.setState({
                targetCM: e.event.target,
                contentCM: contentCM
            })
        };
        let NodeID = e.node.props.eventKey;
        if(NodeID) {
            let SelectData = this.loopFindDataNode(this.props.treeData, NodeID);
            let contentCM = this.props.onOpenCM(
                {SelectData:SelectData, callbackCloseCM:()=>{this.setState({targetCM: null});}}, openCM);
            if(contentCM) {
                //Открываем контекстное меню
                openCM(contentCM);
            }
            else {
                //Иначе надеемся, что контекстное меню откроется через callback
            }
            //Для выбора узла в дереве
            this.state.Tabs.forEach((item, i)=> {
                if(item["isActive"]) {
                    item.data.selectedKey = SelectData.id;
                }
            });
            if(this.state.RootTab.isActive) {
                this.state.RootTab.data.selectedKey = SelectData.id;
            }
            this.setState({});

            if(this.props.onSelectR) {
                this.props.onSelectR(SelectData);
            }
        }
    };
    onExpandNode = (TreeID, expandedKeys) => {
        let isRoot = false;
        if(this.props.RootTab.ID == TreeID) {
           this.state.RootTab.data.expandedKeys = expandedKeys;
            isRoot = true;
        }
        else {
            this.state.Tabs.forEach((item, i)=> {
                if(TreeID == item.ID) {
                    item.data.expandedKeys = expandedKeys;
                }
            });
        }

        let all_expandedKeys = [];
        //#region Колхозные метод собрать все открытые узлы, пока мы не контролируем наши открытые вкладки дерева через пропс
        this.state.Tabs.forEach((item, i)=> {
            if(item.data.expandedKeys != undefined) {
                for( let i = 0; i < item.data.expandedKeys.length; i ++) {
                    if(all_expandedKeys.indexOf(item.data.expandedKeys[i]) === -1) {
                        all_expandedKeys.push(item.data.expandedKeys[i]);
                    }
                }
            }
        });

        if(this.state.RootTab.data.expandedKeys != undefined) {
            for (let i = 0; i < this.state.RootTab.data.expandedKeys.length; i++) {
                if (all_expandedKeys.indexOf(this.state.RootTab.data.expandedKeys[i]) === -1) {
                    all_expandedKeys.push(this.state.RootTab.data.expandedKeys[i]);
                }
            }
        }
        //#endregion

        this.setState({});

        if(this.props.onExpandNode) {
            this.props.onExpandNode(all_expandedKeys);
        }
    };
    onSelectNode = (SelectData) => {
        if(SelectData) {

            this.state.Tabs.forEach((item, i)=> {
                if(item.isActive) {
                    item.data.selectedKey = SelectData.id;
                }
                else {
                    item.data.selectedKey = null;
                }
            });
            if(this.state.RootTab.isActive) {
                this.state.RootTab.data.selectedKey = SelectData.id;
            }


            this.setState({
                SelectNode: {
                    SelectData: SelectData,
                    isSelected: true,
                    ID: SelectData.id
                },
                RootTab: this.state.RootTab,
                Tabs: this.state.Tabs,
            });
            if(this.props.onSelect) {
                this.props.onSelect(SelectData);
            }
        }
        else {
            this.setState({
                SelectNode: {
                    isSelected: false,
                },
            });
        }
    };
    onLoadData = (SelectData, callbackResolve) => {
        if(SelectData) {
            if(this.props.onLoadData) {
                this.props.onLoadData(SelectData, () => {
                    this.setState({});
                    callbackResolve();
                } );
            }
            else {
                callbackResolve();
            }
        }
        else {
            callbackResolve();
        }
    };

    onOpenCM_btn = (e) => {
        const openCM = (contentCM) => {
            this.setState({
                targetCM: e.target,
                contentCM: contentCM
            })
        };
        if(this.state.SelectNode.isSelected) {
            let contentCM = this.props.onOpenCM(
                {
                    SelectData: this.state.SelectNode.SelectData,
                    callbackCloseCM:()=>{this.setState({targetCM: null});},
                    IsBtnCM: true,
                    }, openCM);
            if(contentCM) {
                //Открываем контекстное меню
                openCM(contentCM);
            }
            else {
                //Иначе надеемся, что контекстное меню откроется через callback
            }
        }
    };

    RenderToolbarTree = () => {
        let isDisabledButtons = false;
        if(!this.state.SelectNode.isSelected) {
            isDisabledButtons = true;
        }

        if(this.props.CustomBtn){
          return [<input
              key="ttp_inp_search"
              onChange={(e)=>{this.setState({SearchString: e.target.value})}}
              className={`ttp_inp_search form-control`}
              placeholder="Search..."
          />, ...this.RenderCustomBtn()]
        }

        return [
            <input
                key="ttp_inp_search"
                onChange={(e)=>{this.setState({SearchString: e.target.value})}}
                className={`ttp_inp_search form-control`}
                placeholder="Search..."
            /> ,
            <button
                disabled={isDisabledButtons || this.props.AddBtnDisabled}
                onClick={(info)=>{
                    if(this.props.OnAddBtnFunc) {
                        this.props.OnAddBtnFunc(this.state.SelectNode.SelectData);
                    }
                }}
                key="ttp_btn_add"
                className={`ttp_btn_add ttp_toolbar_btns btn btn-default`}>
                <i className="fa fa-plus" />
            </button>,
            <button
                disabled={isDisabledButtons || this.props.EditBtnDisabled}
                onClick={(info)=>{
                    if(this.props.OnEditBtnFunc) {
                        this.props.OnEditBtnFunc(this.state.SelectNode.SelectData);
                    }
                }}
                key="ttp_btn_edit"
                className={`ttp_btn_edit ttp_toolbar_btns btn btn-default`}>
                <i className="fa fa-edit" />
            </button>,
            <button
                disabled={isDisabledButtons || this.props.DelBtnDisabled}
                onClick={(info)=>{
                    if(this.props.OnDelBtnFunc) {
                        this.props.OnDelBtnFunc(this.state.SelectNode.SelectData);
                    }
                }}
                key="ttp_btn_remove"
                className={`ttp_btn_remove ttp_toolbar_btns btn btn-default`}>
                <i className="fas fa-times" />
            </button>,
            <button
                disabled={isDisabledButtons}
                key="ttp_btn_more"
                onClick={this.onOpenCM_btn}
                onContextMenu={this.onOpenCM_btn}
                className={`ttp_btn_more ttp_toolbar_btns btn btn-default`}>
                <i className="fa fa-ellipsis-h" />
            </button>
        ];
    };
    RenderCustomBtn=()=>{
      return this.props.CustomBtn.map((item, index)=>{
        return <button
            key={'custom_btn'+index}
            {...item.events}
            className={`ttp_toolbar_btns btn btn-default`}>
            <i className={item.classIcon} />
        </button>
      });
    };

    render() {
        let FilteredTreeData = this.props.treeData;


        let Trees = [];

        let CoolTreeTabs_tabs = [];
        //Объекдиняем наш массив вкладок с основной вкладкой

        CoolTreeTabs_tabs.push({
            ID: this.props.RootTab.ID,
            Name: this.props.RootTab.Name,
            isMain: true,
            isActive: this.state.RootTab.isActive,
            Width: this.state.RootTab.Width,
            data: {
                treeDataNode: FilteredTreeData,
                selectedKey: this.state.RootTab.data.selectedKey,
                expandedKeys: this.state.RootTab.data.expandedKeys,
                ID: this.props.RootTab.ID,
            },
            SearchString: this.state.SearchString
        });
        CoolTreeTabs_tabs = CoolTreeTabs_tabs.concat(this.state.Tabs);

        CoolTreeTabs_tabs.forEach( (item, key) => {
            const dataCoolTreeInstance = {
                onRightClick: this.onRightClickNode,
                onSelectNode: this.onSelectNode,
                onExpandNode: this.onExpandNode,
                onLoadData: this.onLoadData,
                data: item.data,
                SearchString: this.state.SearchString,
                search_function: this.props.search_function
            };

            Trees.push(<div key={key} className={item.isActive ? 'active' : 'hide-el'} >
                <CoolTreeInstance {...dataCoolTreeInstance} />
            </div>);
        });

        const dataCoolTreeTabs = {
            Tabs: CoolTreeTabs_tabs,

            isRecalcRender: this.state.isRecalcRender,
            TabsSett: {
                WidthCo: this.state.TabsSett.WidthCo,
                DropDownOpen: this.state.TabsSett.DropDownOpen
            },
            RecalcCoTabTrig: () => { console.log('Событие не назначено')},
            RecalcCoTab: this.RecalcCoTab,

            onDropdownChange: this.onDropdownChange,
            onActiveTab: this.onActiveTab,
            onCloseTab: this.onCloseTab,
        };
        const dataContextMenu = {
            target: this.state.targetCM,
            content: this.state.contentCM,
            CloseCMFunc: () => { this.setState({targetCM: null}) }
        };

        if(this.props.treeData.length == 0) {
            Trees = [<div key={"tmp"} />];
        }

        return (
            <div className="ttp_content">
                <CoolTreeTabs {...dataCoolTreeTabs} />
                <div className="ttp_toolbar">
                    {this.RenderToolbarTree()}
                </div>
                <div className="ttp_co_trees">
                    {Trees}
                </div>
                <HZContextMenu {...dataContextMenu} />
            </div>
        );
    }
}
