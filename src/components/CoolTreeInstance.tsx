/**
 * Данный модуль предназначен для объединения компонента со вкладками с компонентом деревомъ
 * Добавляет возможность открывать несколько экземпляров дерева на разных вкладках
 */

import * as React from "react";
import * as ReactDOM from "react-dom";
import Tree, { TreeNode } from 'rc-tree';


export interface IClassesTreeInstanceProps {
    onRightClick?: (e) => void,
    onSelectNode?: (selectData) => void,
    onExpandNode?: (TreeID, expandNode) => void,
    onLoadData?: (selectData, callbackResolve: () => void) => void,

    SearchString: string,
    data: {
        selectedKey: string,
        expandedKeys: string[],
        isFullLoad: boolean;
        treeDataNode: object;
        Settings: object;
        ID: string;
    }

    SetActiveTabAction?: (TabID:number)=> void;//{ /*Мы получили от сервера ID-открытой вкладки*/ },
    SelectNodeTreeAction?: (Params:object)=>void;//{ /*Выбор узла в дереве*/ },
    TrigSelectNodeAction?: (isTrig:boolean)=>void;//{ /*Говорит о том, что в этот момент компоненту с параметрами можно обновлять данные*/ },
    ChangeTreeDataAction?: (Params:object)=>void;//{ /*Обновляем статус нашего дерева(к вкладке) isFullLoad = true*/ },

    search_function?: (node:any, SearchString: string) => boolean
}
interface IClassesTreeInstanceState {
    Settings: object,

    expandedKeys?: string[]; //Массив открытых узлов
}

export default class CoolTreeInstance extends React.Component<IClassesTreeInstanceProps, IClassesTreeInstanceState> {
    static defaultProps = {
        isOpen: null,
        isFullLoad: true, //
        data: {
            treeDataNode:[],
        },
    };
    state: Readonly<IClassesTreeInstanceState> = {
        Settings: null,
        expandedKeys: [],
    };


    constructor(props: IClassesTreeInstanceProps) {
        super(props);
    };


    componentDidMount = () => {
    };
    componentWillMount = () =>  {
    };
    componentWillUnmount = () => {

    };

    //Для получение всех данных узла
    loopFindDataNode = (children, id) => {
        if(typeof children == "object") {
            for (let i = 0; i < children.length; i++) {
                if(children[i].id == id) {
                    return children[i];
                }
                else if(typeof children[i].children == "object" && children[i].children!== null && children[i].children.length > 0) {
                    let sel_item = this.loopFindDataNode(children[i].children, id);
                    if(sel_item !== null) {
                        return sel_item;
                    }
                }
                else if(typeof children[i].children != "object" ) {
                    debugger
                }
            }
        }
        return null;
    };

    onSelect = (info, e, selectedNodes) => {
        let sel_id_node = e.node.props.eventKey;

        if(sel_id_node) {
            const selectData = this.loopFindDataNode(this.props.data.treeDataNode, sel_id_node);
            //selectData = {ID...}
            this.props.onSelectNode(selectData);
            //this.setState({selectedKey: selectData.id})
        }


    };
    onRightClick = (e) => {
        this.props.data.selectedKey = e.node.props.eventKey;
        this.props.onRightClick(e);
    };
    onExpand = (expandNode) => {
        this.props.onExpandNode(this.props.data.ID, expandNode);
    };

    onLoadData = (treeNode) => {
        console.log('load data...');
        return new Promise((resolve) => {
            let sel_id_node = treeNode.props.eventKey;

            if(sel_id_node) {
                const selectData = this.loopFindDataNode(this.props.data.treeDataNode, sel_id_node);
                this.props.onLoadData(selectData, () => {
                    resolve();
                });
            }
        });
    };


    render() {

        let expandedKeys = [];
        const loop = (data, SearchString) => {
            let children = [];
            if(data.length > 0) {
                data.forEach( (item) => {
                    let icon = item.icon ? item.icon : 'rc-tree-iconEle rc-tree-icon__open';

                    let is_founded;
                    if(this.props.search_function) {
                        is_founded = this.props.search_function(item, SearchString);
                    }
                    else {
                        is_founded = item.Name.toLocaleLowerCase().indexOf(SearchString) !== -1;
                    }

                    let ClassName = is_founded && SearchString != "" ? "ttp_tree_node_founded" : "";

                    const DateTreeNode = {
                        active: false,
                        icon: <span className={icon}/>,
                        title: item.Name,
                        key: item.id,
                        className: ClassName,
                        isLeaf: false,
                        disabled: false
                    };

                    if (item.children) {
                        let childrenJSX = loop(item.children, SearchString);
                        if( is_founded || childrenJSX.length > 0 ) {
                            if(SearchString) {
                                expandedKeys.push(item.id);
                            }
                            children.push(
                                <TreeNode {...DateTreeNode}>
                                    {childrenJSX}
                                </TreeNode>
                            );
                        }
                    }
                    else {
                        let is_founded;
                        if(this.props.search_function) {
                            is_founded = this.props.search_function(item, SearchString);
                        }
                        else {
                            is_founded = item.Name.toLocaleLowerCase().indexOf(SearchString) !== -1;
                        }

                        if( is_founded ) {
                            DateTreeNode.isLeaf = true;
                            DateTreeNode.disabled = item.key === '0-0-0';
                            children.push(<TreeNode {...DateTreeNode} />);
                        }
                    }
                });
            }
            return children;
        };
        const treeNodes = loop(this.props.data.treeDataNode, this.props.SearchString);

        let selectedKeys = [];
        if(this.props.data.selectedKey) {
            selectedKeys.push(this.props.data.selectedKey+"");
        }
        expandedKeys = this.props.data.expandedKeys;


        const DataTree = {
            onSelect: this.onSelect,
            onRightClick: this.onRightClick,
            onExpand: this.onExpand,
            loadData: this.onLoadData,

            selectedKeys: selectedKeys,
            expandedKeys: expandedKeys,
        };

        return (
            <Tree
                {...DataTree}
                checkStrictly
                showLine

            >
                {treeNodes}
            </Tree>
        );
    }
}