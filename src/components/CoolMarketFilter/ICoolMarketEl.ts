import EnumCoolMarketEl from "./EnumCoolMarketEl";

export default interface ICoolMarketEl {
    TypeEl: EnumCoolMarketEl,
    onChange: (e?, item?) => void,
    filter_value_new: any,
    filter_sett_new: any,
    name?: string,

    isArray?: boolean,

    ObjCallback?: (data:{filter_value, SuccFunc, ErrFunc}) => void,
}