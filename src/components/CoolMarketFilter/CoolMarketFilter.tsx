import * as React from "react";
import ICoolMarketEl from "./ICoolMarketEl";
import EnumCoolMarketEl from "./EnumCoolMarketEl";
import DatePicker from "react-datepicker";
import {format, parse} from "date-fns";
import AsyncSelect from 'react-select/async';
import { WithContext as ReactTags } from 'react-tag-input';
import Switch from "react-switch";
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

export interface ICoolMarketFilterProps {
    Elements: ICoolMarketEl[],
    onClickResetBtn?: (e?) => void,
    onClickSubmitBtn?: (e?) => void,
}
interface ICoolMarketFilterState {
}

export default class CoolMarketFilter extends React.Component<ICoolMarketFilterProps, ICoolMarketFilterState> {
    DelayRecalcTimer = 0;
    static defaultProps = {
    };
    constructor(props: ICoolMarketFilterProps) {
        super(props);
        this.state = {
        };
    };

    shouldComponentUpdate(nextProps: Readonly<ICoolMarketFilterProps>, nextState: Readonly<ICoolMarketFilterState>, nextContext: any): boolean {
        return  true;
    }
    componentWillMount = () =>  {
    };
    componentDidMount = () => {
    };
    componentWillUnmount = () => {};

    events = {
    };
    core = {
        renderControls: () => {
            let controls = [];

            if(this.props.Elements) {
                this.props.Elements.forEach( (item, key) => {

                   let btns_arr = [];

                   if(item.isArray) {
                       let isActiveBtn = item.filter_sett_new?.op == "ALL" ? "ALL" : "ANY";

                       btns_arr.push(
                           <div className={"vts_switch_co"} key={"sw_key1"}>
                                <span className={"vts_switch_span"}>ANY</span>
                           <Switch
                               height={18}
                               width={40}
                               title=""
                               className={"pp_c_switch pp_c react-switch"}
                               onChange={() => {
                                   item.filter_sett_new.op = isActiveBtn == "ALL" ? "ANY" : "ALL";
                                   item.onChange({filter_sett_new: item.filter_sett_new}, item);
                               }}
                               checked={isActiveBtn == "ALL"}


                               //handleDiameter={30}
                               onColor={"#a5a5a5"}
                               offColor={"#a5a5a5"}

                               uncheckedIcon={<div style={{
                                   display: "flex",
                                   justifyContent: "center",
                                   alignItems: "center",
                                   height: "100%",
                                   fontSize: 12,
                                   color: "white",
                                   paddingRight: 10
                               }}></div>}
                               checkedIcon={<div style={{
                                   display: "flex",
                                   justifyContent: "center",
                                   alignItems: "center",
                                   height: "100%",
                                   fontSize: 12,
                                   color: "white",
                                   paddingLeft: 10,
                               }}></div>}
                               uncheckedHandleIcon={
                                   <div
                                       style={{
                                           display: "flex",
                                           justifyContent: "center",
                                           alignItems: "center",
                                           height: "100%",
                                           fontSize: 12,
                                           color: "#67ab67",
                                       }}
                                   >
                                       <i className="fas fa-arrow-left"/>
                                   </div>
                               }
                               checkedHandleIcon={
                                   <div
                                       style={{
                                           display: "flex",
                                           justifyContent: "center",
                                           alignItems: "center",
                                           height: "100%",
                                           fontSize: 12,
                                           color: "#67ab67",
                                       }}
                                   >
                                       <i className="fas fa-arrow-right"/>
                                   </div>
                               }

                           />
                                <span className={"vts_switch_span"}>ALL</span>
                           </div>
                       );
                       //<i className="fas fa-arrow-left"></i> <i className="fas fa-arrow-right"></i>
                       /*
                       btns_arr.push(
                           <button key={'btn_any'} className={"cool_mf_btn nf_btn-ellipse vtc_btn_arr " + (isActiveBtn == "ANY" ? "active" : "")}
                                   onClick={() => {
                                       item.filter_sett_new.op = "ANY";
                                       item.onChange({filter_sett_new: item.filter_sett_new}, item);
                                   }}
                           >Any</button>
                       );
                       btns_arr.push(
                           <button key={'btn_all'} className={"cool_mf_btn nf_btn-ellipse vtc_btn_arr " + (isActiveBtn == "ALL" ? "active" : "")}
                                   onClick={() => {
                                       item.filter_sett_new.op = "ALL";
                                       item.onChange({filter_sett_new: item.filter_sett_new}, item);
                                   }}
                           >All</button>
                       );*/
                   }

                   if(item.TypeEl == EnumCoolMarketEl.stringInput) {
                       if(!item.isArray) {
                           controls.push(
                               <div key={key} className={"vtc_control"}>
                                   <div className={"vtc_co_label"}>
                                       <span className={"vtc_label_span"}>{item.name}</span>
                                       {btns_arr}
                                   </div>
                                   <div className={"vtc_co_input"}>
                                       <input className={"vtc_input_filter vtc_input_text form-control"}
                                              value={item.filter_value_new}
                                              onChange={(e) => { item.onChange(e, item); }}
                                              onKeyDown={(e)=>{
                                                  if (e.key === 'Enter') {
                                                      this.props.onClickSubmitBtn();
                                                  }
                                              }}
                                              type={"text"} disabled={false} />
                                   </div>
                               </div>);
                       }
                       else {
                           controls.push(
                               <div key={key} className={"vtc_control"}>
                                   <div className={"vtc_co_label"}>
                                       <span className={"vtc_label_span"}>{item.name}</span>
                                       {btns_arr}
                                   </div>
                                   <div className={"vtc_co_input"}>
                                       {this.core.build.tagsInputControl(item, key, item.onChange )}
                                   </div>
                               </div>);
                       }
                   }
                   else if(item.TypeEl == EnumCoolMarketEl.numberInput) {
                       if(!item.isArray) {
                           let numbers_controls = [];

                           if (typeof item.filter_value_new == "object") {
                               for (let key_n in item.filter_value_new) {
                                   let item_n = item.filter_value_new[key_n];
                                   numbers_controls.push(
                                       this.core.build.numberControl({
                                           value1: item_n.value1,
                                           value2: item_n.value2
                                       }, 0, (data: { value1, value2 }) => {
                                           item_n.value1 = data.value1;
                                           item_n.value2 = data.value2;
                                           item.onChange(item.filter_value_new, item);
                                       })
                                   );
                               }
                           }
                           else {
                               numbers_controls = [
                                   this.core.build.numberControl({
                                       value1: "",
                                       value2: ""
                                   }, 0, (data: { value1, value2 }) => {
                                       item.onChange([{value1: data.value1, value2: data.value2}], item);
                                   })
                               ];
                           }

                           controls.push(
                               <div key={key} className={"vtc_control"}>
                                   <div className={"vtc_co_label"}>
                                       <span className={"vtc_label_span"}>{item.name}</span>
                                       {btns_arr}
                                   </div>
                                   <div className={"vtc_co_input"}>
                                       {numbers_controls}
                                   </div>
                               </div>
                           );
                       }
                       else {
                           controls.push(
                               <div key={key} className={"vtc_control"}>
                                   <div className={"vtc_co_label"}>
                                       <span className={"vtc_label_span"}>{item.name}</span>
                                       {btns_arr}
                                   </div>
                                   <div className={"vtc_co_input"}>
                                       {this.core.build.tagsInputControl(item, key, item.onChange )}
                                   </div>
                               </div>);
                       }
                   }
                   else if(item.TypeEl == EnumCoolMarketEl.objInput) {
                       controls.push(
                           <div key={key} className={"vtc_control"}>
                               <div className={"vtc_co_label"}>
                                   <span className={"vtc_label_span"}>{item.name}</span>
                                   {btns_arr}
                               </div>
                               <div className={"vtc_co_input"}>
                                   {this.core.build.selectControl(item, (new_value) => { item.onChange(new_value, item); })}
                               </div>
                           </div>
                       );
                   }
                   else if (item.TypeEl == EnumCoolMarketEl.datetime) {

                       if(!item.isArray) {
                           let dates = [];

                           if (typeof item.filter_value_new == "object") {
                               for (let key_n in item.filter_value_new) {
                                   let item_n = item.filter_value_new[key_n];
                                   dates.push(
                                       this.core.build.dateTimeControl({
                                           value1: item_n.value1,
                                           value2: item_n.value2
                                       }, 0, (data: { value1, value2 }) => {
                                           item_n.value1 = data.value1;
                                           item_n.value2 = data.value2;
                                           item.onChange(item.filter_value_new, item);
                                       })
                                   );
                               }
                           } else {
                               dates = [
                                   this.core.build.dateTimeControl({
                                       value1: "",
                                       value2: ""
                                   }, 0, (data: { value1, value2 }) => {
                                       item.onChange([{value1: data.value1, value2: data.value2}], item);
                                   })
                               ];
                           }

                           controls.push(
                               <div key={key} className={"vtc_control"}>
                                   <div className={"vtc_co_label"}>
                                       <span className={"vtc_label_span"}>{item.name}</span>
                                       {btns_arr}
                                   </div>
                                   <div className={"vtc_co_input"}>
                                       {dates}
                                   </div>
                               </div>
                           );
                       }
                       else {
                           controls.push(
                               <div key={key} className={"vtc_control"}>
                                   <div className={"vtc_co_label"}>
                                       <span className={"vtc_label_span"}>{item.name}</span>
                                       {btns_arr}
                                   </div>
                                   <div className={"vtc_co_input"}>
                                       {this.core.build.dateTimeControlArray( item, key, item.onChange )}
                                   </div>
                               </div>);
                       }
                   }
                });
            }

            return controls;
        },


        build: {
            dateTimeControlArray: (item, key, onChange) => {
                const dateTime_input = (value, isDisabled, key, onChange) => {
                    return <DatePicker key={key}
                                className={"date_control"}
                                timeFormat="HH:mm"
                                timeIntervals={15}
                                timeCaption="time"
                                dateFormat="dd/MM/yyyy HH:mm"
                                isClearable
                                placeholderText={""}
                                showTimeInput
                                timeInputLabel="Time:"
                                selected={value}
                                title={value}
                                onChange={(date) => {
                                    onChange(date);
                                }}
                    />;
                };

                let _options = item.filter_value_new ? item.filter_value_new : [];
                let _tags = _options.map( (item) => { return {text: item.value, id: item.value}});
                const KeyCodes = {
                    comma: 188,
                    enter: 13,
                };
                const delimiters = [KeyCodes.comma, KeyCodes.enter];

                let main_control = dateTime_input('', false, 'one_control', (date) => {
                    let t_value = date;
                    if(t_value) {
                        const index = _options.length === 0 ? 0 : _options.length;
                        if(t_value) {
                            t_value = format(t_value, 'dd/MM/yyyy HH:mm:ss')
                        }
                        _options[index] = { value: t_value };
                        onChange(_options, item);
                    }
                    else {
                        onChange([], item);
                    }
                });

                return <div className={"vtc_co_tags_datetime_arr"}>
                            <ReactTags
                                readOnly = {false}
                                tags={_tags}
                                allowDragDrop={false}
                                handleDelete={(i) => {
                                    let new_value = _options.filter((tag, index) => index !== i);
                                    onChange(new_value, item);
                                }}
                                delimiters={delimiters}
                                inline={true}
                                placeholder={""}
                            />
                            {main_control}
                        </div>;
            },

            numberControl: (item, key, onChange) => {
                let value1 = item.value1 ? item.value1 : "";
                let value2 = item.value2 ? item.value2 : "";

                return <div style={{display:"flex"}} key={key}>
                    <input placeholder={"From"} className={"vtc_input_filter vtc_input_text form-control"}
                              value={value1}
                              onChange={(e) => {
                                  onChange({value1: e.target.value, value2: item.value2});
                              }}
                              onKeyDown={(e)=>{
                                  if (e.key === 'Enter') {
                                      this.props.onClickSubmitBtn();
                                  }
                              }}
                           type={"number"} disabled={false} />
                    <input placeholder={"To"} className={"vtc_input_filter vtc_input_text form-control"}
                           value={value2}
                           onChange={(e) => {
                               onChange({value1: item.value1, value2: e.target.value });
                           }}
                           onKeyDown={(e)=>{
                               if (e.key === 'Enter') {
                                   this.props.onClickSubmitBtn();
                               }
                           }}
                           type={"number"} disabled={false} />
                </div>;
            },
            dateTimeControl: (item, key, onChange) => {

                let value1 = item.value1 ? item.value1 : "";
                let value2 = item.value2 ? item.value2 : "";
                if(value1) {
                    value1 = parse(
                        value1,
                        'dd/MM/yyyy HH:mm:ss',
                        new Date()
                    );
                }
                if(value2) {
                    value2 = parse(
                        value2,
                        'dd/MM/yyyy HH:mm:ss',
                        new Date()
                    );
                }

                return <div style={{display:"flex"}} key={key}>
                    <DatePicker key={1}
                          className={"vtc_input_filter vtc_input_text form-control "}
                          timeFormat="HH:mm"
                          timeIntervals={15}
                          timeCaption="time"
                          dateFormat="dd/MM/yyyy HH:mm"
                          isClearable
                          placeholderText={"From"}
                          showTimeInput
                          timeInputLabel="Time:"
                          selected={value1}
                          title={value1}
                          onChange={(date) => {
                              if(date) {
                                  date = format(date, 'dd/MM/yyyy HH:mm:ss')
                              }
                              onChange({value1: date, value2: item.value2});
                          }}
                    />
                    <DatePicker key={2}
                          className={"vtc_input_filter vtc_input_text form-control "}
                          timeFormat="HH:mm"
                          timeIntervals={15}
                          timeCaption="time"
                          dateFormat="dd/MM/yyyy HH:mm"
                          isClearable
                          placeholderText={"To"}
                          showTimeInput
                          timeInputLabel="Time:"
                          selected={value2}
                          title={value2}
                          onChange={(date) => {
                              if(date) {
                                  date = format(date, 'dd/MM/yyyy HH:mm:ss')
                              }
                              onChange({value1: item.value1, value2: date});
                          }}
                    />
              </div>;
            },
            selectControl: (item, onChange) => {
                const promiseOptions = inputValue =>
                    new Promise(resolve => {
                        if(item.ObjCallback) {
                            item.ObjCallback({filter_value: inputValue, SuccFunc: (data) => {
                                    console.log(data);
                                resolve(data);

                                }, ErrFunc: () => {
                                    resolve([]);
                                    }
                                }
                            );
                        }
                    });
                //defaultOptions

                let _options = item.filter_value_new ? item.filter_value_new : [];

                return <div className={"vtc_co_select_obj"}>
                    <AsyncSelect
                        isMulti
                        cacheOptions
                        defaultOptions
                        options={_options}
                        loadOptions={promiseOptions}
                        placeholder={""}
                        onChange={onChange}

                        className="abc"
                        classNamePrefix="react-select"

                        value={_options}
                        setValue={(ValueType, ActionTypes) => {}}
                        onKeyDown={(e)=>{
                            if (e.key === 'Enter') {
                                this.props.onClickSubmitBtn();
                            }
                        }}
                    />
                </div>;
            },
            tagsInputControl: (item, key, onChange) => {

                //suggestions={suggestions}
                let _options = item.filter_value_new ? item.filter_value_new : [];
                let _tags = _options.map( (item) => { return {text: item.value, id: item.value}}) ;


                const KeyCodes = {
                    comma: 188,
                    enter: 13,
                };
                const delimiters = [KeyCodes.comma, KeyCodes.enter];

                return <div className={"vtc_co_tags_arr"}>
                    <ReactTags
                        autofocus={false}
                        tags={_tags}
                        allowDragDrop={false}
                        handleDelete={(i) => {
                            let new_value = _options.filter((tag, index) => index !== i);
                            onChange(new_value, item);
                        }}
                        handleAddition={ (tag) => {
                            let new_value = [..._options, {value:tag.id}];
                            onChange(new_value, item);
                        }}
                        delimiters={delimiters}
                        inline={true}
                        placeholder={""}
                    />
                </div>
            },
        },
    };

    render() {
        let SubmitBtn;
        let ResetBtn;

        if(this.props.onClickSubmitBtn) {
            SubmitBtn = <button type={"button"} className={"cool_mf_btn nf_btn-ellipse"} onClick={this.props.onClickSubmitBtn}>Submit</button>;

        }
        if(this.props.onClickResetBtn) {
            ResetBtn = <button type={"button"} className={"cool_mf_btn nf_btn-ellipse"} onClick={this.props.onClickResetBtn}>Clear</button>;
        }

        return (
            <div className="CoolMarketFilter">
                <div className={"cool_mf_btns_panel"}>
                    {SubmitBtn}
                    {ResetBtn}
                </div>
                <div className={"cool_mf_wrapper_controls"}>
                    {this.core.renderControls()}
                </div>
            </div>
        );
    }
}