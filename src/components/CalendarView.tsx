import * as React from "react";
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);


import FullCalendar, { formatDate } from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin, { Draggable } from '@fullcalendar/interaction'
import DatePicker from "react-datepicker";
import Select, { components } from 'react-select';

import "react-datepicker/dist/react-datepicker.css";

import * as ct_apiview from "../api/View-api";
import * as ct_apiobj from "../api/ObjTree-api";


import HZTabs from "./HZTabs";

import './CalendarView.scss'

const DateFormat = 'DD/MM/yyyy';


interface ICalendarViewProps{
    user:{name:string, uid:string},
    view_sett:any,
    onLoad:()=>void,
}
interface ICalendarEvents{
  id: string,
  title:string,
  start: Date,
  allDay: boolean,
  display?:string,

  date_param?:string,
  implementer_param?:string,
  event_restriction?:string,

  isTask?:boolean,
  isEvent?:boolean,

  date_limit?:Date,


  backgroundColor:string,
  borderColor:string,
  textColor:string,
}
interface ICalendarViewState{
    events:ICalendarEvents[],
    tasks:ICalendarEvents[],
    manager_tasks:ICalendarEvents[],
    chosenUser:{name:string, uid:string},
    usersData:{value:string, label:string}[],
    chosenDate:Date,
    calendarType:string,
    view_param_info:any,

}
export class CalendarView extends React.Component<ICalendarViewProps, ICalendarViewState>{
  private calendar_api:any;
  constructor(props){
    super(props);
    this.state={
      events:[],
      chosenUser:null,
      usersData:[],
      tasks:[],
      manager_tasks:[],
      chosenDate:new Date(),
      calendarType:'dayGridMonth',
      view_param_info:{},
    };
    this.calendar_api = React.createRef();
  }
  componentDidMount(){
      console.log(this.props.view_sett);
      let cal_sett= this.props.view_sett.settings.calendar_sett;
      let user=this.props.user;

      this.loadViewInfo(cal_sett).then((view_param_info)=>{
        console.log(view_param_info);
        this.setState({view_param_info:view_param_info}, ()=>{
          if(!cal_sett.calendar_type || cal_sett.calendar_type==0){
            this.loadData(user, cal_sett);
          }else if(cal_sett.calendar_type==1){
            this.loadDataAdv(user, cal_sett);
          }
        })
      })
  }

  loadViewInfo=(cal_sett)=>{
    let load_data_promise=[
      ct_apiview.getHeadData({uid:this.props.view_sett.View}),
    ];
    if(cal_sett.tasks && cal_sett.tasks.enable_task==1 && cal_sett.tasks.task_view){
      load_data_promise.push(ct_apiview.getHeadData({uid:cal_sett.tasks.task_view}));
    }

    return Promise.all(load_data_promise).then((res_arr)=>{
      let view_param_info={};
      if(res_arr[0] && res_arr[0].data.result==1){
        //event attr info
        view_param_info['event']=res_arr[0].data.data;
      }
      if(res_arr[1] && res_arr[1].data.result==1){
        //tasks attr info
        view_param_info['task']=res_arr[1].data.data;
      }
      return view_param_info;
    });
  }

  loadData=(user, cal_sett)=>{
    let load_data_promise=[
      this.loadEvents(this.props.view_sett.View, cal_sett, user)
    ];
    if(cal_sett.tasks && cal_sett.tasks.enable_task==1 && cal_sett.tasks.task_view){
      load_data_promise.push(this.loadTasks(cal_sett.tasks.task_view, cal_sett, user));
    }

    Promise.all(load_data_promise).then((res_arr)=>{
      let task_data=[], events_data=[];
      if(res_arr[0] && res_arr[0].data.result==1){
        //events data
        let event_data_res=res_arr[0].data.data;

        let title_param=cal_sett.events.text_event;
        let date_param=cal_sett.events.start_date;
        let event_restriction=cal_sett.events.event_restriction;
        let type_event=cal_sett.events.type_event==1?'background':'block';

        events_data= event_data_res.map((item)=>{
            return{
              id: item[date_param].o,
              title:title_param && item[title_param] && item[title_param].v?item[title_param].v:'',
              start: date_param && item[date_param] && item[date_param].v?new Date(item[date_param].v):'',
              allDay: true,

              date_param:date_param,
              event_restriction:event_restriction,

              isEvent:true,
              display:type_event,

              backgroundColor:(cal_sett.events.background_color?cal_sett.events.background_color:'#ffffff'),
              borderColor:(cal_sett.events.border_color?cal_sett.events.border_color:'#05c0fa'),
              textColor:(cal_sett.events.text_color?cal_sett.events.text_color:'#5d6e74'),
            }
        })
      }
      if(res_arr[1] && res_arr[1].data.result==1){
        //tasks data
        let task_data_res=res_arr[1].data.data;

        let title_param=cal_sett.tasks.text_task;
        let date_param=cal_sett.tasks.start_date;
        let date_limit_param=cal_sett.tasks.date_limit_param;
        let implementer_param=cal_sett.tasks.implementer_param;

        task_data= task_data_res.map((item)=>{
            return{
              id: item[date_param].o,
              title:title_param && item[title_param] && item[title_param].v?item[title_param].v:'',
              start: date_param && item[date_param] && item[date_param].v?new Date(item[date_param].v):'',
              allDay: true,

              date_param:date_param,
              implementer_param:implementer_param,

              isTask:true,
              date_limit:date_limit_param && item[date_limit_param] && item[date_limit_param].v?new Date(item[date_limit_param].v):'',


              backgroundColor:(cal_sett.tasks.background_color?cal_sett.tasks.background_color:'#ffffff'),
              borderColor:(cal_sett.tasks.border_color?cal_sett.tasks.border_color:'#05c0fa'),
              textColor:(cal_sett.tasks.text_color?cal_sett.tasks.text_color:'#5d6e74'),
            }
        })
      }
      this.setState({events:events_data , tasks:task_data}, ()=>{
        if(this.props.onLoad){
          this.props.onLoad();
        }
      })
    })

  }
  loadDataAdv=(user, cal_sett)=>{
    let load_data_promise=[
      this.loadUsers(cal_sett.auditor_group),
      this.loadTasks(cal_sett.tasks.task_view, cal_sett, null),
    ];
    Promise.all(load_data_promise).then((res_arr)=>{
      let usersData=[], manager_tasks=[];
      if(res_arr[0] && res_arr[0].data.result==1){
        usersData= res_arr[0].data.data.map((item)=>{
          return {
            value:item.uid,
            label:item.name,
          }
        });
      }
      if(res_arr[1] && res_arr[1].data.result==1){
        let task_data_res=res_arr[1].data.data;

        let title_param=cal_sett.tasks.text_task;
        let date_param=cal_sett.tasks.start_date;
        let date_limit_param=cal_sett.tasks.date_limit_param;
        let implementer_param=cal_sett.tasks.implementer_param;

        manager_tasks= task_data_res.filter((item)=>{
          return implementer_param && item[implementer_param] && !item[implementer_param].v?true:false;
        }).map((item)=>{
            return{
              id: item[date_param].o,
              title:title_param && item[title_param] && item[title_param].v?item[title_param].v:'',
              start: date_param && item[date_param] && item[date_param].v?new Date(item[date_param].v):'',
              allDay: true,

              date_param:date_param,
              implementer_param:implementer_param,

              isTask:true,
              date_limit:date_limit_param && item[date_limit_param] && item[date_limit_param].v?new Date(item[date_limit_param].v):'',


              backgroundColor:(cal_sett.tasks.background_color?cal_sett.tasks.background_color:'#ffffff'),
              borderColor:(cal_sett.tasks.border_color?cal_sett.tasks.border_color:'#05c0fa'),
              textColor:(cal_sett.tasks.text_color?cal_sett.tasks.text_color:'#5d6e74'),
            }
        })
      }
      let chosenUser=null;
      if(usersData.length && usersData[0]){
        chosenUser={
          name:usersData[0].label,
          uid:usersData[0].value,
        }
      }
      this.setState({usersData:usersData , manager_tasks:manager_tasks, chosenUser:chosenUser}, ()=>{
        if(this.state.chosenUser){
          this.loadData(this.state.chosenUser, cal_sett);
        }else if(this.props.onLoad){
            this.props.onLoad();
          }
      })
    })
  }

  loadUsers=(auditor_group)=>{
    let classGroupOfusers='00000000-0000-0000-0000-000000000001'
    return ct_apiobj.getChildrenByType(auditor_group, classGroupOfusers);
  }
  loadEvents=(view_uid, call_set, user)=>{
    let req_params:any={
          view: view_uid,
          attr_sort: '',
          sort: 1,
          start: 1,
          num: 100000,
      };
    if(call_set.events && call_set.events.event_restriction && call_set.events.event_restriction==-1){
      //текущий юзер родитель
      req_params.ParentObj=user.uid;
    }else if(call_set.events && call_set.events.event_restriction){
      //текущий юзер параметр
      req_params.attr_filter=[
        {
          op:'IN',
          v:[user.name],
          attr:call_set.events.event_restriction
        }
      ];
    }

  return ct_apiview.getTableData(req_params)

  }
  loadTasks=(view_uid, call_set, user)=>{
    let params:any={
        view: view_uid,
        attr_sort: '',
        sort: 1,
        start: 1,
        num: 100000,
    }
    if(user){
      let f_v=[user.name];
      /*if(call_set.calendar_type==1){
        f_v.push('IS NULL');
      }*/
      let filter_data=[
        {
          op:'IN',
          v:f_v,
          attr:call_set.tasks.implementer_param
        }
      ];
      params.attr_filter=filter_data;
    }
    return ct_apiview.getTableData(params)
  }

  setTaskImplementer=(params)=>{
    //saveValueCellTable
  }



  changeTypeCal=(type)=>{
    this.setState({calendarType:type}, ()=>{
      let calendarApi = this.calendar_api.current.getApi();
      calendarApi.changeView(type);
    })
  }
  nextDateCal=()=>{
    let calendarApi = this.calendar_api.current.getApi()
    calendarApi.next();
    let newDate=calendarApi.getDate();
    this.setState({chosenDate:newDate});
  }
  prevDateCal=()=>{
    let calendarApi = this.calendar_api.current.getApi()
    calendarApi.prev()
    let newDate=calendarApi.getDate();
    this.setState({chosenDate:newDate});
  }
  setDateCal=(date)=>{
    this.setState({chosenDate:date}, ()=>{
      let calendarApi = this.calendar_api.current.getApi()
      calendarApi.gotoDate(date);
    })
  }
  choseUser=(opt_data)=>{
    let cal_sett= this.props.view_sett.settings.calendar_sett;
    this.setState({chosenUser:{
      name:opt_data.label,
      uid:opt_data.value,
    }}, ()=>{
      this.loadData(this.state.chosenUser, cal_sett);
    })
  }
  handleDateSelect = (selectInfo) => {
    let calendarApi = selectInfo.view.calendar
    const range = moment.range(selectInfo.start, selectInfo.end);
    let days:any = Array.from(range.by('days'));
    if(selectInfo.allDay){
      days=Array.from(range.by('days', { excludeEnd: true }));
    }
    let days_str=days.map((item)=>{return item.format(DateFormat)});

    let has_events=false, selected_events=[];
    this.state.events.forEach((event_item)=>{
      if(days_str.indexOf(moment(event_item.start).format(DateFormat))!==-1){
        has_events=true;
        selected_events.push(event_item);
      }
    });
    if(has_events){
      this.removeListEvents(selected_events);
    }else{
      this.addEvents(days);
    }

    calendarApi.unselect();
  }

  addEvents=(days)=>{

    let cal_sett= this.props.view_sett.settings.calendar_sett;
    let parent='00000000-0000-0000-0000-000000000000';
    let param_info= this.state.view_param_info['event'].find((attr_info)=>{
      return attr_info.uid==cal_sett.events.start_date?true:false;
    })
    let type=param_info.class.uid;

    let param_info_restr=null;
    if(cal_sett.events && cal_sett.events.event_restriction && cal_sett.events.event_restriction==-1){
      //текущий юзер родитель
      parent=this.props.user.uid;
    }else if(cal_sett.events && cal_sett.events.event_restriction){
      //текущий юзер параметр
      param_info_restr=this.state.view_param_info['event'].find((attr_info)=>{
        return attr_info.uid==cal_sett.events.event_restriction?true:false;
      })
    }


    let add_data_promise=days.map((day)=>{
        let values=[
          {
            value:day.format(DateFormat),
            attr:cal_sett.events.start_date,
            key:(param_info.key?1:0),
            datatype:'datetime'
          }
        ];
        if(cal_sett.events && cal_sett.events.event_restriction && cal_sett.events.event_restriction!=-1){
          //текущий юзер параметр
          values.push({
            value:this.props.user.uid,
            attr:cal_sett.events.event_restriction,
            key:(param_info_restr.key?1:0),
            datatype:'object'
          })
        }

        return ct_apiobj.addObject(type, parent, values);
    });

    Promise.all(add_data_promise).then((res_arr)=>{
      let new_events= this.state.events;
      days.forEach((date, index)=>{
        let result:any=res_arr[index];
        if(result.data.result==1){
          let title_param=cal_sett.events.text_event;
          let date_param=cal_sett.events.start_date;
          let event_restriction=cal_sett.events.event_restriction;
          let type_event=cal_sett.events.type_event==1?'background':'block';

          new_events.push({
            id: result.data.data.uid,
            title:'',
            start: date.toDate(),
            allDay: true,

            date_param:date_param,
            event_restriction:event_restriction,

            isEvent:true,
            display:type_event,

            backgroundColor:(cal_sett.events.background_color?cal_sett.events.background_color:'#ffffff'),
            borderColor:(cal_sett.events.border_color?cal_sett.events.border_color:'#05c0fa'),
            textColor:(cal_sett.events.text_color?cal_sett.events.text_color:'#5d6e74'),
          })
        }
      })
      this.setState({events:new_events});
    })
  }
  removeListEvents=(selected_events)=>{
    if (confirm('Are you sure you want to delete the selected')){
      let new_events= this.state.events;
      let removed_uids=selected_events.map((event_item)=>{
        return event_item.id
      });

      ct_apiobj.removeObjs(removed_uids).then((res)=>{
        if(res.data.result==1){
          new_events=new_events.filter((event_item)=>{
            return removed_uids.indexOf(event_item.id)==-1?true:false;
          })
          this.setState({events:new_events});
        }
      })

    }
  }

  changeDate=(uid, value, key)=>{
    let changed_data=null, param_info=null;
    if(key=='event'){
      changed_data=this.state.events.find((item)=>{
        return item.id==uid?true:false;
      })
      param_info= this.state.view_param_info['event'].find((attr_info)=>{
        return attr_info.uid==changed_data.date_param?true:false;
      })
    }
    if(key=='task'){
      changed_data=this.state.tasks.find((item)=>{
        return item.id==uid?true:false;
      })
      param_info= this.state.view_param_info['task'].find((attr_info)=>{
        return attr_info.uid==changed_data.date_param?true:false;
      })
    }
    if(key=='manager_tasks'){
      changed_data=this.state.manager_tasks.find((item)=>{
        return item.id==uid?true:false;
      })
      param_info= this.state.view_param_info['task'].find((attr_info)=>{
        return attr_info.uid==changed_data.date_param?true:false;
      })
    }


    let params={
      obj:changed_data.id,
      datatype:'datetime',
      type:param_info.class.uid,
      attr:changed_data.date_param,
      value:value,
    };

    return ct_apiview.saveValueCellTable(params);

  }
  changeImplementer=(uid, value, key)=>{
    let changed_data=null, param_info=null;
    if(key=='task'){
      changed_data=this.state.tasks.find((item)=>{
        return item.id==uid?true:false;
      })
      param_info= this.state.view_param_info['task'].find((attr_info)=>{
        return attr_info.uid==changed_data.implementer_param?true:false;
      })
    }
    if(key=='manager_tasks'){
      changed_data=this.state.manager_tasks.find((item)=>{
        return item.id==uid?true:false;
      })
      param_info= this.state.view_param_info['task'].find((attr_info)=>{
        return attr_info.uid==changed_data.implementer_param?true:false;
      })
    }


    let params={
      obj:changed_data.id,
      datatype:'object',
      type:param_info.class.uid,
      attr:changed_data.implementer_param,
      value:value,
    };

    return ct_apiview.saveValueCellTable(params);

  }

  dropToUser=(data)=>{
    let uid=data.id;
    this.changeImplementer(uid, this.state.chosenUser.uid, 'manager_tasks').then((res)=>{
      if(res.data.result==1){
        let manager_tasks=this.state.manager_tasks.filter((item)=>{
          return item.id==uid?false:true;
        });
        let tasks=[data, ...this.state.tasks];
        this.setState({tasks:tasks, manager_tasks:manager_tasks})
      }
    })
  }
  dropToManager=(data)=>{
    let uid=data.id;
    this.changeImplementer(uid, '', 'task').then((res)=>{
      if(res.data.result==1){
        let tasks=this.state.tasks.filter((item)=>{
          return item.id==uid?false:true;
        });
        let manager_tasks=[data, ...this.state.manager_tasks];
        this.setState({tasks:tasks, manager_tasks:manager_tasks})
      }
    })
  }

  dropTask=(e)=>{
    let uid=e.draggedEl.getAttribute('data-uid');
    let changed_data=this.state.tasks.find((item)=>{
      return item.id==uid?true:false;
    })
    if(changed_data.date_limit && changed_data.date_limit<e.date){
      return false;
    }
    this.changeDate(uid, (moment(e.date).format(DateFormat)), 'task').then((res)=>{
      if(res.data.result==1){
        let tasks=this.state.tasks.map((item)=>{
          if(item.id==uid){
            item.start=e.date;
          }
          return item;
        })
        this.setState({tasks:tasks})
      }
    })

  }
  clickOnEvent=(event)=>{
    if (confirm(`Are you sure you want to delete the event '${event.title}'`)) {
      let uid=event.id;
      let data= this.state.events.find((item)=>{
          return item.id==uid?true:false;
      })
      if(!data){
        //check in tasks
        data= this.state.tasks.find((item)=>{
            return item.id==uid?true:false;
        })
      }
      if(data){
        if(data.isTask){
          this.removeTask(data);
        }else if(data.isEvent){
          this.removeEvent(data);
        }
      }
    }
  }
  eventChange=(e)=>{
    let uid=e.event.id;
    let data= this.state.events.find((item)=>{
        return item.id==uid?true:false;
    })
    if(!data){
      //check in tasks
      data= this.state.tasks.find((item)=>{
          return item.id==uid?true:false;
      })
    }
    if(data){
      if(data.isTask){
        this.changeTask(e.event, e.oldEvent);
      }else if(data.isEvent){
        this.changeEvent(e.event, e.oldEvent);
      }
    }
  }
  changeTask=(new_task_data, old_task_data)=>{
    let tasks=this.state.tasks
    let uid=new_task_data.id;
    let changed_data=this.state.tasks.find((item)=>{
      return item.id==uid?true:false;
    })
    if(changed_data.date_limit && changed_data.date_limit<new_task_data.start){
      this.setState({tasks:tasks});
      return false;
    }
    if(new_task_data.end!=old_task_data.end){
      this.setState({tasks:tasks});
      return false;
    }
    if(new_task_data.start!=old_task_data.start){
      this.changeDate(uid, (moment(new_task_data.start).format(DateFormat)), 'task').then((res)=>{
        if(res.data.result==1){
          tasks=tasks.map((item)=>{
            if(item.id==uid){
              item.start=new_task_data.start;
            }
            return item;
          })
          this.setState({tasks:tasks})
        }
      })
    }

  }
  changeEvent=(new_event_data, old_event_data)=>{
    let events=this.state.events
    let uid=new_event_data.id;
    let changed_data=this.state.events.find((item)=>{
      return item.id==uid?true:false;
    })
    if(new_event_data.end!=old_event_data.end){
      this.setState({events:events});
      return false;
    }
    if(new_event_data.start!=old_event_data.start){
      this.changeDate(uid, (moment(new_event_data.start).format(DateFormat)), 'event').then((res)=>{
        if(res.data.result==1){
          events=events.map((item)=>{
            if(item.id==uid){
              item.start=new_event_data.start;
            }
            return item;
          })
          this.setState({events:events})
        }
      })
    }
  }
  removeTask=(task_data)=>{
    this.changeDate(task_data.id, '', 'task').then((res)=>{
      if(res.data.result==1){
        let tasks=this.state.tasks.map((item)=>{
          if(item.id==task_data.id){
            item.start=null;
          }
          return item;
        })
        this.setState({tasks:tasks})
      }
    })

  }
  removeEvent=(event_data)=>{
    let new_events= this.state.events;
    let removed_uids:any=[event_data.id];

    ct_apiobj.removeObjs(removed_uids).then((res)=>{
      if(res.data.result==1){
        new_events=new_events.filter((event_item)=>{
          return removed_uids.indexOf(event_item.id)==-1?true:false;
        })
        this.setState({events:new_events});
      }
    })
  }

  renderEventContent=(eventInfo)=>{
    let cal_sett= this.props.view_sett.settings.calendar_sett;
    if(eventInfo.event.display=='background'){
      return <div className="event_cont_bg"></div>;
    }else{
      let removeBtn=null;
      if(cal_sett.calendar_type!=1){
        removeBtn=<button type={"button"} className={'btn'} title="Remove" onClick={(e)=>{this.clickOnEvent(eventInfo.event)}}><i className="fa fa-times"></i></button>
      }
      return (
        <div className="event_cont">
          <div className="event_title">{eventInfo.event.title}</div>
          {removeBtn}
        </div>
      )
    }

  }

  renderTools=()=>{
    let toolsJSX=[];
    let cal_sett= this.props.view_sett.settings.calendar_sett;

    toolsJSX.push(<button type={"button"} title="Prev" key={"btn_calendar_prev"}
                           disabled={false}
                           onClick={this.prevDateCal}
                           className={"pp_c pp_c_btn "}>
        <i className="fas fa-chevron-left"/>
    </button>);

    let format='MM/yyyy';
    let onlyMonthChose=true;
    if(this.state.calendarType!='dayGridMonth'){
      format='dd/MM/yyyy';
      onlyMonthChose=false;
    }
    toolsJSX.push(<DatePicker key={'btn_calendar_date'}
        className={'date_control form-control'}
        dateFormat={format}
        placeholderText={"Enter date"}
        selected={this.state.chosenDate}
        title={this.state.chosenDate}
        popperProps={{
          positionFixed: true
        }}
        showMonthYearPicker={onlyMonthChose}
        onChange={this.setDateCal}
    />)

    toolsJSX.push(<button type={"button"} title="Next" key={"btn_calendar_next"}
                           disabled={false}
                           onClick={this.nextDateCal}
                           className={"pp_c pp_c_btn "}>
        <i className="fas fa-chevron-right"/>
    </button>);

    toolsJSX.push(<div key={"btn_calendar_group_type"} className="switch_btn_group">
                    <button type={"button"} className={(this.state.calendarType=='dayGridMonth'?'selected':'')} title="Month" onClick={(e)=>{this.changeTypeCal('dayGridMonth')}}>Month</button>
                    <button type={"button"} className={(this.state.calendarType=='timeGridWeek'?'selected':'')} title="Week" onClick={(e)=>{this.changeTypeCal('timeGridWeek')}}>Week</button>
                    <button type={"button"} className={(this.state.calendarType=='timeGridDay'?'selected':'')} title="Day" onClick={(e)=>{this.changeTypeCal('timeGridDay')}}>Day</button>
                </div>);

    if(cal_sett.calendar_type==1){
        toolsJSX.push(this.renderUserSelect())
    }

    const dataHZTabs_tools = {
        callbackRecalc: () => {},
        className: "pp_tollbar_tools",
        dataJSX: toolsJSX
    };

    return (
        <HZTabs {...dataHZTabs_tools} />
    );
  }
  renderUserSelect=()=>{
    let chosen_value=this.state.usersData.find((item)=>{
      return this.state.chosenUser && this.state.chosenUser.uid==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value=null;
    }
    const customStyles = {
      control: (provided, state) => ({
            ...provided,
            minHeight:26,
            height:26,
      }),
      container: (provided, state) => ({
            ...provided,
            height:26,
      }),
      indicatorsContainer: (provided, state) => ({
            ...provided,
            height:26,
      }),
      dropdownIndicator: (provided, state) => ({
            ...provided,
            padding:'2px 4px',
      }),
      valueContainer:(provided, state) => ({
            ...provided,
            padding:'1px 8px',
      }),
      menu:(provided, state) => ({
            ...provided,
            zIndex:250,
            background:'white',
      }),
    }
    return (<div className="user_select">
        <Select
          styles={customStyles}
          menuPosition={'fixed'}
          options={this.state.usersData}
          value={chosen_value}
          maxHeight={26}
          placeholder={'-Chose User-'}
          onChange={this.choseUser}
        />
      </div>
    )
  }
  renderTasks=()=>{
    let TaskList=null;
    if(this.props.view_sett.settings.calendar_sett.tasks && this.props.view_sett.settings.calendar_sett.tasks.enable_task==1){
      let class_cal='';
      let list_data=[];
      if(this.props.view_sett.settings.calendar_sett.calendar_type==0){
        let tasks=this.state.tasks.filter((item)=>{
          return item.start?false:true;
        })
        class_cal='audit_task';
        list_data=[
          {
            id:'auditor_list',
            title:'User Tasks',
            dragToCalendar:true,
            tasks:tasks,
          },
        ];
      }
      if(this.props.view_sett.settings.calendar_sett.calendar_type==1){
        let tasks=this.state.tasks.filter((item)=>{
          return item.start?false:true;
        })
        class_cal='manager_task';
        list_data=[
          {
            id:'manager_task',
            title:'Available Tasks',
            tasks:this.state.manager_tasks,
            onDropTask:this.dropToManager
          },
          {
            id:'user_task',
            title:'User Tasks',
            //dragToCalendar:true,
            tasks:tasks,
            onDropTask:this.dropToUser
          }
        ];
      }

      TaskList=<ListComponent list_data={list_data} class={class_cal} />;
    }

    return TaskList;
  }

  render() {

    let task_set_events=this.state.tasks.filter((item)=>{
      return item.start?true:false;
    })
    let events=[...this.state.events, ...task_set_events];

    let cal_sett= this.props.view_sett.settings.calendar_sett;
    let adv_stt={};
    let class_wrapper='manager_cal_wrapper';
    if(cal_sett.calendar_type!=1){
      adv_stt={
        editable:true,
        selectable:true,
        weekends:true,
        droppable:true,
      }
      class_wrapper='';
    }
    return (
      <div className="cool_calendar_view">
        <div className="cool_calendar_tollbar">
            {this.renderTools()}
        </div>
        <div className={'cool_calendar_wrapper '+class_wrapper}>
          {this.renderTasks()}
          <div className="calendar_cont">
            <FullCalendar
              ref={this.calendar_api}
              plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
              headerToolbar={false}
              initialView='dayGridMonth'

              {...adv_stt}

            /*  views={{
                dayGrid: {
                    showNonCurrentDates:false,
                }
                timeGridWeekShort: {
                      type: 'timeGrid',
                      minTime:'08:00:00',
                      maxTime:'20:00:00',
                      slotMinTime:'08:00:00',
                      slotMaxTime:'20:00:00',
                      duration: { days: 7 },
                },
                timeGridDayShort:{
                    type: 'timeGrid',
                    minTime:'08:00:00',
                    maxTime:'20:00:00',
                    duration: { days: 1 },
                }
              }}*/
              drop={this.dropTask}
              eventChange={this.eventChange}
              eventOverlap={(e)=>{
                console.log(e);
                return true;
              }}
              events={events} // alternatively, use the `events` setting to fetch from a feed
              select={this.handleDateSelect}
              eventContent={this.renderEventContent}
              eventReceive={(e)=>{
                console.log(e)
                return false;
              }}
              /* you can update a remote database when these fire:
              eventAdd={function(){}}
              eventChange={function(){}}
              eventRemove={function(){}}
              */
      />
          </div>
        </div>
      </div>
    )
  }


}

interface IListData{
  id:string,
  title:string,
  tasks:ICalendarEvents[],
  dragToCalendar?:boolean,
  onDropTask?:(data)=>void
}
interface IListComponentProps{
  class:string,
  list_data:IListData[]
}
interface IListComponentState{
  list_data:IListData[]
  search_text:{[id:string]:string},
  hover_list:string,
  drag_data:{
    list_id:string,
    task_data:ICalendarEvents
  }

}
export class ListComponent extends React.Component<IListComponentProps, IListComponentState>{
  private list_refs: any;
  constructor(props){
    super(props);
    this.list_refs={};
    this.props.list_data.forEach((item)=>{
      this.list_refs[item.id]=React.createRef();
    })
    this.state={
      list_data:this.props.list_data,
      search_text:{},
      drag_data:null,
      hover_list:null,
    };
  }
  componentDidMount(){
    this.initList();
  }
  componentDidUpdate(prevProps){
    let isShoudUpdate=false;
    this.props.list_data.forEach((list_item)=>{
        let old_list= prevProps.list_data.find((old_list_item)=>{
            return list_item.id==old_list_item.id?true:false;
        })
        if(!old_list || old_list.tasks.length!=list_item.tasks.length){
          isShoudUpdate=true;
        }
    });
    if(isShoudUpdate){
      this.setState({list_data:this.props.list_data}, ()=>{
      //  this.initList();
      });

    }
  }

  initList=()=>{
    this.state.list_data.forEach((list_item)=>{
      if(list_item.dragToCalendar){
        new Draggable(this.list_refs[list_item.id].current, {
            itemSelector: ".task_item",
            eventData: function(eventEl) {
              return {
                allDay:true,
                create:false,
              };
            },
            appendTo:this.list_refs[list_item.id].current,
          });
      }

    })
  }

  searchChange=(e, list_id)=>{
    let {value}=e.currentTarget;
    this.setState({search_text:{...this.state.search_text, [list_id]:value}})
  }
  onDragStart=(list_id, task)=>{
    this.setState({drag_data:{list_id:list_id, task_data:task}});
  }
  onDrop=(e, list_id)=>{
    let drag_data=this.state.drag_data;
    if(drag_data && list_id!=drag_data.list_id){
      let list_data=this.state.list_data
      let drop_list=list_data.find((list_data)=>{
        return list_data.id==list_id?true:false;
      })

      let drag_list=list_data.find((list_data)=>{
        return list_data.id==drag_data.list_id?true:false;
      })

      if(drag_list && drop_list){
        drag_list.tasks=drag_list.tasks.filter((task_item)=>{
          return task_item.id==drag_data.task_data.id?false:true;
        })
        drop_list.tasks=[drag_data.task_data, ...drop_list.tasks];
      }
      this.setState({list_data:list_data, hover_list:null, drag_data:null}, ()=>{
        if(drop_list.onDropTask){
          drop_list.onDropTask(drag_data.task_data);
        }
      })
    }
  }
  onDragEnd=(e)=>{
      this.setState({hover_list:null, drag_data:null});
  }
  onDragEnter=(e, list_id)=>{
    let drag_data=this.state.drag_data;
    if(drag_data && list_id!=drag_data.list_id){
      this.setState({hover_list:list_id});
    }
  }
  onDragLeave=()=>{
    this.setState({hover_list:null});
  }

  renderList=(data)=>{
    let searchText=this.state.search_text[data.id]?this.state.search_text[data.id]:'';
    let tasks_elem=data.tasks.filter((task_item)=>{
      return task_item.title.toLowerCase().includes(searchText.toLowerCase());
    }).map((task_item)=>{
      return this.renderTask(data.id, task_item);
    });
    if(this.state.hover_list==data.id){
      tasks_elem=[<div key={'key_empty_task'} className="task_item empty_task"></div>,...tasks_elem]
    }
    return(<div
          ref={this.list_refs[data.id]}
          onDragOver={(e)=>{e.preventDefault();}}
          onDragEnter={(e)=>{this.onDragEnter(e, data.id)}}
          onDragLeave={()=>{this.onDragLeave()}}
          onDrop={(e)=>{this.onDrop(e, data.id)}}
          key={'list_key'+data.id}
          className={'tasks_cont_wrapper '+(this.state.drag_data && this.state.drag_data.list_id!=data.id?'hover_list':'')}>
              <div className="title_list">{data.title}</div>
              <div className="search_list"><input placeholder="Search..." className="form-control" value={searchText} onChange={(e)=>{this.searchChange(e, data.id)}} /></div>
              <div className="task_list">
                  {
                    tasks_elem
                  }
              </div>
    </div>);
  }
  renderTask=(list_id, data)=>{
    let date_limit=null;
    if(data.date_limit){
      date_limit=<div className="date"><div>Done before:</div>{moment(data.date_limit).format(DateFormat)}</div>
    }
    let style={
      backgroundColor:data.backgroundColor?data.backgroundColor:'#fff',
      borderColor:data.borderColor?data.borderColor:'#05c0fa',
      color:data.textColor?data.textColor:'#5d6e74',
    }
    return(<div style={style} data-uid={data.id} draggable="true" onDragStart={(e)=>{this.onDragStart(list_id, data)}} onDragEnd={this.onDragEnd} key={'list_task_'+list_id+data.id} className="task_item">
        <div className="text">{data.title}</div>
        {date_limit}
    </div>)
  }

  render(){
    let lists=this.state.list_data.map((list_item)=>{
      return this.renderList(list_item);
    })
    return(
      <div className={'tasks_cont '+this.props.class}>
        {lists}
      </div>
      );
  }
}
