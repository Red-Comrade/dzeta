import * as React from "react"
import {DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'



interface DnDListOrderProps {
  items:{
    id:string,
    content:string,
  }[],
  events:{
    onDragEnd:(result)=>void,
  }
}
interface DnDListOrderState {
  items:{
    id:string,
    content:string,
  }[],
}
export class DnDListOrder extends React.Component<DnDListOrderProps, DnDListOrderState> {
    constructor(props){
      super(props);
      this.state={
        items:[],
      }
    }

    componentDidMount(){
      this.setState({items:this.props.items});
    }

    getConeinerStyle=(isDraggingOver)=>({
      'padding': 8,
      'backgroundColor': 'rgb(235, 236, 240)',
    })

    getItemsStyle=(draggableStyle, isDragging)=>({

      'padding': '5px 25px',
      'backgroundColor': isDragging?'rgb(222, 235, 255)':'white',
      'border':isDragging?'2px solid rgba(9, 30, 66, 0.71)':'none',
      'boxShadow':isDragging?'#a5adba 2px 2px 1px':'none',
      'marginTop': 8,
      'borderRadius': 4,


      ...draggableStyle
    })

    render() {
    let events={}, keys=Object.keys(this.props.events);
    keys.forEach((key)=>{
      events[key]=this.props.events[key].bind(this);
    })
     return (
       <DragDropContext {...events}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={this.getConeinerStyle(snapshot.isDraggingOver)}
              {...provided.droppableProps}
            >
              {this.state.items.map((item, index) => (
                <Draggable
                  key={item.id}
                  draggableId={item.id}
                  index={index}
                >
                  {(provided, snapshot) => (
                    <div>
                      <div
                        ref={provided.innerRef}
                        {...provided.dragHandleProps}
                        {...provided.draggableProps}
                        style={this.getItemsStyle(
                          provided.draggableProps.style,
                          snapshot.isDragging
                        )}
                      >
                        {item.content}
                      </div>
                      {provided.placeholder}
                    </div>
                   )}
                </Draggable>
               ))}
              {provided.placeholder}
            </div>
           )}
        </Droppable>
      </DragDropContext>
     );
  }

}
