import * as React from "react";
import { Table } from 'react-bootstrap';


interface IClassRelationProps{
  viewUid:string,
  DataFunction:{
    onClassclick:(class_data:any)=>void,
    onChange:(e:any, uid:string)=>void,
  },
  Classes:{DependentClass:{uid:string, name:string}, MainClass:{uid:string, name:string}}[],
}
export class ClassRelation extends React.Component<IClassRelationProps>{
    handleChange=(e, uid)=>{
      this.props.DataFunction.onChange(e, uid);
    }
    clickClass=(class_data)=>{
      this.props.DataFunction.onClassclick(class_data);
    }
    typeSelectRender=(uid, chosen)=>{
      let Option= this.props.Classes.filter((item)=>{
        return uid!==item.DependentClass.uid?true:false;
      }).map((item, index)=>{
          return <option key={'clasrel_'+uid+'_'+item.DependentClass.uid} value={item.DependentClass.uid}>{item.DependentClass.name}</option>
      });
      Option=[<option key={'clasrel_'+uid+'_-1'} value={'-1'}>{'-Select Class-'}</option>, <option key={'clasrel_'+uid+'_00000000-0000-0000-0000-000000000000'} value={'00000000-0000-0000-0000-000000000000'}>{'Main Class'}</option>,...Option];
      return <select value={chosen.uid} onChange={(e)=>{this.handleChange(e, uid)}} className="form-control">{Option}</select>
    }
    render(){
      let DataRow = this.props.Classes.map((item)=>{
          return <tr key={'linkclass_'+item.DependentClass.uid}><td><a href="#" onClick={(e)=>{e.preventDefault();this.clickClass(item.DependentClass)}}>{item.DependentClass.name}</a></td><td>{this.typeSelectRender(item.DependentClass.uid, item.MainClass)}</td></tr>
      });
      return(<Table key={'class_link_'+this.props.viewUid}>
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Relation</th>
                  </tr>
                </thead>
                <tbody>
                    {DataRow}
                </tbody>
            </Table>
          );
    }
}
