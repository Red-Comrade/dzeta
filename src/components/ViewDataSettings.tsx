import * as React from "react";


import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';
import Select, { components } from 'react-select';
import * as ct_api_class from '../api/ClassTree-api'


import {DnDListOrder} from "./DnDListOrder"

export interface IViewDataSettingsProps {
    DataFunction: {
      onChange:(data:object)=>void,
    },
    viewUid: string,
    Settings:{
      settings:{
        display_pers_obj:number,
        table_or_form:number,
        display_number_of_row:number,
        view_sorts:{order:string, params:[]},
        table_edit_mode:number,
        form_view_mode:number,
        class_for_open_form:string,
        select_first_row:number,
        exe_data_source_method:number,
        collor_setting_calc:{uid:string, name:string},
        collor_setting_type:number,
      },
      attrs:{
        uid:string,
        name:string,
        class:{uid:string, name:string}
      }[],
    },
}
export interface IViewDataSettingsState {
      Settings:{
        attrs:{
          uid:string,
          name:string,
          class:{uid:string, name:string}
        }[],
        settings:{
          display_pers_obj:number,
          table_or_form:number,
          display_number_of_row:number,
          view_sorts:{order:string, params:[]},
          table_edit_mode:number,
          form_view_mode:number,
          class_for_open_form:string,
          select_first_row:number,
          exe_data_source_method:number,
          collor_setting_calc:{uid:string, name:string},
          collor_setting_type:number,
        }
      },
      tabs:string,
      searchInput:string,
}

const IOSSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 42,
            height: 26,
            padding: 0,
        },
        switchBase: {
            padding: 1,
            '&$checked': {
                transform: 'translateX(16px)',
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#17a2b8',
                    opacity: 1,
                    border: 'none',
                },
            },
            '&$focusVisible $thumb': {
                color: '#17a2b8',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 24,
            height: 24,
        },
        track: {
            borderRadius: 26 / 2,
            border: `1px solid ${theme.palette.grey[400]}`,
            backgroundColor: theme.palette.grey[50],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
            background: '#007bfe',
        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }:any) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});

export class ViewDataSettings extends React.Component<IViewDataSettingsProps, IViewDataSettingsState>{
    constructor(props){
      super(props);
      this.state={
        Settings:this.props.Settings,
        tabs:'all',
        searchInput:'',
      };
    }
    componentDidUpdate(prevProps, prevSate){
      if(prevProps.viewUid!=this.props.viewUid || (prevProps.Settings.attrs.length!=this.props.Settings.attrs.length)){
        this.setState({Settings:this.props.Settings});
      }
    }

    searchChange=(e)=>{
      let {value}=e.currentTarget;
      this.setState({searchInput:value});
    }
    clickTabs=(tabs)=>{
      this.setState({tabs:tabs});
    }
    settChange=(e)=>{
      let Settings=this.state.Settings;
      let {id, value}=e.currentTarget;
      value=value===''?null:value;
      Settings.settings[id]=value;
      this.setState({Settings:Settings}, ()=>{
          this.props.DataFunction.onChange(this.state.Settings);
      })

    }

    getStrucSett=()=>{
        let Settings=this.state.Settings;
        let strucSett=[
          {tabs:'all', title:'All Settings', Settinngs:[
            {key:'display_pers_obj', title:'Permission output', value:Settings.settings.display_pers_obj},
            {key:'table_or_form', title:'Type of View', value:Settings.settings.table_or_form},
            {key:'exe_data_source_method', title:'Execute Data Source method on load table', value:Settings.settings.exe_data_source_method},
            {key:'class_for_open_form', title:'Open nested form for', value:Settings.settings.class_for_open_form},
            {key:'display_number_of_row', title:'Display number of row', value:Settings.settings.display_number_of_row},
            {key:'view_sorts', title:'Sorts', value:Settings.settings.view_sorts},
            {key:'table_edit_mode', title:'Default mode', value:Settings.settings.table_edit_mode},
            {key:'select_first_row', title:'Selecting first row', value:Settings.settings.select_first_row},
            {key:'collor_setting_calc', title:'Calc for coloring', value:Settings.settings.collor_setting_calc},
            {key:'collor_setting_type', title:'Async coloring', value:Settings.settings.collor_setting_type},
          ]},
          {tabs:'common', title:'Common Settings', Settinngs:[
            {key:'display_pers_obj', title:'Permission output', value:Settings.settings.display_pers_obj},
            {key:'table_or_form', title:'Type of View', value:Settings.settings.table_or_form},
            {key:'exe_data_source_method', title:'Execute Data Source method on load table', value:Settings.settings.exe_data_source_method},
            {key:'class_for_open_form', title:'Open nested form for', value:Settings.settings.class_for_open_form},
          ]},
          {tabs:'visual', title:'Visual Settings', Settinngs:[
            {key:'display_number_of_row', title:'Display number of row', value:Settings.settings.display_number_of_row},
            {key:'view_sorts', title:'Sorts', value:Settings.settings.view_sorts},
            {key:'table_edit_mode', title:'Default mode', value:Settings.settings.table_edit_mode},
            {key:'select_first_row', title:'Selecting first row', value:Settings.settings.select_first_row},
            {key:'collor_setting_calc', title:'Calc for coloring', value:Settings.settings.collor_setting_calc},
            {key:'collor_setting_type', title:'Async coloring', value:Settings.settings.collor_setting_type},
          ]},
        ];
        if(Settings && Settings.settings && Settings.settings.table_or_form==1){
          //Представление - форма
          strucSett=[
           {tabs:'all', title:'All Settings', Settinngs:[
              {key:'display_pers_obj', title:'Permission output', value:Settings.settings.display_pers_obj},
              {key:'table_or_form', title:'Type of View', value:Settings.settings.table_or_form},
              {key:'form_view_mode', title:'Default mode', value:Settings.settings.form_view_mode},
            ]},
           {tabs:'common', title:'Common Settings', Settinngs:[
             {key:'display_pers_obj', title:'Permission output', value:Settings.settings.display_pers_obj},
             {key:'table_or_form', title:'Type of View', value:Settings.settings.table_or_form},
           ]},
           {tabs:'visual', title:'Visual Settings', Settinngs:[
             {key:'form_view_mode', title:'Default mode', value:Settings.settings.form_view_mode},
           ]},

         ];
        }else if(Settings && Settings.settings && Settings.settings.table_or_form==2){
          //Представление - вкладка
          strucSett=[];
        }else if(Settings && Settings.settings && Settings.settings.table_or_form==3){
          strucSett=[
            {tabs:'all', title:'All Settings', Settinngs:[
              {key:'display_pers_obj', title:'Permission output', value:Settings.settings.display_pers_obj},
              {key:'table_or_form', title:'Type of View', value:Settings.settings.table_or_form},
            ]},
            {tabs:'common', title:'Common Settings', Settinngs:[
              {key:'display_pers_obj', title:'Permission output', value:Settings.settings.display_pers_obj},
              {key:'table_or_form', title:'Type of View', value:Settings.settings.table_or_form},
            ]},
          ];
        }
        return strucSett;
    }

    renderSettings=(curSett)=>{
        let renderSett=curSett.Settinngs.filter((item)=>{
          return item.title.toLowerCase().includes(this.state.searchInput.toLowerCase());
        });
        return renderSett.map((item)=>{
          return(<div key={'sett_'+item.key} className="settings__setcont setcont">
                      <div className="row">
                        <label className="col-lg-6">{item.title}</label>
                        <div className="col-lg-6">
                          {this.renderSettByKey(item.key, item.value)}
                        </div>
                      </div>
                  </div>);
        });
    }

    renderSettByKey=(key, value)=>{
        let Settings=this.state.Settings;
        switch(key) {
          case 'display_pers_obj':
            return <select onChange={this.settChange} value={value} id="display_pers_obj" className="form-control">
                          <option value="0">Not limited</option>
                          <option value="1">Owner</option>
                          <option value="2">Owner group</option>
                          <option value="4">Edit by owner</option>
                          <option value="3">Edit by owner group</option>
                    </select>
          case 'table_or_form':
              return <select onChange={this.settChange} value={value} id="table_or_form" className="form-control">
                        <option value="0">Table</option>
                        <option value="1">Form</option>
                        <option value="2">Tab</option>
                        <option value="3">Calendar</option>
                    </select>
          case 'display_number_of_row':
                    return <select onChange={this.settChange} value={value} id="display_number_of_row" className="form-control">
                              <option value="0">None</option>
                              <option value="1">Display</option>
                        </select>
          case 'view_sorts':
                    return <ViewSortCtrl onChange={this.props.DataFunction.onChange} AppViewSett={Settings} />
          case 'table_edit_mode':
                  return <select onChange={this.settChange} value={value} id="table_edit_mode" className="form-control">
                            <option value="0">View</option>
                            <option value="1">Editing</option>
                        </select>
          case 'form_view_mode':
                  return <select onChange={this.settChange} value={value} id="form_view_mode" className="form-control">
                            <option value="0">Editing</option>
                            <option value="1">View</option>
                         </select>
          case 'class_for_open_form':
              return this.renderClassSelect(value);
          case 'select_first_row':
                return <select onChange={this.settChange} value={value} id="select_first_row" className="form-control">
                          <option value="0">None</option>
                          <option value="1">Select</option>
                      </select>
          case 'exe_data_source_method':
                return <select onChange={this.settChange} value={value} id="exe_data_source_method" className="form-control">
                          <option value="0">None</option>
                          <option value="1">Execute</option>
                </select>
          case 'collor_setting_type':
                  return <select onChange={this.settChange} value={value} id="collor_setting_type" className="form-control">
                           <option value="0">Sync</option>
                           <option value="1">Async</option>
                  </select>
          case 'collor_setting_calc':
                  return this.renderCalcSelect(value);

          default:
            return null;
        }
    }

    renderClassSelect=(value)=>{
      let exist_class=[];
      let Options=this.state.Settings.attrs.filter((attr)=>{
        if(attr.class && attr.class.uid && exist_class.indexOf(attr.class.uid)==-1){
          exist_class.push(attr.class.uid);
          return true;
        }
        return false;
      }).map((attr)=>{
        return <option key={'class_for_open_form_'+attr.class.uid} value={attr.class.uid}>{attr.class.name}</option>
      })
      Options=[<option key={'class_for_open_form_null'} value={''}>All classes</option>, ...Options]
      return (<select onChange={this.settChange} value={(value?value:'')} id="class_for_open_form" className="form-control">
                {Options}
             </select>)
    }
    renderCalcSelect=(value)=>{
      let Settings=this.state.Settings;
      let classes=[];
      Settings.attrs.forEach((param)=>{
        if(classes.indexOf(param.class.uid)==-1){
          classes.push(param.class.uid);
        }
      })
      return <CalcCtrl Value={value} Classes={classes} Change={(opt_data)=>{
        let value=null;
        if(opt_data.value){
          value = {uid:opt_data.value, name:opt_data.label, class_uid:opt_data.class_uid};
        }
        this.settChange({currentTarget:{value:value, id:'collor_setting_calc'}})
      }} />
    }

    render(){
      let strucSett=this.getStrucSett();
      let curSett=strucSett.find((item)=>{
        return item.tabs===this.state.tabs?true:false;
      });
      let tabs=strucSett.map((item)=>{
        return <li onClick={()=>{this.clickTabs(item.tabs)}} className={'tabs__item item '+(item.tabs===this.state.tabs?'selected':'')} key={'sett_key_'+item.tabs}>{item.title}</li>;
      })
      return(<div className="viewdatasettings">
        <div className="viewdatasettings__seacrh search">
            <label><i className="fa fa-search" aria-hidden="true"></i><input onChange={this.searchChange} type="text" placeholder="SEARCHSETTINGS" value={this.state.searchInput}/></label>
        </div>
        <div className="viewdatasettings__info info">
          <span className="info__sett">Settings <span className="count">{curSett.Settinngs.length}</span></span>
        </div>
        <ul className="viewdatasettings__tabs tabs">
          {tabs}
        </ul>
        <div className="viewdatasettings__settings settings">
          {curSett && this.renderSettings(curSett)}
        </div>
      </div>)
    }
}



export interface IViewSortCtrlProps {
  onChange:(AppViewSett:any)=>void,
  AppViewSett:any,
}
export interface IViewSortCtrlState {
  data:{id:number, uid:string, order:string}[],
}
class ViewSortCtrl extends React.Component<IViewSortCtrlProps, IViewSortCtrlState>{
  constructor(props){
    super(props);
    this.state={
      data:[],
    };
  }

  componentDidMount(){
    let AppViewSett= this.props.AppViewSett;
    let data=this.getDefDataSett(AppViewSett.attrs);
    if(AppViewSett.settings && AppViewSett.settings.view_sorts){
      data=AppViewSett.settings.view_sorts.map((item)=>{
        return {id:Math.floor(Math.random() * 10000000000), uid:item.uid, order:item.order};
      });
    }
    this.setState({data:data});
  }
  componentDidUpdate(prevProps){
    if(prevProps.AppViewSett.uid!=this.props.AppViewSett.uid || prevProps.AppViewSett.attrs.length!=this.props.AppViewSett.attrs.length){
      let AppViewSett= this.props.AppViewSett;
      let data=this.getDefDataSett(AppViewSett.attrs);
      if(AppViewSett.settings && AppViewSett.settings.view_sorts){
        data=AppViewSett.settings.view_sorts.map((item)=>{
          return {id:Math.floor(Math.random() * 10000000000), uid:item.uid, order:item.order};
        });
      }
      this.setState({data:data});
    }
  }

  getDefDataSett=(params)=>{
    let paramsSort=[];
    let existClass =[];
    //берем 1 параметр из каждого класа
    params.forEach((param)=>{
      if(param.class.uid && existClass.indexOf(param.class.uid)==-1){
        paramsSort.push(param.uid);
        existClass.push(param.class.uid);
      }
    })
    paramsSort = paramsSort.map((item)=>{
      return {id:Math.floor(Math.random() * 10000000000), uid:item, order:'asc'}
    })
    return paramsSort;
  }

  getItemsForParam=(data, all_params)=>{
    let exist_params=[];
    let Items=[];
    Items = data.filter((param)=>{
      let saved_param = all_params.find((item)=>{
        return item.uid==param.uid?true:false;
      })
      return saved_param || !data.uid?true:false;
    }).map((param)=>{
      let saved_param = all_params.find((item)=>{
        return item.uid==param.uid?true:false;
      })
      return {
        id:'drag_'+param.id,
        param_id:param.id,
        content:<div className="sort_view_item">
                  <div className="drag_item"><i className="p-table-reorderablerow-handle pi pi-bars"></i></div>
                  <div className="param_item">{this.renderParams(param, saved_param, all_params, data)}</div>
                  <div className="sort_order_item">{this.renderOrder(param)}</div>
                  <div className="remove_btn_item"><button onClick={()=>{this.removeParam(param.id)}} type="button" className="btn remove_btn"><i className="fa fa-times"></i></button></div>
              </div>
      }
    });
    return Items;
  }


  renderParams=(data, saved_param, all_params, added_params)=>{
    added_params=added_params.map((item)=>{
      return item.uid;
    })
    let Options=all_params.filter((item)=>{
      return added_params.indexOf(item.uid)==-1 || (data && item.uid==data.uid)?true:false;
    }).map((item)=>{
      return {value:item.uid , label:item.class.name+'.'+item.name};
    });
    Options=[{value:null, label:'-Chose param-'}, ...Options];
    let chosen_value=Options.find((item)=>{
      return data && data.uid==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }
    return(<div className="param_value"><Select
        menuPosition={'fixed'}
        options={Options}
        value={chosen_value}
        onChange={(opt_data)=>{this.changeParam(data.id, 'uid', opt_data.value);}}
      /></div>);
  }
  renderOrder=(data)=>{
    return(<React.Fragment>
          {(data.order=='asc'?<span className="switch_label"><i className="fas fa-sort-alpha-down"></i> ASC</span>:<span className="switch_label"><i className="fas fa-sort-alpha-down-alt"></i> DESC</span>)}
                <IOSSwitch checked={data.order=='asc'?true:false}
                           onChange={(e)=>{
                             let order=data.order;
                             let value=order=='asc'?'desc':'asc';
                             this.changeParam(data.id, 'order', value);
                           }} />
          </React.Fragment>)
  }

  addParam=(e)=>{
    let param={id:Math.floor(Math.random() * 10000000000), uid:null, order:'asc'};
    let data=this.state.data;
    data.push(param);
    this.setState({data:data});
  }
  removeParam=(id)=>{
    let data=this.state.data;
    data=data.filter((item)=>{
      return item.id==id ?false:true;
    })
    this.setState({data:data}, ()=>{
        this.saveStore();
    });
  }
  changeParam=(id, name, value)=>{
      let data=this.state.data;
      data=data.map((item)=>{
        if(item.id==id){
          item[name]=value;
        }
        return item;
      })
      this.setState({data:data}, ()=>{
          this.saveStore();
      });
  }

  saveStore=()=>{
    let AppViewSett= this.props.AppViewSett;
    let data=this.state.data;
    let storeData=data.filter((item)=>{
      return item.uid?true:false;
    }).map((item)=>{
      return {uid:item.uid, order:item.order};
    });
    console.log(data);
    AppViewSett.settings.view_sorts=storeData.length?storeData:null;
    this.props.onChange(AppViewSett);
  }

  render(){
    let AppViewSett= this.props.AppViewSett;
    let Items= this.getItemsForParam(this.state.data, AppViewSett.attrs);
    let that=this;
    let events={
      onDragEnd:function(result){
        const reorder=(list, startIndex, endIndex)=>{
          const result = Array.from(list);
          const [removed] = result.splice(startIndex, 1);
          result.splice(endIndex, 0, removed);

          return result;
        };

        if(!result.destination) {
           return;
        }
        console.log(this.state);
        const items = reorder(
          this.state.items,
          result.source.index,
          result.destination.index
        );

        this.setState({
          items:items
        }, ()=>{
          let data=that.state.data;
          let sortedData=items.map((item:any)=>{
            return item.param_id;
          })
          data.sort((a, b)=>{
            return sortedData.indexOf(a.id)-sortedData.indexOf(b.id)
          });
          that.setState({data:data}, ()=>{
            that.saveStore();
          })
        });

      },
    };
    return(<div className="sort_view_ctrl">
        <div key={'dnd_key'+Math.floor(Math.random() * 10000000000)} className="sort_list_ctrl"><DnDListOrder items={Items} events={events}/></div>
        <div className="sort_add_param_ctrl"><button onClick={this.addParam} type="button" className="btn btn-sm"><i className="fa fa-plus" aria-hidden="true"></i> Add Param</button></div>
      </div>);
  }
}


export interface ICalcCtrlProps {
  Value:{uid:string, name:string, class_uid:string},
  Classes:string[],
  Change:(opt_data)=> void,
}
export interface ICalcCtrlState {
  Options:{value:string, label:string, class_uid:string}[],
  loaded:boolean,
}
class CalcCtrl  extends React.Component<ICalcCtrlProps, ICalcCtrlState>{
  state={
    Options:[],
    loaded:false,
  }
  loadCalc=()=>{
      if(!this.state.loaded){
          let classes_promise=this.props.Classes.map((uid)=>{
            return ct_api_class.getCalcs(uid);
          });
          Promise.all(classes_promise).then((res)=>{
              let all_calc=[];
              res.forEach((res_i:any, i)=>{
                if(res_i.data.result==1){
                  res_i.data.data.forEach((item)=>{
                      item.class_uid=this.props.Classes[i];
                      return item;
                  })
                  all_calc=[...all_calc, ...res_i.data.data];
                }
              });

              let Options_data=all_calc.map((item)=>{
                return {label:item.name, value:item.uid, class_uid:item.class_uid}
              });
              Options_data=[{value:null, label:'-Chose Calc-', class_uid:null}, ...Options_data];
              this.setState({Options:Options_data, loaded:true});
          });
      }
  }

  changeCalc=(opt_data)=>{
    this.props.Change(opt_data);
  }

  render(){
    let chosen_value={value:null, label:'-Chose Calc-', class_uid:null};
    if(this.props.Value){
      chosen_value={value:this.props.Value.uid, label:this.props.Value.name, class_uid:this.props.Value.class_uid};
    }
    return(
      <Select
          menuPosition={'fixed'}
          options={this.state.Options}
          value={chosen_value}
          onMenuOpen={this.loadCalc}
          onChange={this.changeCalc}
        />
    );
  }
}
