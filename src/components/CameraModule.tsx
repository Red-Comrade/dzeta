import * as React from "react";
import * as ReactDOM from "react-dom";
import * as moment from 'moment';

import './CameraModule.scss'


interface ICameraModuleProps {
  onSave:(file:any)=> void,
  onClose:()=> void,
}

interface ICameraModuleState {
  showCamera:boolean,
  file_data:any[],
  devices:any[],
  chosen_device:string,
  stream:any,
  showflash:boolean,
}

export class CameraModule extends React.Component<ICameraModuleProps, ICameraModuleState>{
  private video: React.RefObject<HTMLVideoElement>;
  constructor(props){
      super(props);
      this.video = React.createRef();
      this.state={
        showCamera:false,
        file_data:[],
        devices:[],
        chosen_device:'',
        stream:null,
        showflash:true,
      }
  }
  componentDidMount(){
    this.updateDevices().then((DataDevice)=>{
      this.setState({devices:DataDevice})
    });
  }

  handleClick=(e)=>{
    window.navigator.getUserMedia = window.navigator.getUserMedia || window.navigator.webkitGetUserMedia || window.navigator.mozGetUserMedia || window.navigator.msGetUserMedia || window.navigator.mediaDevices.getUserMedia;
    if(this.state.devices.length && window.navigator.getUserMedia){
      this.setState({showCamera:true}, ()=>{this.startStream()});
    }else{
      window.alert('Can\'t open camera');
    }
  }
  deviceChange=(e)=>{
    let {value}=e.currentTarget;
    this.setState({chosen_device:value}, ()=>{
      this.startStream();
    })
  }
  turnLight=(track, savePhoto)=>{
    if(track.applyConstraints && this.state.showflash){
      track.applyConstraints({
        advanced: [{torch: true}]
      }).then(()=>{
        setTimeout(()=>{
          savePhoto();
        }, 600);

      }).catch(()=>{
        savePhoto();
      });
    }else{
      savePhoto();
    }
  }

  startStream=()=>{
    if (this.state.stream) {
        this.state.stream.getTracks().forEach((track)=> {
            track.stop();
        });
    }
    let video_data:any={
        width: { max: 2160, min:1280, },
        height: { max: 2160, min:720, },
        facingMode: "environment",
      };
    if(this.state.chosen_device){
       video_data={
        width: { max: 2160, ideal:2048, min:1280, },
        height: { max: 2160, ideal:1080,  min:720, },
        deviceId: this.state.chosen_device,
       }
    }
    window.navigator.getUserMedia(
            {
              video: video_data
            },
            (stream)=>{
              this.updateDevices().then((DataDevice)=>{
                let deviseInfo = stream.getTracks()[0].getCapabilities();
                this.setState({stream:stream, devices:DataDevice, chosen_device:deviseInfo.deviceId}, ()=>{
                  this.video.current.srcObject = stream;
                  this.video.current.play();

                })
              });

            },
            (err)=>{
                console.error(err);
            });
  }
  updateDevices=()=>{
    return navigator.mediaDevices.enumerateDevices()
      .then((deviceInfos)=> {
        var DataDevice = deviceInfos.filter((device)=>{
          return device.kind === 'videoinput'?true:false;
        })
        return DataDevice;
    });
  }


  makeShapshot= ()=>{
    if(this.state.stream){
      let track = this.state.stream.getVideoTracks()[0];
      this.turnLight(track, this.savePhoto)
    }

  }
  savePhoto=()=>{
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    this.video.current.width=this.video.current.videoWidth;
    this.video.current.height =  this.video.current.videoHeight;
    canvas.width = this.video.current.videoWidth;
    canvas.height = this.video.current.videoHeight;
    ctx.drawImage(this.video.current, 0,0, this.video.current.videoWidth, this.video.current.videoHeight);
    this.video.current.width=null;
    this.video.current.height = null;
    let image_data = canvas.toDataURL('image/jpeg').substring('data:image/jpeg;base64,'.length);
    let data = atob(image_data),
            asArray = new Uint8Array(data.length);

    for (var z = 0, len = data.length; z < len; ++z) {
        asArray[z] = data.charCodeAt(z);
    }

    let blob = new Blob([asArray.buffer], {type: 'image/jpeg'});
    let name = moment().format('DD-MM-yyyy')+'_'+Math.round(Math.random()*1000000000).toString()+'.jpeg';
    let file = new window.File([blob], name, {'type': 'image/jpeg'});
    if (this.state.stream) {
        this.state.stream.getTracks().forEach((track)=> {
            track.stop();
        });
    }
    this.setState({showCamera:false, file_data:[], stream:null, chosen_device:null}, ()=>{
      this.props.onSave(file);
    });

  }
  close=()=>{
    if (this.state.stream) {
        this.state.stream.getTracks().forEach((track)=> {
            track.stop();
        });
    }
    this.setState({showCamera:false, file_data:[], stream:null, chosen_device:null}, ()=>{
      this.props.onClose();
    });
  }
  clickFlash=()=>{
    this.setState({showflash:!this.state.showflash});
  }

  renderDevicesSelect=()=>{
    let DeviceOpt=this.state.devices.filter((item)=>{
      return item.deviceId?true:false;
    }).map((item, i)=>{
      return <option key={'device_'+item.deviceId} value={item.deviceId}>{(item.label?item.label:'Camera_'+i)}</option>
    })
    let select= DeviceOpt.length?<select value={this.state.chosen_device} onChange={this.deviceChange} className="form-control">{DeviceOpt}</select>:null;
    return select;
  }
  renderVideoel=()=>{

    return(<div className="camera_module">
        <div className="camera_module_devices">
          {this.renderDevicesSelect()}
        </div>
        <div className="camera_module_ctrl">
          <button onClick={this.close} type="button" title="Close" className="camera_module_btns close_btn" ><i className="fa fa-undo"></i></button>
          <button onClick={this.makeShapshot} type="button" title="Snapshot" className="camera_module_btns save_btn" ><span></span></button>
          <button onClick={this.clickFlash} type="button" title="Flash" className="camera_module_btns flash_btn" ><i className="fa fa-bolt bolt">{!this.state.showflash && <i className="fa fa-slash off"></i>}</i></button>
        </div>
        <div className="camera_module_video">
          <video ref={this.video} className="video-js"></video>
        </div>
    </div>)
  }

  render(){
    let videoEl = this.state.showCamera?this.renderVideoel():null;
    return(<React.Fragment>
            <button onClick={this.handleClick} type="button" title="Camera" className="file_btns nf_btn-ellipse" ><i className="fas fa-camera"/></button>
            {videoEl}
      </React.Fragment>);
  }
}
