import * as React from "react";
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import Select, { components } from 'react-select';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';
import { WithContext as ReactTags } from 'react-tag-input';

export interface IViewDataSettingsProps {
    DataFunction: {
      onChange:(data:object)=>void,
    },
    AppViewSett: {
      settings:{
        viewFilters:FIlterElement[],
        filters_scheme:string,
        filters_conditions:{op:string, v:string, attr:string}[]
        apply_filter_before_or_after_single_mode:number,
      }
      ClasseLinks:{
        DependentClass:any,
        MainClass:any,
        Settings:any,
      }[],
    },
    AppParamData:{
      name:string,
      uid:string,
      class:{
        uid:string,
        name:string,
      },
      dopSett:any,
      isArray:boolean,
    }[],
}
export interface IFilterBlockState {
    data:FIlterElement
}
interface FIlterElement {
    id:string,
    children?:FIlterElement[],
    param?:string,
    value?:string,
    operation?:string,
    operator?:string,
}
export class FilterBlock extends React.Component<IViewDataSettingsProps, IFilterBlockState>{
  constructor(props){
    super(props);
    this.state={
      data:{
        id:'rootBlock',
        children:[],
      },
    };
  }
  componentDidMount(){
    //
    let saved_data=[]
    if(this.props.AppViewSett.settings.filters_scheme==undefined){
      //Если совсем нету данных для фильтров смотрим что есть у параметров(старые фильтры)
      saved_data=this.props.AppParamData.filter((item)=>{
        return item.dopSett.Filter && item.dopSett.Filter.length &&  item.dopSett.Filter[0] &&  item.dopSett.Filter[0].v?true:false;
      }).map((item, i)=>{
        return {
          id:Math.floor(Math.random() * 10000000000),
          param:item.uid,
          value: item.dopSett.Filter[0].v,
          operation: item.dopSett.Filter[0].op,
          operator:(i!=0?'AND':null),
        };
      })
    }else if(this.props.AppViewSett.settings && this.props.AppViewSett.settings.filters_scheme){
      let parsedData=this.parseStrToData(this.props.AppViewSett.settings.filters_scheme, this.props.AppViewSett.settings.filters_conditions);
      saved_data=parsedData.children;
    }
    this.setState({data:{id:'rootBlock', children:saved_data}});

  }

  addElemnt=(block_id, elemnet)=>{
    let data=this.state.data;
    let addRecursive=(data)=>{
      for (let index = 0; index < data.length; index++) {
        if(data[index].id==block_id){
          if(data[index].children.length>0){
            elemnet.operator='AND';
          }
          data[index].children.push(elemnet);
        }
        if(data[index].children && data[index].children.length){
          addRecursive(data[index].children);
        }
      }
    };
    addRecursive([data]);
    this.setState({data:data}, this.saveStore);
  }
  removeElement=(id)=>{
    let data=this.state.data;
    let removeRecursive=(data)=>{
      data=data.filter((item)=>{
        return item.id==id?false:true;
      }).map((item, i)=>{
        if(item.children && item.children.length){
          item.children= removeRecursive(item.children);
        }
        if(i==0){
          item.operator=null;
        }
        return item;
      })
      return data;
    };
    data.children=removeRecursive(data.children);
    this.setState({data:data}, this.saveStore);
  }
  changeData=(e, id)=>{
    let {name, value}=e.currentTarget;
    let data=this.state.data;
    let chosen_param=null;
    let not_val_oper=['IS NULL', 'IS NOT NULL'];
    if(name=='param'){
      chosen_param=this.props.AppParamData.find((item)=>{
        return item.uid==value?true:false;
      });
    }
    let changeRecursive=(data)=>{
      data=data.map((item)=>{
        if(item.id==id){
          item[name]=value;
          if(chosen_param && !chosen_param.isArray){
            item.operation='=';
            item.value='';
          }
          if(chosen_param && chosen_param.isArray){
            item.operation='ANY';
            item.value='';
          }
          if(name=='operation' && not_val_oper.indexOf(value)!==-1){
            item.value='';
          }
        }
        if(item.children && item.children.length){
          item.children= changeRecursive(item.children);
        }
        return item;
      })
      return data;
    };
    data.children=changeRecursive(data.children);
    this.setState({data:data}, this.saveStore);
  }
  parseTostr=(data, value_express, inBrack)=>{
    let expressions='';
    let not_value_oper=['IS NULL', 'IS NOT NULL'];
    data.forEach((item)=>{
      if(item.children && item.children.length){
        let childExpress=this.parseTostr(item.children, value_express, true);
        if(childExpress){
          expressions+=item.operator?' '+item.operator+' ':'';
          expressions+=childExpress;
        }
      }else{
        if(item.param && (item.value || not_value_oper.indexOf(item.operation)!==-1)){
          expressions+=expressions && item.operator?' '+item.operator+' ':'';
          expressions+='f'+(value_express.length+1);
          value_express.push({op:item.operation, attr:item.param, ...(item.value?{v:item.value}:{})});
        }
      }
    });
    if(expressions){
      return (inBrack?'('+expressions+')':expressions);
    }
    return null;
  }
  parseStrToData=(filters_scheme, filters_conditions)=>{
    let Data={
      id:'rootBlock',
      children:[],
    };
    const removeBlocksBracket = (string) => {
          string = string.substring(1);
          string = string.substring(0,string.length-1);
          return string;
    }
    const getStateData=(obj)=>{
      let Data=[];
      obj.forEach((item)=>{
        let elem;
        if(item.children && item.children.length){
          elem={
            id:Math.floor(Math.random() * 10000000000),
            children:[],
            operator:(item.operator?item.operator:null),
          }
          elem.children=getStateData(item.children);
        }else if(item.data){
          elem ={
            id:Math.floor(Math.random() * 10000000000),
            param:item.data.attr,
            value:item.data.v,
            operation:item.data.op,
            operator:(item.operator?item.operator:null),
          };
        }
        if(elem){
            Data.push(elem);
        }
      });
      return Data;
    }
    let mainobj={
      blocks:filters_scheme,
      start:0,
      end:filters_scheme.length,
      parrent:null,
      children:[],
    };
    let write_item=mainobj;
    for (let index = 0; index < filters_scheme.length; index++) {
      let char=filters_scheme[index];
      if(char=='f'){
        let index_express='';
        let i=1;
        while(filters_scheme.substr(index+i, 1) && Number(filters_scheme.substr(index+i, 1))){
          index_express+=filters_scheme.substr(index+i, 1);
          i++;
        }
        let elem={key:'f'+index_express, data:(filters_conditions && filters_conditions[Number(index_express)-1]?filters_conditions[Number(index_express)-1]:null), operator:null};
        if(write_item.children.length!=0){
          let i_oper=0;
          let operator='';
          while(filters_scheme[index-2-i_oper] && filters_scheme[index-2-i_oper]!=' '){
            operator+=filters_scheme[index-2-i_oper];
            i_oper++;
          }
          elem.operator=operator?operator.split("").reverse().join(""):null;
        }
        write_item.children.push(elem);
      }
      if(char=='('){
        let childObj={
          blocks:'',
          start:index,
          end:null,
          children:[],
          parrent:write_item,
          operator:null,
        };
        if(write_item.children.length!=0){
          let i_oper=0;
          let operator='';
          while(filters_scheme[index-2-i_oper] && filters_scheme[index-2-i_oper]!=' '){
            operator+=filters_scheme[index-2-i_oper];
            i_oper++;
          }
          childObj.operator=operator?operator.split("").reverse().join(""):null;
        }
        write_item.children.push(childObj);
        write_item=childObj;
      }
      if(char==')'){
        write_item.end=index;
        write_item.blocks=filters_scheme.substring(write_item.start, (write_item.end+1));
        write_item=write_item.parrent;
      }

    }
    //remove nonsense brackets
  //  mainobj=removeBlocksBracket(mainobj);
    if(mainobj.children.length){
      Data.children=getStateData(mainobj.children);
    }
    return Data;
  }

  saveStore=()=>{
    let data_saved=this.state.data.children;
    let value_express=[];
    let str_express=this.parseTostr(data_saved, value_express, false);
    console.log(str_express, value_express);
    let AppViewSett=this.props.AppViewSett;
    AppViewSett.settings.filters_scheme=str_express;
    AppViewSett.settings.filters_conditions=(value_express.length?value_express:null);
    this.props.DataFunction.onChange(AppViewSett);
  }


  addBlock=(block_id)=>{
    let elemnet={
      id:Math.floor(Math.random() * 10000000000),
      children:[{
        id:Math.floor(Math.random() * 10000000000),
        param:null,
        value:null,
        operation:'=',
        operator:null,
      }],
      operator:null,
    };
    this.addElemnt(block_id, elemnet);

  }
  addCondition=(block_id)=>{
    let elemnet={
      id:Math.floor(Math.random() * 10000000000),
      param:null,
      value:null,
      operation:'=',
      operator:null,
    };
    this.addElemnt(block_id, elemnet);

  }

  renderBlock=(data)=>{
    let content=data.children.map((item)=>{
      return item.children?this.renderBlock(item):this.renderCondition(item);
    })
    return(
      <div className="block_data" key={'block_'+data.id}>
        {data.operator && this.renderOperator(data)}
        <div className={(data.id=='rootBlock'?'root_filter_block':'filter_block')}>
          <div className="filter_block_btn">
            {data.id!=='rootBlock' &&  <button style={{color: '#766666'}} onClick={(e)=>{this.removeElement(data.id)}} className="btn btn-sm"><i className="fa fa-times" aria-hidden="true"></i></button>}
          </div>
          {content}
          <div className="filter_block_btn">
              <button style={{color: '#766666'}} onClick={(e)=>{this.addBlock(data.id)}} className="btn btn-sm"><i className="fa fa-plus" aria-hidden="true"></i> Add Block</button>
              <button style={{color: '#766666'}} onClick={(e)=>{this.addCondition(data.id)}} className="btn btn-sm"><i className="fa fa-plus" aria-hidden="true"></i> Add Condition</button>
          </div>
        </div>
      </div>
    );
  }
  renderCondition=(data)=>{
    return(<div className="condition_data" key={'condition_'+data.id}>
        {data.operator && this.renderOperator(data)}
        <div className="filter_item">
          {this.renderParams(data)}
          {this.renderOperatrion(data)}
          {this.renderValue(data)}
          <button onClick={(e)=>{this.removeElement(data.id)}} className="btn btn-danger btn-sm btn_remove_condition"><i className="fa fa-times" aria-hidden="true"></i></button>
        </div>
      </div>);
  }
  renderOperator=(data)=>{
    const operator=[
      {text:'AND',value:'AND'},
      {text:'OR',value:'OR'}
    ];
    let options=operator.map((item, i)=>{
      return <option key={'data_operator_'+i+'_'+data.id} value={item.value}>{item.text}</option>
    })
    return(<select onChange={(e)=>{this.changeData(e, data.id)}} name="operator" value={data.operator} className="form-control select_operator">
      {options}
    </select>);
  }
  renderParams=(data)=>{
    let Options=this.props.AppParamData.map((item)=>{
      return {value:item.uid , label:item.class.name+'.'+item.name};
    });
    Options=[{value:null, label:'-Chose param-'}, ...Options];
    let chosen_value=Options.find((item)=>{
      return data.param==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }
    return(<div className="param_value"><Select
        menuPosition={'fixed'}
        options={Options}
        value={chosen_value}
        onChange={(opt_data)=>{this.changeData({currentTarget:{name:'param', value:opt_data.value}}, data.id);}}
      /></div>);
  }
  renderOperatrion=(data)=>{
    let operation=['=', '!=', '>', '>=', '<', '<=', '~', 'IS NULL', 'IS NOT NULL'];
    if(data.param){
      let chosen_param=this.props.AppParamData.find((item)=>{
        return item.uid==data.param?true:false;
      });
      if(chosen_param && chosen_param.isArray){
        operation=['ANY', 'ALL', 'IS NULL', 'IS NOT NULL'];
      }
    }
    let options=operation.map((item, i)=>{
      return <option key={'data_operation_'+i+'_'+data.id} value={item}>{item}</option>
    })
    return(<select onChange={(e)=>{this.changeData(e, data.id)}} name="operation" value={data.operation} className="form-control select_operation">
      {options}
    </select>);
  }
  renderValue=(data)=>{
    let Options=this.props.AppParamData.map((item)=>{
      return {value:item.uid , label:item.class.name+'.'+item.name};
    });
    let disabled_value_oper=['IS NULL', 'IS NOT NULL'];
    Options=[{value:null, label:'-Chose param-'}, ...Options];
    let chosen_value=Options.find((item)=>{
      return data.param==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }
    let chosen_param, inputCtrl;
    if(data.param){
      chosen_param=this.props.AppParamData.find((item)=>{
        return item.uid==data.param?true:false;
      });
    }

    if(chosen_param && chosen_param.isArray){
      let tags=data.value?this.convertValueToTags(data.value):[];
      inputCtrl=<ReactTags
            tags={tags}
            allowDragDrop={false}
            handleAddition={(tag) => {
              tags.push(tag);
              this.changeData({currentTarget:{name:'value', value:this.convertTagsToValue(tags)}}, data.id)
            }}
            readOnly={disabled_value_oper.indexOf(data.operation)!==-1?true:false}
            handleDelete={(i) => {
              tags=tags.filter((tag, index) => index !== i);
              this.changeData({currentTarget:{name:'value', value:this.convertTagsToValue(tags)}}, data.id)
            }}
            autofocus={false}
            delimiters={[188, 13]}
            placeholder={""}
            classNames={{
                 selected: 'ReactTags__selected form-control',
            }}
        />
    }else{
      inputCtrl=<input  disabled={disabled_value_oper.indexOf(data.operation)!==-1?true:false} onChange={(e)=>{this.changeData(e, data.id)}} name="value" value={(data.value?data.value:'')} className="form-control"/>;
    }

    return(<div className="condition_value">
        {inputCtrl}
    </div>);
  }

  convertValueToTags=(value)=>{
    value=value.replaceAll(/{|}|\"/ig, '');
    let tags=value.split(',');
    tags=tags.map((item)=>{
      return {id:item, text:item};
    })
    return tags;
  }
  convertTagsToValue=(tags)=>{
    let str_tags=tags.map((tag)=>{
      return '"'+tag.text+'"';
    });
    return str_tags.length?'{'+str_tags.join(',')+'}':'';
  }

  render(){
    let Content=this.renderBlock(this.state.data);
    return(<div className="settings__setcont setcont">
      <div className="row"><label className="col-lg-12">View Filters</label></div>
      {Content}
    </div>)
  }
}

export interface ISingleModeBlockState {
    data:{
      id:number,
      param:string,
      condition:string,
    }[],
    switch:boolean,
    order:number,
}
export class SingleModeBlock extends React.Component<IViewDataSettingsProps, ISingleModeBlockState>{
  constructor(props){
    super(props);
    this.state={
      data:[],
      order:1,
      switch:false,
    };
  }
  componentDidMount(){
    if(this.props.AppViewSett.ClasseLinks && this.props.AppViewSett.ClasseLinks.length){
      let class_settings=this.props.AppViewSett.ClasseLinks.filter((item)=>{
        return item.Settings && item.Settings.single_mode_attr?true:false;
      });
      if(class_settings && class_settings.length){
        let data_filter=class_settings.map((item)=>{
          return {id:Math.floor(Math.random() * 10000000000), param:item.Settings.single_mode_attr.uid, condition:item.Settings.single_mode_op};
        })
        let order= this.props.AppViewSett.settings && this.props.AppViewSett.settings.apply_filter_before_or_after_single_mode?this.props.AppViewSett.settings.apply_filter_before_or_after_single_mode:1;
        this.setState({data:data_filter, switch:true, order:order})
      }
    }
  }
  changeData=(value)=>{
    let data=this.state.data;
    data=data.map((item)=>{
      if(item.id==value.id){
        item[value.name]=value.value;
      }
      return item;
    })
    this.setState({data:data}, ()=>{
      this.saveStore();
    });
  }
  changeOrder=(e)=>{
    let {value}=e.currentTarget;
    this.setState({order:value}, ()=>{
      this.saveStore();
    });
  }
  addFilter=()=>{
    let item={id:Math.floor(Math.random() * 10000000000), param:null, condition:'max'};
    let data= this.state.data;
    data.push(item);
    this.setState({data:data});
  }
  removeFilter=(id)=>{
    let data= this.state.data;
    data=data.filter((item)=>{
      return item.id==id?false:true;
    })
    this.setState({data:data}, ()=>{
      this.saveStore();
    });
  }
  saveStore=()=>{
    //dataFilter
    let data =this.state.data;
    let AppViewSett=this.props.AppViewSett;
    let nullData={single_mode_attr:null, single_mode_op:null};
    data=data.filter((item)=>{
      return item.param?true:false;
    });
    if(data.length){
      let class_used=[];
      data.forEach((item_data)=>{
        let chosen_param=this.props.AppParamData.find((item)=>{
          return item.uid==item_data.param?true:false;
        })
        class_used.push(chosen_param.class.uid);
        AppViewSett.ClasseLinks=AppViewSett.ClasseLinks.map((item)=>{
          if(item.DependentClass.uid==chosen_param.class.uid){
            item.Settings={single_mode_attr:{uid:item_data.param}, single_mode_op:item_data.condition};
          }
          return item;
        });
      });
      //removeOld data
      AppViewSett.ClasseLinks=AppViewSett.ClasseLinks.map((item)=>{
        if(class_used.indexOf(item.DependentClass.uid)==-1){
          item.Settings=nullData;
        }
        return item;
      });
      //Order
      let order= this.state.order;
      AppViewSett.settings.apply_filter_before_or_after_single_mode=order;
    }else{
      AppViewSett.ClasseLinks=AppViewSett.ClasseLinks.map((item)=>{
        item.Settings=nullData;
        return item;
      });
      AppViewSett.settings.apply_filter_before_or_after_single_mode=null;
    }
    this.props.DataFunction.onChange(AppViewSett);
  }
  renderParams=(data)=>{
    let chosen_data_params=this.state.data.filter((item)=>{
      return item.param && item.id==data.id?false:true;
    }).map((item)=>{
      return item.param;
    });
    let used_classes= this.props.AppParamData.filter((item)=>{
      return chosen_data_params.indexOf(item.uid)!==-1?true:false;
    }).map((item)=>{
      return item.class.uid;
    });
    let Options=this.props.AppParamData.filter((item)=>{
      return used_classes.indexOf(item.class.uid)==-1?true:false;
    }).map((item)=>{
      return {value:item.uid , label:item.class.name+'.'+item.name};
    });
    Options=[{value:null, label:'-Chose param-'}, ...Options];
    let chosen_value=Options.find((item)=>{
      return data.param==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose param-'};
    }
    return(<div className="param_min_max"><Select
        menuPosition={'fixed'}
        options={Options}
        value={chosen_value}
        onChange={(opt_data)=>{this.changeData({id:data.id, name:'param', value:opt_data.value});}}
      /></div>);
  }
  renderCondition=(data)=>{
    const operation=['max', 'min'];
    let options=operation.map((item, i)=>{
      return <option key={'data_min_max_'+data.param+'_'+i} value={item}>{item}</option>
    })
    return(<select onChange={(e)=>{this.changeData({id:data.id, name:'condition', value:e.currentTarget.value})}} name="condition" value={data.condition} className="form-control select_min_max_condition">
      {options}
    </select>);
  }
  renderParamData=(data)=>{

    return(<div key={data.id} style={{marginTop:15}} className="row">
            <div className="col-lg-6">{this.renderParams(data)}</div>
            <div style={{display:'flex'}} className="col-lg-6">{this.renderCondition(data)} <button onClick={(e)=>{this.removeFilter(data.id)}} className="btn btn-danger btn-sm btn_remove_condition"><i className="fa fa-times" aria-hidden="true"></i></button></div>
          </div>)
  }
  renderChoseOrder=(order)=>{
    const operation=[{value:0, text:'Apply filter before single line mode'}, {value:1, text:'Apply filter after single line mode'}];
    let options=operation.map((item, i)=>{
      return <option key={'data_filter_order_'+i} value={item.value}>{item.text}</option>
    })
    return (<div style={{marginTop:15}} className="row">
      <div className="col-md-6">
      <select onChange={(e)=>{this.changeOrder(e)}} name="condition" value={order} className="form-control select_filter_order">
        {options}
      </select>
      </div>
    </div>);
  }

  render(){
    let paramData=this.state.switch && this.state.data.length? this.state.data.map((item)=>{return this.renderParamData(item)}):null;
    let AppViewSett= this.props.AppViewSett;
    return(<div className="single_mode_data">
          <div className="settings__setcont setcont">
            <div className="row">
              <label className="col-lg-6">Single line mode</label>
              <div className="col-lg-6 switch_cont">
                <span className="switch_label">{(this.state.switch?'On':'Off')}</span>
                <IOSSwitch checked={this.state.switch}
                           onChange={(e)=>{
                               let data=[{id:Math.floor(Math.random() * 10000000000), param:null, condition:'max'}];
                               this.setState({data:data, switch:!this.state.switch, order:(!this.state.switch?1:null)}, ()=>{
                                 this.saveStore();
                               })
                           }} />
              </div>
            </div>
            {paramData}
            {this.state.switch && this.state.data.length<AppViewSett.ClasseLinks.length && <div className="filter_block_btn"><button onClick={this.addFilter} className="btn btn-sm" style={{color: 'rgb(118, 102, 102)'}}><i className="fa fa-plus" aria-hidden="true"></i> Add</button></div>}
            {this.state.switch && this.renderChoseOrder(this.state.order)}
          </div>

      </div>)
  }
}

const IOSSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 42,
            height: 26,
            padding: 0,
        },
        switchBase: {
            padding: 1,
            '&$checked': {
                transform: 'translateX(16px)',
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#17a2b8',
                    opacity: 1,
                    border: 'none',
                },
            },
            '&$focusVisible $thumb': {
                color: '#17a2b8',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 24,
            height: 24,
        },
        track: {
            borderRadius: 26 / 2,
            border: `1px solid ${theme.palette.grey[400]}`,
            backgroundColor: theme.palette.grey[50],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }:any) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});
