import * as React from "react";
import * as ReactDOM from "react-dom";


//пункт меню по умолчанию
const def_item = {
    "separator_before": false,
    "separator_after": false,
    "label": "#Пусто#",
    "action": ()=>{}, //callback(data)
    "icon_className": '',
    "children": [],
    disabled: false,
};

export interface IHZContextMenuInstanceProps {
    className?: string;
    content: object[];
    _ref?: (ref:HTMLInputElement) => void;
}
interface IHZContextMenuState {
    hovers: any,
}
export interface HZContextMenuInstance_content_item {
    separator_before?: boolean;
    separator_after?: boolean;
    label: string;
    action?: ()=>void,
    icon_className?: string,
    children?: object[]
}

export default class HZContextMenuInstance extends React.Component<IHZContextMenuInstanceProps, IHZContextMenuState> {
    constructor(props) {
        super(props);
    }
    state: Readonly<IHZContextMenuState> = {
        hovers: []
    };

    private stepRef: React.RefObject<HTMLInputElement>;

    _ref: HTMLDivElement;

    componentDidMount = () => {

    };

    onMouseEnterHandler = (e, data) => {
        if(e.currentTarget) {
            let hovers = this.state.hovers;
            hovers.splice(0,0);

            let target = e.currentTarget;

            let sizes_target = target.getBoundingClientRect();

            let HeightWindow = window.innerHeight;
            let WidthWindow = window.innerWidth;

            //260 - максимальная ширина меню
            let is_left_ul = false;
            if(sizes_target.x + sizes_target.width + 260 > WidthWindow) {
                is_left_ul = true;
            }
            else {
                is_left_ul = false;
            }

            if(data.level == 0 || hovers[data.level-1] != undefined) {
                hovers[data.level] = {index: data.index, is_left_ul: is_left_ul };
                hovers.splice(data.level+1);
            }

            this.setState({
                hovers: hovers
            });
        }
    };
    onMouseLeaveHandler = (e, data) => {
        let hovers = this.state.hovers;
        hovers.splice();

        if(e.relatedTarget!== undefined && e.relatedTarget !== window) {
            if(this._ref.contains(e.relatedTarget)) {
                hovers.splice(data.level);
                this.setState({
                    hovers: hovers
                });
            }
        }
    };

    render() {
        const GenerateMenuRec = (content, level) => {
            return content.map((item, index) => {

                item = Object.assign({}, def_item, item);

                let tree = [];

                let isHoverClass = this.state.hovers[level] != undefined && this.state.hovers[level].index === index && !item.disabled ? 'hover_TypeTreeCM_li' : '';
                let isLeftUlClass = this.state.hovers[level] != undefined && this.state.hovers[level].index === index && this.state.hovers[level].is_left_ul ? "TypeTreeCM_a_left_ul" : "";

                if(item.separator_before) {
                    tree.push(
                        <li key={(level+'_'+index + '_sb')} className="TypeTreeCM_separator">
                            <a className="TypeTreeCM_a" href="#">&nbsp;</a>
                        </li>);
                }

                if(item.children.length > 0) {
                    tree.push(
                        <li className={`TypeTreeCM_li TypeTreeCM_co_sub ${isHoverClass} ${isLeftUlClass}` }
                            onClick={()=>{console.log('li_click')}}
                            onMouseEnter={(e)=>{this.onMouseEnterHandler(e, {level: level, index: index})}}
                            onMouseLeave={(e)=>{this.onMouseLeaveHandler(e, {level: level, index: index})}}
                            key={(level+'_'+index)} >
                            <a className={"TypeTreeCM_a "+(item.disabled?"disabled_a":"")} onClick={item.action} >
                                <i className={`${item.icon_className} TypeTreeCM_a_i`} />
                                <span className="TypeTreeCM_a_separator">&nbsp;</span>
                                {item.label}
                                <span className="TypeTreeCM_children_span" >»</span>
                            </a>
                            <ul className="TypeTreeCM_ul">{GenerateMenuRec(item.children, (level+1))}</ul>
                        </li>);
                }
                else {
                    tree.push(
                        <li className={`TypeTreeCM_li ${isHoverClass}`}
                            onClick={()=>{console.log('li_click')}} key={(level+'_'+index)}
                            onMouseEnter={()=>{this.onMouseEnterHandler(this, {level: level, index: index})}}
                            onMouseLeave={()=>{this.onMouseLeaveHandler(this, {level: level, index: index})}}
                        >
                            <a className={"TypeTreeCM_a "+(item.disabled?"disabled_a":"") }  onClick={item.action} >
                                <i className={`${item.icon_className} TypeTreeCM_a_i`} />
                                <span className="TypeTreeCM_a_separator">&nbsp;</span>
                                {item.label}
                            </a>
                        </li>);
                }

                if(item.separator_after) {
                    tree.push(
                        <li key={(level+'_'+index + '_sa')} className="TypeTreeCM_separator">
                            <a className="TypeTreeCM_a" href="#">&nbsp;</a>
                        </li>);
                }

                return tree;
            });
        };

        let _className = this.props.className ? this.props.className : "";

        return (
            <div className={"TypeTreeCM_wrap " + _className} ref={(ref)=> {
                this.props._ref(ref as HTMLInputElement);
                this._ref = ref;
            }}>
                <ul className="TypeTreeCM_ul">
                    {GenerateMenuRec(this.props.content, 0)}
                </ul>
            </div>
        );
    }
}