import * as React from "react";
import * as ReactDOM from "react-dom";
import { inject, observer } from "mobx-react";
import Select from 'react-select';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import * as ct_api from '../api/AppTree-api'
import * as ct_api_class from '../api/ClassTree-api'

import './TabbleApp.scss'


export class TabeleApp extends React.Component{
  state={
    Parrents:[],
  }
  render(){
    return(
      <div className="TableApp">
          <TableHeader Data={this.props.HeaderData} />
          <TableBody Data={this.props.AttrData} />
      </div>
    )
  }
}

const TableHeader=(props)=>{
  const Header= props.Data.map((item, index)=>{
    let opts = {};
    if (item.style) {
        opts['style'] = item.style;
    }
    return <div {...opts} key={'header_'+index} className="header__item">{item.name}</div>
  })
  return(
    <div className="TableApp__header">
      {Header}
    </div>
  )
}

const TableBody=(props)=>{
  let ColumnData= props.Data.map((item, index)=>{
    const isFocus=!item.uid?true:false;
    return <TableColumn isFocus={isFocus} key={'column_'+item.id} Data={item}/>
  })
  ColumnData = [<EmpytTableColumn key={'column_0'}/> , ...ColumnData]
  return(
    <div className="TableApp__body">
        {ColumnData}
    </div>
  )
}

@inject("ApplicationStore")
@observer
class TableColumn extends React.Component{

  selectDataTypeHandler=(e)=>{
    let {value} = e.currentTarget;
    this.props.ApplicationStore.UpdateParamData({...this.props.Data, datatype:value, uid:null});
  }
  changeNameHandler=(name)=>{
      if(name){
        this.props.ApplicationStore.UpdateParamData({...this.props.Data, name:name, uid:null});
      }else{
        this.props.ApplicationStore.RemoveParamData(this.props.Data.id);
      }

  }

  render(){
    const spanIsNew= this.props.Data.uid?<span className="isNewParam"><i className="fas fa-check-circle"/></span>:<span className="isNewParam"><i className="fas fa-plus-circle"/></span>;
    return (<div className="body__column">
          {spanIsNew}
          <div style={{padding:'0px'}} className="body__column__item"><ParamNameCtrl updateName={this.changeNameHandler} isFocus={this.props.isFocus} ParamData={this.props.Data}/></div>
          <div style={{padding:'0px'}} className="body__column__item"><DataTypeSelect isDisabled={this.props.Data.uid}  changeHandler={this.selectDataTypeHandler}  key={'selectDataType_'+this.props.Data.id} Chosen={this.props.Data.datatype}/></div>
          <div style={{padding:'0px'}} className="body__column__item"><ClassSelectCtrl Data={this.props.Data} isDisabled={this.props.Data.uid} /></div>
          <div style={{padding:'0px'}} className="body__column__item"><ChoseViewCtrl Data={this.props.Data} /></div>
          <div style={{padding:'0px'}} className="body__column__item"><FilterCtrl Data={this.props.Data} /></div>
          <div style={{padding:'0px'}} className="body__column__item"><ColorCalcCtrl Data={this.props.Data} /></div>
          <div style={{padding:'0px'}} className="body__column__item"><DisplayCtrl Data={this.props.Data} /></div>
      </div>)
  }
}

@inject("ApplicationStore")
@observer
class EmpytTableColumn extends React.Component{
  constructor(){
    super();
    this.state={
      name:'',
      datatype:'text',
      class:null,
      filter:null,
    }
  }
  changeHandler=(e)=>{
    let key=e.currentTarget.getAttribute('paramkey');
    let {value}= e.currentTarget;
    let newParam={
      uid: null,
      name: this.state.name,
      datatype: this.state.datatype,
      class:null,
    };
    newParam[key]=value;
    this.props.ApplicationStore.AddAppParamData(newParam, true);
    this.setState({  name:'',
      datatype:'text',
      class:null,
    });

  }
  render(){
    return (<div className="body__column">
          <div style={{padding:'0px'}} className="body__column__item"><input paramkey="name" onChange={this.changeHandler} value={this.state.name}  style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control"/></div>
          <div style={{padding:'0px'}} className="body__column__item"><DataTypeSelect changeHandler={this.changeHandler} Chosen={this.state.datatype}/></div>
          <div style={{padding:'0px'}} className="body__column__item"><input disabled={true} style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control"/></div>
          <div style={{padding:'0px'}} className="body__column__item"><input disabled={true} style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control"/></div>
          <div style={{padding:'0px'}} className="body__column__item"><input disabled={true} style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control"/></div>
          <div style={{padding:'0px'}} className="body__column__item"><input disabled={true} style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control"/></div>
          <div style={{padding:'0px'}} className="body__column__item"><input disabled={true} style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control"/></div>
      </div>)
  }
}

class DataTypeSelect extends React.Component{
  state={
    value:this.props.Chosen,
  }
  static getDerivedStateFromProps(props, state) {
    if (props.Chosen !== state.value) {
      return {
        value: props.Chosen
      };
    }
    return null;
  }
   render(){
     const DataType=[
       {name:'text', value:'text'},
       {name:'number', value:'number'},
       {name:'datetime', value:'datetime'},
       {name:'object', value:'object'},
       {name:'file', value:'file'},
       {name:'counter', value:'counter'},
     ];
     const Options= DataType.map((item, index)=>{
        return  <option key={'datatypeapp_'+index} value={item.value}>{item.name}</option>
     });
    return (<select disabled={(this.props.isDisabled?true:false)} paramkey="dataType"  onChange={this.props.changeHandler}  value={this.state.value} style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control">
              {Options}
      </select>);
  }
}

@inject("ApplicationStore")
@observer
class ParamNameCtrl extends React.Component {
  constructor(props){
    super()
    this.state={
      FindParamData:[],
      ParamData:{
        ...props.ParamData
      },
      psoitonMenu:{
        x:0,
        y:0,
      }
    }
  }
  count_param=15;
  changeHandler=(e)=>{
    let {value}= e.currentTarget;
    let FindData=this.props.ApplicationStore.ParamFullData.filter((item)=>{
      return value && item.name.toLowerCase().indexOf(value.toLowerCase())!==-1?true:false;
    }).filter((item, index)=>{
      return index<=(this.count_param-1)?true:false;
    })
    let psoitonMenu=this.getPositionMenu(e.currentTarget);
    this.setState({FindParamData:FindData, psoitonMenu:psoitonMenu, ParamData:{...this.state.ParamData, name:value, uid:null, class:null}});
  }
  focusHandler=(e)=>{
    let {value}= e.currentTarget;
    let FindData=this.props.ApplicationStore.ParamFullData.filter((item)=>{
      return value && item.name.toLowerCase().indexOf(value.toLowerCase())!==-1?true:false;
    }).filter((item, index)=>{
      return index<=(this.count_param-1)?true:false;
    })
    let psoitonMenu=this.getPositionMenu(e.currentTarget);
    this.setState({FindParamData:FindData, psoitonMenu:psoitonMenu, ParamData:{...this.state.ParamData}});
  }
  blurHandler=(e, target)=>{
      if(e.relatedTarget && e.relatedTarget.className==='context_param'){
        //click on context menu
        return false;
      }
      this.setState({FindParamData:[], psoitonMenu:{x:0,y:0}, ParamData:{...this.state.ParamData}}, ()=>{
        this.props.updateName(this.state.ParamData.name);
      })

  }
  renderMenu=(Data)=>{
    const liItem= this.state.FindParamData.map((item, index)=>{
      return <li key={'finddata_'+index} onClick={(e)=>{this.choseMenuHandler(item)}}><span title={item.name} className="param_name">{item.name}</span><span className="class_name" title={item.class.name}>{item.class.name}</span></li>
    })
    return <ul style={{top:this.state.psoitonMenu.y, left:this.state.psoitonMenu.x}} tabIndex="0" className="context_param">{liItem}</ul>
  }

  choseMenuHandler=(Data)=>{
    this.setState({FindParamData:[], ParamData:{...Data, id:this.state.ParamData.id}}, ()=>{
      this.props.ApplicationStore.UpdateParamData({...Data, id:this.state.ParamData.id});
    })
  }
  getPositionMenu(input){
    let pos=input.getBoundingClientRect();
    return {x:pos.x, y:(pos.y+pos.height)};
  }

  render(){
    let opts = {};
    console.log(this.state.FindParamData);
    if (this.props.isFocus) {
        opts['autoFocus'] = 'autoFocus';
    }
    const contexParam=this.state.FindParamData.length?this.renderMenu(this.state.FindParamData):null;
    return(<div  className="nameparam_select">
        <input {...opts} onBlur={this.blurHandler} onFocus={this.focusHandler} onChange={this.changeHandler} value={this.state.ParamData.name} style={{width: '100%', borderRadius: '0px', border:'none'}} className="form-control"/>
        {contexParam}
      </div>
    )
  }
}

@inject("ApplicationStore")
@observer
class ClassSelectCtrl extends React.Component{
  focusHandler=(e)=>{
    if(this.props.isDisabled){
      return false;
    }
    this.props.ApplicationStore.setShowClassTree(true, this.props.Data);
    console.log('focus');
  }
  blurHandler=(e)=>{
    if(this.props.isDisabled){
      return false;
    }
    var el=e.relatedTarget, parentsClasses=[];
    while(el){
      parentsClasses.push(el.className);
      el=el.parentNode;
    }
    if(parentsClasses.indexOf('ClassChoseTree')!==-1){
      return false;
    }
    this.props.ApplicationStore.setShowClassTree(false, null);
  }
  render(){
    const className=this.props.Data.class && this.props.Data.class.name? this.props.Data.class.name:'';
    const isSelected= this.props.ApplicationStore.SelectedParamForClass && this.props.ApplicationStore.SelectedParamForClass.id===this.props.Data.id?true:false;
    return(
      <div  tabIndex="0" readOnly={this.props.isDisabled} onFocus={this.focusHandler} onBlur={this.blurHandler} className={'form-control classSelectCtrl'+(isSelected?' selected':'')}>{className}</div>
    );
  }
}

@inject("ApplicationStore")
@observer
class FilterCtrl extends React.Component{
  filterChange=(e)=>{
    let {id, value}=e.currentTarget;
    let ParamData={...this.props.Data};
    if(!ParamData.Filter || !ParamData.Filter.length){
      ParamData.Filter=[{op:'=',v:''}]
    }
    ParamData.Filter[0][id]=value;
    this.props.ApplicationStore.UpdateParamData(ParamData);
  }
  render(){
    const operation=['=', '!=', '>=', '<=', '~'];
    let operOpt=operation.map((item, index)=>{
      return <option key={'filter_'+index+'_'+this.props.Data.id} value={item}>{item}</option>;
    });
    let FilterData=this.props.Data.Filter && this.props.Data.Filter.length?this.props.Data.Filter[0]:{op:'=',v:''};//Only firs Data
    return(
      <div className="filter">
        <select id="op" onChange={this.filterChange} value={FilterData.op} className="form-control">{operOpt}</select>
        <input  id="v" onChange={this.filterChange} value={FilterData.v} className="form-control"/>
      </div>
    );
  }
}

@inject("ApplicationStore")
@observer
class ChoseViewCtrl extends React.Component{
  viewChange=(e)=>{
    let {value}=e.currentTarget;
    let ParamData={...this.props.Data};
    if(value!=-1){
      ParamData.view=value;
    }else{
      ParamData.view=null;
    }
    this.props.ApplicationStore.UpdateParamData(ParamData);
  }
  render(){
    let uid= this.props.ApplicationStore.SelectNode.iddb;
    let parrentView=this.props.ApplicationStore.parrentsView.filter((item)=>{
      return item.type!=='view' || uid==item.uid?false:true;
    }).map((item)=>{
      return {...item};
    })
    let operOpt=parrentView.map((item, index)=>{
      return <option key={'view_'+index+'_'+this.props.Data.id} value={item.uid}>{item.name}</option>;
    });
    operOpt=[<option key={'view_-1_'+this.props.Data.id} value="-1">-Chose View-</option>, ...operOpt];
    return(
      <div className="view">
        <select onChange={this.viewChange} value={this.props.Data.view} className="form-control">{operOpt}</select>
      </div>
    );
  }
}


@inject("ApplicationStore")
@observer
class DisplayCtrl extends React.Component{
  isHideChange=(e)=>{
    let isCheck = e.currentTarget.checked;
    this.props.ApplicationStore.UpdateParamData({...this.props.Data, hide:Number(isCheck)});
  }
  render(){
    return(
      <div className="isHide">
        <input checked={this.props.Data.hide}  onChange={this.isHideChange} type="checkbox" className="form-control"/>
      </div>
    );
  }
}

@inject("ApplicationStore")
@observer
class ColorCalcCtrl extends React.Component{
  state={
    Options:[],
    cache:1,
  }

  componentDidMount(){
    this.loadCalc();
  }
  componentDidUpdate(){
    this.loadCalc();
  }

  loadCalc=()=>{
      if(this.props.Data.class && this.state.cache!=this.props.Data.class.uid){
        ct_api_class.getCalcs(this.props.Data.class.uid).then((res)=>{
          let Options=res.data.data.map((item)=>{
            return {value:item.uid , label:item.name}
          });
          Options=[{value:null, label:'-Chose Calc-'}, ...Options];
          this.setState({Options:Options, cache:this.props.Data.class.uid})
        });
      }
    }
  changeCalc=(opt_data)=>{
    let ParamData={...this.props.Data};
    ParamData.calc_color=opt_data.value;
    this.props.ApplicationStore.UpdateParamData(ParamData);
  }

  render(){
    let chosen_value=this.state.Options.find((item)=>{
      return this.props.Data.calc_color==item.value?true:false;
    })
    if(!chosen_value){
      chosen_value={value:null, label:'-Chose Calc-'};
    }
    return(
      <div className="isHide">
      <Select
          loadOptions={this.loadCalc}
          menuPosition={'fixed'}
          options={this.state.Options}
          value={chosen_value}
          onChange={this.changeCalc}
        />
      </div>
    );
  }
}
