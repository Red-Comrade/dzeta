/**
 * Данный модуль предназначен для вкладок дерева
 */

import * as React from "react";
import ITabsData from './CoolTree'
interface TabsData {
    ID: string;
    _ref?: HTMLElement;
    Width?: number;
}
export interface ICoolTreeTabsProps {
    onActiveTab: (TabID) => void;
    onDropdownChange: (isOpen:boolean) => void;
    onCloseTab: (TabID) => void;

    RecalcCoTabTrig: () => void; //изменяем пропсы
    RecalcCoTab: (params:object) => void;

    isRecalcRender: boolean;
    TabsSett: {
        WidthCo: number,
        DropDownOpen: boolean
    };

    Tabs: TabsData[];

}
interface ICoolTreeTabsState {
    visible: boolean;
}

export class CoolTreeTabs extends React.Component<ICoolTreeTabsProps, ICoolTreeTabsState> {
    close_but = <div className="rt_tab_close_btn" ><svg className="rt_tab_close_svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
        <path className="rt_tab_close_path" fill="currentColor" d="M15.8,14l9.9-9.9c0.5-0.5,0.5-1.3,0-1.8c-0.5-0.5-1.3-0.5-1.8,0L14,12.3L4.1,2.4c-0.5-0.5-1.3-0.5-1.8,0c-0.5,0.5-0.5,1.3,0,1.8l9.9,9.9l-9.9,9.9c-0.5,0.5-0.5,1.3,0,1.8C2.6,25.9,2.9,26,3.3,26s0.7-0.1,0.9-0.3l9.8-9.9l9.9,9.9c0.3,0.3,0.5,0.3,0.9,0.3s0.6-0.1,0.9-0.3c0.5-0.5,0.5-1.3,0-1.8L15.8,14z"></path>
    </svg></div>;

    dropdown_ref:HTMLLIElement;
    dropdown_a_ref:HTMLAnchorElement;
    _ref:HTMLUListElement;

    state: Readonly<ICoolTreeTabsState> = {
        visible: true
    };

    constructor(props) {
        super(props);
    };
    onBtnClick = (data, e) => {
        if(e.target.classList.contains('rt_tab')) {
            let TabID = data.ID; // e._targetInst.memoizedProps.data.ID;
            this.props.onActiveTab(TabID);
            this.props.onDropdownChange(false);
        }
        else if(e.target.classList.contains('rt_tab_close_svg')
            || e.target.classList.contains('rt_tab_close_btn')
            || e.target.classList.contains('rt_tab_close_path')) {
            let TabID = data.ID;
            this.props.onCloseTab(TabID);
            this.props.RecalcCoTabTrig();

            console.log('svg')
        }
    };

    handleClick = (e) => {
        if(this.dropdown_ref!= undefined) {
            if(this.dropdown_ref.contains(e.target)){
                if(this.dropdown_a_ref.contains(e.target)) {
                    this.props.onDropdownChange(this.dropdown_ref.classList.contains('open')== false);
                }
            }
            else {
                this.props.onDropdownChange(false);
            }
        }
    };

    componentWillMount = () => {
        document.addEventListener('mousedown', this.handleClick, false);
    };
    componentWillUnmount = () => {
        document.removeEventListener('mousedown', this.handleClick, false);
    };


    componentDidMount() {
        let TabsArrWidth = {};
        this.props.Tabs.forEach((item, index, arr) => {
            TabsArrWidth[item.ID] = {Width: item._ref.offsetWidth};
        });
        this.props.RecalcCoTab({TabsArrWidth: TabsArrWidth, WidthCo: this._ref.offsetWidth});
    }

    shouldComponentUpdate(nextProps, nextState) {

        return true;
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.isRecalcRender) {
            //Если истина, то нам больше не нужно пересчитывать ширину вкладок и перерендерить компоненту
        }
        else {
            //Если ложь, то нам необходимо пересчитать ширину всех вкладок и главного контейнера и дать ширину каждой,
            //Чтобы построить дропдаун панельку
            //Тут иницилизируем событие, что нужно перерендерить компоненту, предварительно отправив наши данные при построении
            //вкладок
            //Отправляем массив ширин вкладок и ширину главного контейнера

            let TabsArrWidth = {};

            this.props.Tabs.forEach((item, index, arr) => {
                TabsArrWidth[item.ID] = {Width: item._ref.offsetWidth};
            });
            this.props.RecalcCoTab({TabsArrWidth: TabsArrWidth, WidthCo: this._ref.offsetWidth});

        }

        return true;
    }

    render() {
        const props = this.props;
        let TABS_jsx = [];
        let TABS_DROPDOWN_jsx = [];

        let Class = '';
        let Width_all = 0;

        let WidthDropdownDiv = 38;
        let isDropdownActive = false;

        let main_array = [];
        let dropdown_array = [];

        //Разбиваем массив на 2 массива
        props.Tabs.forEach((item, index, arr) => {
            if(props.isRecalcRender) {
                //Строим дропдаун
                if( dropdown_array.length == 0 && Width_all + item.Width < props.TabsSett.WidthCo ) {
                    Width_all += item.Width;
                    main_array.push(item);
                }
                else {
                    if(Width_all + WidthDropdownDiv > props.TabsSett.WidthCo && index > 0) {
                        //Если не хватает места для dropdown, то мы перемещаем последнюю вкладку в dropdown
                        Width_all -= arr[index-1].Width;

                        let tmp_item = main_array.splice(-1,1);
                        if(tmp_item.length > 0) {
                            dropdown_array.push(tmp_item[0]);
                        }
                        dropdown_array.push(item);
                    }
                    else {
                        dropdown_array.push(item);
                    }
                }

            }
            else {
                main_array.push(item)
            }

        });

        //Собираем главный массив для отрисовки
        main_array.forEach((item, index, arr) => {
            Class = 'rt_tab';
            Class += ' rt_tab_1';
            Class +=  item["Close"] ? ' rt_tab_has_close_btn' : "";

            let li_class = item["isActive"] ? ' rt_tab_active' : '';
            li_class+=' rt_tab_li';
            let item_tab = (
                <li className={li_class} ref={el=> (item._ref = el)} onClick={this.onBtnClick.bind(this, item)} key={index}>
                    <a type="button" className={Class} >
                        {item["Name"]} {item["Close"] ? this.close_but : ""}
                    </a>
                </li>
            );
            TABS_jsx.push(item_tab);
        });
        //Собираем массив для дропдауна
        dropdown_array.forEach((item, index, arr) => {
            Class = 'rt_tab';
            Class += ' rt_tab_1';
            let li_class = '';
            if(item["isActive"]) {
                li_class += ' rt_tab_active';
                isDropdownActive = true;
            }
            li_class+=' rt_tab_li';

            let item_tab = (
                <li  className={li_class} ref={el=>{item._ref = el; }} onClick={this.onBtnClick.bind(this, item)} key={index}>
                    <a type="button" className={Class} >
                        {item["Name"]} {item["Close"] ? this.close_but : ""}
                    </a>
                </li>
            );
            TABS_DROPDOWN_jsx.push(item_tab);
        });



        if(TABS_DROPDOWN_jsx.length > 0) {
            let a_class = 'rt_tab rt_tab_1 rt_tab_dropdown_a';
            let isOpen = props.TabsSett.DropDownOpen ? 'open' : '';
            let li_class = isDropdownActive ? ' rt_tab_active' : '';
            li_class += " rt_tab_li rt_tab_dropdown "+isOpen;

            TABS_jsx.push(
                <li key={props.Tabs.length} ref={el=> {this.dropdown_ref = el}} className={li_class}>
                    <a className={a_class} ref={el=>(this.dropdown_a_ref = el)}><i className="fa fa-ellipsis-v"> </i></a>
                    <ul className="rt_tab_dropdown_ul">{TABS_DROPDOWN_jsx}</ul>
                </li>
            )
        }


        return (
            <div  className="rt_tab_wrapper rt_tab_wrapper_1">
                <ul ref={el => (this._ref = el)} className="rt_tab_ul">{TABS_jsx}</ul>
            </div>
        )
    }
}