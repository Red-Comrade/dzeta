import * as React from "react";

//TableControl
import {ChoseViewCtrl} from '../containers/ApplicationPage/ParamComponents/ChoseViewCtrl'
import {FilterCtrl} from '../containers/ApplicationPage/ParamComponents/FilterCtrl'
import {ColorCalcCtrl} from '../containers/ApplicationPage/ParamComponents/ColorCalcCtrl'
import {ColumnWidthCtrl} from '../containers/ApplicationPage/ParamComponents/ColumnWidthCtrl'
import {DisplayCtrl} from '../containers/ApplicationPage/ParamComponents/DisplayCtrl'
import {FixedColumnCtrl} from '../containers/ApplicationPage/ParamComponents/FixedColumnCtrl'
import {AliasCtrl} from '../containers/ApplicationPage/ParamComponents/AliasCtrl'
import {DataSourceCtrl} from '../containers/ApplicationPage/ParamComponents/DataSourceCtrl'



export interface IViewParamSettingsProps {
    DataFunction: object,
    paramUid: string,
    Settings:object,
    view_settings:{
      settings:{
        table_or_form:number,
      }
    },
}
export interface IViewParamSettingsState {
    searchInput: string,
    tabs: string,
}


export class ViewParamSetting extends React.Component<IViewParamSettingsProps, IViewParamSettingsState>{
    constructor(props){
      super(props);
      this.state={
        tabs:'all',
        searchInput:'',
      };
    }

    searchChange=(e)=>{
      let {value}=e.currentTarget;
      this.setState({searchInput:value});
    }
    clickTabs=(tabs)=>{
      this.setState({tabs:tabs});
    }
    settChange=(e)=>{

    }

    getStrucSett=()=>{
        let view_settings=this.props.view_settings;
        let Settings=this.props.Settings;
        let strucSett=[
          {tabs:'all', title:'All Settings', Settinngs:[
            {key:'alias', title:'Alias', value:Settings},
            {key:'view', title:'View', value:Settings},
            {key:'data_source', title:'Data Source', value:Settings},
            {key:'color', title:'Param color', value:Settings},
            {key:'width', title:'Param Width', value:Settings},
            {key:'fixed', title:'Is Fixed', value:Settings},
            {key:'hide', title:'Is Hide', value:Settings},
          ]},
          {tabs:'common', title:'Common Settings', Settinngs:[
            {key:'alias', title:'Alias', value:Settings},
            {key:'view', title:'View', value:Settings},
            {key:'data_source', title:'Data Source', value:Settings},
            {key:'hide', title:'Is Hide', value:Settings},
          ]},
          {tabs:'visual', title:'Visual settings', Settinngs:[
            {key:'color', title:'Param color', value:Settings},
            {key:'width', title:'Param Width', value:Settings},
            {key:'fixed', title:'Is Fixed', value:Settings},
          ]},
        ];
        if(view_settings && view_settings.settings && view_settings.settings.table_or_form==1){
          //Представление - форма
          strucSett=[
            {tabs:'all', title:'All Settings', Settinngs:[
              {key:'alias', title:'Alias', value:Settings},
              {key:'hide', title:'Is Hide', value:Settings},
            ]},
            {tabs:'common', title:'Common Settings', Settinngs:[
              {key:'alias', title:'Alias', value:Settings},
              {key:'hide', title:'Is Hide', value:Settings},
            ]},
            {tabs:'visual', title:'Visual settings', Settinngs:[

            ]},
          ];
        }else if(view_settings && view_settings.settings && view_settings.settings.table_or_form==2){
          //Представление - вкладка
          strucSett=[];
        }else if(view_settings && view_settings.settings && view_settings.settings.table_or_form==3){
          //Представление - календарь
          strucSett=[
            {tabs:'all', title:'All Settings', Settinngs:[
              {key:'alias', title:'Alias', value:Settings},
              {key:'hide', title:'Is Hide', value:Settings},
            ]},
            {tabs:'common', title:'Common Settings', Settinngs:[
              {key:'alias', title:'Alias', value:Settings},
              {key:'hide', title:'Is Hide', value:Settings},
            ]},
          ];
        }
        return strucSett;
    }

    renderSettings=(curSett)=>{
        let renderSett=curSett.Settinngs.filter((item:any)=>{
          return item.title.toLowerCase().includes(this.state.searchInput.toLowerCase());
        });
        return renderSett.map((item:any)=>{
          return(<div key={item.key+'_'+this.props.paramUid} className="settings__setcont setcont">
                      <div className="row">
                        <label className="col-lg-6">{item.title}</label>

                        <div className="col-lg-6">
                          {this.renderSettByKey(item.key, item.value)}
                        </div>
                      </div>
                  </div>);
        });
    }

    renderSettByKey=(key, value)=>{
        switch(key) {
          case 'view':
            return <ChoseViewCtrl Data={value} />
          case 'filter':
            return <FilterCtrl Data={value} />
          case 'color':
            return <ColorCalcCtrl Data={value} />
          case 'hide':
            return <DisplayCtrl Data={value} />
          case 'width':
            return <ColumnWidthCtrl Data={value}/>
          case 'fixed':
              return <FixedColumnCtrl Data={value}/>
          case 'alias':
              return <AliasCtrl Data={value}/>
          case 'data_source':
              return <DataSourceCtrl Data={value}/>
          default:
            return null;
        }
    }

    render(){
      let strucSett=this.getStrucSett();
      let curSett=strucSett.find((item)=>{
        return item.tabs===this.state.tabs?true:false;
      });
      let tabs=strucSett.map((item)=>{
        return <li onClick={()=>{this.clickTabs(item.tabs)}} className={'tabs__item item '+(item.tabs===this.state.tabs?'selected':'')} key={'sett_key_'+item.tabs}>{item.title}</li>;
      })
      return(<div className="viewparam__settings viewparamsettings">
        <div className="viewparamsettings__seacrh search">
            <label><i className="fa fa-search" aria-hidden="true"></i><input onChange={this.searchChange} type="text" placeholder="SEARCHSETTINGS" value={this.state.searchInput}/></label>
        </div>
        <div className="viewparamsettings__info info">
          <span className="info__sett">Settings: <span className="count">{curSett.Settinngs.length}</span></span>
        </div>
        <ul className="viewparamsettings__tabs tabs">
          {tabs}
        </ul>
        <div className="viewparamsettings__settings settings">
          {curSett && this.renderSettings(curSett)}
        </div>
      </div>)
    }
}
