import * as React from "react";
import {BsPrefixComponent} from "./helpers";


export interface ModalProps {
    size?: 'sm' | 'lg' | 'xl';
    centered?: boolean;
    backdrop?: 'static' | boolean;
    backdropClassName?: string;
    keyboard?: boolean;
    animation?: boolean;
    dialogClassName?: string;
    dialogAs?: React.ElementType;
    autoFocus?: boolean;
    enforceFocus?: boolean;
    restoreFocus?: boolean;
    restoreFocusOptions?: FocusOptions;
    show?: boolean;
    onShow?: () => void;
    onHide?: () => void;
    container?: any;
    scrollable?: boolean;
    onEscapeKeyDown?: () => void;
}


import COBody from "./COBody";

export default class COForm<As extends React.ElementType = 'div'> {
    static Body: typeof COBody;
}

// extends BsPrefixComponent<As, ModalProps>





