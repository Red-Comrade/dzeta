export default interface IButtonConfig {
    ClassName?: string,
    type?: "custom" | "close",
    onClick?: () => void,
    contentJSX?: JSX.Element[] | JSX.Element,
    content?: string
}