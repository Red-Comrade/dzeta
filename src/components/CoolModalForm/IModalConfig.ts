import IButtonConfig from "./IButtonConfig";

export default interface IModalConfig {
    id: string,
    isMobile?: boolean, //if true, then we always have maximized window

    onClickCloth?: (ModalConfig:IModalConfig) => void,
    onClose?: (ModalConfig:IModalConfig) => void,

    content?: {
        ClassName?: string,
        headerJSX?: JSX.Element[] | JSX.Element,
        header?:  {
            ClassName?: string,
            contentJSX?: JSX.Element[] | JSX.Element,
            content?: {
                title?: JSX.Element | string,
                buttonsJSX?: JSX.Element[] | JSX.Element,
                buttons?: IButtonConfig[],
            },
        },
        bodyJSX?: JSX.Element[] | JSX.Element,
        body?: {
            ClassName?: string,
            contentJSX?: JSX.Element[] | JSX.Element,
        },

        footerJSX?: JSX.Element[] | JSX.Element,
        footer?: {
            contentJSX?: JSX.Element[] | JSX.Element,
            content?: {
                fixed_buttons?: IButtonConfig[],
                fixed_buttonsJSX?: JSX.Element[] | JSX.Element | IButtonConfig
                buttons?: IButtonConfig[],
                buttonsJSX?: JSX.Element[] | JSX.Element | IButtonConfig
            },
        }
    }
    contentJSX?: JSX.Element[] | JSX.Element,
}

