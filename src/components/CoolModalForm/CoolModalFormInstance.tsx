import * as React from "react";
import * as ReactDOM from "react-dom";
//import HZContextMenuInstance from './HZContextMenuInstance';
import IModalConfig from './IModalConfig'
import IModalConfigDictionary from './IModalConfigDictionary'
import HZContextMenuInstance from "../HZContextMenuInstance";


interface CoolModalFormInstanceProps {
    ModalConfig: IModalConfig,
}
interface CoolModalFormInstanceState {
}
export default class CoolModalFormInstance extends React.Component<CoolModalFormInstanceProps, CoolModalFormInstanceState> {
    state: Readonly<CoolModalFormInstanceState> = {
    };

    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
    };
    componentWillMount = () =>  {
        //document.addEventListener('mousedown', this.handleClick, false);
    };
    componentWillUnmount = () => {
    };
    shouldComponentUpdate = (nextProps, nextState) => {
        return true;
    };

    componentDidUpdate  = (prevProps, prevState) => {

    };

    private events = {
        onClickCloth: (e) => {
            if(this.props.ModalConfig.onClickCloth) {
                this.props.ModalConfig.onClickCloth(this.props.ModalConfig);
            }
        }
    };

    private core = {

    };


    private build = {
        config: {
            HEADER_CLASS: "cof_hdr",
            BODY_CLASS: "cof_body",
            FOOTER_CLASS: "cof_ftr",

            HEADER_TITLE_BAR_CLASS: "cof_titlebar",
            HEADER_TITLE_CLASS: "cof_title",
            HEADER_CONTENT_BUTTONS_CLASS: "cof_hdr_btns_co",
            HEADER_BUTTON_CLASS: "cof_hdr_btn",

            FOOTER_CONTENT_FIXED_BUTTONS_CLASS: "cof_ftr_fixed_btns_co",
            FOOTER_CONTENT_BUTTONS_CLASS: "cof_ftr_btns_co",
            FOOTER_BUTTON_CLASS: "cof_ftr_btn",

        },


        header: (content) => {
            let HEADER;
            if(content.headerJSX) {
                HEADER = content.headerJSX;
            }
            else if(content.header) {
                let ClassName = content.header.className ? content.header.className : "";
                let HEADER_CONTENT;
                if(content.header.contentJSX) {
                    HEADER_CONTENT = content.header.contentJSX
                }
                else if(content.header.content) {
                    let HEADER_CONTENT_TITLE;
                    if(content.header.content.title) {
                        HEADER_CONTENT_TITLE = <div className={this.build.config.HEADER_TITLE_CLASS}>{content.header.content.title}</div>;
                    }
                    let HEADER_CONTENT_BUTTONS;
                    if(content.header.content.buttonsJSX) {
                        HEADER_CONTENT_BUTTONS = content.header.content.buttonsJSX;
                    }
                    else if(content.header.content.buttons) {
                        HEADER_CONTENT_BUTTONS = this.build.buttons(content.header.content.buttons, false);
                    }

                    HEADER_CONTENT = [
                        <div key={"title_hdr"} className={this.build.config.HEADER_TITLE_BAR_CLASS}>{HEADER_CONTENT_TITLE}</div>,
                        <div key={"btns_hdr"} className={this.build.config.HEADER_CONTENT_BUTTONS_CLASS}>{HEADER_CONTENT_BUTTONS}</div>,
                    ];
                }

                HEADER = <div className={this.build.config.HEADER_CLASS + ' ' + ClassName}>{HEADER_CONTENT}</div>;
            }
            return HEADER;
        },
        body: (content) => {
            let BODY;
            if(content.bodyJSX) {
                BODY = content.bodyJSX;
            }
            else if(content.body) {
                let ClassName = content.body.className ? content.body.className : "";
                let BODY_CONTENT;
                if(content.body.contentJSX) {
                    BODY_CONTENT = content.body.contentJSX
                }

                BODY = <div className={this.build.config.BODY_CLASS + ' ' + ClassName}>{BODY_CONTENT}</div>;
            }
            return BODY;
        },
        footer: (content) => {
            let FOOTER;
            if(content.footerJSX) {
                FOOTER = content.footerJSX;
            }
            else if(content.footer) {
                let ClassName = content.footer.className ? content.footer.className : "";
                let FOOTER_CONTENT;

                if(content.footer.contentJSX) {
                    FOOTER_CONTENT = content.header.contentJSX
                }
                else if(content.footer.content) {
                    let FOOTER_CONTENT_FIXED_BUTTONS;
                    if(content.footer.content.fixed_buttonsJSX) {
                        FOOTER_CONTENT_FIXED_BUTTONS = content.header.content.fixed_buttonsJSX;
                    }
                    else if(content.footer.content.fixed_buttons) {
                        FOOTER_CONTENT_FIXED_BUTTONS = this.build.buttons(content.header.content.fixed_buttons, true);
                    }


                    let FOOTER_CONTENT_BUTTONS;
                    if(content.footer.content.buttonsJSX) {
                        FOOTER_CONTENT_BUTTONS = content.header.content.buttonsJSX;
                    }
                    else if(content.footer.content.buttons) {
                        FOOTER_CONTENT_BUTTONS = this.build.buttons(content.footer.content.buttons, true);
                    }

                    FOOTER_CONTENT = [
                        <div key={"btns_fixed_ftr"} className={this.build.config.FOOTER_CONTENT_FIXED_BUTTONS_CLASS}>{FOOTER_CONTENT_FIXED_BUTTONS}</div>,
                        <div key={"btns_ftr"} className={this.build.config.FOOTER_CONTENT_BUTTONS_CLASS}>{FOOTER_CONTENT_BUTTONS}</div>,
                    ];
                }
                FOOTER = <div className={this.build.config.FOOTER_CLASS + ' ' + ClassName}>{FOOTER_CONTENT}</div>;
            }
            return FOOTER;
        },



        buttons: (buttons, isFooter) => {
            let buttonsJSX = [];
            for (let key_btn = 0; key_btn < buttons.length; key_btn ++) {
                let item_btn = buttons[key_btn];

                let BTN_CONTENT;

                if(item_btn.contentJSX) {
                    BTN_CONTENT = item_btn.contentJSX;
                }
                else if(item_btn.content) {
                    BTN_CONTENT = item_btn.content;
                }

                let ClassNameBTN = item_btn.ClassName ? item_btn.ClassName : "";

                const props_btn = {
                    onClick: null,
                };

                if(item_btn.type == "close") {
                    BTN_CONTENT = <i className={"fas fa-times"}/>;
                    props_btn.onClick = (e) => {
                        if(item_btn.onClick) {
                            item_btn.onClick(e, item_btn);
                        }
                        else {
                            this.props.ModalConfig.onClose(this.props.ModalConfig);
                        }
                    };
                }
                else {
                    props_btn.onClick = (e) => {
                        if(item_btn.onClick) {
                            item_btn.onClick(e, item_btn);
                        }
                    };
                }

                if(isFooter) {
                    ClassNameBTN += ' ' + this.build.config.FOOTER_BUTTON_CLASS
                }
                else {
                    ClassNameBTN += ' ' + this.build.config.HEADER_BUTTON_CLASS
                }

                buttonsJSX.push(
                    <button {...props_btn} key={'btn-'+isFooter+'-'+key_btn} className={ClassNameBTN + ' nf_btn-ellipse'}>
                        {BTN_CONTENT}
                    </button>
                );
            }

            return buttonsJSX;
        },
    };



    render() {
        const content = this.props.ModalConfig.content;

        let MODAl_JSX = <div className={"cof_panel cof_maximize"}/>;

        if(this.props.ModalConfig.contentJSX) {
            MODAl_JSX = (<div className={"cof_panel cof_maximize"}>
                {this.props.ModalConfig.contentJSX}
            </div>);
        }
        else if(this.props.ModalConfig.content) {
            MODAl_JSX = (<div className={"cof_panel cof_maximize"} onClick={this.events.onClickCloth} >
                <div className={"cof_panel_shadow"}>
                {this.build.header(content)}
                {this.build.body(content)}
                {this.build.footer(content)}
                </div>
            </div>);
        }

        return (MODAl_JSX);
    }
}