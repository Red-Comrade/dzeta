import IModalConfig from "./IModalConfig";

export default interface IModalConfigDictionary {
    [key: string]: IModalConfig
}