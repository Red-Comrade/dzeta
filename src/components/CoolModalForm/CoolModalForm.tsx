import * as React from "react";
import * as ReactDOM from "react-dom";
//import HZContextMenuInstance from './HZContextMenuInstance';
import IModalConfig from './IModalConfig'
import IModalConfigDictionary from './IModalConfigDictionary'
import HZContextMenuInstance from "../HZContextMenuInstance";
import CoolModalFormInstance from "./CoolModalFormInstance";
import ICoolModalFormProps from "./ICoolModalFormProps";

interface IModalConfigIn {
    [id: string]: {
        ModalConfig: IModalConfig,
        Container: HTMLDivElement,

        ref: HTMLElement,
        content: JSX.Element[] | JSX.Element,
    };
}

interface ICoolModalFormState {
}

export default class CoolModalForm extends React.Component<ICoolModalFormProps, ICoolModalFormState> {
    state: Readonly<ICoolModalFormState> = {
        hovers: []
    };

    private ModalsObjs: IModalConfigIn = {}; // {ModalData: IModalConfig}[] = {}; //Key - is object
    private CoolModalForm_ref: HTMLElement;

    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
    };
    componentWillMount = () =>  {
        //document.addEventListener('mousedown', this.handleClick, false);
    };
    componentWillUnmount = () => {
        Object.keys(this.ModalsObjs).forEach((item_key_mo, index_mo) => {
           let item_mo =  this.ModalsObjs[item_key_mo];

           if(item_mo.Container) {
               this.core.RemoveContainer(item_key_mo);
           }
        });
        this.ModalsObjs = {};
    };
    shouldComponentUpdate = (nextProps, nextState) => {
        return true;
    };

    componentDidUpdate  = (prevProps, prevState) => {
        //Сначала добавляем модалки
        Object.keys(this.props.Modals).forEach((key_mo, index_mo) => {
            let item_mo = this.props.Modals[key_mo];

            if(this.ModalsObjs[key_mo] == undefined) {
                this.core.GenerateModal(key_mo);
            }
            else {
                this.core.RenderModal(key_mo);
            }
        });
        //Потом удаляем, которых уже нет
        Object.keys(this.ModalsObjs).forEach((key_mo, index_mo) => {
            let item_mo = this.ModalsObjs[key_mo];

            if(this.props.Modals[key_mo] == undefined) {
                this.core.RemoveModal(key_mo);
            }
        });
    };

    private events = {
        onClickCloth: (e, ModalConfig:IModalConfig) => {

        },
    };

    private core = {
        GenerateModal: (key_mo) => {
            this.ModalsObjs[key_mo] = {
                ModalConfig: this.props.Modals[key_mo],
                Container: this.core.GenerateContainer(),
                ref: null,
                content: null,
            };
            this.core.RenderModal(key_mo);
        },
        RemoveModal: (key_mo) => {
            this.core.RemoveContainer(key_mo);
            delete this.ModalsObjs[key_mo];
        },

        GenerateContainer: () => {
            let Container =  document.createElement('div');
            this.CoolModalForm_ref.appendChild(Container);
            return Container;
        },
        RemoveContainer: (key_mo) => {
            ReactDOM.unmountComponentAtNode(this.ModalsObjs[key_mo].Container);
            //Удаляем наш элемент
            this.CoolModalForm_ref.removeChild(this.ModalsObjs[key_mo].Container) ;
            this.ModalsObjs[key_mo].Container = null;
        },
        getContainer: (key_mo) => {
            if (!this.ModalsObjs[key_mo].Container) {
                this.ModalsObjs[key_mo].Container = this.core.GenerateContainer();
            }
            return this.ModalsObjs[key_mo].Container;
        },

        RenderModal: (key_mo) => {
            const container = this.core.getContainer(key_mo);
            const dataProps = {
                _ref: (ref)=>{this.ModalsObjs[key_mo].ref = ref},
                ModalConfig: this.props.Modals[key_mo],
            };

            this.ModalsObjs[key_mo].content = (
                <CoolModalFormInstance {...dataProps} />
            );

            Object.assign(this.ModalsObjs[key_mo].Container.style, {
            //    "z-index": '1300',
            //    position: 'absolute',
            //    left: `${0}px`,
            //    top: `${0}px`,
            });
            ReactDOM.render(this.ModalsObjs[key_mo].content, container);
        },
    };

    render() {
        return (
            <div ref={(ref)=>(this.CoolModalForm_ref = ref)} className="CoolModalForm"/> //По сути нам без разницы, что рендерить
        );
    }
}