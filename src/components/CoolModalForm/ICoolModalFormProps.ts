import IModalConfigDictionary from "./IModalConfigDictionary";

export default interface CoolModalFormProps {
    Modals: IModalConfigDictionary,
    container?: HTMLElement, //Пока функционал с передачей контейнера не сделан, по умолчанию всё строится внутри
}