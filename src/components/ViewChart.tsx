import * as React from "react";
import * as ReactDOM from "react-dom";
import { Bar, Pie, Line, Chart} from 'react-chartjs-2';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';

import * as ct_api_view from "../api/View-api";


interface IViewChartState{
  isLoading:boolean,
  chartData:any,
  viewUid:string,
}
interface IViewChartProps{
  viewUid:string,
}
export class ViewChart extends React.Component<IViewChartProps, IViewChartState>{
  constructor(props){
    super(props);
    this.state={
      isLoading:false,
      chartData:{},
      viewUid:null,
    }
  }
  componentDidMount(){
    this.initPieLabelPlugin();
    this.setState({isLoading:true, viewUid:this.props.viewUid, chartData:{}}, this.loadChartData)
  }
  componentDidUpdate(prevProps, prevSate){
    if(this.props.viewUid!==prevProps.viewUid){
      this.setState({isLoading:true, chartData:{}, viewUid:this.props.viewUid}, this.loadChartData)
    }
  }

  initPieLabelPlugin=()=>{
    const Plugins={
      calloutsLabel:function(){
              var Plugin = {
                id:'pieLabel',
                afterDatasetsDraw: function (chart, easing) {
                  if(chart.options.showLable){
                    var ctx = chart.ctx;
                    chart.data.datasets.forEach(function (dataset, i) {
                        var meta = chart.getDatasetMeta(i);
                        var total = meta.total;
                        if (!meta.hidden) {
                            var count=0;
                            var LegendPointArr= new Array();
                            var TextRevers= new Array();
                            TextRevers[0]= new Array();
                            TextRevers[1]= new Array();
                            var fontSize = 12;
                            var fontStyle = 'normal';
                            var fontFamily = 'Helvetica Neue';
                            var SetArrLegendFill= function(ArrPoint, xLabel, yLabel, w, h){
                                    w=parseInt(w);
                                    h=parseInt(h);
                                    xLabel-=w/2;
                                    yLabel-=h/2;
                                    xLabel=parseInt(xLabel);
                                    yLabel=parseInt(yLabel);
                                    for (var x = xLabel-4; x < xLabel+w+4; x++) {
                                        for (var y = yLabel-30; y < yLabel+h+30; y++) {
                                            var PointObj= new Object();
                                            PointObj['x']=x;
                                            PointObj['y']=y;
                                            ArrPoint.push(PointObj);
                                        }
                                    }
                                    return ArrPoint;
                                };
                            var CheckPointInArr = function(x, y, PointArr){
                                    x=parseInt(x);
                                    y=parseInt(y);
                                    for (var i = 0; i < PointArr.length; i++) {
                                        if(PointArr[i].x==x && PointArr[i].y==y){
                                            return true;
                                        }
                                    }
                                    return false;

                                };
                            var SetText = function(context, str, centerX, centerY, radius, angleend, anglestart, count, ColorLineOut) {
                                    context.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                                    context.fillStyle='#000';
                                    context.strokeStyle= ColorLineOut;
                                    var angel=(anglestart+angleend)/2+1.5707963267948966;
                                    var sin=Math.sin(angel);
                                    var cos=-1*Math.cos(angel);
                                    var texttWidth =context.measureText(str).width;
                                    var texttHeig =fontSize;

                                    var angleval= angel%(2*Math.PI);
                                    var xOfsett=0;
                                    var yOfsett=10;
                                    if(angleval>=0 && angleval<=Math.PI/2){
                                        context.beginPath();
                                        var xText=centerX+(sin*radius)+texttWidth/2+xOfsett;
                                        var yText=centerY+(cos*radius)-yOfsett-texttHeig/2;

                                        while(CheckPointInArr(xText-texttWidth/2, yText-texttHeig/2, LegendPointArr)){
                                            //yOfsett--;
                                            xOfsett++;
                                            yOfsett=yOfsett<0?0:yOfsett;
                                            xText=centerX+(sin*radius)+texttWidth/2+xOfsett;
                                            yText=centerY+(cos*radius)-yOfsett-texttHeig/2;
                                        }
                                        context.moveTo(centerX+(sin*radius), centerY+(cos*radius));
                                        context.lineTo(centerX+(sin*radius), centerY+(cos*radius)-yOfsett);
                                        context.lineTo(centerX+(sin*radius)+texttWidth+xOfsett, centerY+(cos*radius)-yOfsett);
                                        context.stroke();
                                        context.fillText(str, xText, yText);
                                        LegendPointArr=SetArrLegendFill(LegendPointArr, xText, yText, texttWidth, texttHeig);
                                   }else  if(angleval>Math.PI/2 && angleval<=Math.PI){
                                       var ReversObj = new Object();
                                       ReversObj['Text']=str;
                                       ReversObj['angleval']=angleval;
                                       ReversObj['r']=radius;
                                       ReversObj['x']=centerX;
                                       ReversObj['y']=centerY;
                                       ReversObj['ColorLineOut']=ColorLineOut;
                                       TextRevers[0].push(ReversObj);
                                   }else  if(angleval>Math.PI && angleval<=Math.PI*3/2){
                                        context.beginPath();
                                        yOfsett=texttHeig-2;
                                        var xText=centerX+(sin*radius)-texttWidth/2-xOfsett;
                                        var yText=centerY+(cos*radius)+yOfsett-texttHeig/2;
                                        while(CheckPointInArr(xText+texttWidth/2, yText+texttHeig/2, LegendPointArr) ){
                                           // yOfsett++;
                                            xOfsett++;
                                            yOfsett=yOfsett<0?0:yOfsett;
                                            xText=centerX+(sin*radius)-texttWidth/2-xOfsett;
                                            yText=centerY+(cos*radius)+yOfsett-texttHeig/2;
                                        }
                                        context.moveTo(centerX+(sin*radius), centerY+(cos*radius));
                                        context.lineTo(centerX+(sin*radius)-xOfsett, centerY+(cos*radius));
                                        context.lineTo(centerX+(sin*radius)-xOfsett, centerY+(cos*radius)+yOfsett);
                                        context.lineTo(centerX+(sin*radius)-texttWidth-xOfsett-5, centerY+(cos*radius)+yOfsett);
                                        context.stroke();
                                        context.fillText(str, xText-5, yText);
                                        LegendPointArr= SetArrLegendFill(LegendPointArr, xText, yText, texttWidth, texttHeig);
                                   }else  if(angleval>Math.PI*3/2 && angleval<=2*Math.PI){
                                       var ReversObj = new Object();
                                       ReversObj['Text']=str;
                                       ReversObj['angleval']=angleval;
                                       ReversObj['r']=radius;
                                       ReversObj['x']=centerX;
                                       ReversObj['y']=centerY;
                                       ReversObj['ColorLineOut']=ColorLineOut;
                                       TextRevers[1].push(ReversObj);
                                   }


                                };
                            var SetTextRevers= function(Revers, context){
                                //2 Part
                              var texttHeig=fontSize;
                              context.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                              context.fillStyle='#000';
                              var Data2= Revers[0].reverse();

                               for (var i = 0; i < Data2.length; i++) {
                                    context.strokeStyle=Data2[i].ColorLineOut;
                                    context.beginPath();
                                    var xOfsett=0;
                                    var yOfsett=texttHeig-2;
                                    var sin=Math.sin(Data2[i].angleval);
                                    var cos=-1*Math.cos(Data2[i].angleval);
                                    var texttWidth =context.measureText(Data2[i].Text).width;

                                    var xText = Data2[i].x + (sin * Data2[i].r) + texttWidth / 2 + xOfsett;
                                    var yText = Data2[i].y + (cos * Data2[i].r) + yOfsett - texttHeig / 2;
                                    while (CheckPointInArr(xText - texttWidth / 2, yText + texttHeig / 2, LegendPointArr)) {
                                        // yOfsett++;
                                        xOfsett++;
                                        xText = Data2[i].x + (sin * Data2[i].r) + texttWidth / 2 + xOfsett;
                                        yText = Data2[i].y + (cos * Data2[i].r) + yOfsett - texttHeig / 2;
                                    }
                                    context.moveTo(Data2[i].x + (sin * Data2[i].r), Data2[i].y + (cos * Data2[i].r));
                                    context.lineTo(Data2[i].x + (sin * Data2[i].r)+xOfsett, Data2[i].y + (cos * Data2[i].r));
                                    context.lineTo(Data2[i].x + (sin * Data2[i].r)+xOfsett, Data2[i].y + (cos * Data2[i].r) + yOfsett);
                                    context.lineTo(Data2[i].x + (sin * Data2[i].r) + texttWidth + xOfsett+5, Data2[i].y + (cos * Data2[i].r) + yOfsett);
                                    context.stroke();
                                    context.fillText(Data2[i].Text, xText+5, yText);
                                    LegendPointArr = SetArrLegendFill(LegendPointArr, xText, yText, texttWidth, texttHeig);
                               }
                                //4 Part
                              var Data4= Revers[1].reverse();
                                for (var i = 0; i < Data4.length; i++) {
                                    context.strokeStyle=Data4[i].ColorLineOut;
                                    context.beginPath();
                                    var xOfsett=0;
                                    var yOfsett=10;
                                    var sin=Math.sin(Data4[i].angleval);
                                    var cos=-1*Math.cos(Data4[i].angleval);
                                    var texttWidth =context.measureText(Data4[i].Text).width;

                                    var xText = Data4[i].x + (sin * Data4[i].r) - texttWidth / 2 - xOfsett;
                                    var yText = Data4[i].y + (cos * Data4[i].r) - yOfsett - texttHeig / 2;
                                    while (CheckPointInArr(xText + texttWidth / 2, yText - texttHeig / 2, LegendPointArr)) {
                                        // yOfsett++;
                                        xOfsett++;
                                        xText = Data4[i].x + (sin * Data4[i].r) - texttWidth / 2 - xOfsett;
                                        yText = Data4[i].y + (cos * Data4[i].r) - yOfsett - texttHeig / 2;
                                    }
                                    context.moveTo(Data4[i].x + (sin * Data4[i].r), Data4[i].y + (cos * Data4[i].r));
                                    context.lineTo(Data4[i].x + (sin * Data4[i].r), Data4[i].y + (cos * Data4[i].r) - yOfsett);
                                    context.lineTo(Data4[i].x + (sin * Data4[i].r) - texttWidth - xOfsett, Data4[i].y + (cos * Data4[i].r) - yOfsett);
                                    context.stroke();
                                    context.fillText(Data4[i].Text, xText, yText);
                                    LegendPointArr = SetArrLegendFill(LegendPointArr, xText, yText, texttWidth, texttHeig);
                                }

                           }
                            meta.data.forEach(function (element, index) {

                                let isDraw=true;
                                if(chart.legend && chart.legend.legendItems && chart.legend.legendItems.length && chart.legend.legendItems[index] && chart.legend.legendItems[index].hidden){
                                  isDraw=false;
                                }
                                if(isDraw){
                                  // Draw the text in black, with the specified font
                                  ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                                  // Just naively convert to string for now
                                  var dataString = dataset.data[index].toString();

                                  // Make sure alignment settings are correct
                                  ctx.textAlign = 'center';
                                  ctx.textBaseline = 'middle';

                                  var padding = 0;
                                  var position = element.tooltipPosition();
                                  var CalacPer = Number(Number(dataset.data[index] / total * 100).toFixed(2));
                                  var percent = String(Number(dataset.data[index] / total * 100).toFixed(1)) + "%";
                                  if(CalacPer>0){
                                      count++;
                                      var ColorLineOut=element._model.backgroundColor;
                                      var label=chart.config.data.labels[index]+' '+percent;
                                      SetText(ctx, label, element._model.x, element._model.y, element._model.outerRadius, element._model.endAngle, element._model.startAngle, count, ColorLineOut);
                                  }
                              }

                            });
                            SetTextRevers(TextRevers, ctx);
                        }
                    });
                  }
              },
                beforeInit: function(chart, options) {
                  chart.titleBlock.afterFit = function() {
                    this.height = this.height + 25;
                  };
                }
              };
              return Plugin;
      },
    }
    let Allplugins=Chart.pluginService.getAll();
    let existPlug= Allplugins.filter((plugin)=>{
      return plugin.id?true:false;
    }).map((plugin)=>{
      return plugin.id;
    })
    if(existPlug.indexOf('pieLabel')==-1){
        Chart.pluginService.register(Plugins.calloutsLabel());
    }
  }

  loadChartData=()=>{
    ct_api_view.getDataGraph(this.props.viewUid).then((res)=>{
      this.setState({isLoading:false, chartData:res.data})
    });
    return false;
    //тестовые данные
    const dataTestBar={
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    };
    const dataTestPie={
        type: 'pie',
        data: {
          	labels: [
          		'Red',
          		'Blue',
          		'Yellow'
          	],
          	datasets: [{
          		data: [300, 50, 100],
          		backgroundColor: [
          		'#FF6384',
          		'#36A2EB',
          		'#FFCE56'
          		],
          		hoverBackgroundColor: [
          		'#FF6384',
          		'#36A2EB',
          		'#FFCE56'
          		]
          	}]
          },
        options:{showLable:true},
    };
    const dataTestline={
      type: 'line',
      data: {
          labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
          datasets: [{
              label: 'My First dataset',
              backgroundColor: 'rgb(255, 99, 132)',
              borderColor: 'rgb(255, 99, 132)',
              data: [0, 10, 5, 2, 20, 30, 45]
          }]
      },
      options: {}
  };
    this.setState({isLoading:false, chartData:{
    'key1':dataTestBar,
    'key2':dataTestPie,
    'key3':dataTestline,
  }})
}

  renderCharts=()=>{
    let chartData=this.state.chartData;
    let keys=chartData?Object.keys(chartData):[];
    if(chartData && keys.length){
      return keys.map((key)=>{
        let options=chartData[key].options?chartData[key].options:{};
        options.maintainAspectRatio=false;
        if(chartData[key].type.toLowerCase()=='bar'){
          return this.renderBar(chartData[key].data, options, key);
        }else if(chartData[key].type.toLowerCase()=='line'){
          return this.renderLine(chartData[key].data, options, key);
        }else if(chartData[key].type.toLowerCase()=='pie'){
          return this.renderPie(chartData[key].data, options, key);
        }

      })
    }
    return null;
  }

  renderBar=(data, options, key)=>{
      return(  <div className="chart" key={'chart_'+'_'+this.state.viewUid+'_'+key}>
          <Bar
            data={data}
            options={options}
          />
        </div>);
  }
  renderLine=(data, options, key)=>{
    return(
      <div className="chart" key={'chart_'+'_'+this.state.viewUid+'_'+key}>
        <Line
          data={data}
          options={options}
        />
      </div>

    );
  }
  renderPie=(data, options, key)=>{
    options.layout = {padding: {
                left: 25,
                right: 25,
                top: 25,
                bottom: 25
            }}
    return(
      <div className="chart" key={'chart_'+'_'+this.state.viewUid+'_'+key}>
        <Pie
          data={data}
          options={options}
        />
      </div>
    );
  }

  render(){
    let LoaderBlockUI = <Loader active type="ball-pulse"/>;
    let Charts= this.renderCharts();
    return(
      <BlockUi style={{display:'block'}} id="zt_AppPage" tag="div" loader={LoaderBlockUI} blocking={this.state.isLoading}>
        {Charts}
      </BlockUi>
    )
  }
}
