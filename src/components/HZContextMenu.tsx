import * as React from "react";
import * as ReactDOM from "react-dom";
import HZContextMenuInstance from './HZContextMenuInstance';

export interface HZContextMenuProps {
    target?: HTMLInputElement; //Если мы таргет не передаём, то элемент удаляется
    content?: object[];
    CloseCMFunc: () => void
}
interface IHZContextMenuState {

}

export default class HZContextMenu extends React.Component<HZContextMenuProps, IHZContextMenuState> {
    state: Readonly<IHZContextMenuState> = {
        hovers: []
    };

    private CM_ref: HTMLDivElement;
    private CM: JSX.Element[] | JSX.Element;
    private cmContainer: HTMLDivElement;


    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
        this.getContainer();
    };
    componentWillMount = () =>  {
        document.addEventListener('mousedown', this.handleClick, false);
    };
    componentWillUnmount = () => {
        if (this.cmContainer) {
            //Размонтируем виртуальный компонент
            ReactDOM.unmountComponentAtNode(this.cmContainer);
            //Удаляем наш элемент
            document.body.removeChild(this.cmContainer) ;
            this.cmContainer = null;
        }
        document.removeEventListener('mousedown', this.handleClick, false);
    };
    shouldComponentUpdate = (nextProps, nextState) => {
        return true;
    };
    componentDidUpdate  = (prevProps, prevState) => {
        if(this.props.target) {
            if(prevProps.target!=this.props.target || !this.props.target) {
                //Т.е. мы не будем производить обновление контекстного меню, пока мы его не закроем, а потом откроем снова!!! Это защита от конфликтов со сменой состояния
                //Например у нас происходит асинхронно 2 события
                //1-ое любая смена состояния родителя, которая вызовет тут componentDidUpdate с пересозданием меню (renderCM)
                //2-ое нажатие на элемент контекстного меню, который тоже вызовет смену состояния, однако менять будет нечего, так как уже произошло пересоздание
                this.renderCM(this.props.target);
            }
            else {
                this.renderCM(this.props.target);
            }

        }
        else {
            if (this.cmContainer) {
                //Размонтируем виртуальный компонент
                ReactDOM.unmountComponentAtNode(this.cmContainer);
                //Удаляем наш элемент
            //    document.body.removeChild(this.cmContainer) ;
            //    this.cmContainer = null;
            }
            this.CM = null;
        }
    };

    handleClick = (e) => {
        if(this.CM_ref) {
            if(this.CM!= null && this.CM_ref.contains(e.target)){
            }
            else if(this.CM!= null) {
                this.props.CloseCMFunc();
                ReactDOM.unmountComponentAtNode(this.cmContainer);
                this.CM = null;
            }
        }
    };
    getContainer = () => {
        if (!this.cmContainer) {
            this.cmContainer = document.createElement('div');
            document.body.appendChild(this.cmContainer);
        }
        return this.cmContainer;
    };
    renderCM = (element: HTMLDivElement) => {
        if (this.CM) {
            ReactDOM.unmountComponentAtNode(this.cmContainer);
            //document.body.removeChild(this.cmContainer) ;
            this.CM = null;

            this.setState({});

            //Скорее всего, если мы сюда попали, то у нас конфликт контекстного меню
            //Мы удаляем контекстное меню и после этого выполняем событи нажатие кнопки над ним
            return;
        }

        const dataProps = {
            content: this.props.content,
            _ref: (ref)=>{this.CM_ref = ref}
        };
        this.CM = (
            <HZContextMenuInstance{...dataProps}/>
        );

        const container = this.getContainer();
        Object.assign(this.cmContainer.style, {
            "z-index": '100',
            position: 'absolute',
            left: `${0}px`,
            top: `${0}px`,
        });


        var el = ReactDOM.render(this.CM, container, () => {
            console.log(this.cmContainer);

            let sizes_target = element.getBoundingClientRect();
            let sizes = this.cmContainer.getBoundingClientRect();
            let x = sizes.x;


            let HeightWindow = window.innerHeight;
            let WidthWindow = window.innerWidth;


            let top = sizes_target.y + sizes_target.height;
            if(HeightWindow < top + sizes.height) {
                top = sizes_target.y - sizes.height;
            }
            let left = sizes_target.x;
            if(WidthWindow < left + sizes.width) {
                left = WidthWindow - sizes.width;
            }

            Object.assign(this.cmContainer.style, {
                left: `${left}px`,
                top: `${top}px`,
            });
        });
    };


    render() {
        return (
            <div className="HZContextMenu_node" style={{display:"none"}}/> //По сути нам без разницы, что рендерить
        );
    }
}