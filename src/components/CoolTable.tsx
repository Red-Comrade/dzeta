/**
Шаблонное дерево с вкладками,
поддержкой пагинации,
Поиском, сортировками и т.д.
*/

import * as React from "react";
import * as ReactDOM from "react-dom";

import HZContextMenu from "./HZContextMenu";
import HZTabs from "./HZTabs";
import {number} from "prop-types";

//Интерфейс атрибутов вкладки

export interface ITD {
    key: string,
    is_merge?: 0 | 1,
    css?: {
        background?: string,
        color?: string,
    }
}
export interface ITR {
    trKey?: number,
    tds?: ITD[],
    noneTR?: boolean,
}

export interface ITR_tree_head {
    key: string,
    Sort?: {isActive: boolean, dir: 0 | 1, disabled: boolean},
    rowspan?: number,
    colspan?: number,
    name: string,
    data?: any,
    data_td?: any,
}

export interface IajaxData {
    xhr: {cancel:any},
    start: number,
    length: number,
    isFirst: boolean,
    isLoadTotal: 0 | 1,
    getOnlyTotal: 0 | 1,

    success?: (e) => void,
    error?: (e) => void,
}

export interface ICoolTableProps {
    headerData: {
        key: string,
        IsFixedWidth: boolean, //Если стоит это значение, то мы при первом построении таблицы не будет автоматически менять ширину этой ячейки
        MinWidth: number, //Минимальная ширина при колонки при построении колонки. Если IsFixedWidth = false, то будет динамически подстраиваться
        IsHide?: boolean,
        IsFixed?: boolean,
        Sort?: {isActive?: boolean, dir: 0 | 1, disabled?: boolean},
        rowspan?: number,
    }[],
    headerDataTree?: ITR_tree_head[][],

    filterData?: {
        key: string,
    }[],

    selectRowKey?: number,
    onSelectRow?: (item_tr:any) => void,
    onClickTD?: (e, params:{item_tr, item_td, index_tr, index_td, item_th} ) => void,
    onDoubleClickTD?: (e, item_tr, item_td) => void,
    onRightClickTD?: (e, item_tr, item_td) => void,
    onDrawTable?: (params: {readonly tableData: any, readonly headerData: any}) => void,  //к сожалению мы передаём ссылку, поэтому не защищены от изменений. Клонировать не хочу

    settings?: {
        isMergeTable?: boolean,
        isTrHTML?: boolean, //IF true, then tr consist html //TODO::FUTURE

        ajax?: {
            url?: string,
            type?: string,
            data?: any, //кастомная дата для передачи, Возможна передача callback-функции
            success?: (e) => void,
            error?: (e) => void,

            callbackData?: (e) => ITR[],
        },

        data?: ITR[];
        ajax_callback?: (ajaxData:IajaxData) => void,

        IsFullData?: boolean,
        Paging?: boolean,


        ajax_callback_tfooter?: (ajaxData:IajaxData) => void,
    }

    callbackRenderTD?: (dataTD?:object, dataTR?:any, dataTH?:any) => { contentJSX: (JSX.Element[] | JSX.Element), className?: string, } | string,
    callbackRenderTH?: (dataTD?:object, dataTR?:any) => JSX.Element[] | JSX.Element | string;
    callbackRenderFilterTH?: (dataTD?:object, dataTR?:any) => JSX.Element[] | JSX.Element | string;

    onClickTH?: (e:object, item_th:object) => void;

    RecalcStyleFunc?: () => void, //При вызове этой функции она самоуничтожается в пропсах

    ReloadTrig?: () => void,
    forceupdate?:number,
}
interface ICoolTableState {
    targetCM?: HTMLInputElement,
    contentCM?: object[],
    columnsWidth: object,
    columnsWidth_fixed: object,
    areaWidth?: number,


    middlePX: number,
    isFirst: boolean,
    data?: ITR[], //Массив строк, который сейчас отображаются
    data_tfooter?: ITR[], //Массив строк, который сейчас отображаются
    isPlug: boolean, //Параметр для оптимизации, чтобы мы не перерисовывали лишний раз.
}

export default class CoolTable extends React.Component<ICoolTableProps, ICoolTableState> {
    static defaultProps = {
        isMergeTable: true,
        IsFullData: false,
        Paging: false,
    };

    _ref_area_wrapper: HTMLElement;

    _ref_cool_t_gc_table: HTMLElement;
    _ref_cool_t_gc_table_fixed: HTMLElement;

    _ref_gc_thead: HTMLElement;
    _ref_gc_thead_fixed: HTMLElement;
    _ref_gc_tbody: HTMLElement;
    _ref_gc_tbody_fixed: HTMLElement;
    _ref_gc_tfooter: HTMLElement;
    _ref_gc_tfooter_fixed: HTMLElement;
    _ref_gc_k_div: HTMLElement;
    _ref_gc_k_div_fixed: HTMLElement;
    _ref_gc_rows: HTMLElement;
    _ref_gc_rows_fixed: HTMLElement;
    _ref_gc_loading: HTMLElement;
    _ref_area: HTMLElement;
    _ref_count_rows: HTMLElement;
    DelayRecalcTimer: any;
    DelayMergeTimer: any;

    splitter = {
        is_resizing: false,
        start_x_drg: 0,
        move_x_drg: 0,
        item_th: null,
        index_th: 0,
        target: null,
    };


    ID_Table:string = "cool_t-" + Math.floor(Math.random() * 1000000); //Внутренний ID, нужен для правильной стилизации


    //config settings
    settings = {
        Start: 0, //Верхний отступ, начало таблицы в количестве строк
        Start_NotNone: 0, //Верхний отступ, начало таблицы в количестве строк
        StartPX: 0, //Верхний отступ, начало таблицы в пикселях
        StartPX_NotNone: 0,

        HeightTR: 31,
        HeightTRZooM: 31,
        HeightHead: 31,
        HeightBody: 31,
        allHeightPX: 31,

        Length:0,
        LengthPX:0,
        Length_NotNone:0,
        LengthPX_NotNone:0,
        End_NotNone:0,

        PreTokenAjax: {
            isToken: true,
            CurrStartPX: 0,
            CurrEndPX: 0,
        },

        PreTokenAjax_tfooter: {
            isToken: true,
        },

        ajax: {
            data: null, //кастомная дата для передачи, Возможна передача callback-функции
            start: 0,
            length: 0,

            isFirst: true,
            isLoadTotal: 0,
            getOnlyTotal: 0,

            success: null,
            error: null,
            callbackData: null,
        },
        ajax_tfooter: {
            data:null,
            success: null,
            error: null,
            callbackData: null,
        },
        Layer3Data: {
            isLoadedTotal: false,
            isLoadTotal: true,
            data: [],
            allCount: 0,
            result: 1,
            OutData: {} //Данные, которые мы отправляем (всякие поиски, сортировки и прочее)
        },
        Layer3Data_tfooter: {
            data: [],
            result: 1,
        },
        xhr: null,
        xhr_tfooter: null,
        is_need_recalc: false,

        ErrorFunction: null,
        SuccessFunction: null,

        data: null,
        scroller: {
            boundaryScale: 0.8, //пограничный коэффициент для прорисовки таблицы по умолчанию(0,5)

            boundaryScale_NotNone: 0.5,
            loadingIndicator: true,
            // displayBuffer: 22, //условное число, для количества записей на странице

            FirstCountBuffer: 200, //Первая выгрузка
            countBuffer: 1200, //Количество записей выгрузки

            countBufferVisible: 50, //Видимые строки
            serverWait: 50, //задержка перед отправкой
            EvgenPrediction: 300, //красная граница, Если останется меньше чем это количество строк для построения 2-ой прослойки, то будем выполнять дополнительную подгрузку, ну это в 3юю прослойку

            FirstLazyCount: 1500, //Догружаемое количество записей, которое будет в таблице
            LazyCount: 5000, //Ленивая выгрузка, столько записей будет выгружаться в ленивом режиме
            EvgenPredicationLazyMax: 3000, //Ленивый Евген, пограничное значение в прослойке, когда не надо загружать данные!!! Чтобы не было слишком частых ajax-запросов по маленькой!

            dirMax: 0.83 //Количество строк в направлении по направлению скролла //коэфф
        },
        slide: {
            timer: null,
            dir: 0,
        },
    };
    //стили, которые нужно иницилизировать для мерджа по ширине
    mergeThs_style:{[key:string]:{start:number, end: number}} = {};

    constructor(props: ICoolTableProps) {
        super(props);
        /*
        * However, it’s not an anti-pattern if you make it clear that the prop is only seed data for the component’s internally-controlled state
        * */

        this.helper.Merge(this.settings, props.settings);

        this.state = {
            columnsWidth: {},
            columnsWidth_fixed: {},
            data: [],
            data_tfooter: [],
            isFirst: true,
            middlePX: 0,
            isPlug: false,
        };
    };
    shouldComponentUpdate(nextProps: Readonly<ICoolTableProps>, nextState: Readonly<ICoolTableState>, nextContext: any): boolean {
        if(nextProps.RecalcStyleFunc) {
            this.core.RecalcStyle();
            nextProps.RecalcStyleFunc();
            return false;
        }
        else if(nextProps.ReloadTrig) {
            nextProps.ReloadTrig();
            this.core.Reload();
        }

        return true;
    }

    componentDidUpdate(prevProps: Readonly<ICoolTableProps>, prevState: Readonly<ICoolTableState>, snapshot?: any): void {
        if(this.state.data.length > 0 && prevState.isPlug != this.state.isPlug) {
            if(this.state.isFirst) {
                this.settings.StartPX_NotNone = 0; //отступ свехру без учета строк none
                this.settings.Start_NotNone = 0;

                let $trs = this._ref_gc_rows.getElementsByClassName('cool_t_tr');
                let $trs_fixed;
                if(this._ref_gc_rows_fixed) {
                    let $trs_fixed = this._ref_gc_rows_fixed.getElementsByClassName('cool_t_tr');
                }

                for(let i = 0; i < $trs.length; i++) {
                    if(i < this.settings.scroller.countBufferVisible) {
                        if($trs[i] != undefined) {
                            $trs[i].classList.remove('noneTR');
                            this.state.data[i].noneTR = false;

                            if($trs_fixed && $trs_fixed[i]!= undefined) {
                                $trs_fixed[i].classList.remove('noneTR');
                            }
                        }
                        else break;
                    }
                    else {
                        $trs[i].classList.add('noneTR');
                        this.state.data[i].noneTR = true;
                    }

                }
                this.settings.End_NotNone = this.settings.scroller.countBufferVisible;
            }
            else {
                /*
                let $trs = this._ref_gc_rows.getElementsByClassName('cool_t_tr');

                for(let i = 0; i < $trs.length; i++) {
                    if($trs[i] != undefined) {
                        $trs[i].classList.add('noneTR');
                    }
                }
                */

                this.core.GoNoneLayer2({
                    middlePX: this.state.middlePX,
                    is_ajax: true,
                })
            }
            console.log('Ajax-request is success!!!');
            this.events.onScrollBody(null)

            if(this.props.onDrawTable) {
                this.props.onDrawTable({
                    tableData: this.settings.Layer3Data.data,
                    headerData: this.props.headerData,
                })
            }
        }
        if(this.props.forceupdate!==prevProps.forceupdate){
          this.setState({data:this.props.settings.data, isPlug:!this.state.isPlug});
        }

        if(this.settings.is_need_recalc) {
            this.settings.is_need_recalc = false;
            this.core.RecalcStyle();
        }
    }

    componentWillMount = () =>  {
        if(this.props.settings.ajax) {
            //Значит мы выполняем режим работы с получением данных с сервера и выгрузки пачками
        }
        else {
            //Иначе мы берём пачку данных, которая пришла с пропсов и работаем с ней.
        }
        document.addEventListener('mousemove', this.events.mousemove);
        document.addEventListener('mouseup', this.events.mouseup);
    };
    componentDidMount = () => {
        this.core.RecalcStyle();
        window.addEventListener('resize', this.events.onResizeDoc , false);

        this._ref_gc_loading.classList.remove('fxs-display-none');
        this.core.GoAjax({isFirst: true, middlePX: 0, dir: 0, FuncSuc: () => {
                this._ref_gc_loading.classList.add('fxs-display-none');
            }});
    };
    componentWillUnmount = () => {
        document.removeEventListener('mousemove', this.events.mousemove);
        document.removeEventListener('mouseup', this.events.mouseup);
        window.removeEventListener('resize', this.events.onResizeDoc , false);
        let style_el = document.getElementById('cool_t_style-'+this.ID_Table);
        if(style_el) {
            style_el.remove();
        }
        if(this.settings.xhr) {
            if(this.settings.xhr.cancel) {
                this.settings.xhr.cancel();
            }
        }
        if(this.settings.xhr_tfooter) {
            if(this.settings.xhr_tfooter.cancel) {
                this.settings.xhr_tfooter.cancel();
            }
        }
    };

    events = {
        onResizeDoc: (e) => {
            this.core.DelayRecalc( () =>{
                this.core.RecalcStyle();
            }, )
        },

        onClickTH: (e, item_th) => {
            if(this.props.onClickTH) {
                this.props.onClickTH(e, item_th);
            }
        },

        onScrollBody: (e) => {
            this._ref_gc_thead.scrollLeft = this._ref_gc_tbody.scrollLeft;
            if(this._ref_gc_tbody_fixed) {
                this._ref_gc_tbody_fixed.scrollTop = this._ref_gc_tbody.scrollTop;
            }
            if(this._ref_gc_tfooter_fixed) {
                this._ref_gc_tfooter_fixed.scrollLeft = this._ref_gc_tbody_fixed.scrollLeft;
            }
            if(this._ref_gc_tfooter) {
                this._ref_gc_tfooter.scrollLeft = this._ref_gc_tbody.scrollLeft;
            }

            if(!this.props.settings.IsFullData) {
                let p_scr = this._ref_gc_tbody.scrollTop;
                let h_body = this.settings.HeightBody;
                let allHeightPX = this.settings.allHeightPX; //Высота контейнера, в котором таблица
                let HeightTRPX = this.settings.HeightTRZooM;

                let middlePX = p_scr + h_body/2; //Центральное положение
                let Length = Math.floor(h_body / HeightTRPX); //Однако это лишь высота таблицы в количествах строк

                let countStart = Math.ceil(p_scr/this.settings.HeightTRZooM);
                let countEnd = countStart + Length;

                let Start = this.settings.Start; //С этого значения мы вставляем блок строк, допустим с 200 (до 200-ой у нас отступ)
                let StartPX = this.settings.StartPX;
                let LengthPX = this.settings.LengthPX;
                let countAll = this.settings.Layer3Data.allCount;



                let boundaryScale = this.settings.scroller.boundaryScale;
                let boundaryScale_NotNone = this.settings.scroller.boundaryScale_NotNone;

                let CurrStartPX = StartPX + (LengthPX - LengthPX * boundaryScale)/2; //начало, после которого срабатывает триггер
                let CurrEndPX = (StartPX + LengthPX) - (LengthPX - LengthPX * boundaryScale)/2;


                //NO PAGING
                //#region count panel
                if(Length>0) {
                    let start_sh = countStart - Start == 0 ? 1 : countStart - Start;
                    let to_sh = Length + countStart - Start;

                    start_sh = (countStart == 0 && countAll != 0) ? 1 : countStart;
                    countEnd = countEnd < countAll ? countEnd : countAll;
                    to_sh = countEnd < countStart ? countStart : countEnd;



                    this._ref_count_rows.innerHTML = 'Showing '+ start_sh+' to '+to_sh+' of '+this.settings.Layer3Data.allCount+' records.';
                }
                else {
                    this._ref_count_rows.innerHTML = 'No data';
                }
                //#endregion


                let
                    StartPX_NotNone = this.settings.StartPX_NotNone,
                    LengthPX_NotNone = this.settings.LengthPX_NotNone,

                    CurrStartPX_NotNone = StartPX_NotNone + (LengthPX_NotNone - LengthPX_NotNone * this.settings.scroller.boundaryScale_NotNone)/2, //начало, после которого срабатывает триггер на вторую прослойку
                    CurrEndPX_NotNone = (StartPX_NotNone + LengthPX_NotNone) - (LengthPX_NotNone - LengthPX_NotNone * this.settings.scroller.boundaryScale_NotNone)/2, //конец, после которого срабатывает триггер на вторую прослойку

                    Layer1 = (middlePX < CurrStartPX && StartPX > 0) || (middlePX > CurrEndPX && (allHeightPX - (StartPX + LengthPX)>0)),
                    Layer2 = (middlePX < CurrStartPX_NotNone && StartPX_NotNone > 0) || (middlePX > CurrEndPX_NotNone && (allHeightPX - (StartPX_NotNone + LengthPX_NotNone)>0));





                if(Layer1) {
                    //Тут срабатывает триггер
                    console.log('Попали в условие');
                    this.core.SideDelay((dir) => {
                        console.error(dir);

                        if(this.props.settings.isMergeTable) {
                            this.core.MergeDelay(null);
                        }

                        if(dir > 0) {
                            console.log('Вверх --->');
                        }
                        else {
                            console.log('Вниз <---');
                        }

                        console.log('Пошло выполнение функции')
                        if(this.settings.PreTokenAjax.isToken) {
                            this.settings.PreTokenAjax.CurrStartPX = CurrStartPX;
                            this.settings.PreTokenAjax.CurrEndPX = CurrEndPX;
                            this.settings.PreTokenAjax.isToken = false;

                            this._ref_gc_loading.classList.remove('fxs-display-none');
                            this.core.GoAjax({isFirst: false, middlePX: middlePX, dir: dir, FuncSuc: () => {
                                    this._ref_gc_loading.classList.add('fxs-display-none');
                                }});

                        }
                        else {
                            console.log('Но не сработало')
                        }
                    }, this.settings.scroller.serverWait, p_scr);
                }
                else {
                    this.core.SideDelay(false, 0, 0); //Второй вариант работы ajax, чтобы не перегружать событиями


                    //Для работы с прослойкой скрытых строк

                    if(Layer2) {
                        if(this.props.settings.isMergeTable) {
                            this.core.MergeDelay(null);
                        }

                        console.log('curr_start: '+CurrStartPX_NotNone + '; curr_end: ' + CurrEndPX_NotNone + '; middle: ' + middlePX);
                        console.log('Второй триггер');

                        this.core.GoNoneLayer2({middlePX: middlePX});
                    }
                    if(this.props.settings.isMergeTable) {
                        this.core.MergeDelay( () => {
                            console.log('Мерджим! Мерджим! Мерджим!');
                            let none_tr = countStart - Start;
                            console.log('Начало мерджа с ' + none_tr + '; Количество ' + Length + '; Start ' + Start);

                            if(Start == 0 && none_tr == 1) {
                                none_tr = 0;
                            }
                            this.core.MergeCells(none_tr, Length);

                        });
                    }
                }

            }
        },
        onScrollBodyFixed: (e) => {
            this._ref_gc_thead_fixed.scrollLeft = this._ref_gc_tbody_fixed.scrollLeft;
        },
        onWheelBodyFixed: (e) => {
            let delta = e.deltaY;
            let scrollTo = 0.3 * delta;

            if (scrollTo) {
                e.preventDefault();
                this._ref_gc_tbody.scrollTop = scrollTo + this._ref_gc_tbody.scrollTop;
            }
        },

        onClickTr: (e, item_tr) => {
            if(this.props.onSelectRow) {
                this.props.onSelectRow(item_tr)
            }
        },
        onClickTD: (e, params) => {
            if(this.props.onClickTD) {
                this.props.onClickTD(e, params);
            }
        },
        onDoubleClickTD: (e, item_tr, item_td) => {
            if(this.props.onDoubleClickTD) {
                this.props.onDoubleClickTD(e, item_tr, item_td);
            }
        },
        onRightClickTD: (e, item_tr, item_td) => {
            if(this.props.onRightClickTD) {
                e.preventDefault();
                this.props.onRightClickTD(e, item_tr, item_td);
            }
            else {
            }
        },

        mousemove: (e) => {
            if(this.splitter.is_resizing) {
                e.preventDefault();
                this.splitter.target.style.left = e.clientX - this.splitter.start_x_drg - 4 + 'px';
            }
        },
        onMouseDown_splitter: (e, item_th, index_th) => {
            console.log(e);
            this.splitter.target = e.target;
            this.splitter.is_resizing = true;
            this.splitter.start_x_drg = e.clientX;
            this.splitter.index_th = index_th;
            this.splitter.item_th = item_th;

            e.preventDefault();
        },
        mouseup: (e) => {
            if(this.splitter.is_resizing) {
                e.preventDefault();
                let pr = e.clientX - this.splitter.start_x_drg - 4; // на такое расстояние мы сдвигаем колонку
                let mWidth = this.splitter.item_th.MinWidth;



                let oldWidth;
                if(this.splitter.item_th.IsFixed) {
                    oldWidth = this.state.columnsWidth_fixed[this.splitter.item_th.key].Width;
                }
                else {
                    oldWidth = this.state.columnsWidth[this.splitter.item_th.key].Width;
                }

                let nWidth = oldWidth + pr;

                if(nWidth < mWidth) {
                    nWidth = mWidth;
                }

                if(this.splitter.item_th.IsFixed) {
                    this.state.columnsWidth_fixed[this.splitter.item_th.key].Width = nWidth;
                    this.state.columnsWidth_fixed[this.splitter.item_th.key].MinWidth = nWidth;
                    this.state.columnsWidth_fixed[this.splitter.item_th.key].IsFlexible = false;
                }
                else {
                    this.state.columnsWidth[this.splitter.item_th.key].Width = nWidth;
                    this.state.columnsWidth[this.splitter.item_th.key].MinWidth = nWidth;
                    this.state.columnsWidth[this.splitter.item_th.key].IsFlexible = false;
                }

                //Проходимся по древовидным колонкам, если есть


                this.splitter.is_resizing = false;
                this.splitter.target.style.removeProperty('left');
                this.splitter.target = null;

                this.core.RecalcStyle();
            }
        },
    };

    core = {
        RecalcStyle: () => {
            let areaWidth = Math.floor(this._ref_gc_tbody.clientWidth); //(отнимаем 1 пиксель, так как браузер неправильно считаетx)
            let contentWidth = Math.floor(this._ref_area.clientWidth);


            let columnsWidth = this.state.columnsWidth;
            let new_columnsWidth = {};
            let columnsWidth_fixed = this.state.columnsWidth_fixed;
            let new_columnsWidth_fixed = {};


            //Делаем проход по всем колонкам, чтобы узнать, нужно ли увеличивать, или уменьшать их
            let minWidth_fixed = 0; //Минимальная ширина фиксированных колонок
            let minWidth = 0;
            let n_flexibleCell = 0;
            let n_flexibleCell_fixed = 0; //Количество растягиваемых фиксированныъ колонок
            let n_notFixedCell = 0;

            this.props.headerData.forEach( (item_th, key) => {
                if(!item_th.IsHide) {
                    if(item_th.IsFixed) {
                        //_ref_cool_t_gc_table_fixed

                        if(columnsWidth_fixed[item_th.key] === undefined) {
                            minWidth_fixed += item_th.MinWidth;
                            new_columnsWidth_fixed[item_th.key] = {
                                IsFixedWidth: item_th.IsFixedWidth,
                                MinWidth: item_th.MinWidth,
                                Width: item_th.MinWidth,
                                IsFlexible: !item_th.IsFixedWidth
                            };
                        }
                        else {
                            minWidth_fixed += columnsWidth_fixed[item_th.key].MinWidth;

                            new_columnsWidth_fixed[item_th.key] = {
                                IsFixedWidth: columnsWidth_fixed[item_th.key].IsFixedWidth,
                                MinWidth: columnsWidth_fixed[item_th.key].MinWidth,
                                Width: columnsWidth_fixed[item_th.key].Width,
                                IsFlexible: columnsWidth_fixed[item_th.key].IsFlexible,
                            };

                            if(!new_columnsWidth_fixed[item_th.key].IsFlexible) {
                                new_columnsWidth_fixed[item_th.key].Width = columnsWidth_fixed[item_th.key].MinWidth;
                            }


                        }

                        if(new_columnsWidth_fixed[item_th.key].IsFlexible) {
                            n_flexibleCell_fixed ++;
                        }
                        if(!new_columnsWidth_fixed[item_th.key].IsFixedWidth) {
                            n_flexibleCell_fixed ++;
                        }

                    }
                    else {
                        if(columnsWidth[item_th.key] === undefined) {
                            minWidth += item_th.MinWidth;
                            new_columnsWidth[item_th.key] = {
                                IsFixedWidth: item_th.IsFixedWidth,
                                MinWidth: item_th.MinWidth,
                                Width: item_th.MinWidth,
                                IsFlexible: !item_th.IsFixedWidth
                            };
                        }
                        else {
                            minWidth += columnsWidth[item_th.key].MinWidth;

                            new_columnsWidth[item_th.key] = {
                                IsFixedWidth: columnsWidth[item_th.key].IsFixedWidth,
                                MinWidth: columnsWidth[item_th.key].MinWidth,
                                Width: columnsWidth[item_th.key].Width,
                                IsFlexible: columnsWidth[item_th.key].IsFlexible,
                            };

                            if(!columnsWidth[item_th.key].IsFlexible) {
                                new_columnsWidth[item_th.key].Width = columnsWidth[item_th.key].MinWidth;
                            }
                        }

                        if(new_columnsWidth[item_th.key].IsFlexible) {
                            n_flexibleCell ++;
                        }
                        if(!new_columnsWidth[item_th.key].IsFixedWidth) {
                            n_notFixedCell ++;
                        }
                    }
                }
            });


            if(minWidth_fixed > contentWidth - 150) {
                //Значит есть какие-то зафиксированные ячейки
                minWidth_fixed = contentWidth/2;
            }



            if(minWidth < areaWidth) {
                //растягиваем ячейки
                let e_width = areaWidth - minWidth;

                if(n_flexibleCell > 0) {
                    //Значит у нас есть ячейки, которые мы мыожем равномерно растягивать
                    let widthPlus = Math.floor(e_width/n_flexibleCell); //Добавочная Ширина всех колонок
                    let LastPlus = widthPlus + (e_width - (widthPlus * n_flexibleCell)); //Добавочная Ширина последней колонки

                    let i_flex = 0;
                    this.props.headerData.forEach( (item_th, key) => {
                        if(!item_th.IsHide) {
                            if(item_th.IsFixed) {

                            }
                            else {
                                if(new_columnsWidth[item_th.key].IsFlexible) {
                                    if(i_flex < n_flexibleCell - 1) {
                                        new_columnsWidth[item_th.key].Width = new_columnsWidth[item_th.key].MinWidth + widthPlus
                                    }
                                    else {
                                        new_columnsWidth[item_th.key].Width = new_columnsWidth[item_th.key].MinWidth + LastPlus
                                    }
                                    i_flex ++;
                                }
                            }
                        }
                    });
                }
                else {
                    //Если нет колонки, которая без фиксированной ширины, то мы делаем динамической последнюю колонку
                    if(n_notFixedCell == 0) {
                        this.props.headerData[this.props.headerData.length - 1].IsFixedWidth = false;
                    }
                    //В противном случае мы начинаем растягивать последню динамическую колонку
                    for( let i = this.props.headerData.length - 1; i >= 0; i -- ) {
                        let item_th = this.props.headerData[i];

                        if(!item_th.IsHide) {
                            if(!item_th.IsFixedWidth) {
                                if(item_th.IsFixed) {

                                }
                                else {
                                    new_columnsWidth[item_th.key].Width = new_columnsWidth[item_th.key].Width + Math.floor(e_width);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else {
                //Сжимаем ячейки

                //Нужно найти отношение каждого свободного к остальным
                //300 150
                if(n_flexibleCell > 0) {

                }
            }

            let styleID = 'cool_t_style-'+this.ID_Table;
            let css = '',
                style = document.getElementById(styleID);


            if(!style) {
                let
                    head = document.head || document.getElementsByTagName('head')[0];
                style = document.createElement('style');
                head.appendChild(style);

                style.setAttribute('type', 'text/css');
                style.setAttribute('id', styleID);
            }


            let visible_ths  = [];
            this.props.headerData.forEach( (item_th, key_th) => {
                if(!item_th.IsHide) {
                    visible_ths.push(item_th);
                    if(item_th.IsFixed) {
                        css += '#' + this.ID_Table + ' .cool_t_th-'+key_th+', #' + this.ID_Table + ' .cool_t_td-'+key_th+' {width:'+new_columnsWidth_fixed[item_th.key].Width+ 'px;} ';
                    }
                    else {
                        css += '#' + this.ID_Table + ' .cool_t_th-'+key_th+', #' + this.ID_Table + ' .cool_t_td-'+key_th+' {width:'+new_columnsWidth[item_th.key].Width+ 'px;} ';
                    }
                }
            });

            //Расставляем стили для смердженых колонок в шапке
            if(Object.keys(this.mergeThs_style).length > 0) {
                Object.keys(this.mergeThs_style).forEach((key_class) => {
                    let item_st = this.mergeThs_style[key_class];

                    let width = 0;

                    for(let i = item_st.start; i <= item_st.end; i++) {
                        if(visible_ths[i] != undefined) {
                            if(visible_ths[i].IsFixed) {
                                width += new_columnsWidth_fixed[visible_ths[i].key].Width;
                            }
                            else {
                                width += new_columnsWidth[visible_ths[i].key].Width;
                            }
                        }
                    }
                    css += '#' + this.ID_Table + ' .cool_t_th-'+item_st.start+'-'+item_st.end+' {width:'+width+ 'px;} ';
                });
            }

            css += '#' + this.ID_Table + ' .cool_t_gc_table { margin-left:'+(minWidth_fixed+1)+'px; width: calc(100% - '+(minWidth_fixed+1)+'px); } ';
            css += '#' + this.ID_Table + ' .cool_t_gc_table_fixed { width:'+(minWidth_fixed+1)+'px; } ';
            css += '#' + this.ID_Table + ' .cool_t_gc_tbody_rows_fixed { width:'+minWidth_fixed+'px } ';
            css += '#' + this.ID_Table + ' .cool_t_gc_thead_row_fixed { width:'+minWidth_fixed+'px } ';
            css += '#' + this.ID_Table + ' .cool_t_gc_k_div_fixed { width:'+minWidth_fixed+'px } ';



            css += '#' + this.ID_Table + ' .cool_t_gc_tbody_rows { width:'+(minWidth < areaWidth ? areaWidth : minWidth )+'px } ';
            css += '#' + this.ID_Table + ' .cool_t_gc_thead_row { width:'+(minWidth < areaWidth ? areaWidth : minWidth )+'px } ';
            css += '#' + this.ID_Table + ' .cool_t_gc_k_div { width:'+(minWidth < areaWidth ? areaWidth : minWidth )+'px } ';

            style.innerHTML = css;

            this.settings.HeightHead = this._ref_gc_thead.clientHeight;
            this.settings.HeightBody = this._ref_gc_tbody.clientHeight - 12; //12 пикселей высота горизонтального скрола

            let tmp_tr = document.createElement("div");
            tmp_tr.className = "cool_t_tr";
            this._ref_area.appendChild(tmp_tr);
            tmp_tr.style.height = this.settings.HeightTR + 'px';
            this.settings.HeightTRZooM = this.helper.GetComputeHeight(tmp_tr);
            tmp_tr.remove();

            this.setState({areaWidth: areaWidth, columnsWidth: new_columnsWidth, columnsWidth_fixed: new_columnsWidth_fixed,isPlug: !this.state.isPlug,});
        },
        UpdateTableWidth: () => {},


        //Данные без прослоек
        FullDataLoad: (Params) => {
            const answer = {
                data: Params.Data,
                recordsTotal:  Params.Data.length,
                result: 1
            };

            Params.options.scroller.countBufferVisible=Params.Data.length;
            Params.options.ajax.data={start:0, data:Params.Data, length:Params.Data.length};

            //Params.$Selector.find('.k-info').html('');

            this.core.MainGenerateContent(Params);

        },

        //Данные со всеми прослойками
        GoAjax: (Params:{isFirst: boolean, middlePX?: number, dir: 0|1, FuncSuc?: () => void }) => {
            let Length:number = (Params.isFirst) ? this.settings.scroller.FirstCountBuffer : this.settings.scroller.countBuffer;
            let LengthPX:number = Length * this.settings.HeightTRZooM;
            let Start:number = 0;
            let topPX:number = Params.middlePX - LengthPX/2;

            if(this.props.settings.Paging) {

            }
            else {
                if( Params.dir < 0 ) {
                    console.log('Вниз');
                    topPX = Params.middlePX - (LengthPX * (1 - this.settings.scroller.dirMax));

                    if(topPX + this.settings.HeightBody >= this.settings.allHeightPX - LengthPX) {
                        //Значит выгрузка вниз больше отображения вниз

                        console.error('Выщли за пределы вниз на (' + (topPX + this.settings.HeightBody) + ') - > (' + (this.settings.allHeightPX - LengthPX) + ')');
                        topPX = this.settings.allHeightPX - LengthPX;
                    }
                }
                else if( Params.dir > 0 ) {
                    topPX = Params.middlePX - (LengthPX * (this.settings.scroller.dirMax));
                }
                else {

                }

                if( topPX < 0) {
                    //Значит  мы начинаем выгрузку с самого начала
                    Params.middlePX = Params.middlePX + Math.abs(topPX);
                    topPX = 0;
                }
                else {
                    Start = Math.floor(topPX/this.settings.HeightTRZooM);
                }
            }


            let mainSettingsAjax = {
                data: {
                    start: Start,
                    length: Length,

                    isFirst: Params.isFirst,
                    isLoadTotal: 0,
                    getOnlyTotal: 0,
                }
            };

            this.helper.Merge(this.settings.ajax, mainSettingsAjax);

            this.core.Layer3Go({
                Start: Start,
                Length: Length,
                isLazy: false,
                isFirst: Params.isFirst,
                middlePX: Params.middlePX,
                isReload: false,
                FuncSuc: Params.FuncSuc,
            });
        },
        Layer3Go: (Params: {Start:number,Length?:number,isLazy?:boolean,isFirst?:boolean,middlePX?:number,isReload?:boolean,FuncSuc?:()=>void}) => {
            //$Selector, Start, Length, isLazy, isFirst, middlePX, isReload, FuncSuc
            if(this.settings.xhr) {
                if(this.settings.xhr.cancel) {
                    this.settings.xhr.cancel();
                }
            }

            if(this.settings.xhr_tfooter && Params.isFirst) {
                //Отменяем запрос получения футера, если таблица перестраивается
                if(this.settings.xhr_tfooter.cancel) {
                    this.settings.xhr_tfooter.cancel();
                }
            }

            const settings = this.settings;
            const tmp_settings = JSON.parse(JSON.stringify(settings));
            let xhr = {cancel: null};
            let xhr_tfooter = {cancel: null};


            tmp_settings.ajax.success = settings.ajax.success;
            tmp_settings.ajax.error = settings.ajax.error;
            tmp_settings.ajax.callbackData = settings.ajax.callbackData;

            const callback = () => {
                if(!Params.isLazy) {
                    if(!this.props.settings.Paging) {
                        let
                            newData = settings.Layer3Data.data.slice(this.settings.ajax.data.start, this.settings.ajax.data.start + this.settings.ajax.data.length),
                            answer = {
                                data: newData,
                                allCount: settings.Layer3Data.allCount,
                                result: 1
                            };

                        if(Params.FuncSuc) {
                            Params.FuncSuc();
                        }


                        this.core.MainGenerateContent( {answer: answer, isFirst: Params.isFirst, middlePX: Params.middlePX} );
                    }
                }
                else {
                    console.error('Ленивая выгрузка!');
                }
            };
            const InitTableData = () => {
                tmp_settings.ajax.success = (answer) => {
                    //Проверка, что метод вызвался от клиентских данных, или от сервера
                    console.log(tmp_settings);

                    //#region errors return
                    if(!tmp_settings.data && !this.props.settings.ajax_callback) {
                        try {
                            answer = JSON.parse(answer);
                        }
                        catch(ex) {
                            if(settings.ErrorFunction) {
                                settings.ErrorFunction({ErrorText:"Не удалось распарсить JSON, Ошибка сервера!"});
                            }
                            return;
                        }
                    }
                    if(answer.result != 1) {
                        console.error('Ошибка выгрузки таблицы код(' +answer.Result + ')');
                        if(settings.ErrorFunction) {
                            settings.ErrorFunction({Result: answer.Result});
                        }
                        return;
                    }
                    if(answer.allCount > 0 && answer.data.length == 0 && answer.getOnlyTotal != 1 && !answer.isLoadTotal ) {
                        console.error('Хранимая возврващает общее количество выводимых записей больще нуля, когда в JSON ничего не приходит');
                        if(settings.ErrorFunction) {
                            settings.ErrorFunction({ErrorText:"Хранимая возврващает общее количество выводимых записей больще нуля, когда в JSON ничего не приходит! Ошибка сервера!"});
                        }
                        return;
                    }
                    //#endregion

                    if(answer.getOnlyTotal != 1) {
                        for (let i = 0; i < answer.data.length; i++) {
                            settings.Layer3Data.data.push(answer.data[i]);
                        }
                        console.log('3 ПРОСЛОЙКА выгружено c ' + tmp_settings.ajax["data"]["start"] + ' по  ' + (tmp_settings.ajax["data"]["start"] + tmp_settings.ajax["data"]["length"]));
                    }

                    if(answer.isLoadTotal === undefined || answer.isLoadTotal) {
                        settings.Layer3Data.allCount = answer.allCount;
                        settings.Layer3Data.isLoadedTotal = true;
                        this.events.onScrollBody(null);
                    }
                    else if(Params.isFirst) {
                        if(!settings.Layer3Data.isLoadedTotal) {
                            settings.Layer3Data.allCount = tmp_settings.ajax.data.length + 50;
                        }
                    }
                    else {
                        if(!settings.Layer3Data.isLoadedTotal) {
                            settings.Layer3Data.allCount = tmp_settings.ajax.data.start + tmp_settings.ajax.data.length + 1000;
                        }
                    }

                    settings.Layer3Data.result = answer.result;
                    settings.xhr = null;

                    if((Params.isFirst || Params.isReload) && this.settings.PreTokenAjax.isToken) {
                        if( !settings.Layer3Data.isLoadedTotal ) {
                            const tmp_settings_for_count = JSON.parse(JSON.stringify(tmp_settings));

                            tmp_settings_for_count.ajax.data.getOnlyTotal = 1;
                            tmp_settings_for_count.ajax.success = (answer_for_count) => {
                                try {
                                    answer_for_count = JSON.parse(answer_for_count);
                                }
                                catch(ex) {
                                    if(settings.ErrorFunction) {
                                        settings.ErrorFunction({ErrorText:"Не удалось распарсить JSON, Ошибка сервера!"});
                                    }
                                    //HideBlockUI();
                                    console.error('Возникла ошибка, ' + ex["statusText"]);
                                    console.log(ex);
                                    return;
                                }

                                if(answer_for_count.result != 1) {
                                    console.error('Ошибка выгрузки таблицы код(' +answer_for_count.result + ')');
                                    //HideBlockUI();
                                    if(settings.ErrorFunction) {
                                        settings.ErrorFunction({result: answer.result});
                                    }
                                    return;
                                }

                                settings.Layer3Data.allCount = answer_for_count.recordsTotal;
                                settings.Layer3Data.isLoadedTotal = true;

                                this.events.onScrollBody(null);
                                callback();
                            };
                            tmp_settings_for_count.ajax.error = tmp_settings.ajax.error;
                            tmp_settings_for_count.ajax.callbackData = false;

                           // let xhr_for_count = $.ajax(tmp_settings_for_count.ajax);
                        }
                        else {
                            callback();
                        }

                        settings.ajax.data.isLoadTotal = false;
                        this.core.Layer3Go({
                            Start: 0,
                            Length: settings.scroller.FirstLazyCount,
                            isLazy: true
                        });
                    }
                    else {
                        callback();
                    }

                    if(Params.isReload) {
                        //Params.$Selector.trigger('reload_new.kirkinatorTable');
                    }
                };
                tmp_settings.ajax_tfooter.success = (answer) => {
                    if(answer.result != 1) {
                        console.error('Ошибка выгрузки таблицы код(' +answer.Result + ')');
                        if(settings.ErrorFunction) {
                            settings.ErrorFunction({Result: answer.Result});
                        }
                        return;
                    }
                    this.settings.is_need_recalc = true;
                    this.settings.Layer3Data_tfooter.data = answer.data;
                    this.settings.PreTokenAjax_tfooter.isToken = true;
                    this.setState({data_tfooter: answer.data});
                };
                if(tmp_settings.ajax.callbackData) {
                    //не видел, чтобы использовалось
                    let callback_data = tmp_settings.ajax.callbackData(tmp_settings.ajax.data);
                    // tmp_settings.ajax.data = $.extend(true, tmp_settings.ajax.data, callback_data);
                    this.helper.Merge(tmp_settings.ajax.data, callback_data);
                }
                //Значит у нас не серверная выгрузка, а на заданный JSON
                if(tmp_settings.data) {
                    let length_data = tmp_settings.data.length;
                    setTimeout(function () {
                        tmp_settings.ajax.success({
                            isLoadTotal: true,
                            data: tmp_settings.data,
                            result: 1,
                            draw: 1,
                            allCount: length_data,
                        })
                    }, 50);
                }
                else {
                    if(this.props.settings.ajax_callback) {
                        const dataAjax = {
                            xhr: xhr,
                            length: tmp_settings.ajax.data.length,
                            start: tmp_settings.ajax.data.start,

                            isFirst: tmp_settings.ajax.data.isFirst,
                            isLoadTotal: tmp_settings.ajax.data.isLoadTotal,
                            getOnlyTotal: tmp_settings.ajax.data.getOnlyTotal,

                            success: tmp_settings.ajax.success,
                            error: (e) => {},
                        };
                        this.props.settings.ajax_callback(dataAjax);
                    }
                    if(this.props.settings.ajax_callback_tfooter && (Params.isFirst || Params.isReload)) {
                        this.settings.PreTokenAjax_tfooter.isToken = false;
                        this.setState({data_tfooter: []});

                        const dataAjax_tfooter = {
                            xhr: xhr_tfooter,
                            length: tmp_settings.ajax.data.length, //none
                            start: tmp_settings.ajax.data.start,  //none

                            isFirst: tmp_settings.ajax.data.isFirst,  //none
                            isLoadTotal: tmp_settings.ajax.data.isLoadTotal,  //none
                            getOnlyTotal: tmp_settings.ajax.data.getOnlyTotal,  //none

                            success: tmp_settings.ajax_tfooter.success,
                            error: (e) => {},
                        };
                        this.props.settings.ajax_callback_tfooter(dataAjax_tfooter);
                    }
                }
            };


            //Первый вызов //Если не найдено в массиве
            if(settings.Layer3Data.data == null || Params.isFirst) {
                settings.Layer3Data.data = [];
                InitTableData();
            }
            else if(Params.isReload) {
                settings.Layer3Data.data = [];
                InitTableData();
            }
            //Загрузка с сервера, если не можем взять из массива
            else if(
                (Params.Start + Params.Length > settings.Layer3Data.data.length + 1 && settings.Layer3Data.allCount > settings.Layer3Data.data.length && !this.props.settings.Paging)
                ) {
                tmp_settings.ajax["data"]["start"] = settings.Layer3Data.data.length + 1;
                tmp_settings.ajax["data"]["length"] = Params.Start + Params.Length - tmp_settings.ajax["data"]["start"] + 1;
                InitTableData();
            }
            //Берем из массива
            else {
                console.log('Выгружаем имеющиеся данные');
                callback();
            }

            this.settings.xhr = xhr;
            //InitTableData();
        },

        MainGenerateContent: (Params :{answer?: any, isFirst?: boolean, middlePX?: number} ) => {
            let
                allCount = Params.answer.allCount,
                HeightTR = this.settings.HeightTRZooM,

                HEIGHT_CLOTH = allCount * HeightTR, //Высота всего табличного холста, расчитанная через общее колиечство строк на высоту строки
                HEIGHT_TABLE = Params.answer.data.length * HeightTR, //Высота отображаемой таблицы
                HEIGHT_CLOTH_tmp = HEIGHT_CLOTH; //тмп

            let StartPX = this.settings.ajax.data.start * HeightTR; //На это количество пикселей подымается таблица -1


            this._ref_gc_rows.style.top = StartPX + "px";
            this._ref_gc_k_div.style.height = HEIGHT_CLOTH + "px";
            this._ref_gc_k_div.style.minHeight = HEIGHT_CLOTH + "px";

            if(this._ref_gc_rows_fixed) {
                this._ref_gc_rows_fixed.style.top = StartPX + "px";
                this._ref_gc_k_div_fixed.style.height = HEIGHT_CLOTH + "px";
                this._ref_gc_k_div_fixed.style.minHeight = HEIGHT_CLOTH + "px";
            }

            //Чистим строки none, так как в React они сохраняются
            let $trs = this._ref_gc_rows.getElementsByClassName('cool_t_tr');
            let $trs_fixed;
            //if is fixed
            if(this._ref_gc_rows_fixed) {
                $trs_fixed = this._ref_gc_rows_fixed.getElementsByClassName('cool_t_tr')
            }

            if($trs.length > 0) {
                let n = 0;
                for (let i = this.settings.Start_NotNone; i < this.settings.End_NotNone; i++) { //noneco_tr + settings.scroller.countBufferVisible
                    if($trs[i] != undefined && n < this.settings.scroller.countBufferVisible + this.settings.Start_NotNone) {
                        $trs[i].classList.add('noneTR');
                        this.state.data[i].noneTR = true;

                        //fixed
                        if($trs_fixed && $trs_fixed[i] != undefined) {
                            $trs_fixed[i].classList.add('noneTR');
                        }
                    }
                    else break;
                    n++;
                }
            }

            this.settings.PreTokenAjax.isToken = true;
            this.settings.allHeightPX = HEIGHT_CLOTH;

            this.settings.Start = this.settings.ajax.data.start;
            this.settings.StartPX = StartPX;

            this.settings.LengthPX = HEIGHT_TABLE;
            this.settings.Length = Params.answer.data.length;

            this.settings.LengthPX_NotNone = this.settings.scroller.countBufferVisible * HeightTR;
            this.settings.Length_NotNone = this.settings.scroller.countBufferVisible;

            this.setState({
                data: Params.answer.data,
                isFirst: Params.isFirst,
                middlePX: Params.middlePX,
                isPlug: !this.state.isPlug,
            });
        },

        GoNoneLayer2: (Params: {middlePX: number, is_ajax?: boolean} ) => {

            let
                settings = this.settings,

                LengthPX_NotNone = this.settings.scroller.countBufferVisible * this.settings.HeightTRZooM,

                Start_NotNone,
                StartPX_NotNone = Math.ceil((Params.middlePX - LengthPX_NotNone/2)/this.settings.HeightTRZooM) * this.settings.HeightTRZooM,

                StartPX = this.settings.StartPX,
                Start = this.settings.Start;

            if( StartPX_NotNone < 0) {
                //Значит  мы начинаем выгрузку с самого начала
                StartPX_NotNone = 0;
                Start_NotNone = 0;
            }
            else {
                Start_NotNone = Math.ceil(StartPX_NotNone/settings.HeightTRZooM);
            }

            let
                noneco_tr = Start_NotNone - Start,
                End = Start + settings.scroller.countBuffer, //количество строк до конца таблицы
                End_NotNone = Start_NotNone + settings.scroller.countBufferVisible; //количество строк до конца таблицы NotNone


            if(noneco_tr < 0) {
                if(Math.abs(noneco_tr) > settings.scroller.EvgenPrediction) {
                    //Если вышли за условие сверху
                    console.error('Вышли за предел лишних строк('+settings.scroller.EvgenPrediction+') на ' + (settings.scroller.EvgenPrediction - Math.abs(noneco_tr)) + ', Тут будет подгрузка из 3-ей прослойки');
                }

                StartPX_NotNone = StartPX_NotNone - noneco_tr * settings.HeightTRZooM;
                noneco_tr = 0;

                End_NotNone = noneco_tr + settings.scroller.countBufferVisible;
            }
            else if( End_NotNone > End ) {
                //Если вышли за условие снизу
                End_NotNone = settings.Length;
            }

            if(End_NotNone > End - settings.scroller.EvgenPrediction) {
                console.error('Вышли за предел лишних строк('+settings.scroller.EvgenPrediction+') на ' + (End_NotNone - (End - settings.scroller.EvgenPrediction)) + ', Тут будет подгрузка из 3-ей прослойки');

                if(!settings.xhr && settings.PreTokenAjax.isToken) {
                    var
                        AllCount = settings.allHeightPX / settings.HeightTRZooM,
                        p_length = End + settings.scroller.LazyCount;

                    if(p_length > AllCount) {
                        p_length = AllCount;

                        this.core.Layer3Go({Start: 0, Length: p_length, isLazy: true });
                    }
                    else if(p_length - settings.Layer3Data.data.length >= settings.scroller.EvgenPredicationLazyMax) {
                        this.core.Layer3Go( {Start: 0, Length: p_length, isLazy: true } );
                    }
                    else {
                        console.error('Не попали в условие третьей ппрослойки!');
                    }


                }
                else {
                    console.error('Не получилось сделать ленивую выгрузку, ибо шел еще запрос');
                }
            }


            console.log('Начало отсчета видимых строк:'+Start_NotNone+'; Начало отсчета выгреженных строк:' + Start + '; noneco_tr: '+ noneco_tr);


            let $trs = this._ref_gc_rows.getElementsByClassName('cool_t_tr');
            let $trs_fixed;
            //if is fixed
            if(this._ref_gc_rows_fixed) {
                $trs_fixed = this._ref_gc_rows_fixed.getElementsByClassName('cool_t_tr')
            }



            if(!Params.is_ajax) {
                //Если мы таблицу получили не только что, и нужно ничего скрывать
                //settings.Start_NotNone Отличается от Start_NotNone. Ибо settings.Start_NotNone получается от старой выборки
                let n = 0;
                for (let i = settings.Start_NotNone; i < settings.End_NotNone; i++) { //noneco_tr + settings.scroller.countBufferVisible
                    if($trs[i] != undefined && n < this.settings.scroller.countBufferVisible) {
                        if(!$trs[i].classList.contains('noneTR')) {
                            $trs[i].classList.add('noneTR');
                            this.state.data[i].noneTR = true;
                        }

                        //fixed
                        if($trs_fixed && $trs_fixed[i] != undefined && !$trs_fixed[i].classList.contains('noneTR')) {
                            $trs_fixed[i].classList.add('noneTR');
                        }
                    }
                    else break;
                    n++;
                }


                //    console.log('Старт скрытых: ' + settings.Start_NotNone)

                //    console.log( $selector.find('.k_t tbody tr').not($selector.find('.k_t tbody tr.noneTR')));
                //    $trs.addClass('noneTR');
            }

            let n = 0;
            for (let i = noneco_tr; i < End_NotNone; i++) { //noneco_tr + settings.scroller.countBufferVisible
                if($trs[i] != undefined && n < this.settings.scroller.countBufferVisible) {
                    if($trs[i].classList.contains('noneTR')) {
                        $trs[i].classList.remove('noneTR');
                        this.state.data[i].noneTR = false;
                    }

                    //fixed
                    if($trs_fixed && $trs_fixed[i] != undefined && $trs_fixed[i].classList.contains('noneTR')) {
                        $trs_fixed[i].classList.remove('noneTR');
                    }
                }
                else break;
                n++;
            }

            Start_NotNone = noneco_tr;

            if(this.settings.StartPX_NotNone != StartPX_NotNone || Params.is_ajax) {
                this._ref_gc_rows.style.top = StartPX_NotNone + "px";
                if(this._ref_gc_rows_fixed) {
                    this._ref_gc_rows_fixed.style.top = StartPX_NotNone + "px";
                }
            }


            this.settings.StartPX_NotNone = StartPX_NotNone;
            this.settings.Start_NotNone = Start_NotNone;
            this.settings.End_NotNone = End_NotNone;
        },

        MergeCells: (none_tr, Length) => {

            let $trs = this._ref_gc_rows.getElementsByClassName('cool_t_tr');

            let
                array_cell = [],
                n = 0;

            for(let i = none_tr; i < none_tr + Length; i++ ) {
                array_cell[n] = [];

                if($trs[i] == undefined) break;

                let
                    m = 0,
                    $tds = $trs[i].getElementsByClassName('cool_t_td');

                for (let j = 0; j < $tds.length; j++) {
                    array_cell[n][m] = $tds[j];
                    let $td = array_cell[n][m];
                    if(n>0 && m>=0) {
                        if(!$td.classList.contains('not_merge')) {
                            $td.classList.add('none-td');
                        }
                    }
                    else if(m>=0) {
                        $td.classList.remove('none-td');
                    }

                    m++;
                }
                n++;
            }
        },

        DelayRecalc: (Func) => {
            clearTimeout(this.DelayRecalcTimer);
            this.DelayRecalcTimer = setTimeout(Func, 200); // Will do the ajax stuff after 1000 ms, or 1 s
        },
        MergeDelay: (Func) => {
            clearTimeout(this.DelayMergeTimer);
            this.DelayMergeTimer = setTimeout(Func, 1000); // Will do the ajax stuff after 1000 ms, or 1 s
        },
        SideDelay: (callback, ms, dir) => {
            clearTimeout(this.settings.slide.timer);
            if(callback !== false) {
                let
                    diff = this.settings.slide.dir - dir,
                    callback1 = function() {
                        callback(diff);
                    };
                this.settings.slide.dir = dir;
                this.settings.slide.timer = setTimeout(callback1, ms);
            }
        },

        Reload: (FuncSuc?) => {
            this._ref_gc_loading.classList.remove('fxs-display-none');
            this.core.Layer3Go({
                Start: 0,
                Length: 0,
                isLazy: false,
                isFirst: false,
                middlePX: 0,
                isReload: true,
                FuncSuc: () => {
                    this._ref_gc_loading.classList.add('fxs-display-none');
                    if(FuncSuc) {
                        FuncSuc();
                    }
                },
            });

        },

    };

    helper = {
        Merge: (obj1, obj2) => {
            //obj1 наследует свойства obj2, если такие есть
            const extend_rec = function (obj1, obj2) {
                Object.keys(obj1).forEach( (key) => {
                    if( obj2 && typeof obj2 === "object" && obj2[key] !== undefined ) {
                        if( obj1 && typeof obj1[key] === "object" && obj1[key] && Object.keys(obj1[key]).length > 0 ) {
                            //Значит можно еще глубже
                            extend_rec(obj1[key], obj2[key]);
                        }
                        else {
                            obj1[key] = obj2[key];
                        }
                    }
                });
            };

            extend_rec(obj1, obj2);
        },

        DelayRecalc: (Func) => {
            clearTimeout(this.DelayRecalcTimer);
            this.DelayRecalcTimer = setTimeout(Func, 200); // Will do the ajax stuff after 1000 ms, or 1 s
        },

        GetComputeHeight: (elem) => {
            let height;
            if(elem) {
                let rect = elem.getBoundingClientRect();
                if (rect.height) {
                    // `width` is available for IE9+
                    height = rect.height;
                } else {
                    // Calculate width for IE8 and below
                    height = rect.top - rect.bottom;
                }
            }
            return height;
        }
    };

    render() {
        const dataContextMenu = {
            target: this.state.targetCM,
            content: this.state.contentCM,
        };

        //<HZContextMenu {...dataContextMenu} />

        //Колонка в шапке имеет 3 состояния/ 1-конст ширина 2-динамическая ширина 3-динамическая колонка,растянутая меньше минимальной ширины
        //Растяжение фиксированной колонки просто устанавливает ей ширину, затрагивая ширину с динамических колонок, не доходя до минимальной ширины
        //Растяжение динамической колонки меньше минимума устанавливает ей ширину, затрагивая ширину с динамических колонок, не доходя до минимальной ширины
        //Растяжение динамической колонки меньше минимума, если другие динамические колонки тоже имеют ширину меньше минимума, то мы увеличиваем ширину во всех колонках


        //#region Header
        let thead_rows = [];
        let thead_rows_fixed = [];
        let dataTHs = [];
        let dataTHs_fixed = [];

        let dataTHs_visible = [];

        this.props.headerData.forEach( (item_th, key_th) => {
            if(!item_th.IsHide) {
                let SortClass = "";
                let after_content_childern = [];
                if(item_th.Sort !== undefined && !item_th.Sort.disabled) {

                    if(item_th.Sort.isActive) {
                        SortClass = item_th.Sort.dir == 0 ? "sorting_DESC" : "sorting_ASC";
                    }

                    after_content_childern.push(
                        <span key={"sort_image"} className="fxc-gc-sorting-sortImage">
                            <svg viewBox="0 0 34.761 26.892">
                                <path d="M15.359 9.478l-6.226-6.21v17.557H7.426V3.268L1.2 9.478 0 8.281 8.279 0l8.279 8.281z" className="cool_t-asc" />
                                <path d="M34.761 18.612l-8.279 8.281-8.282-8.281 1.2-1.2 6.226 6.21V6.068h1.707v17.557l6.226-6.21z" className="cool_t-desc" />
                            </svg>
                        </span>
                    )
                }
                let after_content;
                if(after_content_childern.length > 0) {
                    after_content = (
                        <div className="cool_t_th_aftercontent">
                            {after_content_childern}
                        </div>);
                }

                let class_rowspan = "";

                if(item_th.rowspan) {
                    if(item_th.rowspan == 2) {
                        class_rowspan = "rowspan-2";
                    }
                }
                dataTHs_visible.push(item_th);


                if(item_th.IsFixed) {
                    dataTHs_fixed.push(
                        <div key={key_th} className={"cool_t_th cool_t_th-"+key_th +" "+SortClass + " " + class_rowspan}>
                            <div className="cool_t_th_content" onClick={(e) => {this.events.onClickTH(e, item_th);}} >
                                <div className="fxc-gc-text">{this.props.callbackRenderTH(item_th)}</div>
                                {after_content}
                            </div>
                            <div className="cool_t_th_right_margin">
                                <div className="cool_t_th-resizing-anchor">
                                    <div onMouseDown={(e)=>{this.events.onMouseDown_splitter(e, item_th, key_th)}}  className="cool_t_th-resizing-handle">
                                        <div className="cool_t_th-resizing-handleLine azc-br-muted"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }
                else {
                    dataTHs.push(
                        <div key={key_th} className={"cool_t_th cool_t_th-"+key_th +" "+SortClass + " " + class_rowspan}>
                            <div className="cool_t_th_content" onClick={(e) => {this.events.onClickTH(e, item_th);}} >
                                <div className="fxc-gc-text">{this.props.callbackRenderTH(item_th)}</div>
                                {after_content}
                            </div>
                            <div className="cool_t_th_right_margin">
                                <div className="cool_t_th-resizing-anchor">
                                    <div onMouseDown={(e)=>{this.events.onMouseDown_splitter(e, item_th, key_th)}}  className="cool_t_th-resizing-handle">
                                        <div className="cool_t_th-resizing-handleLine azc-br-muted"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }
            }
        });

        if(this.props.headerDataTree && this.props.headerDataTree.length > 0) {
            //пока сделаю для двухмерной шапки

            //Очищаем стили древовидных таблиц
            this.mergeThs_style = {};
            for (let i = 0; i < this.props.headerDataTree.length; i++) {
                let dataTHs_tree = [];
                let dataTHs_tree_fixed = [];
                let k = 0;
                let z = 0;

                let item_tr_tree = this.props.headerDataTree[i];
                let level_row = i + 2;//Уровень строки для ровспана

                for(let j = 0; j < dataTHs_visible.length; j ++) {
                    if( dataTHs_visible[j].rowspan < level_row ) {
                        //значит не нужно вставлять пустую ячейку и вставляем, что есть

                        if(item_tr_tree[z]) {
                            let class_th = "cool_t_th-"+j;
                            if (item_tr_tree[z].colspan > 1) {

                                class_th = "cool_t_th-" + j + '-'+(j  + item_tr_tree[z].colspan - 1);
                                this.mergeThs_style[class_th] = {
                                    start: j ,
                                    end: j  + item_tr_tree[z].colspan - 1
                                };

                                j = j  + item_tr_tree[z].colspan - 1;
                            }



                            dataTHs_tree.push(<div key={k} className={"cool_t_th "+class_th}>
                                <div className={"cool_t_th_content" + ' ' +'centered'} onClick={(e) => {}} >
                                    <div className="fxc-gc-text">{item_tr_tree[z].name}</div>
                                    {""}
                                </div>
                                <div className="cool_t_th_right_margin">
                                    <div className="cool_t_th-resizing-anchor">
                                        <div onMouseDown={(e)=>{}}  className="cool_t_th-resizing-handle">
                                            <div className="cool_t_th-resizing-handleLine azc-br-muted"/>
                                        </div>
                                    </div>
                                </div>
                            </div>);
                        }

                        z ++;
                    }
                    else {

                        //вставляем пустую ячейку
                        if(dataTHs_visible[j].IsFixed) {
                            dataTHs_tree_fixed.push(<div key={k} className={"cool_t_th cool_t_th-"+ j +' ' + 'merge_space'}> </div>);
                        }
                        else {
                            dataTHs_tree.push(<div key={k} className={"cool_t_th cool_t_th-"+ j +' ' + 'merge_space'}> </div>);
                        }
                    }
                    k ++;
                }

                thead_rows.push(
                    <div key={"level-"+i} className="cool_t_gc_thead_row" >{dataTHs_tree}</div>
                );

                if(dataTHs_fixed.length > 0) {
                    thead_rows_fixed.push(<div key={"level-" +i} className="cool_t_gc_thead_row" >{dataTHs_tree_fixed}</div>);
                }
            }
        }

        thead_rows.push(<div key={"MainRow"} className="cool_t_gc_thead_row" >{dataTHs}</div>);
        if(dataTHs_fixed.length > 0) {
            thead_rows_fixed.push(<div key={"MainRow"} className="cool_t_gc_thead_row" >{dataTHs_fixed}</div>);
        }

        if(this.props.filterData) {
            let dataFTHs = [];
            let dataFTHs_fixed = [];
            this.props.filterData.forEach( (item_th, key_th) => {
                let item_th_orig = this.props.headerData[key_th];

                if(!item_th_orig.IsHide) {
                    if(item_th_orig.IsFixed) {
                        dataFTHs_fixed.push(
                            <div key={key_th} className={"cool_t_th cool_t_th-"+key_th}>
                                <div className="cool_t_th_content">
                                    {this.props.callbackRenderFilterTH(item_th)}
                                </div>
                                <div className="cool_t_th_right_margin">
                                    <div className="cool_t_th-resizing-anchor">
                                        <div className="cool_t_th-resizing-handle">
                                            <div className="cool_t_th-resizing-handleLine azc-br-muted"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    else {
                        dataFTHs.push(
                            <div key={key_th} className={"cool_t_th cool_t_th-"+key_th}>
                                <div className="cool_t_th_content">
                                    {this.props.callbackRenderFilterTH(item_th)}
                                </div>
                                <div className="cool_t_th_right_margin">
                                    <div className="cool_t_th-resizing-anchor">
                                        <div className="cool_t_th-resizing-handle">
                                            <div className="cool_t_th-resizing-handleLine azc-br-muted"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    }
                }
            });


            thead_rows.push(<div key={"FilterRow"} className="cool_t_gc_thead_row  cool_t_gc_thead_filter_row">{dataFTHs}</div>);
            if(dataTHs_fixed.length > 0) {
                thead_rows_fixed.push(<div key={"FilterRow"} className="cool_t_gc_thead_row  cool_t_gc_thead_filter_row">{dataFTHs_fixed}</div>);
            }
        }
        //#endregion

        //#region Content
        let dataTRS = [];
        let dataTRS_fixed = [];
        let dataTRS_tfooter = [];
        let dataTRS_fixed_tfooter = [];
        //

        const FuncTDS_Rend = (item_tr, key_tr, data_tds, is_tfooter) => {
            let dataTDS_fixed = [];
            let dataTDS = [];

            data_tds.forEach( (item_td, key_td) => {
                let item_th_orig = this.props.headerData[key_td];
                if(!item_th_orig.IsHide) {
                    let isMergeClass = item_td && !item_td.is_merge ? " not_merge" : "";
                    if(this.props.callbackRenderTD) {
                        let data_render_td = this.props.callbackRenderTD(item_td, item_tr, item_th_orig);
                        let JSXInsideTD;
                        let class_str = "";

                        if(typeof data_render_td == "object" && data_render_td.contentJSX != undefined) {
                            JSXInsideTD = data_render_td.contentJSX;
                            if(data_render_td.className) {
                                class_str += " " + data_render_td.className;
                            }
                        }
                        else {
                            JSXInsideTD = data_render_td;
                        }

                        let style:any = {};
                        if(item_td.css) {
                            if(item_td.css.background) {
                                style.background = item_td.css.background;
                            }
                            if(item_td.css.color) {
                                style.color = item_td.css.color;
                            }
                        }

                        if(item_th_orig.IsFixed) {
                            dataTDS_fixed.push(
                                <div key={key_td} style={style} className={"cool_t_td cool_t_td-"+key_td + isMergeClass + class_str}
                                     onClick={(e)=>{this.events.onClickTD(e,
                                         {
                                             item_tr: item_tr,
                                             item_td: item_td,
                                             index_tr: key_tr,
                                             item_th: item_th_orig,
                                             index_td: key_td,
                                         }
                                     )}}
                                     onDoubleClick={(e)=>{this.events.onDoubleClickTD(e, item_tr, item_td);}}
                                     onContextMenu={(e)=>{this.events.onRightClickTD(e, item_tr, item_td);}}>
                                    {JSXInsideTD}
                                </div>
                            );
                        }
                        else {
                            dataTDS.push(
                                <div key={key_td} style={style} className={"cool_t_td cool_t_td-"+key_td + isMergeClass + class_str}
                                     onClick={(e)=>{this.events.onClickTD(e,
                                         {
                                             item_tr: item_tr,
                                             item_td: item_td,
                                             index_tr: key_tr,
                                             item_th: item_th_orig,
                                             index_td: key_td,
                                         }
                                     )}}
                                     onDoubleClick={(e)=>{this.events.onDoubleClickTD(e, item_tr, item_td);}}
                                     onContextMenu={(e)=>{this.events.onRightClickTD(e, item_tr, item_td);}}>
                                    {JSXInsideTD}
                                </div>
                            );
                        }

                    }
                    else {
                        if(item_th_orig.IsFixed) {
                            dataTDS_fixed.push(
                                <div key={key_td} className={"cool_t_td cool_t_td-"+key_td + isMergeClass}
                                     onDoubleClick={(e)=>{this.events.onDoubleClickTD(e, item_tr, item_td);}}>
                                    <div className="cool_t_td_content"><div className="fxc-gc-text">{item_td}</div></div>
                                </div>
                            );
                        }
                        else {
                            dataTDS.push(
                                <div key={key_td} className={"cool_t_td cool_t_td-"+key_td + isMergeClass}
                                     onDoubleClick={(e)=>{this.events.onDoubleClickTD(e, item_tr, item_td);}}>
                                    <div className="cool_t_td_content"><div className="fxc-gc-text">{item_td}</div></div>
                                </div>
                            );
                        }
                    }
                }
            });

            return {
                dataTDS_fixed: dataTDS_fixed,
                dataTDS: dataTDS,
            }
        };
        const FuncTRS_Rend = (data_trs, is_tfooter) => {
            let dataTRS = [];
            let dataTRS_fixed = [];
            data_trs.forEach( (item_tr, key_tr) => {
                let dataTDS = [];
                let dataTDS_fixed = [];
                if(item_tr.tds !== undefined) {
                    let res_TDS = FuncTDS_Rend(item_tr, key_tr, item_tr.tds, is_tfooter);
                    dataTDS = res_TDS.dataTDS;
                    dataTDS_fixed = res_TDS.dataTDS_fixed;
                }
                let isSelClass = "";
                if((this.props.selectRowKey || this.props.selectRowKey == 0) && this.props.selectRowKey == item_tr.trKey) {
                    isSelClass = "cool_t_tr_selected "
                }

                let isNoneTRClass = "noneTR ";
                let onClickTr = (e)=>{this.events.onClickTr(e, item_tr)};

                if(is_tfooter) {
                    isSelClass = "";
                    onClickTr = () => {};
                }


                if(item_tr.noneTR !== undefined && item_tr.noneTR === false) {
                    isNoneTRClass = "";
                }

                let index_Class = "cool_t_tr_index-"+item_tr.trKey+" ";



                dataTRS.push(<div className={"cool_t_tr " + isSelClass + isNoneTRClass + index_Class}  key={key_tr} onClick={onClickTr} >{dataTDS}</div>);

                if(dataTDS_fixed.length > 0) {
                    dataTRS_fixed.push(<div className={"cool_t_tr " + isSelClass + isNoneTRClass + index_Class}  key={key_tr} onClick={onClickTr} >{dataTDS_fixed}</div>);
                }
            });

            return {dataTRS: dataTRS, dataTRS_fixed: dataTRS_fixed};
        };
        let res_TRS = FuncTRS_Rend(this.state.data, false);
        dataTRS = res_TRS.dataTRS;
        dataTRS_fixed = res_TRS.dataTRS_fixed;
        //#endregion

        //#region Footer
        let gc_tfooter;
        let gc_tfooter_fixed;

        if(this.props.settings.ajax_callback_tfooter) {
            if(this.settings.PreTokenAjax_tfooter.isToken) {
                let res_TRS = FuncTRS_Rend(this.state.data_tfooter, true);
                dataTRS_tfooter = res_TRS.dataTRS;
                dataTRS_fixed_tfooter = res_TRS.dataTRS_fixed;
            }
            else {
                dataTRS_tfooter = ["Loading footer...."]
            }


            gc_tfooter = (
                <div className="cool_t_gc_tfooter" ref={(ref)=>{this._ref_gc_tfooter = ref}}>
                    {dataTRS_tfooter}
                </div>
            );

            gc_tfooter_fixed = (
                <div className="cool_t_gc_tfooter" ref={(ref)=>{this._ref_gc_tfooter_fixed = ref}}>
                    {dataTRS_fixed_tfooter}
                </div>
            )
        }


        //#endregion


        let FixedTable;

        if(dataTHs_fixed.length > 0) {
            FixedTable = (
                <div className="cool_t_gc_table_fixed" ref={(ref) =>{this._ref_cool_t_gc_table_fixed = ref}}>
                    <div className="cool_t_gc_thead" ref={(ref)=>{this._ref_gc_thead_fixed = ref}}>
                        {thead_rows_fixed}
                    </div>
                    <div className="cool_t_gc_tbody" ref={(ref)=>{this._ref_gc_tbody_fixed = ref}}
                         onScroll={this.events.onScrollBodyFixed}
                         onWheel={this.events.onWheelBodyFixed}
                    >
                        <div className="cool_t_gc_tbody_rows_fixed" ref={(ref)=>{this._ref_gc_rows_fixed = ref}} >
                            {dataTRS_fixed}
                        </div>
                        <div className="cool_t_gc_k_div_fixed" ref={(ref)=>{this._ref_gc_k_div_fixed = ref}} />
                        <div className="cool_t_gc_tbody_nodata fxs-display-none">
                            <div className="cool_t_gc_tbody_nodata_message fxc-gc-text">No Data</div>
                        </div>
                    </div>
                    {gc_tfooter_fixed}
                </div>
            );
        }




        return (
            <div className={"cool_t_scroll_wrapper cool_t_border_table"} id={this.ID_Table}>
                <div className="cool_t_gc_area_wrapper" ref={(ref)=>{this._ref_area_wrapper = ref}}>
                    <div className="cool_t_gc_area"  ref={(ref)=>{this._ref_area = ref}}>
                        <div className="cool_t_gc_content">
                            {FixedTable}
                            <div className="cool_t_gc_table" ref={(ref) =>{this._ref_cool_t_gc_table = ref}}>
                                <div className="cool_t_gc_thead" ref={(ref)=>{this._ref_gc_thead = ref}}>
                                    {thead_rows}
                                </div>
                                <div className="cool_t_gc_tbody" ref={(ref)=>{this._ref_gc_tbody = ref}} onScroll={this.events.onScrollBody} >
                                    <div className="cool_t_gc_tbody_rows" ref={(ref)=>{this._ref_gc_rows = ref}} >
                                        {dataTRS}
                                    </div>
                                    <div className="cool_t_gc_k_div" ref={(ref)=>{this._ref_gc_k_div = ref}} />
                                    <div className="cool_t_gc_tbody_nodata fxs-display-none">
                                        <div className="cool_t_gc_tbody_nodata_message fxc-gc-text">No Data</div>
                                    </div>
                                </div>
                                {gc_tfooter}
                            </div>
                        </div>
                        <div className="cool_t_gc_footer"></div>
                        <div className={"cool_t_gc_loading"} ref={(ref)=>{this._ref_gc_loading = ref}}>
                           <div className="cool_t_gc_loading_co_neom">Loading...</div>
                        </div>
                    </div>
                </div>

                <div className="cool_t_info">
                    <div className="cool_t_count_rows" ref={(ref)=>{this._ref_count_rows = ref}}> </div>
                    <div className="cool_t_prev_btn">  </div>
                    <div className="cool_t_pageprefix"> </div>
                    <div className="cool_t_wr_sel_page"> </div>
                    <div className="cool_t_pagesuffix" data-bind="text: pageTextSuffix"> </div>
                    <div className="cool_t_prev_btn"> </div>
                </div>
            </div>
        );
    }
}
