/**
 * Данный модуль предназначен для вкладок, которые скролятся кнопками <- ->
 */

import * as React from "react";
import * as ReactDOM from "react-dom";



interface IHZTabsData {
    key?: any;
    onClick?: any;
    onContextMenu?: any;
    className?: string;
    Name?: string;
}
export interface IHZTabsProps {
    callbackRecalc?: (tmpFunc?: () => void ) => void;
    data?: IHZTabsData[];
    dataJSX?: JSX.Element[] | JSX.Element;
    className?: string;
    children?: JSX.Element[] | JSX.Element;
}
interface IHZTabsState {
    data_overflowing: string,
    dir: number, //Направление движения скрола
    AnimationToken: boolean,
    animationDelay: number,
    navBarTravelDistance: number,
    class_no_transition: string,
    transform: string
}
export default class HZTabs extends React.Component<IHZTabsProps, IHZTabsState> {
    static defaultProps = {
        data: []
    };
    state: Readonly<IHZTabsState> = {
        data_overflowing: "both", //Показывает, какие кнопки отображены
        dir: 1, //Направление движения скрола
        AnimationToken: false,
        animationDelay: 200,
        navBarTravelDistance: 150,
        class_no_transition: "",
        transform: "none"
    };

    private ref_ProductNav: HTMLDivElement;
    private ref_ProductNav_Contents: HTMLDivElement;


    private core = {
        hzt_Advancer_Left_click: (e) => {
            const state = this.state;

            if((state.data_overflowing == 'left' || state.data_overflowing == 'both') && state.AnimationToken) {
                this.setState({
                    dir: -1,
                    AnimationToken: false,
                    class_no_transition: '',
                    transform: 'translateX('+(state.navBarTravelDistance*(1))+'px)'
                });
            }
        },
        hzt_Advancer_Right_click: (e) => {
            const state = this.state;

            if((state.data_overflowing == 'right' || state.data_overflowing == 'both') && state.AnimationToken) {
                this.setState({
                    dir: 1,
                    AnimationToken: false,
                    class_no_transition: '',
                    transform: 'translateX('+(state.navBarTravelDistance*(-1))+'px)'
                });
            }
        },

        transitionend_func: (e) => {
            //Вызывается каждый раз, когда произойдет событие окончания анимации
            if(e.propertyName == 'transform' && e.target.classList.contains('hzt_ProductNav_Contents') ) {
                this.setState({
                    class_no_transition: 'hzt_ProductNav_Contents-no-transition',
                    transform: 'none'
                });
                let dLeft = this.ref_ProductNav.scrollLeft + (this.state.navBarTravelDistance*this.state.dir);
                this.ref_ProductNav.scrollLeft = dLeft;
                //$NavContents.removeClass('pn-ProductNav_Contents-no-transition');
                this.core.init_data_overflowing();
            }
        },
        scroll_func: (e) => {
            e.stopPropagation();
            this.core.init_data_overflowing();
        },
        mousewheel_func: (e) => {
            let isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

            if(isFirefox) {
                this.ref_ProductNav.scrollLeft += (e.deltaY*5);
            }
            else {
                this.ref_ProductNav.scrollLeft += (e.deltaY);
            }

            e.preventDefault();
        },

        init_data_overflowing: () => {
            //проверка текущего состояния для сокрытия кнопок
            const state = this.state;
            let data_overflowing;
            if(this.ref_ProductNav) {
                let
                    NavOffset = this.ref_ProductNav.getBoundingClientRect(),
                    NavContentOffset = this.ref_ProductNav_Contents.getBoundingClientRect(),
                    width = NavOffset.width,
                    width_content = NavContentOffset.width;

                //Если стоим в самом начале
                if(NavOffset.x == NavContentOffset.x) {
                    if(width_content > width) {
                        data_overflowing = "right";
                    }
                    else {
                        data_overflowing = "none";
                    }
                }
                //Если стоим в конце
                else if( (width_content - width) + (NavContentOffset.x - NavOffset.x) <= 2) {
                    data_overflowing = "left";
                }
                //По середине
                else {
                    data_overflowing = "both";
                }

                this.setState({
                    data_overflowing: data_overflowing,
                    AnimationToken: true
                });
            }
        },

    };

    constructor(props) {
        super(props);
    };



    componentDidMount = () => {
        this.ref_ProductNav.addEventListener('transitionend', this.core.transitionend_func, false);
        this.ref_ProductNav.addEventListener('scroll', this.core.scroll_func, false);
        this.ref_ProductNav.addEventListener('wheel', this.core.mousewheel_func, false);

        window.addEventListener('resize', this.core.init_data_overflowing , false);

        this.core.init_data_overflowing();
    };
    componentWillUnmount = () => {
        this.ref_ProductNav.removeEventListener('transitionend', this.core.transitionend_func, false);
        this.ref_ProductNav.removeEventListener('scroll', this.core.scroll_func, false);
        this.ref_ProductNav.removeEventListener('wheel', this.core.mousewheel_func, false);

        window.removeEventListener('resize', this.core.init_data_overflowing , false);

    };

    componentDidUpdate = (prevProps, prevState) => {
        //Если нам пришел калбек для рекалькуляции
        if(this.props.callbackRecalc) {
            let _this = this;
            this.props.callbackRecalc(()=>{_this.core.init_data_overflowing();})
        }
    };

    //recalc






    render() {
        const state = this.state;
        let tabs_JSX;

        if(this.props.dataJSX) {
            tabs_JSX = this.props.dataJSX;
        }
        else {
            tabs_JSX = [];
            this.props.data.forEach( (item) => {
                tabs_JSX.push(
                    <button
                        onClick={item.onClick}
                        onContextMenu={item.onContextMenu}
                        type="button"
                        key={item.key}
                        className={"hzt_ProductNav_Link " + (item.className ? item.className : "")}>
                        {item.Name}
                    </button>
                )
            });
        }


        let ClassNameNav = this.props.className ? this.props.className : "";

        return (
            <div key={"hzt_ProductNav_Wrapper " + ClassNameNav} className={"hzt_ProductNav_Wrapper " + ClassNameNav}>
                <div className={"hzt_ProductNav data-overflowing_" + state.data_overflowing}
                     ref={(ref) => { this.ref_ProductNav = ref; }}>
                    <div className={"hzt_ProductNav_Contents " + state.class_no_transition}
                         style={{"transform": state.transform}}
                         ref={(ref) => { this.ref_ProductNav_Contents = ref; }}>
                        {tabs_JSX}
                    </div>
                </div>
                <button onClick={this.core.hzt_Advancer_Left_click} className="hzt_Advancer_Left hzt_Advancer">
                    <svg className="hzt_Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                        <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"/>
                    </svg>
                </button>
                <button onClick={this.core.hzt_Advancer_Right_click} className="hzt_Advancer_Right hzt_Advancer">
                    <svg className="hzt_Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
                        <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"/>
                    </svg>
                </button>
            </div>
        )
    }
}