import axios from 'axios';
import qs from 'qs';

export const login = (login:string, password:string) => {
	return axios.post('/login/login', qs.stringify({ login: login, pwd: password }), {
		headers: { 'content-type': 'application/x-www-form-urlencoded' }
	});
}
export const forgotPasswordEmail = (login:string) => {
	return axios.post('/login/forgotPassword', qs.stringify({ login: login }), {
		headers: { 'content-type': 'application/x-www-form-urlencoded' }
	});
}
