import axios from 'axios';
import qs from 'qs';

export const getMainInfo = () => {
    return axios.get(
        '/Index/getMainInfo',{ params: {}}
    );
};