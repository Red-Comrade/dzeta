import axios from 'axios';
import qs from 'qs';

export const getTree = () => {
    return axios.post('/Type/getTree', qs.stringify({ }), {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};

export const getChildren = (uid:string, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;
    return axios.get(
        '/Type/getChildren',{
            params: {uid: uid},
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        },
    );
};

export const addNode = (name:string, parent:number, ) => {
    return axios.post('/Type/add',
        qs.stringify({
            name: name,
            parent: parent,
            isSystem: null,
            base: null
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const renameNode = (uid:string, name) => {
    return axios.post('/Type/update',
        qs.stringify({
            uid: uid,
            name: name,
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const deleteNode = (type:string, type_parent: string ) => {
    return axios.post('/Type/delete',
        qs.stringify({
            type: type,
            type_parent: type_parent,
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};


export const addLink = (type:string, type_parent: string ) => {
    return axios.post('/Type/addLink',
        qs.stringify({
            type: type,
            type_parent: type_parent,
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const deleteAllObjs = (type:string) => {
    return axios.post('/Type/deleteAllObjs',
        qs.stringify({
            type: type,
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const deleteAllObjsCount = (type:string) => {
    return axios.post('/Type/deleteAllObjsCount',
        qs.stringify({
            type: type,
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};



export const addGroup = (parent:string, name:string ) => {
    return axios.post('/AttrGroup/add',
        qs.stringify({
            parent: parent,
            name: name
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const updateGroup = (uid:string, name:string ) => {
    return axios.post('/AttrGroup/update',
        qs.stringify({
            uid: uid,
            name: name
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const deleteGroup = (uid:string) => {
    return axios.post('/AttrGroup/delete',
        qs.stringify({
            uid: uid,
        }),
        {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};

export const getAttributesOfGroup = (type:string, group: string) => {
    return axios.get(
        '/AttrGroup/getAttributes',{ params: {type: type, group: group }}
    );
};


export interface IAttrDataAdd {
    name:string,
    datatype: string,
    type_parent: string,
    group: string,
    array: number,
    unique: number,
    require: number,
    TextType: number,
    calc: string,
    type_link: string,
    calc_triggers: { manual: string },
}
export const addAttr = ( params: IAttrDataAdd ) => {
    params.calc = params.calc ? params.calc : null;
    params.calc_triggers.manual = params.calc_triggers.manual ? params.calc_triggers.manual : null;

    let tmpParam = params;
    //нужно, чтобы булевские значения понимались на сервере
    //tmpParam.array = Number(params.array);
    //tmpParam.unique = Number(params.unique);

    return axios.post('/Attr/add',
        qs.stringify(params),
        {
            headers: {'content-type': 'application/x-www-form-urlencoded'}
        });
};
export interface IAttrDataUpdate {
    attr: string,
    name: string,
    type_parent: string,
    array: number,
    unique: number,
    require: number,
    TextType: number,
    calc: string,
    type_link: string,
    calc_triggers: { manual: string },
}
export const updateAttr = ( params: IAttrDataUpdate ) => {
    params.calc = params.calc ? params.calc : null;
    params.calc_triggers.manual = params.calc_triggers.manual ? params.calc_triggers.manual : null;

    return axios.post('/Attr/update',
        qs.stringify(params),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
export const deleteAttr = (attr:string, type:string) => {
    return axios.post('/Attr/delete',
        qs.stringify({
            attr: attr,
            type: type,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};


export interface ISortParams {
    ID: string,
    Index: number,
}
export const sortAttr = (type:string, params:ISortParams[]) => {
    return axios.post('/Attr/sort',
        qs.stringify({
            type: type,
            params: params,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};


export const getCalcs = (uid: string) => {
    return axios.get(
        '/Type/getCalcs',{ params: {uid: uid }}
    );
};
