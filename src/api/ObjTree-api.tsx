import axios from 'axios';
import qs from 'qs';

export const getChildrenByType = (parent:string, type:string) => {
    return axios.get(
        '/Object/getChildrenByType',{ params: {
            parent: parent,
                type: type,
            }}
    );
};

export const getChildAllObj = (uid:string, uidClass:string) => {
    return axios.get(
        '/Object/getChildAllObj',{ params: {
            uid: uid,
            uidClass: uidClass,
        }}
    );
};
export const getChildObjValues = (uid:string, uidClass:string) => {
    return axios.get(
        '/dop/getChildObjValues',{ params: {
            obj: uid,
            type: uidClass,
        }}
    );
};

export const getAllChildObjValues = (uid:string) => {
  return axios.get('/Object/getAllChildObjValues?obj='+uid, {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};

export const stepFormEniTwo = (data:object ) => {
    return axios.post('/dop/stepFormEniTwo',
        qs.stringify(data),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};

export const getConfigEni = () => {
  return axios.get('/dop/getConfigEni', {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};

export const getFullChildObjValues = (uid:string[]) => {
  return axios.post('/dop/getAllChildObjValues',
      qs.stringify({
          obj: uid,
      }),
      {
          headers: { 'content-type': 'application/x-www-form-urlencoded' }
      });
};


export interface IValue {
    attr: string,
    key: number,
    value: string,
    datatype: string
}
export const addObject = (type:string, parent:string, values: IValue[] ) => {
    return axios.post('/Object/add',
        qs.stringify({
            type: type,
            parent: parent,
            values: values,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
export const registration = (type:string, parent:string, password: string, values: IValue[] ) => {
    return axios.post('/Login/registration',
        qs.stringify({
            type: type,
            parent: parent,
            values: values,
            password: password
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
export const saveObject = (uid:string, type:string, parent:string, values: IValue[], skipUniqueCheck:boolean = false) => {
    return axios.post('/Object/update',
        qs.stringify({
            obj: uid,
            type: type,
            obj_parent: parent,
            values: values,
            skip_unique_check: skipUniqueCheck ? 1 : 0
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
export const delObject = (uid:string) => {
    return axios.post('/Object/delete',
        qs.stringify({
            uid: uid,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
export const getDeleteCount = (uid:string) => {
    return axios.post('/Object/getDeleteCount',
        qs.stringify({
            uid: uid,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
export const getInfObject = (uid:string) => {
    return axios.post('/Object/getInfObject',
        qs.stringify({
            uid: uid,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
export const changeLinkObj = (uidObj:string, uidOldObj:string, uidNewObj: string) => {
    return axios.post('/Object/changeLinkObj',
        qs.stringify({
            uidObj: uidObj,
            uidOldObj: uidOldObj,
            uidNewObj: uidNewObj,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};


export interface IValueFile {
    uid: string, //uid-параметра
    new: number, //0/1
    key: number, //0/1
    value: number, //как бы число, но несовсем
    del: number,
}
export const addFile = (uid:string, type:string, parent:string, Files:any[], values: IValueFile[], onUploadProgress) => {
    let formData = new FormData();

    for( let key_file in Files) {
        formData.append("file"+key_file, Files[key_file]);
    }
    formData.append('uid', uid);
    formData.append('type', type);
    //formData.append('parent', parent);
    formData.append('values', JSON.stringify(values));

    return axios.post('/Object/addFile',
        formData,
        {
            //headers: { 'content-type': 'application/x-www-form-urlencoded' },
            headers: { 'Content-Type': 'multipart/form-data' },
            onUploadProgress: function(progressEvent) {

                if(onUploadProgress)
                    onUploadProgress(progressEvent);

                var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                console.log(percentCompleted)
            }
        });
};


export const changePassword = (uid:string) => {
    return axios.post('/User/changePassword',
        qs.stringify({
            uid: uid,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};



export const getObjsByTypeForFilter = (param:{type:string, filter: string}, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get(
        '/Object/getObjsByTypeForFilter',
        {
            params: param,
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        },

    );
};

export const runCalcList = (param:{obj:string, type:string, values: any, calc: string}, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }

    const CancelToken = axios.CancelToken;

    return axios({
        method: 'post',
        url: '/Calc/runCalcList',
        data: qs.stringify(param),
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            if(cancel_obj) {
                cancel_obj.cancel = c;
            }
        }),
        headers: {'content-type': 'application/x-www-form-urlencoded'}
    });
};

export const getAllValues = (obj:string, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get(
        '/Object/getAllValues',{ params: {
                obj: obj,
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        }
    );
};
export const getAllValuesMany = (objs:[], attrs:string[]) => {
    return axios.get(
        '/Object/getAllValues',{ params: {
                objs: objs,
                attrs:attrs,
            }}
    );
};
export const CalcGetResult = (uidObj:string, uidClass:string, uidCalc:string ) => {
    return axios.get(
        '/calc/getResult',{ params: {
                uidObj: uidObj,
                uidClass: uidClass,
                uidCalc: uidCalc
            }}
    );
};

export const stepFormGetCoefUser=(organization:string, scenario:string, persone:number, date:string)=>{
  return axios.get(
      '/dop/stepFormGetCoefUser',{ params: {
            organization: organization,
            scenario: scenario,
            persone: persone,
            date:date,
      }}
  );
}

export const stepFormGetCoefsUser = (data:any) => {
    return axios.post('/dop/stepFormGetCoefsUser',
        qs.stringify({data:data}),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};



export const getNameObjects = (Objs:[]) => {
    return axios.post('/Object/getNameObjects',
        qs.stringify({
            Objs: Objs,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};

export const removeObjs = (Objs:[]) => {
    return axios.post('/Object/removeObjs',
        qs.stringify({
            Objs: Objs,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};

export const getValueForParamObj = (param:{obj:string, param: string}, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get(
        '/Object/getValueForParamObj',
        {
            params: param,
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        },

    );
};

//#region permission

export const getModulesSettGroup = (param:{group:string}) => {
    return axios.get(
        '/permission/getModulesSettGroup',{ params: {group:param.group}}
    );
};
export const addPermissionGroup = (param:{group:string, modules: {module: string, allow: 0 | 1 }[] }) => {
    return axios.post('/permission/addPermissionGroup',
        qs.stringify({
            group: param.group,
            modules: param.modules,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};

//#endregion

//#region Triggers Calc

export const getRunTrigger = (param:{calc:string, trigger:"manual", obj:string, type:string, values:IValue[] }) => {
    return axios.post('/calc/getRunTrigger',
        qs.stringify(param),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};
//#endregion


export const getKeysValues = (obj:string, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get(
        '/Object/getKeysValues',{ params: {
                obj: obj,
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        }
    );
};

export const copyObject = (data:{obj_parent:string, objs_copy: {obj:string, attrs:{attr:string,datatype:string,value:string}[]}[], copyChildren:number}, cancel_obj?:{cancel:any}) => {
    const CancelToken = axios.CancelToken;

    return axios({
        method: 'post',
        url: '/Object/copy',
        data: qs.stringify(data),
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            if(cancel_obj) {
                cancel_obj.cancel = c;
            }
        }),
        headers: {'content-type': 'application/x-www-form-urlencoded'}
    });
};

