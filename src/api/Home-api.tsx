import axios from 'axios';
import qs from 'qs';

//TODO::В будещем тут будет явно другой метод
export const getTree = () => {
    return axios.post('/Application/getTree', qs.stringify({ }), {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};

export const getHomeApp = () => {
    return axios.post('/Application/getHomeApp', qs.stringify({ }), {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
