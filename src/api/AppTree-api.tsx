import axios from 'axios';
import qs from 'qs';

export const getTree = (isGetPermiss, GroupID, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get('/Application/getTree', {
        params: { isGetPermiss: Number(isGetPermiss), GroupID: GroupID },
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel_obj.cancel = c;
        }),
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const getParentToRoot = (uid) => {
    return axios.get('/view/getParentToRoot?view='+uid, {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};

export const getAttributesForApp = (iddb:string) => {
    return axios.post(
        '/Application/getAttributeView',
        qs.stringify({uid: iddb}), {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        }
      );
};

export const getFullAttrForApp = (isGetPermiss?) => {
    return axios.get(
        '/Attr/getAttributeAll',
        {params: {isGetPermiss: Number(isGetPermiss) }}
    );
};

export const addApp= (name:string, parentuid:string, folder:string)=>{
  return axios.post('/Application/add',
      qs.stringify({
          app_name: name,
          app_parent: parentuid,
          folder:folder,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};
export const addView= (name:string, uidApp:string, settings:object, uidView:string)=>{
  console.log(settings);
  return axios.post('/View/add',
      qs.stringify({
          viewName: name,
          app:uidApp,
          viewSettings:settings,//{}
          view: uidView,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};
export const addFolder= (name:string, settings:object)=>{
  return axios.post('/Folder/add',
      qs.stringify({
          name: name,
          settings:settings,//{}
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};

export const editApp= (uid:string, name:string)=>{
  return axios.post('/Application/rename',
      qs.stringify({
          app: uid,
          app_name: name,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};
export const editView= (uid:string, name:string, settings:object)=>{
  return axios.post('/View/edit',
      qs.stringify({
          view: uid,
          viewName:name,
          viewSettings:settings,//{}
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};
export const editFolder= (uid:string, name:string)=>{
  return axios.post('/Folder/rename',
      qs.stringify({
          uid: uid,
          name: name,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};
export const orderFolders= (folders:{uid:string, index:number}[])=>{
  return axios.post('/Folder/updateOrder',
      qs.stringify({
          folders: folders,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};
export const orderApp= (folderuid:string, apps:{uid:string, index:number}[])=>{
  return axios.post('/Folder/updateAppsOrder',
      qs.stringify({
          folder:folderuid,
          apps: apps,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};

export const addAppToFolder= (folder:string, app:string)=>{
  return axios.post('/Folder/addApp',
      qs.stringify({
          folder: folder,
          app:app,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
};
export const saveView = (data:any)=>{
  return axios.post('/view/save',
      qs.stringify(data),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}
export const editSettings= (data:any)=>{
  return axios.post('/Application/editSettings',
      data,
      {  headers: { 'Content-Type': 'multipart/form-data' }
  });
}

export const getView=(uid:string)=>{
  return axios.get('/view/getView?view='+uid, {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });

}





export const removeView=(uid:string)=>{
  return axios.post('/View/remove',
      qs.stringify({
          view: uid,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}
export const removeApp=(uid:string)=>{
  return axios.post('/Application/remove',
      qs.stringify({
          app: uid,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}
export const removeFolder=(uid:string)=>{
  return axios.post('/Folder/delete',
      qs.stringify({
          uid: uid,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}

export const getViewClass =(uid:string)=>{
  return axios.post('/Application/getViewClass',
      qs.stringify({
          uid: uid,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}
export const addViewClass=(uidView:string, uidApp:string, classesLink:object[])=>{
  return axios.post('/Application/addViewClass',
      qs.stringify({
          uid: uidView,
          uidApp:uidApp,
          classesLink:classesLink,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}

export const attrAdd=(uidView:string, attr:object[])=>{
  return axios.post('/Application/attrAdd',
      qs.stringify({
          uid: uidView,
          attr:attr,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}
export const sortAttrView=(uidView:string, orderParams:string[])=>{
  return axios.post('/Application/addViewAttrOrder',
      qs.stringify({
          uid: uidView,
          orderParams:orderParams,
      }),
      {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}

export const getInfoView  = (view_uid) => {
    return axios.get('/view/getInfoView?view='+view_uid, {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};
export const getSettings  = (app_uid) => {
    return axios.get('/Application/getSettings?app='+app_uid, {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
};



export const addAccessForApp=(params:{App:string, Group:string, Settings: {View:string, Add: number, EditDel: number, Read: number }[] })=>{
    return axios.post('/Application/addAccessForApp',
        qs.stringify({
            App: params.App,
            Group: params.Group,
            Settings: params.Settings,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
}
