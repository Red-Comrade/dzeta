import axios from 'axios';
import qs from 'qs';
import {IValue} from "./ObjTree-api";

export interface ITableData {
    isNewMode?: number,
    view: string, //uuid, —айди представления
    ParentObj?: string, //uuid, —айди представления
    attr_sort: string, // uuid,  —айди атрибута для сортировки , можно передать NULL
    sort: number,
    start: number, //  — номер первой записи в блоке
    num: number, // — количество записей в блоке
    NumOfObjOfMainClass?: number, // — количество записей в блоке
    ParentView?: string,
    SelectObjs?: string[],
    attr_filter?: {"op": string, "v": any, "attr"?: string }[],
    header_data?: any,

    view_sorts?: any,
    block_calcs?: any,
    exe_data_source_method?: string,
    HatID?: number,
}
/*TODO тут скоро будет приходит name и uid и возможно еще настройки*/
export const getTabsData = (App:string, View:string) => {
    View = View ? View : null;
    return axios.get(
        '/Application/getViews',
        { params: {app:App, view: View}}
    );
};
export interface IHeadData {
    uid: string,
    attr_filter?: {"op": string, "v": any, "attr"?: string }[],
    ParentView?: string,
    SelectObjs?: string[],
    ParentObj?: string,
    isNewMode?: number,
    ForView?: number
}
export const getHeadData = (params: IHeadData) => {
    return axios.post(
        '/Application/getAttributeView',
        qs.stringify(params), {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        }
    );
};
export const getTableData = (params: ITableData, cancel_obj?:{cancel:any}) => {
    if (!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    console.log(params);

    return axios({
        method: 'post',
        url: '/View/getTableData',
        data: qs.stringify(params),
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel_obj.cancel = c;
        }),
        headers: {'content-type': 'application/x-www-form-urlencoded'}
    });
};
export const getFooterTableData = (params: ITableData, cancel_obj?:{cancel:any}) => {
    if (!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    console.log(params);

    return axios({
        method: 'post',
        url: '/View/getFooterTableData',
        data: qs.stringify(params),
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel_obj.cancel = c;
        }),
        headers: {'content-type': 'application/x-www-form-urlencoded'}
    });
};
export const getParrentViews = (params: {view:string}, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;
    return axios.get(
        '/View/getParrentViews',
        {
            params: params,
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        }
    );
};

export const saveValueCellTable = (params:{obj:string, type:string, datatype:string, attr:string, value:string}) => {
    return axios.post('/object/updateTmpTest',
        qs.stringify({
            obj: params.obj,
            type: params.type,
            datatype: params.datatype,
            attr: params.attr,
            value: params.value,
        }),
        {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        });
};

export const exportView = (params: ITableData) => {
    return axios({
        method: 'post',
        url: '/View/export',
        data: qs.stringify(params),
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        responseType: 'arraybuffer',
    });
};

export const getDataGraph= (uid:string)=>{
  return axios.get('/view/getDataGraph', {
      params: { view: uid},
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
  });
}

export interface IWindowData {
    is_add: number,
    view: string,
    app: string,
    obj: string,
    type?: string,
}
export const getWindowData = (data:IWindowData, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios({
        method: 'post',
        url: '/View/getWindowData',
        data: qs.stringify(data),
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel_obj.cancel = c;
        }),
        headers: {'content-type': 'application/x-www-form-urlencoded'}
    });
};


export const getAppInfo = (AppUid:string, cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get(
        '/Application/getInfoApp',
        {
            params: {app: AppUid},
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        }
    );
};






export const getConfigEnemat = (cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get(
        '/EView/getConfig',
        {
            params: {},
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        }
    );
};

export const getTableDataEnemat = (params: ITableData, cancel_obj?:{cancel:any}) => {
    if (!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    console.log(params);

    return axios({
        method: 'post',
        url: '/EView/getTableData',
        data: qs.stringify(params),
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel_obj.cancel = c;
        }),
        headers: {'content-type': 'application/x-www-form-urlencoded'}
    });
};


export const removeODTEnemat = (odt:string, action: "archive" | "delete" | "unarchive", cancel_obj?:{cancel:any}) => {
    if(!cancel_obj) {
        cancel_obj = {cancel: null};
    }
    const CancelToken = axios.CancelToken;

    return axios.get(
        '/EView/removeODT',
        {
            params: {odt: odt, action: action},
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel_obj.cancel = c;
            })
        }
    );
};


//EView/removeODT?
