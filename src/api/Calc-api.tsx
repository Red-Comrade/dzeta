import axios from 'axios';
import qs from 'qs';

export const RunCalc = (formula:string, calcUid:string, classUid:string) => {
	return axios.post(
		'/calc/runCalc',
		qs.stringify({ formula: formula, calc: calcUid, class: classUid }),
		{ headers: { 'content-type': 'application/x-www-form-urlencoded' } }
	);
}

export const GetCalc = (uid:string) => {
	return axios.get(
		'/calc/getCalc',
		{ params: {uid: uid} },
		{ headers: { 'content-type': 'application/x-www-form-urlencoded' } }
	);
}

export const SaveCalc = (formula:string, classUid:string, uid:string, name:string) => {
	return axios.post(
		'/calc/save',
		qs.stringify({ formula: formula, class: classUid, calc: uid, calcName: name }),
		{ headers: { 'content-type': 'application/x-www-form-urlencoded' } }
	);
}

export const DeleteCalc = (uid:string) => {
	return axios.post(
		'/calc/delete',
		qs.stringify({ uid: uid }),
		{ headers: { 'content-type': 'application/x-www-form-urlencoded' } }
	);
}
