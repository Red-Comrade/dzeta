import axios from 'axios';
import qs from 'qs';

export const getProgress = (uid:string) => {
    return axios.get(
        '/Progress/get', {
        	params: {
            	uid: uid,
        	}
    	}
	);
};