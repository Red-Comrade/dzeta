import axios from 'axios';
import qs from 'qs';

export const InitStepSettings = () => {
	return axios.post('/dop/getSettingMetka', qs.stringify({ }), {
		headers: { 'content-type': 'application/x-www-form-urlencoded' }
	});
}

export const SaveStepSettings = (json:string) => {
	return axios.post('/dop/SaveSettingsStepForm', qs.stringify({json:json}), {
		headers: { 'content-type': 'application/x-www-form-urlencoded' }
	});
}

export const signDoc = (id:string, type:string, file:any, onUploadProgress?) => {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('id', id);
    formData.append('type', type);

    if(onUploadProgress) {
        return axios({
            method: 'post',
            url: '/dop/sign',
            data: formData,
            headers: {'content-type': 'application/x-www-form-urlencoded'},
        });
    }
    else {
        return axios.post('/dop/sign',
            formData,
            {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                onUploadProgress: function(progressEvent) {
                    if(onUploadProgress)
                        onUploadProgress(progressEvent);
                    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    console.log(percentCompleted)
                }
            });
    }
};

export const verifyTaxes = (uid:string, file:object, settings:object[]) => {
    const formData = new FormData();
    formData.append('file', file);
    for (let i = 0; i < settings.length; i++) {
        for (let key in settings[i]) {
            formData.append('settings[' + i + '][' + key + ']', settings[i][key] ? settings[i][key] : '');
        }
    }
    formData.append('uid', uid);
    return axios({
        method: 'post',
        url: '/dop/verifyTaxes',
        data: formData,
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        responseType: 'arraybuffer',
    });
};

export const getTypeOfPersone = () =>{
	return axios({
			method: 'get',
			url: 'dop/stepFormgetDefBenif',
			headers: {'content-type': 'application/x-www-form-urlencoded'},
	});
}

export const getQualification = (siret:string, date:string)=>{
		return axios.get('/dop/getQualification', {
        params: { siret: siret, date: date },
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    });
}

export const stepFormGetInfoPoSiren = (valueSirn:string)=>{
		return axios.get(
            '/dop/stepFormGetInfoPoSiren?siren='+valueSirn, {
                headers: { 'content-type': 'application/x-www-form-urlencoded' }
            }
        );
};

export const getOperCalcData = (calc:string, data:any) => {
	return axios.post('/calc/runCalcDataRequest', qs.stringify({calc:calc, data:data}), {
		headers: { 'content-type': 'application/x-www-form-urlencoded' }
	});
}
