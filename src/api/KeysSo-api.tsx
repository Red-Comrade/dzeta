import axios from "axios";
import qs from 'qs';

export const tops = (domain: string) => {
    return axios.post(
        '/KeysSo/tops',
        qs.stringify({domain: domain}), {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        }
    );
};

export const query = (domain: string) => {
    return axios.post(
        '/KeysSo/query',
        qs.stringify({domain: domain}), {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        }
    );
};

export const fill = (block: number) => {
    return axios.post(
        '/KeysSo/fill',
        qs.stringify({block: block}), {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        }
    );
};