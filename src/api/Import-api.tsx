import axios from 'axios';
import qs from 'qs';
import i18n from '../i18n';

export const getExcelData = (file:object) => {
	const formData = new FormData();
	formData.append('file', file);
	return axios.post('/import/getExcelData', formData)
		.then((response) => {
			return response.data;
		});
}

export const runImport = (uid:string, file:object, _class:string, settings:object[], parent:string, rewrite:number) => {
	const formData = new FormData();
	formData.append('file', file);
	for (let i = 0; i < settings.length; i++) {
		for (let key in settings[i]) {
			formData.append('settings[' + i + '][' + key + ']', settings[i][key] ? settings[i][key] : '');
		}
	}
	formData.append('parent', parent ? parent : null);
	formData.append('class', _class ? _class : null);
	formData.append('uid', uid);
	formData.append('rewrite', rewrite);
	return axios.post('/import/run', formData)
		.then((response) => {
			return response.data;
		});
}