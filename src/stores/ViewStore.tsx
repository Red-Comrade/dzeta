import {observable, action, ObservableMap} from 'mobx';
import * as React from "react";
import ICoolMarketEl from "../components/CoolMarketFilter/ICoolMarketEl";

export interface IDataMarketFilter {
    TableID: string,
    onClickSubmitBtn?: (e) => void,
    onClickResetBtn?: (e) => void,
    Elements: ICoolMarketEl[],
}

class ViewStore  {
    @observable isMainTableOpen:boolean = false;


    //Активный ID-холста с таблицой
    @observable ActiveClothID:string = null;
    //MarketFilter
    @observable isLoadMarketFilter: boolean = false;
    @observable DataMarketFilter:IDataMarketFilter = {
        TableID: null,
        onClickSubmitBtn: null,
        onClickResetBtn: null,
        Elements: [],
    };
}

export default new ViewStore();