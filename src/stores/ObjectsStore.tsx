import {observable, action, ObservableMap} from 'mobx';
import * as React from "react";
import INodeTemplate from '../containers/ObjectsPage/Interfaces/INodeTemplate'

class ObjectsStore  {
    @observable JSXTree: JSX.Element = null; //Дерево на странице
    @observable isLoadTree: boolean = true; //Указывает, что дерево еще загружается





    //Все, что внизу скоро почистится



    @observable treeData:object[] = [];
    @observable SelectNode?:object = null; //Выбранный узел в дереве (ссылка)

    @action UpdNameNode(id_node, newName) {
        let newTreeData = this.treeData;
        const loopFindAndCreateNode = (children) => {
            children.forEach((item) => {
                if(item.id == id_node) {
                    item.Name = newName;
                    return true;
                }
                else if(item.children) {
                    let isEx = loopFindAndCreateNode(item.children);
                    if(isEx) {
                        return true;
                    }
                }
            });
            return false;
        };
        loopFindAndCreateNode(newTreeData);
        this.treeData = newTreeData.slice();
    }
    @action UpdTreeDataAction(newTreeData:object[]) {
        this.treeData = newTreeData;
    }

    @action AddChildrenInNode(payload:{iddb:string}, _children) {
        let newTreeData = this.treeData;
        const loopFindAndCreateNode = (children) => {
            children.forEach((item) => {
                if(item.iddb == payload.iddb) {
                    item.children = _children;
                    item.isLoaded = true;
                }
                else if(item.children) {
                    loopFindAndCreateNode(item.children);
                }
            });
        };
        loopFindAndCreateNode(newTreeData);


        this.treeData = newTreeData.slice();
        //this.toogle_change = !this.toogle_change;
    }
    @action AddNode( PanentNode: INodeTemplate, ChildNode: INodeTemplate) {
        if(PanentNode.isLoaded) {
            const loopFindAndCreateNode = (children) => {
                children.forEach((item) => {
                    if(item.children) {
                        loopFindAndCreateNode(item.children);
                    }
                    if(item.iddb == PanentNode.iddb) {
                        if(!Array.isArray( item.children )) {
                            item.children = [ChildNode]
                        }
                        else {
                            //Тут можно добавить с учетом сортировки
                            item.children.push(ChildNode);
                            this.h_sort(item.children, "Name", 1);
                        }
                    }
                });
            };
            loopFindAndCreateNode(this.treeData);
        }
    }

    @action DeleteNode(payload: { iddb: string, }) {

        let NewTreeData = [];
        const loopFindAndCreateNode = (children) => {
            let NewChildren = [];
            let isRemove = false;
            children.forEach((item) => {
                if(item.children) {
                    item.children = loopFindAndCreateNode(item.children);
                }
                if(item.iddb != payload.iddb) {
                    NewChildren.push(item);
                }
                else {
                    isRemove = true;
                }
            });
            return NewChildren.length > 0 || !isRemove ? NewChildren : null;
        };
        NewTreeData = loopFindAndCreateNode(this.treeData);
        this.treeData = NewTreeData;
    }

    @action CutNodeAction(payload: {Node: any, ToNode: any}) {

        let ParentID = payload.Node.ParentID;
        let iddb = payload.Node.iddb;
        let NewTreeData = [];
        const loopFindAndCutNode = (children, ParentID_tmp) => {
            let NewChildren = [];
            children.forEach((item) => {
                if(item.children) {
                    item.children = loopFindAndCutNode(item.children, item.iddb);
                }
                if(ParentID_tmp != ParentID || item.iddb != iddb) {
                    NewChildren.push(item);
                }
            });
            return NewChildren;
        };
        NewTreeData = loopFindAndCutNode(this.treeData, null);


        //Переделываем ID node под наши ID
        payload.Node.ParentIDTree = payload.ToNode.id;
        payload.Node.id = payload.ToNode.id + '*' + payload.Node.ObjID;
        payload.Node.FolderID = payload.ToNode.FolderID;
        //Param.TypeID = parrentNode.TypeID;
        payload.Node.ParentID = payload.ToNode.iddb;
        //Param.ObjID = Param.ObjID;
        payload.Node.ParrentTypeID = payload.ToNode.TypeID;
        payload.Node.ParrentFolderID = payload.ToNode.ParrentFolderID;
        payload.Node.ParrentObjID = payload.ToNode.ObjID;

        if(payload.ToNode.children !== null) {
            payload.ToNode.children.unshift(payload.Node);

            if(payload.ToNode.children.length > 0) {
            }
        }
        else {
            payload.ToNode.children = [payload.Node];
        }
        this.treeData = NewTreeData;
    }


    h_sort = (data, key, dir) => {
        if(dir === 1) {
            data.sort(function(a, b) {
                if (a[key] > b[key]) {
                    return 1; }
                if (a[key] < b[key]) {
                    return -1; }
                return 0;
            });
        }
        else {
            data.sort(function(a, b) {
                if (a[key] < b[key]) {
                    return 1; }
                if (a[key] > b[key]) {
                    return -1; }
                return 0;
            });
        }
        return data;
    };



    @observable PageContent = {
        contentJSX: null,
        ObjID: null,
        TypeID: null,
        isLoad: true,
    };
    @observable PageTrig = false;
}


export default new ObjectsStore();

