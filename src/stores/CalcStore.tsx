import { observable, action } from 'mobx';

class CalcStore {
    @observable editor:object = {};
    @observable calc:string = '';
    @observable class:string = '';
    @observable name:string = '';
    @observable answers = [];
    @observable errors = [];
    @observable warnings = [];
    @observable answersExpandedKeys: [];
    @observable errorsExpandedKeys: [];
    @observable warningsExpandedKeys: [];
    @observable showSidebar:boolean = false;

    @action getFormula() {
        return this.editor.getValue();
    }

    @action setFormula(formula:string) {
        this.editor.setValue(formula);
    }

    @action setName(name:string) {
        this.name = name;
    }

    @action setCalc(uid:string) {
        this.calc = uid;
    }

    @action setAnswers(answers) {
        this.answers = answers;
        this.answersExpandedKeys = ['answerCalcRoot0'];
    }

    @action setErrors(errors) {
        this.errors = errors;
        this.errorsExpandedKeys = ['errorsCalcRoot0'];
    }

    @action setWarnings(warnings) {
        this.warnings = warnings;
        this.warningsExpandedKeys = ['warningsCalcRoot0'];
    }

    @action setAnswersExpandedKeys(expandedKeys) {
        this.answersExpandedKeys = expandedKeys;
    }

    @action setErrorsExpandedKeys(expandedKeys) {
        this.errorsExpandedKeys = expandedKeys;
    }

    @action setWarningsExpandedKeys(expandedKeys) {
        this.warningsExpandedKeys = expandedKeys;
    }

    @action setClass(uid:string) {
        this.class = uid;
    }

    @action setValue(formula:string) {
        this.editor.setValue(formula);
    }

    @action setEditor(monaco:object) {
        this.editor = monaco;
    }

    @action insertSnippet(snippet:string) {
        const contribution = this.editor.getContribution('snippetController2');
        contribution.insert(snippet);
        this.editor.focus();
    }

    showSidebar() {
        this.showSidebar = true;
    }

    hideSidebar() {
        this.showSidebar = false;
    }

    triggerSidebar() {
        this.showSidebar = !this.showSidebar;
    }
}


export default new CalcStore();
