import {observable, action, ObservableMap} from 'mobx';
import * as React from "react";

interface INodeTemplate {
    ParentIDTree: string; //ID специально для дерева
    iddb: string;

    ParentID: string;
    id: string;

    Name: string;
    icon?: any;

    children: object[];
    TypeID?: string;
    ParrentTypeID?: string;
    FolderID?: string;
    IsFolder?: string;

    groups?: [],
}

interface ISettings {
    HoverMenuSidebar: boolean,
    HoverFilterSidebar: boolean,
    isOpenMenuSidebar: boolean,
    isOpenMenuSidebarMin: boolean,
    isOpenFilterSidebar: boolean,
}


class ApplicationStore  {
    getSettings = (NameSett, Sett) => {
        const ParamsPanelSett = Sett;
        let tmpParamsPanelSett;
        try {
            tmpParamsPanelSett = JSON.parse(localStorage.getItem(NameSett));
        } catch(e) {
            tmpParamsPanelSett = {};
            console.error("Error parsing JSON \"TypeTreeSett\"");
        }
        const extend_rec = function (obj1, obj2) {
            //obj1 наследует свойства obj2, если такие есть
            Object.keys(obj1).forEach( (key) => {
                if( obj2 && typeof obj2 === "object" && obj2[key]) {
                    if( obj1 && typeof obj1[key] === "object" && Object.keys(obj1[key]).length > 0 ) {
                        //Значит можно еще глубже
                        extend_rec(obj1[key], obj2[key]);
                    }
                    else {
                        obj1[key] = obj2[key];
                    }
                }
            });
        };
        extend_rec(ParamsPanelSett, tmpParamsPanelSett);

        return ParamsPanelSett;
    };

    @observable Settings: ISettings = this.getSettings("Settings", {
        HoverMenuSidebar: false,
        HoverFilterSidebar: false,
        isOpenMenuSidebar: true,
        isOpenMenuSidebarMin: false,
        isOpenFilterSidebar: true,
    });
    @observable IsResized: boolean = true; //вызывает глобальный zt_resize
    @observable showSaveModal:boolean = false;

    @observable showSortModal:boolean = false;
    @observable sortItems:[] = [];
    @observable sortMode:string = '';
    @observable sortAdvData:object = null;



    @observable treeData:object[] = [];
    @observable treeDataClass:object[] = [];

    @observable SelectNode?:object = null; //Выбранный узел в дереве (ссылка)
    @observable SelectNodeClass:object=null;

    @action setSelectNode(payload: object){
      this.SelectNode=payload;
    }

    @action UpdTreeDataAction(newTreeData:object[]) {
        this.treeData = newTreeData
    }
    @action UpdTreeDataClassAction(newTreeData:object[]) {
        this.treeDataClass = newTreeData
    }

    @action AddNode( payload: object) {
        const loopFindAndCreateNode = (children) => {
            children.forEach((item) => {
                if(item.children) {
                    loopFindAndCreateNode(item.children);
                }
                if(item.iddb == payload.ParentID) {
                    let newItem  = {
                        ...payload,
                        ParentID: item.iddb,
                        id: item.id + '|' + payload.iddb,

                    };
                    if(!Array.isArray( item.children )) {
                        item.children = [newItem]
                    }
                    else {
                        //Тут можно добавить с учетом сортировки
                        item.children.push(newItem);
                        this.h_sort(item.children, "Name", 1);
                    }
                }
            });
        };
        loopFindAndCreateNode(this.treeData);
    }
    @action EditNode(payload: object) {
        const loopFindAndCreateNode = (children) => {
            children.forEach((item) => {
                if(item.children) {
                    loopFindAndCreateNode(item.children);
                }
                if(item.iddb == payload.iddb) {
                    item.Name = payload.name
                }
            });
        };
        loopFindAndCreateNode(this.treeData);
    }
    @action DeleteNode(payload: { iddb: string, }) {

        let NewTreeData = [];
        const loopFindAndCreateNode = (children) => {
            let NewChildren = [];
            children.forEach((item) => {
                if(item.children) {
                    item.children = loopFindAndCreateNode(item.children);
                }
                if(item.iddb != payload.iddb) {
                    NewChildren.push(item);
                }
            });
            return NewChildren.length > 0 ? NewChildren : null;
        };
        NewTreeData = loopFindAndCreateNode(this.treeData);
        this.treeData = NewTreeData;

        this.IsSettingView = false;
    }
    @action MoveNode(uid:string, uid_new_parent:string) {

        let NewTreeData = [];
        const loopFindNode = (children) => {
            let Node=null;
            children.forEach((item) => {
                if(item.iddb == uid) {
                    Node=item;
                }
                if(item.children) {
                    let chNode = loopFindNode(item.children);
                    if(chNode){
                      Node =chNode;
                    }
                }

            });
            return Node;
        };
        const loopFindAndDeleteNode = (children, Node) => {
            let NewChildren = [];
            children.forEach((item) => {
                if(item.children) {
                    item.children = loopFindAndDeleteNode(item.children, Node);
                }
                if(item.iddb != Node.iddb) {
                    NewChildren.push(item);
                }
            });
            return NewChildren.length > 0 ? NewChildren : null;
        };
        const loopMoveNode = (children, Node) => {
          children.forEach((item) => {
              if(item.children) {
                  loopMoveNode(item.children, Node);
              }
              if(item.iddb == Node.ParentID) {
                  if(!Array.isArray( item.children )) {
                      item.children = [Node]
                  }
                  else {
                      //Тут можно добавить с учетом сортировки
                      item.children.push(Node);
                      this.h_sort(item.children, "Name", 1);
                  }
              }
          });
        };
        let Node = loopFindNode(this.treeData);
        if(Node){
          console.log(this.treeData);
          Node.ParentID=uid_new_parent;
          NewTreeData = loopFindAndDeleteNode(this.treeData, Node);
          loopMoveNode(NewTreeData, Node);
          this.treeData = NewTreeData;
        }
    }


    //ниже для таблицы параметров
    @observable IsSettingView:boolean = false;
    @observable IsGetDataForTab:boolean = false;
    @observable IsLoad:boolean = false;

    @observable ShowClassTree:boolean=false;
    @observable SelectedParamForClass:object=null;

    @observable ParamFullData: object[]=null;
    @observable AppParamData:object[]=null;
    @observable AppViewSett:any=null;
    @observable AppSett:any=null;
    @action setDataAppViewSett(payload:object){
        this.AppViewSett=payload;
    };
    @action setDataAppSett(payload:object){
        this.AppSett=payload;
    };

    @observable AppContentJSX = null;

    @observable WorkAreaMode={
      mode:'viewTable',
      data:null,
    };
    @action SetWorkAreaMode(payload:{mode:string, data:any}){
        this.WorkAreaMode=payload;
    };

    @action UpdParamsTrigAction = (isLoading:boolean, data:object[]) => {
        if(isLoading == false) {
            let newArr = [];
            data.forEach((item) => {
                let data= {id:Math.floor(Math.random() * 10000000000), ...item};
                data.view=data.view && data.view.uid?data.view.uid:'';
                newArr.push(data);
            });
            this.AppViewSett.attrs = newArr;
        }
        else {
            //Пошла загрузка АЯКС
            this.IsGetDataForTab=false;
            this.IsLoadParams = true;
        }

    };
    @action setDataView(selectNode:object, AppViewSett:any, isLoad:boolean, WorkAreaMode:{mode:string, data:any}, parrentsView:object[]){
        this.SelectNode=selectNode;
        this.IsLoad = isLoad;
        this.WorkAreaMode=WorkAreaMode;
        this.AppViewSett=AppViewSett;
        this.parrentsView=parrentsView;
        this.AppSett=null;
    };
    @action setDataApp(selectNode:object, AppSett:any, isLoad:boolean){
        this.SelectNode=selectNode;
        this.IsLoad = isLoad;
        this.AppSett=AppSett;
        this.AppViewSett=null;

    };
    @action sortParamData(sorted:any){
      if(this.AppViewSett.attrs && this.AppViewSett.attrs.length){
        this.AppViewSett.attrs=this.AppViewSett.attrs.slice().sort((a:any, b:any)=>{
          return sorted.indexOf(a.id)<sorted.indexOf(b.id)?-1:1;
        })
      }

    }
    @action setLoader(isLoad:boolean){
        this.IsLoad = isLoad;
    };
    @action SelectNodeForParamsAction = () => {
        this.SelectNodeForParams = this.SelectNode; //Пока передаётся ссылка, в будущем будет передаваться нормальный набор атрибутов
        this.AppContentJSX = null;


        if(this.SelectNodeForParams) {
            this.IsSettingView = true;
            this.IsGetDataForTab = true;
        }
        else {
            this.IsSettingView = false;
        }
    };

    @action AddAppParamData(payload:object, isbegin:boolean){
      payload={id:Math.floor(Math.random() * 10000000000), ...payload}
      if(isbegin){
        this.AppViewSett.attrs=[payload, ...this.AppViewSett.attrs];
        console.log(this.AppViewSett.attrs);
      }else{
        this.AppViewSett.attrs.push(payload);
      }

      //новый массив классов
      let newClassLinkData=this.AppViewSett.ClasseLinks;
      let existClasses=this.AppViewSett.ClasseLinks.map((item:any)=>{
        return item.DependentClass.uid;
      });
      this.AppViewSett.attrs.forEach((item:any)=>{
        if(existClasses.indexOf(item.class.uid)==-1){
          newClassLinkData.push({
            DependentClass:{
              ...item.class,
            },
            MainClass:{
              name:'',
              uid:'-1',
            },
            Settings:{single_mode_attr:null, single_mode_op:null},
          });
          existClasses.push(item.class.uid);
        }
      });
      this.AppViewSett.ClasseLinks=newClassLinkData;

    }
    @action AddAppArrParamData(payload:object[], isbegin:boolean){
      if(payload && payload.length){
        payload.forEach((param)=>{
          param={id:Math.floor(Math.random() * 10000000000), ...param}
          if(isbegin){
            this.AppViewSett.attrs=[param, ...this.AppViewSett.attrs];
          }else{
            this.AppViewSett.attrs.push(param);
          }
        })
      }

      //новый массив классов
      let newClassLinkData=this.AppViewSett.ClasseLinks;
      let existClasses=this.AppViewSett.ClasseLinks.map((item:any)=>{
        return item.DependentClass.uid;
      });
      this.AppViewSett.attrs.forEach((item:any)=>{
        if(existClasses.indexOf(item.class.uid)==-1){
          newClassLinkData.push({
            DependentClass:{
              ...item.class,
            },
            MainClass:{
              name:'',
              uid:'-1',
            },
            Settings:{single_mode_attr:null, single_mode_op:null},
          });
          existClasses.push(item.class.uid);
        }
      });
      this.AppViewSett.ClasseLinks=newClassLinkData;

    }
    @action UpdateParamData(payload:object){
      let existParam = this.ParamFullData.map((item)=>{
        return {...item};
      }).find((item)=>{
        if(item.name === payload.name &&  payload.class && item.class.uid===payload.class.uid){
          return true;
        }
        return false;
      });
      if(existParam){
        payload={...existParam, ...payload};
      }
      this.AppViewSett.attrs=this.AppViewSett.attrs.filter((item)=>{
          if(item.id !==payload.id && item.name === payload.name &&  payload.class && item.class.uid===payload.class.uid){
            return false;
          }
          return true;
      })
      this.AppViewSett.attrs=this.AppViewSett.attrs.map((item)=>{
        if(payload.id===item.id){
          return payload;
        }
        return item;
      });
    }
    @action RemoveParamData(id:number, viewUid:string){
      this.AppViewSett.attrs=this.AppViewSett.attrs.filter((item)=>{
        if(item.id===id){
          return false;
        }
        return true;
      });

      //новый массив классов
      let newClass_link=this.AppViewSett.attrs.map((item:any)=>{
        return item.class.uid;
      }).filter((item:any, index:number, self:any[])=>{
          return self.indexOf(item) === index;
      });

      //Измениен WorkArea
      if(this.WorkAreaMode.mode==='viewParam' && this.WorkAreaMode.data.id==id){
        this.WorkAreaMode={
          mode:'viewTable',
          data:viewUid,
        };
      }else if(this.WorkAreaMode.mode==='viewClass' && newClass_link.indexOf(this.WorkAreaMode.data.uid)===-1){
        this.WorkAreaMode={
            mode:'viewTable',
            data:viewUid,
        };
      }

      //Обновление списка классов при связи классов
      this.AppViewSett.ClasseLinks=this.AppViewSett.ClasseLinks.filter((item:any)=>{
        //удаление из общего списка
        if(newClass_link.indexOf(item.DependentClass.uid)===-1){
          return false;
        }
        return true;
      }).map((item:any)=>{
        //удаление если выбрано в качестве связи
        if(item.MainClass.uid!='00000000-0000-0000-0000-000000000000' && newClass_link.indexOf(item.MainClass.uid)===-1){
          item.MainClass.uid=-1;
        }
        return item;
      });

      this.paramsRemove()
    }
    @action RemoveArrParamData(ids:number[], viewUid:string){
      this.AppViewSett.attrs=this.AppViewSett.attrs.filter((item)=>{
        if(ids.indexOf(item.id)===-1){
          return true;
        }
        return false;
      });

      //новый массив классов
      let newClass_link=this.AppViewSett.attrs.map((item:any)=>{
        return item.class.uid;
      }).filter((item:any, index:number, self:any[])=>{
          return self.indexOf(item) === index;
      });

      //Измениен WorkArea
      if(this.WorkAreaMode.mode==='viewParam' && ids.indexOf(this.WorkAreaMode.data.id)!==-1){
        this.WorkAreaMode={
            mode:'viewTable',
            data:viewUid,
        };
      }
      else if(this.WorkAreaMode.mode==='viewClass' && newClass_link.indexOf(this.WorkAreaMode.data.uid)===-1){
        this.WorkAreaMode={
            mode:'viewTable',
            data:viewUid,
        };
      }

      ///Обновление списка классов при связи классов
      this.AppViewSett.ClasseLinks=this.AppViewSett.ClasseLinks.filter((item:any)=>{
        //удаление из общего списка
        if(newClass_link.indexOf(item.DependentClass.uid)===-1){
          return false;
        }
        return true;
      }).map((item:any)=>{
        //удаление если выбрано в качестве связи
        if(item.MainClass.uid!='00000000-0000-0000-0000-000000000000' && newClass_link.indexOf(item.MainClass.uid)===-1){
          item.MainClass.uid=-1;
        }
        return item;
      });

      this.paramsRemove();
    }
    @action SetFullParamData(payload:object){
      this.ParamFullData=payload;
    }

    paramsRemove=()=>{
        let exist_params=this.AppViewSett.ClasseLinks.map((item)=>{
          return item.uid;
        });
        let exist_classes=this.AppViewSett.ClasseLinks.map((item)=>{
          return item.DependentClass.uid;
        });

        //Удаление графиков из представления если был удален класс
        if(this.AppViewSett && this.AppViewSett.settings &&  this.AppViewSett.settings.charts){
          this.AppViewSett.settings.charts= this.AppViewSett.settings.charts.filter((item)=>{
            return exist_classes.indexOf(item.class_uid)==-1?false:true;
          })
        }

        //Удаление параметра из сортировки
        if(this.AppViewSett && this.AppViewSett.settings && this.AppViewSett.settings.view_sorts){
          this.AppViewSett.settings.view_sorts= this.AppViewSett.settings.view_sorts.filter((item)=>{
            return exist_params.indexOf(item.uid)==-1?false:true;
          })
          this.AppViewSett.settings.view_sorts=this.AppViewSett.settings.view_sorts.length?this.AppViewSett.settings.view_sorts:null;
        }


    }

    @action setShowClassTree(state:boolean, selectedParamData:object){
      this.ShowClassTree=state;
      this.SelectedParamForClass=selectedParamData;
    }
    @action setSelectedClassTree(payload:object){
      this.SelectNodeClass=payload;
    }

    @action setShowModalSave(state:boolean){
      this.showSaveModal=state;
    }

    @action setShowModalSort(state:boolean, items:[], mode:string, advData:object){
      this.showSortModal=state;
      this.sortItems=items;
      this.sortMode=mode;
      this.sortAdvData=advData;
    }

    @observable classLinkData:object[]=null;
    @action setClassLinkData(payload:object){
      this.classLinkData=payload;
    }


    h_sort = (data, key, dir) => {
        if(dir === 1) {
            data.sort(function(a, b) {
                if (a[key] > b[key]) {
                    return 1; }
                if (a[key] < b[key]) {
                    return -1; }
                return 0;
            });
        }
        else {
            data.sort(function(a, b) {
                if (a[key] < b[key]) {
                    return 1; }
                if (a[key] > b[key]) {
                    return -1; }
                return 0;
            });
        }
        return data;
    };

    @observable parrentsView:object[]=[];
    @action updateParrentsView(payload:[]){
      this.parrentsView=payload;
    }

}


export default new ApplicationStore();
