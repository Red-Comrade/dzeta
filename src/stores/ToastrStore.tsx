import {observable, action, ObservableMap} from 'mobx';
import * as React from "react";
import ToastrStore from 'mobx-toastr'

const options = {
    closeButton: false
};

export default new ToastrStore(options);