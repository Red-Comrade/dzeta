import { observable, action, computed } from 'mobx';
import { ImportStore } from './ImportStore';

class TaxesStore extends ImportStore {
	@observable params:array = [
		'Numéro fiscal',
		'Référence de l\'avis'
	];
}

export default new TaxesStore();