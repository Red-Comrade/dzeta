import { observable, action, computed } from 'mobx';

export interface IImportColumnSettings {
	attribute?:string;
	column:string;
}

export class ImportStore {
	@observable settings:IImportColumnSettings[] = [];
	@observable data:string[] = [];
	@observable file?:object = null;
	@observable rewrite:number = 2;

	@action updateFile(file:object) {
		this.file = file;
	}

	@action updateDataAndSettings(columns:string[], data:string[]) {
		this.settings = [];
		for (let i = 0; i < columns.length; i++) {
			this.settings.push({
				attribute: null,
				column: columns[i]
			});
		}
		this.data = data;
	}

	@action updateColumnAttribute(column:string, attribute?:string) {
		for (let i = 0; i < this.settings.length; i++) {
			if (this.settings[i].column == column) {
				this.settings[i].attribute = attribute;
				break;
			}
		}
	}

	@action updateRewrite(rewrite:number) {
		this.rewrite = rewrite;
	}

	@computed get filename() {
		if (this.file && this.file.name) {
			return this.file.name;
		}
		return '';
	}

	@computed get selectedAttributes() {
		const attributes = [];
		for (let i = 0; i < this.settings.length; i++) {
			if (this.settings[i].attribute) {
				attributes.push(this.settings[i].attribute);
			}
		}
		return attributes;
	}

	columnAttribute(column:string) {
		let attribute = null;
		for (let i = 0; i < this.settings.length; i++) {
			if (this.settings[i].column == column) {
				attribute = this.settings[i].attribute;
				break;
			}
		}
		return attribute;
	}
}

export default new ImportStore();