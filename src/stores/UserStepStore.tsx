import {observable, action, ObservableMap} from 'mobx';
import * as React from "react";

class UserStepStore  {
    @observable currentStep:number = 0;
    @action setCurrentStep(step:number){
      this.currentStep=step;
    }

    @observable facialData:object[] = [];
    @action updateFacialData(paylod:object){
      this.facialData=paylod;
    }

    @observable clientData:object= {
      nom:'',
      prenom:'',
      adresse:'',
      codepostal:'',
      telephone1:'',
      datedesignature:'',
      datefacture:'',
      numerofacture:'',
      raisonsocial:'',
      siren:'',
      fonctionsignataire:'',
      ville:'',
      courriel:'',

    };
    @action updateClientData(paylod:object){
        this.clientData={...this.clientData, ...paylod};
    }

    @observable dossierData:object = {
      deal:'',
      source:'',
      sourcecomp:'',
    };
    @action updateDossierData(paylod:object){
        this.dossierData={...this.dossierData, ...paylod};
    }


    @observable dataOperation:object[] =[];
    @action updateDataOperation(paylod:object){
        this.dataOperation=paylod;
    }

    @observable showOperationModal:boolean =false;
    @action setOperationModal(state:boolean){
      this.showOperationModal=state;
    }

    @observable showAdditBlocksModal:boolean =false;
    @observable additBlockData:object =null;
    @action setVisibleAdditBlocksModal(state:boolean){
      this.showAdditBlocksModal=state;
    }
    @action setAdditBlocksModal(state:boolean, data:object){
      this.showAdditBlocksModal=state;
      this.additBlockData=data;
    }

    @observable confirmModal:{show:boolean, text:string, title:string, YesTitle:string, NonTitle:string, callbackYes:()=>void, callbackNo:()=>void} ={
      show:false,
      text:'',
      title:'',
      YesTitle:'',
      NonTitle:'',
      callbackYes:()=>{},
      callbackNo:()=>{}
    };
    @action setVisibleConfirmModal(payload:any){
      this.confirmModal=payload;
    }

    @observable posetData:object= {
      date:'',
      heure:'',
      poseur:'',
      consignes:'',
    };
    @action updatePosetData(paylod:object){
        this.posetData={...this.posetData, ...paylod};
    }

    @observable operationAvecData:object= {
      operationavec:'',
    };
    @action updateOperationAvecData(paylod:object){
        this.operationAvecData={...this.operationAvecData, ...paylod};
    }

    @observable dataClaculator:object={
      persone:'Personne morale',
      organization:{
        name:'Esso',
        uid:'b63dd855-2b40-4cef-ba4a-201e76966136',
      },
      date:{
        date_de_signature:'',
        date_facture:'',
        numero_facture:'',
      },
      information_lieu_travaux:{
        operationavec:'Operation classique',
        previsite:'',
        nom_travaux:'',
        adresse_travaux:'',
        code_postale_travaux:'',
        ville_travaux:'',
        siern_travaux:'',
        same_adress:0,
        adv_field:{
          //qpv
          code_quartier:'',
          //Bailleur Social
          raison_sociale:'',
          siern:'',
          nom:'',
          prenom:'',
          fonction:'',
          phone:'',
          adresse:'',
          code_postale:'',
          ville:'',

          nombre_total:'',
          nombre_total_menages:'',

          //Operation avec precarite
          adv_fiscal_data:[{
            numero_fiscal:'',
            reference_fiscal:'',
          }],
        }
      },
      beneficiaire:{
        nom:'',
        prenom:'',
        fonction:'',
        phone:'',
        email:'',
        raison_sociale:'',
        siern:'',
        adresse:'',
        code_postale:'',
        ville:'',
        persone_phys_data:[],
      },
      sous_traitance:{
        sous_traitance:1,
        siret:'',
        nom:'',
        prenom:'',
        raison_sociale:'',
      },
    };
    @action initCalculatorData(dataClaculator:object, dataOperation:object[]){
        this.dataClaculator=dataClaculator;
        this.dataOperation=dataOperation;
        this.currentStep=0;
        this.validateError=[];
    }
    @action updateCalculatorData(key:string, paylod:object){
        this.dataClaculator[key]=paylod;
    }

    @observable validateError:{id:string, id_msg?:string}[]=[];
    @action addValidateError(paylod:{id:string, id_msg?:string}){
      this.validateError= this.validateError.filter((error_item)=>{
        return error_item.id==paylod.id?false:true;
      })
      this.validateError.push(paylod);
    }
    @action removeValidateError(id:string){
      this.validateError=this.validateError.filter((error_item)=>{
        return error_item.id===id?false:true;
      });
    }

    @action addValidateErrorArray(paylod:{id:string, id_msg?:string}[]){
      paylod.forEach((error)=>{
        this.validateError= this.validateError.filter((error_item)=>{
          return error_item.id==error.id?false:true;
        })
        this.validateError.push(error);
      });

    }
    @action removeValidateErrorArray(ids:string[]){
      this.validateError=this.validateError.filter((error_item)=>{
        return ids.indexOf(error_item.id)==-1?true:false;
      });
    }

    @observable animation:any={
      clientHeight:0,
      scrollTop:0,
    }
    @action setAnimObj(payload:any){
      this.animation=payload;
    }


}


export default new UserStepStore();
