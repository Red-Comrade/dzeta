import {observable, action, ObservableMap} from 'mobx';
import * as React from "react";

interface INodeTemplate {
    ParentIDTree: string; //ID специально для дерева
    iddb: string;

    ParentID: string;
    id: string;

    Name: string;
    icon?: any;

    children: object[];
    TypeID?: string;
    ParrentTypeID?: string;
    FolderID?: string;
    IsFolder?: string;

    groups?: [],
}

interface ISettings {
    HoverMenuSidebar: boolean,
    HoverFilterSidebar: boolean,
    isOpenMenuSidebar: boolean,
    isOpenMenuSidebarMin: boolean,
    isOpenFilterSidebar: boolean,
}


export class ClassesStore  {
    static readonly CLASSES_FOLDER_TYPE = 'classes_folder';
    static readonly CLASS_TYPE = 'class';
    static readonly METHODS_FOLDER_TYPE = 'methods_folder';
    static readonly METHOD_TYPE = 'method';
    static readonly ROOT_TREE_ICON = 'fas fa-sitemap';
    static readonly CLASS_TREE_ICON = 'fas fa-file';
    static readonly METHOD_TREE_ICON = 'fas fa-square-root-alt';
    static readonly CLASSES_FOLDER_TREE_ICON = 'fas fa-copy';
    static readonly METHODS_FOLDER_TREE_ICON = 'fas fa-calculator';

    getSettings = (NameSett, Sett) => {
        const ParamsPanelSett = Sett;
        let tmpParamsPanelSett;
        try {
            tmpParamsPanelSett = JSON.parse(localStorage.getItem(NameSett));
        } catch(e) {
            tmpParamsPanelSett = {};
            console.error("Error parsing JSON \"TypeTreeSett\"");
        }
        const extend_rec = function (obj1, obj2) {
            //obj1 наследует свойства obj2, если такие есть
            Object.keys(obj1).forEach( (key) => {
                if( obj2 && typeof obj2 === "object" && obj2[key]) {
                    if( obj1 && typeof obj1[key] === "object" && Object.keys(obj1[key]).length > 0 ) {
                        //Значит можно еще глубже
                        extend_rec(obj1[key], obj2[key]);
                    }
                    else {
                        obj1[key] = obj2[key];
                    }
                }
            });
        };
        extend_rec(ParamsPanelSett, tmpParamsPanelSett);

        return ParamsPanelSett;
    };

    @observable Settings: ISettings = this.getSettings("Settings", {
        HoverMenuSidebar: false,
        HoverFilterSidebar: false,
        isOpenMenuSidebar: true,
        isOpenMenuSidebarMin: false,
        isOpenFilterSidebar: true,
    });
    @observable IsResized: boolean = true; //вызывает глобальный zt_resize



    @observable treeData:object[] = [];
    @observable SelectNode?:object = null; //Выбранный узел в дереве (ссылка)

    @action UpdTreeDataAction(newTreeData:object[]) {
        this.treeData = newTreeData
    }

    @action AddNode( payload: {
            ParentIDTree?: string,
            id?: string,

            ParentID:string,
            iddb: string,
            Name: string,
            icon?: any,
            TypeID?: string,
            ParrentTypeID?: string,
            FolderID?: string,
            IsFolder?: string,
        }) {
        const loopFindAndCreateNode = (children) => {
            children.forEach((item) => {
                if(item.children) {
                    loopFindAndCreateNode(item.children);
                }
                if(item.iddb == payload.ParentID) {
                    let newItem : INodeTemplate = {
                        Name: payload.Name,
                        ParentIDTree: item.children[0].id,
                        id: item.id + '|cl' + payload.iddb,
                        icon: payload.icon,

                        ParentID: item.iddb,
                        iddb: payload.iddb,

                        groups: [],
                        children: [
                            {
                                Name: 'Classes',
                                icon: ClassesStore.CLASSES_FOLDER_TREE_ICON,
                                id: item.id + '|cl',
                                key: item.id + '|cl',
                                type: ClassesStore.CLASSES_FOLDER_TYPE,
                                ParentIDTree: item.id + '|' + payload.iddb,
                                children: null,
                            },
                            {
                                Name: 'Methods',
                                icon: ClassesStore.METHODS_FOLDER_TREE_ICON,
                                id: item.id + '|m',
                                key: item.id + '|m',
                                type: ClassesStore.METHODS_FOLDER_TYPE,
                                TypeID: item.uid,
                                ParentIDTree: item.id + '|' + payload.iddb,
                                children: null
                            }
                        ],
                        TypeID: payload.TypeID,
                        ParrentTypeID: payload.ParrentTypeID,
                        FolderID: payload.FolderID,
                        IsFolder: payload.IsFolder,
                        type: ClassesStore.CLASS_TYPE,
                    };
                    if(!Array.isArray( item.children[0].children )) {
                        item.children[0].children = [newItem]
                    }
                    else {
                        //Тут можно добавить с учетом сортировки
                        item.children[0].children.push(newItem);
                        this.h_sort(item.children[0].children, "Name", 1);
                    }
                }
            });
        };
        loopFindAndCreateNode(this.treeData);
    }
    @action RenameNode(payload: { iddb: string, newName: string, }) {
        const loopFindAndCreateNode = (children) => {
            children.forEach((item) => {
                if(item.children) {
                    loopFindAndCreateNode(item.children);
                }
                if(item.iddb == payload.iddb) {
                    item.Name = payload.newName
                }
            });
        };
        loopFindAndCreateNode(this.treeData);
    }
    @action DeleteNode(payload: { iddb: string, ParrentTypeID: string }) {
        let NewTreeData = [];
        const loopFindAndCreateNode = (children) => {
            let NewChildren = [];
            let isRemove = false;
            children.forEach((item) => {
                if(item.children) {
                    item.children = loopFindAndCreateNode(item.children);
                }
                if(item.iddb != payload.iddb || item.ParrentTypeID != payload.ParrentTypeID) {
                    NewChildren.push(item);
                }
                else {
                    isRemove = true;
                }
            });
            return NewChildren.length > 0 || !isRemove ? NewChildren : null ;
        };
        NewTreeData = loopFindAndCreateNode(this.treeData);
        this.treeData = NewTreeData;

        this.IsOpenTable = false;
    }
    @action AddMethodNode( payload: {
            ParentIDTree?: string,
            id?: string,

            ParentID:string,
            iddb: string,
            Name: string,
            icon?: any,
            TypeID?: string,
            ParrentTypeID?: string,
            FolderID?: string,
            IsFolder?: string,
        }) {
        let methodNode = null;
        const loopFindAndCreateNode = (children) => {
            children.forEach((item) => {
                if(item.children) {
                    loopFindAndCreateNode(item.children);
                }
                if(item.iddb == payload.parent) {
                    let newItem : INodeTemplate = {
                        Name: payload.Name,
                        name: payload.Name,
                        ParentIDTree: item.children[1].id,
                        iddb: payload.iddb,
                        uid: payload.iddb,
                        icon: ClassesStore.METHOD_TREE_ICON,
                        id: item.id + '|m|' + payload.iddb,
                        key: item.children[1].ParentIDTree + '|m|' + payload.iddb,
                        type: ClassesStore.METHOD_TYPE,
                        ParentIDTree: item.children[1].ParentIDTree,
                        ParrentTypeID: payload.ParrentTypeID,
                        parent: payload.parent,
                        children: null,
                    };
                    methodNode = newItem;
                    if(!Array.isArray( item.children[1].children )) {
                        item.children[1].children = [newItem]
                    }
                    else {
                        //Тут можно добавить с учетом сортировки
                        item.children[1].children.push(newItem);
                        this.h_sort(item.children[1].children, "Name", 1);
                    }
                }
            });
        }
        loopFindAndCreateNode(this.treeData);
        return methodNode;
    }


    //ниже для таблицы параметров
    @observable SelectNodeForParams?:object = null; //Выбранный узел для панельки с параметрами
    @observable IsOpenTable:boolean = false;
    @observable IsGetDataForTab:boolean = false;
    @observable IsLoadParams: boolean = false; //Если прямо сейчас происходит загрузка параметров
    @observable ActiveGroupID:string = null;
    @observable AttrData:object[] = null;
    @observable SelectTr:object = null; //Выбранная строка

    @action SelectGroupAction = (GroupID) => {
        if(GroupID) {
            this.ActiveGroupID = GroupID;
        }
        this.IsGetDataForTab = true;
    };
    @action UpdParamsTrigAction = (isLoading:boolean, data:object[]) => {
        if(isLoading == false) {
            let newArr = [];
            data.forEach((item) => {
                newArr.push(item);
            });
            this.AttrData = newArr;
            this.IsOpenTable = true;
            this.IsLoadParams = false;
        }
        else {
            //Пошла загрузка АЯКС
            this.IsGetDataForTab = false;
            this.IsLoadParams = true;
        //    this.IsOpenTable = false;
        }
        this.SelectTr = null;
    };
    @action SelectNodeForParamsAction = () => {
        this.SelectNodeForParams = this.SelectNode; //Пока передаётся ссылка, в будущем будет передаваться нормальный набор атрибутов

        if(this.SelectNodeForParams) {
            this.IsOpenTable = true;
            this.IsGetDataForTab = this.SelectNodeForParams.type == ClassesStore.CLASS_TYPE;
        }
        else {
            this.IsOpenTable = false;
        }
    };

    h_sort = (data, key, dir) => {
        if(dir === 1) {
            data.sort(function(a, b) {
                if (a[key] > b[key]) {
                    return 1; }
                if (a[key] < b[key]) {
                    return -1; }
                return 0;
            });
        }
        else {
            data.sort(function(a, b) {
                if (a[key] < b[key]) {
                    return 1; }
                if (a[key] > b[key]) {
                    return -1; }
                return 0;
            });
        }
        return data;
    };

}


export default new ClassesStore();