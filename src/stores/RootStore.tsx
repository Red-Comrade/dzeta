import {observable, action, ObservableMap} from 'mobx';
import * as React from "react";
import IModalConfig from "../components/CoolModalForm/IModalConfig";
import {id} from "date-fns/locale";
import IModalConfigDictionary from "../components/CoolModalForm/IModalConfigDictionary";


interface ISettings {
    HoverMenuSidebar: boolean,
    HoverFilterSidebar: boolean,
    isOpenMenuSidebar: boolean,
    isOpenMenuSidebarMin: boolean,
    isOpenFilterSidebar: boolean,

    WidthFilterSidebar: number, //if null, then we have default width, for example 300px
}

const defSett = {
    HoverMenuSidebar: false,
    HoverFilterSidebar: false,
    isOpenMenuSidebar: true,
    isOpenMenuSidebarMin: true,
    isOpenFilterSidebar: false,
    RightMenuPanel: false,
    WidthFilterSidebar: 0,
};

class RootStore  {
    getSettings = (NameSett, Sett) => {
        const ParamsPanelSett = Sett;
        let tmpParamsPanelSett;
        try {
            tmpParamsPanelSett = JSON.parse(localStorage.getItem(NameSett));
        } catch(e) {
            tmpParamsPanelSett = {};
            console.error("Error parsing JSON \"TypeTreeSett\"");
        }
        const extend_rec = function (obj1, obj2) {
            //obj1 наследует свойства obj2, если такие есть
            Object.keys(obj1).forEach( (key) => {
                if( obj2 && typeof obj2 === "object" && obj2[key] !== undefined) {
                    if( obj1 && typeof obj1[key] === "object" && Object.keys(obj1[key]).length > 0 ) {
                        //Значит можно еще глубже
                        extend_rec(obj1[key], obj2[key]);
                    }
                    else {
                        obj1[key] = obj2[key];
                    }
                }
            });
        };
        extend_rec(ParamsPanelSett, tmpParamsPanelSett);

        return ParamsPanelSett;
    };
    saveSettings = () => {
        localStorage.setItem('SettingsM', JSON.stringify(this.Settings));
    };

    public readonly config = localStorage.config ? JSON.parse(localStorage.config) : {};

    @observable Settings: ISettings = this.getSettings("SettingsM", defSett);
    @observable IsResized: boolean = true; //вызывает глобальный zt_resize
    @observable MSettings: any = {
        UserName: "Login",
        UserUID:'uid',
        UserGroups: [],
        UserObjData: [],
    }; //вызывает глобальный zt_resize
    @observable isLoadMSettings: boolean = false;
    @observable Applications: any = [];
    @observable modules: string[] = [];
    @observable home_url: string = "/";

    @observable ProgressIsOpen:boolean = false;
    @observable ProgressData = {
        completed: 0
    };

    @observable GalleryIsOpen:boolean = false;
    @observable GalleryData = {
        Images: []
    };

    @observable TreeTableUpdate:string = null;
    @action updateTreeTable=()=>{
      this.TreeTableUpdate=new Date().getTime().toString();
    }



    @action OpenForm = (uidForm, ModalConfig) => {
        this.Forms[uidForm] = ModalConfig;
        this.TrigForm = !this.TrigForm;
    };
    @action UpdateForm = (uidForm) => {
        this.Forms[uidForm].TrigForm = !this.Forms[uidForm].TrigForm; //Так мы обновляем только одну форму
        this.TrigForm = !this.TrigForm;
    };
    @action UpdateForms = () => {
        this.TrigForm = !this.TrigForm;
    };
    @action CloseForm = (uidForm) => {
        delete this.Forms[uidForm];
        this.TrigForm = !this.TrigForm;
    };

    @observable TrigForm:boolean = false;

    @observable Forms = {};

    //@observable Forms:IModalConfigDictionary = {};
}



export default new RootStore();
