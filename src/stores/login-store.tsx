import { observable, action } from 'mobx';

class LoginStore {
	@observable login:string = '';
	@observable password:string = '';

	@observable disableLogin:boolean = true;
	@observable disableForgotPWD:boolean = true;
	@observable visibleForgotForm:boolean = false;
	@observable disableForgotEmailSubmit:boolean = false;
	@observable error:boolean = false;

	@action setVisibleForgotForm(visible:boolean) {
		this.visibleForgotForm = visible;
	}

	@action setLogin(login:string) {
		this.login = login;
		this.checkDisable();
		this.checkDisableForgotSubmitEmail();
	}

	@action setPassword(password:string) {
		this.password = password;
		this.checkDisable();
	}

	@action setError(error:boolean) {
		this.error = error;
	}
	@action setDisableForgotEmailSubmit(disabled:boolean) {
		this.disableForgotEmailSubmit = disabled;
	}
	@action setDisable(disabled:boolean) {
		this.disableLogin = disabled;
	}

	checkDisable() {
		this.disableLogin = !this.login || !this.password;
	}
	checkDisableForgotSubmitEmail() {
		this.disableForgotPWD = !this.login;
	}
}

export default new LoginStore();
