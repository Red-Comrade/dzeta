import React from 'react';
import ReactDOM from "react-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

//import CalcEditor from "./containers/CalcPage/CalcEditor";
//import CalcPanel  from "./containers/CalcPage/CalcPanel";
import CalcMain  from "./containers/CalcPage/CalcPage";
import CalcStore from './stores/CalcStore';
import ToastrStore from './stores/ToastrStore'
import '../node_modules/toastr/build/toastr.css';
import "./calc.scss"

import { Provider } from "mobx-react";
  import { Route,BrowserRouter } from "react-router-dom"
import './i18n';

function App() {

  return (
    <Provider CalcStore={CalcStore} ToastrStore={ToastrStore}>
      <BrowserRouter>
        <Route path="/calc" component={CalcMain} />
      </BrowserRouter>
    </Provider>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
