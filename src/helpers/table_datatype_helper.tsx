//Исторически так сложилось, что название такое
import * as React from "react";
import axios from 'axios';
import {format, parse} from "date-fns";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import * as ot_api from "../api/ObjTree-api";
import {Children} from "react";
import RootStore from '../stores/RootStore'
import ToastrStore from '../stores/ToastrStore'
import {IValue} from "../api/ObjTree-api";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import AsyncSelect from 'react-select/async';



let XHR_LOAD_SELECT = {cancel: null};
let XHR_LOAD_VALUE = {cancel: null};

export interface ITableDatatypeClass {
    item_td: any,
    item_th: any,
    item_tr: any,
    HeadData: any,
    key: string | number,
    select_cell: {item_tr, item_td, index_td, item_th, isEdit?:boolean, isNotMove?:boolean, attrCache?: any} | never,
    //attrDataFileCache: any,
    //attrData: any,
    type: "add" | "edit" | "read",
    //ObjID: string;
    //TypeID: string;
    ChangeStateFunc: () => void, // this.setState();
    onClickEInput: (e) => void,
    SaveCellWithoutCache?: (newValue, SuccFunc) => void
}

export class TableDatatypeClass {
    item_td: any;
    item_th: any;
    item_tr: any;
    HeadData: any;
    key: string | number;
    select_cell: {item_tr, item_td, index_td, item_th, isEdit?:boolean, isNotMove?:boolean, attrCache?: any} | never;
    attrDataFileCache: any;
    attrData: any;
    type: "add" | "edit" | "read";

    ChangeStateFunc: () => void;
    SaveCellWithoutCache: (newValue, SuccFunc) => void;
    onClickEInput: (e) => void;

    is_load_value = false;
    is_loading_value = false;

    constructor(params:ITableDatatypeClass) {
        this.item_td = params.item_td;
        this.item_th = params.item_th;
        this.item_tr = params.item_tr;
        this.HeadData = params.HeadData;
        this.key = params.key;
        this.select_cell = params.select_cell;
        //this.attrDataFileCache = params.attrDataFileCache;
        //this.attrData = params.attrData;
        this.type = params.type;
        this.ChangeStateFunc = params.ChangeStateFunc;
        this.onClickEInput = params.onClickEInput;
        this.SaveCellWithoutCache = params.SaveCellWithoutCache;
    }

    public GetControl = () => {


        if(this.item_th.data?.attr?.dopSett.data_source) {
            //Значит к нашему параметру привязан расчет-перечисление
            return this.object();
        }
        else if(this.item_th.data.datatype == "number") {
            return this.number();
        }
        else if(this.item_th.data.datatype == "text") {
            return this.text();
        }
        else if(this.item_th.data.datatype == "object") {
            return this.object();
        }
        else return <div><input autoFocus={true} className={"kt_e_input"} readOnly={true} value={"In developing!"} /></div>;
        /*
        else if(this.item_th.data.datatype == "datetime") {
            return this.text();
        }
        else if(this.item_th.data.datatype == "object") {
            return this.text();
        }
        else if(this.item_th.data.datatype == "file") {
            return this.text();
        }
        */
    };


    private number = () => {
        let value = !this.item_td.v ? "" : this.item_td.v;
        let isDisabled = false;

        if(this.select_cell.attrCache?.value !== undefined) {
            //Значит есть кэш
            value = this.select_cell.attrCache.value;
        }

        return <div className={"kt_e_div"}>
            {this.number_control(value, isDisabled, 'one_control',  (e) => {
                let t_value = e.target.value;
                if(this.select_cell.attrCache == undefined) {
                    this.select_cell.attrCache = {};
                }
                this.select_cell.attrCache.value = t_value;
                this.ChangeStateFunc();
            })}
        </div>;
    };
    private text = () => {
        let value = !this.item_td.v ? "" : this.item_td.v;
        let isDisabled = false;
        if(this.select_cell.attrCache?.value !== undefined) {
            //Значит есть кэш
            value = this.select_cell.attrCache.value;
        }

        let Control;

        if(this.item_th.data.attr.TextType == 1) {
            if(this.item_td.is_load_value) {

            }
            else {
                if(!this.item_td.is_loading_value) {
                    this.item_td.is_loading_value = true;
                    this.core.AJAX.getFullValue(this.item_td.o, this.item_th.data.uid, (answer) => {
                        this.item_td.is_load_value = true;
                        this.item_td.is_loading_value = false;
                        let t_value = answer.data.V;

                        this.SaveCellWithoutCache(t_value, () => {
                            this.ChangeStateFunc();
                        });
                    }, () => {
                        this.item_td.is_load_value = false;
                        this.item_td.is_loading_value = false;
                    });
                }
            }

            if(this.item_td.is_loading_value) {
                return <div className={"kt_e_div"}>
                    Loading
                </div>;
            }
            else {
                let className = "cool_t_select_cell ";

                if(this.select_cell.isEdit) {
                    className += "cool_t_editable_cell ";
                    return this.ckeditor_control(value, false, 'one',
                        (e) => {
                            let t_value = e.target.value;
                            if(this.select_cell.attrCache == undefined) {
                                this.select_cell.attrCache = {};
                            }
                            this.select_cell.attrCache.value = t_value;
                        }
                    );
                }
                else {
                    let valid_value = value.replace(/<\/?[^>]+(>|$)/g, "");

                    return <div className="cool_t_focus_cell_div">
                        <input className={"cool_t_focus_cell"} autoFocus={true} value={valid_value}
                               onDoubleClick={(e) => {
                                   this.events.onDoubleClickFocusInput(e);
                               }}
                               onChange={(e)=>{
                                   this.events.onDoubleClickFocusInput(e);
                               }}
                        />
                    </div>;
                }
            }
        }
        else {

            return <div className={"kt_e_div"}>
                {this.text_control(value, isDisabled, 'one_control',  (e) => {
                    let t_value = e.target.value;
                    if(this.select_cell.attrCache == undefined) {
                        this.select_cell.attrCache = {};
                    }
                    this.select_cell.attrCache.value = t_value;
                    this.ChangeStateFunc();
                })}
            </div>;

        }
    };
    private object = () => {
        let value = !this.item_td.v ? "" : this.item_td.v;
        let label = !this.item_td.ln ? "" : this.item_td.ln;

        let isDisabled = false;

        if(this.select_cell.attrCache?.value != undefined) {
            //Значит есть кэш
            value = this.select_cell.attrCache.value;
            label = this.select_cell.attrCache.label;
        }

        return (
            <div className={"kt_e_div"}>
                {this.object_control({value: value, label: label }, isDisabled, 'one_control',  (option) => {

                    let t_value = option.value ? option.value : "";
                    let t_label = option.label ? option.label : "";

                    if(this.select_cell.attrCache == undefined) {
                        this.select_cell.attrCache = {};
                    }

                    this.select_cell.attrCache.value = t_value;
                    this.select_cell.attrCache.label = t_label;

                    this.ChangeStateFunc();
                })}
            </div>);
    };


    private number_control = (value, isDisabled, key, onChange) => {
        return (
            <input autoFocus={true} placeholder={"Enter number"} key={key} type="number" disabled={isDisabled}
                   value={value} title={value}
                   onChange={(e) => { onChange(e); }}
                   onClick={this.onClickEInput}
                   className={"kt_e_input kt_e_active_control"}
            />
        );
    };
    private text_control = (value, isDisabled, key, onChange) => {
        return (
            <input autoFocus={true} placeholder={"Enter number"} key={key} type="text" disabled={isDisabled}
                   value={value} title={value}
                   onChange={(e) => { onChange(e); }}
                   onClick={this.onClickEInput}
                   className={"kt_e_input kt_e_active_control"}
            />
        );
    };


    private object_control2 = (value, isDisabled, key, onChange) => {
        let JSXOptions = [];
        let isLoadOptions = true;

        if(this.item_th?.data.LinkedClass || this.item_th.data?.attr?.dopSett.data_source) {
            if(this.item_td.SelectData) {
                JSXOptions.push(<option key={0} value={""}> not selected</option>);
                this.item_td.SelectData.forEach((item_o, key_o) => {
                    JSXOptions.push(
                        <option value={item_o.uid}>{item_o.name}</option>
                    );
                });
            }
            else {
                isDisabled = true;
                isLoadOptions = false;
                JSXOptions = [<option key={0} value={""}>#Loading...#</option>];
                this.core.GetSelectData("",
                    (answer) => {
                        this.item_td.SelectData = answer.data;
                        this.ChangeStateFunc();
                    },
                    (ex) => {
                        if (axios.isCancel(ex)) {
                            console.log("post Request canceled");
                        }
                        else {
                            this.item_td.SelectData = [<option key={0} value={""}>#ERROR!#</option>];
                            this.ChangeStateFunc();
                        }
                    });
            }
        }
        else {
            isDisabled = true;
            JSXOptions = [<option key={0} value={""}>#ERROR! NOT SELECTED CLASS#</option>];
        }

        let SelectControl;

        if(isLoadOptions) {
            SelectControl = <select autoFocus={true} placeholder={"Select enum"} key={key} disabled={isDisabled} className={"kt_e_select kt_e_active_control"}
                                    value={value} onChange={(e) => { onChange(e) }} >
                {JSXOptions}
            </select>;
        }
        else {
            SelectControl = <input autoFocus={true} readOnly={true} className={"kt_e_loading_select_input kt_e_active_control"} value={"#Loading...#"}  />;
        }


        return SelectControl;
    };

    private object_control = (option_selected, isDisabled, key, onChange) => {
        let _options = [];
        let _options_selected = {value: option_selected.value, label: option_selected.label};
        let disabled = true;

        if(this.item_td.SelectData) {
            this.item_td.SelectData.forEach((item_o, key_o) => {
                _options.push({key:item_o.uid, value: item_o.uid, label: item_o.name});
                if(_options_selected.value == item_o.uid) {
                    _options_selected.label = item_o.name;
                }
            });
            if(_options_selected.value == "") {
                _options_selected.label = "not selected";
            }
            disabled = false;
        }
        else {
            _options_selected.label = "Loading...";
        }

        const promiseOptions = inputValue =>
            new Promise(resolve => {
                if(this.item_th?.data.LinkedClass || this.item_th.data?.attr?.dopSett.data_source) {
                    this.core.GetSelectData(inputValue,
                        (answer) => {
                            _options = [{key: "none", value: "", label: "not selected"}];

                            answer.data.forEach((item_o, key_o) => {
                                _options.push({key:item_o.uid, value: item_o.uid, label: item_o.name});
                            });

                            resolve(_options);

                            if(!this.item_td.SelectData) {
                                this.item_td.isLoading = false;
                                this.item_td.SelectData = answer.data;
                                this.ChangeStateFunc();
                            }
                        },
                        () => {
                            resolve([]);
                        });
                }
                else {
                    _options = [{key: "none", value: "", label: "not selected"}];
                    resolve(_options);

                    this.item_td.isLoading = false;
                    this.item_td.SelectData = [];
                    this.ChangeStateFunc();
                }
            });
        //defaultOptions


        return <React.Fragment>
            <input autoFocus={true} readOnly={true} className={"cool_t_focus_cell"} style={{position: "absolute", opacity:0}} />
            <AsyncSelect
                autoFocus={false}
            menuPortalTarget={document.querySelector("body")}
            cacheOptions
            defaultOptions
            options={_options}
            isDisabled={disabled}
            loadOptions={promiseOptions}
            placeholder={"Select enum"}
            onChange={(value) => {onChange(value); }}

            className={"kt_select_control"}
            classNamePrefix="react-select"

            value={_options_selected}
            setValue={(ValueType, ActionTypes) => {}}
            onKeyDown={(e)=>{
                if (e.key === 'Enter') {
                    //this.props.onClickSubmitBtn();
                }
            }}
        /></React.Fragment>;
    };


    private ckeditor_control = (value, isDisabled, key, onChange) => {
        /*
        *

       <CKEditor
                        disabled={true}
                        editor={ ClassicEditor }
                        data={value}
                        onInit={ editor => {
                            // You can store the "editor" and use when it is needed.
                            console.log( 'Editor is ready to use!', editor );
                        } }
                        onChange={ ( event, editor ) => {
                            const data = editor.getData();
                            console.log( { event, editor, data } );
                        } }
                        onBlur={ ( event, editor ) => {
                            console.log( 'Blur.', editor );
                        } }
                        onFocus={ ( event, editor ) => {
                            console.log( 'Focus.', editor );
                        } }
                    />


        *
        * */

        let f_value = value.replace(/<\/?[^>]+(>|$)/g, "");


        return (
            <div className={"zt_ck_co"} style={{width:"100%"}} key={key}>
                <div className={"zt_ck_c1"}>
                    <input autoFocus={true} placeholder={"CK_editor"} key={key} type="text" disabled={isDisabled}
                           readOnly={true}
                           value={f_value} title={f_value}
                           onClick={this.onClickEInput}
                           className={"kt_e_input kt_e_active_control"}
                    />

                </div>
                <div className={"zt_ck_c2"}>
                    <OverlayTrigger defaultShow={true} trigger="click" placement="auto" overlay={
                        <Popover id={`popover-array-${this.item_th.uid}-${this.item_td.o}-${key}`} className="zt_ck_popover">
                            <Popover.Title as="div" className={"cof_hdr"}>
                                <div className="cof_titlebar">
                                    <div className="cof_title">Edit {this.item_th.name}
                                    </div>
                                </div>
                                <div className="cof_hdr_btns_co">
                                    <button className="cof_hdr_btn" onClick={() =>{
                                        //Если найдете более элегантное решение, то будет хорошо, я не нашел!
                                        document.getElementById(`btn-close_popover-array-${this.item_th.uid}-${this.item_td.o}-${key}`).click();
                                    }}><i className="fas fa-times"/></button>
                                </div>
                            </Popover.Title>
                            <Popover.Content>
                                <div className="zt_ck_popover_body">
                                    <CKEditor
                                        disabled={isDisabled}
                                        editor={ ClassicEditor }
                                        data={value}
                                        onInit={ editor => {
                                            // You can store the "editor" and use when it is needed.
                                            console.log( 'Editor is ready to use!', editor );
                                        } }
                                        onChange={ ( event, editor ) => {
                                            const data = editor.getData();

                                            console.log( { event, editor, data } );
                                            onChange({target: {value: data}});
                                        } }
                                        onBlur={ ( event, editor ) => {
                                            console.log( 'Blur.', editor );
                                        } }
                                        onFocus={ ( event, editor ) => {
                                            console.log( 'Focus.', editor );
                                        } }
                                    />
                                </div>
                                <div className={"zt_ck_btns_co"}>
                                    <button  onClick={() =>{
                                        //Если найдете более элегантное решение, то будет хорошо, я не нашел!
                                        document.getElementById(`btn-close_popover-array-${this.item_th.uid}-${this.item_td.o}-${key}`).click();
                                    }} className="cof_ftr_btn nf_btn-ellipse">Close</button>
                                </div>
                            </Popover.Content>
                        </Popover>
                    }>
                        <button id={`btn-close_popover-array-${this.item_th.uid}-${this.item_td.o}-${key}`} className="zt_ck_btn_op btn btn-outline-secondary btn-sm" type="button"><i className="fas fa-ellipsis-v"/></button>
                    </OverlayTrigger>
                </div>
            </div>
        );
    };


    private events = {
        onDoubleClickFocusInput: (e) => {
            if(this.select_cell && this.select_cell.item_tr.trKey == this.item_tr.trKey && this.select_cell.item_td.key == this.item_td.key) {
                e.stopPropagation();
                e.preventDefault();

                let element = e.target;

                if (window.getSelection) {
                    window.getSelection().removeAllRanges();

                    // let element = document.getElementsByClassName('cool_t_focus_cell')[0] as HTMLInputElement;
                    let lengthV = element.value.length;
                    element.setSelectionRange(lengthV, lengthV);
                }

                this.select_cell.isEdit = true;
                this.select_cell.isNotMove = true;

                this.ChangeStateFunc();

                return false;
            }
        },
    };

//calc_triggers
    private core = {
        AJAX: {
            getRunTrigger: (param:{calc:string, trigger:"manual", obj:string, type:string, values:IValue[] }, SuccFunc: (res)=>void, ErrFunc: (res)=>void) => {
                ot_api
                    .getRunTrigger(param)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch( (error) => {
                        ErrFunc({result:0, errors: [{text:"Server Error"}]});
                    })
            },


            getFullValue: (obj:string, param:string, SuccFunc, ErrFunc) => {
                if(XHR_LOAD_VALUE.cancel) {
                    XHR_LOAD_VALUE.cancel();
                }
                else {
                    XHR_LOAD_VALUE.cancel = null;
                }

                ot_api
                    .getValueForParamObj({obj:obj, param:param}, XHR_LOAD_SELECT)
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (error) => {
                        console.log(error);
                        ErrFunc(error);
                    })


            },
        },

        GetSelectData: (filter:string, SuccFunc, ErrFunc) => {
            if(XHR_LOAD_SELECT.cancel) {
                XHR_LOAD_SELECT.cancel();
            }
            else {
                XHR_LOAD_SELECT.cancel = null;
            }

            if(this.item_th.data?.attr?.dopSett.data_source) {
                const SuccFuncList = (answer) => {
                    let data_format = [];
                    answer.data.forEach((item, key) => {
                        data_format.push({
                            uid: item.v,
                            name: item.v,
                        });
                    });

                    SuccFunc({data: data_format});
                };

                if(this.item_td.list != undefined) {
                    //То берём значение с этого листа
                    SuccFuncList({data:this.item_td.list});
                }
                else {

                    let values:IValue[] = this.helpers.generateDataForSave();

                    ot_api
                        .runCalcList({obj: this.item_td.o, type: this.item_th.data.class.uid, values:values, calc: this.item_th.data.attr.dopSett.data_source.uid}, XHR_LOAD_SELECT)
                        .then(res => {
                            console.error(res);
                            if(res.data.result == 1) {
                                SuccFuncList(res.data);
                            }
                            else {
                                ErrFunc();
                            }
                        })
                        .catch( (error) => {
                            console.log(error);
                            ErrFunc(error);
                        })
                }
            }
            else {
                ot_api
                    .getObjsByTypeForFilter({type:this.item_th.data.LinkedClass.uid, filter:filter}, XHR_LOAD_SELECT)
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            SuccFunc({data: res.data.data.data.objs});
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (error) => {
                        console.log(error);
                        ErrFunc(error);
                    })
            }
        },

        CalcGetResult: (uidObj:string, uidClass:string, uidCalc:string, SuccFunc: (res)=>void) => {
            ot_api
                .CalcGetResult(uidObj, uidClass, uidCalc)
                .then(res => {
                    console.error(res);
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                    }
                })
                .catch( (error) => {
                    console.log(error);
                })
        },
    };

    private helpers = {
        generateDataForSave: () => {
            let values:IValue[] = [];
            this.item_tr.tds.forEach( (item_td, index) => {
                let item_th = this.HeadData[index];

                let _item:IValue = {
                    value: item_td.v ? item_td.v : null,
                    attr: item_th?.uid,
                    key: item_th?.data?.attr?.isKey ? 1 : 0,
                    datatype: item_th?.data?.attr?.datatype,
                };
                values.push(_item);
            });

            return values;
        },
    }
}