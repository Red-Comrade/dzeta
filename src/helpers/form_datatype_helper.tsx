//Исторически так сложилось, что название такое
import * as React from "react";
import {format, parse} from "date-fns";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Overlay from 'react-bootstrap/Overlay'
import * as ot_api from "../api/ObjTree-api";
import {Children} from "react";
import RootStore from '../stores/RootStore'
import ToastrStore from '../stores/ToastrStore'
import {IValue} from "../api/ObjTree-api";

import InputColor from 'react-input-color';
import CKEditor from '@ckeditor/ckeditor5-react';
import Highlight from '@ckeditor/ckeditor5-highlight/src/highlight';

import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import BalloonEditor from '@ckeditor/ckeditor5-build-balloon';
import AsyncSelect from 'react-select/async';

import {CameraModule} from '../components/CameraModule'

export interface IFormDatatypeClass {
    item: any,
    key: string | number,
    attrDataCache: any,
    attrDataFileCache: any,
    attrData: any,
    type: "add" | "edit" | "read",
    ObjID: string;
    TypeID: string;
    ChangeStateFunc: () => void, // this.setState();
}

export class FormDatatypeClass {
    item: any;
    key: string | number;
    attrDataCache: any;
    attrDataFileCache: any;
    attrData: any;
    type: "add" | "edit" | "read";
    ObjID: string;
    TypeID: string;
    ChangeStateFunc: () => void;

    constructor(params:IFormDatatypeClass) {
        this.item = params.item;
        this.key = params.key;
        this.attrDataCache = params.attrDataCache;
        this.attrDataFileCache = params.attrDataFileCache;
        this.attrData = params.attrData;
        this.type = params.type;
        this.ChangeStateFunc = params.ChangeStateFunc;
        this.ObjID = params.ObjID;
        this.TypeID = params.TypeID;
    }

    public GetControl = () => {
        if(this.item?.dopSett?.data_source) {
            if(this.type == "read") {
                return this.r_object();
            }
            else {
                if (this.item.isArray) {
                    return this.array_object();
                }
                else {
                    return this.object();
                }
            }
        }
        else if(this.item.datatype == "number") {
            if(this.type == "read") {
                return this.r_number();
            }
            else {
                if (this.item.isArray) {
                    return this.array_number();
                }
                else {
                    return this.number();
                }
            }
        }
        else if(this.item.datatype == "text") {
            if(this.type == "read") {
                return this.r_text();
            }
            else {
                if (this.item.isArray) {
                    return this.array_text();
                }
                else {
                    return this.text();
                }
            }
        }
        else if(this.item.datatype == "datetime") {
            if(this.type == "read") {
                return this.r_datetime();
            }
            else {
                if (this.item.isArray) {
                    return this.array_datetime();
                }
                else {
                    return this.datetime();
                }
            }
        }
        else if(this.item.datatype == "object") {
            if(this.type == "read") {
                return this.r_object();
            } else {
                if (this.item.isArray) {
                    return this.array_object();
                }
                else {
                    return this.object();
                }
            }
        }
        else if(this.item.datatype == "file") {
            if(this.type == "read") {
                if (this.item.isArray) {
                    return this.r_array_file();
                } else {
                    return this.r_file();
                }
            }
            else {
                if (this.item.isArray) {
                    return this.array_file();
                } else {
                    return this.file();
                }
            }
        }
    };

    private text = () => {
        let value = !this.item.value || (!this.item.value.value && this.item.value.value!==0) ? "" : this.item.value.value;
        let isDisabled;
        let labels_btns = [];

        if(this.attrDataCache[this.item.uid] != undefined) {
            //Значит есть кэш
            value = this.attrDataCache[this.item.uid].value;
        }

        if(this.item.calc && this.type == "edit") {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }

        let Control;

        if(this.item?.TextType == 1) {
            Control = this.ckeditor_control(value, isDisabled, "one_control", (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                }
                this.attrDataCache[this.item.uid].value = t_value;
                this.ChangeStateFunc();
            });
        }
        else if(this.item?.TextType == 2) {
            Control = this.color_control(value, isDisabled, "one_control", (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                }
                this.attrDataCache[this.item.uid].value = t_value;
                this.ChangeStateFunc();
            });
        }
        else if(this.item?.TextType == 3) {
            Control = this.link_control(value, isDisabled, "one_control", (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                }
                this.attrDataCache[this.item.uid].value = t_value;
                this.ChangeStateFunc();
            });
        }
        else if(this.item?.TextType == 4) {

            if(value) {
                let btn_prev = <button type="button" title="Previews" onClick={() => {
                    RootStore.GalleryIsOpen = true;
                    RootStore.GalleryData.Images = [value];

                }} className="hover_label_btn">
                    <i className="fas fa-images"/></button>;

                labels_btns.push(btn_prev);
            }

            Control = this.image_link_control(value, isDisabled, "one_control", (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                }
                this.attrDataCache[this.item.uid].value = t_value;
                this.ChangeStateFunc();
            });
        }
        else {
            Control = this.text_control(value, isDisabled, "one_control", (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                }
                this.attrDataCache[this.item.uid].value = t_value;
                this.ChangeStateFunc();
            });
        }

        return <div className="form-group zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, labels_btns, value)}
            <div className="input-group">{Control}</div>
        </div>;
    };
    private number = () => {
        let value = !this.item.value || (!this.item.value.value && this.item.value.value!==0) ? "" : this.item.value.value;
        let isDisabled;
        let labels_btns = [];

        if(this.attrDataCache[this.item.uid] != undefined) {
            //Значит есть кэш
            value = this.attrDataCache[this.item.uid].value;
        }

        if(this.item.calc && this.type == "edit") {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }

        return <div className="form-group zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, labels_btns, value)}
            <div className="input-group">
                {
                    this.number_control(value, isDisabled, 'one_control', (e) => {
                        let t_value = e.target.value;
                        if(this.attrDataCache[this.item.uid] == undefined) {
                            this.attrDataCache[this.item.uid] = {};
                            this.attrDataCache[this.item.uid].data = this.item;
                        }
                        this.attrDataCache[this.item.uid].value = t_value;
                        this.ChangeStateFunc();
                    })
                }
            </div>
        </div>;
    };
    private datetime = () => {
        let value = !this.item.value || (!this.item.value.value && this.item.value.value!==0) ? "" : this.item.value.value;
        let labels_btns = [];
        let isDisabled;

        if(value) {
            value = parse(
                value,
                'dd/MM/yyyy HH:mm:ss',
                new Date()
            );
            //2010-04-12 00:00:00+02
        }
        if(this.attrDataCache[this.item.uid] != undefined) {
            //Значит есть кэш
            value = this.attrDataCache[this.item.uid].value;
            if(value) {
                value = parse(
                    value,
                    'dd/MM/yyyy HH:mm:ss',
                    new Date()
                );
            }
        }

        if(this.item.calc && this.type == "edit") {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }

        return <div className="form-group zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, labels_btns, value)}
            <div className="zt_input-group date_group">
                {
                    this.datetime_control(value, isDisabled, 'one_control',(date) => {
                        let t_value = date;
                        if(this.attrDataCache[this.item.uid] == undefined) {
                            this.attrDataCache[this.item.uid] = {};
                            this.attrDataCache[this.item.uid].data = this.item;
                        }
                        if(t_value) {
                            t_value = format(t_value, 'dd/MM/yyyy HH:mm:ss')
                        }
                        this.attrDataCache[this.item.uid].value = t_value;
                        this.ChangeStateFunc();
                    })
                }
            </div>
        </div>;
    };
    private object = () => {
        let value = !this.item.value || !this.item.value.uid ? "" : this.item.value.uid;
        if(!value) {
            //Если это перечисление из текста
            value = !this.item.value || !this.item.value.value ? "" : this.item.value.value;
        }
        let label = !this.item.label || !this.item.value.label ? "" : this.item.value.label;
        let isDisabled;
        let labels_btns = [];

        if(this.attrDataCache[this.item.uid] != undefined) {
            //Значит есть кэш
            value = this.attrDataCache[this.item.uid].value;
            label = this.attrDataCache[this.item.uid].label;
        }

        if(this.item.calc) {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }

        return <div className="zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, labels_btns, value)}
            <div className="zt_input-group">
                {
                    this.object_control({value: value, label: label }, isDisabled, 'one_control',  (option) => {
                        let t_value = option.value ? option.value : "";
                        let t_label = option.label ? option.label : "";


                        if(this.attrDataCache[this.item.uid] == undefined) {
                            this.attrDataCache[this.item.uid] = {};
                            this.attrDataCache[this.item.uid].data = this.item;
                        }
                        this.attrDataCache[this.item.uid].value = t_value;
                        this.attrDataCache[this.item.uid].value_label = t_label;
                        this.ChangeStateFunc();
                    })
                }
            </div>
        </div>;
    };
    private file = () => {
        let value = !this.item.value ? "" : this.item.value;
        let name = this.item.name ? this.item.name : "#NOT NAME#";

        let value_title = "";
        let Previews = [];

        if(this.attrDataCache[this.item.uid] != undefined) {
            //Значит есть кэш/ Кэш нужен только, чтобы показать, что мы удалили файль
            value_title = this.attrDataCache[this.item.uid].value;
        }
        else if(this.item.value) {
            value_title = this.item.value.name;
            if( this.item.value.preview ) {
                Previews = ['/Object/getPreviewFile/' + this.item.value.preview];
            }
        }

        let btn_prev;
        if(Previews.length > 0) {
            btn_prev = <button disabled={Previews.length == 0} type="button" title="Previews" onClick={() => {
                if(Previews.length > 0) {
                    RootStore.GalleryIsOpen = true;
                    RootStore.GalleryData.Images = Previews;
                }
                else {
                    ToastrStore.info('Not previews!');
                }
            }} className="hover_label_btn">
                <i className="fas fa-images"/></button>;
        }

        let _ref_file;


        if(this.attrDataFileCache[this.item.uid]) {
            if(this.attrDataFileCache[this.item.uid].files[0] != undefined) {
                value_title = this.attrDataFileCache[this.item.uid].files[0].name;
            }
        }
        let CameraModuleComp=null;
        if(this.item.ShowCameraModule==1){
          CameraModuleComp = <CameraModule onSave={(file)=>{
                this.attrDataFileCache[this.item.uid] = {};
                this.attrDataFileCache[this.item.uid].files = [file];
                this.attrDataFileCache[this.item.uid].data = this.item;
                this.ChangeStateFunc();
            }} onClose={()=>{}} />
        }

        return <div className="form-group zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(name, btn_prev, value)}
            <div className="zt_input-group">
                <div className="form-control file_control">
                    <span className="file_span" title={value_title} ><i className="fas fa-file-alt"/>{value_title}</span>
                </div>
                <input ref={ref=>(_ref_file = ref)} className="file_input" type="file"  onChange={(e)=>{
                    this.attrDataFileCache[this.item.uid] = {};
                    this.attrDataFileCache[this.item.uid].files = e.target.files;
                    this.attrDataFileCache[this.item.uid].data = this.item;
                    this.ChangeStateFunc();
                }}  />
                <div className="file_btn_control">
                    <button type="button" title="upload" className="file_btns nf_btn-ellipse"
                            onClick={(e)=>{
                                _ref_file.click();
                            }}
                    ><i className="fas fa-file-upload"/></button>
                    <button type="button" title="download" className="file_btns nf_btn-ellipse"
                            onClick={() => {
                                window.open('/Object/downloadFile/?file=' + this.item.value.uid, '_blank');
                            }}
                    ><i className="fas fa-file-download"/></button>
                    {CameraModuleComp}
                    <button type="button" title="remove" className="file_btns nf_btn-ellipse"  onClick={(e) => {

                        //Добавляем к кэш пустое значения параметра файла
                        if(this.attrDataCache[this.item.uid] == undefined) {
                            this.attrDataCache[this.item.uid] = {};
                            this.attrDataCache[this.item.uid].data = this.item;
                        }
                        this.attrDataCache[this.item.uid].value = '';
                        //
                        this.attrDataFileCache[this.item.uid] = {};
                        this.attrDataFileCache[this.item.uid].files = [];
                        this.attrDataFileCache[this.item.uid].data = this.item;

                        _ref_file.value = "";
                        this.ChangeStateFunc();
                    }} ><i className="fas fa-times"/></button>
                </div>
            </div>
        </div>;
    };

    private r_text = () => {
        let value;
        if (this.item.isArray) {
            value = !this.item.value ? [] : this.item.value;
            value = value.map((v) => {
                if (v && v.value) {
                    return v.value;
                }
            }).join(', ');
        } else {
            value = !this.item.value || (!this.item.value.value && this.item.value.value!==0) ? "" : this.item.value.value;
        }

        if(this.item?.TextType == 1) {
            value = this.ckeditor_control(value, true, "one_control", (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                }
                this.attrDataCache[this.item.uid].value = t_value;
                this.ChangeStateFunc();
            });
        }

        return (
            <div className="form-group zt_form_control" key={this.key}>
                {this.helpers.r_hoverLabel(this.item.name, [])}
                <div className="input-group">
                    {value}
                </div>
            </div>
        );
    };
    private r_number = () => {
        let value;
        if (this.item.isArray) {
            value = !this.item.value ? [] : this.item.value;
            value = value.map((v) => {
                if (v && v.value) {
                    return v.value;
                }
            }).join(', ');
        } else {
            value = !this.item.value || (!this.item.value.value && this.item.value.value!==0) ? "" : this.item.value.value;
        }
        return (
            <div className="form-group zt_form_control" key={this.key}>
                {this.helpers.r_hoverLabel(this.item.name, [])}
                <div className="input-group">
                    <div>{value}</div>
                </div>
            </div>
        );
    };
    private r_datetime = () => {
        let value;
        if (this.item.isArray) {
            value = !this.item.value ? [] : this.item.value;
            value = value.map((v) => {
                if (v && v.value) {
                    return v.value;
                }
            }).join(', ');
        } else {
            value = !this.item.value || (!this.item.value.value && this.item.value.value!==0) ? "" : this.item.value.value;
        }
        return (
            <div className="form-group zt_form_control" key={this.key}>
                {this.helpers.r_hoverLabel(this.item.name, [])}
                <div className="input-group">
                    <div>{value}</div>
                </div>
            </div>
        );
    };
    private r_object = () => {
        let value;
        if (this.item.isArray) {
            value = !this.item.value ? [] : this.item.value;
            value = value.map((v) => {
                if (v && v.name) {
                    return v.name;
                }
            }).join(', ');
        } else {
            value = !this.item.value || !this.item.value.name ? "" : this.item.value.name;
        }
        return (
            <div className="form-group zt_form_control" key={this.key}>
                {this.helpers.r_hoverLabel(this.item.name, [])}
                <div className="input-group">
                    <div>{value}</div>
                </div>
            </div>
        );
    };
    private r_file = () => {
        let value = !this.item.value ? "" : this.item.value;
        let name = this.item.name ? this.item.name : "#NOT NAME#";

        let value_title = "";
        let Previews = [];
        if(this.item.value) {
            value_title = this.item.value.name;
            if( this.item.value.preview ) {
                Previews = ['/Object/getPreviewFile/' + this.item.value.preview];
            }
        }
        let btn_prev;
        if(Previews.length > 0) {
            btn_prev = <button disabled={Previews.length == 0} type="button" title="Previews" onClick={() => {
                if(Previews.length > 0) {
                    RootStore.GalleryIsOpen = true;
                    RootStore.GalleryData.Images = Previews;
                }
                else {
                    ToastrStore.info('Not previews!');
                }
            }} className="hover_label_btn">
                <i className="fas fa-images"/></button>;
        }

        let _ref_file;

        return (
            <div className="form-group zt_form_control" key={this.key}>
                {this.helpers.r_hoverLabel(name, btn_prev)}
                <div className="zt_input-group">
                    <div className="form-control file_control">
                        <span className="file_span" title={value_title} ><i className="fas fa-file-alt"/>{value_title}</span>
                    </div>
                    <input ref={ref=>(_ref_file = ref)} className="file_input" type="file"  />
                    <div className="file_btn_control">
                        <button type="button" title="download" className="file_btns nf_btn-ellipse"
                                onClick={() => {
                                    window.open('/Object/downloadFile/?file=' + this.item.value.uid, '_blank');
                                }}
                        ><i className="fas fa-file-download"/></button>
                    </div>
                </div>
            </div>
        );
    };
    private r_array_file = () => {
        let value = !this.item.value ? [] : this.item.value;
        let name = this.item.name ? this.item.name : "#NOT NAME#";

        let value_title = [];
        let Previews = value.map((v) => {
            value_title.push(v.name);
            if (v.preview) {
                return '/Object/getPreviewFile/' + v.preview;
            }
        });
        let btn_prev;
        if(Previews.length > 0) {
            btn_prev = <button disabled={Previews.length == 0} type="button" title="Previews" onClick={() => {
                if(Previews.length > 0) {
                    RootStore.GalleryIsOpen = true;
                    RootStore.GalleryData.Images = Previews;
                }
                else {
                    ToastrStore.info('Not previews!');
                }
            }} className="hover_label_btn">
                <i className="fas fa-images"/></button>;
        }

        let _ref_file;

        return (
            <div className="form-group zt_form_control" key={this.key}>
                {this.helpers.r_hoverLabel(name, btn_prev)}
                <div className="zt_input-group">
                    <div className="form-control file_control">
                        <span className="file_span" title={value_title.join(', ')} ><i className="fas fa-file-alt"/>{value_title.join(', ')}</span>
                    </div>
                    <input ref={ref=>(_ref_file = ref)} className="file_input" type="file"  />
                    <div className="file_btn_control">
                        <button type="button" title="download" className="file_btns nf_btn-ellipse"
                                onClick={() => {
                                    value.forEach((v) => {
                                        window.open('/Object/downloadFile/?file=' + v.uid, '_blank');
                                    })
                                }}
                        ><i className="fas fa-file-download"/></button>
                    </div>
                </div>
            </div>
        );
    };

    private array_text = () => {
        let value = !this.item.value ? [] : this.item.value;
        let isDisabled;
        let labels_btns = [];

        if(this.attrDataCache[this.item.uid] && this.attrDataCache[this.item.uid].value) {
            //Значит есть кэш
            this.attrDataCache[this.item.uid].value.forEach((v, i) => {
                if (!v.remove) {
                    value[i] = v;
                } else {
                    delete this.attrDataCache[this.item.uid].value[i];
                    delete value[i];
                }
            });
        }

        if(this.item.calc && this.type == "edit") {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }
        let viewValues = [];

        let controls = !Array.isArray(value) ? [] : value.map((v, i) => {
            let _value = !v || (!v.value && v.value!==0) ? "" : v.value;
            viewValues.push(_value);
            return this.text_control(_value, isDisabled, i, (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                    this.attrDataCache[this.item.uid].value = value;
                }
                if (t_value != '') {
                    this.attrDataCache[this.item.uid].value[i] = { value: t_value };
                } else {
                    this.attrDataCache[this.item.uid].value[i] = { remove: true };
                }
                this.ChangeStateFunc();
            })
        });
        controls.push(this.text_control('', isDisabled, controls.length, (e) => {
            let t_value = e.target.value;
            if(this.attrDataCache[this.item.uid] == undefined) {
                this.attrDataCache[this.item.uid] = {};
                this.attrDataCache[this.item.uid].data = this.item;
                this.attrDataCache[this.item.uid].value = value;
            }
            const index = value.length === 0 ? 0 : value.length;
            this.attrDataCache[this.item.uid].value[index] = { value: t_value };
            this.ChangeStateFunc();
        }));

        return <div className="form-group zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, labels_btns, value)}
            <div className="input-group">
                <input type="text" disabled={true} value={viewValues.join(', ')} title={viewValues.join(', ')} className="form-control" />
                {this.helpers.arrayBtn(controls, false)}
            </div>
        </div>;
    }
    private array_number = () => {
        let value = !this.item.value ? [] : this.item.value;
        let isDisabled;
        let labels_btns = [];

        if(this.attrDataCache[this.item.uid] && this.attrDataCache[this.item.uid].value) {
            //Значит есть кэш
            this.attrDataCache[this.item.uid].value.forEach((v, i) => {
                if (!v.remove) {
                    value[i] = v;
                } else {
                    delete this.attrDataCache[this.item.uid].value[i];
                    delete value[i];
                }
            });
        }

        if(this.item.calc && this.type == "edit") {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }
        let viewValues = [];

        let controls = !Array.isArray(value) ? [] : value.map((v, i) => {
            let _value = !v || (!v.value && v.value!==0) ? "" : v.value;
            viewValues.push(_value);
            return this.number_control(_value, isDisabled, i, (e) => {
                let t_value = e.target.value;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                    this.attrDataCache[this.item.uid].value = value;
                }
                if (t_value != '') {
                    this.attrDataCache[this.item.uid].value[i] = { value: t_value };
                } else {
                    this.attrDataCache[this.item.uid].value[i] = { remove: true };
                }
                this.ChangeStateFunc();
            })
        });
        controls.push(this.number_control('', isDisabled, 'one_control',(e) => {
            let t_value = e.target.value;
            if(this.attrDataCache[this.item.uid] == undefined) {
                this.attrDataCache[this.item.uid] = {};
                this.attrDataCache[this.item.uid].data = this.item;
                this.attrDataCache[this.item.uid].value = value;
            }
            const index = value.length === 0 ? 0 : value.length;
            this.attrDataCache[this.item.uid].value[index] = { value: t_value };
            this.ChangeStateFunc();
        }))

        return <div className="form-group zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, labels_btns, value)}
            <div className="input-group">
                <input type="text" disabled={true} value={viewValues.join(', ')} title={viewValues.join(', ')} className="form-control" />
                {this.helpers.arrayBtn(controls, false)}
            </div>
        </div>;
    }
    private array_datetime = () => {
        let value = !this.item.value ? [] : this.item.value;
        let isDisabled;
        let labels_btns = [];

        if(this.attrDataCache[this.item.uid] && this.attrDataCache[this.item.uid].value) {
            //Значит есть кэш
            this.attrDataCache[this.item.uid].value.forEach((v, i) => {
                if (!v.remove) {
                    value[i] = v;
                } else {
                    delete this.attrDataCache[this.item.uid].value[i];
                    delete value[i];
                }
            });
        }

        if(this.item.calc && this.type == "edit") {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }
        let viewValues = [];

        let controls = !Array.isArray(value) ? [] : value.map((v, i) => {
            let _value = !value || (!v.value && v.value!==0) ? "" : v.value;
            viewValues.push(_value);
            if(_value) {
                _value = parse(
                    _value,
                    'dd/MM/yyyy HH:mm:ss',
                    new Date()
                );
                //2010-04-12 00:00:00+02
            }
            return this.datetime_control(_value, isDisabled, i, (date) => {
                let t_value = date;
                if(this.attrDataCache[this.item.uid] == undefined) {
                    this.attrDataCache[this.item.uid] = {};
                    this.attrDataCache[this.item.uid].data = this.item;
                    this.attrDataCache[this.item.uid].value = value;
                }
                if(t_value) {
                    t_value = format(t_value, 'dd/MM/yyyy HH:mm:ss')
                    this.attrDataCache[this.item.uid].value[i] = { value: t_value };
                } else {
                    this.attrDataCache[this.item.uid].value[i] = { remove: true };
                }
                this.ChangeStateFunc();
            })
        });
        controls.push(this.datetime_control('', isDisabled, 'one_control', (date) => {
            let t_value = date;
            if(this.attrDataCache[this.item.uid] == undefined) {
                this.attrDataCache[this.item.uid] = {};
                this.attrDataCache[this.item.uid].data = this.item;
                this.attrDataCache[this.item.uid].value = value;
            }
            const index = value.length === 0 ? 0 : value.length;
            if(t_value) {
                t_value = format(t_value, 'dd/MM/yyyy HH:mm:ss')
            }
            this.attrDataCache[this.item.uid].value[index] = { value: t_value };
            this.ChangeStateFunc();
        }))

        return <div className="form-group zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, [], value)}
            <div className="zt_input-group date_group">
                <div className="input-group">
                    <input type="text" disabled={true} value={viewValues.join(', ')} title={viewValues.join(', ')} className="form-control" />
                    {this.helpers.arrayBtn(controls, false)}
                </div>
            </div>
        </div>;
    }
    private array_object = () => {
        let value = !this.item.value ? [] : this.item.value;
        let isDisabled;
        let labels_btns = [];

        value = value.map((v) => {
            return { value: v.uid, name: v.name }
        });

        if(this.attrDataCache[this.item.uid] != undefined) {
            //Значит есть кэш
            this.attrDataCache[this.item.uid].value.forEach((v, i) => {
                if (!v.remove) {
                    value[i] = v;
                } else {
                    delete this.attrDataCache[this.item.uid].value[i];
                    if(value[i] != undefined) {
                        delete value[i];
                    }
                    if(this.item?.value && this.item?.value[i] != undefined) {
                        delete this.item.value[i];
                    }
                }
            });
        }

        if(this.item.calc) {
            isDisabled = true;
            labels_btns.push(this.helpers.calcBtn());
        }
        if(this.item.calc_triggers.manual) {
            labels_btns.push(this.helpers.calcManualBtn());
        }
        let viewValues = [];

        let is_loading_data = true;

        let controls = [];

        if(this.item.SelectData && !this.item.isLoading) {
            is_loading_data = false;
            controls = value.map((v, i) => {
                let _value = !v || !v.value ? "" : v.value;
                let _label = !v || !v.name ? "" : v.name;
                viewValues.push(!v || !v.name ? "" : v.name);
                return this.object_control_arr({value:_value, label:_label}, isDisabled, i, (option_selected) => {
                    let t_value = option_selected.value ? option_selected.value : "";
                    let t_label = option_selected.label ? option_selected.label : "";

                    if(this.attrDataCache[this.item.uid] == undefined) {
                        this.attrDataCache[this.item.uid] = {};
                        this.attrDataCache[this.item.uid].data = this.item;
                        this.attrDataCache[this.item.uid].value = value;
                    }
                    if (t_value != '') {
                        this.attrDataCache[this.item.uid].value[i] = { value: t_value, name: t_label };
                    } else {
                        this.attrDataCache[this.item.uid].value[i] = { remove: true };
                    }
                    this.ChangeStateFunc();
                });
            });
            controls.push(this.object_control_arr({value: "", label: "not selected"}, isDisabled, 'one_control', (option_selected) => {
                let t_value = option_selected.value ? option_selected.value : "";
                let t_label = option_selected.label ? option_selected.label : "";

                if (t_value != '') {
                    if(this.attrDataCache[this.item.uid] == undefined) {
                        this.attrDataCache[this.item.uid] = {};
                        this.attrDataCache[this.item.uid].data = this.item;
                        this.attrDataCache[this.item.uid].value = value;
                    }
                    const index = value.length === 0 ? 0 : value.length;
                    this.attrDataCache[this.item.uid].value[index] = { value: t_value, name: t_label };
                    this.ChangeStateFunc();
                }
            }));
        }
        else if(!this.item.isLoading){
            this.item.isLoading = true;
            if(this.item?.LinkType?.uid) {
                this.core.GetSelectData("",
                    (answer) => {
                        this.item.isLoading = false;
                        this.item.SelectData = answer.data;
                        this.ChangeStateFunc();
                    },
                    () => {
                        this.item.isLoading = false;
                        this.item.SelectData = [<option key={0} value={""}>#ERROR!#</option>];
                        this.ChangeStateFunc();
                    });
            }
            else {
                this.item.isLoading = false;
                this.item.SelectData = [<option key={0} value={""}>#ERROR Not LinkType!#</option>];
                this.ChangeStateFunc();
            }

        }

        return <div className="zt_form_control" key={this.key}>
            {this.helpers.hoverLabel(this.item.name, labels_btns, value)}
            <div className="zt_input-group">
                <div className="input-group">
                    <input type="text" disabled={true} value={viewValues.join(', ')} title={viewValues.join(', ')} className="form-control" />
                    {this.helpers.arrayBtn(controls, is_loading_data)}
                </div>
            </div>
        </div>;
    }
    private array_file = () => {
        let value = !this.item.value ? [] : this.item.value;
        let name = this.item.name ? this.item.name : "#NOT NAME#";

        let value_title = [];
        let Previews = value.map((v) => {
            value_title.push(v.name);
            if (v.preview) {
                return '/Object/getPreviewFile/' + v.preview;
            }
        });
        let btn_prev;
        if(Previews.length > 0) {
            btn_prev = <button disabled={Previews.length == 0} type="button" title="Previews" onClick={() => {
                if(Previews.length > 0) {
                    RootStore.GalleryIsOpen = true;
                    RootStore.GalleryData.Images = Previews;
                }
                else {
                    ToastrStore.info('Not previews!');
                }
            }} className="hover_label_btn">
                <i className="fas fa-images"/></button>;
        }

        let _ref_file;


        if(this.attrDataFileCache[this.item.uid]) {
            if(this.attrDataFileCache[this.item.uid].files != undefined) {
                for (let i = 0; i < this.attrDataFileCache[this.item.uid].files.length; i++) {
                    value_title.push(this.attrDataFileCache[this.item.uid].files[i].name);
                }
            }
        }

        return (
            <div className="form-group zt_form_control" key={this.key}>
                {this.helpers.hoverLabel(name, btn_prev, value)}
                <div className="zt_input-group">
                    <div className="form-control file_control">
                        <span className="file_span" title={value_title.join(', ')} ><i className="fas fa-file-alt"/>{value_title.join(', ')}</span>
                    </div>
                    <input ref={ref=>(_ref_file = ref)} multiple className="file_input" type="file"  onChange={(e)=>{
                        this.attrDataFileCache[this.item.uid] = {};
                        this.attrDataFileCache[this.item.uid].files = e.target.files;
                        this.attrDataFileCache[this.item.uid].data = this.item;
                        this.ChangeStateFunc();
                    }}  />
                    <div className="file_btn_control">
                        <button type="button" title="upload" className="file_btns nf_btn-ellipse"
                                onClick={(e)=>{
                                    _ref_file.click();
                                }}
                        ><i className="fas fa-file-upload"/></button>
                        <button type="button" title="download" className="file_btns nf_btn-ellipse"
                                onClick={() => {
                                    value.forEach((v) => {
                                        window.open('/Object/downloadFile/?file=' + v.uid, '_blank');
                                    })
                                }}
                        ><i className="fas fa-file-download"/></button>
                        <button type="button" title="remove" className="file_btns nf_btn-ellipse"><i className="fas fa-times"/></button>
                    </div>
                </div>
            </div>
        );
    }


    private object_control_arr = (option_selected, isDisabled, key, onChange) => {
        let _options = [];
        let _options_selected = {value: option_selected.value, label: option_selected.label};
        let disabled = true;

        if(this.item.SelectData) {
            this.item.SelectData.forEach((item_o, key_o) => {
                _options.push({key:item_o.uid, value: item_o.uid, label: item_o.name});
                if(_options_selected.value == item_o.uid) {
                    _options_selected.label = item_o.name;
                }
            });
            if(_options_selected.value == "") {
                _options_selected.label = "not selected";
            }
            disabled = false;
        }
        else {
            _options_selected.label = "Loading...";
        }

        const promiseOptions = inputValue =>
            new Promise(resolve => {
                if(!inputValue) {
                    let _options = [{key: "none", value: "", label: "not selected"}];
                    this.item.SelectData.forEach((item_o, key_o) => {
                        _options.push({key:item_o.uid, value: item_o.uid, label: item_o.name});
                    });
                    resolve(_options);
                }
                else {
                    this.core.GetSelectData(inputValue,
                        (answer) => {
                            let _options = [{key: "none", value: "", label: "not selected"}];

                            answer.data.forEach((item_o, key_o) => {
                                _options.push({key:item_o.uid, value: item_o.uid, label: item_o.name});
                            });
                            resolve(_options);
                        },
                        () => {
                            resolve([]);
                        });
                }
            });
        //defaultOptions

        //При закрытии селекта не нужно закрывать попувер

        return <AsyncSelect
            menuPortalTarget={document.querySelector(`body`)}
            styles={{ menuPortal: base => ({ ...base, zIndex: 2000 }) }}
            cacheOptions
            defaultOptions
            options={_options}
            isDisabled={disabled}
            //1061
            loadOptions={promiseOptions}
            placeholder={"Select enum"}
            onChange={(value) => {onChange(value); }}

            className={"zt_select_control " +(this.item.require?"zt_require_input":"")}
            classNamePrefix="react-select"

            value={_options_selected}
            setValue={(ValueType, ActionTypes) => {}}
            onKeyDown={(e)=>{
                if (e.key === 'Enter') {
                    //this.props.onClickSubmitBtn();
                }
            }}
        />;
    };
    private text_control = (value, isDisabled, key, onChange) => {
        return (
            <input placeholder={"Enter text"} key={key} type="text" disabled={isDisabled}
                   value={value} title={value} onChange={(e) => { onChange(e); }}
                   className={"form-control " +(this.item.require?"zt_require_input":"")}/>
        );
    };
    private number_control = (value, isDisabled, key, onChange) => {
        return (
            <input placeholder={"Enter number"} key={key} type="number" disabled={isDisabled}
                   value={value} title={value} onChange={(e) => { onChange(e); }}
                   className={"form-control " +(this.item.require?"zt_require_input":"")}/>
        );
    };
    private datetime_control = (value, isDisabled, key, onChange) => {
        return (
            <DatePicker key={key}
                className={"date_control form-control " +(this.item.require?"zt_require_input":"")}
                timeFormat="HH:mm"
                timeIntervals={15}
                timeCaption="time"
                dateFormat="dd/MM/yyyy HH:mm"
                isClearable
                placeholderText={"Enter date"}
                showTimeInput
                timeInputLabel="Time:"
                selected={value}
                title={value}
                onChange={(date) => {
                    onChange(date);
                }}
            />
        );
    };
    private object_control = (option_selected, isDisabled, key, onChange) => {
        let _options = [];
        let _options_selected = {value: option_selected.value, label: option_selected.label};
        let disabled = true;

        if(this.item.SelectData) {
            this.item.SelectData.forEach((item_o, key_o) => {
                _options.push({key:item_o.uid, value: item_o.uid, label: item_o.name});
                if(_options_selected.value == item_o.uid) {
                    _options_selected.label = item_o.name;
                }
            });
            if(_options_selected.value == "") {
                _options_selected.label = "not selected";
            }
            disabled = false;
        }
        else {
            _options_selected.label = "Loading...";
        }

        const promiseOptions = inputValue =>
            new Promise(resolve => {
                if(this.item?.LinkType?.uid || this.item?.dopSett?.data_source) {
                    this.core.GetSelectData(inputValue,
                        (answer) => {
                            _options = [{key: "none", value: "", label: "not selected"}];
                            answer.data.forEach((item_o, key_o) => {
                                _options.push({key:item_o.uid, value: item_o.uid, label: item_o.name});
                            });
                            resolve(_options);
                            if(!this.item.SelectData) {
                                this.item.isLoading = false;
                                this.item.SelectData = answer.data;
                                this.ChangeStateFunc();
                            }
                        },
                        () => {
                            resolve([]);
                        });
                }
                else {
                    _options = [{key: "none", value: "", label: "not selected"}];
                    resolve(_options);

                    this.item.isLoading = false;
                    this.item.SelectData = [];
                    this.ChangeStateFunc();
                }
            });
        //defaultOptions


        return <AsyncSelect
                menuPortalTarget={document.querySelector("body")}
                cacheOptions
                defaultOptions
                options={_options}
                isDisabled={disabled}

                loadOptions={promiseOptions}
                placeholder={"Select enum"}
                onChange={(value) => {onChange(value); }}

                className={"zt_select_control " +(this.item.require?"zt_require_input":"")}
                classNamePrefix="react-select"

                value={_options_selected}
                setValue={(ValueType, ActionTypes) => {}}
                onKeyDown={(e)=>{
                    if (e.key === 'Enter') {
                        //this.props.onClickSubmitBtn();
                    }
                }}
            />;
    };

    private ckeditor_control = (value, isDisabled, key, onChange) => {
        return (
            <div className={"zt_ck_co"} style={{width:"100%"}} key={key}>
                <div className={"zt_ck_c1"}>
                    <CKEditor
                        disabled={true}
                        editor={ ClassicEditor }
                        data={value}
                        onInit={ editor => {
                            // You can store the "editor" and use when it is needed.
                            console.log( 'Editor is ready to use!', editor );
                        } }
                        onChange={ ( event, editor ) => {
                            const data = editor.getData();
                            console.log( { event, editor, data } );
                        } }
                        onBlur={ ( event, editor ) => {
                            console.log( 'Blur.', editor );
                        } }
                        onFocus={ ( event, editor ) => {
                            console.log( 'Focus.', editor );
                        } }
                    />
                </div>
                <div className={"zt_ck_c2"}>
                    <OverlayTrigger trigger="click" placement="auto" overlay={
                        <Popover id={`popover-array-${this.item.uid}-${this.ObjID}-${this.key}`} className="zt_ck_popover">
                            <Popover.Title as="div" className={"cof_hdr"}>
                                <div className="cof_titlebar">
                                    <div className="cof_title">Edit {this.item.name}
                                    </div>
                                </div>
                                <div className="cof_hdr_btns_co">
                                    <button className="cof_hdr_btn" onClick={() =>{
                                        //Если найдете более элегантное решение, то будет хорошо, я не нашел!
                                        document.getElementById(`btn-close_popover-array-${this.item.uid}-${this.ObjID}-${this.key}`).click();
                                    }}><i className="fas fa-times"/></button>
                                </div>
                            </Popover.Title>
                            <Popover.Content>
                                <div className="zt_ck_popover_body">
                                    <CKEditor
                                        disabled={isDisabled}
                                        editor={ ClassicEditor }
                                        data={value}
                                        onInit={ editor => {
                                            // You can store the "editor" and use when it is needed.
                                            console.log( 'Editor is ready to use!', editor );
                                        } }
                                        onChange={ ( event, editor ) => {
                                            const data = editor.getData();

                                            console.log( { event, editor, data } );
                                            onChange({target: {value: data}});
                                        } }
                                        onBlur={ ( event, editor ) => {
                                            console.log( 'Blur.', editor );
                                        } }
                                        onFocus={ ( event, editor ) => {
                                            console.log( 'Focus.', editor );
                                        } }
                                    />
                                </div>
                                <div className={"zt_ck_btns_co"}>
                                    <button  onClick={() =>{
                                        //Если найдете более элегантное решение, то будет хорошо, я не нашел!
                                        document.getElementById(`btn-close_popover-array-${this.item.uid}-${this.ObjID}-${this.key}`).click();
                                    }} className="cof_ftr_btn nf_btn-ellipse">Close</button>
                                </div>
                            </Popover.Content>
                        </Popover>
                    }>
                        <button id={`btn-close_popover-array-${this.item.uid}-${this.ObjID}-${this.key}`} className="zt_ck_btn_op btn btn-outline-secondary btn-sm" type="button"><i className="fas fa-ellipsis-v"/></button>
                    </OverlayTrigger>
                </div>
            </div>
        );
    };
    private color_control = (value, isDisabled, key, onChange) => {
        let valid_value = '#ffffff';
        if(/^#[0-9A-F]{6}$/i.test(value)) {
            valid_value = value;
        }
        return (
            <div className={"zt-color-pick-co"}>
                <div className={'zt-color-pick-c1'}>
                    <input type={"text"} value={value} onChange={onChange} className={"form-control zt-color-pick-inp"} />
                </div>
                <div className={'zt-color-pick-c2'}>
                    <InputColor initialValue={valid_value} onChange={(setColor)=>{
                        onChange({target:{value: setColor.hex}})
                    }} />
                </div>
            </div>
        );
    };
    private link_control = (value, isDisabled, key, onChange) => {
        let control;
        if(value) {
            control = <a href={value} target={"_blank"} className={"zt-link-a"} >{value}</a>;
        }
        else {
            control = <button type={"button"} className={"zt-link-a"} onClick={() =>{
                //Если найдете более элегантное решение, то будет хорошо, я не нашел!
                document.getElementById(`popover-button-${this.item.uid}-${this.ObjID}-${this.key}`).click();
            }} > </button>;
        }

        return (
            <React.Fragment key={key}>
                {control}
                <div className="input-group-append">
                    <OverlayTrigger trigger="click" rootClose placement="auto" overlay={
                        <Popover id={`popover-array-${this.item.uid}-${this.ObjID}-${this.key}`} className="zt_array_popover">
                            <Popover.Title as="h3">Edit {this.item.name}</Popover.Title>
                            <Popover.Content>
                                <div className="zt_array_popover_body">
                                    <input className={"form-control"} value={value} onChange={onChange} />
                                </div>
                            </Popover.Content>
                        </Popover>
                    }>
                        <button id={`popover-button-${this.item.uid}-${this.ObjID}-${this.key}`} className="btn btn-outline-secondary btn-sm" type="button">
                            <i className="fas fa-pen"/>
                        </button>
                    </OverlayTrigger>
                </div>
            </React.Fragment>
        );
    };
    private image_link_control = (value, isDisabled, key, onChange) => {
        return (
            <input placeholder={"Enter text"} key={key} type="text" disabled={isDisabled}
                   value={value} title={value} onChange={(e) => { onChange(e); }}
                   className={"form-control " +(this.item.require?"zt_require_input":"")}/>
        );
    };

//calc_triggers
    private core = {
        AJAX: {
            getRunTrigger: (param:{calc:string, trigger:"manual", obj:string, type:string, values:IValue[] }, SuccFunc: (res)=>void, ErrFunc: (res)=>void) => {
                ot_api
                    .getRunTrigger(param)
                    .then(res => {
                        if(res.data.result == 1) {
                            SuccFunc(res.data);
                        }
                        else {
                            ErrFunc(res.data);
                        }
                    })
                    .catch( (error) => {
                        ErrFunc({result:0, errors: [{text:"Server Error"}]});
                    })
            },
        },

        GetSelectData: (filter:string, SuccFunc, ErrFunc) => {
            if(this.item?.dopSett?.data_source) {
                const SuccFuncList = (answer) => {
                    let data_format = [];
                    answer.data.forEach((item, key) => {
                        data_format.push({
                            uid: item.v,
                            name: item.v,
                        });
                    });

                    SuccFunc({data: data_format});
                };
                let values:IValue[] = this.helpers.generateDataForSave();
                ot_api
                    .runCalcList({obj: this.ObjID, type: this.TypeID, values: values, calc: this.item.dopSett.data_source.uid})
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            SuccFuncList(res.data);
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (error) => {
                        console.log(error);
                        ErrFunc(error);
                    })
            }
            else {
                ot_api
                    .getObjsByTypeForFilter({type:this.item.LinkType.uid, filter:filter})
                    .then(res => {
                        console.error(res);
                        if(res.data.result == 1) {
                            let res_data = res.data?.data?.data?.objs ? res.data.data.data.objs : [];
                            SuccFunc({data: res_data});
                        }
                        else {
                            ErrFunc();
                        }
                    })
                    .catch( (error) => {
                        console.log(error);
                        ErrFunc();
                    })
            }
        },
        CalcGetResult: (uidObj:string, uidClass:string, uidCalc:string, SuccFunc: (res)=>void) => {
            ot_api
                .CalcGetResult(uidObj, uidClass, uidCalc)
                .then(res => {
                    console.error(res);
                    if(res.data.result == 1) {
                        SuccFunc(res.data);
                    }
                    else {
                    }
                })
                .catch( (error) => {
                    console.log(error);
                })
        },
    };

    private helpers = {
        hoverLabel: (Name, Children, value) => {
            let label_dang;
            let el_require;
            let dang_class = "";
            if(this.item.require) {
                el_require = <i title={"This is a required parameter!"} className={"fas fa-exclamation-circle zt_require_i_label"}/>;
                label_dang = <label className="hover-label-dang">
                    <i title={"Value cannot be empty!"} className="fas fa-exclamation-triangle zt_require_i_label"/>{"Value cannot be empty!"}
                </label>;
                if(!value) {
                    dang_class = "zt_form_control_dang";
                }
            }

            return (<div className={"hover-label-parent " + dang_class}>
                <div className="hover-label_wrap1">
                    <label className="hover-label">{el_require}{Name}</label>
                    {label_dang}
                </div>
                <div className="hover-label_wrap2">
                    {Children}
                </div>
            </div>);
        },
        calcBtn: () => {
            return  <button key={"calcbtn"} className="hover_label_btn" type="button" title="Calculation"
                            onClick={(e) => {
                                let uidCalc = this.item.calc.uid;
                                let uidObj = this.ObjID;
                                let uidClass = this.TypeID;

                                this.core.CalcGetResult(uidObj, uidClass, uidCalc, (res) => {
                                    if(this.attrDataCache[this.item.uid] == undefined) {
                                        this.attrDataCache[this.item.uid] = {};
                                        this.attrDataCache[this.item.uid].data = this.item;
                                    }
                                    this.attrDataCache[this.item.uid].value = res.data.value;
                                    this.ChangeStateFunc();
                                });
                            }}
                            onDoubleClick={()=>{
                                /*
                                let uidClass = this.props.ObjectsStore.SelectNode.TypeID;
                                let uidCalc = item.calc.uid;
                                window.open('/calc/?class=' + uidClass+'&uid='+uidCalc, '_blank');

                                 */
                        }}
            ><i className="fas fa-calculator"/></button>;

        },
        calcManualBtn: () => {
            return (
                <button key={"calcbtn"} className="hover_label_btn" type="button" title="Manual calculation"
                        onClick={(e) => {
                                let uidCalc = this.item.calc_triggers.manual.uid;
                                let uidObj = this.ObjID;

                                if(this.type == "add") {
                                    uidObj = "";
                                }


                                let uidClass = this.TypeID;
                                let trigger = "manual";
                                let values:IValue[] = this.helpers.generateDataForSave();

                                this.core.AJAX.getRunTrigger({
                                    calc:uidCalc,
                                    trigger: "manual",
                                    obj:uidObj,
                                    type:uidClass,
                                    values: values,
                                }, (answer) => {

                                    if(answer.data[uidObj] != undefined) {
                                        let v_attrData = answer.data[uidObj];

                                        this.attrData.forEach( (item_a, index) => {

                                            for(let key_v in v_attrData) {
                                                let item_v = v_attrData[key_v];
                                                if(item_a.uid == key_v) {
                                                    if(this.attrDataCache[item_a.uid] == undefined) {
                                                        this.attrDataCache[item_a.uid] = {};
                                                    }
                                                    this.attrDataCache[item_a.uid].value = item_v.value;
                                                    break;
                                                }
                                            }
                                        });
                                    }

                                    this.ChangeStateFunc();

                                    ToastrStore.success("Success");

                                }, (answer) => {
                                    answer.errors.forEach((err) => {
                                        ToastrStore.error(err.text);
                                    });
                                });
                            }}
            ><i className="fas fa-square-root-alt"/></button>
            );

        },
        arrayBtn: (controls, isLoading) => {
            if(isLoading) {
                return (
                    <div className="input-group-append">
                        <button disabled={true} title={"loading"} className="btn btn-outline-secondary btn-sm" type="button">
                            <i className="fas fa-spinner"/>
                        </button>
                    </div>
                );
            }
            else {
                let ref_t = null;


                let is_show = !!this.item.show_pop;
                let target = this.item.target_pop ? this.item.target_pop : null;

                const handleClick = (event) => {
                    this.item.show_pop = !this.item.show_pop;
                    this.item.target_pop = event.target;
                    this.ChangeStateFunc();
                };

                return (
                    <div className="input-group-append" >
                        <Overlay rootClose={true}
                                 show={is_show}
                                 target={target}
                                 placement="bottom"
                                 containerPadding={20}
                                 onHide={(e:any) =>{
                                     const ignore = ['.react-select__menu', '.react-select__menu-list','.react-select__option', '.react-select__option--is-focused'].some(selector => e.target.matches(selector));
                                     console.log('CLASS');
                                     console.log(e.target.classList);

                                     if (!ignore) {
                                         this.item.show_pop = false;
                                         this.ChangeStateFunc();
                                     }
                                 }}
                        >
                            <Popover id={`popover-array-${this.item.uid}-${this.ObjID}-${this.key}`} className="zt_array_popover">
                                <Popover.Title as="h3">Edit {this.item.name}</Popover.Title>
                                <Popover.Content>
                                    <div className="zt_array_popover_body" style={{width: "200px"}}>
                                        {controls}
                                    </div>
                                </Popover.Content>
                            </Popover>
                        </Overlay>

                        <button ref={ref_t} onClick={handleClick} id={`btn-close_popover-array-${this.item.uid}-${this.ObjID}-${this.key}`} className="btn btn-outline-secondary btn-sm" type="button">
                            <i className="fas fa-ellipsis-v"/>
                        </button>
                    </div>
                );
            }

        },

        r_hoverLabel: (Name, Children) => {
            let el_require = this.item.require ? <i title={"This is a required parameter!"} className={"fas fa-exclamation-circle zt_require_i_label"}/> : "";

            return (<div className="hover-label-parent">
                <div className="hover-label_wrap1">
                    <label className="hover-label" style={{fontWeight: "bold"}}>{el_require}{Name}</label>
                </div>
                <div className="hover-label_wrap2">
                    {Children}
                </div>
            </div>);
        },

        generateDataForSave: () => {
            let values:IValue[] = [];
            this.attrData.forEach( (item, index) => {
                let _item:IValue = {
                    value: item.value ? item.value.value : item.value,
                    attr: item.uid,
                    key: item.isKey ? 1 : 0,
                    datatype: item.datatype,
                };
                if(this.attrDataCache[item.uid] != undefined) {
                    _item.value = this.attrDataCache[item.uid].value;
                    values.push(_item);
                }
                else {
                    values.push(_item);
                }
            });

            return values;
        },
    }
}
