import * as React from "react";
import * as ReactDOM from "react-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-block-ui/style.css';
import 'loaders.css/loaders.min.css';
import "./index.scss"
import { Provider } from "mobx-react";
import './i18n';
import ClassesStore from './stores/ClassesStore';
import ObjectsStore from './stores/ObjectsStore';
import ApplicationStore from './stores/ApplicationStore'
import ViewStore from './stores/ViewStore'
import ImportStore from './stores/ImportStore'
import RootStore from './stores/RootStore'
import UserStepStore from './stores/UserStepStore'
import TaxesStore from './stores/TaxesStore'
import { Master } from "./containers/master/Master";
import ToastrStore from './stores/ToastrStore'
import loginStore from './stores/login-store'
import CalcStore from './stores/CalcStore';
import '../node_modules/toastr/build/toastr.css';


//init Theme

let L_config = JSON.parse(localStorage.config);

if(localStorage.Theme) {
    document.body.classList.add(localStorage.Theme);
}
else if(L_config?.defaultTheme){
    document.body.classList.add(L_config.defaultTheme);
}
else {
    document.body.classList.add('light');
}

ReactDOM.render(
    <Provider ApplicationStore={ApplicationStore} ClassesStore={ClassesStore} loginStore={loginStore}
              ObjectsStore={ObjectsStore} ToastrStore={ToastrStore} CalcStore={CalcStore}
              RootStore={RootStore} ViewStore = {ViewStore} ImportStore={ImportStore} UserStepStore={UserStepStore} TaxesStore={TaxesStore}
    >
        <div>
        <Master compiler="TypeScript" framework="React" />
        </div>
    </Provider>,
    document.getElementById("root")
);


//height: calc(var(--vh, 1vh) * 100 - 400px); - пример в элементе
//Просто какое-то решение для навигационной панели мобильного браузера
//В другом скрипте оно не нужно, так как там флексовая верстка и дисплей фиксд
function GenerateConstVH() {
    let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', vh+'px');
}
GenerateConstVH();
window.addEventListener('resize', function(){
    // We execute the same script as before
    GenerateConstVH();
});