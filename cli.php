<?php

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;

define('BASE_PATH', __DIR__);
define('APP_PATH', BASE_PATH . '/apps');

const GLOBAL_FLAGS = ['--single', '--unlock'];

function unlockTask($fp, $filename) {
    if ($fp) {
        flock($fp, LOCK_UN);
    }
    unlink($filename);
}
$fp=null;
try {
    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new CliDI();

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    $console = new ConsoleApp();
    $console->setDI($di);

    $arguments = [];

    $single = false;
    $unlock = false;

    foreach ($argv as $k => $arg) {
        if ($k === 1) {
            $arguments['task'] = $arg;
        } elseif ($k === 2) {
            $arguments['action'] = $arg;
        } elseif ($k >= 3) {
            if (!in_array($arg, GLOBAL_FLAGS)) {
                $arguments['params'][] = $arg;
            }
            if ($arg == '--single') {
                $single = true;
            }
            if ($arg == '--unlock') {
                $unlock = true;
            }
        }
    }

    if ($single) {
        $task = isset($arguments['task']) ? $arguments['task'] : 'main';
        $action = isset($arguments['action']) ? $arguments['action'] : 'main';
        $params = isset($arguments['params']) ? implode('-', $arguments['params']) : '';

        $lock_file = $task . '-' . $action . '-' . $params . '.txt';
        $lock_file = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $lock_file);
        $lock_file = mb_ereg_replace("([\.]{2,})", '', $lock_file);
        $lock_filename = sys_get_temp_dir() . "/" . $lock_file;

        if ($unlock) {
            $fp = @fopen($lock_filename, "r");
            unlockTask($fp, $lock_filename);
            fclose($fp);
            return;
        }

        $fp = @fopen($lock_filename, "x");

        $running = false;
        if ($fp) {
            if (flock($fp, LOCK_EX | LOCK_NB)) {
                $console->handle($arguments);
            } else {
                $running = true;
            }
            unlockTask($fp, $lock_filename);
        } else {
            $running = true;
        }

        if ($running) {
            echo "Action $action in task $task already running!" . PHP_EOL;
        }
    } else {  
       
        $console->handle($arguments);
        
    }

} catch (\Phalcon\Exception $e) {
    if ($fp) {
        unlockTask($fp, $lock_filename);
    }
    fwrite(STDERR, $e->getMessage() . PHP_EOL);
    fwrite(STDERR, $e->getTraceAsString() . PHP_EOL);
    exit(1);
} catch (\Throwable $throwable) {
    if ($fp) {
        unlockTask($fp, $lock_filename);
    }
    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
    fwrite(STDERR, $throwable->getTraceAsString() . PHP_EOL);
    exit(1);
} catch (\Exception $exception) {
    if ($fp) {
        unlockTask($fp, $lock_filename);
    }
    fwrite(STDERR, $exception->getMessage() . PHP_EOL);
    fwrite(STDERR, $exception->getTraceAsString() . PHP_EOL);
    exit(1);
}