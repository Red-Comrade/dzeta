<?php
namespace Dzeta\Library\SignDoc;

use Dzeta\Helpers\CurlHelper;

use Dzeta\Library\SignDoc\Core\Procedure;
use Dzeta\Library\SignDoc\Core\File;
use Dzeta\Library\SignDoc\Core\Member;
use Dzeta\Library\SignDoc\Core\FileObject;

class Docage extends Client
{
	const API_URL = 'https://api.docage.com';
	const API_PROCEDURES_URL = '/Transactions';
	const API_PROCEDURES_GET_BY_ID_URL = '/ById/{{id}}';
	const API_PROCEDURES_LAUNCH_URL = '/LaunchTransaction/{{transaction}}';
	const API_FILES_URL = '/TransactionFiles';
	const API_FILE_OBJECTS_URL = '/SignatureLocations';
	const API_DOWNLOAD_FILE_URL = '/Download/{{file}}';
	const API_MEMBERS_URL = '/TransactionMembers';
	const API_MEMBERS_CREATE_URL = '/Contacts';
	const API_MEMBERS_GET_BY_EMAIL_URL = '/byemail/{{email}}';

	const SIGNED_STATUS = 5;

	private $email;

	public function __construct(string $apiKey, string $email) {
		parent::__construct($apiKey);
		$this->email = $email;
	}

	public function createProcedure(string $name, string $description, array $config = []) {
		$headers = $this->getHeaders();
		$options = [
			'Name' => $name,
			'Notes' => $description,
			'Webhook' => isset($config['hook']) ? $config['hook'] : '',
		];
		$curl = new CurlHelper(
			self::API_URL . self::API_PROCEDURES_URL,
			'POST',
			$options,
			$headers,
			self::CURL_OPTIONS
		);
		$procedure = $curl->raw();
		return Procedure::fromDocageRequest($procedure);
	}

	public function startProcedure(Procedure $procedure) {
		$headers = $this->getHeaders();
		$headers['Content-Length'] = 0;
		$url = self::API_URL . self::API_PROCEDURES_URL . $this->replaceTag(self::API_PROCEDURES_LAUNCH_URL, 'transaction', $procedure->getId());
		$curl = new CurlHelper(
			$url,
			'POST',
			[],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->raw();
		$result = $result === '' ? $procedure->getId() : $result;
		return Procedure::fromDocageRequest($result);
	}

	public function addFileToProcedure(Procedure $procedure, string $filename, string $realFilename) {
		$headers = $this->getHeaders();
		$headers['Content-Type'] = 'multipart/form-data';
		$options = self::CURL_OPTIONS;
		$options[CURLOPT_POSTFIELDS] = [
			'TransactionId' => $procedure->getId(),
			'FileName' => $realFilename,
			'FileToUpload' => new \CURLFile($filename, null, $realFilename),
		];
		$curl = new CurlHelper(
			self::API_URL . self::API_FILES_URL,
			'POST',
			[],
			$headers,
			$options
		);
		$file = $curl->raw();
		return File::fromDocageRequest($file);
	}

	public function addMemberToProcedure(Procedure $procedure, string $id) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_MEMBERS_URL,
			'POST',
			[
				'ContactId' => $id,
				'MemberRole' => 0,
				'SignMode' => 0,
				'TransactionId' => $procedure->getId(),
			],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->raw();
		return Member::fromDocageRequest($result);
	}

	public function addExternalMemberToProcedure(Procedure $procedure, string $firstname, string $lastname, string $phone, string $email) {
		$contact = $this->getContactByEmail($email);
		if (empty($contact->getId())) {
			$contact = $this->createContact($firstname, $lastname, $phone, $email);
		}
		if (!empty($contact->getId())) {
			$member = $this->addMemberToProcedure($procedure, $contact->getId());
		} else {
			$member = $contact;
		}
		return $member;
	}

	public function addMemberFileObject(File $file, Member $member, string $position, int $page, array $mentions) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_FILE_OBJECTS_URL,
			'POST',
			[
				'TransactionFileId' => $file->getId(),
			    'TransactionMemberId' => $member->getId(),
			    'Coordinates' => $position,
			    'Pages' => $page,
			],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->raw();
		return FileObject::fromDocageRequest($result, $file, $member);
	}

	public function createContact(string $firstname, string $lastname, string $phone, string $email) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_MEMBERS_CREATE_URL,
			'POST',
			[
				'FirstName' => $firstname,
				'LastName' => $lastname,
				'Phone' => $phone,
				'Email' => $email,
			],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->raw();
		return Member::fromDocageRequest($result);
	}

	public function getContactByEmail(string $email) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_MEMBERS_CREATE_URL . $this->replaceTag(self::API_MEMBERS_GET_BY_EMAIL_URL, 'email', $email),
			'GET',
			[],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->json();
		if (!empty($result['Id'])) {
			$result = $result['Id'];
		}
		return Member::fromDocageRequest($result);
	}

	public function getFiles(Procedure $procedure) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_PROCEDURES_URL . $this->replaceTag(self::API_PROCEDURES_GET_BY_ID_URL, 'id', $procedure->getId()),
			'GET',
			[],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->json();
		$files = [];
		if (!empty($result['TransactionFiles'])) {
			foreach ($result['TransactionFiles'] as $f) {
				$files[] = File::fromDocageFullRequest($f);
			}
		}
		return $files;
	}

	public function downloadFile(File $file, string $filename) {
		$url = self::API_URL . self::API_FILES_URL . $this->replaceTag(self::API_DOWNLOAD_FILE_URL, 'file', $file->getId());
		$options = self::CURL_OPTIONS;
		$fp = fopen($filename, 'w+');
		$options[CURLOPT_FILE] = $fp;
		$headers = $this->getHeaders();
		$curl = new CurlHelper($url, 'GET', [], $headers, $options);
		$result = $curl->raw();
		unset($result);
		fclose($fp);
		file_put_contents($filename, base64_decode(trim(file_get_contents($filename), '"')));
		return $url;
	}

	protected function getHeaders() {
		$headers = parent::getHeaders();
		$auth = base64_encode("{$this->email}:{$this->apiKey}");
		$headers['Authorization'] = "Basic {$auth}";
		return $headers;
	}
}

