<?php
namespace Dzeta\Library\SignDoc;

use Dzeta\Helpers\CurlHelper;

use Dzeta\Library\SignDoc\Core\Procedure;
use Dzeta\Library\SignDoc\Core\File;
use Dzeta\Library\SignDoc\Core\Member;
use Dzeta\Library\SignDoc\Core\FileObject;

class Yousign extends Client
{
	const API_URL = 'https://staging-api.yousign.com';
	const API_USERS_URL = '/users';
	const API_PROCEDURES_URL = '/procedures';
	const API_FILES_URL = '/files';
	const API_FILE_OBJECTS_URL = '/file_objects';
	const API_DOWNLOAD_FILE_URL = '/files/{{file}}/';
	const API_MEMBERS_URL = '/members';

	public function getUsers() {
		$curl = new CurlHelper(
			self::API_URL . self::API_USERS_URL,
			'GET',
			[],
			[
				'Authorization' => "Bearer {$this->apiKey}",
				'Content-Type' => 'application/json'
			],
			self::CURL_OPTIONS
		);
		$users = $curl->json();
		return $users;
	}

	public function createProcedure(string $name, string $description, array $config = []) {
		$headers = $this->getHeaders();
		$options = [
			'name' => $name,
			'description' => $description,
			'start' => false,
			'config' => $config
		];
		$curl = new CurlHelper(
			self::API_URL . self::API_PROCEDURES_URL,
			'POST',
			$options,
			$headers,
			self::CURL_OPTIONS
		);
		$procedure = $curl->json();
		return Procedure::fromYousignRequest($procedure);
	}

	public function startProcedure(Procedure $procedure) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . $procedure->getId(),
			'PUT',
			[
				'start' => true,
			],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->json();
		return Procedure::fromYousignRequest($result);
	}

	public function addFileToProcedure(Procedure $procedure, string $filename, string $realFilename) {
		$headers = $this->getHeaders();
		$content = file_get_contents($filename);
		$curl = new CurlHelper(
			self::API_URL . self::API_FILES_URL,
			'POST',
			[
				'name' => $realFilename,
				'content' => base64_encode($content),
				'procedure' => $procedure->getId(),
			],
			$headers,
			self::CURL_OPTIONS
		);
		$file = $curl->json();
		return File::fromYousignRequest($file);
	}

	public function addMemberToProcedure(Procedure $procedure, string $id) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_MEMBERS_URL,
			'POST',
			[
				'user' => $id,
				'operationLevel' => 'custom',
	            'operationCustomModes' => ['email'],
				'procedure' => $procedure->getId(),
			],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->json();
		return Member::fromYousignRequest($result);
	}

	public function addExternalMemberToProcedure(Procedure $procedure, string $firstname, string $lastname, string $phone, string $email) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_MEMBERS_URL,
			'POST',
			[
				'firstname' => $firstname,
				'lastname' => $lastname,
				'phone' => $phone,
				'email' => $email,
				'operationLevel' => 'custom',
	            'operationCustomModes' => ['email'],
				'procedure' => $procedure->getId(),
			],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->json();
		return Member::fromYousignRequest($result);
	}

	public function addMemberFileObject(File $file, Member $member, string $position, int $page, array $mentions) {
		$headers = $this->getHeaders();
		$curl = new CurlHelper(
			self::API_URL . self::API_FILE_OBJECTS_URL,
			'POST',
			[
				'file' => $file->getId(),
			    'member' => $member->getId(),
			    'position' => $position,
			    'page' => $page,
			    'mention' => isset($mentions['mention']) ? $mentions['mention'] : '',
			    'mention2' => isset($mentions['mention2']) ? $mentions['mention2'] : '',
			    'reason' => isset($mentions['reason']) ? $mentions['reason'] : ''
			],
			$headers,
			self::CURL_OPTIONS
		);
		$result = $curl->json();
		return FileObject::fromYousignRequest($result);
	}

	public function downloadFile(File $file, string $filename) {
		$url = self::API_URL . $this->replaceTag(self::API_DOWNLOAD_FILE_URL, 'file', str_replace('/files/', '', $file->getId())) . 'download';
		$options = self::CURL_OPTIONS;
		$fp = fopen($filename, 'w+');
		$options[CURLOPT_FILE] = $fp;
		$headers = $this->getHeaders();
		$curl = new CurlHelper($url, 'GET', [], $headers, $options);
		$result = $curl->run();
		unset($result);
		fclose($fp);
		file_put_contents($filename, base64_decode(file_get_contents($filename)));
		return $url;
	}
}

