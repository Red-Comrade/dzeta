<?php

namespace Dzeta\Library\SignDoc\Core;

class Member
{
	private $id;
	private $firstname;
	private $lastname;
	private $phone;
	private $email;
	private $options;

	public function __construct(string $id, string $firstname, string $lastname, string $phone, string $email, array $options = []) {
		$this->id = $id;
		$this->firstname = $firstname;
		$this->lastname = $lastname;
		$this->phone = $phone;
		$this->email = $email;
		$this->options = $options;
	}

	public function getId() {
		return $this->id;
	}

	public function getFirstname() {
		return $this->firstname;
	}

	public function getLastname() {
		return $this->lastname;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setId(string $id) {
		$this->id = $id;
		return $this;
	}

	public function setFirstname(string $firstname) {
		$this->firstname = $firstname;
		return $this;
	}

	public function setLastname(string $lastname) {
		$this->lastname = $lastname;
		return $this;
	}

	public function setPhone(string $phone) {
		$this->phone = $phone;
		return $this;
	}

	public function setEmail(string $email) {
		$this->email = $email;
		return $this;
	}

	public function setOptions(array $options) {
		$this->options = $options;
		return $this;
	}

	public static function fromYousignRequest(array $options) {
		$obj = new static(
			isset($options['id']) ? $options['id'] : '',
			isset($options['firstname']) ? $options['firstname'] : '',
			isset($options['lastname']) ? $options['lastname'] : '',
			isset($options['phone']) ? $options['phone'] : '',
			isset($options['email']) ? $options['email'] : ''
		);
		unset($options['id'], $options['firstname'], $options['lastname'], $options['phone'], $options['email']);
		$obj->setOptions($options);
		return $obj;
	}

	public static function fromDocageRequest($options) {
		$result = !empty($options) ? trim($options, '"') : '';
		$options = [];
		if (preg_match("/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i", $result) != 1) {
			$options = [
				'detail' => !empty($result) ? $result : 'Unkown error'
			];
			$result = '';
		}
		$obj = new static($result, '', '', '', '');
		$obj->setOptions($options);
		return $obj;
	}
}