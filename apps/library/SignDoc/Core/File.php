<?php

namespace Dzeta\Library\SignDoc\Core;

class File
{
	private $id;
	private $name;
	private $options;

	public function __construct(string $id, string $name, array $options = []) {
		$this->id = $id;
		$this->name = $name;
		$this->options = $options;
	}

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setId(string $id) {
		$this->id = $id;
		return $this;
	}

	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	public function setOptions(array $options) {
		$this->options = $options;
		return $this;
	}

	public static function fromYousignRequest(array $options) {
		$obj = new static(
			isset($options['id']) ? $options['id'] : '',
			isset($options['name']) ? $options['name'] : ''
		);
		unset($options['id'], $options['name']);
		$obj->setOptions($options);
		return $obj;
	}

	public static function fromDocageRequest($options) {
		$result = !empty($options) ? trim($options, '"') : '';
		$options = [];
		if (preg_match("/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i", $result) != 1) {
			$options = [
				'detail' => !empty($result) ? $result : 'Unkown error'
			];
			$result = '';
		}
		$obj = new static($result, '');
		$obj->setOptions($options);
		return $obj;
	}

	public static function fromDocageFullRequest($options) {
		$obj = new static(
			isset($options['Id']) ? $options['Id'] : '',
			(isset($options['Name']) ? $options['Name'] : '') . (isset($options['FileExtension']) ? $options['FileExtension'] : '')
		);
		unset($options['id'], $options['name']);
		$obj->setOptions($options);
		return $obj;
	}
}