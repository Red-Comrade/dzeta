<?php

namespace Dzeta\Library\SignDoc\Core;

class FileObject
{
	private $id;
	private $file;
	private $member;
	private $options;

	public function __construct(string $id, File $file, Member $member, array $options = []) {
		$this->id = $id;
		$this->file = $file;
		$this->member = $member;
		$this->options = $options;
	}

	public function getId() {
		return $this->id;
	}

	public function getFile() {
		return $this->file;
	}

	public function getMember() {
		return $this->member;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setId(string $id) {
		$this->id = $id;
		return $this;
	}

	public function setFile(File $file) {
		$this->file = $file;
		return $this;
	}

	public function setMember(Member $member) {
		$this->member = $member;
		return $this;
	}

	public function setOptions(array $options) {
		$this->options = $options;
		return $this;
	}

	public static function fromYousignRequest(array $options) {
		$member = Member::fromYousignRequest(isset($options['member']) ? $options['member'] : []);
		$file = File::fromYousignRequest([
			'id' => isset($options['file']) ? $options['file'] : '',
			'name' => ''
		]);
		$obj = new static(isset($options['id']) ? $options['id'] : '', $file, $member);
		unset($options['id'], $options['member'], $options['file']);
		$obj->setOptions($options);
		return $obj;
	}

	public static function fromDocageRequest(string $options, File $file, Member $member) {
		$result = !empty($options) ? trim($options, '"') : '';
		$options = [];
		if (preg_match("/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i", $result) != 1) {
			$options = [
				'detail' => !empty($result) ? $result : 'Unkown error'
			];
			$result = '';
		}
		$obj = new static($result, $file, $member);
		$obj->setOptions($options);
		return $obj;
	}
}