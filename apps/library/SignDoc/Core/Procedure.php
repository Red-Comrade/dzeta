<?php

namespace Dzeta\Library\SignDoc\Core;

class Procedure
{
	private $id;
	private $name;
	private $description;
	private $options;

	public function __construct(string $id, ?string $name = null, ?string $description, array $options = []) {
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
		$this->options = $options;
	}

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setId(string $id) {
		$this->id = $id;
		return $this;
	}

	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	public function setDescription(string $description) {
		$this->description = $description;
		return $this;
	}

	public function setOptions(array $options) {
		$this->options = $options;
		return $this;
	}

	public static function fromYousignRequest($options) {
		$obj = new static(
			isset($options['id']) ? $options['id'] : '',
			isset($options['name']) ? $options['name'] : '',
			isset($options['description']) ? $options['description'] : ''
		);
		unset($options['id'], $options['name'], $options['description']);
		$obj->setOptions($options);
		return $obj;
	}

	public static function fromDocageRequest($options) {
		$result = !empty($options) ? trim($options, '"') : '';
		$options = [];
		if (preg_match("/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i", $result) != 1) {
			$options = [
				'detail' => !empty($result) ? $result : 'Unkown error'
			];
			$result = '';
		}
		$obj = new static($result, '', '');
		$obj->setOptions($options);
		return $obj;
	}
}