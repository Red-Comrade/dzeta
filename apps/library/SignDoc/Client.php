<?php

namespace Dzeta\Library\SignDoc;

abstract class Client
{
	const CURL_OPTIONS = [
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	];

	protected $apiKey;

	public function __construct(string $apiKey) {
		$this->apiKey = $apiKey;
	}

	protected function replaceTag($string, $tag, $value) {
		return str_replace('{{' . $tag . '}}', $value, $string);
	}

	protected function getHeaders() {
		return [
			'Authorization' => "Bearer {$this->apiKey}",
			'Content-Type' => 'application/json'
		];
	}
}

