<?php

/**
 * This file is part of PHPWord - A pure PHP library for reading and writing
 * word processing documents.
 *
 * PHPWord is free software distributed under the terms of the GNU Lesser
 * General Public License version 3 as published by the Free Software Foundation.
 *
 * For the full copyright and license information, please read the LICENSE
 * file that was distributed with this source code. For the full list of
 * contributors, visit https://github.com/PHPOffice/PHPWord/contributors.
 *
 * @see         https://github.com/PHPOffice/PHPWord
 * @copyright   2010-2018 PHPWord contributors
 * @license     http://www.gnu.org/licenses/lgpl.txt LGPL version 3
 */



use PhpOffice\Common\Text;
use PhpOffice\PhpWord\Escaper\RegExp;
use PhpOffice\PhpWord\Escaper\Xml;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\Shared\ZipArchive;

class MyTemplateProcessor
{

    const MAXIMUM_REPLACEMENTS_DEFAULT =-1;

    /**
     * ZipArchive object.
     *
     * @var mixed
     */
    protected $zipClass;

    /**
     * @var string Temporary document filename (with path)
     */
    protected $tempDocumentFilename;

    /**
     * Content of main document part (in XML format) of the temporary document
     *
     * @var string
     */
    protected $tempDocumentMainPart;

    /**
     * Content of headers (in XML format) of the temporary document
     *
     * @var string[]
     */
    protected $tempDocumentHeaders = array();

    /**
     * Content of footers (in XML format) of the temporary document
     *
     * @var string[]
     */
    protected $tempDocumentFooters = array();
    private $_countRels;
    private $_rels;
    private $_types;

    /**
     * @since 0.12.0 Throws CreateTemporaryFileException and CopyFileException instead of Exception
     *
     * @param string $documentTemplate The fully qualified template filename
     *
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     */
    protected $tempDocumentComments;
    private $arrRelData = array();
    private $ImageArr=array();

    public function __construct($documentTemplate)
    {
        // Temporary document filename initialization
        $this->tempDocumentFilename = tempnam(\PhpOffice\PhpWord\Settings::getTempDir(), 'PhpWord');
        if (false === $this->tempDocumentFilename)
        {
            throw new CreateTemporaryFileException();
        }

        // Template file cloning
        if (false === copy($documentTemplate, $this->tempDocumentFilename))
        {
            throw new CopyFileException($documentTemplate, $this->tempDocumentFilename);
        }

        // Temporary document content extraction
        $this->zipClass = new ZipArchive();
        $this->zipClass->open($this->tempDocumentFilename);
        $index = 1;
        while (false !== $this->zipClass->locateName($this->getHeaderName($index)))
        {
            $this->tempDocumentHeaders[$index] = $this->fixBrokenMacros(
                    $this->zipClass->getFromName($this->getHeaderName($index))
            );
            $index++;
        }
        $index = 1;
        while (false !== $this->zipClass->locateName($this->getFooterName($index)))
        {
            $this->tempDocumentFooters[$index] = $this->fixBrokenMacros(
                    $this->zipClass->getFromName($this->getFooterName($index))
            );
            $index++;
        }
        $this->tempDocumentMainPart = $this->fixBrokenMacros($this->zipClass->getFromName($this->getMainPartName()));

        $this->_countRels = 100;

        $this->tempDocumentComments = $this->fixBrokenMacros($this->zipClass->getFromName($this->getCommentsName()));
        $this->_rels=$this->fixBrokenMacros($this->zipClass->getFromName($this->getRelsDocName()));
        $this->arrRelData=$this->GetFillArrRel();
    }


    public function GetAllTableSettMetka()
    {
        //$this->tempDocumentMainPart
        $offset = 0;
        $allpos = array();
        $needle='<w:tbl';
        $tableT=[];
        while (($pos = strpos($this->tempDocumentMainPart, $needle, $offset)) !== FALSE)
        {
            $offset = $pos + 1;
            $endPos=$this->findTableEND($offset);
            $startPos=$this->findTableStart($offset);
            $allpos[] = [$startPos,$endPos];
            $offset =$endPos;
            $tableCur=substr($this->tempDocumentMainPart, $startPos, ($endPos - $startPos));
           //$tableT[]=$tableCur;
            preg_match_all('/\{[^}]*\{(.*?)\}.*?\}/', $tableCur, $matches);
            if(!empty($matches))
            {
                $tableT[]=["metk"=>$matches[1],"table"=>$tableCur];
            }
        }
        return $tableT;

    }

    public function deleteXML($textXML)
    {
        $this->tempDocumentMainPart = str_replace($textXML, "", $this->tempDocumentMainPart);
    }


    private function GetFillArrRel()
    {
        $text = $this->fixBrokenMacros($this->zipClass->getFromName($this->getRelsDocName()));
         $newArr = array();
        while (1)
        {
            preg_match('/<Relationship Id=\"(\w+)\" Type=\"(\w+|[^"\s]*)\" Target=\"(\w+|[^"]*)\"/', $text, $output_array);
            if (empty($output_array))
            {
                break;
            }
            $text = str_replace($output_array[0], "", $text);
            if (count($output_array) == 4)
            {
                $newArr[] = array("id" => $output_array[1],
                    "type" => $output_array[2],
                    "target" => $output_array[3]);
            }
        }

        return $newArr;
    }

    /**
     * @param string $xml
     * @param \XSLTProcessor $xsltProcessor
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     *
     * @return string
     */
    protected function transformSingleXml($xml, $xsltProcessor)
    {
        libxml_disable_entity_loader(true);
        $domDocument = new \DOMDocument();
        if (false === $domDocument->loadXML($xml))
        {
            throw new Exception('Could not load the given XML document.');
        }

        $transformedXml = $xsltProcessor->transformToXml($domDocument);
        if (false === $transformedXml)
        {
            throw new Exception('Could not transform the given XML document.');
        }

        return $transformedXml;
    }

    /**
     * @param mixed $xml
     * @param \XSLTProcessor $xsltProcessor
     *
     * @return mixed
     */
    protected function transformXml($xml, $xsltProcessor)
    {
        if (is_array($xml))
        {
            foreach ($xml as &$item)
            {
                $item = $this->transformSingleXml($item, $xsltProcessor);
            }
        } else
        {
            $xml = $this->transformSingleXml($xml, $xsltProcessor);
        }

        return $xml;
    }

    /**
     * Applies XSL style sheet to template's parts.
     *
     * Note: since the method doesn't make any guess on logic of the provided XSL style sheet,
     * make sure that output is correctly escaped. Otherwise you may get broken document.
     *
     * @param \DOMDocument $xslDomDocument
     * @param array $xslOptions
     * @param string $xslOptionsUri
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function applyXslStyleSheet($xslDomDocument, $xslOptions = array(), $xslOptionsUri = '')
    {
        $xsltProcessor = new \XSLTProcessor();

        $xsltProcessor->importStylesheet($xslDomDocument);
        if (false === $xsltProcessor->setParameter($xslOptionsUri, $xslOptions))
        {
            throw new Exception('Could not set values for the given XSL style sheet parameters.');
        }

        $this->tempDocumentHeaders = $this->transformXml($this->tempDocumentHeaders, $xsltProcessor);
        $this->tempDocumentMainPart = $this->transformXml($this->tempDocumentMainPart, $xsltProcessor);
        $this->tempDocumentFooters = $this->transformXml($this->tempDocumentFooters, $xsltProcessor);
    }

    /**
     * @param string $macro
     *
     * @return string
     */
    protected static function ensureMacroCompleted($macro)
    {
        if (substr($macro, 0, 2) !== '{{' && substr($macro, -2) !== '}}')
        {
            $macro = '{{' . $macro . '}}';
        }

        return $macro;
    }

    /**
     * @param string $subject
     *
     * @return string
     */
    protected static function ensureUtf8Encoded($subject)
    {
        if (!Text::isUTF8($subject))
        {
            $subject = utf8_encode($subject);
        }

        return $subject;
    }

    /**
     * @param mixed $search
     * @param mixed $replace
     * @param int $limit
     */
    public function setValue($search, $replace,$limit = self::MAXIMUM_REPLACEMENTS_DEFAULT,$image=false)
    {
        if (is_array($search))
        {
            foreach ($search as &$item)
            {
                $item = self::ensureMacroCompleted($item);
            }
        } else
        {
            $search = self::ensureMacroCompleted($search);
        }
        if($image)
        {
            $dataFilee=$replace;
            $replace=$replace['fileName'];

            $searchT=isset($dataFilee['origMetka'])? $dataFilee['origMetka']:$search;

            foreach ($this->ImageArr as $value)
            {
                if($value['metka']==$searchT)
                {
                    //тут делаем подмену лучше ID, и вставлять новую
                    $replace=$value['text'];
                    $replace= str_replace('<a:blip r:embed="'.$value['idImage'].'"', '<a:blip r:embed="rId'.$this->_countRels.'"', $replace);
                    $toAdd="";
                    $toAdd='<Relationship Id="rId'.$this->_countRels.'" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image'.$this->_countRels.'.png" />';
                    $this->_rels = str_replace('</Relationships>', $toAdd, $this->_rels) . '</Relationships>';
                    $this->zipClass->addFile($dataFilee['path'],'word/media/image'.$this->_countRels.".png");
                    $this->_countRels++;
                }

            }
        }

        if (is_array($replace))
        {
            foreach ($replace as &$item)
            {
                $item = self::ensureUtf8Encoded($item);
            }
        } else
        {
            $replace = self::ensureUtf8Encoded($replace);
            $replace = str_replace("&ge;","≥",$replace);
            $replace = str_replace("&le;","≤",$replace);
            $replace = str_replace("&","&amp;",$replace);
        }

        if (\PhpOffice\PhpWord\Settings::isOutputEscapingEnabled())
        {
            $xmlEscaper = new Xml();
            $replace = $xmlEscaper->escape($replace);
        }

      //  $this->tempDocumentHeaders = $this->setValueForPart($search, $replace, $this->tempDocumentHeaders, $limit);
        $this->tempDocumentMainPart = $this->setValueForPart($search, $replace, $this->tempDocumentMainPart, $limit);
      //  $this->tempDocumentFooters = $this->setValueForPart($search, $replace, $this->tempDocumentFooters, $limit);
    }

    public function getTableAsText($element)
    {
        $xmlWriter = $this->getXmlWriter();
        $writer = new \PhpOffice\PhpWord\Writer\Word2007\Element\Table($xmlWriter, $element);
        $writer->write();
        return $xmlWriter->getData();
    }

    public function setImg($strKey, $imgs)
    {
        $strKOld=$strKey;   
        $strKey = '{{' . $strKey . '}}';
        $relationTmpl = '<Relationship Id="RID" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/IMG"/>';

        $imgTmpl1 = '<w:p><w:pPr><w:jc w:val="center" /></w:pPr><w:pict><v:shape type="#_x0000_t75" style="width:WIDUN;height:HEIUN"><v:imagedata r:id="RID" o:title=""/></v:shape></w:pict><w:br/></w:p>';
        $imgTmpl = '<w:pict><v:shape type="#_x0000_t75" style="width:WIDUN;height:HEIUN" stroke="false" strokecolor="white"  strokeweight="0"><v:imagedata r:id="RID" o:title=""/></v:shape></w:pict><w:br/>';
        $imgTmpl = '<w:pict><v:shape type="#_x0000_t75" style="width:WIDUN;height:HEIUN" stroke="false" strokecolor="white"  strokeweight="0"><v:imagedata r:id="RID" o:title=""/></v:shape></w:pict>';

        $toAdd = $toAddImg = $toAddType = '';
        $aSearch = array('RID', 'IMG');
        $aSearchType = array('IMG', 'EXT');        
        foreach ($imgs as $img)
        {
            $countrels = $this->_countRels++;
            //I'm work for jpg files, if you are working with other images types -> Write conditions here
            $imgExt = isset($img['ext']) ? $img['ext'] : 'jpg';
            if( in_array($imgExt, array('jpg', 'JPG') ) )  $imgExt = 'jpeg';
            $imgName = 'img' . $countrels . '.' . $imgExt;

            $this->zipClass->deleteName('word/media/' . $imgName);

            $this->zipClass->addFile($img['src'], 'word/media/' . $imgName);

            $typeTmpl = '<Override PartName="/word/media/' . $imgName . '" ContentType="image/EXT"/>';


            $rid = 'rId' . $countrels;

            list($w, $h) = getimagesize($img['src']);

            if (isset($img['swh']))
            { //Image proportionally larger side
                if ($w <= $h)
                {
                    $ht = (int) $img['swh'];
                    $ot = $w / $h;
                    $wh = (int) $img['swh'] * $ot;
                    $wh = round($wh);
                }
                if ($w >= $h)
                {
                    $wh = (int) $img['swh'];
                    $ot = $h / $w;
                    $ht = (int) $img['swh'] * $ot;
                    $ht = round($ht);
                }
                $w = $wh;
                $h = $ht;
            }

            if (isset($img['size']))
            {
                $w = $img['size'][0];
                $h = $img['size'][1];
            }
            $u = 'px';
            if (isset($img['unit']))
            {
                $u = $img['unit'];
            }
            $u = 'cm';
           
            if($w/$h>2)
            {
                
                $h=4.5*$h/$w;
                $w=4.5;
            }else{
               
                $w=2.5*$w/$h;
                $h=2.5;
            }
            
          //1px~  0.0264583cm

            $toAddImg .= str_replace(array('RID', 'WID', 'HEI', 'UN'), array($rid, $w, $h, $u), $imgTmpl);
            if (isset($img['dataImg']))
            {
                $toAddImg .= '<w:br/><w:t>' . $this->limpiarString($img['dataImg']) . '</w:t><w:br/>';
            }

            $aReplace = array($imgName, $imgExt);
            $toAddType .= str_replace($aSearchType, $aReplace, $typeTmpl);

            $aReplace = array($rid, $imgName);
            $toAdd .= str_replace($aSearch, $aReplace, $relationTmpl);
        }

        
        /*preg_match_all('/\{[^}]*\{(.*?)\}.*?\}/', $this->tempDocumentMainPart, $matches);
        
        foreach ($matches[1] as $key => $value)
        {
            if($strKey=='{{'.strip_tags($value).'}}')
            {                       
               if(strpos($this->tempDocumentMainPart, '{{'.$value.'}}'))
               {
                $offsetWTT=strpos($this->tempDocumentMainPart, '{{'.$value.'}}');

                $wtStart=$this->findwtStart($offsetWTT);
                $wtFinish=$this->findwtFinish($offsetWTT);
                $xmlWT = $this->getSlice($wtStart, $wtFinish);
                $this->tempDocumentMainPart=str_replace( $xmlWT,$toAddImg ,$this->tempDocumentMainPart);
               // $this->tempDocumentMainPart=str_replace('{{'.$value.'}}',$toAddImg ,$this->tempDocumentMainPart);
               }
                
            }
         
        }*/
        $this->tempDocumentMainPart = $this->setValueForPart($strKey, $toAddImg, $this->tempDocumentMainPart,self::MAXIMUM_REPLACEMENTS_DEFAULT);
        //$this->tempDocumentMainPart = str_replace('<w:t>'.$toAddImg.'</w:t>', $toAddImg, $this->tempDocumentMainPart);
        //$this->tempDocumentMainPart = str_replace('<w:t xml:space="preserve">'.$toAddImg.'</w:t>', $toAddImg, $this->tempDocumentMainPart);
        $posss=strpos($this->tempDocumentMainPart,$toAddImg);
        
        if($posss!==false)
        {
            $wtStart=$this->findwtStart($posss);
            $wtFinish=$this->findwtFinish($posss);
            $xmlWT = $this->getSlice($wtStart, $wtFinish);
            $this->tempDocumentMainPart=str_replace($xmlWT,$toAddImg ,$this->tempDocumentMainPart);
        }
        
        /*$this->tempDocumentMainPart = str_replace('<w:t>' . $strKey . '</w:t>', $toAddImg, $this->tempDocumentMainPart);
        $this->tempDocumentMainPart = str_replace('<w:t xml:space="preserve">' . $strKey . '</w:t>', $toAddImg, $this->tempDocumentMainPart);
        $this->tempDocumentMainPart = str_replace('<w:t xml:space="preserve">' . $strKey . ' </w:t>', $toAddImg, $this->tempDocumentMainPart);       
        $this->tempDocumentMainPart = str_replace($strKey, 'NOZAMENA', $this->tempDocumentMainPart);
       //  $this->tempDocumentMainPart = str_replace('<w:t>{{</w:t>', '', $this->tempDocumentMainPart);*/
       //  $this->tempDocumentMainPart = str_replace('<w:t>}}</w:t>', $toAddImg, $this->tempDocumentMainPart);
        //
        //print $this->tempDocumentMainPart;
        
     //   $this->setValue($strKey, $toAddImg);


        if ($this->_rels == "")
        {
            $this->_rels = $this->zipClass->getFromName('word/_rels/document.xml.rels');
          // $this->_types = $this->zipClass->getFromName('[Content_Types].xml');
        }
        if ($this->_types == "")
        {
           $this->_types = $this->zipClass->getFromName('[Content_Types].xml');
        }

        $this->_types = str_replace('</Types>', $toAddType, $this->_types) . '</Types>';

        $this->_rels = str_replace('</Relationships>', $toAdd, $this->_rels) . '</Relationships>';
        return $countrels;
    }

    /**
     * Returns array of all variables in template.
     *
     * @return string[]
     */
    public function getVariables()
    {
        $variables = $this->getVariablesForPart($this->tempDocumentMainPart);

        foreach ($this->tempDocumentHeaders as $headerXML)
        {
            $variables = array_merge($variables, $this->getVariablesForPart($headerXML));
        }

        foreach ($this->tempDocumentFooters as $footerXML)
        {
            $variables = array_merge($variables, $this->getVariablesForPart($footerXML));
        }

        return array_values($variables);
    }

    /**
     * Clone a table row in a template document.
     *
     * @param string $search
     * @param int $numberOfClones
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function cloneRow($search, $numberOfClones)
    {
        if ('{{' !== substr($search, 0, 2) && '}}' !== substr($search, -2))
        {
            $search = '{{' . $search . '}}';
        }
        preg_match_all('/\\{\{(.*?)}\}/i', $this->tempDocumentMainPart, $matches);

        $tagPos=false;
        foreach ($matches[1] as $key => $value)
        {
            if($search=='{{'.strip_tags($value).'}}')
            {              
              if($value!="")
              {
                $tagPos =strpos($this->tempDocumentMainPart, $value);
                }     
            }
         
        }
        

        if (!$tagPos)
        {
          //  throw new Exception('Can not clone row, template variable not found or variable contains markup.');
            return false;
        }
        if($numberOfClones==0)
        {
          
            return false;
        }
        $rowStart = $this->findRowStart($tagPos);
        $rowEnd = $this->findRowEnd($tagPos);
        

        $xmlRow = $this->getSlice($rowStart, $rowEnd);

        // Check if there's a cell spanning multiple rows.
        if (preg_match('#<w:vMerge w:val="restart"/>#', $xmlRow))
        {
            // $extraRowStart = $rowEnd;
            $extraRowEnd = $rowEnd;
            while (true)
            {
                $extraRowStart = $this->findRowStart($extraRowEnd + 1);
                $extraRowEnd = $this->findRowEnd($extraRowEnd + 1);

                // If extraRowEnd is lower then 7, there was no next row found.
                if ($extraRowEnd < 7)
                {
                    break;
                }

                // If tmpXmlRow doesn't contain continue, this row is no longer part of the spanned row.
                $tmpXmlRow = $this->getSlice($extraRowStart, $extraRowEnd);
                if (!preg_match('#<w:vMerge/>#', $tmpXmlRow) &&
                        !preg_match('#<w:vMerge w:val="continue" />#', $tmpXmlRow))
                {
                    break;
                }
                // This row was a spanned row, update $rowEnd and search for the next row.
                $rowEnd = $extraRowEnd;
            }
            $xmlRow = $this->getSlice($rowStart, $rowEnd);
        }

        $result = $this->getSlice(0, $rowStart);
        for ($i = 1; $i <= $numberOfClones; $i++)
        {
            $result .= preg_replace('/\{\{(.*?)\}\}/', '{{\\1#' . $i . '}}', $xmlRow);
        }
        $result .= $this->getSlice($rowEnd);

        $this->tempDocumentMainPart = $result;
        return true;
    }

    /**
     * Clone a block.
     *
     * @param string $blockname
     * @param int $clones
     * @param bool $replace
     *
     * @return string|null
     */
    public function cloneBlock($blockname, $clones = 1, $replace = true)
    {
        $xmlBlock = null;
        preg_match(
                '/(<\?xml.*)(<w:p.*>\${' . $blockname . '}<\/w:.*?p>)(.*)(<w:p.*\${\/' . $blockname . '}<\/w:.*?p>)/is', $this->tempDocumentMainPart, $matches
        );

        if (isset($matches[3]))
        {
            $xmlBlock = $matches[3];
            $cloned = array();
            for ($i = 1; $i <= $clones; $i++)
            {
                $cloned[] = $xmlBlock;
            }

            if ($replace)
            {
                $this->tempDocumentMainPart = str_replace(
                        $matches[2] . $matches[3] . $matches[4], implode('', $cloned), $this->tempDocumentMainPart
                );
            }
        }

        return $xmlBlock;
    }

    /**
     * Replace a block.
     *
     * @param string $blockname
     * @param string $replacement
     */
    public function replaceBlock($blockname, $replacement)
    {
        preg_match(
                '/(<\?xml.*)(<w:p.*>\${' . $blockname . '}<\/w:.*?p>)(.*)(<w:p.*\${\/' . $blockname . '}<\/w:.*?p>)/is', $this->tempDocumentMainPart, $matches
        );

        if (isset($matches[3]))
        {
            $this->tempDocumentMainPart = str_replace(
                    $matches[2] . $matches[3] . $matches[4], $replacement, $this->tempDocumentMainPart
            );
        }
    }

    /**
     * Delete a block of text.
     *
     * @param string $blockname
     */
    public function deleteBlock($blockname)
    {
        $this->replaceBlock($blockname, '');
    }

    /**
     * Saves the result document.
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     *
     * @return string
     */
    public function save()
    {

        foreach ($this->tempDocumentHeaders as $index => $xml)
        {
            $this->zipClass->addFromString($this->getHeaderName($index), $xml);
        }

        $this->zipClass->addFromString($this->getMainPartName(), $this->tempDocumentMainPart);
        if ($this->_rels != "")
        {
            $this->zipClass->addFromString('word/_rels/document.xml.rels', $this->_rels);
        }
        if ($this->_types != "")
        {
            $this->zipClass->addFromString('[Content_Types].xml', $this->_types);
        }
        foreach ($this->tempDocumentFooters as $index => $xml)
        {
            $this->zipClass->addFromString($this->getFooterName($index), $xml);
        }

        // Close zip file
        if (false === $this->zipClass->close())
        {
            throw new Exception('Could not close zip file.');
        }

        return $this->tempDocumentFilename;
    }

    /**
     * Saves the result document to the user defined file.
     *
     * @since 0.8.0
     *
     * @param string $fileName
     */
    public function saveAs($fileName)
    {
        $tempFileName = $this->save();

        if (file_exists($fileName))
        {
            unlink($fileName);
        }

        /*
         * Note: we do not use `rename` function here, because it loses file ownership data on Windows platform.
         * As a result, user cannot open the file directly getting "Access denied" message.
         *
         * @see https://github.com/PHPOffice/PHPWord/issues/532
         */
        copy($tempFileName, $fileName);
        unlink($tempFileName);
    }

    /**
     * Finds parts of broken macros and sticks them together.
     * Macros, while being edited, could be implicitly broken by some of the word processors.
     *
     * @param string $documentPart The document part in XML representation
     *
     * @return string
     */
    protected function fixBrokenMacros($documentPart)
    {
        $fixedDocumentPart = $documentPart;

        $fixedDocumentPart = preg_replace_callback(
                '|\$[^{]*\{[^}]*\}|U', function ($match)
        {
            return strip_tags($match[0]);
        }, $fixedDocumentPart
        );

        return $fixedDocumentPart;
    }

    /**
     * Find and replace macros in the given XML section.
     *
     * @param mixed $search
     * @param mixed $replace
     * @param string $documentPartXML
     * @param int $limit
     *
     * @return string
     */
    protected function setValueForPart($search, $replace, $documentPartXML, $limit)
    {
        // Note: we can't use the same function for both cases here, because of performance considerations.
        if (self::MAXIMUM_REPLACEMENTS_DEFAULT === $limit)
        {
               
                //preg_match_all('/\{.*?\{.*?\}.*?\}/', $documentPartXML, $matches); 
                preg_match_all('/\{[^}]*\{.*?\}[^{]*\}/', $documentPartXML, $matches);   

                foreach ($matches[0] as $key => $value)
                {
                    
                    if($search==strip_tags($value))
                    {                   
                        if(strpos($replace,'<w:tbl>')===0)
                        {
                            //тут значит таблица и надо узнать позицию
                            $tgpos=strpos($documentPartXML,$value);
                            $wrS=$this->findwpStart($tgpos);
                            $wrF=$this->findwpFinish($tgpos);
                            $slice=$this->getSlice($wrS, $wrF);                            
                            $documentPartXML=str_replace($slice, $replace, $documentPartXML); 
                        }else
                        {
                            $documentPartXML=str_replace($value, $replace, $documentPartXML); 
                        }
                                               
                    }
                 
                }
                    
            return str_replace($search, $replace, $documentPartXML);
        }
        $regExpEscaper = new RegExp();

        return preg_replace($regExpEscaper->escape($search), $replace, $documentPartXML, $limit);
    }

    /**
     * Find all variables in $documentPartXML.
     *
     * @param string $documentPartXML
     *
     * @return string[]
     */
    protected function getVariablesForPart($documentPartXML)
    {
        preg_match_all('/\{[^}]*\{(.*?)\}[^{]*\}/', $documentPartXML, $matches);

        foreach ($matches[1] as $key => $value) {
          $matches[1][$key]=strip_tags($value);
        }
        return $matches[1];
    }

    protected function getCommentsName()
    {
        return 'word/comments.xml';
    }

    protected function getRelsDocName()
    {
        return 'word/_rels/document.xml.rels';
    }

    /**
     * Get the name of the header file for $index.
     *
     * @param int $index
     *
     * @return string
     */
    protected function getHeaderName($index)
    {
        return sprintf('word/header%d.xml', $index);
    }

    /**
     * @return string
     */
    protected function getMainPartName()
    {
        return 'word/document.xml';
    }

    /**
     * Get the name of the footer file for $index.
     *
     * @param int $index
     *
     * @return string
     */
    protected function getFooterName($index)
    {
        return sprintf('word/footer%d.xml', $index);
    }

    /**
     * Find the start position of the nearest table row before $offset.
     *
     * @param int $offset
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     *
     * @return int
     */
    protected function findRowStart($offset)
    {
        $rowStart = strrpos($this->tempDocumentMainPart, '<w:tr ', ((strlen($this->tempDocumentMainPart) - $offset) * -1));

        if (!$rowStart)
        {
            $rowStart = strrpos($this->tempDocumentMainPart, '<w:tr>', ((strlen($this->tempDocumentMainPart) - $offset) * -1));
        }
        if (!$rowStart)
        {
         //   throw new Exception('Can not find the start position of the row to clone.');
        }

        return $rowStart;
    }

    /**
     * Find the end position of the nearest table row after $offset.
     *
     * @param int $offset
     *
     * @return int
     */
    protected function findRowEnd($offset)
    {
        return strpos($this->tempDocumentMainPart, '</w:tr>', $offset) + 7;
    }

    /**
     * Get a slice of a string.
     *
     * @param int $startPosition
     * @param int $endPosition
     *
     * @return string
     */
    protected function getSlice($startPosition, $endPosition = 0)
    {
        if (!$endPosition)
        {
            $endPosition = strlen($this->tempDocumentMainPart);
        }

        return substr($this->tempDocumentMainPart, $startPosition, ($endPosition - $startPosition));
    }

    public function deleteRow($search)
    {
        if ('${' !== substr($search, 0, 2) && '}' !== substr($search, -1))
        {
            $search_start = '${' . $search . '}';
        }

        $tagPos = strpos($this->tempDocumentMainPart, $search_start);

        if (!$tagPos)
        {

            return FALSE;
        }

        $search_end = '${/' . $search . '}';
        $tagPos_end = strpos($this->tempDocumentMainPart, $search_end);
        if (!$tagPos_end)
        {
            $tagPos_end = $tagPos;
        }

        $rowStart = $this->findRowStart($tagPos);
        $rowEnd = $this->findRowEnd($tagPos_end);
        $xmlRow = $this->getSlice($rowStart, $rowEnd);

        $this->tempDocumentMainPart = str_replace($xmlRow, "", $this->tempDocumentMainPart);
        return TRUE;
    }

    public function deleteTable($search)
    {
        if ('${' !== substr($search, 0, 2) && '}' !== substr($search, -1))
        {
            $search = '${' . $search . '}';
        }

        $tagPos = strpos($this->tempDocumentMainPart, $search);

        if (!$tagPos)
        {

            return FALSE;
        }

        $search_end = '${/' . $search . '}';
        $tagPos_end = strpos($this->tempDocumentMainPart, $search_end);
        if (!$tagPos_end)
        {
            $tagPos_end = $tagPos;
        }

        $rowStart = $this->findTableStart($tagPos);
        $rowEnd = $this->findTableEND($tagPos_end);
        $xmlRow = $this->getSlice($rowStart, $rowEnd);

        $this->tempDocumentMainPart = str_replace($xmlRow, "", $this->tempDocumentMainPart);
        return TRUE;
    }

    public function InsertMarkInTable($search)
    {
        if ('${' !== substr($search, 0, 2) && '}' !== substr($search, -1))
        {
            $search_start = '${' . $search . '}';
        }

        $tagPos = strpos($this->tempDocumentMainPart, $search_start);

        if (!$tagPos)
        {

            return FALSE;
        }

        $search_end = '${/' . $search . '}';
        $tagPos_end = strpos($this->tempDocumentMainPart, $search_end);
        if (!$tagPos_end)
        {
            $tagPos_end = $tagPos;
        }

        $rowStart = $this->findRowStart($tagPos);
        $rowEnd = $this->findRowEnd($tagPos_end);

        $xmlRow1 = $this->getSlice($rowStart, $rowEnd);
        $i = 0;
        $count = 0;
        $xmlRow = $xmlRow1;
        //Разбиваем на ячейки
        $arrCell = array();

        while (1)
        {
            $posTD = strpos($xmlRow, "<w:tc>");
            if ($posTD === false)
            {
                break;
            }
            $posFinTD = strpos($xmlRow, "</w:tc>");
            $arrCell[] = array("text" => substr($xmlRow, $posTD, $posFinTD + 7 - $posTD));
            $xmlRow = substr($xmlRow, $posFinTD + 7);
        }
        $xmlRow = $xmlRow1;
        foreach ($arrCell as $key => $value)
        {
            //если метки нет, то заходим в ячейку
            if (strpos($value['text'], '${' . $search . '}') === false)
            {
                $wpStart = strpos($value['text'], "<w:p");
                $wpFin = strpos($value['text'], "</w:p>");
                $tmpWP = substr($value['text'], $wpStart, $wpFin + 6 - $wpStart);
                $wrStart = strpos($tmpWP, "<w:r>");
                $wrFin = strpos($tmpWP, "</w:r>");
                if ($wrStart === false)
                {
                    $wrPrStart = strpos($tmpWP, "<w:rPr>");
                    $wrPrFin = strpos($tmpWP, "</w:rPr>");
                    $wrPr = "";
                    if ($wrPrStart !== false)
                    {
                        $wrPr = substr($tmpWP, $wrPrStart, $wrPrFin + 8 - $wrPrStart);
                    }

                    $wt = '<w:r>' . $wrPr . '<w:t>${' . $search . "_" . $key . "}</w:t></w:r>";
                    $tmpWPnew = str_replace("</w:p>", $wt . "</w:p>", $tmpWP);
                    $value['textN'] = str_replace($tmpWP, $tmpWPnew, $value['text']);
                    $xmlRow1 = str_replace($value['text'], $value['textN'], $xmlRow1);
                }
            }
        }
        $this->tempDocumentMainPart = str_replace($xmlRow, $xmlRow1, $this->tempDocumentMainPart);
        return TRUE;
    }

    protected function findTableStart($offset)
    {
        $rowStart = strrpos($this->tempDocumentMainPart, '<w:tbl ', ((strlen($this->tempDocumentMainPart) - $offset) * -1));

        if (!$rowStart)
        {
            $rowStart = strrpos($this->tempDocumentMainPart, '<w:tbl>', ((strlen($this->tempDocumentMainPart) - $offset) * -1));
        }
        if (!$rowStart)
        {
            throw new Exception('Can not find the start position of the row to clone.');
        }

        return $rowStart;
    }

    protected function findTableEND($offset)
    {
        return strpos($this->tempDocumentMainPart, '</w:tbl>', $offset) + 8;
    }

    protected function findRelStart($offset)
    {
        $rowStart = strrpos($this->tempDocumentComments, '<w:comment ', ((strlen($this->tempDocumentComments) - $offset) * -1));
        return $rowStart;
    }

    protected function findwtStart($offset)
    {
        $rowStart = strrpos($this->tempDocumentMainPart, '<w:t', ((strlen($this->tempDocumentMainPart) - $offset) * -1));
        return $rowStart;
    }
    protected function findwtFinish($offset)
    {
        return strpos($this->tempDocumentMainPart, '</w:t>', $offset) + 6;
    }
    protected function findwpStart($offset)
    {
        $rowStart = strrpos($this->tempDocumentMainPart, '<w:p ', ((strlen($this->tempDocumentMainPart) - $offset) * -1));
        return $rowStart;
    }

    protected function findwpFinish($offset)
    {
        return strpos($this->tempDocumentMainPart, '</w:p>', $offset) + 6;
    }


    public function insertcantSplitTable()
    {
        $iterr=0;

        while(true)
        {
            $iterr++;
            $search='cantSplit';
            if ('{{' !== substr($search, 0, 2) && '}}' !== substr($search, -1))
            {
                $search_start = '{{' . $search . '}}';
            }

            $tagPos = strpos($this->tempDocumentMainPart, $search_start);
            if($iterr>50)
            {
                 return FALSE;
            }
            if (!$tagPos||$iterr>50)
            {
                $tagPos = strpos($this->tempDocumentMainPart,'<w:t>'.$search.'</w:t>');
                if(!$tagPos)
                {
                    return FALSE;
                }                
            }
            $tableStart=$this->findTableStart( $tagPos);
            $tableFinish=$this->findTableEND( $tagPos);
            $xmlTableOrig = $this->getSlice($tableStart, $tableFinish);
            $xmlTableReplace=str_replace('<w:tc>', '<w:trPr><w:cantSplit /></w:trPr><w:tc>', $xmlTableOrig);

            
            $this->tempDocumentMainPart=str_replace($xmlTableOrig, $xmlTableReplace, $this->tempDocumentMainPart);
            $this->setValue($search,'', 1);
        }
        
    }
    
}
