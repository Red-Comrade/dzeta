<?php

class ElasticEmail
{
    const URL='https://api.elasticemail.com/v2/';

    const filePathMain=BASE_PATH.'/config/dop/elasticemail.ini';


    public static function sendMail($subject, $mailTo, $bodyHTML, $bodyText='',$from='')
    {
        $mail=false;
        if(file_exists(self::filePathMain))
        {
            $sett=parse_ini_file(self::filePathMain,true);
            if(isset($sett['elasticMail']))
            {
              $elastic=$sett['elasticMail'];
              if(isset($elastic['from'])&&isset($elastic['fromName'])&&isset($elastic['apiKey']))
              {
                  try
                  {
                      $post = [
                              'from' => empty($from)?$elastic['from']:$from,
                              'fromName' => $elastic['fromName'],
                              'apikey' => $elastic['apiKey'],
                              'subject' => $subject,
                              'to' => $mailTo,
                              'bodyHtml' => $bodyHTML,
                              'bodyText' => $bodyText,
                              'isTransactional' => false];
                      $ch = curl_init();
                      curl_setopt_array($ch, array(
                              CURLOPT_URL =>  self::URL.'email/send',
                              CURLOPT_POST => true,
                              CURLOPT_POSTFIELDS => $post,
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_HEADER => true,
                              CURLOPT_SSL_VERIFYPEER => false
                          ));
                      $result = curl_exec($ch);
                      $info = curl_getinfo($ch);
                      curl_close($ch);                     
                      if ($info['http_code']==200)
                      {
                          $result= substr($result, strpos($result, "{"));
                          $result=json_decode($result, true);
                           \DZ::lgkhmh( $result);
                          if (isset($result['success'])&&$result['success']===true)
                          {
                              $mail=$result['data']['messageid'];
                          }
                      }
                  } catch (Exception $ex)
                  {
                      $mail=false;
                  }
              }
          }
        }
        return $mail;
    }

    public static function getStatusEmails($date_from,$date_to,$status=0,$offset=0,$limit=100)
    {
        $returnData=['status'=>'err','data'=>[]];
        if(file_exists(self::filePathMain))
        {
            $sett=parse_ini_file(self::filePathMain,true);
            if(isset($sett['elasticMail']))
            {
              $elastic=$sett['elasticMail'];
              if(isset($elastic['from'])&&isset($elastic['fromName'])&&isset($elastic['apiKey']))
              {
                  try
                  {
                      $data = array(
                          'apikey' => $elastic['apiKey'],
                          'from' => $date_from->format('Y-m-d'),
                          'to' => $date_to->format('Y-m-d'),
                          'limit' => $limit,
                          'offset' => $offset,
                          'statuses'=>$status
                      );
                     
                      $ch = curl_init();
                      curl_setopt_array($ch, array(
                              CURLOPT_URL =>  self::URL.'log/events?'.http_build_query($data),
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_HEADER => true,
                              CURLOPT_SSL_VERIFYPEER => false
                          ));
                      $result = curl_exec($ch);
                      $info = curl_getinfo($ch);
                      curl_close($ch);                     
                      if ($info['http_code']==200)
                      {
                          $result= substr($result, strpos($result, "{"));
                          $result=json_decode($result, true);
                           $returnData['status']='good';
                          foreach ($result['data']['recipients'] as $key => $value) {
                             $returnData['data'][]=$value;
                          }
                      }
                  } catch (Exception $ex)
                  {
                     $returnData=['status'=>'err','data'=>[]];
                  }
              }
          }
        }
        return  $returnData;
    }
}
