<?php

namespace Dzeta\Library;

class DbError
{

  public static function getError($name,$result,$params=[],$lang='',$uniqCode='') 
  {
		$cook = \Phalcon\Di::getDefault()->getCookies();
    if(empty($lang))
    {
      if($cook->has('dzetalang'))
      {
        $lang=$cook->get('dzetalang')->getValue();
      }else 
      {
        $lang='en';
      }
    }
    switch ($lang) {
      case 'ru':
      case 'en':
      case 'fr':
        $lang=$lang;
        break;
      default:
        $lang='en';
        break;
    }
    $dirPath=__DIR__.'/ErrDB/';
    $errText=null;    
    if(file_exists($dirPath.$lang.'.json'))
    {      
      $langErr= json_decode(file_get_contents($dirPath.$lang.'.json'),true);   

      if($result==0)
      {
        if(isset($langErr['_Global']))
        {
          if(!empty($langErr['_Global']['error'][0]))
          {
            $errText=$langErr['_Global']['error'][0];
            $errText=str_replace("{0}",$name,$errText);
          }
        }
      }else if($result==-1)
      {
        $errAll=\Dzeta\Library\Db::getErrors();
        if(!empty($errAll))
        {
          $lV=array_pop($errAll);
          $uniqCode='_'.$lV['key'];
        }
      }else 
      {
        if(isset($langErr[$name]))
        {
          if(!empty($langErr[$name]['error'][$result]))
          {
            $errText=$langErr[$name]['error'][$result];            
          }
        }
      }
    }else 
    {
      switch ($lang) 
      {
        case 'ru':
          $errText='Файл с языком не найден';
          break;
        case 'en':
          $errText='Language file not found';
          break;
        case 'fr':
          $errText='Fichier de langue introuvable';
          break;
      }
    }
    if(is_null($errText))
    {
      switch ($lang) {
        case 'ru':
          $errText='Неизвестная ошибка';
          break;
        case 'en':
          $errText='Unknown error';
          break;
        case 'fr':
          $errText='Erreur inconnue';
          break;
      }
    }
    foreach ($params as $key => $value) 
    {
      $errText=str_replace("{".$key."}",$value,$errText);
    }
    $errText=$errText.' :'.$uniqCode;
		return $errText;
	}

}
