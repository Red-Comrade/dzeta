<?php

/*
	Example of usage
	$keys = new KeysSoLib('api_key');
	$data = $keys->searchStat('stroimaterial-deshevo.ru');
*/

class KeysSoLib {

	const BASE_URL = 'https://api.keys.so/';
	const DOMAIN_DASHBOARD_URL = 'report/simple/domain_dashboard';
	const ORGANIC_KEYWORDS_URL = 'report/simple/organic/keywords';

	const MSK_YANDEX_BASE = 'msk';
	const MSK_GOOGLE_BASE = 'gru';

	const ASC_ORDER = 'asc';
	const DESC_ORDER = 'desc';

	private $key;
	private $log;

	public function __construct($key, $log = null) {
		$this->key = $key;
		$this->log = $log;
	}

	public static function parseDomain($domain) {
		$domain = strtolower($domain);
		$domain = str_replace(['https://', 'http://'], '', $domain);
		$domain = parse_url('http://' . $domain, PHP_URL_HOST);
		$domain = str_replace('www.', '', $domain);
		return $domain;
	}

	public function domainDashboard($base = 'none', $domain = 'none') {
		$url = self::BASE_URL . self::DOMAIN_DASHBOARD_URL;
		$result = $this->json(
			$url,
			'GET',
			[
				'base' => $base,
				'domain' => $domain,
				'auth-token' => $this->key,
			],
			[],
			[],
		);
		return $result;
	}

	public function organicKeywords($base = 'none', $domain = 'none', $filter = []) {
		$url = self::BASE_URL . self::ORGANIC_KEYWORDS_URL;
		$sort = isset($filter['sort']) ? $filter['sort'] : 'pos';
		$sort .= '|' . (isset($filter['order']) ? $filter['order'] : self::ASC_ORDER);
		$page = isset($filter['page']) ? $filter['page'] : 1;
		$perPage = isset($filter['per_page']) ? $filter['per_page'] : 25;
		$result = $this->json(
			$url,
			'GET',
			[
				'base' => $base,
				'domain' => $domain,
				'sort' => $sort,
				'page' => $page,
				'per_page' => $perPage,
				'auth-token' => $this->key,
			],
			[],
			[],
		);
		return $result;
	}

	public function searchStat($domain, $sort = []) {
		$filter = [
			'page' => 1,
			'per_page' => 1,
			'sort' => isset($sort['sort']) ? $sort['sort'] : 'pos',
			'order' => isset($sort['order']) ? $sort['order'] : self::ASC_ORDER,
		];
		$yandex = $this->organicKeywords(self::MSK_YANDEX_BASE, $domain, $filter);
		$google = $this->organicKeywords(self::MSK_GOOGLE_BASE, $domain, $filter);
		if (isset($yandex['total'])) {
			$filter['per_page'] = $yandex['total'];
			$yandex = $this->organicKeywords(self::MSK_YANDEX_BASE, $domain, $filter);
		}
		if (isset($google['total'])) {
			$filter['per_page'] = $google['total'];
			$google = $this->organicKeywords(self::MSK_GOOGLE_BASE, $domain, $filter);
		}
		$yandexWords = [];
		if (isset($yandex['data'])) {
			$yandexWords = array_combine(array_column($yandex['data'], 'word'), $yandex['data']);
		}
		$googleWords = [];
		if (isset($google['data'])) {
			$googleWords = array_combine(array_column($google['data'], 'word'), $google['data']);
		}
		$stat = [];
		foreach ($yandexWords as $word => $value) {
			if (!isset($stat[$word])) {
				$stat[$word] = [
					'word' => $word,
					'yandex' => [
						'ws' => $value['ws'],
						'pos' => $value['pos'],
						'url' => $value['url'],
					],
					'google' => [
						'ws' => 0,
						'pos' => 0,
						'url' => '',
					]
				];
				if (isset($googleWords[$word])) {
					$stat[$word]['google'] = [
						'ws' => $googleWords[$word]['ws'],
						'pos' => $googleWords[$word]['pos'],
						'url' => $googleWords[$word]['url'],
					];
					unset($googleWords[$word]);
				}
			}
		}
		foreach ($googleWords as $word => $value) {
			if (!isset($stat[$word])) {
				$stat[$word] = [
					'word' => $word,
					'yandex' => [
						'ws' => 0,
						'pos' => 0,
						'url' => ''
					],
					'google' => [
						'ws' => $value['ws'],
						'pos' => $value['pos'],
						'url' => $value['url'],
					]
				];
			}
		}
		$stat = array_values($stat);
		return $stat;
	}

	public function tops($domain) {
		$yandex = $this->domainDashboard(self::MSK_YANDEX_BASE, $domain);
		$google = $this->domainDashboard(self::MSK_GOOGLE_BASE, $domain);
		$tops = [
			'yandex' => [
				'top1' => isset($yandex['it1']) ? $yandex['it1'] : 0,
				'top3' => isset($yandex['it3']) ? $yandex['it3'] : 0,
				'top5' => isset($yandex['it5']) ? $yandex['it5'] : 0,
				'top10' => isset($yandex['it10']) ? $yandex['it10'] : 0,
				'top50' => isset($yandex['it50']) ? $yandex['it50'] : 0,
			],
			'google' => [
				'top1' => isset($google['it1']) ? $google['it1'] : 0,
				'top3' => isset($google['it3']) ? $google['it3'] : 0,
				'top5' => isset($google['it5']) ? $google['it5'] : 0,
				'top10' => isset($google['it10']) ? $google['it10'] : 0,
				'top50' => isset($google['it50']) ? $google['it50'] : 0,
			]
		];
		return $tops;
	}

	public function countTops($words) {
		$tops = [
			'yandex' => [
				'top10' => 0,
				'top50' => 0,
			],
			'google' => [
				'top10' => 0,
				'top50' => 0,
			]
		];
		foreach ($words as $word) {
			if ($word['yandex']['pos'] <= 10 && $word['yandex']['pos'] !== 0) {
				$tops['yandex']['top10']++;
			}
			if ($word['yandex']['pos'] <= 50 && $word['yandex']['pos'] !== 0) {
				$tops['yandex']['top50']++;
			}
			if ($word['google']['pos'] <= 10 && $word['google']['pos'] !== 0) {
				$tops['google']['top10']++;
			}
			if ($word['google']['pos'] <= 50 && $word['google']['pos'] !== 0) {
				$tops['google']['top50']++;
			}
		}
		return $tops;
	}

	private function json($url, $method, $request, $headers, $options) {
		$result = $this->curl($url, $method, $request, $headers, $options, true);
		$data = [];
		if ($this->log && $result['code'] != 200) {
			file_put_contents($this->log, json_encode(['date' => time(), 'data' => $result, 'url' => $url, 'request' => $request]) . PHP_EOL, FILE_APPEND);
		}
		if ($result['code'] == 200) {
			$data = json_decode($result['body'], true);
		} else if ($result['code'] == 202) {
			usleep(500000);
			$data = $this->json($url, $method, $request, $headers, $options);
		}
		return $data;
	}

	private function curl($url, $method, $request, $headers, $options, $get_code = false) {
		if ($method == 'GET' && !empty($request)) {
			$url .= '?' . http_build_query($request);
		}

		$curl = curl_init($url);

		if ($method == 'POST' && !empty($request)) {
			curl_setopt($curl, CURLOPT_POSTFIELDS, is_array($request) ? http_build_query($request) : $request);
		}

		if (!empty($headers)) {
			$_headers = [];
			foreach ($headers as $key => $header) {
				$_headers[] = $key . ': ' . $header;
			}
			curl_setopt($curl, CURLOPT_HTTPHEADER, $_headers);
		}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0);
		curl_setopt($curl, CURLOPT_TIMEOUT, 40);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
		if ($get_code) {
			curl_setopt($curl, CURLOPT_HEADER, true);
		}
		foreach ($options as $key => $option) {
			curl_setopt($curl, $key, $option);
		}

		$response = curl_exec($curl);
		$code = null;
		if ($get_code) {
			$headers_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
			$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$response = substr($response, $headers_size);
		}
		curl_close($curl);
		return $get_code ? ['code' => $code, 'body' => $response] : $response;
	}
}