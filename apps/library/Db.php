<?php

namespace Dzeta\Library;

use Dzeta\Core;

class Db
{
	const INITIAL_CODE = -1;
	const SUCCESS_CODE = 1;
	private static $errorsDataBase=[];

	public static function _escapeValue($value)
	{
		if(is_array($value))
		{
			foreach ($value as $key => $val)
			{
				$value[self::_escapeValue($key)]=self::_escapeValue($val);
			}
			return $value;
		}elseif(is_null($value))
		{
			return null;
		}else 
		{
			return pg_escape_string($value);
		}
	}
	public static function runProcedure($settings, $data) 
	{
		$time=microtime(true);
		$queryParams = [];

		foreach ($settings['params'] as $key => $param) 
		{
			if (array_key_exists($key, $data)) 
			{
				if (is_null($data[$key])) 
				{
					$queryParams[] = 'null';
				}else 
				{
					if ($param['type'] == Core\Type\DataType::TYPE_NUMBER || $param['type'] == Core\Type\DataType::TYPE_STRING) 
					{
						$tmp=self::_escapeValue($data[$key]);
						$queryParams[] = "'{$tmp}'";
					}elseif($param['type'] == Core\Type\DataType::TYPE_JSON)
					{
						$tmp=json_encode(self::_escapeValue($data[$key]),JSON_UNESCAPED_UNICODE);
						$queryParams[] = "'{$tmp}'";
					}elseif($param['type'] == Core\Type\DataType::TYPE_ARRAY)
					{
						$tmp='{"'.implode('","',self::_escapeValue($data[$key])).'"}';
						$queryParams[] = "'{$tmp}'";
					}
				}
			}
		}

		$query = 'SELECT *  FROM public."' . $settings['name'] . '"(' . implode(',', $queryParams) . ')';

		if ($settings['debug']) 
		{
			$debug = $settings['name'] . ' query';
			\DZ::$debug($query);
		}

		$queryResult = [];
		$config = \Phalcon\Di::getDefault()->getShared('config');
		if ($config->dzeta->useDB) 
		{
			$connection = \Phalcon\Di::getDefault()->getShared('db');
			try
			{
				$sql = $connection->query($query);
				$queryResult = $sql->fetchAll(\PDO::FETCH_ASSOC);
			} catch (\Exception $e) 
			{
				$keyIDd=uniqid();
				$arrErr=[
					'date'=>date('Y-m-d H:i:s'),
					'message'=>$e->getMessage(),
					'query'=>$query,
					'key'=>$keyIDd,
					'php'=>[
						'sett'=>$settings,
						'data'=>$data
					]
				];
				self::$errorsDataBase[]=$arrErr;
				$fd = fopen(BASE_PATH."/ufiles/_errDB.txt", 'a');
				fwrite($fd, json_encode($arrErr). "\n");
				fclose($fd);
			}
		}else 
		{
			$queryResult = MockDb::getData($settings['name']);
		}

		if ($settings['debug']) 
		{
			$debug = $settings['name'] . ' data';
			\DZ::$debug($queryResult);
			$debug = $settings['name'] . ' time';
			\DZ::$debug(microtime(true)-$time);
		}

		return $queryResult;
	}

	public static function getErrors()
	{
		return self::$errorsDataBase;
	}
	public static function clearErrors()
	{
		self::$errorsDataBase=[];
		return true;
	}
}
