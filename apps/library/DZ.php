<?php
class DZ {

    public function __construct()
    {

    }

    public static function debug($data, $tags = null)
    {
        $config = \Phalcon\Di::getDefault()->getShared('config');
        if($config->dzeta->debug)
        {
          $tags = $tags ?? 'debug';
          \PC::$tags($data);
          \FB::log($data, $tags);
        }
    }

    public static function __callStatic($tags, $args)
    {
        static::debug(isset($args[0]) ? $args[0] : null, $tags);
    }
}
