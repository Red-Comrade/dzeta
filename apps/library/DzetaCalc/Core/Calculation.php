<?php

/*
TODO
- выделение названий атрибутов
- проверить смещения
- написание функций
- проблема $x[5]++;
*/
namespace DzetaCalcParser\Core;

use DzetaCalcParser\Core\Analytics;
use DzetaCalcParser\Core\AnalyticsDatabase;
use DzetaCalcParser\Core\Helper;
use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Functions\LogicOperations;
use DzetaCalcParser\Functions\MathOperations;

use Dzeta\Core\Instance;

class Calculation
{
	const MAX_ITERATIONS = 50000000;

	private $forMark = null;
	private $globalMark = null;

	private $variables = [];
	private $lastValue = null;
	private $formAttrs = [];
	private $loopIndexes = [];

	private $functions = [];
	private $methods = [];
	private $errors = ['warnings' => [], 'errors' => []];

	private $formulaTree = [];
	private $formula = '';

	private $databaseAnalytics = null;
	private $databaseCache = [];

	private $saveHistory = false;

	private $user = null;

	/*
		context
		formula
		history
	*/
	public function __construct($data)
	{
		if(!empty($data['context']))
		{
			//пока временно это значит есть контекс
            $this->functions = $contextData['functions'];
            $this->formula = $contextData['formula'];
            $this->formulaTree = $contextData['tree'];
            $this->variables = $contextData['variables'];
            $contextPath = $contextData['path'];
		} else
		{
			$this->functions = $data['functions'];
			$this->methods = $data['methods'];
			$this->formula = $data['formula'];
			$this->formulaTree = Analytics::createTreeParseExpression($this->formula, [], $this);
		}

		if (isset($data['history']))
		{
			$this->saveHistory = $data['history'];
		}

		$object = null;
		$cache = [];
		if (isset($data['databaseCache']))
		{
			$cache = $data['databaseCache'];
		}
		if (isset($data['trigger']['object']))
		{
			$object = $data['trigger']['object'];
			$cache[] = $object;
		}
		if (isset($data['object']))
		{
			$object = $data['object'];
		}
		$arguments = [];
		if (isset($data['arguments']))
		{
			$arguments = $data['arguments'];
		}
		$this->initDefaultValues($object, $arguments);
		$this->initDatabaseCache($cache);

		if (isset($data['user']))
		{
			$this->user = $data['user'];
		}
	}

	public function getVariables()
	{
		return $this->variables;
	}

    public function getVariable($var)
    {
        return isset($this->variables[$var]) ? $this->variables[$var] : null;
    }

	public function getLastValue()
	{
		return $this->lastValue;
	}

	public function getFormAttrs()
	{
		return $this->formAttrs;
	}

    public function getErrors()
    {
        return $this->errors['errors'];
    }

    public function getErrorsAndWarnings()
    {
        return $this->errors;
    }

    public function getUser()
    {
    	return $this->user;
    }

    public function setGlobalMark($mark)
    {
    	$this->globalMark = $mark;
    }

    public function setLastValue($value)
    {
    	$this->lastValue = $value;
    }

    public function setFormAttr($obj, $attr, $value)
    {
    	$this->formAttrs[$obj] = isset($this->formAttrs[$obj]) ? $this->formAttrs[$obj] : [];
    	$this->formAttrs[$obj][$attr] = $value;
    }

    public function setAttrCache($objUid, $attrUid, $value)
    {
    	$this->databaseCache[$objUid][$attrUid] = $value;
    }

    public function getAttrCache($objUid, $attrUid)
    {
    	$result = ['found' => false, 'value' => null];
    	if (array_key_exists($objUid, $this->databaseCache) && array_key_exists($attrUid, $this->databaseCache[$objUid]))
    	{
    		$result['found'] = true;
    		$result['value'] = $this->databaseCache[$objUid][$attrUid];
    	}
    	return $result;
    }

    public function addError($text, $position, $props = [])
    {
        $this->errors['errors'][] = $this->formErrorValue($text, $position, $props);
    }

    public function addWarning($text, $position, $props = [])
    {
        $this->errors['warnings'][] = $this->formErrorValue($text, $position, $props);
    }

    public function run()
    {
		$context = [];
		$this->calcTree($this->formulaTree, [], $context);
    }

	private function calcTree($tree, $currentPath, &$contextPath)
	{
		$count = count($tree);
		$curIndex = 0;
		$copyPath = $currentPath;
		if (in_array($this->globalMark, [Constants::TYPE_ACTION_SAVE_CONTEXT, Constants::TYPE_ACTION_RETURN]))
		{
			return;
		}
		if (!empty($contextPath))
		{
			$curData = array_shift($contextPath);
			$curIndex = $curData['index'];
		}
		for (; $curIndex < $count; $curIndex++)
		{
			$currentPath = $copyPath;
			if ($tree[$curIndex]['type'] == Constants::TYPE_ACTION_SIMPLE)
			{
				$currentPath[] = ['index' => $curIndex];
				$this->calcBody(
					$tree[$curIndex]['body'],
					$tree[$curIndex]['ps'],
					$tree[$curIndex]['pf'],
					$tree[$curIndex]['rows'],
					$currentPath,
					$contextPath
				);
			} else if ($tree[$curIndex]['type'] == Constants::TYPE_ACTION_FOR)
			{
				$initialization = $tree[$curIndex]['initialisation'];
				if (!empty($tree[$curIndex]['initialisation']))
				{
					$curIndexInita = 0;
					$countInita = count($initialization['body']);
					for (; $curIndexInita < $countInita; $curIndexInita++)
					{
						$this->calcBody(
							$initialization['body'][$curIndexInita]['body'],
							$initialization['body'][$curIndexInita]['ps'],
							$initialization['body'][$curIndexInita]['pf'],
							$initialization['body'][$curIndexInita]['rows'],
							$currentPath,
							$contextPath
						);
					}
				}
				$counnIttt = 0;
				while(true)
				{
					$condition = $tree[$curIndex]['condition'];
					$flagCondtion = true;
					$counnIttt++;
					if (!empty($condition))
					{
						$curIndexCond = 0;
						$countCond = count($condition['body']);
						for (; $curIndexCond < $countCond; $curIndexCond++)
						{
							$answerr = $this->calcBody(
								$condition['body'][$curIndexCond]['body'],
								$condition['body'][$curIndexCond]['ps'],
								$condition['body'][$curIndexCond]['pf'],
								$condition['body'][$curIndexCond]['rows'],
								$currentPath,
								$contextPath
							);
							if (!is_null($answerr['answer']) && $flagCondtion)
							{
								$flagCondtion = false;
								if($answerr['answer']['type'] == Constants::TYPE_BOOL && $answerr['answer']['value'])
								{
									$flagCondtion = true;
								}
							}
						}
					}
					if ($counnIttt > self::MAX_ITERATIONS)
					{
						$flagCondtion = false;
						Error::addError(
			                ['general'],
			                ['calculation', 'too_much_loop'],
			                [
			                    'count' => self::MAX_ITERATIONS,
			                ],
			                $this,
			                [
			                    'ps' => $condition['ps'],
			                    'pf' => $condition['pf'],
			                    'rows' => $condition['rows'],
			                ]
			            );
					}
					if(empty($flagCondtion))
					{
                        $this->setLoopIndexes([]);
						break;
					}

					$bodyFor = $tree[$curIndex]['bodyFor'];
					if (!empty($bodyFor))
					{
						$this->calcTree($bodyFor['body'], $currentPath, $contextPath);
					}
					if (!is_null($this->forMark))
					{
						if ($this->forMark == Constants::TYPE_ACTION_FOR_CONTINUE)
						{
							$this->forMark = null;
						} else if ($this->forMark == Constants::TYPE_ACTION_FOR_BREAK)
						{
							$this->forMark = null;
	                        $this->setLoopIndexes([]);
							break;
						}
					}
					$modification = $tree[$curIndex]['modification'];
					if (!empty($modification))
					{
						$curIndexMod = 0;
						$countMod = count($modification['body']);
						for (; $curIndexMod < $countMod; $curIndexMod++)
						{
							$ans = $this->calcBody(
								$modification['body'][$curIndexMod]['body'],
								$modification['body'][$curIndexMod]['ps'],
								$modification['body'][$curIndexMod]['pf'],
								$modification['body'][$curIndexMod]['rows'],
								$currentPath,
								$contextPath
							);
                            if (!empty($ans['variable'])) {
                            	$index = $ans['answer'];
                            	$index['var'] = $ans['variable'];
                            	$this->setLoopIndex($ans['variable'], $index);
                            }
						}
					}
					if (count($this->getErrors()) > 0)
					{
						break;
					}
					if (in_array($this->globalMark, [Constants::TYPE_ACTION_SAVE_CONTEXT, Constants::TYPE_ACTION_RETURN]))
					{
						return;
					}
				}
			} else if ($tree[$curIndex]['type'] == Constants::TYPE_ACTION_IF)
			{
				$curIndexDopIf = 0;
				$dopIf = $tree[$curIndex]['dopif'];
				$countDopIf = count($dopIf);
				for (; $curIndexDopIf < $countDopIf; $curIndexDopIf++)
				{
					$cond = false;
					if ($dopIf[$curIndexDopIf]['type'] == 'if' || $dopIf[$curIndexDopIf]['type'] == 'elseif')
					{
						$condition = $dopIf[$curIndexDopIf]['condition'];
						//тут добавить некий for
						$indCond = 0;
						$countCond = count($condition['body']);
						for (; $indCond < $countCond; $indCond++)
						{
							if (!$cond)
							{
								$answerr = $this->calcBody(
									$condition['body'][$indCond]['body'],
									$condition['body'][$indCond]['ps'],
									$condition['body'][$indCond]['pf'],
									$condition['body'][$indCond]['rows'],
									$currentPath,
									$contextPath
								);
								if (isset($answerr['answer']['type']) && $answerr['answer']['type'] == Constants::TYPE_BOOL && $answerr['answer']['value'])
								{
									$cond = true;
								}
							}
						}
					} else if ($dopIf[$curIndexDopIf]['type'] == 'else')
					{
						$cond = true;
					}

					if ($cond)
					{
						$bodyCond = $dopIf[$curIndexDopIf]['body'];
						$this->calcTree(
							$bodyCond,
							$currentPath,
							$contextPath
						);
						break;
					}
				}
			}
			if (count($this->getErrors()) != 0)
			{
				return;
			}
			if (in_array($this->globalMark, [Constants::TYPE_ACTION_SAVE_CONTEXT, Constants::TYPE_ACTION_RETURN]))
			{
				return;
			}
			if (!is_null($this->forMark))
			{
				return;
			}
		}
	}

	private function calcBody($body, $ps, $pf, $rows, $currentPath, &$contextPath)
	{
		$curIndex = 0;
		$countAction = count($body['action']);

		$oper = [];
        $elem = 0;
        $copyPath = $currentPath;
		if (in_array($this->globalMark, [Constants::TYPE_ACTION_SAVE_CONTEXT, Constants::TYPE_ACTION_RETURN]))
		{
			return;
		}
        if (!empty($contextPath))
        {
        	$curData = array_shift($contextPath);
        	$oper = $curData['oper'];
        	$elem = $curData['elem'];
        	$curIndex = $curData['index'];
        }
		for (; $curIndex < $countAction; $curIndex++)
		{
			$currentPath = $copyPath;
			$currentPath[] = [
				'index' => $curIndex,
				'elem' => $elem,
				'oper' => $oper
			];
			if (count($this->getErrors()) != 0)
			{
				return;
			}
			if ($body['action'][$curIndex]['type'] == Constants::TYPE_OPER_SPC)
			{
				if (isset($oper[$elem - 2]) && isset($oper[$elem - 1]))
				{
					$operElems = [$oper[$elem - 2], $oper[$elem - 1]];
				} else
				{
					$operElems = [$oper[$elem - 1]];
				}
				$var_do = null;
				foreach ($operElems as $k_elems => $operElem)
				{
					if ($operElem['type'] == Constants::TYPE_VARIABLE)
					{
						$var_do = $operElem['value'];
						if (empty($operElem['arr']))
						{
							$v_var = $this->getVariable($operElem['value']);
						} else
						{
							$arr_var = $this->getVariable($operElem['value']);
							$v_var = $this->getArrayElement($arr_var, $operElem['arr']);
						}
						$operElems[$k_elems]['type'] = $v_var['type'];
						$operElems[$k_elems]['value'] = $v_var['value'];
					} else if ($operElem['type'] == Constants::TYPE_ACTION_FOR)
					{
						Error::addError(
			                ['general'],
			                ['calculation', 'unexpected_use'],
			                [
			                    'value' => $operElem['value'],
			                ],
			                $this,
			                [
			                    'ps' => $operElem['ps'],
			                    'pf' => $operElem['pf'],
			                    'rows' => $operElem['rows'],
			                ]
			            );
			            return;
					}
				}
				switch ($body['action'][$curIndex]['value'])
				{
					case '+':
						$oper[$elem - 2] = MathOperations::Additional($operElems[0], $operElems[1], $this);
						break;
					case '-':
						$oper[$elem - 2] = MathOperations::Subtraction($operElems[0], $operElems[1], $this);
						break;
					case '*':
						$oper[$elem - 2] = MathOperations::Multiplication($operElems[0], $operElems[1], $this);
						break;
					case '/':
						$oper[$elem - 2] = MathOperations::Division($operElems[0], $operElems[1], $this);
						break;
					case '<':
						$oper[$elem - 2] = LogicOperations::Less($operElems[0], $operElems[1], $this);
						break;
					case '>':
						$oper[$elem - 2] = LogicOperations::Great($operElems[0], $operElems[1], $this);
						break;
					case '<=':
						$oper[$elem - 2] = LogicOperations::LessEqual($operElems[0], $operElems[1], $this);
						break;
					case '>=':
						$oper[$elem - 2] = LogicOperations::GreatEqual($operElems[0], $operElems[1], $this);
						break;
					case '==':
						$oper[$elem - 2] = LogicOperations::Equal($operElems[0], $operElems[1], $this);
						break;
					case '!=':
						$oper[$elem - 2] = LogicOperations::NotEqual($operElems[0], $operElems[1], $this);
						break;
					case '&&':
						$oper[$elem - 2] = LogicOperations::LAND($operElems[0], $operElems[1], $this);
						break;
					case '||':
						$oper[$elem - 2] = LogicOperations::LOR($operElems[0], $operElems[1], $this);
						break;
					case '++':
						if (empty($var_do))
						{
							Error::addError(
				                ['general'],
				                ['calculation', 'not_var_decrement'],
				                [],
				                $this,
				                [
				                    'ps' => $ps,
				                    'pf' => $pf,
				                    'rows' => $rows,
				                ]
				            );
						} else
						{
							$answer = MathOperations::Increment($operElems[0], $this);
							$this->setVariable($var_do, $answer);
							if ($countAction == 2) {
								$body['variable'] = ['value' => $var_do];
							}
							$oper[$elem - 1] = $answer;
							$elem++;
						}
						break;
					case '--':
						if(empty($var_do))
						{
							Error::addError(
				                ['general'],
				                ['calculation', 'not_var_decrement'],
				                [],
				                $this,
				                [
				                    'ps' => $ps,
				                    'pf' => $pf,
				                    'rows' => $rows,
				                ]
				            );
						} else
						{
							$answer = MathOperations::Decrement($operElems[0], $this);
							$this->setVariable($var_do, $answer);
							if ($countAction == 2) {
								$body['variable'] = ['value' => $var_do];
							}
							$oper[$elem - 1] = $answer;
							$elem++;
						}
						break;
				}
				$elem = $elem - 1;
			} else
			{
				if ($body['action'][$curIndex]['type'] == Constants::TYPE_FUNCTION)
				{
					$argsF = [];
					$argM = null;
					$curIndexFuncArg = 0;
					for ($countParams = count($body['action'][$curIndex]['args']); $curIndexFuncArg < $countParams; $curIndexFuncArg++)
					{
						$copyDo = $currentPath;
						$copyDo[] = [
							'type' => 'function',
							'index' => $curIndexFuncArg,
							'elemFunc' => $argsF
						];
						$answ = $this->calcBody(
							$body['action'][$curIndex]['args'][$curIndexFuncArg],
							$body['action'][$curIndex]['args'][$curIndexFuncArg]['ps'],
							$body['action'][$curIndex]['args'][$curIndexFuncArg]['pf'],
							$body['action'][$curIndex]['args'][$curIndexFuncArg]['rows'],
							$copyDo,
							$contextPath
						);
						$argsF[] = $answ['answer'];
					}
					$curIndexFuncArg = 0;
					if (!empty($body['action'][$curIndex]['argM'])) {
						$copyDo = $currentPath;
						$copyDo[] = [
							'type' => 'function',
							'index' => $curIndexFuncArg,
							'elemFunc' => $argM
						];
						$arg = $body['action'][$curIndex]['argM'];
						unset($arg['action'][0]);
						$answ = $this->calcBody(
							['action' => [$arg]],
							$body['action'][$curIndex]['argM']['ps'],
							$body['action'][$curIndex]['argM']['pf'],
							$body['action'][$curIndex]['argM']['rows'],
							$currentPath,
							$contextPath
						);
						$argM = $answ['answer'];
					}
					if (empty($body['action'][$curIndex]['argM']))
					{
						$function = $this->getFunction($body['action'][$curIndex]['value']);
						if (!is_null($function))
						{
							$ans = call_user_func_array(
								$function,
								[
									$this,
									$argsF,
									null,
									[
										'ps' => $body['action'][$curIndex]['ps'],
										'pf' => $body['action'][$curIndex]['pf'],
										'rows' => $body['action'][$curIndex]['rows'],
									],
									$currentPath,
									$contextPath
								]
							);
							if (isset($ans['type']) && $ans['type'] == Constants::TYPE_DATETIME)
							{
								$ans['formatted'] = $ans['value']->format(Constants::DATETIME_FORMAT);
							}
							$oper[$elem++] = $ans;
						} else
						{
							Error::addError(
			                    ['general'],
			                    ['functions', 'undefined_function'],
			                    [
			                        'function' => $body['action'][$curIndex]['value'],
			                    ],
			                    $this,
			                    [
									'ps' => $body['action'][$curIndex]['ps'],
									'pf' => $body['action'][$curIndex]['pf'],
									'rows' => $body['action'][$curIndex]['rows'],
			                    ]
			                );
						}
					} else
					{
						$method = $this->getMethod($body['action'][$curIndex]['value']);
						if (!is_null($method))
						{
							$argM['method'] = true;
							$ans = call_user_func_array(
								$method,
								[
									$this,
									$argsF,
									$argM,
									[
										'ps' => $body['action'][$curIndex]['ps'],
										'pf' => $body['action'][$curIndex]['pf'],
										'rows' => $body['action'][$curIndex]['rows'],
									],
									$currentPath,
									$contextPath
								]
							);
							if (isset($ans['type']) && $ans['type'] == Constants::TYPE_DATETIME)
							{
								$ans['formatted'] = $ans['value']->format(Constants::DATETIME_FORMAT);
							}
							$oper[$elem++] = $ans;
						} else
						{
							Error::addError(
			                    ['general'],
			                    ['functions', 'undefined_method'],
			                    [
			                        'method' => $body['action'][$curIndex]['value'],
			                    ],
			                    $this,
			                    [
									'ps' => $body['action'][$curIndex]['ps'],
									'pf' => $body['action'][$curIndex]['pf'],
									'rows' => $body['action'][$curIndex]['rows'],
			                    ]
			                );
						}
					}
				} else
				{
					if (!empty($body['action'][$curIndex]['action']))
					{
						//значит тут надо вызывать методы
						$curIndexMethods = 0;
						$copyDo = [];
						for ($countMeth = count($body['action'][$curIndex]['action']); $curIndexMethods < $countMeth; $curIndexMethods++)
						{
							$body['action'][$curIndex]['action'][$curIndexMethods]['argM'] = $body['action'][$curIndex];
							$tmp = $body['action'][$curIndex]['action'][$curIndexMethods];
							$answ = $this->calcBody(
								['action' => [$tmp]],
								$body['action'][$curIndex]['action'][$curIndexMethods]['ps'],
								$body['action'][$curIndex]['action'][$curIndexMethods]['pf'],
								$body['action'][$curIndex]['action'][$curIndexMethods]['rows'],
								$copyDo,
								$contextPath
							);
							$body['action'][$curIndex]['type'] = $answ['answer']['type'];
							$body['action'][$curIndex]['value'] = $answ['answer']['value'];
							$body['action'][$curIndex]['props'] = isset($answ['answer']['props']) ? $answ['answer']['props'] : [];
						}
					}
					$oper[$elem++] = $body['action'][$curIndex];
				}
			}
			if ($this->globalMark == Constants::TYPE_ACTION_SAVE_NOCONTEXT)
			{
				$currentPath = $copyPath;
				$currentPath[] = [
					'index' => $curIndex + 1,
					'elem' => $elem,
					'oper' => $oper
				];
				$contextJSON = Helper::saveContextJSON($this, $currentPath);
				$this->globalMark = Constants::TYPE_ACTION_SAVE_CONTEXT;
			}
			if (in_array($this->globalMark, [Constants::TYPE_ACTION_SAVE_CONTEXT, Constants::TYPE_ACTION_RETURN]))
			{
				return;
			}
		}
		$answer = Constants::SPEC_VALUES['null'];
		if (count($this->getErrors()) != 0)
		{
			return ['answer' => $answer, 'variable' => null];
		}
		if (isset($oper[$elem-1]))
		{
			if ($oper[$elem-1]['type'] == Constants::TYPE_VARIABLE)
			{
				//для ситуации когда идет переменная равна переменной
				$var = $this->getVariable($oper[$elem-1]['value']);
				if (isset($var))
				{
					if (isset($oper[$elem-1]['arr']))
					{
						//значит идет получение элемента массива
						$answ_arr = $this->getArrayElement(
							$this->getVariable($oper[$elem-1]['value']),
							$oper[$elem-1]['arr']
						);
						$answer = $answ_arr;
					} else
					{
						$answer = $this->getVariable($oper[$elem-1]['value']);
					}
				} else
				{
					Error::addError(
		                ['general'],
		                ['calculation', 'not_found_var'],
		                [
		                    'variable' => $oper[$elem - 1]['value'],
		                ],
		                $this,
		                [
		                    'ps' => $ps,
		                    'pf' => $pf,
		                    'rows' => $rows,
		                ]
		            );
				}

			} else if ($oper[$elem-1]['type'] == Constants::TYPE_ACTION_FOR)
			{
				$this->forMark = $oper[$elem-1]['value'];
			} else
			{
				$answer = $oper[$elem-1];
			}

		}
		if ($answer['type'] == Constants::TYPE_DATETIME)
		{
			$answer['formatted'] = $answer['value']->format(Constants::DATETIME_FORMAT);
		}
		if (!empty($body['variable']))
		{
			if (isset($body['arr']))
			{
				$indexArr = [];
				$curIndArr = 0;
				for ($countInd = count($body['arr']); $curIndArr < $countInd; $curIndArr++)
				{
					//calcBody($body,$ps,$pf,$currentPath,&$contextPath)
					$answ = $this->calcBody(
						['action' => $body['arr'][$curIndArr]],
						min(array_column($body['arr'][$curIndArr], 'ps')),
						max(array_column($body['arr'][$curIndArr], 'pf')),
						array_merge(array_column($body['arr'][$curIndArr], 'rows')),
						$currentPath,
						$contextPath
					);
					$indexArr[] = $answ['answer']['value'];
				}
				if (isset($this->variables[$body['variable']['value']]))
				{
					$this->setArrayValue(
						$this->variables[$body['variable']['value']]['value'],
						$answer,
						$indexArr,
						[
							'ps' => $ps,
							'pf' => $pf,
							'rows' => $rows,
						]
					);
				}
			} else
			{
				$props = isset($answer['props']) ? $answer['props'] : [];
				$props['ps'] = $ps;
				$props['pf'] = $pf;
				$props['rows'] = $rows;
				$props['formatted'] = isset($answer['formatted']) ? $answer['formatted'] : null;
				$this->addVariable(
					$body['variable']['value'],
					$answer['type'],
					$answer['value'],
					$props
				);
			}

		}
		if (isset($body['variable']['value']))
		{
			$this->lastValue = $answer;
		}
		return ['answer' => $answer, 'variable' => isset($body['variable']['value']) ? $body['variable']['value'] : null];
	}

    private function getFunction($name)
    {
        return isset($this->functions[$name]) ? $this->functions[$name] : null;
    }

    private function getMethod($name)
    {
        return isset($this->methods[$name]) ? $this->methods[$name] : null;
    }

    private function setLoopIndex($index, $value)
    {
        $this->loopIndexes[$index] = $value;
    }

    private function setLoopIndexes($indexes)
    {
        $this->loopIndexes = $indexes;
    }

    private function setVariable($name, $props)
    {
        $type = isset($props['type']) ? $props['type'] : null;
        $value = isset($props['value']) ? $props['value'] : null;
        unset($props['type'], $props['value']);
        $this->addVariable($name, $type, $value, $props);
    }

    private function addVariable($name, $type, $value, $settings = [])
    {
        $previous = $this->getVariable($name);
        $this->variables[$name] = $this->formVariableValue($previous, $type, $value, $settings);
    }

    private function getArrayElement($elems_arr, $iter_arr)
    {
        $curElem = array_shift($iter_arr);
        $contextPath = [];
        $result = $this->calcBody(['action' => $curElem], 0, 0, 0, [], $contextPath);
        if (isset($elems_arr['value']) && isset($elems_arr['value'][$result['answer']['value']]))
        {
            if (count($iter_arr) == 0)
            {
                return $elems_arr['value'][$result['answer']['value']];
            }

            return $this->getArrayElement($elems_arr['value'][$result['answer']['value']], $iter_arr);
        }
        $nullV = Constants::SPEC_VALUES['null'];
        return $nullV;
    }

    private function setArrayValue(&$vArr, $val, $pthArr, $position)
    {
        $in = array_shift($pthArr);
        if (isset($vArr[$in]))
        {
            if (count($pthArr) == 0)
            {
                $type = isset($val['type']) ? $val['type'] : null;
                $value = isset($val['value']) ? $val['value'] : null;
                $settings = $val;
                unset($settings['type'], $settings['value']);
                $vArr[$in] = $this->formVariableValue($vArr[$in], $type, $value, $settings);
            } else
            {
                $this->setArrayValue($vArr[$in]['value'], $val, $pthArr, $position);
            }
        } else
        {
            if (!is_array($vArr))
            {
				Error::addError(
	                ['general'],
	                ['calculation', 'not_array_access'],
	                [],
	                $this,
	                [
	                    'ps' => $position['ps'],
	                    'pf' => $position['pf'],
	                    'rows' => $position['rows'],
	                ]
	            );
            } else
            {
	        	$vArr[$in] = $val;
            }
            //тут пишем всегда предупреждение типа элемента нет
        }
    }

    private function initDefaultValues(?Instance $object, array $arguments)
    {
    	if (!empty($object))
    	{
    		$class = $object->getType();
    		if (empty($class))
    		{
				$_obj = new \Dzeta\Models\Obj();
	            $_obj->GetClassForObject($object);
	            $class = $object->getType();
    		}
	    	$this->addVariable(
	    		'$this',
	    		Constants::TYPE_OBJECT,
	    		$object->getUid(),
	    		[
	    			'objClass' => $class->getUid(),
	    			'name' => $object->getName(),
	    			'rows' => [0],
	    		]
	    	);
    	}

    	if (!empty($arguments))
    	{
	        foreach ($arguments as $argument)
	        {
	            $variable = Helper::convertPhpValue(
	            	$argument['value'],
	            	[
	            		'ps' => 0,
	            		'pf' => 0,
	            		'rows' => [0]
	            	]
	            );
	            $this->addVariable(
	            	$argument['variable'],
	            	$variable['type'],
	            	$variable['value'],
	            	$variable['props']
	            );
	        }
    	}
    }

    private function initDatabaseCache($cache)
    {
    	foreach ($cache as $obj)
    	{
    		$attrs = $obj->getAttributes();
    		foreach ($attrs as $attr)
    		{
    			$attrValue = $attr->getValue();
    			$value = null;
    			if (!is_null($attrValue))
    			{
    				if ($attrValue instanceof Instance)
    				{
    					$value = $attrValue;
    				} else
    				{
	    				$value = $attrValue->getValue();
    				}
    			}
    			$this->setAttrCache($obj->getUid(), $attr->getUid(), $value);
    		}
    	}
    }

	private function formVariableValue($previous, $type, $value, $settings = [])
	{
		$ps = isset($settings['ps']) ? $settings['ps'] : 0;
		$pf = isset($settings['pf']) ? $settings['pf'] : 0;
		$rows = isset($settings['rows']) ? $settings['rows'] : [];
		unset($settings['ps'], $settings['pf'], $settings['rows']);
		if ($this->saveHistory) {
			$history = isset($previous['history']) ? $previous['history'] : [];
			if (isset($previous)) {
				unset($previous['history']);
				$history[] = $previous;
			}
			$variable = [
				'type' => $type,
				'value' => $value,
				'ps' => $ps,
				'pf' => $pf,
				'rows' => $rows,
				'props' => $settings,
                'loop' => array_values($this->loopIndexes),
				'history' => $history,
	 		];
		} else {
			$variable = [
				'type' => $type,
				'value' => $value,
				'ps' => $ps,
				'pf' => $pf,
				'rows' => $rows,
				'props' => $settings
	 		];
		}
		return $variable;
	}

    private function formErrorValue($text, $position, $props = [])
    {
        return [
            'text' => $text,
            'ps' => isset($position['ps']) ? $position['ps'] : 0,
            'pf' => isset($position['pf']) ? $position['pf'] : 0,
            'rows' => isset($position['rows']) ? $position['rows'] : [],
            'props' => $props
        ];
    }
}