<?php

namespace DzetaCalcParser\Core;
//parseArgFunction  Analytics -643 стррока false понять причинц зацикливания
class Analytics
{
	private static $row = 1;

	public static function createTreeParseExpression($curFormula, $settings, $dzetaCalc)
	{
		//TODO тут подумать что сделать умнее с окончанием строки
		if (isset($curFormula[-1]) && $curFormula[-1] != Constants::SYMBOL_END_EXPRESSION) {
			$curFormula .= ';';
		}

		$posSpecSymbol = [];
		$countSpecSymbol = ['rndBr' => 0, 'sqBr' => 0, 'figBr' => 0];
		$isString = false;
		$deltaPos = isset($settings['deltaPos']) ? $settings['deltaPos'] : 0;
		$lengthFormula = mb_strlen($curFormula);
		$prevSymbol = '';
		self::$row = isset($settings['row']) ? $settings['row'] : 1;
		$rows = [];
		$singleComment = false;
		$multiComment = false;
		for ($curIndexFrml = 0; $curIndexFrml < $lengthFormula; $curIndexFrml++)
		{
			$curSymbol = mb_substr($curFormula, $curIndexFrml, 1);
			$prevSymbol = '';
			if ($curIndexFrml > 0)
			{
				$prevSymbol = mb_substr($curFormula, $curIndexFrml - 1, 1);
			}
			if ($curSymbol != Constants::SYMBOL_NEW_ROW && $curSymbol != Constants::SYMBOL_CARRIAGE_RETURN)
			{
				if (!in_array(self::$row, $rows))
				{
					$rows[] = self::$row;
				}
			}
			if ($curSymbol == Constants::SYMBOL_NEW_ROW)
			{
				self::$row++;
				if ($singleComment)
				{
					$posSpecSymbol[] = [
						'type' => Constants::SYMBOL_SINGLE_COMMENT,
						'ind' => $curIndexFrml - 1,
						'rows' => $rows,
					];
					$rows = [];
				}
				$singleComment = false;
			}
			if ($prevSymbol . $curSymbol == Constants::SYMBOL_SINGLE_COMMENT)
			{
				$singleComment = true;
			}
			if ($prevSymbol . $curSymbol == Constants::SYMBOL_MULTI_COMMENT_START)
			{
				$multiComment = true;
				$posSpecSymbol[] = [
					'type' => Constants::SYMBOL_MULTI_COMMENT_START,
					'ind' => $curIndexFrml - 1,
					'rows' => $rows,
				];
				$rows = [];
			}
			if ($prevSymbol . $curSymbol == Constants::SYMBOL_MULTI_COMMENT_END)
			{
				if ($multiComment)
				{
					$posSpecSymbol[] = [
						'type' => Constants::SYMBOL_MULTI_COMMENT_END,
						'ind' => $curIndexFrml + 1,
						'rows' => $rows,
					];
					$rows = [];
				}
				$multiComment = false;
			}

			if ($curSymbol == Constants::SYMBOL_STRING && !$singleComment && !$multiComment)
			{
				if ($isString)
				{
					if ($prevSymbol != Constants::SYMBOL_ESCAPE)
					{
						$isString = false;
					}
				} else
				{
					$isString = true;
				}
			} else if ($curSymbol == Constants::SYMBOL_END_EXPRESSION &&
				!$isString && !$singleComment && !$multiComment && $countSpecSymbol['rndBr'] == 0 && $countSpecSymbol['figBr'] == 0 &&
				$countSpecSymbol['figBr'] == 0)
			{
				$posSpecSymbol[] = [
					'type' => Constants::SYMBOL_END_EXPRESSION,
					'ind' => $curIndexFrml,
					'rows' => $rows,
				];
				$rows = [];
			} else if ($curSymbol == Constants::BRACKETS_BLOCK_SIMPLE[0] && !$isString && !$singleComment && !$multiComment)
			{
				if ($countSpecSymbol['figBr'] == 0)
				{
					$posSpecSymbol[] = [
						'type' => Constants::BRACKETS_BLOCK_SIMPLE[0],
						'ind' => $curIndexFrml,
						'rows' => $rows,
					];
					$rows = [];
				}
				$countSpecSymbol['figBr']++;
			} else if ($curSymbol == Constants::BRACKETS_BLOCK_SIMPLE[1] && !$isString && !$singleComment && !$multiComment)
			{
				$countSpecSymbol['figBr']--;
				if ($countSpecSymbol['figBr'] == 0)
				{
					$posSpecSymbol[] = [
						'type' => Constants::BRACKETS_BLOCK_SIMPLE[1],
						'ind' => $curIndexFrml,
						'rows' => $rows,
					];
					$rows = [];
				}
			} else if ($curSymbol == '(' && !$isString && !$singleComment && !$multiComment)
			{
				$countSpecSymbol['rndBr']++;
			} else if ($curSymbol == ')' && !$isString && !$singleComment && !$multiComment)
			{
				$countSpecSymbol['rndBr']--;
			} else if ($curSymbol == '[' && !$isString && !$singleComment && !$multiComment)
			{
				$countSpecSymbol['sqBr']++;
			} else if ($curSymbol == ']' && !$isString && !$singleComment && !$multiComment)
			{
				$countSpecSymbol['sqBr']--;
			}
			//тут делаем проверку на положительность скобок
			if ($countSpecSymbol['figBr'] < 0)
			{
				Error::addError(
	                ['general'],
	                ['calculation', 'unexpected_use'],
	                [
	                    'value' => $curSymbol,
	                ],
	                $dzetaCalc,
	                [
	                    'ps' => $curIndexFrml + 1,
	                    'pf' => $curIndexFrml + 2,
	                    'rows' => $rows,
	                ]
	            );
	            break;
			}
			if($countSpecSymbol['sqBr'] < 0) {
				Error::addError(
	                ['general'],
	                ['calculation', 'unexpected_use'],
	                [
	                    'value' => $curSymbol,
	                ],
	                $dzetaCalc,
	                [
	                    'ps' => $curIndexFrml + 1,
	                    'pf' => $curIndexFrml + 2,
	                    'rows' => $rows,
	                ]
	            );
	            break;
			}
			if($countSpecSymbol['rndBr'] < 0) {

				Error::addError(
	                ['general'],
	                ['calculation', 'unexpected_use'],
	                [
	                    'value' => $curSymbol,
	                ],
	                $dzetaCalc,
	                [
	                    'ps' => $curIndexFrml + 1,
	                    'pf' => $curIndexFrml + 2,
	                    'rows' => $rows,
	                ]
	            );
	            break;
			}
		}		
		if ($countSpecSymbol['figBr'] > 0)
		{
			Error::addError(
                ['general'],
                ['calculation', 'unexpected_use'],
                [
                    'value' => $curFormula,
                ],
                $dzetaCalc,
                [
                    'ps' => $deltaPos,
                    'pf' => $deltaPos+$lengthFormula,
                    'rows' => $rows,
                ]
            );
		}
		if($countSpecSymbol['sqBr'] > 0) {
			Error::addError(
                ['general'],
                ['calculation', 'unexpected_use'],
                [
                    'value' => $curFormula,
                ],
                $dzetaCalc,
                [
                    'ps' => $deltaPos,
                    'pf' => $deltaPos+$lengthFormula,
                    'rows' => $rows,
                ]
            );
		}
		if($countSpecSymbol['rndBr'] > 0) {

			Error::addError(
                ['general'],
                ['calculation', 'unexpected_use'],
                [
                    'value' => $curFormula,
                ],
                $dzetaCalc,
                [
                    'ps' => $deltaPos,
                    'pf' => $deltaPos+$lengthFormula,
                    'rows' => $rows,
                ]
            );
		}
		return count($dzetaCalc->getErrors()) > 0 ? [] : self::parseBlockTreeExpression(
			$posSpecSymbol,
			[
				'formula' => $curFormula,
				'deltaPos' => $deltaPos,
			],
			$dzetaCalc
		);
	}
	

	public static function parseBlockTreeExpression($blocks, $settings, $dzetaCalc)
	{		
		$blockParseArr = [];
		$startPosCur = 0;
		$startPosGlobal = isset($settings['deltaPos']) ? $settings['deltaPos'] : 0;
		$rows = [];

		for ($curIndex = 0, $countBlock = count($blocks); $curIndex < $countBlock; $curIndex++)
		{
			$rows = array_merge($rows, $blocks[$curIndex]['rows']);
			if ($blocks[$curIndex]['type'] == Constants::SYMBOL_END_EXPRESSION)
			{
				$elemFrml = mb_substr($settings['formula'], $startPosCur, $blocks[$curIndex]['ind'] - $startPosCur);				
				//Analytics::parseExpression($elemFrml,['deltaPos'=>$startPosGlobal+$startPosCur],$dzetaCalc);
				if (trim($elemFrml) == '')
				{
					$startPosCur = $blocks[$curIndex]['ind'] + 1;
					continue;
				}		
				
				$flagSimple=true;
				$posRndBrack = mb_strpos($elemFrml, Constants::BRACKETS_SIMPLE[0]);
				if($posRndBrack!==false)
				{
					$nameBlockFunc = mb_substr($elemFrml, 0, $posRndBrack);
					$nameBlockFunc=trim($nameBlockFunc);
					//это проверяем на строковый if/elseif
					switch ($nameBlockFunc) 
					{
						case 'if':
						case 'elseif':														
							$posRndBrackClose =Helper::findCloseRoundBracketExpression($elemFrml,$posRndBrack);								
							if($posRndBrackClose === false)
							{
								//тут тогда ошибка получается 
								throw new \Exception("Ошибка с линейным if", 1);
								Error::addError(
					                ['general'],
					                ['calculation', 'unexpected_use'],
					                [
					                	'value' => $elemFrml,
					                ],
					                $dzetaCalc,
					                [
					                    'ps' => $startPosGlobal+ $startPosCur  + $posRndBrack + 2,
					                    'pf' => $startPosGlobal+ $startPosCur  + $posRndBrack + 2,
					                    'rows' => $rows
					                ]
					            );
					            return [];
							}
							$declFrml = mb_substr($elemFrml, $posRndBrack + 1, $posRndBrackClose - $posRndBrack - 1);								
							$declRowsCount = substr_count($declFrml, Constants::SYMBOL_NEW_ROW);
							$declBody = [
								'body' => Analytics::createTreeParseExpression(
									$declFrml,
									[
										'deltaPos' => $startPosGlobal  + $posRndBrack + $startPosCur+2,
										'row' => min($rows) 
									],
									$dzetaCalc
								),
								'formula' => $declFrml,
								'ps' => $startPosGlobal+ $startPosCur  + $posRndBrack + 2,
								'pf' => $startPosGlobal+ $startPosCur + $posRndBrackClose+1,
								'rows' => range(min($rows), min($rows) + $declRowsCount),
							];		

							$bodyfrmula=mb_substr($elemFrml, $posRndBrackClose + 1);							
							$bodyParse = Analytics::createTreeParseExpression(
								$bodyfrmula,
								[
									'deltaPos' => $startPosGlobal+ $startPosCur + $posRndBrackClose+2,
									'row' => min($rows) + $declRowsCount
								],
								$dzetaCalc,
							);	
							$bodyRowsCount = substr_count($bodyfrmula, Constants::SYMBOL_NEW_ROW);						
							$blockParseArr[] = [
								'type' => $nameBlockFunc,
								'condition' => $declBody,
								'formula' =>$elemFrml,
								'formulaBody' => $bodyfrmula,
								'body' => $bodyParse,
								'ps' => $startPosCur + $startPosGlobal,
								'pf' =>$blocks[$curIndex]['ind']+2,
								'rows' => range(min($rows),min($rows)+$declRowsCount+$bodyRowsCount)
							];
							$flagSimple=false;
							$startPosCur = $blocks[$curIndex]['ind'] + 1;
							$rows = [];
							break;
					}					
				}
				if($flagSimple===true)
				{
					//теперь проверяем на else
					if( mb_strpos(trim($elemFrml),'else ')===0||
						mb_strpos(trim($elemFrml),'else'.Constants::SYMBOL_NEW_ROW)===0||
						mb_strpos(trim($elemFrml),'else'.Constants::SYMBOL_CARRIAGE_RETURN)===0)
					{
						//теперь надо убрать else
						$dlta=mb_strlen($elemFrml)-mb_strlen(ltrim($elemFrml));
						
						$elemFrml=mb_substr($elemFrml,$dlta+4);	
						$declRowsCount = substr_count($declFrml, Constants::SYMBOL_NEW_ROW);
						$bodyParse = Analytics::createTreeParseExpression(
							$elemFrml,
							[
								'deltaPos' => $startPosGlobal+$dlta,
								'row' => min($rows),
							],
							$dzetaCalc,
						);
						$bodyRowsCount = substr_count($bodyfrmula, Constants::SYMBOL_NEW_ROW);
						$blockParseArr[] = [
							'type' => 'else',
							'condition' => [],
							'formula' => $elemFrml,
							'formulaBody' => $elemFrml,
							'body' => $bodyParse,
							'ps' => $startPosGlobal+ $startPosCur +$dlta,
							'pf' => $blocks[$curIndex]['ind']+ $startPosGlobal + 2,
							'rows' =>range(min($rows),min($rows)+$declRowsCount+$bodyRowsCount)
						];
						$flagSimple=false;
						$startPosCur = $blocks[$curIndex]['ind'] + 1;
						$rows = [];
					}
				}
				if($flagSimple===true)
				{
					$blockParseArr[] = [
						'type' => Constants::TYPE_ACTION_SIMPLE,
						'formula' => trim($elemFrml),
						'body' => self::createTreeExpression(
							$elemFrml,
							[
								'ps' => $startPosCur + $startPosGlobal,
								'row' => min($rows)
							],
							$dzetaCalc
						),
						'ps' => $startPosCur + $startPosGlobal,
						'pf' => $blocks[$curIndex]['ind'] + $startPosGlobal + 1,
						'rows' => $rows,
					];
					$rows = [];
					$startPosCur = $blocks[$curIndex]['ind'] + 1;
				}
				
			} else if ($blocks[$curIndex]['type'] == Constants::BRACKETS_BLOCK_SIMPLE[0])
			{
				//тут блочная конструкция
				$startBlockConstruction = $blocks[$curIndex]['ind'] + 1;
				$nextBlock = $blocks[$curIndex + 1];
				$endBlockConstruction = $nextBlock['ind'] - 1;
				$rows = array_merge($rows, $nextBlock['rows']);
				$startPosCur_ = $startPosCur;
				//это тело блочной функции
				$elemFrml = mb_substr($settings['formula'], $startBlockConstruction, $endBlockConstruction - $startBlockConstruction);
				//это наше определние блочной конструкции
				$decl = mb_substr($settings['formula'], $startPosCur, $startBlockConstruction - $startPosCur - 1);
				$declAllrowsCount = substr_count(trim($decl), Constants::SYMBOL_NEW_ROW);
				$declIfAllRowsCount = substr_count($decl, Constants::SYMBOL_NEW_ROW);
				$allFormula = trim(mb_substr($settings['formula'], $startPosCur, $endBlockConstruction - $startPosCur + 2));

		 		$declBody = null;
		 		$posRndBrackClose = false;
		 		//определяем название блочной функции
				$posRndBrack = mb_strpos($decl, Constants::BRACKETS_SIMPLE[0]);
				$newRowsCount = 0;
				if($posRndBrack === false)
				{
					$posRndBrack = $startBlockConstruction - $startPosCur - 1;
				} else
				{
					$posRndBrackClose = mb_strrpos($decl, Constants::BRACKETS_SIMPLE[1]);
					$newRowsCount = substr_count(trim($decl, " \n\r"), Constants::SYMBOL_NEW_ROW, 0, $posRndBrack);
				}
				$nameBlockFunc = mb_substr($decl, 0, $posRndBrack);
				//вытягиваем тело функции
				$startPosCur = $endBlockConstruction + 2;
				$pStartBody = $startPosGlobal + $startBlockConstruction;
				$pFinishBody = $endBlockConstruction;

				$bodyParse = Analytics::createTreeParseExpression(
					$elemFrml,
					[
						'deltaPos' => $startPosGlobal + $startBlockConstruction,
						'row' => min($rows) + $declAllrowsCount,
					],
					$dzetaCalc,
				);

				$elemFrmlBeforeRowsCount = substr_count(str_replace(trim($elemFrml), '', rtrim($elemFrml)), Constants::SYMBOL_NEW_ROW);
				$elemFrml = trim($elemFrml);
				$nameBlockFunc = trim($nameBlockFunc);

				switch ($nameBlockFunc)
				{
					case 'if':
					case 'elseif':
					case 'else':
						//TODO проверить смещения
						$declRowsCount = 0;
						if($posRndBrackClose !== false)
						{
							$declFrml = mb_substr($decl, $posRndBrack + 1, $posRndBrackClose - $posRndBrack - 1);
							$declRowsCount = substr_count($declFrml, Constants::SYMBOL_NEW_ROW);
							$declBody = [
								'body' => Analytics::createTreeParseExpression(
									$declFrml,
									[
										'deltaPos' => $startPosGlobal + $startPosCur_ + $posRndBrack + 2,
										'row' => min($rows) + $newRowsCount
									],
									$dzetaCalc
								),
								'formula' => $declFrml,
								'ps' => $startPosGlobal + $startPosCur_ + $posRndBrack + 2,
								'pf' => $startPosGlobal + $startPosCur_ + $posRndBrackClose,
								'rows' => range(min($rows) + $newRowsCount, min($rows) + $newRowsCount + $declRowsCount),
							];
						}
						$bodyParse = Analytics::createTreeParseExpression(
							$elemFrml,
							[
								'deltaPos' => $startPosGlobal + $startBlockConstruction,
								'row' => min($rows) + $newRowsCount + $declIfAllRowsCount + $elemFrmlBeforeRowsCount,
							],
							$dzetaCalc,
						);
						$blockParseArr[] = [
							'type' => $nameBlockFunc,
							'condition' => $declBody,
							'formula' => $allFormula,
							'formulaBody' => $elemFrml,
							'body' => $bodyParse,
							'ps' => $startPosCur_ + $startPosGlobal,
							'pf' => $endBlockConstruction + $startPosGlobal + 2,
							'rows' => $rows,
						];
						$rows = [];
						break;
					case 'for':
						$initialisation = null;
						$condition = null;
						$modification = null;
						$declRowsCount = 0;
						if($posRndBrackClose !== false)
						{
							$declFrml = mb_substr($decl, $posRndBrack + 1, $posRndBrackClose - $posRndBrack - 1);
							$declArr = Helper::explodeFuncArgs($declFrml, Constants::SYMBOL_END_EXPRESSION);
							$initRowsCount = 0;
							$condRowsCount = 0;
							$modRowsCount = 0;
							if (isset($declArr[0]))
							{
								$tmp = Helper::explodeFuncArgs($declArr[0], Constants::DELIMITER_FUNCTION);
								$initRowsCount = substr_count($declArr[0], Constants::SYMBOL_NEW_ROW);
								$body = [];
								$delta = 0;
								$declNewRowsCount = 0;
								foreach ($tmp as $valueTmp)
								{
									$posDo = ($posRndBrack + 1) + $delta;
									$delta += mb_strlen($valueTmp);
									$body[] = Analytics::createTreeParseExpression(
										$valueTmp,
										[
											'deltaPos' => $startPosGlobal + $startPosCur_ + $posDo,
											'row' => min($rows) + $declNewRowsCount,
										],
										$dzetaCalc
									)[0];
									$declNewRowsCount += substr_count($valueTmp, Constants::SYMBOL_NEW_ROW);
									$delta += 1;
								}
								$initialisation = [
									'body' => $body,
									'formula' => $declArr[0],
									'ps' => $startPosGlobal + $startPosCur_ + $posRndBrack + 2,
									'pf' => $startPosGlobal + $startPosCur_ + $posRndBrack + 1 + mb_strlen($declArr[0]),
									'rows' => range(min($rows) + $newRowsCount, min($rows) + $newRowsCount + $initRowsCount),
								];
								$declRowsCount += $initRowsCount;
							}
							if (isset($declArr[1]))
							{
								//тут только одно условие
								$tmp = Helper::explodeFuncArgs($declArr[1], Constants::DELIMITER_FUNCTION);
								$condRowsCount = substr_count($declArr[1], Constants::SYMBOL_NEW_ROW);
								if (count($tmp) != 1)
								{
									Error::addError(
						                ['general'],
						                ['calculation', 'only_one_for_condition'],
						                [],
						                $dzetaCalc,
						                [
						                    'ps' => $startPosGlobal + $startPosCur_ + $posRndBrack + 2 + mb_strlen($declArr[0]),
						                    'pf' => $startPosGlobal + $startPosCur_ + $posRndBrack + 1 + mb_strlen($declArr[0]) + mb_strlen($declArr[1]),
						                    'rows' => range(min($rows), min($rows) + $condRowsCount),
						                ]
						            );
						            break 2;
								}
								$body = [];
								$delta = 0;
								$declNewRowsCount = 0;
								foreach ($tmp as $valueTmp)
								{
									$posDo = ($posRndBrack + 1) + $delta;
									$delta += mb_strlen($valueTmp);
									$body[] = Analytics::createTreeParseExpression(
										$valueTmp,
										[
											'deltaPos' => $startPosGlobal + $startPosCur_ + $posDo,
											'row' => min($rows) + $declNewRowsCount,
										],
										$dzetaCalc
									)[0];
									$delta += 1;
								}
								$condition = [
									'body' => $body,
									'formula' => $declArr[1],
									'ps' => $startPosGlobal + $startPosCur_ + $posRndBrack + 2 + mb_strlen($declArr[0]),
									'pf' => $startPosGlobal + $startPosCur_ + $posRndBrack + 1 + mb_strlen($declArr[0]) + mb_strlen($declArr[1]),
									'rows' => range(min($rows) + $newRowsCount + $declRowsCount, min($rows) + $newRowsCount + $declRowsCount + $condRowsCount),
								];
								$declRowsCount += $condRowsCount;
							}
							if (isset($declArr[2]))
							{
								$tmp = Helper::explodeFuncArgs($declArr[2], Constants::DELIMITER_FUNCTION);
								$modRowsCount += substr_count($declArr[2], Constants::SYMBOL_NEW_ROW);
								$body = [];
								$delta = 0;
								$declNewRowsCount = 0;
								foreach ($tmp as $valueTmp)
								{
									$posDo = ($posRndBrack + 1) + $delta;
									$delta += mb_strlen($valueTmp);
									$body[] = Analytics::createTreeParseExpression(
										$valueTmp,
										[
											'deltaPos' => $startPosGlobal + $startPosCur_ + $posDo,
											'row' => min($rows) + $declNewRowsCount,
										],
										$dzetaCalc
									)[0];
									$delta += 1;
								}
								$modification = [
									'body' => $body,
									'formula' => $declArr[2],
									'ps' => $startPosGlobal + $startPosCur_ + $posRndBrack + 3 + mb_strlen($declArr[0]) + mb_strlen($declArr[1]),
									'pf' => $startPosGlobal + $startPosCur_ + $posRndBrack + 3 + mb_strlen($declArr[0]) + mb_strlen($declArr[1]) + mb_strlen($declArr[2]),
									'rows' => range(min($rows) + $newRowsCount + $declRowsCount, min($rows) + $newRowsCount + $declRowsCount + $modRowsCount),
								];
								$declRowsCount += $modRowsCount;
							}
						}
						//для for мы чуть иначе делаем, выделяя Инициализацию, Условия, Модифика
						$blockParseArr[] = [
							'type' => Constants::TYPE_ACTION_FOR,
							'initialisation' => $initialisation,
							'condition' => $condition,
							'modification' => $modification,
							'bodyFor' => [
								'body' => $bodyParse,
								'formula' => $elemFrml,
								'ps' => $pStartBody,
								'pf' => $pFinishBody
							],
							'formula' => $allFormula,
							'ps' => $startPosCur_ + $startPosGlobal,
							'pf' => $endBlockConstruction + $startPosGlobal + 2,
							'rows' => $rows,
						];
						$rows = [];
						break;
				}
				$curIndex++;
			} else if ($blocks[$curIndex]['type'] == Constants::SYMBOL_SINGLE_COMMENT)
			{
				$comment = mb_substr($settings['formula'], $startPosCur, $blocks[$curIndex]['ind'] - $startPosCur);
				$blockParseArr[] = [
					'type' => Constants::TYPE_ACTION_SINGLE_COMMENT,
					'formula' => trim($comment),
					'body' => trim($comment),
					'ps' => $startPosCur + $startPosGlobal,
					'pf' => $blocks[$curIndex]['ind'] + $startPosGlobal + 1,
					'rows' => $rows,
				];
				$startPosCur = $blocks[$curIndex]['ind'] + 1;
			} else if ($blocks[$curIndex]['type'] == Constants::SYMBOL_MULTI_COMMENT_START)
			{
				$nextBlock = $blocks[$curIndex + 1];
				$comment = mb_substr($settings['formula'], $startPosCur, $nextBlock['ind'] - $startPosCur);
				$blockParseArr[] = [
					'type' => Constants::TYPE_ACTION_MULTI_COMMENT,
					'formula' => trim($comment),
					'body' => trim($comment),
					'ps' => $startPosCur + $startPosGlobal,
					'pf' => $nextBlock['ind'] + $startPosGlobal + 1,
					'rows' => array_values(array_unique([...$rows, ...$nextBlock['rows']])),
				];
				$curIndex++;
				$startPosCur = $blocks[$curIndex]['ind'] + 1;
			}
		}
		
		//блок который мониторит if
		for ($curIndex = 0, $countBlock = count($blockParseArr); $curIndex < $countBlock; $curIndex++)
		{
			if ($blockParseArr[$curIndex]['type'] == Constants::TYPE_ACTION_IF)
			{
				$posIF = $curIndex;
				//$blockParseArr[$posIF];
				$ifBlock = [
					'type' => Constants::TYPE_ACTION_IF,
					'body' => $blockParseArr[$posIF]['body'],
					'condition' => $blockParseArr[$posIF]['condition'],
					'formula' => $blockParseArr[$posIF]['formula'],
					'formulaBody' => $blockParseArr[$posIF]['formulaBody'],
					'ps' => $blockParseArr[$posIF]['ps'],
					'pf' => $blockParseArr[$posIF]['pf'],
					'rows' => $blockParseArr[$posIF]['rows'],
				];
				$blockParseArr[$posIF]['dopif'] = [];
				$blockParseArr[$posIF]['dopif'][] = $ifBlock;
				$pend = $blockParseArr[$posIF]['pf'];
				//теперь ищем блоки if
				$dopIfIndex = $curIndex;
				for ($dopIfIndex++; $dopIfIndex < $countBlock; $dopIfIndex++)
				{
					if ($blockParseArr[$dopIfIndex]['type'] == Constants::TYPE_ACTION_ELSEIF)
					{
						$blockParseArr[$posIF]['dopif'][] = $blockParseArr[$dopIfIndex];
						$pend = $blockParseArr[$dopIfIndex]['pf'];
						unset($blockParseArr[$dopIfIndex]);
						$curIndex++;
					} else if ($blockParseArr[$dopIfIndex]['type'] == Constants::TYPE_ACTION_ELSE)
					{
						$blockParseArr[$posIF]['dopif'][] = $blockParseArr[$dopIfIndex];
						$pend = $blockParseArr[$dopIfIndex]['pf'];
						unset($blockParseArr[$dopIfIndex]);
						$curIndex++;
						break;
					} else {
						break;
					}
				}
				$formulaIF = mb_substr($settings['formula'], $blockParseArr[$posIF]['ps'], $pend - $blockParseArr[$posIF]['ps']);
				$blockParseArr[$posIF]['formula'] = trim($formulaIF);
				$blockParseArr[$posIF]['pf'] = $pend;
				unset($blockParseArr[$posIF]['condition'], $blockParseArr[$posIF]['body']);
			} else if ($blockParseArr[$curIndex]['type'] == Constants::TYPE_ACTION_ELSEIF || $blockParseArr[$curIndex]['type'] == Constants::TYPE_ACTION_ELSE)
			{
				Error::addError(
	                ['general'],
	                ['calculation', 'unexpected_use'],
	                [
	                	'value' => $blockParseArr[$curIndex]['type'],
	                ],
	                $dzetaCalc,
	                [
	                    'ps' => $blockParseArr[$curIndex]['ps'],
	                    'pf' => $blockParseArr[$curIndex]['pf'],
	                    'rows' => $blockParseArr[$curIndex]['rows'],
	                ]
	            );
	            break;
			}
		}
		$blockParseArr = array_values($blockParseArr);

		return $blockParseArr;
	}

	public static function createTreeExpression($expression, $settings, $dzetaCalc,$returnOnlyAction=false)
	{
		
		$blocksExpression = [];
		$expression .= ' ';
		$expression = trim($expression);
		$lenExpression = mb_strlen($expression);
		$row = isset($settings['row']) ? $settings['row'] : 1;
		$rows = [];

		$OPERATIONS_ALL = [
			...Constants::OPERATIONS_LOW,
			...Constants::OPERATIONS_HIGH,
			...Constants::OPERATIONS_VERY_HIGH,
			...Constants::OPERATIONS_HIGH_LOGIC,
			...Constants::OPERATIONS_LOGIC
		];
		$BRACKETS_ALL = [
			...Constants::BRACKETS_SIMPLE,
			...Constants::BRACKETS_ARRAY_SIMPLE,
			...Constants::BRACKETS_BLOCK_SIMPLE
		];
		$OPERATIONS_AND_BRACKETS = [
			...$OPERATIONS_ALL,
			...Constants::BRACKETS_SIMPLE,
			...Constants::BRACKETS_ARRAY_SIMPLE,
			Constants::DELIMITER_VAR_EXPR,
		];
		$doubleOper = [];
		foreach ($OPERATIONS_ALL as $operation)
		{
			if (mb_strlen($operation) == 2)
			{
				$doubleOper[] = $operation;
			}
		}
		for ($curIndex = 0; $curIndex < $lenExpression; $curIndex++)
		{
			$curSymbol = mb_substr($expression, $curIndex, 1);
			$nextSymbol = mb_substr($expression, $curIndex + 1, 1);
			if ($curSymbol != Constants::SYMBOL_NEW_ROW && $curSymbol != Constants::SYMBOL_CARRIAGE_RETURN)
			{
				if (!in_array($row, $rows))
				{
					$rows[] = $row;
				}
			}
			if ($curSymbol == Constants::SYMBOL_NEW_ROW)
			{
				$row++;
			}

			if (in_array($curSymbol . $nextSymbol, $doubleOper))
			{
				//это двойные операции
				$blocksExpression[] = [
					'type' => Constants::TYPE_OPER_SPC,
					'value' => $curSymbol . $nextSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 2,
					'rows' => $rows,
				];
				$rows = [];
				$curIndex++;
			} else if (in_array($curSymbol, $OPERATIONS_ALL))
			{
				//это обычные операции
				$blocksExpression[] = [
					'type' => Constants::TYPE_OPER_SPC,
					'value' => $curSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 1,
					'rows' => $rows,
				];
				$rows = [];
			} else if ($curSymbol == Constants::START_VARIABLE)
			{
				//это переменная
				$indDo = $curIndex;
				for ($curIndex += 1; $curIndex < $lenExpression; $curIndex++)
				{
					$curSymbol = mb_substr($expression, $curIndex, 1);
					if ($curSymbol != Constants::SYMBOL_NEW_ROW && $curSymbol != Constants::SYMBOL_CARRIAGE_RETURN)
					{
						if (!in_array($row, $rows))
						{
							$rows[] = $row;
						}
					}
					if ($curSymbol == Constants::SYMBOL_NEW_ROW)
					{
						$row++;
					}
					if (in_array($curSymbol, Constants::SYMBOL_P) || $curIndex + 1 == $lenExpression)
					{
						if ($curIndex + 1 == $lenExpression && !in_array($curSymbol, Constants::SYMBOL_P)) {
							$curIndex++;
						}
						$blocksExpression[] = [
							'type' => Constants::TYPE_VARIABLE,
							'value' => trim(mb_substr($expression, $indDo, $curIndex - $indDo), " \n\r"),
							'ps' => $settings['ps'] + $indDo,
							'pf' => $settings['ps'] + $curIndex,
							'rows' => $rows,
						];
						$rows = [];
						$curIndex--;
						break;
					}

				}
			} else if ($curSymbol == Constants::SYMBOL_STRING)
			{
				//это строка
				$indDo = $curIndex;
				$stringS = "";
				for ($curIndex += 1; $curIndex < $lenExpression; $curIndex++)
				{
					$curSymbol = mb_substr($expression, $curIndex, 1);
					$nextSymbol = mb_substr($expression, $curIndex + 1, 1);
					if ($curSymbol != Constants::SYMBOL_NEW_ROW && $curSymbol != Constants::SYMBOL_CARRIAGE_RETURN)
					{
						if (!in_array($row, $rows))
						{
							$rows[] = $row;
						}
					}
					if ($curSymbol == Constants::SYMBOL_NEW_ROW)
					{
						$row++;
					}
					if ($curSymbol == Constants::SYMBOL_ESCAPE)
					{
						if (in_array($nextSymbol, Constants::ESCAPE_SYMBOL))
						{
							$stringS .= $nextSymbol;
							$curIndex++;
						}
					}
					if ($curSymbol == Constants::SYMBOL_STRING)
					{
						$blocksExpression[] = [
							'type' => Constants::TYPE_STRING,
							'value' => $stringS,
							'ps' => $settings['ps'] + $indDo,
							'pf' => $settings['ps'] + $curIndex + 1,
							'rows' => $rows,
						];
						$rows = [];
						break;
					}
					$stringS .= $curSymbol;
				}
			} else if ($curSymbol == ' ' || $curSymbol == "\n" || $curSymbol == "\r" || $curSymbol == "\t")
			{
				continue;
			} else if ($curSymbol == Constants::DELIMITER_VAR_EXPR)
			{
				$blocksExpression[] = [
					'type' => Constants::TYPE_DELIM_VAR,
					'value' => $curSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 1,
					'rows' => $rows,
				];
				$rows = [];
			} else if (is_numeric($curSymbol))
			{
				$blocksExpression[] = [
					'type' => Constants::TYPE_NUMBER,
					'value' => $curSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 1,
					'rows' => $rows,
				];
				$rows = [];
			} else if ($curSymbol == Constants::DELIMITER_NUMBER_AND_METHOD)
			{
				$blocksExpression[] = [
					'type' => Constants::DELIMITER_NUMBER_AND_METHOD,
					'value' => $curSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 1,
					'rows' => $rows,
				];
				$rows = [];
			} else if (in_array($curSymbol, Constants::BRACKETS_SIMPLE))
			{
				$blocksExpression[] = [
					'type' => Constants::TYPE_BRACKETS_SIMPLE,
					'value' => $curSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 1,
					'rows' => $rows,
				];
				$rows = [];
			} else if (in_array($curSymbol, Constants::BRACKETS_ARRAY_SIMPLE))
			{
				$blocksExpression[] = [
					'type' => Constants::TYPE_BRACKETS_ARRAY,
					'value' => $curSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 1,
					'rows' => $rows,
				];
				$rows = [];
			} else if ($curSymbol == Constants::DELIMITER_FUNCTION)
			{
				$blocksExpression[] = [
					'type' => Constants::DELIMITER_FUNCTION,
					'value' => $curSymbol,
					'ps' => $settings['ps'] + $curIndex,
					'pf' => $settings['ps'] + $curIndex + 1,
					'rows' => $rows,
				];
			} else
			{
				//тут типа функция идет
				$indDo = $curIndex;
				for ($curIndex += 1; $curIndex < $lenExpression; $curIndex++)
				{
					$curSymbol = mb_substr($expression, $curIndex, 1);
					if ($curSymbol != Constants::SYMBOL_NEW_ROW && $curSymbol != Constants::SYMBOL_CARRIAGE_RETURN)
					{
						if (!in_array($row, $rows))
						{
							$rows[] = $row;
						}
					}
					if ($curSymbol == Constants::SYMBOL_NEW_ROW)
					{
						$row++;
					}
					if (in_array($curSymbol, Constants::SYMBOL_P) || $curIndex + 1 == $lenExpression)
					{
						if ($curIndex + 1 == $lenExpression && !in_array($curSymbol, Constants::SYMBOL_P)) {
							$curIndex++;
						}
						$frmlExpression = mb_substr($expression, $indDo, $curIndex - $indDo);
						if (trim($frmlExpression) == '')
						{
							continue;
						}
						//тут этап на котором надо еще вытянуть конструкции типа false/true/null
						if (isset(Constants::SPEC_VALUES[trim($frmlExpression)]))
						{
							$blocksExpression[] = [
								'type' => Constants::SPEC_VALUES[trim($frmlExpression)]['type'],
								'value' => Constants::SPEC_VALUES[trim($frmlExpression)]['value'],
								'ps' => $settings['ps'] + $indDo,
								'pf' => $settings['ps'] + $curIndex,
								'rows' => $rows,
							];
							$rows = [];
						} else
						{	
							$blocksExpression[] = [
								'type' => Constants::TYPE_FUNCTION,
								'value' => trim($frmlExpression, " \n\r"),
								'ps' => $settings['ps'] + $indDo,
								'pf' => $settings['ps'] + $curIndex,
								'rows' => $rows,
							];
							$rows = [];
						}
						$curIndex--;
						break;
					}
				}
			}
		}
		
		//блок в котором собираем числа все
		$TYPES = [
			...Constants::DATATYPES,
			Constants::TYPE_VARIABLE,
			Constants::TYPE_FUNCTION,
			Constants::TYPE_BRACKETS_SIMPLE,
			Constants::TYPE_BRACKETS_ARRAY,
			Constants::TYPE_OPER_SPC,
			Constants::TYPE_DELIM_VAR,
		];
		$isInFunction = 0;
		$prevOper = false;
		$openBracket = false;
		for ($curIndex = 0, $countBlock = count($blocksExpression); $curIndex < $countBlock; $curIndex++)
		{
			if ($prevOper && !in_array($blocksExpression[$curIndex]['type'], $TYPES))
			{
				Error::addError(
	                ['general'],
	                ['calculation', 'unexpected_use'],
	                [
	                	'value' => $blocksExpression[$curIndex]['value'],
	                ],
	                $dzetaCalc,
	                [
	                    'ps' => $blocksExpression[$curIndex]['ps'],
	                    'pf' => $blocksExpression[$curIndex]['pf'],
	                    'rows' => $blocksExpression[$curIndex]['rows'],
	                ]
	            );
	            break;
			}
			if ($isInFunction == 2 && $blocksExpression[$curIndex]['type'] == Constants::DELIMITER_FUNCTION)
			{
				if (!isset($blocksExpression[$curIndex + 1]) || ($blocksExpression[$curIndex + 1]['type'] == Constants::DELIMITER_FUNCTION))
				{
					Error::addError(
		                ['general'],
		                ['calculation', 'unexpected_use'],
		                [
		                	'value' => $blocksExpression[$curIndex]['value'],
		                ],
		                $dzetaCalc,
		                [
		                    'ps' => $blocksExpression[$curIndex]['ps'],
		                    'pf' => $blocksExpression[$curIndex]['pf'],
		                    'rows' => $blocksExpression[$curIndex]['rows'],
		                ]
		            );
		            break;
				}
			}
			if ($blocksExpression[$curIndex]['type'] == Constants::TYPE_NUMBER)
			{
				$flagDopNum = false;
				//разбираем числа блоки
				$num = $blocksExpression[$curIndex]['value'];
				$indDo = $curIndex;
				$pf = $blocksExpression[$curIndex]['pf'];
				$arrKeys = array_keys($blocksExpression);
				$key1 = array_search($curIndex, $arrKeys);
				if (isset($arrKeys[$key1 - 1]) && isset($blocksExpression[$arrKeys[$key1 - 1]]))
				{
					if ($blocksExpression[$arrKeys[$key1 - 1]]['type'] == Constants::TYPE_OPER_SPC)
					{
						if (in_array($blocksExpression[$arrKeys[$key1 - 1]]['value'], Constants::OPERATIONS_LOW))
						{
							if (isset($arrKeys[$key1 - 2]) && isset($blocksExpression[$arrKeys[$key1 - 2]]))
							{
								if (in_array($blocksExpression[$arrKeys[$key1 - 2]]['type'], [Constants::TYPE_OPER_SPC, Constants::TYPE_BRACKETS_SIMPLE, Constants::TYPE_DELIM_VAR]))
								{
									if (Constants::TYPE_BRACKETS_SIMPLE == $blocksExpression[$arrKeys[$key1 - 2]]['type'] && $blocksExpression[$arrKeys[$key1 - 2]]['value'] == Constants::BRACKETS_SIMPLE[1])
									{

									} else
									{
										$flagDopNum = true;
									}
								}
							} else
							{
								$flagDopNum = true;
							}
						}
					}
				}

				if ($flagDopNum)
				{
					$pf = $blocksExpression[$arrKeys[$key1 - 1]]['pf'];
					$num = $blocksExpression[$arrKeys[$key1 - 1]]['value'] . $num;
					unset($blocksExpression[$arrKeys[$key1 - 1]]);
				}
				$flagDelim = false;
				$prevNum = true;
				for ($curIndex++; $curIndex < $countBlock; $curIndex++)
				{
					if ($blocksExpression[$curIndex]['type'] == Constants::TYPE_NUMBER)
					{
						$num .= $blocksExpression[$curIndex]['value'];
						$pf = $blocksExpression[$curIndex]['pf'];
						unset($blocksExpression[$curIndex]);
						$prevNum = true;
					} else if ($blocksExpression[$curIndex]['type'] == Constants::DELIMITER_NUMBER_AND_METHOD && isset($blocksExpression[$curIndex + 1]) && $blocksExpression[$curIndex + 1]['type'] == Constants::TYPE_NUMBER)
					{
						if ($flagDelim)
						{
							Error::addError(
				                ['general'],
				                ['calculation', 'unexpected_use'],
				                [
				                	'value' => $blocksExpression[$curIndex]['value'] . $blocksExpression[$curIndex + 1]['value'],
				                ],
				                $dzetaCalc,
				                [
				                    'ps' => $blocksExpression[$curIndex]['ps'],
				                    'pf' => $blocksExpression[$curIndex + 1]['pf'],
				                    'rows' => array_unique([
				                    	...$blocksExpression[$curIndex]['rows'],
				                    	...$blocksExpression[$curIndex + 1]['rows']
				                    ]),
				                ]
				            );
				            break;
						}
						$num .= $blocksExpression[$curIndex]['value'];
						$pf = $blocksExpression[$curIndex]['pf'];
						unset($blocksExpression[$curIndex]);
						$flagDelim = true;
						$prevNum = false;
					} else
					{
						if ($prevNum && !in_array($blocksExpression[$curIndex]['value'], $OPERATIONS_AND_BRACKETS) && $isInFunction != 2)
						{
							Error::addError(
				                ['general'],
				                ['calculation', 'unexpected_use'],
				                [
				                	'value' => $blocksExpression[$curIndex]['value'],
				                ],
				                $dzetaCalc,
				                [
				                    'ps' => $blocksExpression[$curIndex]['ps'],
				                    'pf' => $blocksExpression[$curIndex]['pf'],
				                    'rows' => $blocksExpression[$curIndex]['rows'],
				                ]
				            );
				            break;
						}

						if ($isInFunction == 2 && $blocksExpression[$curIndex]['type'] == Constants::DELIMITER_FUNCTION)
						{
							if (!isset($blocksExpression[$curIndex + 1]) || $blocksExpression[$curIndex + 1]['type'] == Constants::DELIMITER_FUNCTION || ($blocksExpression[$curIndex + 1]['type'] == Constants::TYPE_BRACKETS_SIMPLE && $blocksExpression[$curIndex + 1]['value'] == Constants::BRACKETS_SIMPLE[1]))
							{
								Error::addError(
					                ['general'],
					                ['calculation', 'unexpected_use'],
					                [
					                	'value' => $blocksExpression[$curIndex]['value'],
					                ],
					                $dzetaCalc,
					                [
					                    'ps' => $blocksExpression[$curIndex]['ps'],
					                    'pf' => $blocksExpression[$curIndex]['pf'],
					                    'rows' => $blocksExpression[$curIndex]['rows'],
					                ]
					            );
					            break;
							}
						}

						$curIndex--;
						$prevNum = false;
						break;
					}
				}
				$blocksExpression[$indDo]['value'] = (float)$num;
				$blocksExpression[$indDo]['pf'] = $pf;
			} else if ($blocksExpression[$curIndex]['type'] == Constants::TYPE_FUNCTION)
			{
				$isInFunction = 1;
			} else if ($blocksExpression[$curIndex]['type'] == Constants::TYPE_BRACKETS_SIMPLE)
			{
				if ($isInFunction == 1 && $blocksExpression[$curIndex]['value'] == Constants::BRACKETS_SIMPLE[0])
				{
					$isInFunction = 2;
				} else if (!$openBracket) {
					$isInFunction = 0;
				}
				if ($isInFunction == 2)
				{
					if ($blocksExpression[$curIndex]['value'] == Constants::BRACKETS_SIMPLE[0])
					{
						$openBracket = true;
					} else
					{
						$openBracket = false;
					}
				}
			}
			if (isset($blocksExpression[$curIndex]))
			{
				if (in_array($blocksExpression[$curIndex]['value'], $OPERATIONS_AND_BRACKETS))
				{
					$prevOper = true;
				} else
				{
					$prevOper = false;
				}
			} else
			{
				$prevOper = false;
			}
		}

		//unset($blocksExpression[0], $blocksExpression[1]);
		$blocksExpression = array_values($blocksExpression);
		
		$varBlock = null;
		/*тут надо иначе делать, делитель не обязательно второй*/

		$arrCol = array_column($blocksExpression, 'type');
		$indSearch = array_search(Constants::TYPE_DELIM_VAR, $arrCol);
		$var = null;
		if ($indSearch&&!$returnOnlyAction)
		{
			$varBlock = array_slice($blocksExpression, 0, $indSearch);
			$var = $blocksExpression[0];
			if (count($varBlock) == 1)
			{
				$var = $blocksExpression[0];
			} else
			{
				$var = $varBlock[0];
				
				/*if(!isset(self::parseMethodBlocks($varBlock)[0]['arr']))
				{
					echo 777;
					var_dump(self::parseMethodBlocks($varBlock));
				}*/
				$actionVar = self::parseMethodBlocks($varBlock)[0]['arr'];
			}
			$blocksExpression = array_slice($blocksExpression, $indSearch + 1);
		}
		$blocksExpression = self::parseMethodBlocks($blocksExpression);
		if($returnOnlyAction)
		{
			return $blocksExpression;
		}
		$answer = [
			'variable' => $var,
			'action' => $blocksExpression
		];
		if (!empty($actionVar))
		{
			$answer['arr'] = $actionVar;
		}
		//тут еще можно блок на анализ всего что осталось, чтобы выйдить лишнее
		return $answer;
	}

	public static function parseMethodBlocks($blocksExpression)
	{
		$availabletypeorMerhod = [
			Constants::TYPE_VARIABLE,
			Constants::TYPE_FUNCTION,
			Constants::TYPE_STRING,
			Constants::TYPE_NUMBER
		];
		$blocksExpression = Analytics::parseArgFunction($blocksExpression);

		//теперь собираем методы
		for ($curIndex = 0, $countBlock = count($blocksExpression); $curIndex < $countBlock; $curIndex++)
		{
			if ($blocksExpression[$curIndex]['type'] == Constants::DELIMITER_NUMBER_AND_METHOD && isset($blocksExpression[$curIndex - 1]) && in_array($blocksExpression[$curIndex - 1]['type'], $availabletypeorMerhod) && isset($blocksExpression[$curIndex + 1]) && $blocksExpression[$curIndex + 1]['type'] == Constants::TYPE_FUNCTION)
			{
				if (!isset($blocksExpression[$curIndex - 1]['action']))
				{
					$blocksExpression[$curIndex - 1]['action'] = [];
				}
				$blocksExpression[$curIndex - 1]['action'][] = $blocksExpression[$curIndex + 1];
				unset($blocksExpression[$curIndex], $blocksExpression[$curIndex + 1]);
				$blocksExpression = array_values($blocksExpression);
				$curIndex = 0;
				$countBlock = count($blocksExpression);
			}
		}
		$blocksExpression = array_values($blocksExpression);

		//делаем обработку для массивов
		for ($curIndex = 0, $countBlock = count($blocksExpression); $curIndex < $countBlock; $curIndex++)
		{
			if ($blocksExpression[$curIndex]['type'] == Constants::TYPE_BRACKETS_ARRAY && $blocksExpression[$curIndex]['value'] == Constants::BRACKETS_ARRAY_SIMPLE[0])
			{
				//тут надо искать завершенную скобку, а также ивенты, при этом доступ может идти по порядку, типа [0][1][2]
				Analytics::parseArgElemArr($blocksExpression, $curIndex);
				$curIndex = -1;
				$blocksExpression = array_values($blocksExpression);
				$countBlock = count($blocksExpression);
			}
		}
		return $blocksExpression;
	}

	public static function parseArgElemArr(&$blocksExpression, $startBracket)
	{
		$countBrackets = 1;
		$flagDop = false;
		for ($curIndex = $startBracket + 1, $countBlock = count($blocksExpression); $curIndex < $countBlock; $curIndex++)
		{
			if ($blocksExpression[$curIndex]['type'] == Constants::TYPE_BRACKETS_ARRAY)
			{
				if ($blocksExpression[$curIndex]['value'] == Constants::BRACKETS_ARRAY_SIMPLE[0])
				{
					Analytics::parseArgElemArr($blocksExpression, $curIndex);
					$curIndex = $startBracket;
					$blocksExpression = array_values($blocksExpression);
					$countBlock = count($blocksExpression);
					continue;
				} else if($blocksExpression[$curIndex]['value'] == Constants::BRACKETS_ARRAY_SIMPLE[1])
				{
					$countBrackets--;
				}
				if ($countBrackets == 0)
				{
					//значит у нас обнуление и надо чуть править
					//$startBracket-начало блока
					//$curIndex-окончание блока
					if (isset($blocksExpression[$startBracket - 1]) && $blocksExpression[$startBracket - 1]['type'] == Constants::TYPE_VARIABLE)
					{
						if (!isset($blocksExpression[$startBracket - 1]['arr']))
						{
							$blocksExpression[$startBracket - 1]['arr']=[];
						}
						$argArr = [];
						for ($i = $startBracket + 1; $i < $curIndex; $i++)
						{
							$argArr[] = $blocksExpression[$i];
							unset($blocksExpression[$i]);
						}
						unset($blocksExpression[$curIndex]);
						unset($blocksExpression[$startBracket]);
						$blocksExpression[$startBracket - 1]['arr'][] = Analytics::parseArgFunction($argArr);
						return;
					}
				}
			}
		}
	}

	public static function createOrderElements($blocksExpression)
	{
		$pf = array();
        $ops = new Stack();
        //массивы с операциями по приоритету
        $O_VH = Constants::OPERATIONS_VERY_HIGH;
        $O_H = [
        	...Constants::OPERATIONS_HIGH,
        	...Constants::OPERATIONS_HIGH_LOGIC
        ];
        $O_L = [
        	...Constants::OPERATIONS_LOW,
        	...Constants::OPERATIONS_LOGIC
        ];
        $O_VHHL = [
        	...$O_L,
        	...$O_H,
        	...$O_VH
        ];

        $pfIndex = 0;

        foreach ($blocksExpression as $valueBE)
        {
        	if ($valueBE['type'] == Constants::TYPE_BRACKETS_SIMPLE)
        	{
        		if ($valueBE['value'] == Constants::BRACKETS_SIMPLE[0])
        		{
        			$ops->push($valueBE);
        		} else if ($valueBE['value'] == Constants::BRACKETS_SIMPLE[1])
        		{
        			while ($ops->peek() && $ops->peek()['type'] != Constants::TYPE_BRACKETS_SIMPLE && $ops->peek()['value'] != Constants::BRACKETS_SIMPLE[0])
        			{
        				$pf[++$pfIndex] = $ops->pop();
        			}
        			$ops->pop();
        		}
        	} else if ($valueBE['type'] == Constants::TYPE_OPER_SPC && in_array($valueBE['value'], $O_VH))
        	{
        		while ($ops->peek() && $ops->peek()['type'] == Constants::TYPE_OPER_SPC && in_array($ops->peek()['value'], $O_VH))
        		{
        			$pf[++$pfIndex] = $ops->pop();
                }
                $ops->push($valueBE);
                $pfIndex++;
        	} else if($valueBE['type'] == Constants::TYPE_OPER_SPC && in_array($valueBE['value'], $O_H))
        	{
        		while ($ops->peek() && $ops->peek()['type'] == Constants::TYPE_OPER_SPC && (in_array($ops->peek()['value'], $O_H) || in_array($ops->peek()['value'], $O_VH)))
        		{
        			 $pf[++$pfIndex] = $ops->pop();
        		}
                $ops->push($valueBE);
                $pfIndex++;
        	} else if ($valueBE['type'] == Constants::TYPE_OPER_SPC && in_array($valueBE['value'], $O_L))
        	{
        		while ($ops->peek() && $ops->peek()['type'] == Constants::TYPE_OPER_SPC && in_array($ops->peek()['value'], $O_VHHL))
        		{
        		    $pf[++$pfIndex] = $ops->pop();
        		}
                $ops->push($valueBE);
                $pfIndex++;
        	} else
        	{
        		$pf[$pfIndex++] = $valueBE;
        	}
        }
        while (($tmp = $ops->pop()) !== false)
        {
          	$pf[++$pfIndex] = $tmp;
        }
        $pf = array_values($pf);
        return $pf;
	}

	public static function parseArgFunction($argFunctions)
	{
		for ($curIndex = 0, $countBlock = count($argFunctions); $curIndex < $countBlock; $curIndex++)
		{
			//TODO- данный блок просмотреть
			if ($argFunctions[$curIndex]['type'] == Constants::TYPE_FUNCTION)
			{
				$indDo = $curIndex;
				if($argFunctions[$curIndex]['value']=='else')
				{

				}elseif (!(isset($argFunctions[$curIndex + 1]) && $argFunctions[$curIndex + 1]['type'] == Constants::TYPE_BRACKETS_SIMPLE && $argFunctions[$curIndex + 1]['value'] == Constants::BRACKETS_SIMPLE[0]))
				{					
					throw new \Exception("Косяк с функциями с началом 2", 1);
				}else
				{
					unset($argFunctions[$curIndex+1]);
				}
				$pf = $argFunctions[$curIndex]['pf'];
				
				$argFunc = [];
				$argFuncG = [];
				$countBrackets = 1;
				for ($curIndex += 2; $curIndex < $countBlock; $curIndex++)
				{
					$pf = $argFunctions[$curIndex]['pf'];
					if ($argFunctions[$curIndex]['type'] == Constants::TYPE_BRACKETS_SIMPLE)
					{
						if ($argFunctions[$curIndex]['value'] == Constants::BRACKETS_SIMPLE[0])
						{
							$countBrackets++;
						} else if ($argFunctions[$curIndex]['value'] == Constants::BRACKETS_SIMPLE[1])
						{
							$countBrackets--;
						}
					}
					if ($argFunctions[$curIndex]['type'] == Constants::TYPE_BRACKETS_SIMPLE && $argFunctions[$curIndex]['value'] == Constants::BRACKETS_SIMPLE[1] && $countBrackets == 0)
					{

						unset($argFunctions[$curIndex]);
						break;
					}
					if ($argFunctions[$curIndex]['type'] == Constants::DELIMITER_FUNCTION && $countBrackets == 1)
					{
						$argFuncG[] = $argFunc;
						$argFunc = [];
						unset($argFunctions[$curIndex]);
						continue;
					}

					$argFunc[] = $argFunctions[$curIndex];
					unset($argFunctions[$curIndex]);
				}
				if (!empty($argFunc))
				{
					$argFuncG[] = $argFunc;
				}

				foreach ($argFuncG as $keyargFunctionsG => $argFunctionsGvalue)
				{
					$argss = Analytics::parseArgFunction($argFunctionsGvalue);
					$argFuncG[$keyargFunctionsG] = [
						'action' => $argss,
						'pf' => count($argss) > 0 ? max(array_column($argss, 'pf')) : 0,
						'ps' => count($argss) > 0 ? min(array_column($argss, 'ps')) : 0,
						'rows' => array_merge(array_column($argss, 'rows')),
					];
				}

				$argFunctions[$indDo]['args'] = $argFuncG;
				$argFunctions[$indDo]['pf'] = $pf;
			}
		}

		$argFunctions = array_values($argFunctions);
		for ($curIndex = 0, $countBlock = count($argFunctions); $curIndex < $countBlock; $curIndex++)
		{
			if ($argFunctions[$curIndex]['type'] == Constants::TYPE_BRACKETS_ARRAY && $argFunctions[$curIndex]['value'] == Constants::BRACKETS_ARRAY_SIMPLE[0])
			{
				//тут надо искать завершенную скобку, а также ивенты, при этом доступ может идти по порядку, типа [0][1][2]
				Analytics::parseArgElemArr($argFunctions, $curIndex);
				$curIndex = -1;
				$argFunctions = array_values($argFunctions);
				$countBlock = count($argFunctions);
			}
		}
		$argFunctions = array_values($argFunctions);
		$argFunctions = Analytics::createOrderElements($argFunctions);
		return $argFunctions;
	}
}