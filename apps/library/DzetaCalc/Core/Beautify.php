<?php

namespace DzetaCalcParser\Core;

/*Класс для создания красивого кода*/
class Beautify
{
	public static $symbolMargin = '   ';
	public static $symbolBreak = '<br/>';

	public static function formatCodeFormula($treeFormula, $countMargin = 0)
	{
		$formulaBeaut = '';

		foreach ($treeFormula as $key => $value)
		{
			if ($value['type'] == Constants::TYPE_ACTION_SIMPLE)
			{
				$formulaBeaut .= Beautify::formatSimpleExpression($value['formula'], $countMargin);
			} else if ($value['type'] == Constants::TYPE_ACTION_FOR)
			{
				$formulaBeaut .= Beautify::formatForExpression($value, $countMargin);
			} else if ($value['type'] == Constants::TYPE_ACTION_IF)
			{
				$formulaBeaut .= Beautify::formatIFExpression($value, $countMargin);
			}
		}
		return $formulaBeaut;
	}

	private static function formatIFExpression($treeIF, $countMargin)
	{
		$formula = '';
		foreach ($treeIF['dopif'] as $value)
		{
			if ($value['type'] == Constants::TYPE_ACTION_IF)
			{
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . 'if(' . $value['condition']['formula'] . ')' . Beautify::$symbolBreak;
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '{' . Beautify::$symbolBreak;
				$formula .= Beautify::formatCodeFormula($value['body'], $countMargin + 1);
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '}' . Beautify::$symbolBreak;
			} else if ($value['type'] == Constants::TYPE_ACTION_ELSEIF)
			{
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . 'elseif(' . $value['condition']['formula'] . ')' . Beautify::$symbolBreak;
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '{' . Beautify::$symbolBreak;
				$formula .= Beautify::formatCodeFormula($value['body'], $countMargin + 1);
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '}' . Beautify::$symbolBreak;
			} else if($value['type']==Constants::TYPE_ACTION_ELSE)
			{
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . 'else' . Beautify::$symbolBreak;
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '{' . Beautify::$symbolBreak;
				$formula .= Beautify::formatCodeFormula($value['body'], $countMargin + 1);
				$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '}' . Beautify::$symbolBreak;
			}
		}
		return $formula;
	}

	private static function formatForExpression($treeFor, $countMargin)
	{
		$formula = '';
		$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . 'for(' ;
		if (empty($treeFor['initialisation']))
		{
			$formula .= Constants::SYMBOL_END_EXPRESSION;
		} else
		{
			$formula .= $treeFor['initialisation']['formula'] . Constants::SYMBOL_END_EXPRESSION;
		}
		if (empty($treeFor['condition']))
		{
			$formula .= Constants::SYMBOL_END_EXPRESSION;
		} else
		{
			$formula .= $treeFor['condition']['formula'] . Constants::SYMBOL_END_EXPRESSION;
		}
		if (empty($treeFor['modification']))
		{
			$formula .= ')';
		} else
		{
			$formula .= $treeFor['modification']['formula'] . ')';
		}
		$formula .= Beautify::$symbolBreak;
		$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '{' . Beautify::$symbolBreak;
		$countMargin++;
		$formula .= Beautify::formatCodeFormula($treeFor['bodyFor']['body'], $countMargin);
		$countMargin--;
		$formula .= str_repeat(Beautify::$symbolMargin, $countMargin) . '}' . Beautify::$symbolBreak;
		return $formula;
	}

	private static function formatSimpleExpression($formula, $countMargin)
	{
		return str_repeat(Beautify::$symbolMargin, $countMargin) . $formula . Constants::SYMBOL_END_EXPRESSION . Beautify::$symbolBreak;
	}


	//функция которая приводит данные к виду футера таблицы
	public static function formatFooterDataTable($resultData)
	{
		$result = 0; 
        $data=[];
		if($resultData['type']==\DzetaCalcParser\Core\Constants::TYPE_ARRAY)
        {
            foreach ($resultData['value'] as $valueA) 
            {
                if($valueA['type']==\DzetaCalcParser\Core\Constants::TYPE_ARRAY)
                {
                    $row=[];
                    foreach ($valueA['value'] as $valueD) 
                    {
                        if($valueD['type']==\DzetaCalcParser\Core\Constants::TYPE_STRING)
                        {
                            $row[]=['v'=>$valueD['value']];
                        }elseif($valueD['type']==\DzetaCalcParser\Core\Constants::TYPE_NULL)
                        {
                            $row[]=['v'=>''];
                        }
                    }
                    $result = 1;
                    $data[]=$row;
                }else
                {
                   $result = 0; 
                   $data=[];  
                   break;
                }
            }
        }
        return ['result'=>$result,'data'=>$data];
	}

	public static function formatCalcListData($resultData)
	{
		$result = 0; 
        $data=[];
        if($resultData['type']==\DzetaCalcParser\Core\Constants::TYPE_ARRAY)
        {
            foreach ($resultData['value'] as $valueA) 
            {
                if($valueA['type']==\DzetaCalcParser\Core\Constants::TYPE_OBJECT)
                {
                	$data[]=
                	[
                		'uid'=>$valueA['value'],
                		'name'=>$valueA['props']['name'],
                		'type'=>'object'
                	];
                	$result = 1;
                }elseif($valueA['type']==\DzetaCalcParser\Core\Constants::TYPE_NUMBER)
                {      
                	$data[]=
                	[
                		'v'=>$valueA['value'],
                		'type'=>'number'
                	];
                	$result = 1;
                }elseif($valueA['type']==\DzetaCalcParser\Core\Constants::TYPE_STRING)
                {      
                	$data[]=
                	[
                		'v'=>$valueA['value'],
                		'type'=>'string'
                	];
                	$result = 1;
                }
            }
        }
        return ['result'=>$result,'data'=>$data];
	}
}