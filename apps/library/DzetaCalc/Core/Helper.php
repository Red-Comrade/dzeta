<?php

namespace DzetaCalcParser\Core;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\Type\DataType;

use Dzeta\Models\Obj;

class Helper
{
	public static function explodeFuncArgs($formula, $sign = ",")
    {
    	//тут еще флаг строки надо
        $args_array = array();
        $brak = 0;
        $lastV = 0;
        $flagString = false;
        for ($i = 0, $len = mb_strlen($formula); $i < $len; $i++)
        {
            $charFrm = mb_substr($formula, $i, 1);

			if ($i > 0)
			{
				$prevSymbol = mb_substr($formula, $i - 1, 1);
			}

			if($charFrm == Constants::SYMBOL_STRING)
			{
				if ($flagString)
				{
					if ($prevSymbol != Constants::SYMBOL_ESCAPE)
					{
						$flagString = false;
					}
				} else
                {
					$flagString = true;
				}
			}
            if (($i + 1) == $len)
            {
                $args_array[] = mb_substr($formula, ($lastV == 0 ? $lastV : $lastV + 1), $len - $lastV);
            }
            if ($charFrm == $sign && $brak == 0 && !$flagString)
            {
                $args_array[] = mb_substr($formula, ($lastV == 0 ? $lastV : $lastV + 1), ($lastV == 0 ? $i - $lastV : $i - $lastV - 1));
                $lastV = $i;
                if (($i + 1) == $len)
                {
                    $i--;
                }
            }
            if ($charFrm == Constants::BRACKETS_SIMPLE[0] && !$flagString)
            {
                $brak++;
            }
            if ($charFrm == Constants::BRACKETS_SIMPLE[1] && !$flagString)
            {
                $brak--;
            }
        }
        return $args_array;
    }

    public static function getRawArray($variable)
    {
        $array = [];

        foreach ($variable['value'] as $value) {
            if ($value['type'] == Constants::TYPE_ARRAY) {
                $array[] = self::getRawArray($value);
            } else {
                $array[] = $value['value'];
            }
        }

        return $array;
    }

    public static function convertPhpValue($value, $position, $props = [])
    {
        $variable = [
            'value' => $value,
            'type' => null,
            'ps' => $position['ps'],
            'pf' => $position['pf'],
            'rows' => isset($position['rows']) ? $position['rows'] : [],
            'props' => $props,
        ];
        if (is_array($value))
        {
            $variable['value'] = [];
            $variable['type'] = Constants::TYPE_ARRAY;
            foreach ($value as $k => $v)
            {
                $variable['value'][$k] = self::convertPhpValue($v, $position);
            }
        } else
        {
            if ($value instanceof Instance)
            {
                $variable['type'] = Constants::TYPE_OBJECT;
                $variable['value'] = $value->getUid();
                $variable['name'] = $value->getName();
                $class = $value->getType();
                if (empty($class))
                {
                    $_obj = new Obj();
                    $_obj->GetClassForObject($value);
                    $class = $value->getType();
                }
                $variable['props']['objClass'] = !empty($class) ? $class->getUid() : '';
            } else if ($value instanceof Attribute)
            {
                $variable['type'] = Constants::TYPE_ATTRIBUTE;
                $variable['value'] = $value->getUid();
                $variable['props']['name'] = $value->getName();
                $class = $value->getType();
                $variable['props']['attrClass'] = !empty($class) ? $class->getUid() : '';
            } else if ($value instanceof Core\Type\Type)
            {
                $variable['type'] = Constants::TYPE_CLASS;
                $variable['value'] = $value->getUid();
                $variable['props']['name'] = $value->getName();
            } else if (is_bool($value))
            {
                $variable['type'] = Constants::TYPE_BOOL;
            } else if (is_string($value))
            {
                $variable['type'] = Constants::TYPE_STRING;
            } else if (is_float($value) || is_int($value))
            {
                $variable['type'] = Constants::TYPE_NUMBER;
            } else
            {
                $variable['type'] = Constants::SPEC_VALUES['null']['type'];
                $variable['value'] = Constants::SPEC_VALUES['null']['value'];
            }
        }
        return $variable;
    }

    public static function convertAttributeValue(Attribute $attr, $position, $props = [])
    {
        $answer = null;
        if ($attr->isArray())
        {
            $answer = [
                'type' => Constants::TYPE_ARRAY,
                'value' => [],
                'ps' => $position['ps'],
                'pf' => $position['pf'],
                'rows' => $position['rows'],
                'props' => $props
            ];
            $type = $attr->getDatatype();
            $values = $attr->getValue();
            foreach ($values as $value) {
                $answer['value'][] = self::convertDatabaseValue($value, $type, $position, $props);
            }
        } else
        {
            $answer = self::convertDatabaseValue($attr->getValue(), $attr->getDatatype(), $position, $props);
        }
        return $answer;
    }

    public static function convertDatabaseValue($value, $type, $position, $props = [])
    {
        $answer = null;
        if (!is_null($value))
        {
            $answer = [
                'ps' => $position['ps'],
                'pf' => $position['pf'],
                'rows' => $position['rows'],
                'props' => $props,
            ];
            switch ($type) {
                case DataType::TYPE_TEXT:
                    $answer['type'] = Constants::TYPE_STRING;
                    $answer['value'] = $value->getValue();
                    break;
                case DataType::TYPE_NUMBER:
                case DataType::TYPE_COUNTER:
                    $answer['type'] = Constants::TYPE_NUMBER;
                    $answer['value'] = $value->getValue();
                    break;
                case DataType::TYPE_DATETIME:
                    $answer['type'] = Constants::TYPE_DATETIME;
                    $answer['value'] = $value->getValueN();
                    break;
                case DataType::TYPE_OBJECT:
                    $answer['type'] = Constants::TYPE_OBJECT;
                    $answer['value'] = $value->getUid();
                    $answer['props']['name'] = $value->getName();
                    $answer['props']['objClass'] = $value->getType()->getUid();
                    break;
                default:
                    $answer = null;
                    break;
            }
        }
        return $answer;
    }

    public static function makeNullVariable(array $props)
    {
        $null = Constants::SPEC_VALUES['null'];
        $null['ps'] = isset($props['ps']) ? $props['ps'] : 0;
        $null['pf'] = isset($props['pf']) ? $props['pf'] : 0;
        $null['rows'] = isset($props['rows']) ? $props['rows'] : [];
        return $null;
    }

    public static function saveContextJSON($dzetaCalc, $currentPath)
    {
        $jsonArr = [
            'tree' => $dzetaCalc->formulaTree,
            'functions' => $dzetaCalc->functions,
            'variables' => $dzetaCalc->variables,
            'formula' => $dzetaCalc->formula,
            'path' => $currentPath
        ];
        return json_encode($jsonArr);
    }

    public static function convertViewDopSettingJSON(Core\App\View $view)
    {
        $arrResult=[];
        $needKeysDopSett=['filter','view_sorts','ParentView','ParentObj','SelectObjs'];
        foreach ($needKeysDopSett as $keyN) 
        {
            $arrResult[$keyN]=$view->getDopSett($keyN);
        }
        return json_encode($arrResult);
    }

    public static function findCloseRoundBracketExpression($expression,$posStart)
    {        
        $closePos = null;
        $countBracket=1;
        $flagString=false;
        for($ind=$posStart+1,$lenStr=mb_strlen($expression);$ind<$lenStr;$ind++)
        {
            $curSymb=mb_substr($expression, $ind, 1);
            if($curSymb==Constants::SYMBOL_STRING)
            {
                $flagString=!$flagString;
            }elseif(($curSymb==Constants::BRACKETS_SIMPLE[0])&&!$flagString)
            {
                $countBracket++;
            }elseif(($curSymb==Constants::BRACKETS_SIMPLE[1])&&!$flagString)
            {
                $countBracket--;
            }
            if($countBracket==0)
            {
                $closePos = $ind;
                break;
            }
        }
        return $closePos;
    }
}