<?php

namespace DzetaCalcParser\Core;

//класс который анализирует код на данные из БД и получает все данные из БД потом для их замены
class AnalyticsDatabase
{
	/*
		Подумать над обратной заменой, также продумать когда атрибут будет у нас типа объект и переприсваивается другой перемнной
		а также переприсваивание переменной
	*/

	/*Тут мы храним переменную и ее текущий тип и атрибут для последующего анализа*/
	public $dataVariableCurrent = [];

	/*тут храним массив в котором храним массив
		имя класса->
		[
			уид класса
			массив имен атрибутов->имя атрибута
			[
				уид атрибута
			]


		]
	*/
	public $dataTypeAttrsDB=[];

	public function __construct()
	{

	}

	public function addVariableType($nameVar, $class, $obj, $type)
	{
		$this->dataVariableCurrent[$nameVar] = [
			'class' => $class,
			'object' => $obj,
			'type' => $type
		];
	}

	public function addTypeAttrDB($nameClass, $uidClass)
	{
		$this->dataTypeAttrsDB[$nameClass] = [
			'uid' => $uidClass,
			'attrs' => []
		];
	}

	public function beginAnalytics($treeExpression)
	{
		foreach ($treeExpression as $keyExpress => $expression)
		{
			if (isset($expression['body']))
			{
				if (isset($expression['body']['action']))
				{
					if (!empty($expression['body']['variable']['value']))
					{
						$this->analyticsAction($expression['body']['action'], '', $expression['body']['variable']['value']);
					} else
					{
						$this->analyticsAction($expression['body']['action']);
					}
				}
			}
		}
	}

	public function analyticsAction($actions, $nameVarM = '', $nameVarExpress = '')
	{
		foreach ($actions as $key => $value)
		{
			if ($value['type'] == Constants::TYPE_VARIABLE)
			{
				//тут у нас метод идет
				$this->analyticsAction($value['action'], $value['value'], $nameVarExpress);
			} else if ($value['type'] == Constants::TYPE_FUNCTION)
			{
				if ($value['value'] == 'getAttr')
				{
					$nameAttr = $value['args'][0]['action'][0]['value'];
					if (isset($this->dataVariableCurrent[$nameVarM]))
					{
						//значит переменная есть
						$nameClass = $this->findNameClassUID($this->dataVariableCurrent[$nameVarM]['class']);
						$uidAttr = $this->findAttrUIDforNameClass($nameClass, $nameAttr);

						if(!isset($this->dataTypeAttrsDB[$nameClass]['attrs'][$nameAttr]['pos']))
						{
							$this->dataTypeAttrsDB[$nameClass]['attrs'][$nameAttr]['pos'] = [];
						}
						$this->dataTypeAttrsDB[$nameClass]['attrs'][$nameAttr]['pos'][] = [
							'ps' => $value['ps'],
							'pf' => $value['pf']
						];
					} else
					{
						throw new Exception("Error Processing Request", 1);
					}

				}
			}
		}
	}

	/*метод который по уид возвращал */
	public function findNameClassUID($uidClass)
	{
		$nameClass = null;
		foreach ($this->dataTypeAttrsDB as $key => $value)
		{
			if ($value['uid'] == $uidClass)
			{
				$nameClass = $key;
			}
		}
		return $nameClass;
	}

	/*метод в котором мы ищем уид атрибута, зная имя класса и его уид*/
	public function findAttrUIDforNameClass($nameClass, $nameAttr)
	{
		$attrs = $this->dataTypeAttrsDB[$nameClass]['attrs'];
		if (isset($attrs[$nameAttr]))
		{
			return $attrs[$nameAttr]['uid'];
		} else
		{
			//ну а тут запрос к БД делаем
			$this->dataTypeAttrsDB[$nameClass]['attrs'][$nameAttr] = [];
			$this->dataTypeAttrsDB[$nameClass]['attrs'][$nameAttr]['uid'] = 'testAttrUID';
			$this->dataTypeAttrsDB[$nameClass]['attrs'][$nameAttr]['type'] = Constants::TYPE_NUMBER;
			return $this->dataTypeAttrsDB[$nameClass]['attrs'][$nameAttr]['uid'];
		}
	}

	/*метод в котором происходит магия по замене атрибутов на актуальные названия*/
	public static function replaceNameDBFormula($formula, $attrReplace)
	{
		//вначале надо выставть приоритеты по заменам
		$repArr = [];
		foreach ($attrReplace as $key => $valueClass)
		{
			foreach ($valueClass['attrs'] as $nameAttr => $valueAttr)
			{
				if($valueAttr['newName'] != $nameAttr)
				{
					foreach ($valueAttr['pos'] as $valuePos)
					{
						$repArr[$valuePos['ps']] = [
							'ps' => $valuePos['ps'],
							'pf' => $valuePos['pf'],
							'newName' => $valueAttr['newName'],
							'oldName' => $nameAttr
						];
					}
				}
			}
		}
		ksort($repArr);
		$delta = 0;
		foreach ($repArr as $key => $value)
		{
			//вырезаем из строки часть
			$tmpText = mb_strcut($formula, $value['ps'] + $delta, $value['pf'] - $value['ps']);
			if ($tmpText == '"' . $value['oldName'] . '"')
			{
				$formulanew = mb_strcut($formula, 0, $value['ps'] + $delta);
				$formulanew .= '"' . $value['newName'] . '"';
				$formulanew .= mb_strcut($formula, $value['pf'] + $delta);
				$delta += mb_strlen($value['newName']) - mb_strlen($value['oldName']);

				$formula = $formulanew;
			} else
			{
				throw new Exception("Проблема с заменой", 1);
			}
		}
	}
}