<?php

namespace DzetaCalcParser\Core;

class Locales
{
	const PATH_ERROR = '/Locales';
	public static $realPath = '';

	public static function init($language)
	{
		self::$realPath = self::PATH_ERROR . '/' . $language;
	}

	public static function getConfig($path)
	{
		$pathReal = dirname(__DIR__) . self::$realPath . '/' . implode('/', $path) . '.json';

		if (!file_exists($pathReal))
		{
			return [];
		}

		$config = json_decode(file_get_contents($pathReal), true);
		return $config;
	}

	public static function getText($arrKeys, $errConfig)
	{
		$key = array_shift($arrKeys);
		if (isset($errConfig[$key]))
		{
			if (count($arrKeys) == 0)
			{
				if(isset($errConfig[$key]['text']) && isset($errConfig[$key]['type']))
				{
					return $errConfig[$key];
				}
				return null;
			}
			return self::getText($arrKeys, $errConfig[$key]);
		}
		return null;
	}

	public static function replaceMarks($text, $marks)
	{
		foreach ($marks as $key => $value)
		{
			$text = str_replace('{'.$key.'}', $value, $text);
		}
		return $text;
	}
}