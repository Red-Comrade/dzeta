<?php

namespace DzetaCalcParser\Core;

use DzetaCalcParser\Core\Constants;

abstract class BaseFunctions
{

    public static function getFunctions($onlyNames = true)
    {
        $functions = static::doGetFunctions();
        foreach ($functions as $key => $function) {
            if ($onlyNames) {
                $functions[$key] = static::getFullFunctionName($function['name']);
            } else {
                $functions[$key]['name'] = static::getFullFunctionName($function['name']);
            }
        }
        return $functions;
    }

    public static function getFunctionsCompletions()
    {
        $methods = static::doGetFunctions();
        $category = static::getClassName(true);
        $configName = strtolower(str_replace('Functions', '', $category));
        $locales = Locales::getConfig(['documentation', $configName]);
        foreach ($methods as $key => $method) {
            if (isset($method['completion'])) {
                $text = Locales::getText([$key], $locales);
                if (!is_null($text)) {
                    $method['completion']['documentation'] = $text['text'];
                }
                $methods[$key] = $method['completion'];
                $methods[$key]['name'] = $key;
            } else {
                unset($methods[$key]);
            }
        }
        return ['category' => $category, 'functions' => array_values($methods)];
    }

    public static function getMethods($onlyNames = true)
    {
        $methods = static::doGetMethods();
        foreach ($methods as $key => $method) {
            if ($onlyNames) {
                $methods[$key] = static::getFullFunctionName($method['name']);
            } else {
                $methods[$key]['name'] = static::getFullFunctionName($method['name']);
            }
        }
        return $methods;
    }

    public static function getMethodsCompletions()
    {
        $methods = static::doGetMethods();
        $category = static::getClassName(true);
        foreach ($methods as $key => $method) {
            if (isset($method['completion'])) {
                $methods[$key] = $method['completion'];
                $methods[$key]['name'] = $key;
            } else {
                unset($methods[$key]);
            }
        }
        return ['category' => $category, 'methods' => array_values($methods)];
    }

    protected static function doGetMethods()
    {
        return static::$methods;
    }

    protected static function doGetFunctions()
    {
        return static::$functions;
    }

    protected static function checkArguments($calc, $functionName, $arguments, $position, $type = Constants::TYPE_FUNCTION)
    {
        $check = true;
        $function = $type == Constants::TYPE_FUNCTION ? self::getFunctionByName($functionName) : self::getMethodByName($functionName);
        if (count($arguments) != count($function['arguments'])) {
            $count = count($arguments);
            foreach ($function['arguments'] as $key => $argument) {
                if (!isset($arguments[$key])) {
                    if (isset($argument['optional']) && $argument['optional'] === true) {
                        $count++;
                    }
                }
            }
            $check = $count == count($function['arguments']);
            if (!$check) {
                Error::addError(
                    ['general'],
                    ['functions', 'params_count'],
                    [
                        'function' => $function['name'],
                        'passed_count' => count($arguments),
                        'function_count' => count($function['arguments'])
                    ],
                    $calc,
                    [
                        'ps' => $position['ps'],
                        'pf' => $position['pf'],
                        'rows' => $position['rows'],
                    ]
                );
            }
        }
        foreach ($function['arguments'] as $key => $argument) {
            if (isset($arguments[$key])) {
                $checkType = true;
                if (is_array($argument['type'])) {
                    $checkType = in_array($arguments[$key]['type'], $argument['type']);
                } else {
                    if ($argument['type'] != Constants::TYPE_ANY) {
                        $checkType = $arguments[$key]['type'] == $argument['type'];
                    }
                }
                if (!$checkType) {
                    $passedType = is_array($arguments[$key]['type']) ? implode(', ', $arguments[$key]['type']) : $arguments[$key]['type'];
                    $functionType = is_array($argument['type']) ? implode(', ', $argument['type']) : $argument['type'];
                    if (isset($arguments[$key]['method']) && $arguments[$key]['method'])
                    {
                        Error::addError(
                            ['general'],
                            ['functions', 'method_params_type'],
                            [
                                'function' => $function['calcName'],
                                'passed_type' => $passedType,
                                'function_type' => $functionType,
                            ],
                            $calc,
                            [
                                'ps' => $position['ps'],
                                'pf' => $position['pf'],
                                'rows' => $position['rows'],
                            ]
                        );
                    } else
                    {
                        Error::addError(
                            ['general'],
                            ['functions', 'params_type'],
                            [
                                'function' => $function['calcName'],
                                'index' => $type == Constants::TYPE_FUNCTION ? ($key + 1) : $key,
                                'passed_type' => $passedType,
                                'function_type' => $functionType,
                            ],
                            $calc,
                            [
                                'ps' => $position['ps'],
                                'pf' => $position['pf'],
                                'rows' => $position['rows'],
                            ]
                        );
                    }
                    $check = false;
                } else
                {
                    if (isset($argument['allowed_values']))
                    {
                        if (!in_array($arguments[$key]['value'], $argument['allowed_values']))
                        {
                            Error::addError(
                                ['general'],
                                ['functions', 'params_enum_matching'],
                                [
                                    'function' => $function['calcName'],
                                    'index' => $type == Constants::TYPE_FUNCTION ? ($key + 1) : $key,
                                    'value' => $arguments[$key]['value'],
                                    'allowed_values' => implode(', ', $argument['allowed_values']),
                                ],
                                $calc,
                                [
                                    'ps' => $position['ps'],
                                    'pf' => $position['pf'],
                                    'rows' => $position['rows'],
                                ]
                            );
                        }
                    }
                }
            }
        }
        return $check;
    }

    protected static function getNameFunctions($function)
    {
        $functions = array_merge(static::getFunctions());
        return array_search($function, $functions);
    }

    protected static function getNameMethods($method)
    {
        $methods = array_merge(static::getMethods());
        return array_search($method, $methods);
    }

    protected static function getClassName($short = false)
    {
        $reflect = new \ReflectionClass(get_called_class());
        $class = $short ? $reflect->getShortName() : $reflect->getName();
        return $class;
    }

    protected static function getFullFunctionName($function, $class = null)
    {
        $class = is_null($class) ? self::getClassName() : $class;
        $fullname = "\\$class::$function";
        return $fullname;
    }

    protected static function getFunctionByName($name)
    {
        $function = null;
        foreach (static::$functions as $key => $value) {
            if ($value['name'] == $name) {
                $function = $value;
                $function['calcName'] = $key;
                break;
            }
        }
        return $function;
    }

    protected static function getMethodByName($name)
    {
        $method = null;
        foreach (static::$methods as $key => $value) {
            if ($value['name'] == $name) {
                $method = $value;
                $method['calcName'] = $key;
                break;
            }
        }
        return $method;
    }
}