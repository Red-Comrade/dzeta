<?php

namespace DzetaCalcParser\Core;

class Constants {

	const DATETIME_FORMAT = 'd.m.Y H:i:s';

	const ESCAPE_SYMBOL = ['"'];

	const OPERATIONS_LOW = ['+', '-'];
	const OPERATIONS_HIGH = ['/', '*'];
	const OPERATIONS_VERY_HIGH = ['^', '++', '--', /*'~', '!'*/];
	const OPERATIONS_HIGH_LOGIC = ['||', '&&'];
	const OPERATIONS_LOGIC = ['>', '<', '>=', '<=', '!=', '=='];

	const BRACKETS_SIMPLE = ['(', ')'];
	const BRACKETS_ARRAY_SIMPLE = ['[', ']'];
	const BRACKETS_BLOCK_SIMPLE = ['{', '}'];

	const DELIMITER_FUNCTION = ',';
	const DELIMITER_VAR_EXPR = '=';
	const DELIMITER_NUMBER_AND_METHOD = '.';

	const START_VARIABLE = '$';
	const SYMBOL_STRING = '"';
	const SYMBOL_ESCAPE = '\\';
	const SYMBOL_END_EXPRESSION = ';';
	const SYMBOL_NEW_ROW = "\n";
	const SYMBOL_CARRIAGE_RETURN = "\r";
	const SYMBOL_SINGLE_COMMENT = '//';
	const SYMBOL_MULTI_COMMENT_START = '/*';
	const SYMBOL_MULTI_COMMENT_END = '*/';

	const SYMBOL_P = [
		Constants::DELIMITER_NUMBER_AND_METHOD,
		' ',
		'&',
		'|',
		'!',
		Constants::DELIMITER_FUNCTION,
		Constants::DELIMITER_VAR_EXPR,
		Constants::START_VARIABLE,
		...Constants::OPERATIONS_LOW,
		...Constants::OPERATIONS_HIGH,
		...Constants::OPERATIONS_VERY_HIGH,
		...Constants::OPERATIONS_HIGH_LOGIC,
		...Constants::OPERATIONS_LOGIC,
		...Constants::BRACKETS_SIMPLE,
		...Constants::BRACKETS_ARRAY_SIMPLE,
		...Constants::BRACKETS_BLOCK_SIMPLE,
	];

	const TYPE_STRING = 'string';
	const TYPE_NUMBER = 'number';
	const TYPE_DATETIME = 'datetime';
	const TYPE_ARRAY = 'array';
	const TYPE_OBJECT = 'object';
	const TYPE_ATTRIBUTE = 'attribute';
	const TYPE_CLASS = 'class';
	const TYPE_BOOL = 'bool';
	const TYPE_NULL = 'null';
	const TYPE_ANY = 'any';
	const TYPE_FOR_SPEC = 'for';

	const DATATYPES = [
		Constants::TYPE_STRING,
		Constants::TYPE_NUMBER,
		Constants::TYPE_DATETIME,
		Constants::TYPE_ARRAY,
		Constants::TYPE_OBJECT,
		Constants::TYPE_ATTRIBUTE,
		Constants::TYPE_CLASS,
		Constants::TYPE_BOOL,
		Constants::TYPE_NULL,
	];

	const TYPE_EXPRESSION_VARIABLE = 'variable';
	const TYPE_EXPRESSION_NOT_VARIABLE = 'not-variable';

	const TYPE_OPER_SPC = 'operation';
	const TYPE_EXPR_SPC = 'expression';
	const TYPE_DELIM_VAR = 'delim-var';
	const TYPE_DELIM_FUNC = 'delim-func';
	const TYPE_VARIABLE = 'variable';
	const TYPE_FUNCTION = 'function';
	const TYPE_METHOD = 'method';
	const TYPE_VARIABLE_EXP = 'variable-exp';
	const TYPE_BRACKETS_SIMPLE = 'brackets-simple';
	const TYPE_BRACKETS_ARRAY = 'brackets-array';

	const TYPE_ACTION_SIMPLE = 'simple';
	const TYPE_ACTION_FOR = 'for';
	const TYPE_ACTION_FOR_BREAK = 'break';
	const TYPE_ACTION_FOR_CONTINUE = 'continue';
	const TYPE_ACTION_IF = 'if';
	const TYPE_ACTION_ELSE = 'else';
	const TYPE_ACTION_ELSEIF = 'elseif';
	const TYPE_ACTION_SINGLE_COMMENT = 'single-comment';
	const TYPE_ACTION_MULTI_COMMENT = 'multi-comment';
	const TYPE_ACTION_RETURN = 'return';
	const TYPE_ACTION_SAVE_CONTEXT = 'save-context';
	const TYPE_ACTION_SAVE_NOCONTEXT = 'no-save-context'; //это когда поступила команда на сохранение контекста, но он еше не в JSON

	const SPEC_VALUES = [
		'true' => [
			'type' => Constants::TYPE_BOOL,
			'value' => true
		],
		'false' => [
			'type' => Constants::TYPE_BOOL,
			'value' => false
		],
		'null' => [
			'type' => Constants::TYPE_NULL,
			'value' => null
		],
		'break' => [
			'type' => Constants::TYPE_FOR_SPEC,
			'value' => Constants::TYPE_ACTION_FOR_BREAK
		],
		'continue' => [
			'type' => Constants::TYPE_FOR_SPEC,
			'value' => Constants::TYPE_ACTION_FOR_CONTINUE
		]
	];
}