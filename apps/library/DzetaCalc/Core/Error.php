<?php

namespace DzetaCalcParser\Core;

class Error
{
	public static function addError($pathArr, $pathErrorArr, $replaceArr, $dzetaCalc, $pos)
	{
		array_unshift($pathArr, 'errors');
		$errConfig = Locales::getConfig($pathArr);
		$text = Locales::getText($pathErrorArr, $errConfig);
		if(is_null($text))
		{
			throw new \Exception("404 ошибка не найдена", 1);
		}
		$text['text'] = Locales::replaceMarks($text['text'], $replaceArr);
		if ($text['type'] == 1)
		{
			//значит критическая ошибка
			$dzetaCalc->addError($text['text'], [
				'ps' => $pos['ps'],
				'pf' => $pos['pf'],
				'rows' => isset($pos['rows']) ? $pos['rows'] : []
			]);
		} else if ($text['type'] == 0)
		{
			//значит варнинг
			$dzetaCalc->addWarning($text['text'], [
				'ps' => $pos['ps'],
				'pf' => $pos['pf'],
				'rows' => isset($pos['rows']) ? $pos['rows'] : []
			]);
		}
	}
}