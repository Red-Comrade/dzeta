<?php

namespace DzetaCalcParser;

define('DZETA_CALC_PATH', realpath(dirname(__FILE__)));

use DzetaCalcParser\Core\Calculation;
use DzetaCalcParser\Core\Locales;

class DzetaCalc
{
    private $calculation = null;

	public function __construct($data)
	{
		$memory = memory_get_usage();
		$this->initCore();

        $language = 'en';
        if (isset($data['language'])) {
            $language = $data['language'];
        }
        Locales::init($language);

        $data['functions'] = $this->getFunctions();
        $data['methods'] = $this->getMethods();
        $this->calculation = new Calculation($data);

		//замер памяти
		$memory = memory_get_usage() - $memory;
		$i = 0;
		while (floor($memory / 1024) > 0) {
			$i++;
			$memory /= 1024;
		}
		$name = array('байт', 'КБ', 'МБ');
	}

	public static function getFunctions($onlyNames = true)
    {
        $functions = [];
        $classes = self::getFunctionsClasses();
        foreach ($classes as $class) {
            $functions = array_merge($functions, $class::getFunctions($onlyNames));
        }
        return $functions;
    }

	public static function getMethods($onlyNames = true)
    {
        $functions = [];
        $classes = self::getFunctionsClasses();
        foreach ($classes as $class) {
            $functions = array_merge($functions, $class::getMethods($onlyNames));
        }
        return $functions;
    }

    public static function getFunctionsCompletions()
    {
    	self::initCore();
        $classes = self::getFunctionsClasses();
        $completions = [];
        foreach ($classes as $class) {
            $completions[] = $class::getFunctionsCompletions();
        }
        return $completions;
    }

    public static function getMethodsCompletions()
    {
        self::initCore();
        $classes = self::getFunctionsClasses();
        $completions = [];
        foreach ($classes as $class) {
            $completions[] = $class::getMethodsCompletions();
        }
        return $completions;
    }

	public function start()
	{
        $this->calculation->run();
	}

	public function getResults()
	{
		$variables = [];
        $vars = $this->calculation->getVariables();
		foreach ($vars as $var => $value) {
			$value['var'] = $var;
			$variables[] = $value;
		}
        $answer = $this->calculation->getLastValue();
        if (!is_null($answer)) {
            $answer['var'] = 'answer';
            array_unshift($variables, $answer);
        }
		return $variables;
	}

    public function getResult()
    {
        return $this->calculation->getLastValue();
    }

    public function getErrors()
    {
        return $this->calculation->getErrorsAndWarnings();
    }

    public function getFormData()
    {
        return $this->calculation->getFormAttrs();
    }

    private static function initCore()
    {
        //подключение ядра
        $dirs = ['/Core', '/Functions'];
        foreach ($dirs as $key => $dir)
        {
            self::includeFolder(DZETA_CALC_PATH . $dir);
        }
        $modules = new \Phalcon\Config\Adapter\Json(BASE_PATH . '/config/Modules.json');
        $di = \Phalcon\DI::getDefault();
        foreach ($modules as $key => $value) {
            if (!in_array($key, ['backend', 'frontend'])) {
                include_once(APP_PATH . $value['path']);
                $module = new $value['className']();
                $module->registerAutoloaders($di);
                $path = str_replace('Module.php', '', $value['path']);
                self::includeFolder(APP_PATH . $path . 'library/DzetaCalc/Functions');
            }
        }
    }

    private static function includeFolder($dir)
    {
        if (is_dir($dir)) {
            $catalog = opendir($dir);
            while ($filename = readdir($catalog))
            {
                if (!in_array($filename, ['.', '..']))
                {
                   include_once("{$dir}/{$filename}");
                }
            }
            closedir($catalog);
        }
    }

    private static function getFunctionsClasses()
    {
        $classes = get_declared_classes();
        $functionClasses = [];
        foreach ($classes as $class) {
            if (preg_match("/^DzetaCalcParser\\\\Functions\\\\(?:[a-zA-Z0-9_\\\\])*Functions/", $class)) {
                if (!in_array($class, ['DzetaCalcParser\Functions\LogicOperations', 'DzetaCalcParser\Functions\MathOperations'])) {
                    $functionClasses[] = $class;
                }
            }
        }
        return $functionClasses;
    }
}