<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Core\BaseFunctions;

class SpecialFunctions extends BaseFunctions
{
	protected static $methods = [];

	protected static $functions = [
		'sendMail' => [
            'name' => 'sendMail',
            'completion' => [
                'label' => 'sendMail',
                'detail' => 'sendMail(string to, string subject, string text, string from)',
                'kind' => 'function',
                'insertText' => 'sendMail(${1:to}, ${2:subject}, ${3:text}, ${4:from})',
            ],
            'arguments' => [
            	[
            		'type' => Constants::TYPE_STRING,
            	],
            	[
            		'type' => Constants::TYPE_STRING,
            	],
            	[
            		'type' => Constants::TYPE_STRING,
            	],
            	[
            		'type' => Constants::TYPE_STRING,
            	],
            ]
		],
		'error' => [
			'name' => 'error',
			'completion' => [
				'label' => 'error',
				'detail' => 'error(string text)',
				'kind' => 'function',
				'insertText' => 'error(${1:text})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_STRING
				]
			]
		],
		'return' => [
			'name' => 'return',
			'completion' => [
				'label' => 'return',
				'detail' => 'return(any value)',
				'kind' => 'function',
				'insertText' => 'return(${1:value})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_ANY
				]
			]
		],
		'getCurrentUser'=>[
			'name' => 'getCurrentUser',
			'completion' => [
				'label' => 'getCurrentUser',
				'detail' => 'getCurrentUser()',
				'kind' => 'function',
				'insertText' => 'getCurrentUser()'
			],
			'arguments' => [
				
			]
		]
	];

	public static function getCurrentUser($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (empty($paramMethod))
		{
			return [
	        	'type' => Constants::TYPE_OBJECT,
	        	'value' => $dzetaCalc->getUser()->getUid(),
	        	'ps' => $position['ps'],
	        	'pf' => $position['pf'],
	        	'rows' => $position['rows'],
	        	'props' => [
	        		'objClass' =>\Dzeta\Core\User\User::USER_TYPE_UID,
	        		'name'=>$dzetaCalc->getUser()->getName()
	        	]
	        ];
		}
		
	}

	public static function sendMail($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$loader = new \Phalcon\Loader();
		        $loader->registerClasses([
		            'ElasticEmail' => APP_PATH . '/library/ElasticEmail.php'
				]);
		        $loader->register();
		        $to = $paramsFunc[0]['value'];
		        $subject = $paramsFunc[1]['value'];
		        $text = $paramsFunc[2]['value'];
		        $from = $paramsFunc[3]['value'];
		        $mail = \ElasticEmail::sendMail($subject, $to, $text, '', $from);
		        $answer = Constants::SPEC_VALUES['null'];
		        $answer['ps'] = $position['ps'];
		        $answer['pf'] = $position['pf'];
		        $answer['rows'] = $position['rows'];
		        if ($mail)
		        {
		        	$answer['type'] = Constants::TYPE_STRING;
		        	$answer['value'] = $mail;
		        } else
		        {
		        	$answer['type'] = Constants::TYPE_BOOL;
		        	$answer['value'] = false;
		        }
		        return $answer;
			}
		}
	}

	public static function error($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				Error::addError(
	                ['general'],
	                ['functions', 'user_error'],
	                [
	                    'text' => $paramsFunc[0]['value'],
	                ],
	                $dzetaCalc,
	                [
	                    'ps' => $position['ps'],
	                    'pf' => $position['pf'],
	                    'rows' => $position['rows'],
	                ]
	            );
			}
		}
	}

	public static function return($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$dzetaCalc->setGlobalMark(Constants::TYPE_ACTION_RETURN);
				$dzetaCalc->setLastValue($paramsFunc[0]);
				return $paramsFunc[0];
			}
		}
	}
}
