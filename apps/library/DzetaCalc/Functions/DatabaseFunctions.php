<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Core\Helper;
use DzetaCalcParser\Core\BaseFunctions;

use Dzeta\Models\Obj;
use Dzeta\Models\Type;
use Dzeta\Models\Attr;
use Dzeta\Models\View;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\App;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\Value\DateTimeV;

class DatabaseFunctions extends BaseFunctions
{
	protected static $methods = [
		'getUid' => [
			'name' => 'getUid',
			'completion' => [
				'label' => 'getUid',
				'detail' => 'getUid()',
				'kind' => 'method',
				'insertText' => 'getUid()'
			],
			'arguments' => [
				[
					'type' => [Constants::TYPE_OBJECT, Constants::TYPE_CLASS, Constants::TYPE_ATTRIBUTE],
				],
			]
		],
		'getName' => [
			'name' => 'getName',
			'completion' => [
				'label' => 'getName',
				'detail' => 'getName()',
				'kind' => 'method',
				'insertText' => 'getName()'
			],
			'arguments' => [
				[
					'type' => [Constants::TYPE_OBJECT, Constants::TYPE_CLASS, Constants::TYPE_ATTRIBUTE],
				],
			]
		],
		'getAttr' => [
			'name' => 'getAttr',
			'completion' => [
				'label' => 'getAttr',
				'detail' => 'getAttr(string name)',
				'kind' => 'method',
				'insertText' => 'getAttr(${1:name})'
			],
			'arguments' => [
				[
					'type' => [Constants::TYPE_OBJECT, Constants::TYPE_CLASS],
				],
				[
					'type' => Constants::TYPE_STRING,
				]
			]
		],
		'getValue' => [
			'name' => 'getValue',
			'completion' => [
				'label' => 'getValue',
				'detail' => 'getValue(string|attribute value)',
				'kind' => 'method',
				'insertText' => 'getValue(${1:attribute})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_OBJECT,
				],
				[
					'type' => [Constants::TYPE_STRING, Constants::TYPE_ATTRIBUTE],
				]
			]
		],
		'getChildren' => [
			'name' => 'getChildren',
			'completion' => [
				'label' => 'getChildren',
				'detail' => 'getChildren(class value, int limit = 0, int offset = 0)',
				'kind' => 'method',
				'insertText' => 'getChildren(${1:class})',
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_OBJECT
				],
				[
					'type' => Constants::TYPE_CLASS
				],
				[
					'type' => Constants::TYPE_NUMBER,
					'optional' => true,
					'default' => 0,
				],
				[
					'type' => Constants::TYPE_NUMBER,
					'optional' => true,
					'default' => 0,
				]
			]
		],
		'getChildrenAggr' => [
			'name' => 'getChildrenAggr',
			'completion' => [
				'label' => 'getChildrenAggr',
				'detail' => 'getChildrenAggr(string|class class, string|attribute attr, string aggr)',
				'kind' => 'method',
				'insertText' => 'getChildrenAggr(${1:class}, ${2:attr}, ${3:aggr})',
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_OBJECT
				],
				[
					'type' => [Constants::TYPE_STRING, Constants::TYPE_CLASS]
				],
				[
					'type' => [Constants::TYPE_STRING, Constants::TYPE_ATTRIBUTE]
				],
				[
					'type' => Constants::TYPE_STRING,
					'allowed_values' => ['max', 'min', 'sum']
				],
			]
		],
		'getParent' => [
			'name' => 'getParent',
			'completion' => [
				'label' => 'getParent',
				'detail' => 'getParent()',
				'kind' => 'method',
				'insertText' => 'getParent()',
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_OBJECT
				]
			]
		],
		'createObj' => [
			'name' => 'createObj',
			'completion' => [
				'label' => 'createObj',
				'detail' => 'createObj(string|class class, array params)',
				'kind' => 'method',
				'insertText' => 'createObj(${1:class}, ${2:params})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_OBJECT
				],
				[
					'type' => [Constants::TYPE_STRING, Constants::TYPE_CLASS]
				],
				[
					'type' => Constants::TYPE_ARRAY
				]
			]
		],
		'updateObj' => [
			'name' => 'updateObj',
			'completion' => [
				'label' => 'updateObj',
				'detail' => 'updateObj(array params)',
				'kind' => 'method',
				'insertText' => 'updateObj(${1:params})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_OBJECT
				],
				[
					'type' => Constants::TYPE_ARRAY
				]
			]
		],
		'setFormAttr' => [
			'name' => 'setFormAttr',
			'completion' => [
				'label' => 'setFormAttr',
				'detail' => 'setFormAttr(string|attribute attr, any value)',
				'kind' => 'method',
				'insertText' => 'setFormAttr(${1:attr}, ${2:value})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_OBJECT
				],
				[
					'type' => [Constants::TYPE_STRING, Constants::TYPE_ATTRIBUTE]
				],
				[
					'type' => Constants::TYPE_ANY
				]
			]
		],
		'parseDB' => [
			'name' => 'parseDb',
			'completion' => [
				'label' => 'parseDB',
				'detail' => 'parseDB()',
				'kind' => 'method',
				'insertText' => 'parseDB()'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_STRING
				]
			]
		]
	];

	protected static $functions = [
		'getClass' => [
			'name' => 'getClass',
			'completion' => [
				'label' => 'getClass',
				'detail' => 'getClass(string name)',
				'kind' => 'function',
				'insertText' => 'getClass(${1:name})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_STRING,
				]
			]
		],
		'searchObj' => [
			'name' => 'searchObj',
			'completion' => [
				'label' => 'searchObj',
				'detail' => 'searchObj(string|class class, array filters, object delimiter = root)',
				'kind' => 'function',
				'insertText' => 'searchObj(${1:class}, ${2:filters})',
			],
			'arguments' => [
				[
					'type' => [Constants::TYPE_STRING, Constants::TYPE_CLASS]
				],
				[
					'type' => Constants::TYPE_ARRAY
				],
				[
					'type' => Constants::TYPE_OBJECT,
					'optional' => true,
					'default' => Instance::INSTANCE_ROOT,
				]
			]
		],
		'getViewData' => [
			'name' => 'getViewData',
			'completion' => [
				'label' => 'getViewData',
				'detail' => 'getViewData(string viewUid, string sortAttrUid, number num = 1000)',
				'kind' => 'function',
				'insertText' => 'getViewData(${1:viewUid}, ${2:sortAttrUid})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_STRING
				],
				[
					'type' => [Constants::TYPE_STRING,Constants::TYPE_NULL]
				],
				[
					'type' => [Constants::TYPE_NUMBER,Constants::TYPE_NULL],
					'optional' => true,
					'default' => 1000,
				],
				[
					'type' => Constants::TYPE_STRING,
					'optional' => true,
					'default' => '',
				]
			]
		],
		'createObjUid'=>
		[
			'name' => 'createObjUid',
			'completion' => [
				'label' => 'createObjUid',
				'detail' => 'createObjUid(string objUid)',
				'kind' => 'function',
				'insertText' => 'createObjUid(${1:objUid})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_STRING
				]
			]
		]
	];
	public static function createObjUid($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$_obj = new Obj();
				$obj = new Instance($paramsFunc[0]['value'], '');

				$_obj->GetClassForObject($obj);
				$_obj->ObjectGetNames([$obj]);
				if(is_null($obj->getType()))
				{
					return Helper::makeNullVariable([
						'ps' => $position['ps'],
						'pf' => $position['pf'],
						'rows' => $position['rows']
					]);	
				}else
				{
					return [
						'type' => Constants::TYPE_OBJECT,
	     				'value' => $paramsFunc[0]['value'],
	     				'ps' => $position['ps'],
	     				'pf' => $position['pf'],
	     				'rows' => $position['rows'],
	     				'props' => [
	     					'objClass' => $obj->getType()->getUid(),
	     					'name' =>$obj->getName()
	     				]
					];
				}
				
			}
		}
	}
	public static function getUid($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				return [
					'type' => Constants::TYPE_STRING,
					'value' => $paramMethod['value'],
					'ps' => $position['ps'],
					'pf' => $position['pf'],
					'rows' => $position['rows']
				];
			}
		}
	}

	public static function getName($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check && isset($paramMethod['props']['name']))
			{
				return [
					'type' => Constants::TYPE_STRING,
					'value' => $paramMethod['props']['name'],
					'ps' => $position['ps'],
					'pf' => $position['pf'],
					'rows' => $position['rows']
				];
			}
		}
	}

	public static function getAttr($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				$_obj = new Obj();
	            $_attr = new Attr();
	            $attr = new Attribute('', $paramsFunc[0]['value'], 1, false, false);
	            $classUid = null;
	            if ($paramMethod['type'] == Constants::TYPE_OBJECT)
	            {
	            	$classUid = $paramMethod['props']['objClass'];
	            } else if ($paramMethod['type'] == Constants::TYPE_CLASS)
	            {
	            	$classUid = $paramMethod['value'];
	            }
	            $attr->setParent(new Core\Type\Type($classUid, ''));
	            $result = $_attr->GetIDByName([$attr]);
	            if ($result['result'] != 1)
	            {
	            	Error::addError(
		                ['database'],
		                ['not_found', 'attribute'],
		                [
		                    'name' => $paramsFunc[0]['value'],
		                    'class' => '',
		                ],
		                $dzetaCalc,
		                [
		                    'ps' => $position['ps'],
		                    'pf' => $position['pf'],
		                    'rows' => $position['rows'],
		                ]
		            );
	            } else
	            {
	            	return [
	            		'type' => Constants::TYPE_ATTRIBUTE,
	            		'value' => $attr->getUid(),
	            		'ps' => $position['ps'],
	            		'pf' => $position['pf'],
	            		'rows' => $position['rows'],
	            		'props' => [
	            			'attrClass' => $classUid,
	            			'name' => $paramsFunc[0]['value'],
	            		]
	            	];
	            }
			}
		}
	}

	public static function getValue($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				$check = true;
				$attrName = null;
				if ($paramsFunc[0]['type'] == Constants::TYPE_STRING)
				{
		            $_attr = new Attr();
					$attr = new Attribute('', $paramsFunc[0]['value'], 1, false, false);
					$attr->setParent(new Core\Type\Type($paramMethod['props']['objClass'], ''));
					$result = $_attr->GetIDByName([$attr]);
		            if ($result['result'] != 1)
		            {
		            	Error::addError(
			                ['database'],
			                ['not_found', 'attribute'],
			                [
			                    'name' => $paramsFunc[0]['value'],
			                    'class' => '',
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
			            $check = false;
		            }
				} else if ($paramsFunc[0]['type'] == Constants::TYPE_ATTRIBUTE)
				{
					$attr = new Attribute($paramsFunc[0]['value'], $paramsFunc[0]['props']['name'], 1, false, false);
				}
				if ($check)
				{
					$obj = new \Dzeta\Core\Instance($paramMethod['value'], '');
		            $obj->setAttributes([$attr]);
					$cache = $dzetaCalc->getAttrCache($paramMethod['value'], $attr->getUid());
		            if ($cache['found'])
		            {
		            	$attr->setValue(['value' => $cache['value']]);
		            } else
		            {
		            	$_obj = new Obj();
			            $_obj->getAllValues([$obj]);
		            }
		            return Helper::convertAttributeValue(
		            	$obj->getAttribute($attr->getUid()),
	            		$position,
	            		[
	            			'obj' => $obj->getUid(),
	            			'class' => $paramMethod['props']['objClass'],
	            			'attrName' => $attr->getName()
	            		]
	            	);
				}
			}
		}
	}

	public static function getChildren($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				$method = self::getMethodByName(__FUNCTION__);
				$_obj = new Obj();
	         	$obj = new Instance($paramMethod['value'], '');
	         	$check = true;
	         	if ($paramsFunc[0]['type'] == Constants::TYPE_CLASS)
	         	{
		         	$class = new Core\Type\Type($paramsFunc[0]['value'], '');
	         	} else if ($paramsFunc[0]['type'] == Constants::TYPE_STRING)
	         	{
	         		$_class = new Type();
					$class = new Core\Type\Type('', $paramsFunc[0]['value']);
					$result = $_class->GetIDByName($class);
					if ($result != 1)
					{
		            	Error::addError(
			                ['database'],
			                ['not_found', 'class'],
			                [
			                    'name' => $paramsFunc[0]['value'],
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
			            $check = false;
					}
	         	}
	         	if ($check)
	         	{
		         	$limit = isset($paramsFunc[1]) ? $paramsFunc[1]['value'] : $method['arguments'][2]['default'];
		         	$offset = isset($paramsFunc[2]) ? $paramsFunc[2]['value'] : $method['arguments'][3]['default'];
		         	$children = $_obj->getChildrenByType($obj, $class);
		         	$answer = [
		         		'type' => Constants::TYPE_ARRAY,
		         		'value' => [],
		         		'ps' => $position['ps'],
		         		'pf' => $position['pf'],
		         		'rows'=> $position['rows'],
		         	];
		         	//TODO: limit and offset through database
		         	$count = 0;
		         	foreach ($children as $key => $child)
		         	{
	         			$push = true;
		         		if ($limit > 0)
		         		{
		         			if ($count >= $limit)
		         			{
		         				$push = false;
		         			} else
		         			{
		         				$count++;
		         			}
		         		}
		         		if ($offset > 0)
		         		{
		         			if ($offset > $key)
		         			{
		         				$push = false;
		         				$count = 0;
		         			}
		         		}
		         		if ($push)
		         		{
		         			$answer['value'][] = [
		         				'type' => Constants::TYPE_OBJECT,
		         				'value' => $child->getUid(),
		         				'ps' => $position['ps'],
		         				'pf' => $position['pf'],
		         				'rows' => $position['rows'],
		         				'props' => [
		         					'objClass' => $paramsFunc[0]['value'],
		         					'name' => $child->getName(),
		         				]
		         			];
		         		}
		         	}

		         	return $answer;
	         	}
			}
		}
	}

	public static function getChildrenAggr($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				$method = self::getMethodByName(__FUNCTION__);
				$_obj = new Obj();
	         	$obj = new Instance($paramMethod['value'], '');
	         	$check = true;
	         	if ($paramsFunc[0]['type'] == Constants::TYPE_CLASS)
	         	{
		         	$class = new Core\Type\Type($paramsFunc[0]['value'], $paramsFunc[0]['props']['name']);
	         	} else if ($paramsFunc[0]['type'] == Constants::TYPE_STRING)
	         	{
	         		$_class = new Type();
					$class = new Core\Type\Type('', $paramsFunc[0]['value']);
					$result = $_class->GetIDByName($class);
					if ($result != 1)
					{
		            	Error::addError(
			                ['database'],
			                ['not_found', 'class'],
			                [
			                    'name' => $paramsFunc[0]['value'],
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
			            $check = false;
					}
	         	}
	         	if ($check)
	         	{
	         		if ($paramsFunc[1]['type'] == Constants::TYPE_STRING)
					{
			            $_attr = new Attr();
						$attr = new Attribute('', $paramsFunc[1]['value'], 1, false, false);
						$attr->setParent($class);
						$result = $_attr->GetIDByName([$attr]);
			            if ($result['result'] != 1)
			            {
			            	Error::addError(
				                ['database'],
				                ['not_found', 'attribute'],
				                [
				                    'name' => $paramsFunc[1]['value'],
				                    'class' => $class->getName(),
				                ],
				                $dzetaCalc,
				                [
				                    'ps' => $position['ps'],
				                    'pf' => $position['pf'],
				                    'rows' => $position['rows'],
				                ]
				            );
				            $check = false;
			            }
					} else if ($paramsFunc[1]['type'] == Constants::TYPE_ATTRIBUTE)
					{
						$attr = new Attribute($paramsFunc[1]['value'], $paramsFunc[1]['props']['name'], 1, false, false);
					}
	         	}
	         	if ($check)
	         	{
		         	$_obj = new Obj();
		            $obj = new Instance($paramMethod['value'], '');
	         		$aggr = $paramsFunc[2]['value'];
         			$aggr = strtoupper($aggr);
		            $result = $_obj->getChildrenAggr($obj, $class, $attr, $aggr);
		            if (!is_null($result))
		            {
		            	return [
		            		'type' => Constants::TYPE_NUMBER,
		            		'value' => $result,
		            		'ps' => $position['ps'],
		            		'pf' => $position['pf'],
		            		'rows' => $position['rows']
		            	];
		            }
	         	}
			}
		}
	}

	public static function getParent($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				$_obj = new Obj();
		        $obj = new \Dzeta\Core\Instance($paramMethod['value'], '');
		        $_obj->getParent($obj);
		        return [
		        	'type' => Constants::TYPE_OBJECT,
		        	'value' => $obj->getParent()->getUid(),
		        	'ps' => $position['ps'],
		        	'pf' => $position['pf'],
		        	'rows' => $position['rows'],
		        	'props' => [
		        		'objClass' => $obj->getParent()->getType()->getUid(),
		        		'name' => $obj->getParent()->getName(),
		        	]
		        ];
			}
		}
	}

	public static function createObj($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
	         	if ($paramsFunc[0]['type'] == Constants::TYPE_CLASS)
	         	{
		         	$class = new Core\Type\Type($paramsFunc[0]['value'], $paramsFunc[0]['props']['name']);
	         	} else if ($paramsFunc[0]['type'] == Constants::TYPE_STRING)
	         	{
	         		$_class = new Type();
					$class = new Core\Type\Type('', $paramsFunc[0]['value']);
					$result = $_class->GetIDByName($class);
					if ($result != 1)
					{
		            	Error::addError(
			                ['database'],
			                ['not_found', 'class'],
			                [
			                    'name' => $paramsFunc[0]['value'],
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
			            $check = false;
					}
	         	}

	         	if ($check)
	         	{
	         		$attrs = [];
					$_attr = new Attr();
	         		foreach ($paramsFunc[1]['value'] as $value)
	         		{
	         			if ($value['type'] == Constants::TYPE_ARRAY)
	         			{
	         				if (isset($value['value']['param']) && isset($value['value']['value']))
	         				{
	         					if ($value['value']['param']['type'] == Constants::TYPE_STRING)
	         					{
									$attr = new Attribute('', $value['value']['param']['value'], 1, false, false);
									$attr->setParent($class);
									$result = $_attr->GetIDByName([$attr]);
						            if ($result['result'] != 1)
						            {
						            	Error::addError(
							                ['database'],
							                ['not_found', 'attribute'],
							                [
							                    'name' => $value['value']['param']['value'],
							                    'class' => $class->getName(),
							                ],
							                $dzetaCalc,
							                [
							                    'ps' => $position['ps'],
							                    'pf' => $position['pf'],
							                    'rows' => $position['rows'],
							                ]
							            );
							            $check = false;
						            }
	         					} else if ($value['value']['param']['type'] == Constants::TYPE_ATTRIBUTE)
	         					{
	         						$attr = new Attribute($value['value']['param']['value'], $value['value']['param']['props']['name'], 1, false, false);
	         					} else
	         					{
	         						$check = false;
	         					}

	         					if ($check)
	         					{
	         						$attrs[] = $attr;
	         					}
	         				} else
	         				{
	         					$check = false;
	         				}
	         			} else
	         			{
	         				$check = false;
	         			}
	         			if (!$check)
	         			{
	         				break;
	         			}
	         		}
	         		if (!$check)
					{
						$function = self::getMethodByName(__FUNCTION__);
						Error::addError(
			                ['general'],
			                ['functions', 'params_array_elems_format'],
			                [
			                    'index' => 2,
			                    'function' => $function['calcName'],
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
					} else
					{
			            $_attr->getAttributesByList($attrs);
						foreach ($paramsFunc[1]['value'] as $key => $value)
		         		{
		         			$attrs[$key]->setValue(['value' => $value['value']['value']['value']]);
		         		}
		         		$_obj = new Obj();
				        $obj = new Instance($paramMethod['value'], '');
				        $_obj->getParent($obj);
				        $child = new Instance('', '');
			            $child->setType($class)->setParent($obj);
			            $child->setAttributes($attrs);
			            $results = $_obj->checkUniqueAndAddWithValues($child, Instance::ADDED_BY_CALC, $dzetaCalc->getUser());
			            if (!empty($results['errors']))
			            {
							$function = self::getMethodByName(__FUNCTION__);
							$errors = array_column($results['errors'], 'text');
			            	Error::addError(
				                ['database'],
				                ['functions', 'errors'],
				                [
				                    'function' => $function['calcName'],
				                    'errors' => implode('. ', $errors),
				                ],
				                $dzetaCalc,
				                [
				                    'ps' => $position['ps'],
				                    'pf' => $position['pf'],
				                    'rows' => $position['rows'],
				                ]
				            );
			            } else
			            {
			            	return [
				            	'type' => Constants::TYPE_OBJECT,
				            	'value' => $child->getUid(),
				            	'ps' => $position['ps'],
				            	'pf' => $position['pf'],
				            	'rows' => $position['rows'],
				            	'props' => [
				            		'objClass' => $class->getUid(),
				            		'name' => $child->getName(),
				            	]
				            ];
			            }
					}
	         	}
			}
		}
	}

	public static function updateObj($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				$class = new Core\Type\Type($paramMethod['props']['objClass'], '');
         		$attrs = [];
				$_attr = new Attr();
         		foreach ($paramsFunc[0]['value'] as $value)
         		{
         			if ($value['type'] == Constants::TYPE_ARRAY)
         			{
         				if (isset($value['value']['param']) && isset($value['value']['value']))
         				{
         					if ($value['value']['param']['type'] == Constants::TYPE_STRING)
         					{
								$attr = new Attribute('', $value['value']['param']['value'], 1, false, false);
								$attr->setParent($class);
								$result = $_attr->GetIDByName([$attr]);
					            if ($result['result'] != 1)
					            {
					            	Error::addError(
						                ['database'],
						                ['not_found', 'attribute'],
						                [
						                    'name' => $value['value']['param']['value'],
						                    'class' => $class->getName(),
						                ],
						                $dzetaCalc,
						                [
						                    'ps' => $position['ps'],
						                    'pf' => $position['pf'],
						                    'rows' => $position['rows'],
						                ]
						            );
						            $check = false;
					            }
         					} else if ($value['value']['param']['type'] == Constants::TYPE_ATTRIBUTE)
         					{
         						$attr = new Attribute($value['value']['param']['value'], $value['value']['param']['props']['name'], 1, false, false);
         					} else
         					{
         						$check = false;
         					}

         					if ($check)
         					{
         						$attrs[] = $attr;
         					}
         				} else
         				{
         					$check = false;
         				}
         			} else
         			{
         				$check = false;
         			}
         			if (!$check)
         			{
         				break;
         			}
         		}
         		if (!$check)
				{
					$function = self::getMethodByName(__FUNCTION__);
					Error::addError(
		                ['general'],
		                ['functions', 'params_array_elems_format'],
		                [
		                    'index' => 2,
		                    'function' => $function['calcName'],
		                ],
		                $dzetaCalc,
		                [
		                    'ps' => $position['ps'],
		                    'pf' => $position['pf'],
		                    'rows' => $position['rows'],
		                ]
		            );
				} else
				{
		            $_attr->getAttributesByList($attrs);
					foreach ($paramsFunc[0]['value'] as $key => $value)
	         		{
	         			$attrs[$key]->setValue(['value' => $value['value']['value']['value']]);
	         		}
	         		$_obj = new Obj();
			        $obj = new Instance($paramMethod['value'], '');
			        $_obj->getParent($obj);
		            $obj->setType($class);
		            $obj->setAttributes($attrs);
		            $results = $_obj->checkUniqueAndUpdateValues($obj, $dzetaCalc->getUser());
		            if (!empty($results['errors']))
		            {
						$function = self::getMethodByName(__FUNCTION__);
						$errors = array_column($results['errors'], 'text');
		            	Error::addError(
			                ['database'],
			                ['functions', 'errors'],
			                [
			                    'function' => $function['calcName'],
			                    'errors' => implode('. ', $errors),
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
		            } else
		            {
		            	return [
			            	'type' => Constants::TYPE_OBJECT,
			            	'value' => $obj->getUid(),
			            	'ps' => $position['ps'],
			            	'pf' => $position['pf'],
			            	'rows' => $position['rows'],
			            	'props' => [
			            		'objClass' => $class->getUid(),
			            		'name' => $obj->getName(),
			            	]
			            ];
		            }
				}
			}
		}
	}

	public static function setFormAttr($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				if ($paramsFunc[0]['type'] == Constants::TYPE_STRING)
				{
		            $_attr = new Attr();
					$attr = new Attribute('', $paramsFunc[0]['value'], 1, false, false);
					$attr->setParent(new Core\Type\Type($paramMethod['props']['objClass'], ''));
					$result = $_attr->GetIDByName([$attr]);
		            if ($result['result'] != 1)
		            {
		            	Error::addError(
			                ['database'],
			                ['not_found', 'attribute'],
			                [
			                    'name' => $paramsFunc[0]['value'],
			                    'class' => '',
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
			            $check = false;
		            }
				} else if ($paramsFunc[0]['type'] == Constants::TYPE_ATTRIBUTE)
				{
					$attr = new Attribute($paramsFunc[0]['value'], $paramsFunc[0]['props']['name'], 1, false, false);
				}

				if ($check)
				{
					$dzetaCalc->setFormAttr($paramMethod['value'], $attr->getUid(), $paramsFunc[1]);

					$true = Constants::SPEC_VALUES['true'];
					$true['ps'] = $position['ps'];
					$true['pf'] = $position['pf'];
					$true['rows'] = $position['rows'];
					return $true;
				}
			}
		}
	}

	public static function parseDb($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$arguments = [
				$paramMethod,
				...$paramsFunc
			];
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
			if ($check)
			{
				$str = $paramMethod['value'];
		        $attrCur = [];
		        $attrTree = [];
		        for ($i = 0, $len = mb_strlen($str); $i < $len; $i++)
		        {
		            $chr = mb_substr($str, $i, 1);
		            if ($chr == '{')
		            {
		            	$flagB1 = $i;
		            } elseif ($chr == '}')
		            {
		              	$lennnStr = mb_substr($str, $flagB1 + 1, $i - $flagB1 - 1);
		              	for ($j = 0; $j < mb_strlen($lennnStr); $j++)
		              	{
		                  	$chr = mb_substr($lennnStr, $j, 1);
		                  	if ($chr == '.')
		                  	{
		                      	$typeObj = mb_substr($lennnStr,0,$j);
		                      	$typeName = '';
		                      	$objName = '';
		                      	for ($j1 = 0; $j1 < mb_strlen($typeObj); $j1++)
		                      	{
		                        	$chr = mb_substr($typeObj, $j1, 1);
		                        	if ($chr == '(')
		                        	{
		                          		$typeName = mb_substr($typeObj, 0, $j1);
		                          		$objName = mb_substr($typeObj, $j1 + 1, mb_strlen($typeObj) - $j1 - 2);
		                        	}
		                      	}
		                      	$attr = mb_substr($lennnStr, $j + 1);
		                      	$attrTree[] = [
		                      		'type' => $typeName,
		                      		'objname' => $objName,
		                      		'attr' => $attr
		                      	];
		                  	}
		              	}
		            } elseif ($chr == '[')
		            {
		               $flagB2=$i;
		            } elseif ($chr == ']')
		            {
		              	$attrCur[] = ['attr' => mb_substr($str, $flagB2 + 1, $i - $flagB2 - 1)];
		            }
		        }

		        $replArr = [];
		        $_obj = new Obj();
		        $_attr = new Attr();
	            $_class = new Type();

		        foreach ($attrTree as $key => $value)
		        {
		            $class = new \Dzeta\Core\Type\Type('', $value['type']);
		            $res = $_class->GetIDByName($class);
		            if ($res == 1)
		            {
		                $attr = new \Dzeta\Core\Type\Attribute('', $value['attr'], 1, false, false);
		                $attr->setParent($class);
		                $res = $_attr->GetIDByName([$attr]);
		                $objs = $_obj->getObjsByClass($class, $value['objname']);
		                if ($objs['result'] == 1 && !empty($objs['objs']))
		                {
		                    $obj = $objs['objs'][0];
		                    $obj->setAttributes([$attr]);
		                    $_obj->getAllValues([$obj]);
		                    $attr = $obj->getAttribute($attr->getUid());
		                    if (!$attr->isArray())
		                    {
		                    	$attrValue = $attr->getValue();
		                    	$replace = is_null($attrValue) ? '' : ($attr->getDatatype() == \Dzeta\Core\Type\DataType::TYPE_OBJECT ? $attrValue->getUid() : $attrValue->getValue());
		                        $replArr[] = [
		                        	'search' => '{' . $value['type'] . '(' . $value['objname'] . ')' . '.' . $value['attr'] . '}',
		                        	'replace' => $replace
		                        ];
		                    }
		                }
		            }
		        }

		        $thisObj = $dzetaCalc->getVariable('$this');
		        if (!is_null($thisObj))
		        {
		        	$class = new \Dzeta\Core\Type\Type($thisObj['props']['objClass'], '');
		        	$obj = new \Dzeta\Core\Instance($thisObj['value'], '', $class);
		        	$attrs = [];

			        foreach ($attrCur as $key => $value)
			        {
			           	$attr = new \Dzeta\Core\Type\Attribute('', $value['attr'], 1, false, false);
			           	$attr->setParent($class);
			           	$res = $_attr->GetIDByName([$attr]);

			           	if ($res['result'] == 1)
			           	{
			              	$attrs[] = $attr;
			              	$attrCur[$key]['uid'] = $attr->getUid();
			           	}
			        }

			        $obj->setAttributes($attrs);
			        $_obj->getAllValues([$obj]);
			        foreach ($attrCur as $a)
			        {
			        	if (isset($a['uid']))
			        	{
			        		$attr = $obj->getAttribute($a['uid']);
		                    if (!$attr->isArray())
		                    {
		                    	$attrValue = $attr->getValue();
		                    	$replace = is_null($attrValue) ? '' : ($attr->getDatatype() == \Dzeta\Core\Type\DataType::TYPE_OBJECT ? $attrValue->getUid() : $attrValue->getValue());
		                        $replArr[] = [
		                        	'search' => '[' . $a['attr'] . ']',
		                        	'replace' => $replace
		                        ];
		                    }
			        	}
			        }
		        }

		        foreach ($replArr as $key => $value) {
		           $str = str_replace($value['search'], $value['replace'], $str);
		        }
		        return [
		        	'type' => Constants::TYPE_STRING,
		        	'value' => $str,
		        	'ps' => $position['ps'],
		        	'pf' => $position['pf'],
		        	'rows' => $position['rows'],
		        ];
			}
		}
	}

	public static function getClass($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
	            $_class = new Type();
				$class = new Core\Type\Type('', $paramsFunc[0]['value']);
				$result = $_class->GetIDByName($class);
				if ($result != 1)
				{
	            	Error::addError(
		                ['database'],
		                ['not_found', 'class'],
		                [
		                    'name' => $paramsFunc[0]['value'],
		                ],
		                $dzetaCalc,
		                [
		                    'ps' => $position['ps'],
		                    'pf' => $position['pf'],
		                    'rows' => $position['rows'],
		                ]
		            );
				} else
				{
					return [
	            		'type' => Constants::TYPE_CLASS,
	            		'value' => $class->getUid(),
	            		'ps' => $position['ps'],
	            		'pf' => $position['pf'],
	            		'rows' => $position['rows'],
	            		'props' => [
	            			'name' => $paramsFunc[0]['value'],
	            		]
	            	];
				}
			}
		}
	}

	public static function searchObj($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				if ($paramsFunc[0]['type'] == Constants::TYPE_STRING)
				{
					$_class = new Type();
					$class = new Core\Type\Type('', $paramsFunc[0]['value']);
					$result = $_class->GetIDByName($class);
					if ($result != 1)
					{
		            	Error::addError(
			                ['database'],
			                ['not_found', 'class'],
			                [
			                    'name' => $paramsFunc[0]['value'],
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
			            $check = false;
					}
				} else if ($paramsFunc[0]['type'] == Constants::TYPE_CLASS)
				{
					$class = new Core\Type\Type($paramsFunc[0]['value'], $paramsFunc[0]['props']['name']);
				}
				if ($check)
				{
					$filters = [];
					$_attr = new Attr();
					foreach ($paramsFunc[1]['value'] as $filter)
					{
						if ($filter['type'] == Constants::TYPE_ARRAY)
						{
							if (isset($filter['value']['name']) && isset($filter['value']['cond']) && isset($filter['value']['value']))
							{
								if ($filter['value']['name']['type'] == Constants::TYPE_STRING)
								{
									$attr = new Attribute('', $filter['value']['name']['value'], 1, false, false);
									$attr->setParent($class);
									$result = $_attr->GetIDByName([$attr]);
						            if ($result['result'] != 1)
						            {
						            	Error::addError(
							                ['database'],
							                ['not_found', 'attribute'],
							                [
							                    'name' => $filter['value']['name']['value'],
							                    'class' => $class->getName(),
							                ],
							                $dzetaCalc,
							                [
							                    'ps' => $position['ps'],
							                    'pf' => $position['pf'],
							                    'rows' => $position['rows'],
							                ]
							            );
							            $check = false;
						            }
								} else if ($filter['value']['name']['type'] == Constants::TYPE_ATTRIBUTE)
								{
									$attr = new Attribute($filter['value']['name']['value'], $filter['value']['name']['props']['name'], 1, false, false);
								} else
								{
									$check = false;
								}

								if ($check)
								{
									if ($filter['value']['value']['type'] == Constants::TYPE_DATETIME)
									{
										$value = $filter['value']['value']['value']->format(DateTimeV::DATE_FORMAT_SEARCH);
									} else
									{
										$value = $filter['value']['value']['value'];
									}
								}

								if ($check)
								{
									if ($filter['value']['cond']['type'] == Constants::TYPE_STRING)
									{
										$condition = $filter['value']['cond']['value'];
									} else
									{
										$check = false;
									}
								}

								if ($check)
								{
									$filters[] = '[' . $attr->getUid() . ']' . $condition . '#' . $value . '#';
								}
							} else
							{
								$check = false;
							}
						} else
						{
							$check = false;
						}
						if (!$check)
						{
							break;
						}
					}
					if (!$check)
					{
						$function = self::getFunctionByName(__FUNCTION__);
						Error::addError(
			                ['general'],
			                ['functions', 'params_array_elems_format'],
			                [
			                    'index' => 2,
			                    'function' => $function['calcName'],
			                ],
			                $dzetaCalc,
			                [
			                    'ps' => $position['ps'],
			                    'pf' => $position['pf'],
			                    'rows' => $position['rows'],
			                ]
			            );
					}
				}
				if ($check)
				{
					$delimiters = [];
					if (!isset($paramsFunc[2]))
					{
						$delimiters[] = new Instance(Instance::INSTANCE_ROOT, '');
					} else
					{
						$delimiters[] = new Instance($paramsFunc[2]['value'], '');
					}

					$_obj = new Obj();
			        $children = $_obj->searchObj($class, implode(' AND ', $filters), $delimiters, 1);
			        $answer = [
		         		'type' => Constants::TYPE_ARRAY,
		         		'value' => [],
		         		'ps' => $position['ps'],
		         		'pf' => $position['pf'],
		         		'rows'=> $position['rows'],
		         	];
		         	foreach ($children['data'] as $key => $child)
		         	{
	         			$answer['value'][] = [
	         				'type' => Constants::TYPE_OBJECT,
	         				'value' => $child->getUid(),
	         				'ps' => $position['ps'],
	         				'pf' => $position['pf'],
	         				'rows' => $position['rows'],
	         				'props' => [
	         					'objClass' => $class->getUid(),
	         					'name' => $child->getName(),
	         				]
	         			];
		         	}

		         	return $answer;
				}
			}
		}
	}

	public static function getViewData($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$viewUid = $paramsFunc[0]['value'];
		        $sortAttrUid = $paramsFunc[1]['value'];
		        $function = self::getFunctionByName(__FUNCTION__);
		        $num = $function['arguments'][2]['default'];
		        if (isset($paramsFunc[2]))
		        {
		          	$num = $paramsFunc[2]['value'];
		        }

		        $_view = new View();
		        $view = new App\View($viewUid, '');
		        $typeSort = 0;
		        $start = 1;
		        if(isset($paramsFunc[3]))
		        {		        	
		        	//тут у нас приходит дополнительный JSON
		        	$dataJson=$paramsFunc[3]['value'];
		        	$dataJson=json_decode($dataJson,true);
		        	foreach ($dataJson as $key => $value) 
		        	{
		        		$view->setDopSett($key, $value);
		        	}
		        	if($num!=null)
		        	{
		        		$view->setDopSett('num', $num - 1);
		        	}	
		        	$view->setDopSett('start', $start);
		        }else
		        {
		        	$view->setDopSett('attrSort', new Attribute($sortAttrUid, '', 1, false, false));
			        $view->setDopSett('sort', $typeSort);
			        if($num!=null)
		        	{
		        		$view->setDopSett('num', $num - 1);
		        	}
			        $view->setDopSett('start', $start);
		        }
		        

		       	$data = $_view->getDataTable($view, $dzetaCalc->getUser()->getUid());
		       	$answer = [
		       		'type' => Constants::TYPE_ARRAY,
		       		'value' => [],
		       		'ps' => $position['ps'],
		       		'pf' => $position['pf'],
		       		'rows' => $position['rows'],
		       	];
		       	if (isset($data['data']))
		       	{
		       		$null = Constants::SPEC_VALUES['null'];
		       		$null['ps'] = $position['ps'];
		       		$null['pf'] = $position['pf'];
		       		$null['rows'] = $position['rows'];
		          	foreach ($data['data'] as $key => $value)
		          	{
		              	$row = [];
		              	foreach ($value as $key => $val)
		              	{
		                	if (isset($val['v']))
		                	{
		                 		$row[] = [
		                 			'type' => Constants::TYPE_STRING,
		                 			'value' => $val['v'],
		                 			'ps' => $position['ps'],
		                 			'pf' => $position['pf'],
		                 			'rows' => $position['rows'],
		                 		];
		                	} else
		                	{
		                  		$row[] = $null;
		                	}
		              	}
		              	$answer['value'][] = [
		              		'type' => Constants::TYPE_ARRAY,
		              		'value' => $row,
		              		'ps' => $position['ps'],
		              		'pf' => $position['pf'],
		              		'rows' => $position['rows'],
		              	];
		          	}
		       	}
		       	return $answer;
			}
		}
	}
}
