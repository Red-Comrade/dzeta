<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;
use DzetaCalcParser\Core\Error;

class MathOperations
{

	public static function Increment($operand1, $dzetaCalc)
	{
		$rows = isset($operand1['rows']) ? $operand1['rows'] : [];
		if ($operand1['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_NUMBER,
				'value' => $operand1['value'] + 1,
				'ps' => $operand1['ps'],
				'pf' => $operand1['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['mathOper'],
			['Increment', 'mO_incr_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand1['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand1['pf'],
			'rows' => $rows,
		]);
	}

	public static function Decrement($operand1, $dzetaCalc)
	{
		$rows = isset($operand1['rows']) ? $operand1['rows'] : [];
		if ($operand1['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_NUMBER,
				'value' => $operand1['value']-1,
				'ps' => $operand1['ps'],
				'pf' => $operand1['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['mathOper'],
			['Decrement', 'mO_decr_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand1['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand1['pf'],
			'rows' => $rows,
		]);
	}

	public static function Additional($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_NUMBER,
				'value' => $operand1['value'] + $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		} else if (($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_STRING) || ($operand1['type'] == Constants::TYPE_STRING && $operand2['type'] == Constants::TYPE_NUMBER) || ($operand1['type'] == Constants::TYPE_STRING && $operand2['type'] == Constants::TYPE_STRING))
		{
			return [
				'type' => Constants::TYPE_STRING,
				'value' => $operand1['value'] . $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['mathOper'],
			['Additional','mO_add_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function Subtraction($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_NUMBER,
				'value' => $operand1['value'] - $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['mathOper'],
			['Subtraction','mO_sub_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function Multiplication($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_NUMBER,
				'value' => $operand1['value'] * $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['mathOper'],
			['Multiplication','mO_mult_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function Division($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		//не забываем про деление на 0
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			if ($operand2['value'] == 0)
			{
				Error::addError(
					['mathOper'],
					['Division','mO_div_0'],
					[],
					$dzetaCalc,
					[
						'ps' => $operand1['ps'],
						'pf' => $operand2['pf'],
						'rows' => $rows,
					]
				);
				return Helper::makeNullVariable([
					'ps' => $operand1['ps'],
					'pf' => $operand2['pf'],
					'rows' => $rows,
				]);
			}
			return [
				'type' => Constants::TYPE_NUMBER,
				'value' => $operand1['value'] / $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['mathOper'],
			['Division','mO_div_1'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function Pow($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_NUMBER,
				'value' => pow($operand1['value'], $operand2['value']),
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['mathOper'],
			['Pow', 'mO_pow_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

}