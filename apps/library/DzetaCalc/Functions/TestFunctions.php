<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\BaseFunctions;

class TestFunctions extends BaseFunctions
{
	protected static $methods = [];

	protected static $functions = [
      	'testSaveContext' => [
      		'name' => 'testSaveContext'
      	], //тестовая функция по сохранению контекста
    ];

	public static function testSaveContext($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath, $contextParh)
	{
		$dzetaCalc->EXPRESSION_METKA_GLOBAL = Constants::TYPE_ACTION_SAVE_NOCONTEXT;
		$null = Constants::SPEC_VALUES['NULL'];
		$null['ps'] = 0;
		$null['pf'] = 10;
		return $null;
	}
}