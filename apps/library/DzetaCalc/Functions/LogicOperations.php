<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Core\Helper;

class LogicOperations
{
	public static function LAND($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_BOOL && $operand2['type'] == Constants::TYPE_BOOL)
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => $operand1['value'] && $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['logicOper'],
			['AND','lO_and_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function LOR($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_BOOL && $operand2['type'] == Constants::TYPE_BOOL)
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => $operand1['value'] || $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['logicOper'],
			['OR','lO_or_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function Less($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => $operand1['value'] < $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['logicOper'],
			['Less','lO_less_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function LessEqual($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => $operand1['value'] < $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['logicOper'],
			['LessEq','lO_lessEq_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function Great($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => $operand1['value'] > $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['logicOper'],
			['Great','lO_great_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function GreatEqual($operand1, $operand2, $dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] == Constants::TYPE_NUMBER && $operand2['type'] == Constants::TYPE_NUMBER)
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => $operand1['value'] >= $operand2['value'],
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		Error::addError(
			['logicOper'],
			['GreatEq','lO_greatEq_0'],
			[],
			$dzetaCalc,
			[
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			]
		);
		return Helper::makeNullVariable([
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		]);
	}

	public static function Equal($operand1,$operand2,$dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] != $operand2['type'])
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => false,
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		if ($operand1['value'] == $operand2['value'])
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => true,
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		return [
			'type' => Constants::TYPE_BOOL,
			'value' => false,
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		];
	}

	public static function NotEqual($operand1,$operand2,$dzetaCalc)
	{
		$rows = array_unique([
			...(isset($operand1['rows']) ? $operand1['rows'] : []),
			...(isset($operand2['rows']) ? $operand2['rows'] : []),
		]);
		if ($operand1['type'] != $operand2['type'])
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => true,
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		if ($operand1['value'] == $operand2['value'])
		{
			return [
				'type' => Constants::TYPE_BOOL,
				'value' => false,
				'ps' => $operand1['ps'],
				'pf' => $operand2['pf'],
				'rows' => $rows,
			];
		}
		return [
			'type' => Constants::TYPE_BOOL,
			'value' => true,
			'ps' => $operand1['ps'],
			'pf' => $operand2['pf'],
			'rows' => $rows,
		];
	}
}