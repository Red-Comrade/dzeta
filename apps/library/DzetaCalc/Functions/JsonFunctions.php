<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\BaseFunctions;
use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;

class JsonFunctions extends BaseFunctions
{
    protected static $methods = [];

	protected static $functions = [
        'jsonEncode' => [
            'name' => 'encode',
            'completion' => [
                'label' => 'jsonEncode',
                'detail' => 'jsonEncode(mixed value)',
                'kind' => 'function',
                'insertText' => 'jsonEncode(${1:value})',
            ],
            'arguments' => [
                [
                    'type' => Constants::TYPE_ANY
                ]
            ]
        ],
        'jsonDecode' => [
            'name' => 'decode',
            'completion' => [
                'label' => 'jsonDecode',
                'detail' => 'jsonDecode(string json)',
                'kind' => 'function',
                'insertText' => 'jsonDecode(${1:json})',
            ],
            'arguments' => [
                [
                    'type' => Constants::TYPE_STRING
                ]
            ]
        ],
    ];

    public static function encode($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $value = $paramsFunc[0]['value'];
                if ($paramsFunc[0]['type'] == Constants::TYPE_ARRAY)
                {
                    $value = Helper::getRawArray($paramsFunc[0]);
                }
                return [
                    'type' => Constants::TYPE_STRING,
                    'value' => json_encode($value),
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows']
                ];
            }
        }
    }

    public static function decode($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $json = json_decode($paramsFunc[0]['value'], true);
                return Helper::convertPhpValue($json, $position);
            }
        }
    }
}