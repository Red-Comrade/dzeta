<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\BaseFunctions;
use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;

class DateTimeFunctions extends BaseFunctions
{
    protected static $methods = [
        'setYear' => [
            'name' => 'setYear',
            'completion' => [
                'label' => 'setYear',
                'detail' => 'setYear(number year)',
                'kind' => 'method',
                'insertText' => 'setYear(${1:year})',
            ],
            'arguments' => [
                [
                    'type' => Constants::TYPE_DATETIME
                ],
                [
                    'type' => Constants::TYPE_NUMBER,
                ]
            ]
        ]
    ];

	protected static $functions = [
        'dateTime' => [
            'name' => 'dateTime',
            'completion' => [
                'label' => 'dateTime',
                'detail' => 'dateTime(number day, number month, number year, number hour = 0, number minute = 0, number second = 0)',
                'kind' => 'function',
                'insertText' => 'dateTime(${1:day}, ${2:month}, ${3:year})'
            ],
            'arguments' => [
                [
                    'type' => Constants::TYPE_NUMBER,
                ],
                [
                    'type' => Constants::TYPE_NUMBER,
                ],
                [
                    'type' => Constants::TYPE_NUMBER,
                ],
                [
                    'type' => Constants::TYPE_NUMBER,
                    'optional' => true,
                    'default' => 0,
                ],
                [
                    'type' => Constants::TYPE_NUMBER,
                    'optional' => true,
                    'default' => 0,
                ],
                [
                    'type' => Constants::TYPE_NUMBER,
                    'optional' => true,
                    'default' => 0,
                ],
            ]
        ]
    ];

    public static function setYear($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(!empty($paramMethod))
        {
            $arguments = [
                $paramMethod,
                ...$paramsFunc
            ];
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, Constants::TYPE_METHOD);
            if ($check)
            {
                $date = clone $paramMethod['value'];
                $month = intval($date->format('m'));
                $day = intval($date->format('d'));
                $date->setDate($paramsFunc[0]['value'], $month, $day);
                return [
                    'type' => Constants::TYPE_DATETIME,
                    'value' => $date,
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows'],
                ];
            }
        }
    }

    public static function dateTime($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $function = self::getFunctionByName(__FUNCTION__);
                $day = $paramsFunc[0]['value'];
                $month = $paramsFunc[1]['value'];
                $year = $paramsFunc[2]['value'];
                $hour = isset($paramsFunc[3]) ? $paramsFunc[3]['value'] : $function['arguments'][3]['default'];
                $minute = isset($paramsFunc[4]) ? $paramsFunc[4]['value'] : $function['arguments'][4]['default'];
                $second = isset($paramsFunc[5]) ? $paramsFunc[5]['value'] : $function['arguments'][5]['default'];
                $date = new \DateTime();
                $date->setDate($year, $month, $day);
                $date->setTime($hour, $minute, $second);
                return [
                    'type' => Constants::TYPE_DATETIME,
                    'value' => $date,
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows'],
                ];
            }
        }
    }
}