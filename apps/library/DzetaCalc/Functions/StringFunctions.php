<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Core\BaseFunctions;

class StringFunctions extends BaseFunctions
{
	protected static $methods = [
		'toString' => [
            'name' => 'toString',
            'completion' => [
	            'label' => 'toString',
	            'detail' => 'toString()',
	            'kind' => 'method',
	            'insertText' => 'toString()',
	        ],
	        'arguments' => [],
        ],
        'replace' => [
            'name' => 'replace',
            'completion' => [
	            'label' => 'replace',
	            'detail' => 'replace(string|array search, string|array replace)',
	            'kind' => 'method',
	            'insertText' => 'replace(${1:search}, ${2:replace})',
	        ],
	        'arguments' => [
	        	[
	        		'type' => [
	        			Constants::TYPE_STRING,
	        			Constants::TYPE_ARRAY
	        		]
	        	],
	        	[
	        		'type' => [
	        			Constants::TYPE_STRING,
	        			Constants::TYPE_ARRAY
	        		]
	        	],
	        	[
	        		'type' => Constants::TYPE_STRING,
	        	],
	        ],
        ],
        'pos' => [
        	'name' => 'position',
        	'completion' => [
        		'label' => 'pos',
        		'detail' => 'pos(string needle, int offset = 0)',
        		'kind' => 'method',
        		'insertText' => 'pos(${1:needle})'
        	],
        	'arguments' => [
        		[
        			'type' => Constants::TYPE_STRING
        		],
        		[
        			'type' => Constants::TYPE_STRING
        		],
        		[
        			'type' => Constants::TYPE_NUMBER,
        			'optional' => true,
        			'default' => 0,
        		]
        	]
        ]
	];

	protected static $functions = [
		'strReplace' => [
			'name' => 'replace',
			'completion' => [
				'label' => 'strReplace',
				'detail' => 'strReplace(string|array search, string|array replace, string subject)',
				'kind' => 'function',
				'insertText' => 'strReplace(${1:search}, ${2:replace}, ${3:subject})',
			],
			'arguments' => [
				[
	        		'type' => [
	        			Constants::TYPE_STRING,
	        			Constants::TYPE_ARRAY
	        		]
	        	],
	        	[
	        		'type' => [
	        			Constants::TYPE_STRING,
	        			Constants::TYPE_ARRAY
	        		]
	        	],
	        	[
	        		'type' => Constants::TYPE_STRING,
	        	],
			]
		],
        'strPos' => [
        	'name' => 'position',
        	'completion' => [
        		'label' => 'strPos',
        		'detail' => 'strPos(string haystack, string needle, int offset = 0)',
        		'kind' => 'method',
        		'insertText' => 'strPos(${1:haystack}, ${2:needle})'
        	],
        	'arguments' => [
        		[
        			'type' => Constants::TYPE_STRING
        		],
        		[
        			'type' => Constants::TYPE_STRING
        		],
        		[
        			'type' => Constants::TYPE_NUMBER,
        			'optional' => true,
        			'default' => 0,
        		]
        	]
        ]
	];

	public static function toString($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$elemMethods = $paramMethod;
			if ($elemMethods['type'] == Constants::TYPE_NUMBER)
			{
				if (empty($paramsFunc))
				{
					//тупо к строке значит
					return [
						'type' => Constants::TYPE_STRING,
						'value' => $elemMethods['value'] . '',
						'ps' => $position['ps'],
						'pf' => $position['pf'],
						'rows' => $position['rows'],
					];
				}
			}
		}
	}

	public static function replace($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$type = Constants::TYPE_METHOD;
			$arguments = [
				...$paramsFunc,
				$paramMethod
			];
		} else
		{
			$type = Constants::TYPE_FUNCTION;
			$arguments = $paramsFunc;
		}
		$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, $type);
		if ($arguments[0]['type'] != Constants::TYPE_ARRAY && $arguments[1]['type'] == Constants::TYPE_ARRAY) {
			$check = false;
			$function = $type == Constants::TYPE_FUNCTION ? self::getFunctionByName(__FUNCTION__) : self::getMethodByName(__FUNCTION__);
			Error::addError(
                ['general'],
                ['functions', 'params_types_matching'],
                [
                    'function' => $function['name'],
                    'first_index' => 2,
                    'first_passed_type' => $arguments[1]['type'],
                    'first_function_type' => Constants::TYPE_STRING,
                    'second_index' => 1,
                    'second_passed_type' => $arguments[0]['type'],
                ],
                $dzetaCalc,
                [
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows'],
                ]
            );
		}
		if ($check) {
			$search = $arguments[0]['type'] == Constants::TYPE_ARRAY ? Helper::getRawArray($arguments[0]) : $arguments[0]['value'];
			$replace = $arguments[1]['type'] == Constants::TYPE_ARRAY ? Helper::getRawArray($arguments[1]) : $arguments[1]['value'];
			return [
				'type' => Constants::TYPE_STRING,
				'value' => str_replace($search, $replace, $arguments[2]['value']),
				'ps' => $position['ps'],
				'pf' => $position['pf'],
				'rows' => $position['rows'],
			];
		}
	}

	public static function position($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (!empty($paramMethod))
		{
			$type = Constants::TYPE_METHOD;
			$arguments = [
				$paramMethod,
				...$paramsFunc,
			];
		} else
		{
			$type = Constants::TYPE_FUNCTION;
			$arguments = $paramsFunc;
		}
		$check = self::checkArguments($dzetaCalc, __FUNCTION__, $arguments, $position, $type);
		if ($check) {
			$function = $type == Constants::TYPE_FUNCTION ? self::getFunctionByName(__FUNCTION__) : self::getMethodByName(__FUNCTION__);
			$offset = isset($arguments[2]) ? $arguments[2]['value'] : $function['arguments'][2]['default'];
			$pos = mb_strpos($arguments[0]['value'], $arguments[1]['value'], $offset);
			return [
				'type' => $pos === false ? Constants::TYPE_BOOL : Constants::TYPE_STRING,
				'value' => $pos,
				'ps' => $position['ps'],
				'pf' => $position['pf'],
				'rows' => $position['rows'],
			];
		}
	}
}