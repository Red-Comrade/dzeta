<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\BaseFunctions;
use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;

class NumberFunctions extends BaseFunctions
{
    protected static $methods = [];

	protected static $functions = [
        'abs' => [
                'name' => 'abs',
                'completion' => [
                    'label' => 'abs',
                    'detail' => 'abs(number value)',
                    'kind' => 'function',
                    'insertText' => 'abs(${1:value})',
                ],
                'arguments' => [
                    [
                        'type' => Constants::TYPE_NUMBER,
                    ],
                ]
            ],
            'ceil' => [
                'name' => 'ceil',
                'completion' => [
                    'label' => 'ceil',
                    'detail' => 'ceil(number value)',
                    'kind' => 'function',
                    'insertText' => 'ceil(${1:value})',
                ],
                'arguments' => [
                    [
                        'type' => Constants::TYPE_NUMBER,
                    ],
                ]
            ],
            'floor' => [
                'name' => 'floor',
                'completion' => [
                    'label' => 'floor',
                    'detail' => 'floor(number value)',
                    'kind' => 'function',
                    'insertText' => 'floor(${1:value})',
                ],
                'arguments' => [
                    [
                        'type' => Constants::TYPE_NUMBER,
                    ],
                ]
            ],
            'round' => [
                'name' => 'round',
                'completion' => [
                    'label' => 'round',
                    'detail' => 'round(number value, number precision = 0)',
                    'kind' => 'function',
                    'insertText' => 'round(${1:value})',
                ],
                'arguments' => [
                    [
                        'type' => Constants::TYPE_NUMBER,
                    ],
                    [
                        'type' => Constants::TYPE_NUMBER,
                        'optional' => true,
                        'default' => 0
                    ]
                ]
            ],
    ];

    public static function abs($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $value = $paramsFunc[0]['value'];
                return [
                    'type' => Constants::TYPE_NUMBER,
                    'value' => abs($value),
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows']
                ];
            }
        }
    }

    public static function ceil($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $value = $paramsFunc[0]['value'];
                return [
                    'type' => Constants::TYPE_NUMBER,
                    'value' => ceil($value),
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows']
                ];
            }
        }
    }

    public static function floor($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $value = $paramsFunc[0]['value'];
                return [
                    'type' => Constants::TYPE_NUMBER,
                    'value' => floor($value),
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows']
                ];
            }
        }
    }

    public static function round($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
    {
        if(empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $value = $paramsFunc[0]['value'];
                $function = self::getFunctionByName(__FUNCTION__);
                $precision = isset($paramsFunc[1]['value']) ? $paramsFunc[1]['value'] : $function['arguments'][1]['default'];
                return [
                    'type' => Constants::TYPE_NUMBER,
                    'value' => round($value, $precision),
                    'ps' => $position['ps'],
                    'pf' => $position['pf'],
                    'rows' => $position['rows']
                ];
            }
        }
    }
}