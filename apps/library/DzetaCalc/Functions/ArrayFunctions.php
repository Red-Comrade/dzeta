<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\BaseFunctions;
use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;

class ArrayFunctions extends BaseFunctions
{
    protected static $methods = [];

	protected static $functions = [
        'array' => [
            'name' => 'array',
            'completion' => [
                'label' => 'array',
                'detail' => 'array(mixed ...values)',
                'kind' => 'function',
                'insertText' => 'array(${1:...values})',
            ]
        ],
        'count' => [
            'name' => 'count',
            'completion' => [
                'label' => 'count',
                'detail' => 'count(mixed value)',
                'kind' => 'function',
                'insertText' => 'count(${1:value})',
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_ANY
				]
			]
        ],
        'inArray' => [
            'name' => 'inArray',
            'completion' => [
                'label' => 'inArray',
                'detail' => 'inArray(mixed needle, array haystack)',
                'kind' => 'function',
                'insertText' => 'inArray(${1:needle}, ${2:haystack})',
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_ANY
				],
				[
					'type' => Constants::TYPE_ARRAY
				]
			]
        ],
    ];

	public static function array($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if(empty($paramMethod))
		{
			//значит работает как функция
			$arrElems = [];
			$ps_arr = [];
			$pf_arr = [];
			$ps_arr[] = $position['ps'];
			$pf_arr[] = $position['pf'];
			$curIndex = 0;
			$answParam = [];
			foreach ($paramsFunc as $answ)
			{
				$arrElems[] = $answ;
				$ps_arr[] = $answ['ps'];
				$pf_arr[] = $answ['pf'];
			}
			return [
				'type' => Constants::TYPE_ARRAY,
				'value' => $arrElems,
				'ps' => min($ps_arr),
				'pf' => max($pf_arr)
			];
		}
	}

	public static function count($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if(empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				if ($paramsFunc[0]['type'] == Constants::TYPE_ARRAY) {
					return [
						'type' => Constants::TYPE_NUMBER,
						'value' => count($paramsFunc[0]['value']),
						'ps' => $position['ps'],
						'pf' => $position['pf'],
						'rows' => $position['rows']
					];
				} else {
					return [
						'type' => Constants::TYPE_NUMBER,
						'value' => 0,
						'ps' => $position['ps'],
						'pf' => $position['pf'],
						'rows' => $position['rows']
					];
				}
			}
		}
	}

	public static function inArray($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if(empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$search = $paramsFunc[0]['value'];
				if ($paramsFunc[0]['type'] == Constants::TYPE_ARRAY)
				{
					$search = Helper::getRawArray($paramsFunc[0]);
				}
				return [
					'type' => Constants::TYPE_BOOL,
					'value' => true,
					'value' => in_array($search, Helper::getRawArray($paramsFunc[1])),
					'ps' => $position['ps'],
					'pf' => $position['pf'],
					'rows' => $position['rows']
				];
			}
		}
	}
}
