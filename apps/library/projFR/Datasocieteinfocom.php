<?php

class Datasocieteinfocom
{
    const URL='https://societeinfo.com/app/rest/api/v2/';
    const filePathMain=BASE_PATH.'/config/dop/datasocieteinfocom.txt';
    public static function GetInfo($code)
    {
        $pathUrl=self::URL.'company.json/'.$code;
        return self::GetZapros($pathUrl);
    }

    private static function GetZapros($url)
    {
      $token=file_get_contents(self::filePathMain);
      $url.='?key='.$token;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;
    }
    ///api/v1/Entreprise/
}
