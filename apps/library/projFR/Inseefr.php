<?php

class Inseefr
{
    const URL='https://api.insee.fr/entreprises/sirene/V3/';
    const filePathMain=BASE_PATH.'/config/dop/inseefr.txt';
    public static function GetInfo($code)
    {
      //  $pathUrl=self::URL.'FicheIdentite/'.$code;
        return self::GetZapros($pathUrl);
    }

    private static function GetZapros($url)
    {
        $headr = array();
        $headr[] = 'Authorization: Bearer eef837ec-841d-3469-8542-c463724d5791';
        $headr[] = 'Accept: application/json';
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_HTTPHEADER,$headr);
        $rest = curl_exec($crl);
        curl_close($crl);
    }

    public static function GetInfoSiren($siren)
    {
        $url=self::URL.'siren/'.$siren;
        echo $url.PHP_EOL;
        self::GetZapros($url);
    }
    ///api/v1/Entreprise/
}
