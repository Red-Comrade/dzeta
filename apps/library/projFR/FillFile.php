<?php
use Phalcon\Config\Adapter\Json;
class FillFile
{
    

    private $tempFolders=BASE_PATH.'/ufiles/fillFileFr/';
    public $uid='';
    public $filesSpec=[];
    private $curPage=1;

    public function __construct(array $urls, $Data,$urlFoeExcel=[])
    {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/enematGlobal.json');
        $uniqID=uniqid().'_'.rand(1,1000000);
        if (!mkdir($this->tempFolders.$uniqID, 0777)) {
            $uid='';
            return null;
        }
        @chmod($this->tempFolders.$uniqID, 0777);
        $this->uid=$uniqID;
        $this->tempFolders=$this->tempFolders.$uniqID.'/';
        $allPage=array_sum(array_column($urls, 'sPage'));
        $Data['pages']=$allPage;
        /*  if(count($urls)!=count($jsonData))
          {
                echo 1;
              return null;
          }*/
        $pdf = new \Jurosh\PDFMerge\PDFMerger;
        $resArray=[];
        $acode='';
        $filesGenerate=[];
        $filesSpec=[];
        $flderID=$uniqID;
        foreach ($urls as $key => $value) 
        {            
            $newDataa=$Data;
            if (isset($value['sPage'])) 
            {
                $newDataa['page']=$this->curPage++;
                for ($ii=2;$ii<=$value['sPage'];$ii++) 
                {
                    $newDataa['page'.$ii]=$this->curPage++;
                }
            }

            if (isset($value['aCode'])) 
            {
                if (empty($value['aCode'])) 
                {
                    $newDataa['ACODE']='';
                } else 
                {
                    if (empty($acode)) 
                    {
                        $newDataa['ACODE']=1;
                        $acode=2;
                    }else 
                    {
                        $newDataa['ACODE']=$acode++;
                    }
                }
            }
            if(isset($value['dopData']))
            {
                foreach ($value['dopData'] as $key2 => $value1) 
                {
                    $newDataa[$key2]=$value1;
                }
            }
            $uniqID=($key+1).'_'.uniqid().'_'.$flderID;
            $fileTXT=$this->tempFolders.$uniqID.'.txt';
            $fileJSON=fopen($fileTXT, "w");
            fwrite($fileJSON, json_encode(['data'=>$newDataa,'url'=>$value['url'],'id'=>$uniqID,'path'=>$this->tempFolders]));
            fclose($fileJSON);
            $fileNohup=$this->tempFolders.$uniqID.'.nohup';
            switch ($config['modeConvert']) {
                case 'uLibra':
                    $pdfCmdExec='php  '.BASE_PATH.'/cli.php enemat  generateDoc '.$fileTXT;
                    break;
                case 'acermi':
                case 'Racermi':
                    if($config['typeGenerate']=='sequentially')
                    {
                        $pdfCmdExec='php  '.BASE_PATH.'/cli.php enemat  generateDoc '.$fileTXT;
                    }else
                    {
                        $pdfCmdExec='nohup php  '.BASE_PATH.'/cli.php enemat  generateDoc '.$fileTXT.' > '.$fileNohup.' &';
                    }                    
                    break;
            }
            exec($pdfCmdExec);
            if(isset($value['retDoc']))
            {
                $this->filesSpec[$value['retDoc']]=$uniqID;
            }
            $filesGenerate[]=$uniqID;
            continue;            
        }       
        $numIterr=0;
        while(1)
        {
            $numIterr++;
            $countPDF=0;
            $countNohup=0;
            if($handle = opendir( $this->tempFolders))
            {
                while(false !== ($file = readdir($handle))) 
                {
                    if($file != "." && $file != "..")
                    {
                        $ext=pathinfo($file, PATHINFO_EXTENSION);
                        if($ext=='logt')
                        {
                            $countPDF++;
                        }
                        if($ext=='nohup')
                        {
                            $countNohup++;
                        }
                        clearstatcache();
                    }
                }
            }
            if($countPDF>=count($filesGenerate))
            {
                break;
            }
            if($numIterr>200)
            {                
                throw new Exception("Ошибка при генерации, превышено время ожидания", 1);                
                break;
            }
            sleep(1);
        }
        usleep(10000);
        $iter=0;
        foreach ($filesGenerate as $key => $value) 
        {
            $pdfF=$this->tempFolders.$value.'_f.pdf';
            if(file_exists($pdfF))
            {
                $pdf->addPDF($pdfF, 'all', 'vertical'); 
            }else
            {
                sleep(2);
                if(file_exists($pdfF))
                {
                    $pdf->addPDF($pdfF, 'all', 'vertical'); 
                }else
                {                    
                    throw new Exception("Ошибка при генерации, превышено время ожидания не удалось с генерировать файл", 1);                
                }                
            }
            $indSpec=array_search($value,$this->filesSpec);
            if($indSpec!==false)
            {                
                $this->filesSpec[$indSpec]=$this->tempFolders.$value.'_f.pdf';
            }
        }
        $pdf->merge('file', $this->tempFolders.'result.pdf');
        if(!empty($urlFoeExcel))
        {
            self::createExcelData($urlFoeExcel,$Data);
        }
        
    }

    public  function createExcelData($dataSingle,$dataGlobal)
    {
        //1-скачиваем один файл Excel
        $urlGlob='https://docs.google.com/spreadsheets/d/1MFPyZiVQGh8hS33Ka3LCfxEWjVitFcbi9Bc97xeNNxg/edit#gid=0';
        self::dwnlFileGoogleExcel($urlGlob,$this->tempFolders.'excelN.xlsx');
        //1.5 надо удалить файл xl\drawings\drawing1.xml
        $zip = new \ZipArchive();
        $zip->open($this->tempFolders.'excelN.xlsx');
        $zip->deleteName('xl\drawings\drawing1.xml');
        $zip->close();
        $pathExcel=$this->tempFolders.'excel/';
        mkdir($pathExcel, 0777);
        //2- теперь проходим по сингл массиву где какждый элемент новый файл копия из этого

        foreach ($dataSingle as $key => $valueData) 
        {           
            $metkData=array_merge($valueData['dopData'],$dataGlobal);
            
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load($this->tempFolders.'excelN.xlsx');
            $cells = $spreadsheet->getSheetByName('Sheet1')->getCellCollection();
            $sheetExcell=$spreadsheet->getSheetByName('Sheet1');
            $maxRow=$cells->getHighestRow();
            $maxColumn=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($cells->getHighestColumn());

            for($row = 1; $row <= $maxRow; $row++)
            {
                for($column=1;$column<=$maxColumn;$column++)
                {
                    $colLetter=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column);
                    $cellValue=$cells->get($colLetter.$row)?$cells->get($colLetter.$row)->getValue():'';

                    preg_match_all('/\{[^}]*\{(.*?)\}.*?\}/', $cellValue, $output_array);
                   
                    if(!empty($output_array[1]))
                    {
                        foreach($output_array[1] as $value) 
                        {                           
                            if(isset($metkData[$value]))
                            {
                                if(isset($metkData[$value]['image']))
                                {
                                   
                                    //картинка                                    
                                    $tmpfname=$metkData[$value]['image'];
                                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                                    list($w, $h) = getimagesize($tmpfname);
                                    if($w/$h>2)
                                    {            
                                        $h=4.5*$h/$w;
                                        $w=4.5;
                                    }else
                                    {           
                                        $w=2.5*$w/$h;
                                        $h=2.5;
                                    }
                                    $w*=38;
                                    $h*=38;
                                    $drawing->setPath($tmpfname);
                                    $drawing->setName('testN'.uniqid());
                                    $drawing->setDescription('testD'.uniqid());
                                    $drawing->setHeight($h);
                                    $drawing->setWidth($w);
                                    //var_dump(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column) . $row);
                                    $drawing->setCoordinates(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column) . $row);
                                    $drawing->setWorksheet($sheetExcell);
                                    $cellValue='';
                                }else
                                {                                  
                                    //не картинка
                                    $cellValue=str_replace('{{'.$value.'}}',strip_tags($metkData[$value]), $cellValue);
                                }
                            }
                        }
                        $sheetExcell->setCellValue(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column) . $row,
                            strip_tags($cellValue));
                    }
                }
            }
            $roww=18;
            $columnn=['C','D','E'];
            $countt=0;
            foreach ($valueData['tableExcel'] as $valueTE) 
            {
                $sheetExcell->setCellValue($columnn[0]. $roww,$valueTE['marque']);
                $sheetExcell->setCellValue($columnn[1]. $roww,$valueTE['designation']);
                $sheetExcell->setCellValue($columnn[2]. $roww,$valueTE['diametre']);
                $roww++;
                if($roww>37)
                {
                    $columnn=['H','I','J'];
                    $roww=18;
                    $countt++;
                }
                if($countt==2)
                {
                    break;
                }
            }
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $writer->setIncludeCharts(true);
            $writer->save($pathExcel.($valueData['nameFile']).'.xlsx');            
        }
    }

    public static function dwnlFileGoogleP($url,$nameFile)
    {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/enematGlobal.json');

        preg_match('/\/d\/(.*?)\//', $url, $output_array);
       
        
        if (empty($output_array)) 
        {
            $outHandle = fopen($nameFile.'nfile', "w");
            fwrite($outHandle, 'имя файла\n');       
            fclose($outHandle);
            return false;
        }
        $id=$output_array[1];
        //red-octane-311512-4a5098edb07d.json  gifted-honor-236118-0330fc9ff24c.json
        $googleAccountKeyFilePath = BASE_PATH.$config['pathServiceAcc'];
        putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->setScopes('https://www.googleapis.com/auth/drive');
        $service = new \Google_Service_Drive($client);
        
        try
        {
            $content = $service->files->get($id, array("alt" => "media"));             
            if($content->getStatusCode()==200)
            {
                $outHandle = fopen($nameFile, "w+");
                while (!$content->getBody()->eof()) 
                {
                    fwrite($outHandle, $content->getBody()->read(1024));
                }        
                fclose($outHandle);
                return true;
            }else
            {                
                $outHandle = fopen($nameFile.'err', "w");
                fwrite($outHandle, json_encode($content->getStatusCode()).'\n');       
                fclose($outHandle);
                return false;
            }  
        }catch(Exception $err)
        {            
            $outHandle = fopen($nameFile.'err', "w");
            fwrite($outHandle, json_encode($err->getMessage()).'\n');       
            fclose($outHandle);
            return false;
        }        
        return false;
    }
    public static function replaceMetkaDocxP($nameFile, $jsonData,$nameFileFinal)
    {
        $templ=new \MyTemplateProcessor($nameFile);
        $allVar=$templ->getVariables();   
       
        $checkArr=[];
        if(isset($jsonData['Operations.CheckArray']))
        {
            $checkArr=$jsonData['Operations.CheckArray'];
        }
        if(isset($jsonData['Global.Check']))
        {
            $checkArr=array_merge($jsonData['Global.Check'], $checkArr);
        }
        foreach ($allVar as $key => $value) 
        {
            if (isset($jsonData[$value])) 
            {
                if (isset($jsonData[$value]['image'])) 
                {
                    continue;
                    $data=file_get_contents($jsonData[$value]['image']);
                    $tmpfname = tempnam("/tmp", "FOO");
                    $handle = fopen($tmpfname, "w");
                    fwrite($handle, $data);
                    fclose($handle);
                    $templ->setImg($value, [['src'=>$tmpfname]]);
                }else 
                {                    
                    $vale=$jsonData[$value];
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                    $vale=str_replace('&nbsp;', '', $vale);
                    $vale=str_replace('{{nl}}', '</w:t><w:br/><w:t xml:space="preserve">', $vale);
                    $templ->setValue('{{'.strip_tags($value).'}}', $vale);
                }
            }else
            {
                $metkExplode=explode('.',$value);
                if($metkExplode[0]=='Check')
                {

                    if(in_array(trim($value), $checkArr))
                    {                        
                        $templ->setValue('{{'.$value.'}}','✓'); //✓ //✔
                    }else
                    {
                        $templ->setValue('{{'.$value.'}}','□ ');
                    }
                }else if($metkExplode[0]=='CheckON')
                {                    
                    if(in_array(trim($value), $checkArr))
                    {                        
                        $templ->setValue('{{'.$value.'}}','✓');
                    }else
                    {
                        $templ->setValue('{{'.$value.'}}','□ ');
                    }
                }
            }            
        }
        foreach ($allVar as $key => $value) 
        {
            if (isset($jsonData[$value])) 
            {
                if (isset($jsonData[$value]['image'])) 
                {                    
                    $data=file_get_contents($jsonData[$value]['image']);
                    $tmpfname = tempnam("/tmp", "FOO");
                    $handle = fopen($tmpfname, "w");
                    fwrite($handle, $data);
                    fclose($handle);
                    $templ->setImg($value, [['src'=>$tmpfname]]);
                }
            }       
        }
        //теперь таблицы
        if(isset($jsonData['SYSTEME'])&&isset($jsonData['SYSTEME']['tble']))
        {
            for($i=0;$i<5;$i++)
            {
              $table=$jsonData['SYSTEME']['tble'];
                foreach ($table as  $valTale)
                {
                    $clone=false;
                    foreach ($valTale as $key => $value)
                    {
                       if(empty($clone))
                       {
                            if(!is_null($value)&&is_array($value))
                            {                        
                                $clone=$templ->cloneRow('{{'.$key.'}}', count($value));
                                
                            }                    
                       }
                       if(empty($clone)||!is_array($value))
                       {
                            continue;
                       }
                       
                       foreach ($value as $num => $vale) 
                       {
                           $vale=strip_tags($vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('&nbsp;', '', $vale);                          
                           $vale=str_replace('{{nl}}', '</w:t><w:br/><w:t xml:space="preserve">', $vale);
                           $templ->setValue('{{'.$key.'#'.($num+1).'}}',$vale);                          
                       }                       
                    }                   
                }  
            }
        }  
        $templ->insertcantSplitTable();
        $templ->setValue('{{cantSplit}}','');
        //зачистка не вставленных меток
        $allVar=$templ->getVariables();
        foreach ($allVar as $key => $value) 
        {
            $templ->setValue('{{'.$value.'}}','');
        }   
        $templ->saveAs($nameFileFinal);
        return true;
    }
    public static function replaceMetkaDocxP_new($nameFile, $jsonData,$nameFileFinal)
    {
        $templ=new \MyTemplateProcessor($nameFile);
        $allVar=$templ->getVariables();   
        $fileJSON=fopen($nameFile.'.json', "w");
        fwrite($fileJSON, json_encode(['data'=>$allVar,'urlFile'=>$jsonData]));
        fclose($fileJSON);
        $checkArr=[];
        $simpleMetka=isset($jsonData['simple'])?$jsonData['simple']:[];
        $tableMetka=isset($jsonData['table'])?$jsonData['table']:[];
        $imageKey=[];
        foreach ($allVar as $key => $valueKey) 
        {
            if(isset($simpleMetka[$valueKey]))
            {
                if($simpleMetka[$valueKey]['type']=='string')
                {
                    $needStriTags=false;
                    $v=$simpleMetka[$valueKey]['v'];   
                    $v=is_null($v)?'':$v;                 
                   // $v=self::replaceNLValue($v);
                    $templ->setValue('{{'.$valueKey.'}}', $v);
                    unset($allVar[$key]);
                    unset($simpleMetka[$valueKey]);
                }elseif($simpleMetka[$valueKey]['type']=='file')
                {
                    $imageKey[$valueKey]=$simpleMetka[$valueKey];
                    unset($allVar[$key]);
                    unset($simpleMetka[$valueKey]);
                }elseif($simpleMetka[$valueKey]['type']=='checkbox')
                {
                    if($simpleMetka[$valueKey]['v']==1)
                    {
                        $templ->setValue('{{'.$valueKey.'}}','✓'); //✓ //✔
                    }else
                    {
                        $templ->setValue('{{'.$valueKey.'}}','□ '); //✓ //✔
                    }
                }
            }            
        }
        foreach ($imageKey as $key => $value) 
        {
            $data=file_get_contents($value['v']);
            $tmpfname = tempnam("/tmp", "FOO");
            $handle = fopen($tmpfname, "w");
            fwrite($handle, $data);
            fclose($handle);
            $templ->setImg($key, [['src'=>$tmpfname]]);
        }

        if(!empty($tableMetka))
        {
            //вначале надо склонировать строку
            $countRow=count($tableMetka);
            $flagClone=false;
            foreach ($tableMetka as $valueRow) 
            {
                //теперь ищем что можно склонировать, надо именно первое 
                foreach ($valueRow as $keyMetka => $valueCell) 
                {
                    if(strpos($keyMetka,'Table.')===0)
                    {
                        if(!empty($templ->cloneRow($keyMetka, $countRow)))
                        {
                            $flagClone=true;
                            break;
                        }
                    }                    
                }
                break;
            }
            if($flagClone)
            {
                foreach ($tableMetka as $numRow=>$valueRow) 
                {                   
                    foreach ($valueRow as $keyMetka => $valueCell) 
                    {
                        $v=is_null($valueCell['v'])?'':$valueCell['v'];
                        $v=self::replaceNLValue($v);
                        $templ->setValue('{{'.$keyMetka.'#'.($numRow+1).'}}', $v);
                    }                    
                }
            }           
        }

         
        $templ->insertcantSplitTable();
        $templ->setValue('{{cantSplit}}','');
        $allVar=$templ->getVariables();   
        if(!empty($allVar))
        {
            $fileJSON=fopen($nameFile.'.novar', "w");
            fwrite($fileJSON, json_encode(['data'=>$allVar]));
            fclose($fileJSON);
        }       
        $templ->saveAs($nameFileFinal);
        return true;
    }

    private function dwnlFileGoogleExcel($url,$nameFile)
    {
        preg_match('/\/d\/(.*?)\//', $url, $output_array);
        if (empty($output_array)) {

            return false;
        }
        $id=$output_array[1];
        /*
            https://docs.google.com/spreadsheets/d/1MFPyZiVQGh8hS33Ka3LCfxEWjVitFcbi9Bc97xeNNxg/export?format=xlsx&id=1MFPyZiVQGh8hS33Ka3LCfxEWjVitFcbi9Bc97xeNNxg

        */
        $urlDwnl='https://docs.google.com/spreadsheets/d/'.$id.'/export?format=xlsx&id='.$id;
        $uniqID=uniqid();

        $googleAccountKeyFilePath = BASE_PATH.'/config/dop/gifted-honor-236118-0330fc9ff24c.json';
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->setScopes('https://www.googleapis.com/auth/drive');
        $service = new Google_Service_Drive($client);
        
        try
        {            
            $content = $service->files->export($id,'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', array(
            'alt' => 'media')); 

            if($content->getStatusCode()==200)
            {
                $outHandle = fopen($nameFile, "w+");                
                while (!$content->getBody()->eof()) 
                {                   
                    fwrite($outHandle, $content->getBody()->read(1024));
                }        
                fclose($outHandle);
                return true;
            }else
            {                
                $outHandle = fopen($nameFile.'err', "w");
                fwrite($outHandle, json_encode($content->getStatusCode()).'\n');       
                fclose($outHandle);
                return false;
            }  
        }catch(Exception $err)
        {            
            $outHandle = fopen($nameFile.'err', "w");
            fwrite($outHandle, json_encode($err->getMessage()).'\n');       
            fclose($outHandle);
            return false;
        }        
        return false;
    }

    private function dwnlFileGoogle($url)
    {
        preg_match('/\/d\/(.*?)\//', $url, $output_array);
        if (empty($output_array)) {

            return false;
        }
        $id=$output_array[1];
        $urlDwnl='https://docs.google.com/document/export?format=docx&id='.$id;
        $uniqID=uniqid();

        $googleAccountKeyFilePath = BASE_PATH.'/config/dop/gifted-honor-236118-0330fc9ff24c.json';
        putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->setScopes('https://www.googleapis.com/auth/drive');
        $service = new Google_Service_Drive($client);
        $content = $service->files->get($id, array("alt" => "media"));
        $outHandle = fopen($this->tempFolders.$uniqID.'.docx', "w+");
        while (!$content->getBody()->eof()) 
        {
            fwrite($outHandle, $content->getBody()->read(1024));
        }        
        fclose($outHandle);
        return $this->tempFolders.$uniqID.'.docx';
    }
    private function replaceMetkaDocx($nameFile, $jsonData)
    {
        $templ=new \MyTemplateProcessor($nameFile);
        $allVar=$templ->getVariables();   
       
        $checkArr=[];
        if(isset($jsonData['Operations.CheckArray']))
        {
            $checkArr=$jsonData['Operations.CheckArray'];
        }
        if(isset($jsonData['Global.Check']))
        {
             $checkArr=array_merge($jsonData['Global.Check'], $checkArr);
        }
        foreach ($allVar as $key => $value) {
            if (isset($jsonData[$value])) {
                if (isset($jsonData[$value]['image'])) {
                    $data=file_get_contents($jsonData[$value]['image']);
                    $tmpfname = tempnam("/tmp", "FOO");
                    $handle = fopen($tmpfname, "w");
                    fwrite($handle, $data);
                    fclose($handle);
                    $templ->setImg($value, [['src'=>$tmpfname]]);
                }  else {
					// $value=str_replace('&nbsp;', '</w:t><w:br/><w:t xml:space="preserve">', $value);
                    $templ->setValue('{{'.strip_tags($value).'}}', $jsonData[$value]);
                }
            }else{
                $metkExplode=explode('.',$value);
                if($metkExplode[0]=='Check')
                {

                    if(in_array(trim($value), $checkArr))
                    {
                        
                        $templ->setValue('{{'.$value.'}}','✓');
                    }else
                    {
                        $templ->setValue('{{'.$value.'}}','□ ');
                    }
                }else if($metkExplode[0]=='CheckON')
                {                    
                    if(in_array(trim($value), $checkArr))
                    {                        
                        $templ->setValue('{{'.$value.'}}','✓');
                    }else
                    {
                        $templ->setValue('{{'.$value.'}}','□ ');
                    }
                }
            }
            
        }

      /*  $allVar=$templ->getVariables();   
        echo json_encode(array_values($allVar));
        echo PHP_EOL;*/
        //теперь таблицы
        if(isset($jsonData['SYSTEME'])&&isset($jsonData['SYSTEME']['tble']))
        {
            for($i=0;$i<5;$i++)
            {
              $table=$jsonData['SYSTEME']['tble'];
                foreach ($table as  $valTale)
                {
                    $clone=false;
                    

                    foreach ($valTale as $key => $value)
                    {
                       if(empty($clone))
                       {
                            if(!is_null($value)&&is_array($value))
                            {                        
                                $clone=$templ->cloneRow('{{'.$key.'}}', count($value));
                                
                            }                    
                       }
                       if(empty($clone)||!is_array($value))
                       {
                        continue;
                       }
                       
                       foreach ($value as $num => $vale) {
                           $vale=strip_tags($vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('{{nl}}{{nl}}', '{{nl}}', $vale);
                           $vale=str_replace('&nbsp;', '', $vale);
                          
                           $vale=str_replace('{{nl}}', '</w:t><w:br/><w:t xml:space="preserve">', $vale);
                           $templ->setValue('{{'.$key.'#'.($num+1).'}}',$vale);
                          
                       }
                       
                    }
                   
                }  
            }
        }
        
        
        
        $templ->insertcantSplitTable();
        $templ->setValue('{{cantSplit}}','');
        $nOut=$this->tempFolders.uniqid().'.docx';
        $templ->saveAs($nOut);
        return $nOut;
    }

    public static function replaceNLValue($val)
    {

        //тут заменяем nl на норм значения
        $val=strip_tags($val);  
        while(1)
        {
            if(strpos($val,'{{nl}}{{nl}}')!==false)
            {
                $val=str_replace('{{nl}}{{nl}}', '{{nl}}', $val);
            }
            break;
        }
        $val=str_replace('&nbsp;', '', $val);              
        $val=str_replace('{{nl}}', '</w:t><w:br/><w:t xml:space="preserve">', $val);
        return $val;
    }
}
