<?php

class Datainfogreffe
{
    const URL='https://api.datainfogreffe.fr/api/v1/Entreprise/';
    const filePathMain=BASE_PATH.'/config/dop/datainfogreffe.txt';
    public static function GetInfo($code)
    {
        $pathUrl=self::URL.'FicheIdentite/'.$code;
        return self::GetZapros($pathUrl);
    }

    private static function GetZapros($url)
    {
      $token=file_get_contents(self::filePathMain);
      $url.='?token='.$token;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;

    }
    ///api/v1/Entreprise/
}
