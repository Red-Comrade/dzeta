<?php
//пересмотреть изолянты как они работают
namespace generateDocEnemat;
class generateDocs
{
	public $USER=null;
	public $requestData=null;
	public $dataLogs=[];
	public $UID_FOLDER='';
	public $objectsGlobal=[
		'objet'=>null,
		'devis'=>null,
		'facture'=>null,
		'ah'=>null,
		'results'=>null,
		'resultsEMMY'=>null
	];
    public $CASHE_OBJ_VALUES=[];
	private $generateUIDFilesPDF=[];
	public function __construct($requestData,$User)
	{
		$this->requestData=$requestData;		
		$this->dataLogs[]=[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Начинаем процесс',
			'data'=>[
				'request'=>$requestData,
				'userUID'=>$User->getUid(),
				'userName'=>$User->getName()
			]
		];
		$this->USER=$User;
		$this->initCore();
		$this->createFolder();
	}
	public function start()
	{
		$r=$this->startProc();
		$this->writeLogs();
        if(empty($r))
        {
        	return false;
        }
        
        return true;
	}
	private function initCore()
	{
		//подключаем файлы
		$dirs=['/core'];
		foreach ($dirs as $key => $value)
		{
			$dir = __DIR__.$value;  
		 	$catalog = opendir($dir);	 
		   	while ($filename = readdir($catalog ))
		   	{  	      
		   		if($filename!='.'&&$filename!='..')
		   		{
		   			include_once($dir."/".$filename); 
		   		}	      
		    }
		    closedir($catalog);
		}

		if(!file_exists(Constans::PATH_CONFIG_SETT_METKA))
		{
			throw new \Exception("Отсутвует файл 1", 1);			
		}
		if(!file_exists(Constans::PATH_CONFIG_SETT_OBJ_REQUEST))
		{
			throw new \Exception("Отсутвует файл 2", 1);			
		}
		
	}

	public function genUidFiles()
	{
		return $this->generateUIDFilesPDF;
	}

	public function startProc()
	{
		//метод в котором идет вызов всего остального, Excel и т.д
		//вначале надо вытянуть тип документа

		$copyRequest=$this->requestData;
		$typeGenDoc=null;
        $typeModeCreateDoc=null;
        $dateNow=date('d/m/Y H:i:s');
        $typeGenDocArr=[];
        $flagFactureAH_GLOBAL=false;
        $configParamData=json_decode(file_get_contents(Constans::PATH_CONFIG_SETT_OBJ_REQUEST),true);
        if(isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']])||
    		isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['edit']]))
        {
        	$typeGenDoc=isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']])? $copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']]:$copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['edit']];
        	$typeModeCreateDoc=isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']])? 'create':'edit';
        	switch ($typeGenDoc) 
            {
              	case 'devis':
                	$typeGenDoc='devis';
                	$typeGenDocArr[]='devis';
                	break;
              	case 'ah':
                	$typeGenDoc='ah';
                	$typeGenDocArr[]='ah';
                	break;
              	case 'facture':
                	$typeGenDoc='facture';
                	$typeGenDocArr[]='facture';
                	$copyOperations=HelperFunctions::findValueConfigRequest($copyRequest,Constans::KEY_OPERATIONS_DATA);

		            $copyOperations=empty($copyOperations)? []:$copyOperations;
		            $flagFactureAH=true;
		            foreach ($copyOperations as $key => $valueOper) 
		            {
		                $keyLock=HelperFunctions::findValueConfigRequest($valueOper,Constans::KEY_LOCK_FACTURE_AH); 
		                if($keyLock==="false")
		                {
		                	$keyLock=false;
		                }
		                if(empty($keyLock))
		                {
		                	$flagFactureAH=false;
		                	break;
		                }
		            }
		            if($flagFactureAH)
		            {
		            	$flagFactureAH_GLOBAL=true;
		                $typeGenDocArr[]='ah';
		            } 
                	break;
              	case 'devisah':
                	$typeGenDoc='devisah';
                	$typeGenDocArr[]='devisah';
                	break;
            }
        }        
        if(is_null($typeGenDoc)||is_null($typeModeCreateDoc))
        {
        	$this->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Отсутсвует тип документа в запросе',
				'err'=>true
			];
        	return false;
        }
        $attrUID=null;
        $startName='';
        $ObjMain=null;
        $valPersone=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['writeDefBenificare']['key']);
        foreach ($typeGenDocArr as $typeGenDoc) 
        {
        	switch ($typeGenDoc) 
	        {
	        	case 'devis':
	        		/*пишем в юзера тип персоны*/        		
	        		$res=objDeTravaux::writeDefaultBenificiare($valPersone,$this);
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		/*теперь работаем с веткой objDesTravaux*/
	        		$objDeTravaux=new objDeTravaux($copyRequest,$this);
	        		$res=$objDeTravaux->init();
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		$this->objectsGlobal['objet']=[];
	        		$this->objectsGlobal['devis']=[];
	        		$this->objectsGlobal['objet']=['obj'=>$res['Objetdestravaux']];
	        		$this->objectsGlobal['devis']=['obj'=>$res['Devis']];
	        		/*теперь генерим pdf в девис*/
	        		$pdf=new createPDF($copyRequest,$this,'devis');
	        		$filePDF=$pdf->init();
	        		if(empty($filePDF))
	        		{
	        			return false;
	        		}
	        		//var_dump($filePDF);
	        		//теперь делаем запсиь
	        		$attrUID=Constans::ATTR_UIDS_FILES['devis']['uid'];
	        		$startName=Constans::ATTR_UIDS_FILES['devis']['nameStart'];
	        		$ObjMain=new \Dzeta\Core\Instance($this->objectsGlobal['devis']['obj'], '');
	        		break;
	        	case 'facture':
	        		$objDeTravaux=new objDeTravaux($copyRequest,$this);
	        		$res=$objDeTravaux->init();
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		$this->objectsGlobal['objet']=[];
	        		$this->objectsGlobal['facture']=[];
	        		$this->objectsGlobal['objet']=['obj'=>$res['Objetdestravaux']];
	        		$this->objectsGlobal['facture']=['obj'=>$res['Facture']];
	        		$pdf=new createPDF($copyRequest,$this,'facture');
	        		$filePDF=$pdf->init();
	        		if(empty($filePDF))
	        		{
	        			return false;
	        		}        		
	        		//теперь делаем запсиь
	        		$attrUID=Constans::ATTR_UIDS_FILES['facture']['uid'];
	        		$startName=Constans::ATTR_UIDS_FILES['facture']['nameStart'];
	        		$ObjMain=new \Dzeta\Core\Instance($this->objectsGlobal['facture']['obj'], '');
	        		//тут еще процесс по экслелькам
	        		$fileExcel=$pdf->pathExcelArrs;
	        		$pathFileForWrite=null;
	        		if(count($fileExcel)==1)
	        		{
	        			$pathFileForWrite=$fileExcel[0];
	        			$nameFile=basename($pathFileForWrite);
	        		}elseif(count($fileExcel)>1)
	        		{
	        			$zip = new \ZipArchive();
						$pathFileForWrite =@tempnam("/tmp", "zip");;
						$zip->open($temp, \ZIPARCHIVE::CREATE);
						foreach ($filelist as $valueFl) 
						{
						  	$zip->addFromString(basename($valueFl), file_get_contents($valueFl));
						}
						$zip->close();
						$nameFile='excelTable.zip';
	        		}
	        		$uuidd=\Dzeta\Core\Value\File::GenerateUIDFile();    
	        		$this->generateUIDFilesPDF[]=$uuidd;
			        $res=copy($pathFileForWrite, BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuidd.\Dzeta\Core\Value\File::FILE_EXT);
			        if(empty($res))
			        {
			        	$this->dataLogs[]=
						[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Проблема с копированием 2',
							'err'=>true,
							'data'=>
							[
								'uid'=>$uuidd,
								'filePath'=>$pathFileForWrite
							]
						];
			        	return false;
			        }    
			        $attrUIDExcel=Constans::ATTR_UIDS_FILES['factureExcel']['uid'];
			        $filePDFobj=new \Dzeta\Core\Value\File($uuidd, $nameFile, null, filesize($pathFileForWrite));
			        $attr = new \Dzeta\Core\Type\Attribute($attrUIDExcel, '',5, false, false);
			        $attr->setValueS($filePDFobj);
			        $ObjMain->setAttributes([$attr]);
			        $_obj=new  \Dzeta\Models\Obj();
			        $results = $_obj->AddFiles($ObjMain, $this->USER);

					if(isset($results[$attrUIDExcel]))
					{
						if(isset($results[$attrUIDExcel]['result'])&&$results[$attrUIDExcel]['result']==1)
						{
							$flagErr=false;
						}
					}
					if($flagErr)
					{			
						$this->generateDoc->dataLogs[]=
						[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Не удалось записать файл в объект',
							'err'=>true,
							'data'=>
							[
								'uid'=>$ObjMain->getUid(),
								'file'=>$uuidd,
								'origPath'=>$filePDF
							]
						];
						return false;
					}
			        $this->dataLogs[]=
					[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Файл записан в объект',
						'data'=>
						[
							'uid'=>$ObjMain->getUid(),
							'file'=>$uuidd,
							'origPath'=>$filePDF
						]
					];	
	        		break;
	        	case 'ah':
	        		$copyRequestAH=$copyRequest;
	        		if($flagFactureAH_GLOBAL)
	        		{
	        			//тут надо делать перегон реквеста
	        			$copyRequestAH['created_document']='ah';
	        			foreach ($copyRequestAH['operation'] as $key => $value) 
	        			{
	        				foreach ($copyRequestAH['operation'][$key]['isolant'] as $keyI => $value) 
	        				{
	        					unset($copyRequestAH['operation'][$key]['isolant'][$keyI]['linked_obj']);
	        				}
	        				unset($copyRequestAH['operation'][$key]['linked_obj']);
	        			}
	        		}
	        		$objResult=new objResult($copyRequestAH,$this);
	        		$res=$objResult->init();
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		$this->objectsGlobal['results']=[];
	        		$this->objectsGlobal['results']=['obj'=>$res['mainResult']];
	        		$objResultEMMY=new objResultEMMY($copyRequestAH,$this);
	        		$res=$objResultEMMY->init();
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		$this->objectsGlobal['resultsEMMY']=[];
	        		foreach ($res as $value) 
	        		{
	        			$this->objectsGlobal['resultsEMMY'][]=['obj'=>$value];
	        		}
	        		$objDeTravaux=new objDeTravaux($copyRequestAH,$this);
	        		$res=$objDeTravaux->init();
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		$this->objectsGlobal['objet']=[];
	        		$this->objectsGlobal['ah']=[];
	        		$this->objectsGlobal['objet']=['obj'=>$res['Objetdestravaux']];
	        		$this->objectsGlobal['ah']=['obj'=>$res['AH']];
	        		$pdf=new createPDF($copyRequest,$this,'ah');
	        		$filePDF=$pdf->init();
	        		if(empty($filePDF))
	        		{
	        			return false;
	        		}        		
	        		//теперь делаем запсиь
	        		$attrUID=Constans::ATTR_UIDS_FILES['ah']['uid'];
	        		$startName=Constans::ATTR_UIDS_FILES['ah']['nameStart'];
	        		$ObjMain=new \Dzeta\Core\Instance($this->objectsGlobal['ah']['obj'], '');
	        		break;
	        	case 'devisah':
	        		$res=objDeTravaux::writeDefaultBenificiare($valPersone,$this);
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		/*теперь работаем с веткой objDesTravaux*/
	        		$objDeTravaux=new objDeTravaux($copyRequest,$this);
	        		$res=$objDeTravaux->init();
	        		if(empty($res))
	        		{
	        			return false;
	        		}
	        		$this->objectsGlobal['objet']=[];
	        		$this->objectsGlobal['devis']=[];
	        		$this->objectsGlobal['ah']=[];
	        		$this->objectsGlobal['objet']=['obj'=>$res['Objetdestravaux']];
	        		$this->objectsGlobal['devis']=['obj'=>$res['Devis']];
	        		$this->objectsGlobal['ah']=['obj'=>$res['AH']];
	        		$pdf=new createPDF($copyRequest,$this,'devisah');
	        		$filePDF=$pdf->init();
	        		if(empty($filePDF))
	        		{
	        			return false;
	        		}        		
	        		//теперь делаем запсиь
	        		$attrUID=Constans::ATTR_UIDS_FILES['ah']['uid'];
	        		$startName=Constans::ATTR_UIDS_FILES['ah']['nameStart'];
	        		$ObjMain=new \Dzeta\Core\Instance($this->objectsGlobal['ah']['obj'], '');
	        		break;	        	
	        }
	        $uuidd=\Dzeta\Core\Value\File::GenerateUIDFile();        
	        $this->generateUIDFilesPDF[]=$uuidd;
	        $res=copy($filePDF, BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuidd.\Dzeta\Core\Value\File::FILE_EXT);
	        if(empty($res))
	        {
	        	$this->dataLogs[]=
				[
					'time'=>date('d/m/Y H:i:s'),
					'text'=>'Проблема с копированием',
					'err'=>true,
					'data'=>
					[
						'uid'=>$uuidd,
						'filePath'=>$filePDF
					]
				];
	        	return false;
	        }
	        $nameKeys=[];
	        $symbImplode='';
	        if($valPersone=='Personne physique')
	        {                      
	        	$nameKeys=Constans::ATTR_UIDS_FILES['nameFilee']['phys']['attrsArr'];
	        	$symbImplode=Constans::ATTR_UIDS_FILES['nameFilee']['phys']['symb'];
	        }else
	        {
	        	$nameKeys=Constans::ATTR_UIDS_FILES['nameFilee']['pmorale']['attrsArr'];
	        	$symbImplode=Constans::ATTR_UIDS_FILES['nameFilee']['pmorale']['symb'];
	        }
	        $nArr=[];
	        foreach ($nameKeys as $key => $value) 
	        {
	        	$tmp=HelperFunctions::findValueConfigRequest($copyRequest,$value);
	        	$nArr[]=is_null($tmp)? '':$tmp;
	        }
	        $nameFile=$startName.implode($symbImplode,$nArr).'.pdf';
	        $filePDFobj=new \Dzeta\Core\Value\File($uuidd, $nameFile, null, filesize($filePDF));
	        $attr = new \Dzeta\Core\Type\Attribute($attrUID, '',5, false, false);
	        $attr->setValueS($filePDFobj);
	        $ObjMain->setAttributes([$attr]);
	        $_obj=new  \Dzeta\Models\Obj();
	        $results = $_obj->AddFiles($ObjMain, $this->USER);

			if(isset($results[$attrUID]))
			{
				if(isset($results[$attrUID]['result'])&&$results[$attrUID]['result']==1)
				{
					$flagErr=false;
				}
			}
			if($flagErr)
			{			
				$this->generateDoc->dataLogs[]=
				[
					'time'=>date('d/m/Y H:i:s'),
					'text'=>'Не удалось записать файл в объект',
					'err'=>true,
					'data'=>
					[
						'uid'=>$ObjMain->getUid(),
						'file'=>$uuidd,
						'origPath'=>$filePDF
					]
				];
				return false;
			}
	        $this->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Файл записан в объект',
				'data'=>
				[
					'uid'=>$ObjMain->getUid(),
					'file'=>$uuidd,
					'origPath'=>$filePDF
				]
			];				
        }
        
        return true;
	}

	private function createFolder()
	{
		$this->UID_FOLDER=uniqid();
		mkdir(Constans::PATH_FOLDER_GENERATE.$this->UID_FOLDER,0777);
		$this->dataLogs[]=[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Папка для складирования uid:'.$this->UID_FOLDER,
			'data'=>[
				'uid'=>$this->UID_FOLDER
			]
		];		
	}

	private function writeLogs()
	{
		$handle = fopen(Constans::PATH_FOLDER_GENERATE.$this->UID_FOLDER.'/'.$this->UID_FOLDER.'.log', "w");
		$this->dataLogs[]=[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Начинаем писать в лог'			
		];
		foreach ($this->dataLogs as $value) 
		{
			fwrite($handle, json_encode($value). "\n");
		}
		fwrite($handle, json_encode([
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Закончили писать в лог'			
		]). "\n");
		fclose($handle);
	}
	private function writeExcelLogs()
	{
		
	}
	
}