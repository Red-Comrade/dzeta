<?php

namespace generateDocEnemat;

class HelperDB
{
	public static function getLastDevis($generateDocs)
	{
		$_obj=new \Dzeta\Models\Obj();
		$objForDevis=new \Dzeta\Core\Instance(Constans::GLOBAL_SETT_CONFIG['№devis']['obj'], '',new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['№devis']['type'], ''));
		
		$objForDevis->setAttributes(
		[
			new \Dzeta\Core\Type\Attribute(Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['uid'],'Devis number',Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['datatype'],false,false)
		]);
		
		$_obj->getAllValues([$objForDevis]);

		$numDevis=null;
		$allAttr=$objForDevis->getAttributes();
		foreach ($allAttr as $key=> $attr) 
		{
			switch ($attr->getUid()) 
			{
				case Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['uid']:
					if(!empty($attr->getValue()))
					{
						$numDevis=$attr->getValue()->getValue();
						$numDevis=$numDevis+1;
					}else
					{
						throw new \Exception("Не найдено Devis Global", 1);						
					}
					$allAttr[$key]->setValue(['value'=>$numDevis]);
					break;
			}
		}
		if(empty($numDevis))
		{
			throw new \Exception("Не найдено Devis Global", 1);		
		}
		$objForDevis->setAttributes($allAttr);
		$_obj->addValues([$objForDevis],$generateDocs->USER);		

		$objUserCopy=new \Dzeta\Core\Instance($generateDocs->USER->getUid(), '',new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['userDevis']['type'], ''));
		$objUserCopy->setAttributes(
		[
			new \Dzeta\Core\Type\Attribute(Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['uid'],'User devis',Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['datatype'],false,false)
		]);
		$_obj->getAllValues([$objUserCopy]);

		$numDevisUser=null;
		$allAttr=$objUserCopy->getAttributes();
		foreach ($allAttr as $key=> $attr) 
		{
			switch ($attr->getUid()) 
			{
				case Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['uid']:
					if(!empty($attr->getValue()))
					{
						$numDevisUser=$attr->getValue()->getValue();
					}else
					{
						$numDevisUser=0;					
					}
					$numDevisUser=$numDevisUser+1;
					$allAttr[$key]->setValue(['value'=>$numDevisUser]);
					break;
			}
		}
		if(empty($numDevisUser))
		{
			throw new \Exception("Не найдено Devis User", 1);		
		}
		$_obj->addValues([$objUserCopy],$generateDocs->USER);
		return ['devisUser'=>$numDevisUser,'devisGlobal'=>$numDevis,'devis'=>$numDevis.'-'.$numDevisUser];
	}


	public static function getDateCreationobjetDesTravaux($uidObjDesTravaux,$generateDocs)
	{		
		$_obj=new \Dzeta\Models\Obj();
		$obj=new \Dzeta\Core\Instance($uidObjDesTravaux, '',new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['dateCreateionDesTravaux']['type'], ''));
		$obj->setAttributes(
		[
			new \Dzeta\Core\Type\Attribute(Constans::GLOBAL_SETT_CONFIG['dateCreateionDesTravaux']['attr']['uid'],'Devis number',Constans::GLOBAL_SETT_CONFIG['dateCreateionDesTravaux']['attr']['datatype'],false,false)
		]);		
		$_obj->getAllValues([$obj]);
		$dateCreat=null;
		$allAttr=$obj->getAttributes();
		foreach ($allAttr as $key => $attr) 
		{
			switch ($attr->getUid()) 
			{
				case Constans::GLOBAL_SETT_CONFIG['dateCreateionDesTravaux']['attr']['uid']:
					if(!empty($attr->getValue()))
					{
						$dateCreat=$attr->getValue()->getValue();
					}	
					break;
			}
		}
		if(empty($dateCreat))
		{
			throw new \Exception("Не получено Date Creation", 1);	
		}
		return $dateCreat;
	}

	
}