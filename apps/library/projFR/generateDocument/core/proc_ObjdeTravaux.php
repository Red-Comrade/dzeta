<?php

namespace generateDocEnemat;

class objDeTravaux
{
	private $copyRequest=[];
	private $generateDoc=null;


	public function __construct($requestData,$objGenDoc)
	{
		$this->copyRequest=$requestData;
		$this->generateDoc=$objGenDoc;
	}

	public function init()
	{
		$copyRequest=$this->copyRequest;
		$this->generateDoc->dataLogs[]=
		[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Начинаем работать с веткой Objet des travaux'
		];
		$_group = new \Dzeta\Models\AttrGroup();
        $_obj=new \Dzeta\Models\Obj();
        if(!file_exists(Constans::PATH_CONFIG_SETT_OBJ_REQUEST))
        {
        	$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Отсутсвует файл с настройками',
				'err'=>true
			];
        	return false;
        }

        $typeGenDoc=null;
        $typeModeCreateDoc=null;
        $dateNow=date('d/m/Y H:i:s');
        
        $configParamData=json_decode(file_get_contents(Constans::PATH_CONFIG_SETT_OBJ_REQUEST),true);
        if(isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']])||
    		isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['edit']]))
        {
        	$typeGenDoc=isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']])? $copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']]:$copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['edit']];
        	$typeModeCreateDoc=isset($copyRequest[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['typeGenerateDocument']['create']])? 'create':'edit';
        	switch ($typeGenDoc) 
            {
              	case 'devis':
                	$typeGenDoc='devis';
                	break;
              	case 'ah':
                	$typeGenDoc='ah';
                	break;
              	case 'facture':
                	$typeGenDoc='facture';
                	break;
              	case 'devisah':
                	$typeGenDoc='devisah';
                	break;
            }
        }

        if(is_null($typeGenDoc)||is_null($typeModeCreateDoc))
        {
        	$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Отсутсвует тип документа в запросе',
				'err'=>true
			];
        	return false;
        }
        
        //уиды нашей ветки
        $objectUIDS=[
        	'Objetdestravaux'=>null,
        	'Devis'=>null,
        	'Facture'=>null,
        	'AH'=>null
        ];
        
        //надо предварительная обработка для некоторых ситуаций
        if($copyRequest['persone']=='Personne physique')
        {
           $copyRequest['information_lieu_travaux']['nom_travaux']=$copyRequest['beneficiaire']['nom'].' '.$copyRequest['beneficiaire']['prenom']; 
        }
        if($typeGenDoc=='devis')
        {
        	//пишем в пользователя Default beneficiare
        	$valPersone=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['writeDefBenificare']['key']);
        	 
        	if($typeModeCreateDoc=='create')
        	{        		
        		$nDevis=$this->getLastDevis();
        		if(empty($nDevis))
        		{
        			return false;
        		}
        		//вначале надо создать objetDeTravaux
        		$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Objetdestravaux, ''));
        		$dataParamObjDeTravauxDB=$_group->getAttributes($group);
        		$dataParamObjDeTravaux=HelperFunctions::fillParamForFormData($dataParamObjDeTravauxDB,$configParamData,$copyRequest,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis['globalDevis'].'-'.$nDevis['userDevis'],'user'=>$this->generateDoc->USER->getUid()]);
        		$objDeTravaux=new \Dzeta\Core\Instance('', '');
        		$objDeTravaux->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Objetdestravaux, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
        		$objDeTravaux->setAttributes($dataParamObjDeTravaux);
        		$res=$_obj->checkUniqueAndAddWithValues($objDeTravaux, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
        		if(empty($objDeTravaux->getUid()))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Не удалось создать obj De travaux',
						'data'=>
						[
							'res'=>$res
						]
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),						
						'text'=>'Удалось создать obj De travaux',
						'data'=>
						[
							'uid'=>$objDeTravaux->getUid()
						]
					];
        		}
        		$objectUIDS['Objetdestravaux']=$objDeTravaux->getUid();
        		//теперь создаем Devis
        		$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DEVIS, ''));
        		$dataParamDevisDB=$_group->getAttributes($group);
        		$dataParamDevis=HelperFunctions::fillParamForFormData($dataParamDevisDB,$configParamData,$copyRequest,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis['globalDevis'].'-'.$nDevis['userDevis'],'user'=>$this->generateDoc->USER->getUid()]);
        		$objDevis=new \Dzeta\Core\Instance('', '');
        		$objDevis->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DEVIS, ''))->setParent($objDeTravaux);
        		$objDevis->setAttributes($dataParamDevis);
        		$_obj->checkUniqueAndAddWithValues($objDevis, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
        		if(empty($objDevis->getUid()))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Не удалось создать Devis'
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),						
						'text'=>'Удалось создать Devis',
						'data'=>
						[
							'uid'=>$objDevis->getUid()
						]
					];
        		}
        		$objectUIDS['Devis']=$objDevis->getUid();
        	}else
        	{
        		//редактирование
        		$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DEVIS, ''));

        		$objID=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['keyObjectID']);
        		if(empty($objID))
        		{
        			$this->generateDoc->dataLogs[]=
					[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Отсутсвует главный объект в запросе',
						'err'=>true
					];
		        	return false;
        		}
        		$objectUIDS['Devis']=$objID;
        		$dataParamDevisDB=$_group->getAttributes($group);
        		$dataParamDevis=HelperFunctions::fillParamForFormData($dataParamDevisDB,$configParamData,$copyRequest,['dateNow'=>null,'user'=>$this->generateDoc->USER->getUid()]);
        		$objDevis=new \Dzeta\Core\Instance($objID, '');
        		$objDevis->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DEVIS, ''));
        		$objDevis->setAttributes($dataParamDevis);
        		
        		$this->generateDoc->dataLogs[]=
				[
					'time'=>date('d/m/Y H:i:s'),
					'text'=>'Обновляем объект с uid='.$objID,
					'data'=>
					[
						'uid'=>$objID,
						'obj'=>$objDevis
					]
				];

				$res=$_obj->addValues([$objDevis],$this->generateDoc->USER);
				
        		if(empty($res['result']))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта Devis не успешно',
						'err'=>true
					];
					return false;
        		}
        		$flagErr=false;
        		foreach ($res['result'] as  $valueAttr) 
        		{
        			if($valueAttr['result']!=1)
        			{
        				$flagErr=true;
        				break;
        			}
        		}
        		if(empty($res)||$flagErr)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта Devis не успешно',
						'err'=>true
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление успешно',
						
					];
        		}
        		//теперь получаем objDesTravaux
        		$res=$_obj->getParent($objDevis);
        		if($res['result']!=1)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Не удалось получить родителя объекта Devis',
						'err'=>true
					];
					return false;
        		}
        		$objDeTravaux=$objDevis->getParent();
        		$objectUIDS['Objetdestravaux']=$objDeTravaux->getUid();
        		//теперь надо получить dateCreateion
        		$dataObjDeTravaux=HelperFunctions::getDataFromDB($objectUIDS['Objetdestravaux']);
                $objdeTravauxDateCreation=HelperFunctions::findValueFromDataUID(Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['objDeTravauxSpecAttr']['dateDeCreation']['uid'],$dataObjDeTravaux);
                $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Objetdestravaux, ''));
        		$dataParamObjDeTravauxDB=$_group->getAttributes($group);
                $dataParamObjDeTravaux=HelperFunctions::fillParamForFormData($dataParamObjDeTravauxDB,$configParamData,$copyRequest,['dateNow'=>$objdeTravauxDateCreation->getValue(),'user'=>$this->generateDoc->USER->getUid()]);
                $objDeTravaux=new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], '');
        		$objDeTravaux->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Objetdestravaux, ''));
        		$objDeTravaux->setAttributes($dataParamObjDeTravaux);
                //теперь делаем обновление его
                $this->generateDoc->dataLogs[]=
				[
					'time'=>date('d/m/Y H:i:s'),
					'text'=>'Обновляем объект с uid='.$objectUIDS['Objetdestravaux'],
					'data'=>
					[
						'uid'=>$objectUIDS['Objetdestravaux'],
						'obj'=>$objDeTravaux
					]
				];
        		$res=$_obj->addValues([$objDeTravaux],$this->generateDoc->USER);
        		if(empty($res['result']))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта objDetravaux не успешно',
						'err'=>true
					];
					return false;
        		}
        		$flagErr=false;
        		foreach ($res['result'] as  $valueAttr) 
        		{
        			if($valueAttr['result']!=1)
        			{
        				$flagErr=true;
        				break;
        			}
        		}
        		if(empty($res)||$flagErr)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта objDetravaux не успешно',
						'err'=>true
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление успешно'						
					];
        		}
        		//теперь необходимо удалить все facture и ah которые есть у объекта
        		$uidDel=[];
        		$allAdditionalBloksOprDB=$_obj->getChildrenByType($objDeTravaux,new \Dzeta\Core\Type\Type(Constans::CLS_UID_FACTURE, ''));        		
        		foreach ($allAdditionalBloksOprDB as $vo) 
        		{
        			$uidDel[]=$vo->getUid();
        		}
        		$allAdditionalBloksOprDB=$_obj->getChildrenByType($objDeTravaux,new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''));        		
        		foreach ($allAdditionalBloksOprDB as $vo) 
        		{
        			$uidDel[]=$vo->getUid();
        		}
        		if(!empty($uidDel))
        		{
        			$res=$_obj->removeObjs($uidDel,$this->generateDoc->USER);
        			if($res==1)
        			{
        				$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Успешно удалены дочерние объекты в кол-ве='.count($uidDel),
							'data'=>
							[
								'objs'=>implode(', ',$uidDel)
							]						
						];
        			}else
        			{
        				$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'err'=>true,
							'text'=>'Не удалены дочерние объекты',
							'data'=>
							[
								'objs'=>implode(', ',$uidDel)
							]							
						];
						return false;
        			}
        		}
        	}


        	//теперь блок PersoneMorale
        	$allPersonaleMoraleDB=$_obj->getChildrenByType(new \Dzeta\Core\Instance($objectUIDS['Devis'], ''),new \Dzeta\Core\Type\Type(Constans::CLS_UID_PERSONNE_PHYS, ''));
        	$uidDel=[];
        	foreach ($allPersonaleMoraleDB as $vo) 
    		{
    			$uidDel[]=$vo->getUid();
    		}
    		if(!empty($uidDel))
    		{
    			$res=$_obj->removeObjs($uidDel,$this->generateDoc->USER);
    			if($res==1)
    			{
    				$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Успешно удалены дочерние объекты PM в кол-ве='.count($uidDel),
						'data'=>
						[
							'objs'=>implode(', ',$uidDel)
						]						
					];
    			}else
    			{
    				$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Не удалены дочерние объекты PM',
						'data'=>
						[
							'objs'=>implode(', ',$uidDel)
						]							
					];
					return false;
    			}
    		}
    		$dataRequestPersoneMorale=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['key']);
    		if(!empty($dataRequestPersoneMorale))
    		{
    			$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_PERSONNE_PHYS, ''));
        		$dataParamPersoneMorale=$_group->getAttributes($group);
    			foreach ($dataRequestPersoneMorale as $valuePM) 
	    		{
	    			$personMorale_obj=new \Dzeta\Core\Instance('', '');
	    			$personMorale_obj->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_PERSONNE_PHYS, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Devis'], ''), '');
	    			$copyAttrPM=$dataParamPersoneMorale;
	    			if(isset($valuePM[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyJSON']]))
	    			{
	    				$jsData=json_decode($valuePM[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyJSON']],true);
	    				$persones=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyNomenre']);
	    				$revenue=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyRevenu']);
	    				if(!is_null($persones))
	    				{
	    					$valuePM['personnes']=$persones;
	    				}
	    				if(!is_null($revenue))
	    				{
	    					$valuePM['revenue']=preg_replace("/[^,.0-9]/", '',$revenue);
	    				}
	    			}
	    			$dataParamPM=HelperFunctions::fillParamForFormData($copyAttrPM,$configParamData,$valuePM,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
	    			$personMorale_obj->setAttributes($dataParamPM);
	    			$res=$_obj->checkUniqueAndAddWithValues($personMorale_obj,\Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
	    			if(empty($res['result']))
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Не удалось создать PM',
							'err'=>true,
							'data'=>
							[
								'res'=>$res
							]
						];
						return false;
	        		}
	        		$flagErr=false;
	        		foreach ($res['result'] as  $valueAttr) 
	        		{
	        			if($valueAttr['result']!=1)
	        			{
	        				$flagErr=true;
	        				break;
	        			}
	        		}
	        		if(empty($res)||$flagErr)
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Не удалось создать PM',
							'err'=>true,
							'data'=>
							[
								'res'=>$res
							]
						];
						return false;
	        		}else
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Объект PM создан успешно',	
							'data'=>
							[
								'uid'=>$personMorale_obj->getUid()
							]					
						];
	        		}
	    			
	    		}
    		}    		
        }else if($typeGenDoc=='facture')
        {
        	$objUid=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['keyObjectID']);
        	if(empty($objUid))
        	{
        		$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Отсутсвует uid объекта в запросе'
					
				];
				return false;
        	}
        	if($typeModeCreateDoc=='create')
        	{
        		$objectUIDS['Devis']=$objUid;
        		$objDevis=new \Dzeta\Core\Instance($objUid, '');
        		$objDevis->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DEVIS, ''));
        		$res=$_obj->getParent($objDevis);
        		if($res['result']!=1)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Ошибка при получении родителя'						
					];
					return false;
        		}        		
        		$objectUIDS['Objetdestravaux']=$objDevis->getParent()->getUid();
        	}else
        	{
        		$objectUIDS['Facture']=$objUid;
        		$objFacture=new \Dzeta\Core\Instance($objUid, '');
        		$objFacture->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_FACTURE, ''));
        		$res=$_obj->getParent($objFacture);
        		if($res['result']!=1)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Ошибка при получении родителя у Facture'						
					];
					return false;
        		}
        		$objectUIDS['Objetdestravaux']=$objFacture->getParent()->getUid();
        	}
        	//теперь надо удалить AH
        	$allAdditionalBloksOprDB=$_obj->getChildrenByType(new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], ''),new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''));        		
    		foreach ($allAdditionalBloksOprDB as $vo) 
    		{
    			$uidDel[]=$vo->getUid();
    		}
    		if(!empty($uidDel))
    		{
    			$res=$_obj->removeObjs($uidDel,$this->generateDoc->USER);
    			if($res==1)
    			{
    				$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Успешно удалены дочерние объекты в кол-ве='.count($uidDel),
						'data'=>
						[
							'objs'=>implode(', ',$uidDel)
						]						
					];
    			}else
    			{
    				$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Не удалены дочерние объекты',
						'data'=>
						[
							'objs'=>implode(', ',$uidDel)
						]							
					];
					return false;
    			}
    		}
    		//теперь надо получить numDevis
    		$data=HelperFunctions::getDataFromDB($objectUIDS['Objetdestravaux']);
			$numDevisUser=HelperFunctions::findValueFromDataUID(Constans::CLS_UID_BaseNumber,$data);
			if(empty($numDevisUser))
			{
				$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Ошибка при получении BaseNumber'						
				];
				return false;
			}else
			{
				$numDevisUser=$numDevisUser->getValue();
			}
			$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_FACTURE, ''));
    		$dataParamFactureDB=$_group->getAttributes($group);
    		
			//теперь создаем или редактируем фактуры
			if($typeModeCreateDoc=='create')
        	{
        		$dataParamFacture=HelperFunctions::fillParamForFormData($dataParamFactureDB,$configParamData,$copyRequest,
        			['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$numDevisUser,'user'=>$this->generateDoc->USER->getUid()]);
        		$objFacture = new \Dzeta\Core\Instance('', '');
        		$objFacture->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_FACTURE, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], ''), '');
        		$objFacture->setAttributes($dataParamFacture);
        		$res=$_obj->checkUniqueAndAddWithValues($objFacture, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
        		if(empty($objFacture->getUid()))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Не удалось создать Facture',
						'data'=>
						[
							'res'=>$res
						]
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),						
						'text'=>'Удалось создать Facture',
						'data'=>
						[
							'uid'=>$objFacture->getUid()
						]
					];
        		}
        		$objectUIDS['Facture']=$objFacture->getUid();
        	}else
        	{
        		$dataParamFacture=HelperFunctions::fillParamForFormData($dataParamFactureDB,$configParamData,$copyRequest,
        			['dateNow'=>null,'user'=>$this->generateDoc->USER->getUid()]);
        		$objFacture = new \Dzeta\Core\Instance($objectUIDS['Facture'], '');
        		$objFacture->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_FACTURE, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], ''), '');
        		$objFacture->setAttributes($dataParamFacture);
        		$res=$_obj->addValues([$objFacture],$this->generateDoc->USER);
				
        		if(empty($res['result']))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта Facture не успешно',
						'err'=>true
					];
					return false;
        		}
        		$flagErr=false;
        		foreach ($res['result'] as  $valueAttr) 
        		{
        			if($valueAttr['result']!=1)
        			{
        				$flagErr=true;
        				break;
        			}
        		}
        		if(empty($res)||$flagErr)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта Facture не успешно',
						'err'=>true,
						'data'=>
						[
							'uid'=>$objFacture->getUid()
						]
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление успешно',
						'data'=>
						[
							'uid'=>$objFacture->getUid()
						]
					];
        		}
        	}
        }else if($typeGenDoc=='ah')
        {
        	//тут делаем объекты ObjResult
        	$objUid=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['keyObjectID']);
        	if(empty($objUid))
        	{
        		$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Отсутсвует uid объекта в запросе'
					
				];
				return false;
        	}
        	$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
            $group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''));
            $dataParamAHDB = $_group->getAttributes($group);
        	if($typeModeCreateDoc=='create')
        	{
        		$objectUIDS['Facture']=$objUid;
        		$objFacture=new \Dzeta\Core\Instance($objUid, '');
        		$objFacture->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_FACTURE, ''));
        		$res=$_obj->getParent($objFacture);
        		if($res['result']!=1)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Ошибка при получении родителя'						
					];
					return false;
        		}        		
        		$objectUIDS['Objetdestravaux']=$objFacture->getParent()->getUid();
        		$dataParamAH=HelperFunctions::fillParamForFormData($dataParamAHDB,$configParamData,$copyRequest,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
        		$objAH = new \Dzeta\Core\Instance('', '');
        		$objAH->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], ''), '');
        		$objAH->setAttributes($dataParamAH);
        		$res=$_obj->checkUniqueAndAddWithValues($objAH, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
        		if(empty($objAH->getUid()))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Не удалось создать AH',
						'data'=>
						[
							'res'=>$res
						]
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),						
						'text'=>'Удалось создать AH',
						'data'=>
						[
							'uid'=>$objAH->getUid()
						]
					];
        		}
        		$objectUIDS['AH']=$objAH->getUid();
        	}else
        	{
        		$objectUIDS['AH']=$objUid;
        		$objAH=new \Dzeta\Core\Instance($objUid, '');
        		$objAH->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''));
        		$res=$_obj->getParent($objAH);
        		if($res['result']!=1)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Ошибка при получении родителя'						
					];
					return false;
        		}        		
        		$objectUIDS['Objetdestravaux']=$objAH->getParent()->getUid();

        		$dataParamAH=HelperFunctions::fillParamForFormData($dataParamAHDB,$configParamData,$copyRequest,['dateNow'=>null,'user'=>$this->generateDoc->USER->getUid()]);
        		$objAH = new \Dzeta\Core\Instance($objectUIDS['AH'], '');
        		$objAH->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], ''), '');
        		$objAH->setAttributes($dataParamAH);
        		$res=$_obj->addValues([$objAH],$this->generateDoc->USER);
				
        		if(empty($res['result']))
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта AH не успешно',
						'err'=>true
					];
					return false;
        		}
        		$flagErr=false;
        		foreach ($res['result'] as  $valueAttr) 
        		{
        			if($valueAttr['result']!=1)
        			{
        				$flagErr=true;
        				break;
        			}
        		}
        		if(empty($res)||$flagErr)
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление объекта AH не успешно',
						'err'=>true,
						'data'=>
						[
							'uid'=>$objAH->getUid()
						]
					];
					return false;
        		}else
        		{
        			$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Обновление успешно',
						'data'=>
						[
							'uid'=>$objAH->getUid()
						]
					];
        		}
        	}
        }else if($typeGenDoc=='devisah')
        {
        	$valPersone=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['writeDefBenificare']['key']);        	
        	$nDevis=$this->getLastDevis();
    		if(empty($nDevis))
    		{
    			return false;
    		}
    		//создаем Верх
    		$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Objetdestravaux, ''));
    		$dataParamObjDeTravauxDB=$_group->getAttributes($group);
    		$dataParamObjDeTravaux=HelperFunctions::fillParamForFormData($dataParamObjDeTravauxDB,$configParamData,$copyRequest,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis['globalDevis'].'-'.$nDevis['userDevis'],'user'=>$this->generateDoc->USER->getUid()]);
    		$objDeTravaux=new \Dzeta\Core\Instance('', '');
    		$objDeTravaux->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Objetdestravaux, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
    		$objDeTravaux->setAttributes($dataParamObjDeTravaux);
    		$res=$_obj->checkUniqueAndAddWithValues($objDeTravaux, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
    		if(empty($objDeTravaux->getUid()))
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Не удалось создать obj De travaux',
					'data'=>
					[
						'res'=>$res
					]
				];
				return false;
    		}else
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),						
					'text'=>'Удалось создать obj De travaux',
					'data'=>
					[
						'uid'=>$objDeTravaux->getUid()
					]
				];
    		}
    		$objectUIDS['Objetdestravaux']=$objDeTravaux->getUid();
    		//создаем Девис
    		$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DEVIS, ''));
    		$dataParamDevisDB=$_group->getAttributes($group);
    		$dataParamDevis=HelperFunctions::fillParamForFormData($dataParamDevisDB,$configParamData,$copyRequest,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis['globalDevis'].'-'.$nDevis['userDevis'],'user'=>$this->generateDoc->USER->getUid()]);
    		$objDevis=new \Dzeta\Core\Instance('', '');
    		$objDevis->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DEVIS, ''))->setParent($objDeTravaux);
    		$objDevis->setAttributes($dataParamDevis);
    		$_obj->checkUniqueAndAddWithValues($objDevis, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
    		if(empty($objDevis->getUid()))
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Не удалось создать Devis'
				];
				return false;
    		}else
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),						
					'text'=>'Удалось создать Devis',
					'data'=>
					[
						'uid'=>$objDevis->getUid()
					]
				];
    		}
    		$objectUIDS['Devis']=$objDevis->getUid();
    		//создаем Facture
    		$dataParamFacture=HelperFunctions::fillParamForFormData($dataParamFactureDB,$configParamData,$copyRequest,
    			['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis['globalDevis'].'-'.$nDevis['userDevis'],'user'=>$this->generateDoc->USER->getUid()]);
    		$objFacture = new \Dzeta\Core\Instance('', '');
    		$objFacture->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_FACTURE, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], ''), '');
    		$objFacture->setAttributes($dataParamFacture);
    		$res=$_obj->checkUniqueAndAddWithValues($objFacture, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
    		if(empty($objFacture->getUid()))
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Не удалось создать Facture',
					'data'=>
					[
						'res'=>$res
					]
				];
				return false;
    		}else
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),						
					'text'=>'Удалось создать Facture',
					'data'=>
					[
						'uid'=>$objFacture->getUid()
					]
				];
    		}
    		$objectUIDS['Facture']=$objFacture->getUid();
    		//создаем AH
    		$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
            $group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''));
            $dataParamAHDB = $_group->getAttributes($group);
            $dataParamAH=HelperFunctions::fillParamForFormData($dataParamAHDB,$configParamData,$copyRequest,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
    		$objAH = new \Dzeta\Core\Instance('', '');
    		$objAH->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_AH, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Objetdestravaux'], ''), '');
    		$objAH->setAttributes($dataParamAH);
    		$res=$_obj->checkUniqueAndAddWithValues($objAH, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
    		if(empty($objAH->getUid()))
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Не удалось создать AH',
					'data'=>
					[
						'res'=>$res
					]
				];
				return false;
    		}else
    		{
    			$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),						
					'text'=>'Удалось создать AH',
					'data'=>
					[
						'uid'=>$objAH->getUid()
					]
				];
    		}
    		$objectUIDS['AH']=$objAH->getUid();
    		//создаем PM
    		$dataRequestPersoneMorale=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['key']);
    		if(!empty($dataRequestPersoneMorale))
    		{
    			$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        		$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_PERSONNE_PHYS, ''));
        		$dataParamPersoneMorale=$_group->getAttributes($group);
    			foreach ($dataRequestPersoneMorale as $valuePM) 
	    		{
	    			$personMorale_obj=new \Dzeta\Core\Instance('', '');
	    			$personMorale_obj->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_PERSONNE_PHYS, ''))->setParent(new \Dzeta\Core\Instance($objectUIDS['Devis'], ''), '');
	    			$copyAttrPM=$dataParamPersoneMorale;
	    			if(isset($valuePM[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyJSON']]))
	    			{
	    				$jsData=json_decode($valuePM[Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyJSON']],true);
	    				$persones=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyNomenre']);
	    				$revenue=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['personeMorale']['keyRevenu']);
	    				if(!is_null($persones))
	    				{
	    					$valuePM['personnes']=$persones;
	    				}
	    				if(!is_null($revenue))
	    				{
	    					$valuePM['revenue']=preg_replace("/[^,.0-9]/", '',$revenue);
	    				}
	    			}
	    			$dataParamPM=HelperFunctions::fillParamForFormData($copyAttrPM,$configParamData,$valuePM,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
	    			$personMorale_obj->setAttributes($dataParamPM);
	    			$res=$_obj->checkUniqueAndAddWithValues($personMorale_obj,\Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
	    			if(empty($res['result']))
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Не удалось создать PM',
							'err'=>true,
							'data'=>
							[
								'res'=>$res
							]
						];
						return false;
	        		}
	        		$flagErr=false;
	        		foreach ($res['result'] as  $valueAttr) 
	        		{
	        			if($valueAttr['result']!=1)
	        			{
	        				$flagErr=true;
	        				break;
	        			}
	        		}
	        		if(empty($res)||$flagErr)
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Не удалось создать PM',
							'err'=>true,
							'data'=>
							[
								'res'=>$res
							]
						];
						return false;
	        		}else
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Объект PM создан успешно',	
							'data'=>
							[
								'uid'=>$personMorale_obj->getUid()
							]					
						];
	        		}	    			
	    		}
    		}
        }

        $copyOperations=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['operations']['key']);
        $copyOperations=empty($copyOperations)? []:$copyOperations;
        //получаем параметры из бд, которые нужны
        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    	$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION, ''));
    	$dataParamOPERATION = $_group->getAttributes($group);

    	$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    	$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION_BLOCKS, ''));
    	$dataParamOperationBlocks = $_group->getAttributes($group);

    	$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    	$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION_ELEMENTS, ''));
    	$dataParamOperationBlocksElem = $_group->getAttributes($group);

    	$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    	$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_MATERIAL_OPERATION, ''));
    	$dataParamOperationIsolant = $_group->getAttributes($group);

    	$group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
    	$group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DIAMETRE, ''));
    	$dataParamIsolantBlocks = $_group->getAttributes($group);

    	//теперь выделяем родитеелй наших операций
    	$parentOperation=[];
    	if($typeGenDoc=='devis')
    	{
    		$parentOperation[]=$objectUIDS['Devis'];
    	}else if($typeGenDoc=='facture')
    	{
    		$parentOperation[]=$objectUIDS['Facture'];
    	}else if($typeGenDoc=='ah')
    	{
    		$parentOperation[]=$objectUIDS['AH'];
    	}else if($typeGenDoc=='devisah')
    	{
    		$parentOperation[]=$objectUIDS['Devis'];
    		$parentOperation[]=$objectUIDS['Facture'];
    		$parentOperation[]=$objectUIDS['AH'];
    	}

    	foreach ($parentOperation as $numKeyParentOperation=> $valueParent) 
    	{
    		$objParent=new \Dzeta\Core\Instance($valueParent, '');
    		$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Работаем с операциями у объекта с uid='.$valueParent,
				'data'=>
				[
					'uid'=>$valueParent
				]
			];
    		foreach ($copyOperations as $indexOper => $valueOper) 
	        {
	        	if(!empty($valueOper['linked_obj'])&&($numKeyParentOperation!=1&&$typeGenDoc!='factureah'))
	        	{
	        		//значит редактируем
	        		$operObj=new \Dzeta\Core\Instance($valueOper['linked_obj'], '');
	        		$operObj->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION, ''));
	        		$copyAttrDB=$dataParamOPERATION;
					$copyAttr=HelperFunctions::fillParamForFormData($copyAttrDB,$configParamData,$valueOper,['dateNow'=>null,'user'=>$this->generateDoc->USER->getUid()]);
					$operObj->setAttributes($copyAttr);
	        		$operObj->setParent($objParent);
	        		$res=$_obj->addValues([$operObj],$this->generateDoc->USER);	
                   			
	        		if(empty($res['result']))
	        		{                        
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Обновление операции не успешно',
							'err'=>true
						];
						return false;
	        		}
	        		$flagErr=false;
	        		foreach ($res['result'] as  $valueAttr) 
	        		{
	        			if($valueAttr['result']!=1)
	        			{
	        				$flagErr=true;
	        				break;
	        			}
	        		}
	        		if(empty($res)||$flagErr)
	        		{                        
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Обновление операции не успешно',
							'err'=>true
						];
						return false;
	        		}else
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Обновление операции успешно',
							
						];
	        		}	
	        	}else
	        	{
	        		$operObj=new \Dzeta\Core\Instance('', '');
	        		$operObj->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION, ''));
	        		$copyAttrDB=$dataParamOPERATION;
					$copyAttr=HelperFunctions::fillParamForFormData($copyAttrDB,$configParamData,$valueOper,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
					$operObj->setAttributes($copyAttr);
	        		$operObj->setParent($objParent);
	        		$res=$_obj->checkUniqueAndAddWithValues($operObj, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
	        		if(empty($operObj->getUid()))
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'err'=>true,
							'text'=>'Не удалось создать операцию',
							'data'=>
							[
								'res'=>$res
							]
						];
						return false;
	        		}else
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),						
							'text'=>'Удалось создать операцию',
							'data'=>
							[
								'uid'=>$operObj->getUid()
							]
						];
	        		}
	        	}

	        	//теперь надо удалить дочерние блоки Аддитионал блокс
	        	$uidDel=[];
        		$allAdditionalBloksOprDB=$_obj->getChildrenByType($operObj,new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION_BLOCKS, ''));        		
        		foreach ($allAdditionalBloksOprDB as $vo) 
        		{
        			$uidDel[]=$vo->getUid();
        		}
        		if(!empty($uidDel))
        		{
        			$res=$_obj->removeObjs($uidDel,$this->generateDoc->USER);
        			if($res==1)
        			{
        				$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Успешно удалены дочерние объекты в кол-ве='.count($uidDel),
							'data'=>
							[
								'objs'=>implode(', ',$uidDel)
							]						
						];
        			}else
        			{
        				$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'err'=>true,
							'text'=>'Не удалены дочерние объекты',
							'data'=>
							[
								'objs'=>implode(', ',$uidDel)
							]							
						];
						return false;
        			}
        		}
        		//теперь надо создать
        		$copyAdditionalBloks=HelperFunctions::findValueConfigRequest($valueOper,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['operationsAB']['key']);
        		$copyAdditionalBloks=empty($copyAdditionalBloks)? []:$copyAdditionalBloks;
        		foreach ($copyAdditionalBloks as $keyAB => $valueAB) 
        		{
        			$operBloks=new \Dzeta\Core\Instance('', '');
        			$operBloks->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION_BLOCKS, ''))->setParent($operObj, '');
        			$copyAttrDB=$dataParamOperationBlocks;
        			$copyAttr=HelperFunctions::fillParamForFormData($copyAttrDB ,$configParamData,$valueAB,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
        			$operBloks->setAttributes($copyAttr);
        			$res=$_obj->checkUniqueAndAddWithValues($operBloks, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
	        		if(empty($operBloks->getUid()))
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'err'=>true,
							'text'=>'Не удалось создать Operation-blocks',
							'data'=>
							[
								'res'=>$res
							]
						];
						return false;
	        		}else
	        		{
	        			$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),						
							'text'=>'Удалось создать Operation-blocks',
							'data'=>
							[
								'uid'=>$operBloks->getUid()
							]
						];
	        		}
	        		$copyAdditionalBloksData=HelperFunctions::findValueConfigRequest($valueAB,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['operationsABdata']['key']);
        			$copyAdditionalBloksData=empty($copyAdditionalBloksData)? []:$copyAdditionalBloksData;
        			foreach ($copyAdditionalBloksData as  $valueAdditionalBloksData) 
        			{
        				$operBlockElem=new \Dzeta\Core\Instance('', '');
	        			$operBlockElem->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_OPERATION_ELEMENTS, ''))->setParent($operBloks, '');
	        			$copyAttrDB=$dataParamOperationBlocksElem;
	        			$copyAttr=HelperFunctions::fillParamForFormData($copyAttrDB,$configParamData,$valueAdditionalBloksData,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
	        			$operBlockElem->setAttributes($copyAttr);
	        			$res=$_obj->checkUniqueAndAddWithValues($operBlockElem, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
		        		if(empty($operBlockElem->getUid()))
		        		{
		        			$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'err'=>true,
								'text'=>'Не удалось создать в дополнительном блоке операции, дополнительный блок',
								'data'=>
								[
									'res'=>$res
								]
							];
							return false;
		        		}else
		        		{
		        			$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),						
								'text'=>'Удалось создать в дополнительном блоке операции, дополнительный блок',
								'data'=>
								[
									'uid'=>$operBlockElem->getUid()
								]
							];
		        		}
        			}
        		}

        		//теперь изолянты
        		$copyIsolantBloks=HelperFunctions::findValueConfigRequest($valueOper,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['operIsolantBloks']['key']);
        		$copyIsolantBloks=empty($copyIsolantBloks)? []:$copyIsolantBloks;
               
                foreach ($copyIsolantBloks as $keyIsol => $valueIsolant) 
        		{
        			if(!empty($valueIsolant['linked_obj']))
        			{
        				$isol=new \Dzeta\Core\Instance($valueIsolant['linked_obj'], '');
	        			$isol->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_MATERIAL_OPERATION, ''));
	        			$isol->setParent($operObj);
	        			$copyAttrDB=$dataParamOperationIsolant;
	        			$copyAttr=HelperFunctions::fillParamForFormData($copyAttrDB,$configParamData,$valueIsolant,['dateNow'=>null,'user'=>$this->generateDoc->USER->getUid()]);
	        			$isol->setAttributes($copyAttr);
	        			$res=$_obj->addValues([$isol],$this->generateDoc->USER);				
		        		if(empty($res['result']))
		        		{
		        			$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'text'=>'Обновление isol не успешно',
								'err'=>true
							];
							return false;
		        		}
		        		$flagErr=false;
		        		foreach ($res['result'] as  $valueAttr) 
		        		{
		        			if($valueAttr['result']!=1)
		        			{
		        				$flagErr=true;
		        				break;
		        			}
		        		}
		        		if(empty($res)||$flagErr)
		        		{
		        			$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'text'=>'Обновление isol не успешно',
								'err'=>true
							];
							return false;
		        		}else
		        		{
		        			$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'text'=>'Обновление isol успешно',
								
							];
		        		}	
        			}else
        			{
        				$isol=new \Dzeta\Core\Instance('', '');
	        			$isol->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_MATERIAL_OPERATION, ''));
	        			$isol->setParent($operObj);
	        			$copyAttrDB=$dataParamOperationIsolant;
	        			$copyAttr=HelperFunctions::fillParamForFormData($copyAttrDB,$configParamData,$valueIsolant,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
	        			$isol->setAttributes($copyAttr);
        				$res=$_obj->checkUniqueAndAddWithValues($isol, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
		        		if(empty($isol->getUid()))
		        		{
		        			$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'err'=>true,
								'text'=>'Не удалось создать изолянт у операции',
								'data'=>
								[
									'res'=>$res
								]
							];
							return false;
		        		}else
		        		{
		        			$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),						
								'text'=>'Удалось создать изолянт у операции',
								'data'=>
								[
									'uid'=>$isol->getUid()
								]
							];
		        		}
        			}
        			
        			
        			//удаляем дочерние Diametre у изолянтов
        			$allAdditionalBloksIsolDB=$_obj->getChildrenByType($isol,new \Dzeta\Core\Type\Type(Constans::CLS_UID_DIAMETRE, ''));
		        	$uidDel=[];
		        	foreach ($allAdditionalBloksIsolDB as $vo) 
		    		{
		    			$uidDel[]=$vo->getUid();
		    		}
		    		if(!empty($uidDel))
		    		{
		    			$res=$_obj->removeObjs($uidDel,$this->generateDoc->USER);
		    			if($res==1)
		    			{
		    				$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'text'=>'Успешно удалены дочерние объекты Diametre в кол-ве='.count($uidDel),
								'data'=>
								[
									'objs'=>implode(', ',$uidDel)
								]						
							];
		    			}else
		    			{
		    				$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'err'=>true,
								'text'=>'Не удалены дочерние объекты Diametre',
								'data'=>
								[
									'objs'=>implode(', ',$uidDel)
								]							
							];
							return false;
		    			}
		    		}
                    //operIsolantDiametre
                    $copyIsolantDiametre=HelperFunctions::findValueConfigRequest($valueIsolant,Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['operIsolantDiametre']['key']);
                    $copyIsolantDiametre=empty($copyIsolantDiametre)? []:$copyIsolantDiametre;
                    foreach ($copyIsolantDiametre as $keyDiametre => $valueDiametre) 
                    {
                        $isolBlocks=new \Dzeta\Core\Instance('', '');
                        $isolBlocks->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_DIAMETRE, ''))->setParent($isol, '');
                        $copyAttrDB=$dataParamIsolantBlocks;
                        $copyAttr=HelperFunctions::fillParamForFormData($copyAttrDB,$configParamData,$valueDiametre,['dateNow'=>$dateNow,'user'=>$this->generateDoc->USER->getUid()]);
                        $isolBlocks->setAttributes($copyAttr);
                        $res=$_obj->checkUniqueAndAddWithValues($isolBlocks, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
                        if(empty($isolBlocks->getUid()))
                        {
                            $this->generateDoc->dataLogs[]=[
                                'time'=>date('d/m/Y H:i:s'),
                                'err'=>true,
                                'text'=>'Не удалось создать diamter у изолянта',
                                'data'=>
                                [
                                    'res'=>$res
                                ]
                            ];
                            return false;
                        }else
                        {
                            $this->generateDoc->dataLogs[]=[
                                'time'=>date('d/m/Y H:i:s'),                        
                                'text'=>'Удалось создать diamter у изолянта',
                                'data'=>
                                [
                                    'uid'=>$isolBlocks->getUid()
                                ]
                            ];
                        }
                    }
        		}
	        }
    	}
        
        return $objectUIDS;
	}

	public static function writeDefaultBenificiare($typeBenificiare,$generateDoc)
	{		
		if(empty($typeBenificiare))
		{
			$generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось записать тип пользователя по умолчанию, по причине отсутсвия типа',
				'warn'=>true
			];
			return false;
		}
		$_obj=new \Dzeta\Models\Obj();
		$uidUser=$generateDoc->USER->getUid();
        $uidAttr=Constans::OBJ_DE_TRAVAUX_CONFIG_SETT['writeDefBenificare']['attr'];
		$attr = new \Dzeta\Core\Type\Attribute($uidAttr['uid'], '',$uidAttr['type'], false, false);
		$attr->setValue(['value'=>$typeBenificiare]);
		$objUs=new \Dzeta\Core\Instance($uidUser, '');
		$objUs->setAttributes([$attr]);
		$res=$_obj->addValues([$objUs],$generateDoc->USER);       

		if(isset($res['result'][$uidAttr['uid']]))
		{
            $resu=$res['result'][$uidAttr['uid']];
			if(isset($resu['result'])&&$resu['result']==1)
			{
				return true;
			}
		}
		$generateDoc->dataLogs[]=
		[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Не удалось записать тип пользователя по умолчанию d БД',
			'warn'=>true,
			'data'=>
			[
				'result'=>$res,
				'obj'=>$objUs
			]
		];
		return false;
	}

	private function getLastDevis()
	{
		$_obj=new \Dzeta\Models\Obj();
		$uidObj=Constans::GLOBAL_SETT_CONFIG['№devis']['obj'];
		$obj=new \Dzeta\Core\Instance($uidObj, '',
			new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['№devis']['type'], ''));
		$data=HelperFunctions::getDataFromDB($uidObj);
		$numDevis=HelperFunctions::findValueFromDataUID(Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['uid'],$data);
		if(!is_null($numDevis))
		{
			$numDevis=$numDevis->getValue();	
		}else
		{			
			$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось получить глобальный LastDevis',
				'err'=>true
			];
			return false;
		}
		$numDevis++;
		//теперь делаем апдейт +1
		$attr = new \Dzeta\Core\Type\Attribute(Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['uid'], '',Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['datatype'], false, false);
		$attr->setValue(['value'=>$numDevis]);
		$obj->setAttributes([$attr]);
		$res=$_obj->addValues([$obj],$this->generateDoc->USER);
		$flagErr=true;
		if(empty($res['result']))
		{
			$this->generateDoc->dataLogs[]=[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось записать LastDevis',
				'err'=>true,
				'data'=>
				[
					'res'=>$res
				]
			];
			return false;
		}
		$res=$res['result'];
		
		if(isset($res[Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['uid']]))
		{
			if(isset($res[Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['uid']]['result'])&&$res[Constans::GLOBAL_SETT_CONFIG['№devis']['attr']['uid']]['result']==1)
			{
				$flagErr=false;
			}
		}
		if($flagErr)
		{			
			$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось записать LastDevis',
				'err'=>true
			];
			return false;
		}
		//теперь надо обновить у юзера в поле
		$uidUser=$this->generateDoc->USER->getUid();
		$data=HelperFunctions::getDataFromDB($uidUser);
		$numDevisUser=HelperFunctions::findValueFromDataUID(Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['uid'],$data);
		if(!is_null($numDevisUser))
		{
			$numDevisUser=$numDevisUser->getValue();
		}else
		{
			$numDevisUser=0;
		}
		$numDevisUser++;
		$obj=new \Dzeta\Core\Instance($uidUser, '',
			new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['userDevis']['type'], ''));
		$attr = new \Dzeta\Core\Type\Attribute(Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['uid'], '',Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['datatype'], false, false);
		$attr->setValue(['value'=>$numDevisUser]);
		$obj->setAttributes([$attr]);
		$res=$_obj->addValues([$obj],$this->generateDoc->USER);
		$flagErr=true;
		if(empty($res['result']))
		{
			$this->generateDoc->dataLogs[]=[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось записать LastDevis user',
				'err'=>true,
				'data'=>
				[
					'res'=>$res
				]
			];
			return false;
		}
		$res=$res['result'];
		if(isset($res[Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['uid']]))
		{
			if(isset($res[Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['uid']]['result'])&&$res[Constans::GLOBAL_SETT_CONFIG['userDevis']['attr']['uid']]['result']==1)
			{
				$flagErr=false;
			}
		}
		if($flagErr)
		{			
			$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось записать LastDevis user',
				'err'=>true
			];
			return false;
		}
		$this->generateDoc->dataLogs[]=
		[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Успешно записаны Devis global and user',
			'data'=>
			[
				'user'=>$numDevisUser,
				'global'=>$numDevis
			]
		];
		
		return ['globalDevis'=>$numDevis,'userDevis'=>$numDevisUser];
	}
}