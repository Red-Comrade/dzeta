<?php

namespace generateDocEnemat;

class objResult
{
	private $copyRequest=[];
	private $generateDoc=null;


	public function __construct($requestData,$objGenDoc)
	{
		$this->copyRequest=$requestData;
		$this->generateDoc=$objGenDoc;
	}

	public function init()
	{
		$copyRequest=$this->copyRequest;
		$dateNow=date('d/m/Y H:i:s');
		$this->generateDoc->dataLogs[]=
		[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Начинаем работать с веткой Result'
		];
		$_group = new \Dzeta\Models\AttrGroup();
        $_obj=new \Dzeta\Models\Obj();
        if(!file_exists(Constans::PATH_CONFIG_SETT_OBJ_REQUEST_RESULTS))
        {
        	$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Отсутсвует файл с настройками Results',
				'err'=>true
			];
        	return false;
        }
        $configParamData=json_decode(file_get_contents(Constans::PATH_CONFIG_SETT_OBJ_REQUEST_RESULTS),true);

        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Results, ''));
        $dataParamResultsdb = $_group->getAttributes($group);

        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Operations, ''));
        $dataParamOperationsdb = $_group->getAttributes($group);

        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Isolant_operation, ''));
        $dataParamIsolantdb = $_group->getAttributes($group);


        $dataParamResults=HelperFunctions::fillParamForFormData($dataParamResultsdb,$configParamData,$copyRequest,['dateNow'=>$dateNow]);

        $objResults = new \Dzeta\Core\Instance('', '');
    	$objResults->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Results, ''))
    			 ->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
    	$objResults->setAttributes($dataParamResults);


    	$res=$_obj->checkUniqueAndAddWithValues($objResults, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
		if(empty($objResults->getUid()))
		{
			$this->generateDoc->dataLogs[]=[
				'time'=>date('d/m/Y H:i:s'),
				'err'=>true,
				'text'=>'Не удалось создать Results',
				'data'=>
				[
					'res'=>$res
				]
			];
			return false;
		}else
		{
			$this->generateDoc->dataLogs[]=[
				'time'=>date('d/m/Y H:i:s'),						
				'text'=>'Удалось создать Results',
				'data'=>
				[
					'uid'=>$objResults->getUid()
				]
			];
		}
		//теперь идем по операциям
		$operationsArr=[];
		$copyOperations=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_RESULT_CONFIG_SETT['operations']['key']);
        $copyOperations=empty($copyOperations)? []:$copyOperations;

        $benPrevisitte=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_RESULT_CONFIG_SETT['prevesite']['keyMain']);
        foreach ($copyOperations as $valueOperation) 
        {
        	$valueOperation[Constans::OBJ_RESULT_CONFIG_SETT['prevesite']['keyDop']]=$benPrevisitte;

        	$objOper = new \Dzeta\Core\Instance('', '');
    		$objOper->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Operations, ''))
    			 ->setParent($objResults);
    		$copyAttr=$dataParamOperationsdb;
    		$dataParamOper=HelperFunctions::fillParamForFormData($copyAttr,$configParamData,$valueOperation,['dateNow'=>$dateNow]);

        	$objOper->setAttributes($dataParamOper);

        	$res=$_obj->checkUniqueAndAddWithValues($objOper, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
			if(empty($objOper->getUid()))
			{
				$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Не удалось создать operations',
					'data'=>
					[
						'res'=>$res
					]
				];
				return false;
			}else
			{
				$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),						
					'text'=>'Удалось создать operations',
					'data'=>
					[
						'uid'=>$objOper->getUid()
					]
				];
			}
			$copyIsolants=HelperFunctions::findValueConfigRequest($valueOperation,Constans::OBJ_RESULT_CONFIG_SETT['operIsolant']['key']);
        	$copyIsolants=empty($copyIsolants)? []:$copyIsolants;
        	$arrIsolants=[];
        	foreach ($copyIsolants as $valueIsolant) 
        	{
        		$objIsolant = new \Dzeta\Core\Instance('', '');
	    		$objIsolant->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Isolant_operation, ''))
	    			 ->setParent($objOper);
	    		$copyAttr=$dataParamIsolantdb;

	    		$dataParamIsolant=HelperFunctions::fillParamForFormData($copyAttr,$configParamData,$valueIsolant,['dateNow'=>$dateNow]);
        		$objIsolant->setAttributes($dataParamIsolant);
        		$res=$_obj->checkUniqueAndAddWithValues($objIsolant, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
				if(empty($objIsolant->getUid()))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'err'=>true,
						'text'=>'Не удалось создать isolant',
						'data'=>
						[
							'res'=>$res
						]
					];
					return false;
				}else
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),						
						'text'=>'Удалось создать isolant',
						'data'=>
						[
							'uid'=>$objIsolant->getUid()
						]
					];
				}
				$arrIsolants[]=$objIsolant->getUid();
        	}
        	$operationsArr[]=['isolants'=>$arrIsolants,'operation'=>$objOper->getUid()];
        }
        $this->generateDoc->dataLogs[]=
		[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Закончили работать с веткой Result',
			'data'=>
			[
				'data'=>['mainResult'=>$objResults->getUid(),'operations'=>$operationsArr]
			]
		];
        return ['mainResult'=>$objResults->getUid(),'operations'=>$operationsArr];
	}
}