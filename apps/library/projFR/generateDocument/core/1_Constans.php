<?php

namespace generateDocEnemat;
class Constans
{
	const PATH_CONFIG_SETT_METKA=BASE_PATH.'/config/dop/DataEnemat/new/settEnemat.json';
	const PATH_CONFIG_SETT_OBJ_REQUEST=BASE_PATH.'/config/dop/DataEnemat/new/configDataSpecEnemat.json';
	const PATH_CONFIG_SETT_OBJ_REQUEST_RESULTS=BASE_PATH.'/config/dop/DataEnemat/new/configDataTypeResults.json';
	const PATH_CONFIG_SETT_OBJ_REQUEST_RESULTSEMMY=BASE_PATH.'/config/dop/DataEnemat/new//configDataTypeResultsEMMY.json';
	const PATH_CONFIG_SPEC_TABLE_BENEFICIARE=BASE_PATH.'/config/dop/DataEnemat/new/configTablejson.docx';
	const PATH_FILE_EXCEL_CONFIG=BASE_PATH.'/config/dop/DataEnemat/new/googleExcel.txt';
	const PATH_FOLDER_GENERATE=BASE_PATH.'/ufiles/fillFileFr1/';
	const PATH_FOLDER_EXCEL='/excel';
	const PATH_CONF_GLOBAL=BASE_PATH . '/config/dop/DataEnemat/enematGlobal.json';

	//урл для генерации excel
	const PATH_URL_GENERATE_EXCEL='https://docs.google.com/spreadsheets/d/1MFPyZiVQGh8hS33Ka3LCfxEWjVitFcbi9Bc97xeNNxg/edit#gid=0';


	const DELIMETER_NEW_BRAKE_WORD='</w:t><w:br/><w:t xml:space="preserve">';

	//uid классов и параметров
	//uid блоков классов Objet des travaux
	const CLS_UID_Objetdestravaux	='5086f503-d8b2-4a5b-a3d9-e5abc7ccb728';
	const CLS_UID_DEVIS				='5a6364ee-e0de-4772-983f-9f992d57a8fb';
	const CLS_UID_FACTURE			='cc056b7b-b1ba-45b0-a7e3-446bd321abf9';
	const CLS_UID_AH				='0207572e-779a-46eb-953f-90c64f98b697';
	const CLS_UID_OPERATION 		='9887b9f5-81f9-4a40-9b9b-c0458701fa9d';
	const CLS_UID_MATERIAL_OPERATION='38e56551-7b85-47a2-985d-21589255b3de';
	const CLS_UID_DIAMETRE			='10db6093-56c2-4c43-af67-afa8e6436317';
	const CLS_UID_OPERATION_BLOCKS	='9a31a433-8b11-41dc-b480-4bee8ec00e57';
	const CLS_UID_OPERATION_ELEMENTS='d7f886f1-0ae0-4e8b-9f2d-66b6b77e07e2';
	const CLS_UID_PERSONNE_PHYS		='e883e744-8718-4da3-8d09-d4fc500b55a4';
	//uid блоков классов Results
	const CLS_UID_Results			='e1e563da-e41b-4a04-b73e-db4903243d91';
	const CLS_UID_Operations		='9352f443-4851-4bb1-8e91-1d665a0bb93f';
	const CLS_UID_Isolant_operation	='fb0219b2-b1cc-47fd-95e3-827b8c9d807a';

	//uid блоков для генерации PDF
	const CLS_UID_BaseNumber		='da552880-9024-4acb-8764-7152df385cd0';
	const CLS_UID_DateDeCreation	='25e2e72e-b4b3-451a-a1f1-2dcf8c393e65';
	const CLS_UID_DateDeCreationF	='135c63bd-2a11-41c7-ba8d-0734ee71cadf';

	//uid блоков для Date range
	const CLS_UID_DateRange			='89a9922b-5c91-42bb-b8bf-19f14b0bb20f';
	const CLS_UID_PAGESET			='a657749a-7362-40ec-a1fd-4b678fc27f87';
	const ATTR_UID_Page_type		='c8d0397d-281a-4ce6-ac74-aa1e94dc76d2';
	//uid блоков для конфигов
	const CLS_UID_ORGANISATION		='2fc7b0b2-2eb7-43f3-9d5a-95bf324b815d';
	const CLS_UID_USERS				='00000000-0000-0000-0000-000000000001';
	//uid блоков ResultEMMY
	const CLS_UID_Results_EMMY		='86731ded-a598-45e2-95dc-3eedc337d176';
	//uid параметров text additional blocks
	const ATTR_UIDS_ADDITIONAL_BLOKS=[
		'additional'=>'8757ba2d-8b76-488d-b767-a15fec0f53af',
		'blockElem'=>'77e083c5-0fb4-44af-979e-1bab31dcdab6'
	];
	const ATTR_UIDS_FILES=[
		'devis'=>['uid'=>'9065f915-ea0a-47f1-aa41-dc3ddb13f493','nameStart'=>'Devis '],
		'facture'=>['uid'=>'ac0f9b55-f91d-45ba-bc16-fc3d0a0fa069','nameStart'=>'Facture '],
		'ah'=>['uid'=>'be249592-c0d8-43ea-b9c6-312e7385983f','nameStart'=>'AH '],	
		'factureExcel'=>['uid'=>'9f764ae2-21ba-4120-8d03-37daed0e7d84']	,
		'nameFilee'=>
		[
			'phys'=>
			[
				'attrsArr'=>
				[
					['beneficiaire','nom'],
					['beneficiaire','prenom']
				],
				'symb'=>' '
			],
			'pmorale'=>
			[
				'attrsArr'=>
				[
					['information_lieu_travaux','nom_travaux']
				],
				'symb'=>' '
			]
		]
	];

	const OPER_PAGE_VERSION=
	[
		'paramDate'=>
		[
			'dateStart'	=>'06e55dbd-1574-4aa5-9b8e-f062b73aad62',
			'dateFinish'=>'23361d2b-5c69-432e-a7f5-34efa1168d4b'
		],
		'oper'=>
		[
			'page1'=>'7762042b-7025-4a1b-aef9-a8be70e9edb6',
			'page2'=>'d6c7a437-23a6-49f1-8605-22312c8a7cc2',
			'AttestationSimplifiee'=>'42aa0f47-06b1-4464-8a3b-47127515b570',
			'urlPrevisitte'=>'a8f77ef2-d278-470f-8313-bc2eebb48340'
		],
		'operDataRange'=>
		[
			'page1'=>'065b778e-3b21-4424-a3fd-ad40eb25c368',
			'page2'=>'490b7211-75d5-404a-896b-de527ed70070'
		]
	];

	//тут uid настроек глобальным
	const GLOBAL_SETT_CONFIG=[
		'AdditionalBlocksText'=>
		[
			'parentAttr'	 =>'8757ba2d-8b76-488d-b767-a15fec0f53af',
			'childAttr' 	 =>'77e083c5-0fb4-44af-979e-1bab31dcdab6',
			'typeChildrenUid'=>'104995b9-b651-447f-b483-682a914ec435'
		],
		'AttrGenFactureAH'=>[
			'attr'=>[
				'uid'=>'f23e965a-1493-4de1-8a49-dec86103de36',

			],
			'specValue'=>'a29b83eb-495f-4b54-83f3-7ec6e161f5ad',
			'type'=>'46ca906e-cfae-4a63-bfbf-a22b4ca4c37e'
		],
		'№devis'=>[
			'obj'=>'066d9258-73c5-4953-b5d1-77d9d2389d75',
			'attr'=>[
				'uid'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
				'datatype'=>1
			],
			'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
		],
		'defaultUnit'=>[
			'obj'=>'4729181c-b16a-4ce0-a94c-0322e77a2b5f',
			'attr'=>[
				'uid'=>'1e73bc31-db9a-4e0e-8a26-659bc7607b84',
				'datatype'=>2
			],
			'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
		],
		'userDevis'=>[
			'attr'=>[
				'uid'=>'184259cd-1736-4476-ac17-28dfe3c14efd',
				'datatype'=>1
			],
			'type'=>'00000000-0000-0000-0000-000000000001'
		],
		'dateCreateionDesTravaux'=>[
			'attr'=>[
				'uid'=>'2cba0bab-fa5a-44bd-83d2-9f5d07e757e3',
				'datatype'=>3
			],
			'type'=>'5086f503-d8b2-4a5b-a3d9-e5abc7ccb728'
		],
		'dataSousTraite'=>[
			'obj'=>'1f82df2e-30c3-4b59-8293-908be5e82075',
			'attr'=>[
				'uid'=>'1e73bc31-db9a-4e0e-8a26-659bc7607b84',
				'datatype'=>2
			],
			'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
		],
		'Data range'=>[
			'type'=>'89a9922b-5c91-42bb-b8bf-19f14b0bb20f',
			'configAttr'=>[
				'startDateRange'=>'06e55dbd-1574-4aa5-9b8e-f062b73aad62',
		        'finishDateRange'=>'23361d2b-5c69-432e-a7f5-34efa1168d4b',
		        'page1'=>'065b778e-3b21-4424-a3fd-ad40eb25c368',
		        'page2'=>'490b7211-75d5-404a-896b-de527ed70070'
			]
		],
		'Maprimrenov'=>[
			'codePostale_le-de-France'=>['75','77','78','91','92','93','94','95'],
			'codePostalKey'=>['beneficiaire','code_postale'],
			'revenueKey'=>['beneficiaire','revenue'],
			'numberDePersne'=>['beneficiaire','number_de_perosne'],
			'mainType'=>
			[
				'type'=>'8e641eb0-c9d7-43d6-b650-fb36bd3395e8',
				'attrs'=>
				[
					'start'=>'a061ff68-dd56-4d1f-8492-8b04f9a4c6e7',
					'finish'=>'459ea47e-2b25-4b4e-845c-11f42d55b949'
				],
			],
			'childType'=>
			[
				'type'=>'6eb8b809-82fb-46cd-a6fe-9d881adcdb1c',
				'attrs'=>
				[
					'count'=>'a5ff96e5-fa00-4d8b-99c5-1243d23fbff7'
				],
				'attrsColour'=>
				[					
					'bleu'=>
					[
						'ledeFrance'	=>'f0de6bb2-436e-4f22-8123-9e1750a187f7',
						'autresRegions'	=>'d759fca7-06ac-40c1-bbac-7f1313798c39'
					],
					'jaune'=>
					[
						'ledeFrance'	=>'480834bd-221e-4b59-b6f4-854b69f3d5a2',
						'autresRegions'	=>'86e2af96-2d92-408e-83d8-9715acaa88cf'
					],
					'violet'=>
					[
						'ledeFrance'	=>'8c67de11-ea60-4cb8-a4fc-b95e7ecc93c5',
						'autresRegions'	=>'175dec8b-fac7-4aa0-976c-0386eccd8798'
					]
				]
			],
			'systemSett'=>
			[
				'bleu'=>
				[
					'obj'		=>'0f9fbd61-7b97-4b28-9cdb-0a84fda94be3',
					'attr'		=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
					'attrOper'	=>'881163b7-4558-4d7c-ac64-62aeceab12d3'
				],
				'jaune'=>
				[
					'obj'		=>'9ba87caf-8ef8-4499-bed5-fd07d6af9cec',
					'attr'		=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
					'attrOper'	=>'d9939a08-0026-4177-b1be-d470eba6cb56'
				],
				'violet'=>
				[
					'obj'		=>'99c658f3-6e53-4d9f-9e16-f74958493e4e',
					'attr'		=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
					'attrOper'	=>'7ba6bfc0-f73c-4486-a157-e02723d5474f'
				],
				'rose'=>
				[
					'obj'		=>'98e81afa-c56f-45fc-8aec-8949c2f08c99',
					'attr'		=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
					'attrOper'	=>'acdef3c0-3fd4-4cd9-b927-387e6e6aef62'
				],
				'maxLimit'=>
				[
					'obj'		=>'1be5b1f4-ef5d-427c-af62-181fe29639b6',
					'attr'		=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
				]
			]
			

		],

		'ExcelGenerate'=>[
			'specOper'=>['BAT-TH-155','BAR-TH-161'],
			'arrcolL'=>'l',
			'arrcolD'=>'d',
			'designationKeysIsolant'=>['SPEC_DESIGNATION'],
			'SpecTemperatuerMode'=>['SPEC_TEMPERATUERMode'],
			'designationKeys'=>['additional_blocks','0','data','0','name'],
			'table'=>
			[
				'startRow'=>18,
				'start2Row'=>37,
				'column1'=>['C','D','E'],
				'column2'=>['H','I','J'],
			]
		],
		'diametreSpecBlock'=>[
			'operNameDiam1'=>
			[
				'BAT-TH-155',
				'BAR-TH-161'
			],
			'operNameDiam2'=>
			[
				'BAT-TH-155 - échangeur à plaques',
				'BAR-TH-161 - échangeur à plaques'
			],
			'nameTemperature'=>
			[
				'100'=>'50°C ≤ Tfluide ≤ 120°C',
				'200'=>'Tfluide > 120°C'
			]

		]
	];

	//ключи для поиска в запросах
	const KEY_OPERATIONS_DATA=['operation'];
	const KEY_OPERATION_UID=['operation','uid'];
	const KEY_OPERATION_NAME=['operation','name'];
	const KEY_ISOLANT_UID=['value'];
	const KEY_BURE_UID=['bureaudecontrole'];
	const KEY_OPERATION_OBJID=['objUid'];
	const KEY_OPERATION_ISOL=['isolant'];
	const KEY_OPERATION_ISOL_ADVPARAMS=['advParams'];
	const KEY_OPERATION_ISOL_ADVPARAMS_TEXT='title';
	const KEY_SOUS_TRAINTCE=['sous_traitance','sous_traitance'];
	const KEY_ORGANISATION_UID=['organization','uid'];
	const KEY_TYPE_PERSONE=['persone'];
	const KEY_PERSONE_PHYS_DATA=['beneficiaire','persone_phys_data'];
	const KEY_TABLE_DIAMETRE=['adv_size'];
	const KEY_OPERATION_ADD_BLOKS=['additional_blocks'];
	const KEY_OPERATION_ADD_BLOKS_DOPDATA=['additional_blocks'];
	const KEY_DATE_DEVIS=['date','date_de_signature'];
	const KEY_LOCK_FACTURE_AH=['lockquantity'];

	const OBJ_DE_TRAVAUX_CONFIG_SETT=[
		'writeDefBenificare'=>[
			'key'=>['persone'],
			'attr'=>
			[
				'uid'=>'065991e9-4913-43fe-9b4e-000b50a00e16',
				'type'=>2
			]
		],
		'typeGenerateDocument'=>
			[
				'create'=>'created_document',
				'edit'=>'edited_document'
			],
		'keyObjectID'=>['object_id'],
		'objDeTravauxSpecAttr'=>
			[
				'dateDeCreation'=>
				[
					'uid'=>'2cba0bab-fa5a-44bd-83d2-9f5d07e757e3'
				]
			],
		'personeMorale'=>[
				'key'=>['beneficiaire','persone_phys_data'],
				'keyJSON'=>'JSON',
				'keyNomenre'=>['common','Nombre de personne(s) a charge'],
				'keyRevenu'=>['common','Revenu fiscal de reference'],
			],
		'operations'=>
		[
			'key'=>['operation']
		],
		'operationsAB'=>
		[
			'key'=>['additional_blocks']
		],
		'operationsABdata'=>
		[
			'key'=>['data']
		],
		'operIsolantBloks'=>
		[
			'key'=>['isolant']
		],
		'operIsolantDiametre'=>
		[
			'key'=>['adv_size']
		]


	];

	const OBJ_RESULT_CONFIG_SETT=
	[
		'operations'=>
			[
				'key'=>['operation']
			],
		'prevesite'=>
			[
				'keyMain'=>['beneficiaire','previsite'],
				'keyDop'=>'previsiteBeneficiare'
			],
		'operIsolant'=>
			[
				'key'=>['isolant']
			]
	];

	const OBJ_RESULTEMMY_CONFIG_SETT=
	[
		'operations'=>
			[
				'key'=>['operation'],
				'textKey'=>['operation','name']
			],		
		'operIsolant'=>
			[
				'key'=>['isolant'],
				'surfaceKey'=>['surface'],
				'specOper'=>['BAR-TH-160','BAR-TH-146']
			],
		'operIsolanAdvP'=>
			[
				'key'=>['advParams']
			],
		'actions'=>
			[
				'orgKey'=>
				[
					'organization','uid'
				],
				'siretKey'=>
				[
					'sous_traitance','sous_traitance'
				],
				'bureKey'=>
				[
					'bureaudecontrole'
				],
				'objUid'=>
				[
					'objUid'
				],
				'specOperIsol'=>
				[
					'values'=>
					[
						'Électricité'=>0,
						'Combustible'=>1
					],
					'opers'=>
					[
						'BAR-TH-160','BAT-TH-146'
					]
				],
				'sectDeActivity'=>
				[
					'label'=>'Secteur d’activité',
					'values'=>
					[
						'Autres secteurs'=>3,
						'Bureaux'=>0,
						'Enseignement'=>0,
						'Hôtellerie / Restauration'=>1,
						'Santé'=>2
					]
				],
				'typeDePose'=>
				[
					'label'=>'Type de pose'
				]
			],
		'blocksIsolantAdvParams'=>
		[
			'actions'=>
			[
				'isolMarque'=>'Marque',
				'isolReference'=>'Reference'
			],
			'delim'=>'\\',
			'key'=>'value'
		]
	];

	//специальные блоки настроек для неких нужных величин
	const SPEC_BLOCK_TAX=[
		'defaultObjTax'=>[
			'obj'=>'a103001b-d249-4bb9-94ef-9682295cdddb',
			'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
			'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2',
			'datatype'=>1
		],
		'defTaxSpec'=>[
			'value'=>5.5,
			'text'=>'EHPAD',
			'textArr'=>['EHPAD','(EHPAD)'],
			'keys'=>['beneficiaire','raison_sociale'],
			'operFirst'=>'BAT',
			'keysName'=>['operation','name']
		],
		'uidOperationTax'=>'b14c4168-d6d4-453a-8d9d-ef4560751fbe'
	];

	const keyPageType=[
		'fixed'=>'fixed',
		'page1'=>'page1',
		'page2'=>'page2',
	];
	//настройка наших сценариев конфиг
	const OBJ_SCENARIO=[
		'devis'=>
		[
			'pphysic'=>
				[
					'BAT'=>
					[
						'obj'=>'7b2d3548-f09e-4e5e-b5d4-b17e949a1551'
					],
					'BAR'=>
					[
						'obj'=>'b98b70f7-2b6a-4b71-8128-24c7c6761371'
					]
				],
			'pmorale'=>
				[
					'BAT'=>
					[
	    				'obj'=>'7b2d3548-f09e-4e5e-b5d4-b17e949a1551'
	    			],
	    			'BAR'=>
	    			[
	    				'obj'=>'02a50c8f-42bc-4343-8295-502d83251988'
	    			]
				]
		],
		'ah'=>
		[
			'pphysic'=>
				[
					'BAT'=>
					[
						'obj'=>'ef7a546d-94b0-4d91-b964-e2b6f6572dd9'
					],
					'BAR'=>
					[
						'obj'=>'631525b2-e240-4817-b9af-6f7798f04163'
					]
				],
			'pmorale'=>
				[
					'BAT'=>
					[
	    				'obj'=>'ef7a546d-94b0-4d91-b964-e2b6f6572dd9'
	    			],
	    			'BAR'=>
	    			[
	    				'obj'=>'7315d6ed-6a0f-4715-b19f-91bb9dd15618'
	    			]
				]
		],
		'devisah'=>
		[
			'pphysic'=>
				[
					'BAT'=>
					[
						'obj'=>'ef7a546d-94b0-4d91-b964-e2b6f6572dd9'
					],
					'BAR'=>
					[
						'obj'=>'631525b2-e240-4817-b9af-6f7798f04163'
					]
				],
			'pmorale'=>
				[
					'BAT'=>
					[
	    				'obj'=>'ef7a546d-94b0-4d91-b964-e2b6f6572dd9'
	    			],
	    			'BAR'=>
	    			[
	    				'obj'=>'7315d6ed-6a0f-4715-b19f-91bb9dd15618'
	    			]
				]
		],
		'facture'=>
		[
			'pphysic'=>
				[
					'BAT'=>
					[
						'obj'=>'d159a247-3628-4b48-83df-dff45cf006d0'
					],
					'BAR'=>
					[
						'obj'=>'d159a247-3628-4b48-83df-dff45cf006d0'
					]
				],
			'pmorale'=>
				[
					'BAT'=>
					[
	    				'obj'=>'b5aaeedb-922c-4755-869d-5d5e4a86040f'
	    			],
	    			'BAR'=>
	    			[
	    				'obj'=>'b5aaeedb-922c-4755-869d-5d5e4a86040f'
	    			]
				]			
		]
	];

	const configPageType=[
		'fixed'=>
		[
			'value'=>'beefdb51-0ac8-48ae-90d7-e3d23d89e451',
			'version'=>
			[
				'type'=>'cca36e1c-293d-4ca6-9c7d-593a511aed55',
				'attrs'=>
				[
					'date1'=>'668b7570-baf6-4fca-af7c-87d09a452bb7',
					'date2'=>'38707166-2c4e-4b73-838f-a5f3fec05351',
					'url'  =>'a69c87ce-4e86-499e-ad9a-1493be715d5f',
					'sPage'=>'6394728f-32cb-489d-a522-d720f9c7fc9a'
				]
			]
		],
		'page1'=>
		[
			'value'=>'ee11791e-2585-4a8e-b367-2edd5f13b9e3',
			'version'=>
			[
				'type'=>'cca36e1c-293d-4ca6-9c7d-593a511aed55',
				'attrs'=>
				[
					'date1'=>'668b7570-baf6-4fca-af7c-87d09a452bb7',
					'date2'=>'38707166-2c4e-4b73-838f-a5f3fec05351',
					'url'  =>'a69c87ce-4e86-499e-ad9a-1493be715d5f',
					'sPage'=>'6394728f-32cb-489d-a522-d720f9c7fc9a'
				]
			]
		],
		'page2'=>
		[
			'value'=>'ed6b2955-ebfb-4927-a251-2caf9fa11f31'			
		],
		'QPV'=>
		[
			'value'=>'e782fe9e-881b-4aa4-b423-d6b7281e0fe0',
			'version'=>
			[
				'type'=>'cca36e1c-293d-4ca6-9c7d-593a511aed55',
				'attrs'=>
				[
					'date1'=>'668b7570-baf6-4fca-af7c-87d09a452bb7',
					'date2'=>'38707166-2c4e-4b73-838f-a5f3fec05351',
					'url'  =>'a69c87ce-4e86-499e-ad9a-1493be715d5f',
					'sPage'=>'6394728f-32cb-489d-a522-d720f9c7fc9a'
				]
			]
		],
		'R1'=>
		[
			'value'=>'5cc6a302-b026-4a74-878f-f1d36fadf232',
			'version'=>
			[
				'type'=>'cca36e1c-293d-4ca6-9c7d-593a511aed55',
				'attrs'=>
				[
					'date1'=>'668b7570-baf6-4fca-af7c-87d09a452bb7',
					'date2'=>'38707166-2c4e-4b73-838f-a5f3fec05351',
					'url'  =>'a69c87ce-4e86-499e-ad9a-1493be715d5f',
					'sPage'=>'6394728f-32cb-489d-a522-d720f9c7fc9a'
				]
			]
		],
		'R2'=>
		[
			'value'=>'0518aa50-b40b-4168-8c6a-452b1e79af5b',
			'version'=>
			[
				'type'=>'cca36e1c-293d-4ca6-9c7d-593a511aed55',
				'attrs'=>
				[
					'date1'=>'668b7570-baf6-4fca-af7c-87d09a452bb7',
					'date2'=>'38707166-2c4e-4b73-838f-a5f3fec05351',
					'url'  =>'a69c87ce-4e86-499e-ad9a-1493be715d5f',
					'sPage'=>'6394728f-32cb-489d-a522-d720f9c7fc9a'
				]
			]
		],
		'Bailleur_Social'=>
		[
			'value'=>'4b43d186-dcae-419d-b77d-51d478ccf7c8',
			'version'=>
			[
				'type'=>'cca36e1c-293d-4ca6-9c7d-593a511aed55',
				'attrs'=>
				[
					'date1'=>'668b7570-baf6-4fca-af7c-87d09a452bb7',
					'date2'=>'38707166-2c4e-4b73-838f-a5f3fec05351',
					'url'  =>'a69c87ce-4e86-499e-ad9a-1493be715d5f',
					'sPage'=>'6394728f-32cb-489d-a522-d720f9c7fc9a'
				]
			]
		],
		'operPrevisite'=>
		[
			'value'=>'e57fb4db-1ab5-447b-8b34-f53f002c550a'			
		],
		'operGenConditions'=>
		[
			'value'=>'16558399-511f-4bfe-ab61-33f95bb068c6'			
		],
		'operAttSimpleLife'=>
		[
			'value'=>'71a1b860-096b-4a66-b8a2-dd68a010abb6'			
		]
	];

	const opCoopAve=[
		'qpv'=>['value'=>'QPV'],
		'R2'=>['value'=>'OPERATION AVEC PRECARITE'],
		'bailerSoc'=>['value'=>'Bailleur Social']
	];

	const MIME_TYPE_EXCEL_GOOGLE='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
}
