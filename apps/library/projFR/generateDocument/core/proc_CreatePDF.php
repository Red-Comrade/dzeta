<?php

namespace generateDocEnemat;
class  createPDF
{
	//процесс который создает объект типа ResultEmmy
	private $copyRequest=[];
	private $generateDoc=null;
	private $typeDocument='';
	public  $pathExcelArrs=[];

	public function __construct($requestData,$objGenDoc,$typeDocument)
	{
		$this->copyRequest=$requestData;
		$this->generateDoc=$objGenDoc;
		$this->typeDocument=$typeDocument;
	}

	public function init()
	{
		$_obj=new \Dzeta\Models\Obj();
        $settMetkaArrConfig=json_decode(file_get_contents(Constans::PATH_CONFIG_SETT_METKA),true);
        $copyDataRequest=$this->copyRequest;
        $DATENOW=new \DateTime();
        $this->generateDoc->dataLogs[]=[
            'time'=>date('d/m/Y H:i:s'),
            'text'=>'Начинаем генерировать файл',
            'data'=>[
                'params'=>$copyDataRequest,
                'config'=>$settMetkaArrConfig
            ]               
        ];

        $typeUser=null;
		$typeUser=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::KEY_TYPE_PERSONE);
		if($typeUser=='Personne physique')
		{
			$typeUser='pphysic';
		}else if($typeUser=='Personne morale')
		{
			$typeUser='pmorale';
		}else
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Неизвестен тип пользователя. Генерация невозможна',
	            'err'=>true   
	        ];	
	        return false;		
		}

		$arrSettt=
		[
			'operNameFirst'=>null,
			'operNameFirst3'=>null,
			'typePerson'=>$typeUser,
			'dateDeviss'=>$this->getDateDevis($copyDataRequest)
		];

		//$dateDeviss=!empty($dateClint['date_de_signature'])?\DateTime::createFromFormat(\Dzeta\Core\Value\DateTimeV::DATE_FORMAT_new,$dateClint['date_de_signature']) :new DateTime();

		$metksGlobalArr=[];
		$allMetkaElems=[];
		HelperFunctions::getAllElemsFromConfig($settMetkaArrConfig,$allMetkaElems);	

		$fl=$this->workSimpleMetka($allMetkaElems,$metksGlobalArr,$copyDataRequest);
		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при обработки простых меток',
	            'err'=>true    
	        ];	
	        return false;
		}

		$fl=$this->workBeneficiareMetka($allMetkaElems,$copyDataRequest);
		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при обработки меток Beneficiare',
	            'err'=>true    
	        ];	
	        return false;
		}

		$metksBenecifiareArr=$fl['metkBen'];
		$dataBeneficiareDop=$fl['benifiDop'];
		$arrSettt['tableA']=$fl['tA'];
		$arrSettt['tableB']=$fl['tB'];
		$fl=$this->workSousTraitanceMetka($allMetkaElems,$copyDataRequest);
		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при обработки меток SousTraitance',
	            'err'=>true    
	        ];	
	        return false;
		}
		$sousMetkaArr=$fl;
		$fl=$this->worksimpleDBValue($allMetkaElems,$copyDataRequest,$metksGlobalArr);

		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при обработки простых меток из БД',
	            'err'=>true   
	        ];	
	        return false;
		}
		$CONFIGGLOBAL=$this->workMaprenov($copyDataRequest,$DATENOW);

		$operBureMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'operBureSimple');
		$operCheckTextMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'operCheckText');
		$opersCheckMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'operCheck');
		$opersMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'simpleOperation');		
		$opersTableMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'tableOper');
		$isolsTableMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'tableIsol');
		$operDiamTableMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'operDiametreSpec');

		

		$isolantAdvParamsMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'simpleAdvParamsIsolant');		
		$diamTableMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'tableDiam');
		
		$opersAdditionalBlocksMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'specAdditionalBlocks');
		$elemsGlobalDataMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'elemGlobalData');
		

		$copyOperData=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::KEY_OPERATIONS_DATA);

		$flagTax=false;
		$defaultTax=null;
		$fl=$this->getTax($copyOperData,$copyDataRequest);

		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при получении БД',
	            'err'=>true    
	        ];	
	        return false;
		}	
		$flagTax=$fl['flagTax'];
		$defaultTax=$fl['defaultTax'];
		

		$textOper=null;
		$IncitationArr=[];
		$allOpersData=[];
		
		$fl=$this->getNameFirstOperation($copyOperData);
		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при получении БД',
	            'err'=>true    
	        ];	
	        return false;
		}
		$arrSettt['operNameFirst']=$fl['nameOper'];
		$arrSettt['operNameFirst3']=$fl['nameOper3'];
		
		$CONFIG_BIG=[];
		$CONFIG_BIG['incitation']=0;
		$CONFIG_BIG['cumac']=0;
		$CONFIG_BIG['resteAcharge']=0;
		$CONFIG_BIG['operAvec']='';
		$CONFIG_BIG['defUnit']=$this->getDefaultUnit();
		$CONFIG_BIG['aCode']=$this->getAcode($copyOperData);
		$CONFIG_BIG['sectDeActiv']='';
		$CONFIG_BIG['energyDeChauf']='';
		$CONFIG_BIG['opersTitle']=[];
		$CONFIG_BIG['allIsolantRequest']=[];
		$tableMetkaValueAll=[];
		$opersMetkaValueArr=[];
		$opersIsolantMetkaValueArr=[];
		$pagesURLSOPER=['page2'=>[],'AttestationSimplifiee'=>[],'urlPrevisitte'=>[]];
		
		foreach ($copyOperData as $curNumOper=> $valueOperation) 
		{
			$operCONFIG=[];
			$operMetka=[];
			$operCheckMetka=[];
			$opersTableData=[];
			$uidOperation=HelperFunctions::findValueConfigRequest($valueOperation,Constans::KEY_OPERATION_UID);
			$dataOperDB=HelperFunctions::getDataFromCahsheDB($uidOperation,$this->generateDoc);

			$requestOperObjUID=HelperFunctions::findValueConfigRequest($valueOperation,Constans::KEY_OPERATION_OBJID);
			
			$fl=$this->workBureDeControle($valueOperation,$operBureMetka);
			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при получении BureDeControle',
		            'err'=>true    
		        ];	
		        return false;
			}
			$bureMetka=$fl;	

			$fl=$this->workOperation($valueOperation,$opersMetka,$operMetka,
				['flagTax'=>$flagTax,'defaultTax'=>$defaultTax,'dataOperDB'=>$dataOperDB]);
			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при работе с simpleOperation',
		            'err'=>true    
		        ];	
		        return false;
			}
			
			$operCONFIG['operSurface']=$fl['surface'];
			$operCONFIG['costClient']=$fl['incitation'];			
			$operCONFIG['operTTC']=$operCONFIG['costClient']/$fl['surface'];
			$operCONFIG['opersTitle']=$fl['opersTitle'];
			$CONFIG_BIG['incitation']+=$fl['incitation'];
			$CONFIG_BIG['opersTitle'][]=$fl['opersTitle'];
			$pageUrlSett=$this->getVersionUrlP1P2($uidOperation,['dataDevis'=>$arrSettt['dateDeviss']]);

			$fl=$this->workTextCheck($valueOperation,$operCheckTextMetka,$operMetka);
			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при работе с operCheckText',
		            'err'=>true    
		        ];	
		        return false;
			}
			if($curNumOper==0)
			{
				$CONFIG_BIG['sectDeActiv']=$fl['sectDeActiv'];
				$CONFIG_BIG['energyDeChauf']=$fl['energyDeChauf'];
			}

			$fl=$this->workOperCheckBox($valueOperation,$opersCheckMetka,$operCheckMetka);
			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при работе с OperCheck',
		            'err'=>true    
		        ];	
		        return false;
			}
			
			$fl=$this->workWithAdditionalBlocks($valueOperation,$opersAdditionalBlocksMetka,$operCheckMetka);

			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при работе с AdditionalBloks',
		            'err'=>true    
		        ];	
		        return false;
			}			
			$AdditionalBloksMetkasData=$fl['simple'];



			$fl=$this->workOpersTableMetka($opersTableMetka,$valueOperation,$opersTableData,$uidOperation,['defUnit'=>$CONFIG_BIG['defUnit']]);
			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при работе с OperTable',
		            'err'=>true    
		        ];	
		        return false;
			}

			$operCONFIG['prix_unitare']=$fl['prix_unitare'];
			$CONFIG_BIG['cumac']+=$fl['cumac'];
			$CONFIG_BIG['resteAcharge']+=$fl['resteAcharge'];

			$designationOper=HelperFunctions::findValueConfigRequest($valueOperation,Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['designationKeys']);
			 
			$tableDataMetkaAll=[];
			$copyIsolant=HelperFunctions::findValueConfigRequest($valueOperation,Constans::KEY_OPERATION_ISOL);	

			foreach ($copyIsolant as $valueIsolant) 
			{
				$valueIsolant[Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['designationKeysIsolant'][0]]=$designationOper;
				

				$isolantValueMetka=[];
				$uidIsolant=HelperFunctions::findValueConfigRequest($valueIsolant,Constans::KEY_ISOLANT_UID);
				$dataIsolantDB=[];				
				if(!empty(HelperFunctions::stringUid($uidIsolant)))
				{	
					$dataIsolantDB=HelperFunctions::getDataFromCahsheDB($uidIsolant,$this->generateDoc);
				}
				$fl=$this->workSimpleIsolant($isolsTableMetka,$valueIsolant,$isolantValueMetka,
					['dataIsolDB'=>$dataIsolantDB,'operSurface'=>$operCONFIG['operSurface'],'costClient'=>$operCONFIG['costClient'],
					'operTTC'=>$operCONFIG['operTTC'],'prix_unitare'=>$operCONFIG['operTTC']]);				
				if(!$fl)
				{
					$this->generateDoc->dataLogs[]=[
			            'time'=>date('d/m/Y H:i:s'),
			            'text'=>'Произошла ошибка при работе с tableIsol',
			            'err'=>true    
			        ];	
			        return false;
				}
				$configIsolant['surfaceIsolant']=$fl['surfaceIsolant'];

				$fl=$this->workSimpleAdvParamsIsolant($isolantAdvParamsMetka,$valueIsolant,$isolantValueMetka);				
				if(!$fl)
				{
					$this->generateDoc->dataLogs[]=[
			            'time'=>date('d/m/Y H:i:s'),
			            'text'=>'Произошла ошибка при работе с AdvParamsIsolant',
			            'err'=>true    
			        ];	
			        return false;
				}
				
				$fl=$this->workWithOperDiametreSpec($operDiamTableMetka,$isolantValueMetka,
					[
						'isolantRequest'=>$valueIsolant,
						'objUidRequest'=>$requestOperObjUID,
						'operName'=>$operCONFIG['opersTitle'],
						'surfaceIsolant'=>$configIsolant['surfaceIsolant']
					]
				);
				if(!$fl)
				{
					$this->generateDoc->dataLogs[]=[
			            'time'=>date('d/m/Y H:i:s'),
			            'text'=>'Произошла ошибка при работе с operDiamTableMetka',
			            'err'=>true    
			        ];	
			        return false;
				}
				$configIsolant['tempMode']=$fl['tempMode'];
				$valueIsolant[Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['SpecTemperatuerMode'][0]]=$configIsolant['tempMode'];

				$dataDiametre=HelperFunctions::findValueConfigRequest($valueIsolant,Constans::KEY_TABLE_DIAMETRE);
				$dataDiametre=empty($dataDiametre)?[]:$dataDiametre;
				$metksDiametrev=[];
				foreach ($dataDiametre as $valueDiametre) 
				{
					$metkDiametre=[];
					$fl=$this->workDiametre($valueDiametre,$diamTableMetka,$metkDiametre);
					if(!$fl)
					{
						$this->generateDoc->dataLogs[]=[
				            'time'=>date('d/m/Y H:i:s'),
				            'text'=>'Произошла ошибка при работе с tableDiam',
				            'err'=>true    
				        ];	
				        return false;
					}
					$metksDiametrev[]=$metkDiametre;
				}
				$rowTableArrMetka=[];
				$arrToMerge=[$operMetka,$bureMetka,$AdditionalBloksMetkasData,$operCheckMetka,$isolantValueMetka,$opersTableData];
				foreach ($arrToMerge as  $valueToMerge) 
				{
					$fl=$this->mergeKeyToArr($valueToMerge,$rowTableArrMetka);
					if(!$fl)
					{
						$this->generateDoc->dataLogs[]=[
				            'time'=>date('d/m/Y H:i:s'),
				            'text'=>'Произошла ошибка при объединении',
				            'err'=>true    
				        ];	
				        return false;
					}
				}
				
				$fl=$this->replaceMetkaArrs($rowTableArrMetka,['mArr'=>$metksDiametrev]);
				if(!$fl)
				{
					$this->generateDoc->dataLogs[]=[
			            'time'=>date('d/m/Y H:i:s'),
			            'text'=>'Произошла ошибка при выполнении замен',
			            'err'=>true    
			        ];	
			        return false;
				}
				$tableMetkaValueAll[]=$rowTableArrMetka;
				//теперь обычные метки операции-изолянты
				$rowOperIsolantArrMetka=[];
				$arrToMerge=[$operMetka,$isolantValueMetka,$bureMetka,$AdditionalBloksMetkasData,$operCheckMetka];
				foreach ($arrToMerge as  $valueToMerge) 
				{
					$fl=$this->mergeKeyToArr($valueToMerge,$rowOperIsolantArrMetka);
					if(!$fl)
					{
						$this->generateDoc->dataLogs[]=[
				            'time'=>date('d/m/Y H:i:s'),
				            'text'=>'Произошла ошибка при объединении 2',
				            'err'=>true    
				        ];	
				        return false;
					}
				}

				$fl=$this->replaceMetkaArrs($rowOperIsolantArrMetka);

				if(!$fl)
				{
					$this->generateDoc->dataLogs[]=[
			            'time'=>date('d/m/Y H:i:s'),
			            'text'=>'Произошла ошибка при выполнении замен2',
			            'err'=>true    
			        ];	
			        return false;
				}				
				$opersIsolantMetkaValueArr[]=$rowOperIsolantArrMetka;

				if(empty($pageUrlSett['page2']))
				{
					$pagesURLSOPER['page2'][]='';
				}else
				{
					$pagesURLSOPER['page2'][]=$pageUrlSett['page2'];
				}

				$CONFIG_BIG['allIsolantRequest'][]=$valueIsolant;
			}
			if(empty($pageUrlSett['AttestationSimplifiee']))
			{
				$pagesURLSOPER['AttestationSimplifiee'][]='';
			}else
			{
				$pagesURLSOPER['AttestationSimplifiee'][]=$pageUrlSett['AttestationSimplifiee'];
			}
			if(empty($pageUrlSett['urlPrevisitte']))
			{
				$pagesURLSOPER['urlPrevisitte'][]='';
			}else
			{
				$pagesURLSOPER['urlPrevisitte'][]=$pageUrlSett['urlPrevisitte'];
			}
			$rowOperArrMetka=[];
			$arrToMerge=[$operMetka,$bureMetka,$AdditionalBloksMetkasData,$operCheckMetka];
			foreach ($arrToMerge as  $valueToMerge) 
			{
				$fl=$this->mergeKeyToArr($valueToMerge,$rowOperArrMetka);
				if(!$fl)
				{
					$this->generateDoc->dataLogs[]=[
			            'time'=>date('d/m/Y H:i:s'),
			            'text'=>'Произошла ошибка при объединении3',
			            'err'=>true    
			        ];	
			        return false;
				}
			}
			$fl=$this->replaceMetkaArrs($rowOperArrMetka);
			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при выполнении замен3',
		            'err'=>true    
		        ];	
		        return false;
			}				
			$opersMetkaValueArr[]=$rowOperArrMetka;
		}	


		$fl=$this->workGlobalDataMetka($copyDataRequest,$elemsGlobalDataMetka,$metksGlobalArr,
			['incitationGlobal'=>$CONFIG_BIG['incitation'],'typePersone'=>$typeUser,
			'cumac'=>$CONFIG_BIG['cumac'],'resteAcharge'=>$CONFIG_BIG['resteAcharge'],
			'tax'=>$defaultTax,"sectDeActiv"=>$CONFIG_BIG['sectDeActiv'],"energyDeChauf"=>$CONFIG_BIG['energyDeChauf'],
			'allOpersTitle'=>$CONFIG_BIG['opersTitle']
			]
		);
		
		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при выполнении elemGlobalData',
	            'err'=>true    
	        ];	
	        return false;
		}
		$CONFIG_BIG['operAvec']=$fl['operAvec'];
		//теперь надо сделать метки под замены и прочую дичь
		$finalMetkaGlobal=[];
		$arrToMerge=[$metksGlobalArr,$sousMetkaArr,$metksBenecifiareArr];
		foreach ($arrToMerge as  $valueToMerge) 
		{
			$fl=$this->mergeKeyToArr($valueToMerge,$finalMetkaGlobal);
			if(!$fl)
			{
				$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при объединении4',
		            'err'=>true    
		        ];	
		        return false;
			}
		}
		$fl=$this->replaceMetkaArrs($finalMetkaGlobal);
		if(!$fl)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Произошла ошибка при выполнении замен4',
	            'err'=>true    
	        ];	
	        return false;
		}				
		if($this->typeDocument=='facture')
        {
        	/*echo json_encode(['opi'=>$opersIsolantMetkaValueArr,'glb'=>$finalMetkaGlobal]);
        	echo '<br/><br/><br/>';*/
        	$fl=$this->generateExcelfiles($opersIsolantMetkaValueArr,$finalMetkaGlobal,['allIsolantRequest'=>$CONFIG_BIG['allIsolantRequest']]);
        	if($fl===false)
        	{
        		$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Произошла ошибка при генерации excel',
		            'err'=>true    
		        ];	
		        return false;
        	}
        	if(!empty($fl))
        	{
        		$this->pathExcelArrs=$fl;
        	}
        	
        }
		
		$uidScenario=$this->getUidScenario($arrSettt['operNameFirst3'],$this->typeDocument,$arrSettt['typePerson']);
		
		if($uidScenario==false)
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Не получен uid Сценария',
	            'err'=>true    
	        ];	
	        return false;
		}
		$this->generateDoc->dataLogs[]=[
            'time'=>date('d/m/Y H:i:s'),
            'text'=>'Получен uid Сценария-'.$uidScenario,
            'data'=>
            [
            	'uid'=>$uidScenario
            ]   
        ];
        /*echo '<br/>';
        echo '<br/>';
        echo strip_tags(json_encode(['operIsolant'=>$opersIsolantMetkaValueArr]));
        echo '<br/>';
        echo '<br/>';
        return null;*/
		$pages=$this->getPageSceneario($uidScenario,['dateDevis'=>$arrSettt['dateDeviss'],'typePerson'=>$arrSettt['typePerson'],'tableA'=>$arrSettt['tableA'],'tableB'=>$arrSettt['tableB'],'opCoopAve'=>$CONFIG_BIG['operAvec'],'aCode'=>$CONFIG_BIG['aCode']],
			['global'=>$finalMetkaGlobal,'table'=>$tableMetkaValueAll,'operation'=>$opersMetkaValueArr,'operIsolant'=>$opersIsolantMetkaValueArr,
			'urlsSett'=>$pagesURLSOPER]);
		
		if(empty($pages))
		{
			$this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Не получены страницы',
	            'err'=>true    
	        ];	
	        return false;
		}
		$this->generateDoc->dataLogs[]=[
            'time'=>date('d/m/Y H:i:s'),
            'text'=>'Получены страницы в количестве '.count($pages)
        ]; 

        
           
        return $this->createPDFFiles($pages);
	}

	private function workWithOperDiametreSpec($isolantMetkaConfig,&$isolantMetka,$dopSett=[])
	{
		//dopSett isolantRequest objUidRequest operName surfaceIsolant
		$advvSizeBlock= HelperFunctions::findValueConfigRequest($dopSett['isolantRequest'],Constans::KEY_TABLE_DIAMETRE);
		
		//массив с нашими диаметрами
		$countForDiameter=
        [
          '100'=>
          [
            '20_65'=>0,
            '65_100'=>0,
            '100'=>0,
            'countD'=>0
          ],
          '200'=>
          [
            '20_65'=>0,
            '65_100'=>0,
            '100'=>0,
            'countD'=>0
          ]
        ];
        $tempBlock=0;
        foreach ($dopSett['objUidRequest'] as  $valueObjRequest) 
        {
        	if($valueObjRequest['name']==Constans:: GLOBAL_SETT_CONFIG['diametreSpecBlock']['nameTemperature']['100'])
        	{
        		$tempBlock="100";
        		break;
        	}else if($valueObjRequest['name']==Constans:: GLOBAL_SETT_CONFIG['diametreSpecBlock']['nameTemperature']['200'])
        	{
        		$tempBlock="200";
        		break;
        	}
        }
        foreach ($advvSizeBlock as $valueSizeBlock) 
        {
        	$keyB=null;
        	if(20<=$valueSizeBlock['d']&&$valueSizeBlock['d']<=65)
        	{
        		$keyB='20_65';
        	}else if(65<$valueSizeBlock['d']&&$valueSizeBlock['d']<=100)
        	{
        		$keyB='65_100';
        	}else if($valueSizeBlock['d']>100)
        	{
        		$keyB='100';
        	}
        	if(!is_null($keyB))
        	{
        		if(isset($countForDiameter[$tempBlock])&&isset($countForDiameter[$tempBlock][$keyB]))
        		{
        			$countForDiameter[$tempBlock][$keyB]+=$valueSizeBlock['l'];
        		}
        		if(isset($countForDiameter[$tempBlock]))
        		{
        			$countForDiameter[$tempBlock]['countD']+=$dopSett['surfaceIsolant'];
        		}
        	}
        }
		foreach ($isolantMetkaConfig as $valueMetka) 
		{
			if($valueMetka['metka']!='')
			{
				if(isset($isolantMetka[$valueMetka['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueMetka['metka'],
						'data'=>[                               
							'config'=>$valueMetka
						]               
					];
					return false;
				}else
				{
					//проверяем на принадлежность к спец операциям
					$valMetka='';
					if(in_array($dopSett['operName'], Constans::GLOBAL_SETT_CONFIG['diametreSpecBlock']['operNameDiam1']))
					{
						
						if(isset($valueMetka['action']))
						{
							switch ($valueMetka['action']) 
							{
								case 'temp_50-120@diam_20-65':
									$valMetka=$countForDiameter['100']['20_65'];
									break;
								case 'temp_50-120@diam_65-100':
									$valMetka=$countForDiameter['100']['65_100'];
									break;
								case 'temp_50-120@diam_100':
									$valMetka=$countForDiameter['100']['100'];
									break;
								case 'temp_120@diam_20-65':
									$valMetka=$countForDiameter['200']['20_65'];
									break;
								case 'temp_120@diam_65-100':
									$valMetka=$countForDiameter['200']['65_100'];
									break;
								case 'temp_120@diam_100':
									$valMetka=$countForDiameter['200']['100'];
									break;
								case 'num@temp_50-120@d0':
								case 'num@temp_120@d0':
									$valMetka=0;
									break;
							}
						}						
					}elseif(in_array($dopSett['operName'], Constans::GLOBAL_SETT_CONFIG['diametreSpecBlock']['operNameDiam2']))
					{
						if(isset($valueMetka['action']))
						{
							switch ($valueMetka['action']) 
							{
								case 'num@temp_50-120@d0':
									$valMetka=$countForDiameter['100']['countD'];
									break;
								case 'num@temp_120@d0':
									$valMetka=$countForDiameter['200']['countD'];
									break;
								case 'temp_50-120@diam_20-65':									
								case 'temp_50-120@diam_65-100':									
								case 'temp_50-120@diam_100':									
								case 'temp_120@diam_20-65':									
								case 'temp_120@diam_65-100':									
								case 'temp_120@diam_65-100':
									$valMetka=0;
									break;
							}
						}
					}else
					{
						$valMetka='';
					}
					$isolantMetka[$valueMetka['metka']]=['v'=>$valMetka,'type'=>'string','sett'=>$valueMetka];
				}
			}
		}
		return ['tempMode'=>$tempBlock];
	}

	private function generateExcelfiles($dataSingle,$dataGlobal,$dopSett=[])
	{		
		//excelN.xlsx
		$pathExcelFILE=Constans::PATH_FOLDER_GENERATE.$this->generateDoc->UID_FOLDER.'/excelN.xlsx';
		$fl=$this->downloadExcelFileGoogleDisk(Constans::PATH_URL_GENERATE_EXCEL,$pathExcelFILE);
		$pathExcelFolder=Constans::PATH_FOLDER_GENERATE.$this->generateDoc->UID_FOLDER.Constans::PATH_FOLDER_EXCEL;
		mkdir($pathExcelFolder,0777);
		//ибо есть глюк с двоением файла
		$zip = new \ZipArchive();
        $zip->open($pathExcelFILE);
        $zip->deleteName('xl\drawings\drawing1.xml');
        $zip->close();

		if(empty($fl))
		{
			return $fl;
		}
		$excelFilesFolder=[];
		$allIsolantsReq=$dopSett['allIsolantRequest'];
		foreach ($dataSingle as $keyMm=>$valueMetka) 
		{
			//получаем имя операции для Excel
			$nameOpercurr=HelperFunctions::findValueForMetkapoActionAndType('opersTitle','simpleOperation',$valueMetka);
			if(!in_array($nameOpercurr, Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['specOper']))
			{
				continue;
			}
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load($pathExcelFILE);
            $cells = $spreadsheet->getSheetByName('Sheet1')->getCellCollection();
            $sheetExcell=$spreadsheet->getSheetByName('Sheet1');
            $maxRow=$cells->getHighestRow();
            $maxColumn=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($cells->getHighestColumn());
            $metkData=array_merge($valueMetka,$dataGlobal);
            //тут надо частично обнулить метки
            $tempMode=HelperFunctions::findValueConfigRequest($allIsolantsReq[$keyMm],Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['SpecTemperatuerMode']);
            if($tempMode=="100"||$tempMode=="200")
            {
            	if($tempMode="100")
            	{
            		$arrObnulAction=['temp_120@diam_20-65','temp_120@diam_65-100','temp_120@diam_100'];
            	}else if($tempMode="200")
            	{
            		$arrObnulAction=['temp_50-120@diam_20-65','temp_50-120@diam_65-100','temp_50-120@diam_100'];
            	}
            	foreach ($arrObnulAction as  $valueObnull) 
            	{
            		$kkkk=HelperFunctions::findKEYForMetkapoActionAndType( $valueObnull,'operDiametreSpec',$metkData);
            		if(!is_null($kkkk))
            		{
            			$metkData[$kkkk]['v']='';
            		}
            	}
            }
            //делаем перебор данных листа Excel
            for($row = 1; $row <= $maxRow; $row++)
            {
                for($column=1;$column<=$maxColumn;$column++)
                {
                    $colLetter=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column);
                    $cellValue=$cells->get($colLetter.$row)?$cells->get($colLetter.$row)->getValue():'';
                    preg_match_all('/\{[^}]*\{(.*?)\}.*?\}/', $cellValue, $output_array);                   
                    if(!empty($output_array[1]))
                    {
                        foreach($output_array[1] as $value) 
                        {                           
                            if(isset($metkData[$value]))
                            {   
                            	if($metkData[$value]['type']=='string')
                            	{
                            		$cellValue=str_replace('{{'.$value.'}}',strip_tags($metkData[$value]['v']), $cellValue);
                            	}elseif($metkData[$value]['type']=='file'&&file_exists($metkData[$value]['v']))
                            	{
                            		$tmpfname=$metkData[$value]['v'];
                                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                                    list($w, $h) = getimagesize($tmpfname);
                                    if($w/$h>2)
                                    {            
                                        $h=4.5*$h/$w;
                                        $w=4.5;
                                    }else
                                    {           
                                        $w=2.5*$w/$h;
                                        $h=2.5;
                                    }
                                    $w*=38;
                                    $h*=38;
                                    $drawing->setPath($tmpfname);
                                    $drawing->setName('testN'.uniqid());
                                    $drawing->setDescription('testD'.uniqid());
                                    $drawing->setHeight($h);
                                    $drawing->setWidth($w);
                                    $drawing->setCoordinates(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column) . $row);
                                    $drawing->setWorksheet($sheetExcell);
                                    $cellValue='';
                            	}elseif($metkData[$value]['type']=='file')
                            	{
                            		$cellValue=str_replace('{{'.$value.'}}','File 404', $cellValue);
                            	} 
                            }
                        }
                        $sheetExcell->setCellValue(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column) . $row,
                            strip_tags($cellValue));
                    }
                }
            }
            //тут теперь заполнение таблицы
            $tableForExcel=[];
            $advvSize=HelperFunctions::findValueConfigRequest($allIsolantsReq[$keyMm], Constans::KEY_TABLE_DIAMETRE);
            $designation=HelperFunctions::findValueConfigRequest($allIsolantsReq[$keyMm],Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['designationKeysIsolant']);
            foreach ($advvSize as $valueAdvSize) 
            {
            	for($i=0;$i<$valueAdvSize[Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['arrcolL']];$i++)
            	{
            		$tableForExcel[]=
            		[
            			'marque'=>HelperFunctions::findValueForMetkapoActionAndType('marque','simpleAdvParamsIsolant',$valueMetka),
            			'designation'=>$designation,
            			'diametre'=>$valueAdvSize[Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['arrcolD']]
            		];
            	}
            }
            $rowInit=Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['table']['startRow'];
            $counttt=0;
            $columnKyees=Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['table']['column1'];

            foreach ($tableForExcel as $valueExcel) 
            {
            	$sheetExcell->setCellValue($columnKyees[0]. $rowInit,$valueExcel['marque']);
				$sheetExcell->setCellValue($columnKyees[1]. $rowInit,$valueExcel['designation']);
				$sheetExcell->setCellValue($columnKyees[2]. $rowInit,$valueExcel['diametre']);
				$rowInit++;
				if($rowInit>Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['table']['start2Row'])
				{
					$columnKyees=Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['table']['column2'];
					$rowInit=Constans::GLOBAL_SETT_CONFIG['ExcelGenerate']['table']['startRow'];
					$counttt++;
				}
				if($counttt==2)
				{
					break;
				}
            }

            $this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'В табличке Excel записей:'.count($tableForExcel),
	            'data'=>
	            [
	            	'tableExcel'=>$tableForExcel
	            ]
	        ];
            $arrForName=
            [
            	['type'=>'simple','action'=>'infDeLaCorp'],            	
            	['type'=>'simpleOperation','action'=>'opersTitle'],
            	['type'=>'tableIsol','action'=>'isolantMarque'],
            	['type'=>'tableIsol','action'=>'isolantReference']
            ];
            $arrToName=[];
            foreach ($arrForName as $valueNamee) 
            {
            	$arrToName[]=HelperFunctions::findValueForMetkapoActionAndType($valueNamee['action'],$valueNamee['type'],$metkData);
            }
            $arrToName[]='_'.($keyMm+1);
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $writer->setIncludeCharts(true);
            $nameExcelFile=$pathExcelFolder.'/'.implode(' ',$arrToName).'.xlsx';
            $writer->save($nameExcelFile); 
            $excelFilesFolder[]=$nameExcelFile;
            $this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Сгенерирован файл Excel по пути:'.$nameExcelFile,
	            'data'=>
	            [
	            	'filepath'=>$nameExcelFile
	            ]
	        ];
		}
		return $excelFilesFolder;
	}

	private function workTextCheck($valueOperation,$operCheckTextMetka,&$operMetka)
	{
		$operObjsUID=HelperFunctions::findValueConfigRequest($valueOperation,Constans::KEY_OPERATION_OBJID);
		$namesObjsUID=array_column($operObjsUID,'label');
		$resArr=['sectDeActiv'=>'','energyDeChauf'=>''];
		foreach ($operCheckTextMetka as $value_operCheckText) 
		{
			if($value_operCheckText['metka']!='')
			{
				if(isset($operMetka[$value_operCheckText['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$value_operCheckText['metka'],
						'data'=>[                               
							'config'=>$value_operCheckText
						]               
					];
					return false;
				}else
				{
					$val='';
					$k_sear=array_search($value_operCheckText['name'], $namesObjsUID);
					if($k_sear!==false)
					{
						$val=$operObjsUID[$k_sear]['name'];
					}					
					if(isset($value_operCheckText['format']))
					{
						$val=HelperFunctions::execFormatValue($val,$value_operCheckText['format']);
					}
					if(isset($value_operCheckText['action']))
					{
						switch ($value_operCheckText['action']) 
						{
							case 'energyDeChauf':
								$resArr['energyDeChauf']=$val;
								break;
							case 'sectDeActiv':
								$resArr['sectDeActiv']=$val;
								break;								
						}
					}
					$operMetka[$value_operCheckText['metka']]=['v'=>$val,'type'=>'string','sett'=>$value_operCheckText];
				}
			}
		}
		return $resArr;
	}

	private function getDefaultUnit()
	{		
		$defaultUnit='';
		$dataOperDB=HelperFunctions::getDataFromCahsheDB(Constans::GLOBAL_SETT_CONFIG['defaultUnit']['obj'],$this->generateDoc);
		$valUnit=HelperFunctions::findValueFromDataUID(Constans::GLOBAL_SETT_CONFIG['defaultUnit']['attr']['uid'],$dataOperDB);
		if(!is_null($valUnit))
		{
			$defaultUnit=$valUnit->getValue();
		}
		return $defaultUnit;
	}

	private function workWithAdditionalBlocks($valueOperation,$dataConfig,&$operCheckMetka)
	{
		$allMetkaElems=['simple'=>[]];
		//
		$data=HelperFunctions::findValueConfigRequest($valueOperation,Constans::KEY_OPERATION_ADD_BLOKS);

		$dataIssetValues=[];
		foreach ($data as $valueData) 
		{
			$tmp=[];
			$tmp['uid']=$valueData['uid'];
			$childrenD=[];
			$valueData['data']=empty($valueData['data'])? []:$valueData['data'];
			foreach ($valueData['data'] as $valueChildren) 
			{
				$childrenD[$valueChildren['name']]=$valueChildren;
			}
			$tmp['val']=$childrenD;
			$dataIssetValues[$valueData['name']]=$tmp;
		}
		$metkIssetValue=[];
		foreach ($dataConfig as $valueConfig) 
		{
			if($valueConfig['metka']!='')
			{
				if(in_array($valueConfig['metka'], $metkIssetValue))
				{
					continue;
				}
				if(isset($allMetkaElems['simple'][$valueConfig['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueConfig['metka'],
						'data'=>[                               
							'config'=>$valueConfig
						]               
					];
					return false;
				}
				if(isset($valueConfig['nameParent']))
				{
					$flagNo=false;
					//значит у нас тут дочерний пункт
					if(isset($dataIssetValues[$valueConfig['nameParent']]))
					{
						if(isset($dataIssetValues[$valueConfig['nameParent']][$valueConfig['name']]))
						{
							if(strpos($valueConfig['metka'],'CheckON.')===0)
							{
								$operCheckMetka[$valueConfig['metka'].'.OUI']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
							}else
							{	
								$dataDB=HelperFunctions::getDataFromCahsheDB($dataIssetValues[$valueConfig['nameParent']][$valueConfig['name']]['uid'],$this->generateDoc);
								$valDB=HelperFunctions::findValueFromDataUID(Constans::GLOBAL_SETT_CONFIG['AdditionalBlocksText']['childAttr'],$dataDB);
								if(!is_null($valDB))
								{
									$valDB=$valDB->getValue();
								}else
								{
									$valDB='';
								}
								$allMetkaElems['simple'][$valueConfig['metka']]=[
									'v'=>$valDB,
									'type'=>'string',
									'sett'=>$valueConfig
								];
								$operCheckMetka['CheckON.'.$valueConfig['metka'].'.OUI']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
								$operCheckMetka['Check.'.$valueConfig['metka']]=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
								
							}
							$nameAttrSpec=HelperFunctions::findValueFromDataUID(Constans::GLOBAL_SETT_CONFIG['AdditionalBlocksText']['typeChildrenUid'],$dataDB);
							/*if(!is_null($nameAttrSpec))
							{
								$nameFind=$valueConfig['name'].'.'.$nameAttrSpec->getName();
								$conf=HelperFunctions::findValueNameFromConfig($dataConfig,$nameFind);
								if(!is_null($conf))
								{
									if($conf['metka']!='')
									{
										$v=HelperFunctions::findValueConfigRequest($dataIssetValues[$valueConfig['nameParent']][$valueConfig['name']],Constans::userData);
										$v=is_null($v)? '':$v;
										$allMetkaElems['simple'][$nameFind]=[
											'v'=>$v,
											'type'=>'string',
											'sett'=>$valueConfig
										];
										$metkIssetValue[]=$nameFind;
									}									
								}
							}*/
						}else
						{
							$flagNo=true;
						}
					}else
					{
						$flagNo=true;												
					}
					if($flagNo)
					{
						if(strpos($valueConfig['metka'],'CheckON.')===0)
						{
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.NON']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.OUI']=['v'=>0,'type'=>'checkbox','sett'=>$valueConfig];
						}else
						{
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.NON']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.OUI']=['v'=>0,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka['Check.'.$valueConfig['metka']]=['v'=>0,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka[$valueConfig['metka']]=['v'=>'','type'=>'string','sett'=>$valueConfig];
						}
					}
				}else
				{
					//тут родительский пункт
					if(isset($dataIssetValues[$valueConfig['name']]))
					{
						//получаем метку с тесктом из БД по уид
						if(strpos($valueConfig['metka'],'CheckON.')===0)
						{
							//значит просто чекбокс
							$operCheckMetka[$valueConfig['metka'].'.OUI']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
						}else
						{
							$dataDB=HelperFunctions::getDataFromCahsheDB($dataIssetValues[$valueConfig['name']]['uid'],$this->generateDoc);
							$valDB=HelperFunctions::findValueFromDataUID(Constans::GLOBAL_SETT_CONFIG['AdditionalBlocksText']['parentAttr'],$dataDB);
							if(!is_null($valDB))
							{
								$valDB=$valDB->getValue();
							}else
							{
								$valDB='';
							}							
							$allMetkaElems['simple'][$valueConfig['metka']]=[
								'v'=>$valDB,
								'type'=>'string',
								'sett'=>$valueConfig
							];
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.OUI']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.NON']=['v'=>0,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka['Check.'.$valueConfig['metka']]=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
						}
					}else
					{
						//надо проверить на checkON. и добавить .NON
						if(strpos($valueConfig['metka'],'CheckON.')===0)
						{
							//значит просто чекбокс
							$operCheckMetka[$valueConfig['metka'].'.NON']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka[$valueConfig['metka'].'.OUI']=['v'=>0,'type'=>'checkbox','sett'=>$valueConfig];
						}else
						{							
							$allMetkaElems['simple'][$valueConfig['metka']]=[
								'v'=>'',
								'type'=>'string',
								'sett'=>$valueConfig
							];							
							$operCheckMetka['Check.'.$valueConfig['metka']]=['v'=>0,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.OUI']=['v'=>0,'type'=>'checkbox','sett'=>$valueConfig];
							$operCheckMetka['CheckON.'.$valueConfig['metka'].'.NON']=['v'=>1,'type'=>'checkbox','sett'=>$valueConfig];
						}
					}
				}
			}			
		}
		return $allMetkaElems;
	}

	private function workGlobalDataMetka($data,$configDataMetka,&$dataMetka,$dopSetting=[])
	{
		//dopSetting metksGlobalArr typePersone(pphysic pmorale) cumac resteAcharge tax
		$valIncitation=null;
		$configTotalTTC=null;
		$configTotal=null;
		$configTVA=null;
		$valrestaeAcharge=null;
		$operAvec=null;
		foreach ($configDataMetka as $valueMetka) 
		{
			if($valueMetka['metka']!='')
			{
				if(isset($dataMetka[$valueMetka['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueMetka['metka'],
						'data'=>[                               
							'config'=>$valueMetka
						]               
					];
					return false;
				}else
				{
					$val='';
					$flagDB=false;
					$flagAdd=true;
					if(isset($valueMetka['request']))
					{

					}elseif(isset($valueMetka['uid']))
					{
						$flagDB=true;
					}
					if($flagDB)
					{
						if(!is_null($val))
						{
							$val=$val->getValue();
						}
					}
					$val=is_null($val)? '':$val;
					if(isset($valueMetka['action']))
					{
						switch ($valueMetka['action']) 
						{
							case 'incitationGlobal':
								$valIncitation=$dopSetting['incitationGlobal'];
								$val=$valIncitation;
								break;
							case 'cumacGlobal':								
								$val=$dopSetting['cumac'];
								break;
							case 'resteAcharge':
								$valrestaeAcharge=$dopSetting['resteAcharge'];
								if($dopSetting['typePersone']=='pphysic')
								{
									$valrestaeAcharge++;
								}
								$val=$valrestaeAcharge;
								break;
							case 'totalTTC':
								$val='';
								$configTotalTTC=$valueMetka;
								break;	
							case 'total':
								$val='';
								$configTotal=$valueMetka;
								break;	
							case 'tva':
								$val='';
								$configTVA=$valueMetka;
								break;	
							case 'tax':
								$val=$dopSetting['tax'];
								break;	
							case 'operationAvec':
								switch($val)
					            {
						            case 'Operation avec precarite':
						              	$val='OPERATION AVEC PRECARITE';
						                break;
						            case 'QPV':
						                $val='QPV';
						              	break;
						            case 'Bailleur Social':
						              	$val='Bailleur Social';
						              break;
					            }
					            $operAvec=$val;
								break;	
							case 'globalSectDeActi':
								$val=$dopSetting['sectDeActiv'];
								break;
							case 'globalEnergDeChauf':
								$val=$dopSetting['energyDeChauf'];
								break;
							case 'listOperTitle':
								$val=implode(Constans::DELIMETER_NEW_BRAKE_WORD,$dopSetting['allOpersTitle']);
								break;
						}
					}						
					if($flagAdd)
					{
						if(isset($valueMetka['format']))
						{
							$val=HelperFunctions::execFormatValue($val,$valueMetka['format']);
						}
						$dataMetka[$valueMetka['metka']]=['v'=>$val,'type'=>'string','sett'=>$valueMetka];
					}					
				}
			}
		}
		if(!is_null($configTotalTTC))
		{
			$v=$valrestaeAcharge+$valIncitation;
			if(isset($configTotalTTC['format']))
			{
				$v=HelperFunctions::execFormatValue($v,$configTotalTTC['format']);
			}
			$dataMetka[$configTotalTTC['metka']]=['v'=>$v,'type'=>'string','sett'=>$configTotalTTC];
		}
		if(!is_null($configTotal))
		{
			$v=($valrestaeAcharge+$valIncitation)/(1+$dopSetting['tax']/100);
			if(isset($configTotal['format']))
			{
				$v=HelperFunctions::execFormatValue($v,$configTotal['format']);
			}
			$dataMetka[$configTotal['metka']]=['v'=>$v,'type'=>'string','sett'=>$configTotal];
		}
		if(!is_null($configTVA))
		{
			$v=($valrestaeAcharge+$valIncitation)/(1+$dopSetting['tax']/100)*($dopSetting['tax']/100);
			if(isset($configTVA['format']))
			{
				$v=HelperFunctions::execFormatValue($v,$configTVA['format']);
			}
			$dataMetka[$configTVA['metka']]=['v'=>$v,'type'=>'string','sett'=>$configTVA];
		}
		return ['operAvec'=>$operAvec];
	}
	private function getPageSceneario($uidScenario,$dopSetting=[],$dataForMetka=[])
	{
		//dopSetting- opCoopAve R2 urlPrevisitte urlGenCond urlSimpleLife dateDevis
		//$dataForMetka - global table operation operIsolant		
		$_obj=new  \Dzeta\Models\Obj();
		$objScenario=new \Dzeta\Core\Instance($uidScenario, '');
		$objScenDataChild_objs=$_obj->getChildrenByType($objScenario, new \Dzeta\Core\Type\Type(Constans::CLS_UID_PAGESET, ''));         
		$_obj->getAllValues($objScenDataChild_objs);
		$dateDeviss=$dopSetting['dateDevis'];
		$GLOBALS_URLS=[];
		

		foreach ($objScenDataChild_objs as $objScenDataChild_obj) 
		{
			$attrsAll=$objScenDataChild_obj->getAttributes();
			$pageTypee=null;
			foreach ($attrsAll as  $vaPT) 
			{
				switch ($vaPT->getUid()) 
				{
					case Constans::ATTR_UID_Page_type:
						$pageTypee=$vaPT->getValue();
						break;
				}
			}
			if(!is_null($pageTypee))
			{
				switch ($pageTypee->getUid()) 
				{
					case Constans::configPageType['fixed']['value']:
						//fixec
						$tmp=$this->getPageDateVersion($objScenDataChild_obj,Constans::configPageType['fixed'],$dateDeviss,'fixed');
						if(!empty($tmp))
						{
							$tmp['typePage']='fixed';
							$tmp['dataMetka']=['simple'=>$dataForMetka['global'],'table'=>$dataForMetka['table']];
							$GLOBALS_URLS[]=$tmp;
						}
						break;
					case Constans::configPageType['page1']['value']:
						$tmpM=$this->getPageDateVersion($objScenDataChild_obj,Constans::configPageType['page1'],$dateDeviss,'page1');
						if(!empty($tmpM))
						{
							foreach ($dataForMetka['operation'] as $keyNum=>$valueP) 
							{								
								$tmp=$tmpM;
								$tmpArr=[];
								$arrToMerge=[$dataForMetka['global'],$valueP];
								foreach ($arrToMerge as  $valueToMerge) 
								{
									$fl=$this->mergeKeyToArr($valueToMerge,$tmpArr);
									if(!$fl)
									{
										$this->generateDoc->dataLogs[]=[
								            'time'=>date('d/m/Y H:i:s'),
								            'text'=>'Произошла ошибка при объединении5',
								            'err'=>true    
								        ];	
								        return false;
									}
								}	
								$tmp['typePage']='page1';
								$tmp['pages']=1;
								$tmp['dataMetka']=['simple'=>$tmpArr,'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;															
							}							
						}
						break;
					case Constans::configPageType['page2']['value']:						
						foreach ($dataForMetka['operIsolant'] as $keyNum=>$valueP) 
						{
							if(!empty($dataForMetka['urlsSett']['page2'][$keyNum]))
							{
								$tmp=[];
								$tmp['url']=$dataForMetka['urlsSett']['page2'][$keyNum];
								$tmp['typePage']='page2';
								$tmp['sPage']=1;
								$tmp['pages']=1;
								$tmp['aCode']=$dopSetting['aCode'];
								$tmpArr=[];
								$arrToMerge=[$dataForMetka['global'],$valueP];
								foreach ($arrToMerge as  $valueToMerge) 
								{
									$fl=$this->mergeKeyToArr($valueToMerge,$tmpArr);
									if(!$fl)
									{
										$this->generateDoc->dataLogs[]=[
								            'time'=>date('d/m/Y H:i:s'),
								            'text'=>'Произошла ошибка при объединении4',
								            'err'=>true    
								        ];	
								        return false;
									}
								}								
								$tmp['dataMetka']=['simple'=>$tmpArr,'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;
							}							
						}
						break;
					case Constans::configPageType['QPV']['value']:
						if(isset($dopSetting['opCoopAve'])&&$dopSetting['opCoopAve']==Constans::opCoopAve['qpv']['value'])
						{
							$tmp=$this->getPageDateVersion($objScenDataChild_obj,Constans::configPageType['QPV'],$dateDeviss,'qpv');
							if(!empty($tmp))
							{
								$tmp['typePage']='qpv';
								$tmp['dataMetka']=['simple'=>$dataForMetka['global'],'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;
							}
						}
						break;
					case Constans::configPageType['Bailleur_Social']['value']:
						if(isset($dopSetting['opCoopAve'])&&$dopSetting['opCoopAve']==Constans::opCoopAve['bailerSoc']['value'])
						{
							$tmp=$this->getPageDateVersion($objScenDataChild_obj,Constans::configPageType['Bailleur_Social'],$dateDeviss,'bailerSoc');
							if(!empty($tmp))
							{
								$tmp['typePage']='bSoc';
								$tmp['dataMetka']=['simple'=>$dataForMetka['global'],'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;
							}
						}
						break;
					case Constans::configPageType['R1']['value']:
						if($dopSetting['typePerson']=='pphysic'&&($dopSetting['tableA']||$dopSetting['tableB']))
						{
							$tmp=$this->getPageDateVersion($objScenDataChild_obj,Constans::configPageType['R2'],$dateDeviss,'r2');
							if(!empty($tmp))
							{
								$tmp['typePage']='R1';
								$tmp['dataMetka']=['simple'=>$dataForMetka['global'],'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;
							}
						}
						break;
					case Constans::configPageType['R2']['value']:
						if(isset($dopSetting['opCoopAve'])&&$dopSetting['opCoopAve']==Constans::opCoopAve['R2']['value'])
						{
							$tmp=$this->getPageDateVersion($objScenDataChild_obj,Constans::configPageType['R2'],$dateDeviss,'r2');
							if(!empty($tmp))
							{
								$tmp['typePage']='R2';
								$tmp['dataMetka']=['simple'=>$dataForMetka['global'],'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;
							}
						}
						break;
					case Constans::configPageType['operPrevisite']['value']:
						foreach ($dataForMetka['operation'] as $keyNum=>$valueP) 
						{
							if(!empty($dataForMetka['urlsSett']['urlPrevisitte'][$keyNum]))
							{
								$tmp=[];
								$tmp['url']=$dataForMetka['urlsSett']['urlPrevisitte'][$keyNum];
								$tmp['typePage']='urlPrevisitte';
								$tmpArr=[];
								$arrToMerge=[$dataForMetka['global'],$valueP];
								foreach ($arrToMerge as  $valueToMerge) 
								{
									$fl=$this->mergeKeyToArr($valueToMerge,$tmpArr);
									if(!$fl)
									{
										$this->generateDoc->dataLogs[]=[
								            'time'=>date('d/m/Y H:i:s'),
								            'text'=>'Произошла ошибка при объединении6',
								            'err'=>true    
								        ];	
								        return false;
									}
								}								
								$tmp['dataMetka']=['simple'=>$tmpArr,'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;
							}							
						}
						break;
					case Constans::configPageType['operGenConditions']['value']:
						if(!empty($dopSetting['urlGenCond']))
						{
							foreach ($dopSetting['urlGenCond'] as  $value) 
							{
								$value['typePage']='operGenConditions';
								$GLOBALS_URLS[]=$value;
							}
						}
						break;
					case Constans::configPageType['operAttSimpleLife']['value']:						
						foreach ($dataForMetka['operation'] as $keyNum=>$valueP) 
						{
							if(!empty($dataForMetka['urlsSett']['AttestationSimplifiee'][$keyNum]))
							{
								$tmp=[];
								$tmp['url']=$dataForMetka['urlsSett']['AttestationSimplifiee'][$keyNum];
								$tmp['typePage']='AttestationSimplifiee';
								$tmpArr=[];
								$arrToMerge=[$dataForMetka['global'],$valueP];
								foreach ($arrToMerge as  $valueToMerge) 
								{
									$fl=$this->mergeKeyToArr($valueToMerge,$tmpArr);
									if(!$fl)
									{
										$this->generateDoc->dataLogs[]=[
								            'time'=>date('d/m/Y H:i:s'),
								            'text'=>'Произошла ошибка при объединении5',
								            'err'=>true    
								        ];	
								        return false;
									}
								}								
								$tmp['dataMetka']=['simple'=>$tmpArr,'table'=>$dataForMetka['table']];
								$GLOBALS_URLS[]=$tmp;
							}							
						}
						break;
				}
			}
		}

		$curPage=1;
		$allPages=array_sum(array_column($GLOBALS_URLS, 'sPage'));
		$acode='';
		foreach ($GLOBALS_URLS as $key => $value) 
		{
			if(isset($value['sPage']))
			{
				$GLOBALS_URLS[$key]['dataMetka']['simple']['page']=['v'=>$curPage++,'type'=>'string','sett'=>[]];
				for ($ii=2;$ii<=$value['sPage'];$ii++) 
                {
                	$GLOBALS_URLS[$key]['dataMetka']['simple']['page'.$ii]=['v'=>$curPage++,'type'=>'string','sett'=>[]];
                }
			}
			$GLOBALS_URLS[$key]['dataMetka']['simple']['pages']=['v'=>$allPages,'type'=>'string','sett'=>[]];
			if(isset($value['aCode']))
			{
				if(empty($value['aCode']))
				{
					$GLOBALS_URLS[$key]['dataMetka']['simple']['ACODE']=['v'=>'','type'=>'string','sett'=>[]];
				}else
				{
					if(empty($acode))
					{
						$GLOBALS_URLS[$key]['dataMetka']['simple']['ACODE']=['v'=>1,'type'=>'string','sett'=>[]];
                        $acode=2;
					}else
					{
						$GLOBALS_URLS[$key]['dataMetka']['simple']['ACODE']=['v'=>$acode++,'type'=>'string','sett'=>[]];
					}
				}
			}
		}
		return $GLOBALS_URLS;
	}
	private function getVersionUrlP1P2($uidOperation,$dopSetting=[])
	{
		//$dopSetting dataDevis
		$_obj=new  \Dzeta\Models\Obj();
		$dataOperDB=HelperFunctions::getDataFromCahsheDB($uidOperation,$this->generateDoc);
		//теперь надо получить pageVersion		
		$objOperation=new \Dzeta\Core\Instance($uidOperation, '');
		$objScenDataChild_objs=$_obj->getChildrenByType($objOperation, new \Dzeta\Core\Type\Type(Constans::CLS_UID_DateRange, ''));         
		$_obj->getAllValues($objScenDataChild_objs);
		$arrRetun=['page2'=>'','AttestationSimplifiee'=>'','urlPrevisitte'=>''];
		foreach ($objScenDataChild_objs as $objScenDataChild_obj) 
		{
			$attrsAll=$objScenDataChild_obj->getAttributes();
			$dateStart=HelperFunctions::findValueFromDataUID(Constans::OPER_PAGE_VERSION['paramDate']['dateStart'],$attrsAll);
			$dateFinish=HelperFunctions::findValueFromDataUID(Constans::OPER_PAGE_VERSION['paramDate']['dateFinish'],$attrsAll);
			if(!is_null($dateStart)&&!is_null($dateFinish))
			{
				if($dateStart->getValueN()<$dopSetting['dataDevis']&&$dateFinish->getValueN()>$dopSetting['dataDevis'])
				{					
					$url2=HelperFunctions::findValueFromDataUID(Constans::OPER_PAGE_VERSION['operDataRange']['page2'],$attrsAll);					
					if(!is_null($url2))
					{
						$arrRetun['page2']=$url2->getValue();
					}
					break;
				}	
			}
		}		
		if(empty($arrRetun['page2']))
		{
			$url=HelperFunctions::findValueFromDataUID(Constans::OPER_PAGE_VERSION['oper']['page2'],$dataOperDB);
			if(!is_null($url))
			{
				$arrRetun['page2']=$url->getValue();
			}			
		}
		if(empty($arrRetun['AttestationSimplifiee']))
		{
			$url=HelperFunctions::findValueFromDataUID(Constans::OPER_PAGE_VERSION['oper']['AttestationSimplifiee'],$dataOperDB);
			if(!is_null($url))
			{
				$arrRetun['AttestationSimplifiee']=$url->getValue();
			}			
		}
		if(empty($arrRetun['urlPrevisitte']))
		{
			$url=HelperFunctions::findValueFromDataUID(Constans::OPER_PAGE_VERSION['oper']['urlPrevisitte'],$dataOperDB);
			if(!is_null($url))
			{
				$arrRetun['urlPrevisitte']=$url->getValue();
			}			
		}
		return $arrRetun;
	}

	private function replaceMetkaArrs(&$metksArr,$dopSettings=[])
	{
		$arrReplace=[];
		//выделяем позиции для замены
		foreach ($metksArr as $key => $value) 
		{
			if(!empty($value["sett"]["dopSettMetka"]))
			{
				$arrReplace[$key]=$value;
			}
		}
		$resOrder=[];$countIter=0;
		$fl=HelperFunctions::createOrderReplaceMetka($arrReplace,$resOrder,$countIter,$this);
		if(!$fl)
		{
			return false;
		}
		$dataMetkaArr=isset($dopSettings['mArr'])?$dopSettings['mArr']:[];
		foreach ($resOrder as $key => $value) 
		{
			$newV=HelperFunctions::replaceMetkaValue($key,$value['v'],$value['sett']['dopSettMetka'],$metksArr,$dataMetkaArr);
			$metksArr[$key]['v']=$newV;
		}
		//echo strip_tags(json_encode(['do'=>$arrReplace,'posle'=>$resOrder]));
		return true;
	}

	private function mergeKeyToArr($arrMetka,&$arrMetkaAll)
	{
		foreach ($arrMetka as $key => $valueMetka) 
		{
			if(isset($arrMetkaAll[$key]))
			{
				echo $key;
				$this->generateDoc->dataLogs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'text'=>'Дубляж метки: '.$key,
					'data'=>[                               
						'config'=>$valueMetka
					]               
				];
				return false;
			}else
			{
				$arrMetkaAll[$key]=$valueMetka;
			}
		}
		return true;
	}

	private function workDiametre($valueDiametre,$arrMetka,&$diamValueMetka)
	{
		foreach ($arrMetka as $valueMetka) 
		{
			if($valueMetka['metka']!='')
			{
				if(isset($diamValueMetka[$valueMetka['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueMetka['metka'],
						'data'=>[                               
							'config'=>$valueMetka
						]               
					];
					return false;
				}else
				{
					$val='';
					if(isset($valueMetka['request']))
					{
						$val=HelperFunctions::findValueConfigRequest($valueDiametre,$valueMetka['request']);
					}
					if(isset($valueMetka['format']))
					{
						$val=HelperFunctions::execFormatValue($val,$valueMetka['format']);
					}
					$diamValueMetka[$valueMetka['metka']]=['v'=>$val,'type'=>'string','sett'=>$valueMetka];
				}
			}
		}
		return true;
	}

	
	private function createPDFFiles($dataPages)
	{
		$config = new  \Phalcon\Config\Adapter\Json(Constans::PATH_CONF_GLOBAL);
		$pathFolder=Constans::PATH_FOLDER_GENERATE.$this->generateDoc->UID_FOLDER;
		
		$filesGenerate=[];
		foreach ($dataPages as $numPage => $value) 
		{			
			$value['data']=isset($value['dataMetka'])? $value['dataMetka']:[];
			$uniqID=($numPage+1).'_'.uniqid();
            $fileTXT_path=$pathFolder.'/'.$uniqID.'.txt';

            $fileJSON=fopen($fileTXT_path, "w");
            fwrite($fileJSON, json_encode(['data'=>$value['data'],'url'=>$value['url'],'id'=>$uniqID,'path'=>$pathFolder.'/']));            
            fclose($fileJSON);
            $pdfCmdExec='';
            $fileNohup=$pathFolder.'/'.$uniqID.'.nohup';
            $method='generateDocnew';
            switch ($config['modeConvert']) 
            {
                case 'uLibra':
                    $pdfCmdExec='php  '.BASE_PATH.'/cli.php enemat  '.$method.' '.$fileTXT_path;
                    break;
                case 'acermi':
                case 'Racermi':
                    if($config['typeGenerate']=='sequentially')
                    {
                        $pdfCmdExec='php  '.BASE_PATH.'/cli.php enemat  '.$method.' '.$fileTXT_path;
                    }else
                    {
                        $pdfCmdExec='nohup php  '.BASE_PATH.'/cli.php enemat  '.$method.' '.$fileTXT_path.' > '.$fileNohup.' &';
                    }                    
                    break;
            }
            exec($pdfCmdExec);
            $filesGenerate[]=$uniqID;
            $this->generateDoc->dataLogs[]=[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Запускаем поток',
	            'data'=>
	            [
	            	'cmd'=>$pdfCmdExec
	            ]
	        ];	
		}
		$numIterr=0;
		$allCountPages=count($dataPages);
		$numIterr=0;
        while(1)
        {
            $numIterr++;
            $countPDF=0;
            $countNohup=0;
            if($handle = opendir($pathFolder))
            {
                while(false !== ($file = readdir($handle))) 
                {
                    if($file != "." && $file != "..")
                    {
                        $ext=pathinfo($file, PATHINFO_EXTENSION);
                        if($ext=='logt')
                        {
                            $countPDF++;
                        }
                        if($ext=='nohup')
                        {
                            $countNohup++;
                        }
                        clearstatcache();
                    }
                }
            }
            if($countPDF>=count($filesGenerate))
            {
                break;
            }
            if($numIterr>600)
            {                
                $this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'При генерации произошла PDF ошибка',
		            'err'=>true
		        ];	
		       	return false;
            }
            sleep(1);
        }
        
        clearstatcache();
        $pdf = new \Jurosh\PDFMerge\PDFMerger;
        foreach ($filesGenerate as $key => $value) 
        {
            $pdfF=$pathFolder.'/'.$value.'_f.pdf';
            clearstatcache();
            if(file_exists($pdfF))
            {
                $pdf->addPDF($pdfF, 'all', 'vertical'); 
            }else
            {            	
            	$this->generateDoc->dataLogs[]=[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'При объединении PDF произошла ошибка',
		            'err'=>true,
		            'data'=>
		            [
		            	'file'=>$pdfF
		            ]
		        ];	                          	
		       	return false;                
            }
        }
        $pdfFinal=$pathFolder.'/result.pdf';
        if(file_exists($pdfFinal))
        {
        	$pdfFinal=$pathFolder.'/result1.pdf';
        }
        $pdf->merge('file',$pdfFinal);
        return $pdfFinal; 
	}

	private function getDateDevis($copyRequest)
	{	
		$dateDeviss=HelperFunctions::findValueConfigRequest($copyRequest,Constans::KEY_DATE_DEVIS);
		if(empty($dateDeviss))
		{
			return new \DateTime();
		}else
		{
			return \DateTime::createFromFormat(\Dzeta\Core\Value\DateTimeV::DATE_FORMAT_new,$dateDeviss);
		}
	}

	private function getNameFirstOperation($copyOperation)
	{
		if(isset($copyOperation[0]))
		{
			$nameOper=HelperFunctions::findValueConfigRequest($copyOperation[0],Constans::KEY_OPERATION_NAME);
			$nameOper3=substr($nameOper,0,3);
			return ['nameOper'=>$nameOper,'nameOper3'=>$nameOper3];
		}
		return null;
	}

	private function getUidScenario($operFirst,$typeDocument,$typePersone)
	{
		if(isset(Constans::OBJ_SCENARIO[$typeDocument]))
		{
			if(isset(Constans::OBJ_SCENARIO[$typeDocument][$typePersone]))
			{
				if(Constans::OBJ_SCENARIO[$typeDocument][$typePersone][$operFirst])
				{
					return Constans::OBJ_SCENARIO[$typeDocument][$typePersone][$operFirst]['obj'];
				}
			}
		}
		return false;
	}
	
	

	private function getPageDateVersion($objParent,$configPage,$dateDeviss,$typePageText)
	{
		$_obj=new \Dzeta\Models\Obj();
		$dtVers=$_obj->getChildrenByType($objParent,
  			new \Dzeta\Core\Type\Type($configPage['version']['type'], ''));
		$dataVers=$_obj->getAllValues($dtVers);
		$page=null;
		foreach ($dataVers as $valueDataVers) 
		{
			$urlVersion=null;
            $date1Version=null;
            $date2Version=null;
            $sPageVersion=null;
            foreach ($valueDataVers as $valueAttr) 
            {
            	$attrrUi=$valueAttr->getUid();
            	switch ($attrrUi) 
            	{
            		case $configPage['version']['attrs']['date1']:
            			$date1Version=$valueAttr->getValue();
            			break;
            		case $configPage['version']['attrs']['date2']:
            			$date2Version=$valueAttr->getValue();
            			break;
            		case $configPage['version']['attrs']['url']:
            			$urlVersion=$valueAttr->getValue();
            			break;
            		case $configPage['version']['attrs']['sPage']:
            			$sPageVersion=$valueAttr->getValue();
            			break;
            	}
            }
            if(!is_null($date1Version)&&!is_null($date2Version))
            {
            	if($date1Version->getValueN()<$dateDeviss&&$date2Version->getValueN()>$dateDeviss)
            	{
            		if(!is_null($urlVersion))
            		{
            			$page['url']=$urlVersion->getValue();
            			$page['typePage']=$typePageText;
            			if(!is_null($sPageVersion))
	            		{
	            			$page['sPage']=$sPageVersion->getValue();
	            		}
            		}
            	}
            }
		}
		return $page;
	}

	

	private function workOperation($valueOperation,$opersMetka,&$operMetka,$dopSetting=[])
	{
		$incitation=null;$surface=null;$opersTitle=null;
		foreach ($opersMetka as  $valueMetka) 
		{
			if($valueMetka['metka']!='')
			{
				if(isset($operMetka[$valueMetka['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueMetka['metka'],
						'data'=>[                               
							'config'=>$valueMetka
						]               
					];
					return false;
				}else
				{
					$val='';
					$valDB=false;
					if(isset($valueMetka['request']))
					{
						$val=HelperFunctions::findValueConfigRequest($valueOperation,$valueMetka['request']);
					}else if(isset($valueMetka['uid']))
					{						
						$val=HelperFunctions::findValueFromDataUID($valueMetka['uid'],$dopSetting['dataOperDB']);
						$valDB=true;
					}	
					if(isset($valueMetka['action']))
					{						
						switch ($valueMetka['action']) 
						{							
							case 'incitation':
								$incitation=$val;
								break;
							case 'surface':
								$surface=$val;
								break;
							case 'opersTitle':
								$opersTitle=$val;
								break;
						}
					}	
					if($valDB)
					{
						if(is_null($val))
						{
							$val='';
						}else
						{
							$val=$val->getValue();
						}						
					}	
					if(isset($valueMetka['format']))
					{
						$val=HelperFunctions::execFormatValue($val,$valueMetka['format']);
					}		
					$operMetka[$valueMetka['metka']]=['v'=>$val,'type'=>'string','sett'=>$valueMetka];
				}
			}
		}
		return ['incitation'=>$incitation,'surface'=>$surface,'opersTitle'=>$opersTitle];
	}
	private function workSimpleAdvParamsIsolant($isolantsMetka,$valueIsolant,&$isolantValueMetka,$dopSetting=[])
	{
		foreach ($isolantsMetka as $valueMetka) 
		{
			if($valueMetka['metka']!='')
			{
				if(isset($isolantValueMetka[$valueMetka['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueMetka['metka'],
						'data'=>[                               
							'config'=>$valueMetka
						]               
					];
					return false;
				}else
				{
					$val='';
					$flagWrite=true;
					if(isset($valueMetka['request']))
					{
						$val=HelperFunctions::findValueConfigRequest($valueIsolant,$valueMetka['request']);
					}
					if($flagWrite)
					{
						if(isset($valueMetka['format']))
						{
							$val=HelperFunctions::execFormatValue($val,$valueMetka['format']);
						}
						$isolantValueMetka[$valueMetka['metka']]=['v'=>$val,'type'=>'string','sett'=>$valueMetka];
					}					
				}
			}
		}
		return true;
	}
	private function workSimpleIsolant($isolantsMetka,$valueIsolant,&$isolantValueMetka,$dopSetting=[])
	{
		//$dopSetting  dataIsolDB
		$configIncititation=null;$surfaceIsolant=null;$config_totalTTC=null;
		foreach ($isolantsMetka as $valueMetka) 
		{
			if($valueMetka['metka']!='')
			{
				if(isset($isolantValueMetka[$valueMetka['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueMetka['metka'],
						'data'=>[                               
							'config'=>$valueMetka
						]               
					];
					return false;
				}else
				{
					$val='';
					$flagWrite=true;
					if(isset($valueMetka['request']))
					{
						$val=HelperFunctions::findValueConfigRequest($valueIsolant,$valueMetka['request']);
					}else if(isset($valueMetka['uid']))
					{						
						$valDB=HelperFunctions::findValueFromDataUID($valueMetka['uid'],$dopSetting['dataIsolDB']);
						
						if(!is_null($valDB))
						{
							$val=$valDB->getValue();
						}else
						{
							$val='';
						}
					}
					if(isset($valueMetka['action']))
					{
						switch ($valueMetka['action']) 
						{
							case 'incitation':
								$flagWrite=false;
								$configIncititation=$valueMetka;
								break;
							case 'surface':
								$surfaceIsolant=$val;
								break;
							case 'totalTTC':
								$flagWrite=false;
								$config_totalTTC=$valueMetka;
								break;
						}
					}
					if($flagWrite)
					{
						if(isset($valueMetka['format']))
						{

							$val=HelperFunctions::execFormatValue($val,$valueMetka['format']);
						}
						$isolantValueMetka[$valueMetka['metka']]=['v'=>$val,'type'=>'string','sett'=>$valueMetka];
					}					
				}
			}
		}
		if(!is_null($configIncititation))
		{
			if($configIncititation['metka']!=''&&!is_null($surfaceIsolant)&&isset($dopSetting['operTTC']))
			{
				$val=$surfaceIsolant*$dopSetting['operTTC'];
				if(isset($configIncititation['format']))
				{
					$val=HelperFunctions::execFormatValue($val,$configIncititation['format']);
				}
				$isolantValueMetka[$configIncititation['metka']]=['v'=>$val,'type'=>'string','sett'=>$configIncititation];
			}
		}
		if(!is_null($config_totalTTC))
		{
			if($config_totalTTC['metka']!=''&&!is_null($surfaceIsolant)&&isset($dopSetting['prix_unitare']))
			{		
				$val=$surfaceIsolant*$dopSetting['prix_unitare'];
				if(isset($config_totalTTC['format']))
				{
					$val=HelperFunctions::execFormatValue($val,$config_totalTTC['format']);
				}		
				$isolantValueMetka[$config_totalTTC['metka']]=['v'=>$val,'type'=>'string','sett'=>$config_totalTTC];
			}
		}
		return ['surfaceIsolant'=>$surfaceIsolant];
	}

	private function workOpersTableMetka($opersTableMetka,$valueOperation,&$opersTableData,$uidOperation,$dopSetting=[])
	{
		$prixUnitareOper=null;
		$tableTTCOper=null;		
		$dataOperValue=HelperFunctions::getDataFromCahsheDB($uidOperation,$this->generateDoc);
		$prixUnitareValue=null;
		$cumacValue=null;
		$resteAchargeValue=null;
		$surfaceValue=null;
		$mainDoeuvreConf=null;
		$prixWorkValue=null;
		foreach ($opersTableMetka as $valueOperTable) 
		{
			if($valueOperTable['metka']!='')
			{
				if(isset($opersTableData[$valueOperTable['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$valueOperTable['metka'],
						'data'=>[                               
							'config'=>$valueOperTable
						]               
					];                        
					return false;
				}else
				{
					$val='';
					$flagAdd=true;
					$valDB=false;
					if(isset($valueOperTable['request']))
					{
						$val=HelperFunctions::findValueConfigRequest($valueOperation,$valueOperTable['request']);
					}else if(isset($valueOperTable['uid']))
					{                           
						$val=HelperFunctions::findValueFromDataUID($valueOperTable['uid'],$dataOperValue);
						$valDB=true;
					}
					if($valDB)
					{
						$val=is_null($val)? '':$val->getValue();
					}
					if(isset($valueOperTable['action']))
					{
						switch ($valueOperTable['action']) 
						{
							/*case 'tableTTC':
								$flagAdd=false;
								$tableTTCOper=['v'=>'','type'=>'string','sett'=>$valueOperTable];
								break;*/
							case 'prixeUnitare':					
								$prixUnitareValue=$val;			
								$prixUnitareOper=['v'=>$val,'type'=>'string','sett'=>$valueOperTable];
								break;
							case 'resteAcharge':
								$resteAchargeValue=$val;
								break;
							case 'cumac':
								$cumacValue=$val;
								break;
							case 'mainDoeuvre':
								$mainDoeuvreConf=$valueOperTable;
								break;
							case 'surfaceOper':
								$surfaceValue=$val;
								break;
							case 'prixWork':
								$prixWorkValue=$val;
								break;
							case 'unit':
								if($val=='')
								{
									$val=$dopSetting['defUnit'];
								}
								break;
						}
					}
					
					if($flagAdd)
					{
						if(isset($valueOperTable['format']))
						{
							$val=HelperFunctions::execFormatValue($val,$valueOperTable['format']);
						}
						$opersTableData[$valueOperTable['metka']] =['v'=>$val,'type'=>'string','sett'=>$valueOperTable];
					}
				}
			}
		}
		if(!is_null($mainDoeuvreConf))
		{
			if($mainDoeuvreConf['metka']!=''&&!is_null($prixWorkValue)&&!is_null($surfaceValue))
			{
				$val=$prixWorkValue*$surfaceValue;
				if(isset($mainDoeuvreConf['format']))
				{
					$val=HelperFunctions::execFormatValue($val,$mainDoeuvreConf['format']);
				}
				$opersTableData[$mainDoeuvreConf['metka']]=['v'=>$val,'type'=>'string','sett'=>$mainDoeuvreConf];
			}
		}
		return ['prix_unitare'=>$prixUnitareValue,'cumac'=>$cumacValue,'resteAcharge'=>$resteAchargeValue];
	}

	

	private function workOperCheckBox($valueOperation,$opersCheckMetka,&$operCheckMetka)
	{
		$operObjsUID=HelperFunctions::findValueConfigRequest($valueOperation,Constans::KEY_OPERATION_OBJID);
		$namesObjsUID=array_column($operObjsUID,'label');
		
		foreach ($opersCheckMetka as $value_operCheck) 
		{
			if($value_operCheck['metka']!='')
			{
				if(isset($operCheckMetka[$value_operCheck['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$value_operCheck['metka'],
						'data'=>[                               
							'config'=>$value_operCheck
						]               
					];
					return false;
				}else
				{
					$val=0;
					$k_sear=array_search($value_operCheck['name'], $namesObjsUID);
					if($k_sear!==false)
					{
						$val=1;
					}					
					$operCheckMetka[$value_operCheck['metka']]=['v'=>$val,'type'=>'checkbox','sett'=>$value_operCheck];
				}
			}
		}
		return true;
	}

	

	private function workBureDeControle($valueOperation,$operBureMetka)
	{
		$bureUID=HelperFunctions::findValueConfigRequest($valueOperation,Constans::KEY_BURE_UID);
		if(!empty($bureUID))
		{
			//значит надо делать запрос к БД
			$dataBureValue=HelperFunctions::getDataFromDB($bureUID);
			foreach ($operBureMetka as $valueBure) 
			{
				if($valueBure['metka']!='')
				{
					if(isset($bureMetka[$valueBure['metka']]))
					{
						$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Дубляж метки: '.$valueBure['metka'],
							'data'=>[                               
								'config'=>$valueBure
							]               
						];
						return false;
					}else
					{
						//тут потом ищем в значениях бд
						$valDB=HelperFunctions::findValueFromDataUID($valueBure['uid'],$dataBureValue);
						if(is_null($valDB))
						{
							$valDB='';
						}
						if(!empty($valueBure['action']))
						{
							switch ($valueBure['action']) 
							{
								case 'dateString':
									if(!empty($valDB))
									{
										$valDB=$valDB->getValueN()->format('d/m/Y');
									}
									break;
							}
						}
						if(isset($valueBure['format']))
						{
							$valDB=HelperFunctions::execFormatValue($valDB,$valueBure['format']);
						}
						$bureMetka[$valueBure['metka']]=['v'=>$valDB,'type'=>'string','sett'=>$valueBure];
					}                       
				}
			}
		}else
		{
			foreach ($operBureMetka as $valueBure) 
			{
				if($valueBure['metka']!='')
				{
					if(isset($bureMetka[$valueBure['metka']]))
					{
						$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Дубляж метки: '.$valueBure['metka'],
							'data'=>[                               
								'config'=>$valueBure
							]               
						];
						return false;
					}else
					{
						$bureMetka[$valueBure['metka']]=['v'=>'','type'=>'string','sett'=>$valueBure];
					}                       
				}
			}
		}
		return $bureMetka;
	}
	private function getAcode($copyOperData)
	{
		$aCode=false;
		if(count($copyOperData)>1)
		{
			$aCode=true;
		}else if(isset($copyOperData[0]))
		{
			$dataIsolant=HelperFunctions::findValueConfigRequest($copyOperData[0],Constans::KEY_OPERATION_ISOL);
			if(count($dataIsolant)>1)
			{
				$aCode=true;
			}
		}
		return $aCode;
	}

	private function getTax($copyOperData,$copyDataRequest)
	{
		$flagTax=false;
		$defaultTax=null;
		if(isset($copyOperData[0]))
		{
			//берем первую операцию она должна BAT
			$textOper=HelperFunctions::findValueConfigRequest($copyOperData[0],Constans::SPEC_BLOCK_TAX['defTaxSpec']['keysName']);
			$textOper=substr($textOper,0,3);
			$textOther=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::SPEC_BLOCK_TAX['defTaxSpec']['keys']);
			$textOther=mb_strtoupper($textOther);
			$textSpecArr=explode(' ',$textOther);
			foreach (Constans::SPEC_BLOCK_TAX['defTaxSpec']['textArr'] as  $value) 
			{
				if(in_array($value, $textSpecArr))
				{
					$flagTax=true;
					$defaultTax=Constans::SPEC_BLOCK_TAX['defTaxSpec']['value'];
					break;
				}
			}
			
		}else
		{
			return false;
		}
		if(empty($flagTax))
		{
			//теперь нам надо смотреть TAX у первой операции, и ее значение будет для всех
			$uidOperation=HelperFunctions::findValueConfigRequest($copyOperData[0],Constans::KEY_OPERATION_UID);
			$dataOperDB=HelperFunctions::getDataFromCahsheDB($uidOperation,$this->generateDoc);
			$valTAX=HelperFunctions::findValueFromDataUID(Constans::SPEC_BLOCK_TAX['uidOperationTax'],$dataOperDB);
			if(!is_null($valTAX))
			{
				$defaultTax=$valTAX->getValue();
			}else
			{
				$dtTax=HelperFunctions::getDataFromDB(Constans::SPEC_BLOCK_TAX['defaultObjTax']['obj']);
				$tmp=HelperFunctions::findValueFromDataUID(Constans::SPEC_BLOCK_TAX['defaultObjTax']['attr'],$dtTax);
				if(is_null($tmp))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Ошибка при получении tax',
						'err'=>true				
					];
					return false;
				}
				$defaultTax=$tmp->getValue();
			}
		}
		return ['flagTax'=>$flagTax,'defaultTax'=>$defaultTax];
	}

	private function worksimpleDBValue($allMetkaElems,$copyDataRequest,&$metksGlobalArr)
	{
		$simpleDBMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'simpleDB');
		$dataDB=[];
		//организация
		$orgUID=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::KEY_ORGANISATION_UID);
		if(!empty($orgUID))
		{
			$dataDB=[...HelperFunctions::getDataFromDB($orgUID),...$dataDB];
		}            
		$dataDB=[...HelperFunctions::getDataFromDB($this->generateDoc->USER->getUid()),...$dataDB];
		$dataDBnew=[];
		foreach ($dataDB as $value) 
		{
			$dataDBnew[$value->getUid()]=$value;
		}
		$uLogo=null;
		$uLogoUpload=null;
		foreach ($simpleDBMetka as $value) 
		{
			if($value['metka']!='')
			{
				if(isset($metksGlobalArr[$value['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$value['metka'],
						'data'=>[                               
							'config'=>$value
						]               
					];
					return false;
				}
				if(isset($value['uid']))
				{
					if(isset($dataDBnew[$value['uid']]))
					{
						$flagAdd=true;
						$val='';
						if(isset($value['action']))
						{
							switch ($value['action']) 
							{
								case 'uLogo':
									if(!is_null($dataDBnew[$value['uid']]->getValue()))
									{
										$uLogo=['sett'=>$value,'value'=>$dataDBnew[$value['uid']]];
										$flagAdd=false;
									}
									break;
								 case 'uLogoUpload':     
									if(!is_null($dataDBnew[$value['uid']]->getValue()))
									{                               
										$uLogoUpload=['sett'=>$value,'value'=>$dataDBnew[$value['uid']]]; 
										$flagAdd=false;
									}
									break;
							}
						}
						if($flagAdd)
						{
							if(!is_null($dataDBnew[$value['uid']]->getValue()))
							{
								switch ($dataDBnew[$value['uid']]->getDatatype()) 
								{
									case \Dzeta\Core\Type\DataType::TYPE_STRING:
									case \Dzeta\Core\Type\DataType::TYPE_TEXT:
									case \Dzeta\Core\Type\DataType::TYPE_NUMBER:                                        
										$val=$dataDBnew[$value['uid']]->getValue()->getValue(); 
										if(isset($value['format']))
										{
											$val=HelperFunctions::execFormatValue($val,$value['format']);
										}
										$metksGlobalArr[$value['metka']]=['v'=>$val,'type'=>'string','sett'=>$value];                                       
										break;
									case \Dzeta\Core\Type\DataType::TYPE_DATETIME:
										$val=$dataDBnew[$value['uid']]->getValue()->getValueN()->format('d/m/Y');
										$metksGlobalArr[$value['metka']]=['v'=>$val,'type'=>'string','sett'=>$value];
										break;
									case \Dzeta\Core\Type\DataType::TYPE_FILE:
										$val=$dataDBnew[$value['uid']]->getValue()->getPath(); 
										$metksGlobalArr[$value['metka']]=['v'=>$val,'type'=>'file','sett'=>$value];
										break;
								}
							}                                
						}
					}else
					{
						$metksGlobalArr[$value['metka']]=['v'=>'','type'=>'string','sett'=>$value];
					}
				}
			}
		}
		
		if(true)
		{
			//обработка uLogo и uLogoUpload
			if(!empty($uLogoUpload))
			{
				$metksGlobalArr[$uLogoUpload['sett']['metka']]=['v'=>$uLogoUpload['value']->getValue()->getPath(),'type'=>'file','sett'=>$uLogoUpload['sett']];
			}else if(!empty($uLogo))
			{
				$ur=$uLogo['value']->getValue()->getValue();
				preg_match('/\/d\/(.*?)\//', $ur, $output_array);
				if(empty($output_array))
				{
					$metksGlobalArr[$uLogo['sett']['metka']]=['v'=>'','type'=>'string','sett'=>$uLogo['sett']];
				}else
				{
					$urll="https://drive.google.com/u/0/uc?id={$output_array[1]}&export=download";
					$metksGlobalArr[$uLogo['sett']['metka']]=['v'=>$urll,'type'=>'file','sett'=>$uLogo['sett']];
				}
			}
		}  
		return true;
	}

	private function workMaprenov($copyDataRequest,$DATENOW)
	{
		$CONFIGGLOBAL=
		[
			'maprenov'=>
			[
				'colorCoef'=>null,
				'attrOper'=>null,
				'maxLimit'=>null,
				'text'=>''
			]
		];

		$_obj=new \Dzeta\Models\Obj();

		$codePostaleM=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['codePostalKey']);
		$revenuM=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['revenueKey']);
		$numDePersonM=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['numberDePersne']);
		if(!is_null($revenuM)&&!is_null($codePostaleM)&&!is_null($numDePersonM))
		{
			$ledeFrance=false;
			$codePostaleRegionM=substr($codePostaleM, 0, 2);
			if(in_array($codePostaleRegionM, Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['codePostale_le-de-France']))
			{
				$ledeFrance=true;
			}
			//теперь надо искать версию объекта
			$allObjects=$_obj->getChildrenByType(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''),
				new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['mainType']['type'], ''));
			$_obj->getAllValues($allObjects);
			$parentMaprenov=null;
			foreach ($allObjects as $valueObject) 
			{
				$attributesAll=$valueObject->getAttributes();
				$dtStart=null;
				$dtFinis=null;
				foreach ($attributesAll as $val) 
				{
					if(!is_null($val->getValue()))
					{
						switch ($val->getUid()) 
						{
							case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['mainType']['attrs']['start']:
								$dtStart=$val->getValue()->getValueN();
								break;
							case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['mainType']['attrs']['finish']:
								$dtFinis=$val->getValue()->getValueN();
								break;
						}
					}
				}
				if(!is_null($dtStart)&&!is_null($dtFinis))
				{
					if($dtStart<$DATENOW&&$DATENOW<$dtFinis)
					{
						$parentMaprenov=$valueObject->getUid();
						break;
					}
				}
			}

			if(!is_null($parentMaprenov))
			{
				$allObjects=$_obj->getChildrenByType(new \Dzeta\Core\Instance($parentMaprenov, ''),
				new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['type'], ''));
				$_obj->getAllValues($allObjects);
				$indArr=[];
				foreach ($allObjects as $valueObject) 
				{
					$attributesAll=$valueObject->getAttributes();
					$countAttr=null;
					$priceBleu=null;
					$priceJaune=null;
					$priceViolet=null;
					foreach ($attributesAll as $val) 
					{
						if(!is_null($val->getValue()))
						{
							switch ($val->getUid()) 
							{
								case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['attrs']['count']:
									$countAttr=$val->getValue()->getValue();
									break;                                    
							}
							if($ledeFrance)
							{
								switch ($val->getUid()) 
								{
									case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['attrsColour']['bleu']['ledeFrance']:
										$priceBleu=$val->getValue()->getValue();
										break;  
									case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['attrsColour']['jaune']['ledeFrance']:
										$priceJaune=$val->getValue()->getValue();
										break;
									case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['attrsColour']['violet']['ledeFrance']:
										$priceViolet=$val->getValue()->getValue();
										break;                                  
								}
							}else
							{
								switch ($val->getUid()) 
								{
									case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['attrsColour']['bleu']['autresRegions']:
										$priceBleu=$val->getValue()->getValue();
										break;  
									case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['attrsColour']['jaune']['autresRegions']:
										$priceJaune=$val->getValue()->getValue();
										break;
									case Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['childType']['attrsColour']['violet']['autresRegions']:
										$priceViolet=$val->getValue()->getValue();
										break;                                  
								}
							}
						}
					}
					if(!in_array(null, [$countAttr,$priceBleu,$priceJaune,$priceViolet],true))
					{
						$indArr[$countAttr]=['blue'=>$priceBleu,'jaune'=>$priceJaune,'violet'=>$priceViolet];
					}
				}
				$priceMaxColor=null;
				if(!empty($indArr))
				{
					if(isset($indArr[$numDePersonM]))
					{
						$priceMaxColor=$indArr[$numDePersonM];
					}else
					{
						$arrKeyss=array_keys($indArr);
						$maxKey=max($arrKeyss);
						if($maxKey<$numDePersonM)
						{
							$delta=$numDePersonM-$maxKey;
							$priceMaxColor['blue']=$delta*$indArr[0]['blue']+$indArr[$maxKey]['blue'];
							$priceMaxColor['jaune']=$delta*$indArr[0]['jaune']+$indArr[$maxKey]['jaune'];
							$priceMaxColor['violet']=$delta*$indArr[0]['violet']+$indArr[$maxKey]['violet'];
						}
					}
				}
				if(!is_null($priceMaxColor))
				{
					$objColor=null;
					if($priceMaxColor['blue']>=$revenuM)
					{
						$objColor=Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['systemSett']['bleu'];
					}else if($priceMaxColor['blue']<$revenuM&&$priceMaxColor['jaune']>=$revenuM)
					{
						$objColor=Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['systemSett']['jaune'];
					}else if($priceMaxColor['jaune']<$revenuM&&$priceMaxColor['violet']>=$revenuM)
					{
						$objColor=Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['systemSett']['violet'];
					}else if($priceMaxColor['violet']<$revenuM)
					{
						$objColor=Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['systemSett']['rose'];
					}
					if(!is_null($objColor))
					{
						$dataColors=HelperFunctions::getDataFromDB($objColor['obj']);
						$valColorOper=HelperFunctions::findValueFromDataUID($objColor['attr'],$dataColors);
						if(!is_null($valColorOper))
						{
							$CONFIGGLOBAL['maprenov']['colorCoef']=$valColorOper->getValue();
							$CONFIGGLOBAL['maprenov']['attrOper']=$objColor['attrOper'];
							//раз мы зашли сюда, то считаем максимальный лимит
							$dataMaxLimit=HelperFunctions::getDataFromDB(Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['systemSett']['maxLimit']['obj']);
							$valMaxLimit=HelperFunctions::findValueFromDataUID(Constans::GLOBAL_SETT_CONFIG['Maprimrenov']['systemSett']['maxLimit']['attr'],$dataMaxLimit);
							if(!is_null($valMaxLimit))
							{
								$CONFIGGLOBAL['maprenov']['maxLimit']=$valMaxLimit->getValue();
							}
						}
						
					}
				}
			}
		}
		return $CONFIGGLOBAL;
	}

	private function workSousTraitanceMetka($allMetkaElems,$copyDataRequest)
	{
		$SousTraitanceMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'simpleSousTraitance');
		$sousTrantceExist=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::KEY_SOUS_TRAINTCE);
		$sousMetkaArr=[];
		foreach ($SousTraitanceMetka as $value) 
		{
			if($value['metka']!='')
			{
				if(isset($sousMetkaArr[$value['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$value['metka'],
						'data'=>[                               
							'config'=>$value
						]               
					];
					return false;
				}
				if(isset($value['request']))
				{
					$val=HelperFunctions::findValueConfigRequest($copyDataRequest,$value['request']);
				}
				if(isset($value['action']))
				{
					switch ($value['action']) 
					{
						case 'siret':
							if(!is_null($val))
							{
								$val=HelperFunctions::clearSiretValue($val);
							}
							break;	
						case 'check_non':
							$val=empty($val)? 1:0;
							break;	
						case 'check_on':
							$val=empty($val)? 0:1;
							break;					
						case 'sousTraitanceDevis':
							if(!empty($sousTrantceExist)||true)
							{
								//тут делаем запрос в бд к объекту
								$val=HelperFunctions::action_sousTraintceDevis();
							}
							break;
					}
				}
				if(strpos($value['metka'],'Check.')===0)
				{
					$sousMetkaArr[$value['metka']]=['v'=>$val,'type'=>'checkbox','sett'=>$value];
				}else
				{
					if(empty($sousTrantceExist))
					{
						$val='';
					}
					$sousMetkaArr[$value['metka']]=['v'=>$val,'type'=>'string','sett'=>$value];
				}

				
			}
		}
		return $sousMetkaArr;
	}


	private function workBeneficiareMetka($allMetkaElems,$copyDataRequest)
	{
		$metksBenecifiareArr=[];
		$dataBeneficiareDop=[];

		$beneficiareMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'simpleBeneficiare');
		$beneficiareNumero=null;
		$beneficiareRefere=null;  
		$beneficiareTable =null;   
		$beneficiareTableA=null;
		$beneficiareTableB=null;   
		foreach ($beneficiareMetka as $value) 
		{
			if($value['metka']!='')
			{
				$val=null;
				$fileFlag=false;
				$metkaAdd=true;
				if(isset($value['request']))
				{
					$val=HelperFunctions::findValueConfigRequest($copyDataRequest,$value['request']);
				}
				if(isset($metksBenecifiareArr[$value['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$value['metka'],
						'err'=>true,
						'data'=>[                               
							'config'=>$value
						]               
					];
					return false;
				}   
				if(!empty($value['action']))
				{
					switch ($value['action']) 
					{
						//beneficiareTable  beneficareNumFiscal beneficareRefAvis  beneficareFamilyy                        
						case 'beneficareRepresentePar':
							$val=HelperFunctions::action_beneficareRepresentePar($copyDataRequest,$this);
							if($val===false)
							{
								echo 777;
								return false;
							}
							break;
						case 'beneficareNumPerson':
							$val=HelperFunctions::action_beneficareNumPerson($copyDataRequest,$this);
							break;
						case 'beneficareFamilyy':
							$val=HelperFunctions::action_beneficareFamilyy($copyDataRequest,$this);
							break;
						case 'numOfLines':
							$val=HelperFunctions::action_beneficareNumOfLines($copyDataRequest,$this);
							break;                           
						case 'siren':
							$val=HelperFunctions::clearSirenValue($val);
							break;
						case 'beneficareNumFiscal':
							$metkaAdd=false;
							$beneficiareNumero=$value;
							break;
						case 'beneficareRefAvis':
							$metkaAdd=false;
							$beneficiareRefere=$value;
							break;
						case 'beneficiareTable':
							$metkaAdd=false;
							$beneficiareTable=$value;
							break;
						case 'beneficiareTableA':
							if(!empty($val))
							{
								$beneficiareTableA=true;
							}
							break;
						case 'beneficiareTableB':
							if(!empty($val))
							{
								$beneficiareTableB=true;
							}
							break;
						default:
							$this->generateDoc->dataLogs[]=[
								'time'=>date('d/m/Y H:i:s'),
								'text'=>'Неизвестный action в бенефициаре: '.$value['action'],
								'err'=>true,
								'data'=>[                               
									'config'=>$value
								]               
							];
							return false;
							break;

					}
				}           
				if(empty($fileFlag)&&$metkaAdd)
				{              
					if(isset($value['format']))
					{
						$val=HelperFunctions::execFormatValue($val,$value['format']);
					}    
					if(strpos($value['metka'],'Check.')===0)
					{
						$val=empty($val)? 0:1;
						$metksBenecifiareArr[$value['metka']]=['v'=>$val,'type'=>'checkbox','sett'=>$value];
					}else
					{
						$metksBenecifiareArr[$value['metka']]=['v'=>$val,'type'=>'string','sett'=>$value];
					}  
					
				}
			}
		}
		$beneficiarePersonData=HelperFunctions::findValueConfigRequest($copyDataRequest,Constans::KEY_PERSONE_PHYS_DATA);
		if(!empty($beneficiarePersonData)&&!empty($beneficiareNumero)&&!empty($beneficiareRefere)&&!empty($beneficiareTable))
		{
			foreach ($beneficiarePersonData as $value) 
			{
				$valNumero=HelperFunctions::findValueConfigRequest($value,$beneficiareNumero['request']);
				$valReference=HelperFunctions::findValueConfigRequest($value,$beneficiareRefere['request']);
				$valTable='';
				if(!empty($value['JSON']))
				{                        
					$valTable=HelperFunctions::action_beneficareTableSpec($value,$this);
					if(empty($valTable))
					{
						$this->generateDoc->dataLogs[]=[
							'time'=>date('d/m/Y H:i:s'),
							'text'=>'Ошибка в получении меток для таблицы бенефициара 2',
							'err'=>true
						];
						return false;
					}
				}
				
				$oneRow=[];
				$oneRow=[
					$beneficiareNumero['metka']=>['v'=>$valNumero,'type'=>'string','sett'=>$beneficiareNumero],
					$beneficiareRefere['metka']=>['v'=>$valReference,'type'=>'string','sett'=>$beneficiareRefere],
					$beneficiareTable['metka']=>['v'=>$valTable,'type'=>'string','sett'=>$beneficiareTable],
				];
				if($beneficiareTableA)
				{
					$oneRow[$beneficiareNumero['metka'].'.TA']=['v'=>$valNumero,'type'=>'string','sett'=>$beneficiareNumero];
					$oneRow[$beneficiareRefere['metka'].'.TA']=['v'=>$valReference,'type'=>'string','sett'=>$beneficiareRefere];
					$oneRow[$beneficiareTable['metka'].'.TA']=['v'=>$valTable,'type'=>'string','sett'=>$beneficiareTable];                     
				}else
				{
					$oneRow[$beneficiareNumero['metka'].'.TA']=['v'=>'','type'=>'string','sett'=>$beneficiareNumero];
					$oneRow[$beneficiareRefere['metka'].'.TA']=['v'=>'','type'=>'string','sett'=>$beneficiareRefere];
					$oneRow[$beneficiareTable['metka'].'.TA']=['v'=>'','type'=>'string','sett'=>$beneficiareTable];
				}
				if($beneficiareTableB)
				{
					$oneRow[$beneficiareNumero['metka'].'.TB']=['v'=>$valNumero,'type'=>'string','sett'=>$beneficiareNumero];
					$oneRow[$beneficiareRefere['metka'].'.TB']=['v'=>$valReference,'type'=>'string','sett'=>$beneficiareRefere];
					$oneRow[$beneficiareTable['metka'].'.TB']=['v'=>$valTable,'type'=>'string','sett'=>$beneficiareTable];                     
				}else
				{
					$oneRow[$beneficiareNumero['metka'].'.TB']=['v'=>'','type'=>'string','sett'=>$beneficiareNumero];
					$oneRow[$beneficiareRefere['metka'].'.TB']=['v'=>'','type'=>'string','sett'=>$beneficiareRefere];
					$oneRow[$beneficiareTable['metka'].'.TB']=['v'=>'','type'=>'string','sett'=>$beneficiareTable];
				}
				$dataBeneficiareDop[]=$oneRow;
			}
		}else
		{
			/**$this->generateDoc->dataLogs[]=[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Ошибка в получении меток для таблицы бенефициара',
				'err'=>true
			];
			return false;*/
		} 
		//теперь делаем копии массивов для ТаблицыА и таблицыБ
		$copyTMP=$metksBenecifiareArr;
		foreach ($copyTMP as $key => $value) 
		{
			if($beneficiareTableA)
			{
				$metksBenecifiareArr[$key.'.TA']=$value;
			}else
			{
				$metksBenecifiareArr[$key.'.TA']=$value;
				$metksBenecifiareArr[$key.'.TA']['v']='';
			}
			if($beneficiareTableB)
			{
				$metksBenecifiareArr[$key.'.TB']=$value;
			}else
			{
				$metksBenecifiareArr[$key.'.TB']=$value;
				$metksBenecifiareArr[$key.'.TB']['v']='';
			}
		} 
		return ['metkBen'=>$metksBenecifiareArr,'benifiDop'=>$dataBeneficiareDop,'tA'=>$beneficiareTableA,'tB'=>$beneficiareTableB];
	}
	private function workSimpleMetka($allMetkaElems,&$metksGlobalArr,$copyDataRequest)
	{
		$simpleMetka=HelperFunctions::getElemsConfigforTypeElem($allMetkaElems,'simple');
		foreach ($simpleMetka as $value) 
		{
			if($value['metka']!='')
			{
				//тут ищем среди реквестов
				$val=null;
				$fileFlag=false;
				if(isset($value['request']))
				{
					$val=HelperFunctions::findValueConfigRequest($copyDataRequest,$value['request']);
				}
				if(isset($metksGlobalArr[$value['metka']]))
				{
					$this->generateDoc->dataLogs[]=[
						'time'=>date('d/m/Y H:i:s'),
						'text'=>'Дубляж метки: '.$value['metka'],
						'data'=>[								
							'config'=>$value
						]				
					];
					return false;
				}	
				if(!empty($value['action']))
				{
					switch ($value['action']) 
					{
						case 'siren':
							if(!is_null($val))
							{
								$val=HelperFunctions::clearSirenValue($val);
							}							
							break;
						case 'numeroFacture':	
							$val=HelperFunctions::action_numeroFacture($copyDataRequest,$this->generateDoc);
							if($val===false)
							{								
								return false;
							}
							break;
						case 'numDevis':
							$val=HelperFunctions::action_numDevis($copyDataRequest,$this->generateDoc);
							if($val===false)
							{
								return false;
							}
							break;
						case 'agr_gen':
							$val=HelperFunctions::action_agr_gen($copyDataRequest,$this->generateDoc);
							if($val===false)
							{
								return false;
							}
							break;
						case 'inv_gen':
							$val=HelperFunctions::action_inv_gen($copyDataRequest,$this->generateDoc);
							if($val===false)
							{
								return false;
							}
							break;
						case 'numDevisOrig':
							$val=HelperFunctions::action_inv_gen($copyDataRequest,$this->generateDoc);
							if($val===false)
							{
								return false;
							}
							break;
					}
				}			
				if(empty($fileFlag))
				{
					if(isset($value['format']))
					{
						$val=HelperFunctions::execFormatValue($val,$value['format']);
					} 
					$metksGlobalArr[$value['metka']]=['v'=>$val,'type'=>'string','sett'=>$value];
				}				
			}
		}
		return true;
	}

	private function downloadExcelFileGoogleDisk($urlDownload,$nameFile)
	{
		$config = new  \Phalcon\Config\Adapter\Json(Constans::PATH_CONF_GLOBAL);		
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' .BASE_PATH.$config['pathServiceAcc']);
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->setScopes('https://www.googleapis.com/auth/drive');
        $service = new \Google_Service_Drive($client);
        //выделяем id из урла
        preg_match('/\/d\/(.*?)\//', $urlDownload, $output_array);
        if (empty($output_array)) 
        {
        	$this->generateDoc->dataLogs[]=
        	[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Ошибка при выделении id из урла загрузки Excel',
	            'err'=>true,
	            'data'=>
	            [
	            	'url'=>$urlDownload
	            ]
	        ];
            return false;
        }
        $id=$output_array[1];
        try
        {            
            $content = $service->files->export($id,Constans::MIME_TYPE_EXCEL_GOOGLE,['alt' => 'media']); 
            if($content->getStatusCode()==200)
            {
                $outHandle = fopen($nameFile, "w+");                
                while (!$content->getBody()->eof()) 
                {                   
                    fwrite($outHandle, $content->getBody()->read(1024));
                }        
                fclose($outHandle);
                return true;
            }else
            {     
            	$this->generateDoc->dataLogs[]=
	        	[
		            'time'=>date('d/m/Y H:i:s'),
		            'text'=>'Ошибка при скачке файла Excel',
		            'err'=>true,
		            'data'=>
		            [
		            	'statucCode'=>$content->getStatusCode()
		            ]
		        ];  
                return false;
            }  
        }catch(\Exception $err)
        {    
            $this->generateDoc->dataLogs[]=
        	[
	            'time'=>date('d/m/Y H:i:s'),
	            'text'=>'Ошибка при скачке файла Excel 2',
	            'err'=>true,
	            'data'=>
	            [
	            	'err'=>$err->getMessage()
	            ]
	        ];
            return false;
        }        
        return false;
	}
}