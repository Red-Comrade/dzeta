<?php 
//https://mgteam.atlassian.net/browse/FRANCE1-252
namespace generateDocEnemat;

class  objResultEMMY
{
	//процесс который создает объект типа ResultEmmy
	private $copyRequest=[];
	private $generateDoc=null;


	public function __construct($requestData,$objGenDoc)
	{
		$this->copyRequest=$requestData;
		$this->generateDoc=$objGenDoc;
	}

	public function init()
	{
		$copyRequest=$this->copyRequest;
		$dateNow=date('d/m/Y H:i:s');

		$this->generateDoc->dataLogs[]=
		[
			'time'=>date('d/m/Y H:i:s'),
			'text'=>'Начинаем работать с веткой ResultEMMY'
		];
		$_group = new \Dzeta\Models\AttrGroup();
        $_obj=new \Dzeta\Models\Obj();
        if(!file_exists(Constans::PATH_CONFIG_SETT_OBJ_REQUEST_RESULTSEMMY))
        {
        	$this->generateDoc->dataLogs[]=
			[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Отсутсвует файл с настройками ResultsEMMY',
				'err'=>true
			];
        	return false;
        }
        if(!file_exists(Constans::PATH_FILE_EXCEL_CONFIG))
        {
            $this->generateDoc->dataLogs[]=
            [
                'time'=>date('d/m/Y H:i:s'),
                'text'=>'Отсутсвует файл с ключом Excel',
                'err'=>true
            ];
            return false;
        }
        $configParamData=$this->configSettInit();
       
        //тут пилим по изолянтам

        $copyOperations=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_RESULTEMMY_CONFIG_SETT['operations']['key']);
        $copyOperations=empty($copyOperations)? []:$copyOperations;
        $dataExcel=[];
        $objEmmyResUID=[];
        $globalData=[];
        $formData=isset($configParamData['form'])?$configParamData['form']:[];
        $formUser=isset($configParamData['formUser'])?$configParamData['formUser']:[];
        $formOrg=isset($configParamData['formOrg'])?$configParamData['formOrg']:[];
        $formSiret=isset($configParamData['formSiret'])?$configParamData['formSiret']:[];
        $formOperation=isset($configParamData['formOperation'])?$configParamData['formOperation']:[];
        $formIsolant=isset($configParamData['formIsolant'])?$configParamData['formIsolant']:[];
        $formBure=isset($configParamData['formBuro'])?$configParamData['formBuro']:[];
        $formObjUid=isset($configParamData['formObjUid'])?$configParamData['formObjUid']:[];
        
        foreach($formData as $key=>$valueFormData) 
        {
        	$val='';
        	if(isset($valueFormData['keys']))
        	{
        		$val=HelperFunctions::findValueConfigRequest($copyRequest,$valueFormData['keys']);
        	}
        	$globalData[$key]=['v'=>$val,'sett'=>$valueFormData];
        }

        //теперь получаем все данные Юзера
        $dataUser=HelperFunctions::getDataFromDB($this->generateDoc->USER->getUid());
        foreach ($formUser as $key => $valueUser) 
        {
        	$val=HelperFunctions::findValueFromDataUID($valueUser['param'],$dataUser);
        	if(!is_null($val))
        	{
        		$val=$val->getValue();
        	}
        	$globalData[$key]=['v'=>$val,'sett'=>$valueUser];
        }
        $uidUser=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['orgKey']);
        if(!is_null($uidUser))
        {
        	$dataOrg=HelperFunctions::getDataFromDB($uidUser);
        	foreach ($formOrg as $key => $valueOrg) 
        	{
        		$val=HelperFunctions::findValueFromDataUID($valueOrg['param'],$dataOrg);
        		if(!is_null($val))
	        	{
	        		$val=$val->getValue();
	        	}
	        	$globalData[$key]=['v'=>$val,'sett'=>$valueOrg];
        	}
        }else
        {
        	foreach ($formOrg as $key => $valueOrg) 
        	{
        		$val='';
	        	$globalData[$key]=['v'=>$val,'sett'=>$valueOrg];
        	}
        }
        $sousSiret=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['siretKey']);
        if(empty($sousSiret))
        {
        	foreach ($formSiret as $key => $valueOrg) 
        	{
        		$val='';
	        	$globalData[$key]=['v'=>$val,'sett'=>$valueOrg];
        	}
        }else
        {
        	foreach ($formOrg as $key => $valueOrg) 
        	{
                $val='';
                if(isset($valueOrg['keys']))
                {
                    $val=HelperFunctions::findValueConfigRequest($copyRequest,$valueOrg['keys']);
                    if(!is_null($val))
                    {
                        $val=$val->getValue();
                    }
                }        		
	        	$globalData[$key]=['v'=>$val,'sett'=>$valueOrg];
        	}
        }
       
        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Results_EMMY, ''));
        $dataParamResultsEMMYdb = $_group->getAttributes($group);

        foreach ($copyOperations as $valueOperations) 
        {
        	$dataExcelRow=[];
        	$dataOper=[];
            $operText=HelperFunctions::findValueConfigRequest($valueOperations,Constans::OBJ_RESULTEMMY_CONFIG_SETT['operations']['textKey']);
           
        	foreach ($formOperation as $key => $value) 
        	{
        		$val=HelperFunctions::findValueConfigRequest($valueOperations,$value['keys']);
        		if(!is_null($val))
	        	{
	        		$val=$val;
	        	}else
	        	{
	        		$val='';
	        	}
	        	$dataOper[$key]=['v'=>$val,'sett'=>$value];
        	}

        	$bureUid=HelperFunctions::findValueConfigRequest($valueOperations,Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['bureKey']);
        	
        	if(empty($bureUid))
        	{        		
        		foreach ($formBure as $key => $value) 
	        	{	        		
		        	$val='';
		        	$dataOper[$key]=['v'=>$val,'sett'=>$value];
	        	}
        	}else
        	{
        		$dataBure=HelperFunctions::getDataFromDB($bureUid);
        		foreach ($formBure as $key => $value) 
	        	{	        		
		        	$val=HelperFunctions::findValueFromDataUID($value['param'],$dataBure);
	        		if(!is_null($val))
		        	{
		        		$val=$val->getValue();
		        	}
		        	$dataOper[$key]=['v'=>$val,'sett'=>$value];
	        	}
        	}

        	$copyObjUid=$formObjUid;
        	$actionCol=array_column($copyObjUid,'action');
        	$formObjUID=HelperFunctions::findValueConfigRequest($copyRequest,Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['objUid']);
        	$formObjUID=empty($formObjUID)? []:$formObjUID;
        	$arrKeysSpecIsol=array_keys(Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['specOperIsol']['values']);
        	foreach ($formObjUid as $key => $value) 
        	{
        		if(isset($value['name']))
        		{
        			if(in_array($value['name'], $arrKeysSpecIsol))
        			{
        				$val=Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['specOperIsol']['values'][$value['name']];
        				//значит это у нас energgieDeChauffage
        				$keyEnergyDeChauffage=array_search('energgieDeChauffage',$actionCol);
        				if($keyEnergyDeChauffage!==false)
        				{
        					if(in_array($operFirst, Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['specOperIsol']['opers']))
        					{
        						$val=1;
        					}
        					$dataOper[$keyEnergyDeChauffage]=['v'=>$val,'sett'=>$copyObjUid[$keyEnergyDeChauffage]];
        					unset($copyObjUid[$keyEnergyDeChauffage]);
        				}
        			}
        		}
        		if(isset($value['label'])&&isset($value['name']))
        		{
        			//это у нас сект де активити и typeDepos
        			if($value['label']==Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['sectDeActivity']['label'])
        			{
        				$vName=$value['name'];
        				$val='';
        				if(isset(Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['sectDeActivity']['values'][$vName]))
        				{
        					$val=Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['sectDeActivity']['values'][$vName];
        				}
        				$keyEnergyDeChauffage=array_search('sectDeActivity',$actionCol);
        				if($keyEnergyDeChauffage!==false)
        				{
        					$dataOper[$keyEnergyDeChauffage]=['v'=>$val,'sett'=>$copyObjUid[$keyEnergyDeChauffage]];
        					unset($copyObjUid[$keyEnergyDeChauffage]);
        				}
        			}else if($value['label']==Constans::OBJ_RESULTEMMY_CONFIG_SETT['actions']['typeDePose']['label'])
        			{
        				$keyEnergyDeChauffage=array_search('typeDePose',$actionCol);
        				if($keyEnergyDeChauffage!==false)
        				{
        					$dataOper[$keyEnergyDeChauffage]=['v'=>$value['name'],'sett'=>$copyObjUid[$keyEnergyDeChauffage]];
        					unset($copyObjUid[$keyEnergyDeChauffage]);
        				}
        			}
        		}
        	}
        	foreach ($copyObjUid as $key => $value) 
        	{
        		$dataOper[$key]=['v'=>'','sett'=>$value];
        	}
        	
        	//operIsolant
        	$copyIsol=HelperFunctions::findValueConfigRequest($valueOperations,Constans::OBJ_RESULTEMMY_CONFIG_SETT['operIsolant']['key']);
        	$copyIsol=empty($copyIsol)? []:$copyIsol;
            $surFaceArr=[];
            $dtAdvParams=Constans::OBJ_RESULTEMMY_CONFIG_SETT['blocksIsolantAdvParams']['actions'];
            $dataAdvP=[];

        	foreach ($copyIsol as $key => $value) 
        	{
        		//собираем все в массивы и потом их имплоудим
                $advParams=HelperFunctions::findValueConfigRequest($value,Constans::OBJ_RESULTEMMY_CONFIG_SETT['operIsolanAdvP']['key']);
                foreach ($dtAdvParams as $keyT => $valueT) 
                {
                    if(isset($advParams[$valueT])&&$advParams[$valueT][Constans::OBJ_RESULTEMMY_CONFIG_SETT['blocksIsolantAdvParams']['key']])
                    {
                        $v=$advParams[$valueT][Constans::OBJ_RESULTEMMY_CONFIG_SETT['blocksIsolantAdvParams']['key']];
                        if(!isset($dataAdvP[$keyT]))
                        {
                            $dataAdvP[$keyT]=[];
                        }                   
                        $dataAdvP[$keyT][]=$v;     
                    }
                } 
                $surFaceArr[]=HelperFunctions::findValueConfigRequest($value,Constans::OBJ_RESULTEMMY_CONFIG_SETT['operIsolant']['surfaceKey']);                
        	}
            $cformIsolant=$formIsolant;
            foreach ($cformIsolant as $key => $value) 
            {
                if(isset($value['action']))
                {
                    if($value['action']=='isolantSurface1'&&!in_array($operText, Constans::OBJ_RESULTEMMY_CONFIG_SETT['operIsolant']['specOper']))
                    {
                        $dataOper[$key]=['v'=>array_sum($surFaceArr),'sett'=>$value];
                        unset($cformIsolant[$key]);
                    }elseif($value['action']=='isolantSurface2'&&!in_array($operText, Constans::OBJ_RESULTEMMY_CONFIG_SETT['operIsolant']['specOper']))
                    {
                        $dataOper[$key]=['v'=>array_sum($surFaceArr),'sett'=>$value];
                        unset($cformIsolant[$key]);
                    }else if(isset($dataAdvP[$value['action']]))
                    {
                        $dataOper[$key]=['v'=>implode(Constans::OBJ_RESULTEMMY_CONFIG_SETT['blocksIsolantAdvParams']['delim'],$dataAdvP[$value['action']]),'sett'=>$value];
                        unset($cformIsolant[$key]);
                    }
                }
            }
            foreach ($cformIsolant as $key => $value) 
            {
                $dataOper[$key]=['v'=>'','sett'=>$value];
            }
            foreach ($globalData as $key => $value) 
            {
                $dataOper[$key]=$value;
            }
            $objResultsEMMY = new \Dzeta\Core\Instance('', '');
            $objResultsEMMY->setType(new \Dzeta\Core\Type\Type(Constans::CLS_UID_Results_EMMY, ''))
                     ->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
            $dataParamResultsEMMY=$this->fillParamDataDB($dataParamResultsEMMYdb,$dataOper);
            $objResultsEMMY->setAttributes($dataParamResultsEMMY);
            $res=$_obj->checkUniqueAndAddWithValues($objResultsEMMY, \Dzeta\Core\Instance::ADDED_BY_USER,$this->generateDoc->USER);
            if(empty($objResultsEMMY->getUid()))
            {
                $this->generateDoc->dataLogs[]=[
                    'time'=>date('d/m/Y H:i:s'),
                    'err'=>true,
                    'text'=>'Не удалось создать ResultsEMMY',
                    'data'=>
                    [
                        'res'=>$res
                    ]
                ];
                return false;
            }else
            {
                $this->generateDoc->dataLogs[]=[
                    'time'=>date('d/m/Y H:i:s'),                        
                    'text'=>'Удалось создать ResultsEMMY',
                    'data'=>
                    [
                        'uid'=>$objResultsEMMY->getUid()
                    ]
                ];
            }
            $objEmmyResUID[]=$objResultsEMMY->getUid();
            $dataExcel[]=$this->fillEXCEL($dataOper);
        }    

        $dataForExcel=[];
        foreach ($dataExcel as $valueArr) 
        {
            $dataExcelR=[];
            foreach ($valueArr as $key => $value) 
            {
                $dataExcelR[\PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($key)]=$value;
            }
            $t=[];
            for($i=0;$i<232;$i++)
            {
                if(isset($dataExcelR[$i+1]))
                {
                    $t[]=$dataExcelR[$i+1];
                }else
                {
                    $t[]='';
                }  
            }
            $dataForExcel[]=$t;
        } 
        $this->generateDoc->dataLogs[]=[
            'time'=>date('d/m/Y H:i:s'),                        
            'text'=>'Начинаем вставку в Excel-Google',
            'data'=>
            [
                
            ]
        ];
        $this->writeExcel($dataForExcel);
        $this->generateDoc->dataLogs[]=[
            'time'=>date('d/m/Y H:i:s'),                        
            'text'=>'Заканчена вставка в Excel-Google',
            'data'=>
            [
                
            ]
        ];
        return $objEmmyResUID;
	}

    private function writeExcel($dataExcel)
    {
        $googleAccountKeyFilePath = BASE_PATH.'/config/dop/gifted-honor-236118-0330fc9ff24c.json';
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.$googleAccountKeyFilePath);
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope('https://www.googleapis.com/auth/spreadsheets');
        $service = new \Google_Service_Sheets($client);

        $spreadsheetId=file_get_contents(Constans::PATH_FILE_EXCEL_CONFIG);
        $spreadsheetId = '1pdnlNujnTEYfZ2MsIavJWXNmhL6LLX3FEDPzGHOcFsg';
        $response = $service->spreadsheets->get($spreadsheetId);
        $range = 'CRM EXTERNE!A:A';
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();
        $curRow=count($values);
        $body    = new \Google_Service_Sheets_ValueRange(['values'=>$dataExcel]);
        $options = ['valueInputOption' => 'RAW'];
        $service->spreadsheets_values->update($spreadsheetId,'CRM EXTERNE!A'.($curRow+1),$body, $options);
        $this->generateDoc->dataLogs[]=[
            'time'=>date('d/m/Y H:i:s'),                        
            'text'=>'Произошла вставка в Excel',
            'data'=>
            [
                'data'=>$dataExcel,
                'row'=>$curRow+1,
                'id'=>$spreadsheetId
            ]
        ];
    }

	private function configSettInit()
	{
		$configParamData=json_decode(file_get_contents(Constans::PATH_CONFIG_SETT_OBJ_REQUEST_RESULTSEMMY),true);
		$dataFormRequest=[];
		$dataAction=[];
		$dataDefValue=[];
		$forms=[];
		foreach ($configParamData as $key => $value) 
		{
			if(isset($value['type']))
			{
				if(!isset($forms[$value['type']]))
				{
					$forms[$value['type']]=[];
				}
				$forms[$value['type']][$key]=$value;
			}
		}
		
		return $forms;
	}

	private function fillParamDataDB($dataDB,$dataForm)
	{
		$attrAll=[];
		foreach ($dataDB as $key => $value) 
		{
			$uidParam=$value->getUid();
			if(isset($dataForm[$uidParam]))
			{
				$value->setValue(['value'=>$dataForm[$uidParam]['v']]);
				$attrAll[]=$value;
			}
		}
		return $attrAll;
	}

	private function fillEXCEL($data)
	{
		$dataExcel=[];
		foreach ($data as $key => $value)
		{
			$dataExcel[$value['sett']['EXCEL']]= $value['v'];
		}
		return $dataExcel;
	}
}