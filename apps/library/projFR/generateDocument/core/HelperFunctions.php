<?php

namespace generateDocEnemat;

class HelperFunctions
{
	public static function fillParamForFormData($dataParamOBJET,$configParamData,$requestData,$dopSpec=[])
	{
		foreach ($dataParamOBJET as $key => $value) 
		{
            $uidParam=$value->getUid();
            if(isset($configParamData[$uidParam]))
            {
              	switch ($configParamData[$uidParam]['type']) 
              	{
              		case 'form':
              			if(isset($configParamData[$uidParam]['notwrite']))
	                    {
	                      	unset($dataParamOBJET[$key]);
	                    }else
	                    {
	                        $v=self::getValueForDataFormKeys($requestData,$configParamData[$uidParam]['keys']);
	                        if(isset($configParamData[$uidParam]['action']))
	                        {
	                            switch ($configParamData[$uidParam]['action']) 
	                            {
	                                case 'empty':
	                                    $v=empty($v)? '':$v;
	                                    $v=$v=='false'? '':$v;
	                                    break;
	                                case 'emptyNum':
	                                    $v=empty($v)? 0:$v;
	                                    $v=$v=='false'? 0:$v;
	                                    $v=empty($v)? 0:1;
	                                    break;
	                            }
	                        }
	                        $value->setValue(['value'=>$v]);
	                    } 
              			break;    
              		case 'dateNow':
	              		if(empty($dopSpec['dateNow']))
	                    {
	                      	unset($dataParamOBJET[$key]);
	                    }else
	                    {
	                      	$value->setValue(['value'=>$dopSpec['dateNow']]);
	                    } 
              			break; 
              		case 'allRequest':                    
                      	$value->setValue(['value'=>json_encode($requestData)]); 
                    	break;
                    case 'curUser':                    
                        $value->setValue(['value'=>$dopSpec['user']]); 
                        break;
                    case 'form-json':                    
                        if(isset($configParamData[$uidParam]['notwrite']))
                        {
                            unset($dataParamOBJET[$key]);
                        }else
                        {
                            $vvvv=HelperFunctions::getValueForDataFormKeys($requestData,$configParamData[$uidParam]['keys']);
                            $vvvv=is_null($vvvv)? null:json_encode($vvvv);
                            $value->setValue(['value'=>$vvvv]);
                        }     
                        break; 
                    case 'numDevis':
	                    if(!empty($dopSpec['createDevis']))
	                    {//тут получаем numDevis и пишем его потом	                      
	                      $numD=$dopSpec['numDevis'];
	                      $value->setValue(['value'=>$numD]); 
	                    }else
	                    {
	                       unset($dataParamOBJET[$key]);
	                    }                    
                    	break; 
                     case 'dopNumdevis':
                        if(!empty($dopSpec['createDevis']))
                        {
                          //тут получаем numDevis и пишем его потом
                          $numD=$configParamData[$uidParam]['symb'].$dopSpec['numDevis'];
                          $value->setValue(['value'=>$numD]); 
                        }else{
                           unset($dataParamOBJET[$key]);
                        }
                        break;   		
              		default:
              			# code...
              			break;
              	}
            }
        }
        return $dataParamOBJET;
	}

	public static function getValueForDataFormKeys($data,$keys)
	{
      	if(empty($keys)) return null;
      	if(count($keys)<=1)
      	{

        	if(isset($data[$keys[0]]))
        	{

          		return $data[$keys[0]];
        	}
        	return null;
      	}
      	$upKey=array_shift($keys);
      	if(isset($data[$upKey]))
      	{
          	return self::getValueForDataFormKeys($data[$upKey],$keys);
      	}
      	return null;
    }

    public static function getAllElemsFromConfig($dataConfig,&$arrResults)
    {
    	foreach ($dataConfig as $valuesConfig) 
    	{
    		if($valuesConfig['type']=='block'||$valuesConfig['type']=='blockForm'||$valuesConfig['type']=='blockDB'||
                $valuesConfig['type']=='blockCheck'||$valuesConfig['type']=='blockObjwithChild')
    		{
    			self::getAllElemsFromConfig($valuesConfig['value'],$arrResults);
    		}else if($valuesConfig['type']=='elemWithChild')
            {
                $tmp=$valuesConfig;
                unset($tmp['value']);
                $arrResults[]=$tmp;
                self::getAllElemsFromConfig($valuesConfig['value'],$arrResults);
            }
            else if($valuesConfig['type']=='elem')
    		{
    			$arrResults[]=$valuesConfig;
    		}
    	}
    }

    public static function getElemsConfigforTypeElem($dataElem,$keyFind)
    {
    	$elems=[];
    	foreach ($dataElem as  $value) 
    	{
    		if($value['typeElem']==$keyFind)
    		{
    			$elems[]=$value;
    		}
    	}
    	return $elems;
    }

    public static function findValueConfigRequest($dataRequest,$arrKeys)
    {
    	if(isset($arrKeys[0])&&isset($dataRequest[$arrKeys[0]]))
    	{
    		if(count($arrKeys)==1)
    		{
    			return $dataRequest[$arrKeys[0]];
    		}else
    		{
    			$t=$arrKeys[0];
	          	array_shift($arrKeys);
	          	return self::findValueConfigRequest($dataRequest[$t],$arrKeys);
    		}
    	}
    	return null;
    }

    public static function getDataFromDB($uidObj)
    {
    	$_obj=new \Dzeta\Models\Obj();
    	$data=$_obj->getAllValues([new \Dzeta\Core\Instance($uidObj, '')]);
    	return $data[$uidObj];
    }
    
    public static function findValueFromDataUID($uid,$data)
    {
    	foreach ($data as $value) 
    	{
    		if($value->getUid()==$uid)
    		{
    			return $value->getValue();
    		}
    	}
    	return null;
    }
    public static function clearSiretValue($siret)
    {
    	return substr(str_replace(' ','',$siret),0,14);
    }
    public static function clearSirenValue($siren)
    {
    	return substr(str_replace(' ','',$siren),0,9);
    }

    public static function action_numeroFacture($dataRequest,$generateDoc)
    {
    	//обработка numeroFacture
    	$typeGenDoc=isset($dataRequest['created_document'])? $dataRequest['created_document']:$dataRequest['edited_document'];
    	if($typeGenDoc=='devisah'||$typeGenDoc=='devis'||$typeGenDoc=='facture')
    	{
    		if(empty($generateDoc->objectsGlobal['objet']))
    		{
    			$generateDoc->logs[]=[
    				'time'=>date('d/m/Y H:i:s'),
    				'err'=>true,
					'text'=>'Проблема с объектом objet'
    			];
    			return false;
    		}
    		if(empty($generateDoc->objectsGlobal['objet']['params']))
    		{
    			//значит делаем запрос на получение данных
    			$generateDoc->objectsGlobal['objet']['params']=self::getDataFromDB($generateDoc->objectsGlobal['objet']['obj']);
    		}
    		$vvv=self::findValueFromDataUID(Constans::CLS_UID_BaseNumber,$generateDoc->objectsGlobal['objet']['params']);
    		if(is_null($vvv))
    		{
    			$generateDoc->logs[]=[
    				'time'=>date('d/m/Y H:i:s'),
					'text'=>'Не удалось получить Base number в objet',
					'err'=>true
    			];
    			return false;
    		}
    		if($typeGenDoc=='devisah'||$typeGenDoc=='devis')
    		{
    			return 'D'.$vvv->getValue();
    		}else
    		{
    			return 'F'.$vvv->getValue();
    		}    		
    	}
    	return '';
    }
    //TODO:: перепроверить, надо ли типы документов тут
    public static function action_numDevis($dataRequest,$generateDoc)
    {
    	//обработка numeroFacture
    	$typeGenDoc=isset($dataRequest['created_document'])? $dataRequest['created_document']:$dataRequest['edited_document'];
    	
		if(empty($generateDoc->objectsGlobal['objet']))
		{
			$generateDoc->logs[]=[
				'time'=>date('d/m/Y H:i:s'),
				'err'=>true,
				'text'=>'Проблема с объектом objet'
			];
			return '';
		}
		if(empty($generateDoc->objectsGlobal['objet']['params']))
		{
			//значит делаем запрос на получение данных
			$generateDoc->objectsGlobal['objet']['params']=self::getDataFromDB($generateDoc->objectsGlobal['objet']['obj']);
		}
		$vvv=self::findValueFromDataUID(Constans::CLS_UID_BaseNumber,$generateDoc->objectsGlobal['objet']['params']);
		if(is_null($vvv))
		{
			$generateDoc->logs[]=[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось получить Base number в objet',
				'err'=>true
			];
			return false;
		}
		return $vvv->getValue();	
    }

    public static function action_agr_gen($dataRequest,$generateDoc)
    {
    	$typeGenDoc=isset($dataRequest['created_document'])? $dataRequest['created_document']:$dataRequest['edited_document'];
    	if($typeGenDoc=='devisah'||$typeGenDoc=='devis'||$typeGenDoc=='facture')
    	{
    		//CLS_UID_DateDeCreation
    		if(empty($generateDoc->objectsGlobal['devis']))
			{
				$generateDoc->logs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Проблема с объектом devis'
				];
				return '';
			}
			if(empty($generateDoc->objectsGlobal['devis']['params']))
			{
				//значит делаем запрос на получение данных
				$generateDoc->objectsGlobal['devis']['params']=self::getDataFromDB($generateDoc->objectsGlobal['devis']['obj']);
			}
			$vvv=self::findValueFromDataUID(Constans::CLS_UID_DateDeCreation,$generateDoc->objectsGlobal['devis']['params']);
			if(is_null($vvv))
			{
				$generateDoc->logs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'text'=>'Не удалось получить Date de création в devis',
					'err'=>true
				];
				return false;
			}
			return $vvv->getValueN()->format('d/m/Y');;
    	}
    	return '';
    }

    public static function action_inv_gen($dataRequest,$generateDoc)
    {
    	$typeGenDoc=isset($dataRequest['created_document'])? $dataRequest['created_document']:$dataRequest['edited_document'];
    	if($typeGenDoc=='facture')
    	{
    		//CLS_UID_DateDeCreation
    		if(empty($generateDoc->objectsGlobal['facture']))
			{
				$generateDoc->logs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'err'=>true,
					'text'=>'Проблема с объектом facture'
				];
				return '';
			}
			if(empty($generateDoc->objectsGlobal['facture']['params']))
			{
				//значит делаем запрос на получение данных
				$generateDoc->objectsGlobal['facture']['params']=self::getDataFromDB($generateDoc->objectsGlobal['facture']['obj']);
			}
			$vvv=self::findValueFromDataUID(Constans::CLS_UID_DateDeCreationF,$generateDoc->objectsGlobal['facture']['params']);
			if(is_null($vvv))
			{
				$generateDoc->logs[]=[
					'time'=>date('d/m/Y H:i:s'),
					'text'=>'Не удалось получить Date de création в facture',
					'err'=>true
				];
				return false;
			}
			return $vvv->getValueN()->format('d/m/Y');;
    	}
    	return '';
    }

    public static function action_beneficareRepresentePar($dataRequest,$generateDoc)
    {
    	if(!isset($dataRequest['persone']))
    	{
    		$generateDoc->logs[]=[
				'time'=>date('d/m/Y H:i:s'),
				'text'=>'Не удалось получить тип Persone',
				'err'=>true
			];
			return false;
    	}
    	
    	if($dataRequest['persone']=='Personne physique')
    	{
    		return '';
    	}else
    	{
    		return 'Reprèsentè par: ';
    	}
    }

    public static function action_beneficareNumPerson($dataRequest,$generateDoc)
    {
    	if(!empty($dataRequest['beneficiaire']['persone_phys_data']))
    	{
    		return count($dataRequest['beneficiaire']['persone_phys_data']);
    	}
    	return '';
    }

    public static function action_beneficareFamilyy($dataRequest,$generateDoc)
    {
    	if(!empty($dataRequest['beneficiaire']['persone_phys_data']))
    	{
    		$copy=$dataRequest['beneficiaire']['persone_phys_data'];
    		array_shift($copy);
    		$arrFam=[];
            foreach ($copy as $valuePPD) 
            {
                if(isset($valuePPD['JSON']))
                {
                    $jsonn=json_decode($valuePPD['JSON'],true);
                    $arrFam[]=$jsonn['declarant1']['Nom'].' '.$jsonn['declarant1']['Prenom(s)'];
                }
            }
            if(!empty($arrFam))
            {
               return implode(';',$arrFam);
            }
    	}
    	return '';
    }

    public static function action_beneficareNumOfLines($dataRequest,$generateDoc)
    {
        if(isset($dataRequest['beneficiaire']['persone_phys_data']))
        {
            return count($dataRequest['beneficiaire']['persone_phys_data']);
        }
        return '';
    }

    public static function action_beneficareTableSpec($dataRequest,$generateDoc)
    {        
        if(!file_exists(Constans::PATH_CONFIG_SPEC_TABLE_BENEFICIARE))
        {
            $generateDoc->logs[]=[
                'time'=>date('d/m/Y H:i:s'),
                'text'=>'Не удалось найти шаблон с таблицей',
                'err'=>true
            ];
            return false;
        }
        $templ=new \MyTemplateProcessor(Constans::PATH_CONFIG_SPEC_TABLE_BENEFICIARE);
        $allTableFile=$templ->GetAllTableSettMetka();
        if(!isset($allTableFile[0])&&!isset($allTableFile[0]['table']))
        {
            $generateDoc->logs[]=[
                'time'=>date('d/m/Y H:i:s'),
                'text'=>'Не найдено таблиц в файле шаблона',
                'err'=>true
            ];
            return false;
        }
        $tbl=$allTableFile[0]['table'];
        if(isset($dataRequest['declarant1']))
        {
            foreach ($dataRequest['declarant1'] as $key => $value) 
            {
                if(strpos($key,'Adresse ')===0)
                {
                    $dataRequest['declarant1']['Adresse']=$value;
                }
            }
        }
        if(isset($dataRequest['declarant2']))
        {
            foreach ($dataRequest['declarant2'] as $key => $value) 
            {
                if(strpos($key,'Adresse ')===0)
                {
                    $dataRequest['declarant2']['Adresse']=$value;
                }
            }
        }
        if(isset($dataRequest['common']))
        {
            if($dataRequest['common']['Year'])
            {
                $dataRequest['common']['Year-1']=$dataRequest['common']['Year']-1;
                $dataRequest['common']['Year']=$dataRequest['common']['Year'].' ';                
            }
        }
       
        foreach ($allTableFile[0]['metk'] as $key => $value) 
        {           
           $metk=strip_tags($value);
           $mValue=explode('.',$metk);
           $vRequest=HelperFunctions::findValueConfigRequest($dataRequest,$mValue);
           $tbl=str_replace('{{'.$value.'}}',$vRequest,$tbl);
        }
        return $tbl;
    }

    public static function action_sousTraintceDevis()
    {
    	$_obj=new \Dzeta\Models\Obj();
    	$obj=new \Dzeta\Core\Instance(Constans::GLOBAL_SETT_CONFIG['dataSousTraite']['obj'], '');
    	$obj->setType(new \Dzeta\Core\Type\Type(Constans::GLOBAL_SETT_CONFIG['dataSousTraite']['type'], ''));
    	$obj->setAttributes([
			new \Dzeta\Core\Type\Attribute(Constans::GLOBAL_SETT_CONFIG['dataSousTraite']['attr']['uid'],'Sous-traite',Constans::GLOBAL_SETT_CONFIG['dataSousTraite']['attr']['datatype'],false,false)
		]);
		$_obj->getAllValues([$obj]);

		$text='';
		$allAttr=$obj->getAttributes();
		foreach ($allAttr as $key=> $attr) 
		{
			switch ($attr->getUid()) 
			{
				case Constans::GLOBAL_SETT_CONFIG['dataSousTraite']['attr']['uid']:
					if(!empty($attr->getValue()))
					{
						$text=$attr->getValue()->getValue();
					}
					break;
			}
		}
		return $text;
    }

    public static function createOrderReplaceMetka($replaceArrs,&$resultOrder,&$countIter,$docGenerate)
    {        
        $countIter++;
        if($countIter>10000)
        {
            $generateDoc->logs[]=[
                'time'=>date('d/m/Y H:i:s'),
                'text'=>'Превышен лимит для замены',
                'data'=>[
                    'params'=>$replaceArrs
                ],
                'err'=>true
            ];
            return false;
        }
        foreach ($replaceArrs as $key => $value) 
        {
            $flagIsset=true;            
            $arrKeys=array_keys($replaceArrs);
            $valMetkaaArr=array_column($value['sett']['dopSettMetka'], 'selectedValue');
            foreach ($arrKeys as  $valueArrK) 
            {
                if(in_array($valueArrK,$valMetkaaArr))
                {
                    $flagIsset=false;
                    break;
                }
            }
            if($flagIsset)
            {
                $resultOrder[$key]=$value;
                unset($replaceArrs[$key]);  
            }            
        }
        if(count($replaceArrs)==0)
        {
            return true;
        }
        return self::createOrderReplaceMetka($replaceArrs,$resultOrder,$countIter,$docGenerate);
    }


    
    //функция для замены данных
    public static function replaceMetkaValue($keyMetka,$origMetka,$settingsReplace,$dataMetka,$dataMetkaArr=[])
    {        
        //вначале выделяем метки с List.
        if(strpos($keyMetka,'List.')===0)
        {
            $listMetka=[];
            foreach ($dataMetkaArr as $valueArr) 
            {
                $copyText=$origMetka;
                foreach ($settingsReplace as $value) 
                {
                    if(isset($valueArr[$value['selectedValue']]))
                    {
                        $copyText=str_replace('{{'.$value['selectedValue'].'}}',$valueArr[$value['selectedValue']]['v'],$copyText);
                    }
                }
                $listMetka[]=$copyText;
            }
            if(!empty($listMetka))
            {
                $origMetka=implode(Constans::DELIMETER_NEW_BRAKE_WORD,$listMetka);
            }
        }
        foreach ($settingsReplace as $value) 
        {
            if(isset($dataMetka[$value['selectedValue']]))
            {
                $origMetka=str_replace('{{'.$value['selectedValue'].'}}',$dataMetka[$value['selectedValue']]['v'],$origMetka);
            }
        }
        return $origMetka;
    }

    public static function additionalBloksToarray($dataAdditionalBlocks)
    {
        $resArr=[];
        foreach ($dataAdditionalBlocks as $valueAdB) 
        {

            if(isset($valueAdB['data']))
            {
                foreach ($valueAdB['data'] as $value) 
                {
                    $tmp=[
                        'name'=>$value['name'],
                        'uid'=>$value['uid']                        
                    ];
                    if(isset($value['userData']))
                    {
                        $tmp['val']=$value['userData'];
                    }
                    if(isset($value['linked_obj']))
                    {
                        $tmp['linked_obj']=$value['linked_obj'];
                    }
                    $resArr[]=$tmp;
                }
            }
            $tmp=[
                'name'=>$valueAdB['name'],
                'uid'=>$valueAdB['uid']
            ];
            if(isset($valueAdB['linked_obj']))
            {
                $tmp['linked_obj']=$valueAdB['linked_obj'];
            }
            if(isset($valueAdB['userData']))
            {
                $tmp['val']=$valueAdB['userData'];
            }
            $resArr[]=$tmp;
        }
        return $resArr;
    }

    public static function execFormatValue($val,$typeFormat)
    {
        switch ($typeFormat)
        {
            case 'numToStr2':
                $val=str_replace('.',',',round($val,2).'');
                break;
        }
        return $val;
    }


    public static function getDataFromCahsheDB($uidObj,$genDocument)
    {
        if(isset($genDocument->CASHE_OBJ_VALUES[$uidObj]))
        {
            return $genDocument->CASHE_OBJ_VALUES[$uidObj];
        }else
        {
            $data=HelperFunctions::getDataFromDB($uidObj);
            $genDocument->CASHE_OBJ_VALUES[$uidObj]=$data;
            return $data;
        }
    }

    public static function stringUid($uid)
    {
        preg_match('/^\{?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\}?$/', $uid, $output_array);
        if(empty($output_array))
        {
            return false;
        }else
        {
            return true;
        }
    }

    public static function findValueNameFromConfig($config,$valueName)
    {
        foreach ($config as $key => $value) 
        {
            if($value['name']==$valueName)
            {
                return $value;
            }
        }
        return null;
    }

    public static function findValueForMetkapoActionAndType($action,$typeElem,$allBlock)
    {
        foreach ($allBlock as $valueBlock) 
        {
            if(isset($valueBlock['sett']))
            {
                if(isset($valueBlock['sett']['typeElem'])&&isset($valueBlock['sett']['action']))
                {
                    if($valueBlock['sett']['typeElem']==$typeElem&&$valueBlock['sett']['action']==$action)
                    {
                        return $valueBlock['v'];
                    }
                }
            }
        }
        return '';
    }
    public static function findKEYForMetkapoActionAndType($action,$typeElem,$allBlock)
    {
        foreach ($allBlock as $key=> $valueBlock) 
        {
            if(isset($valueBlock['sett']))
            {
                if(isset($valueBlock['sett']['typeElem'])&&isset($valueBlock['sett']['action']))
                {
                    if($valueBlock['sett']['typeElem']==$typeElem&&$valueBlock['sett']['action']==$action)
                    {
                        return $key;
                    }
                }
            }
        }
        return null;
    }
}