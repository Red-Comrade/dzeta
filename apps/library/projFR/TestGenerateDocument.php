<?php

class TestGenerateDocument
{
	//класс который предназначен для чтения папки с  файлами гугл диска, вытягивание всех меток 
	const pathGoogleKey=BASE_PATH.'/config/dop/gifted-honor-236118-0330fc9ff24c.json';
	const rootFolderID='1f9Os36B6znCypZDy5ZDxUURWszjAi5W4';
	const nameGoogleFolder='googFile';
	const nameWordModFolder='wordFile';
	const namePDFFolder='pdfFile';

	const DefaultData=[


	];

	private $uniqID='';
	private $pathSave=BASE_PATH.'/ufiles/fillFileFr/';
	
	private $pathFilesWord=[];

	public function __construct()
    {

    }
	public function init()
	{
		$this->uniqID=uniqid();
		$this->pathSave.=$this->uniqID.'_save/';
        if (!mkdir($this->pathSave, 0777)) 
        {            
            return false;
        }
        //создаем папку гугла
        if (!mkdir($this->pathSave.TestGenerateDocument::nameGoogleFolder, 0777)) 
        {         
        	echo 'ggogle';   
            return false;
        }
        //создаем папку ворда
        if (!mkdir($this->pathSave.TestGenerateDocument::nameWordModFolder, 0777)) 
        {            
        	echo 'word';
            return false;
        }
         //создаем папку ворда
        if (!mkdir($this->pathSave.TestGenerateDocument::namePDFFolder, 0777)) 
        {            
        	echo 'pdf';
            return false;
        }
        //теперь получаем все файлы и папки
        $this->dwnloadGoogleObject();
        $this->insertDataMetka();
        $this->workDoc2pdf();
	}

	private function insertDataMetka()
	{
		foreach ($this->pathFilesWord as $valueFile) 
		{
			$sourceFile=$valueFile;
			$pathFolder=explode('/',$valueFile);
			foreach ($pathFolder as $key => $valueK) 
			{
				if($valueK==TestGenerateDocument::nameGoogleFolder)
				{
					$pathFolder[$key]=TestGenerateDocument::nameWordModFolder;
					break;
				}
			}
			$finFile=implode('/',$pathFolder);
			$templ=new \MyTemplateProcessor($valueFile);
            $variables=$templ->getVariables();
            $templ->saveAs($finFile);
		}
	}
	private function workLibra()
	{
		foreach ($this->pathFilesWord as $valueFile) 
		{			
			$pathFolder=explode('/',$valueFile);
			$pathPDF='';
			foreach ($pathFolder as $key => $valueK) 
			{
				if($valueK==TestGenerateDocument::nameGoogleFolder)
				{
					$pathFolder[$key]=TestGenerateDocument::nameWordModFolder;
					break;
				}
			}
			$sourceFile=implode('/',$pathFolder);
			foreach ($pathFolder as $key => $valueK) 
			{
				if($valueK==TestGenerateDocument::nameWordModFolder)
				{
					$pathFolder[$key]=TestGenerateDocument::namePDFFolder;
					break;
				}
			}			
			$pathPDF=dirname(implode('/',$pathFolder));			
			$cmd='libreoffice --headless --convert-to pdf  --outdir "'.$pathPDF.'" '.'"'.$sourceFile.'"';
			$output = exec($cmd);
		}
	}
	private function workAsposeOnline()
	{
		
        // the step is optional, the default value is https://api.aspose.cloud        
        $api = new \Aspose\Words\WordsApi("e7b30bec-8178-43a5-b7a7-d706c9e0977a", '31c688c0c8fbad1b611be3ca51aac8b3');
        foreach ($this->pathFilesWord as $valueFile) 
		{			
			$pathFolder=explode('/',$valueFile);
			$pathPDF='';
			foreach ($pathFolder as $key => $valueK) 
			{
				if($valueK==TestGenerateDocument::nameGoogleFolder)
				{
					$pathFolder[$key]=TestGenerateDocument::nameWordModFolder;
					break;
				}
			}
			$sourceFile=implode('/',$pathFolder);
			foreach ($pathFolder as $key => $valueK) 
			{
				if($valueK==TestGenerateDocument::nameWordModFolder)
				{
					$pathFolder[$key]=TestGenerateDocument::namePDFFolder;
					break;
				}
			}			
			$pathPDF=implode('/',$pathFolder);
			$pathPDF=str_replace('.docx','.pdf',$pathPDF);
			$localFilePath=$sourceFile;
	        // upload file to cloud
	        $upload_request = new \Aspose\Words\Model\Requests\UploadFileRequest($localFilePath, 'test.docx');
	        $upload_result = $api->uploadFile($upload_request);

	        // save as pdf file
	        $saveOptions = new \Aspose\Words\Model\SaveOptionsData(array("save_format" => "pdf", "file_name" => 'test.pdf'));
	        $request = new \Aspose\Words\Model\Requests\SaveAsRequest('test.docx', $saveOptions);
	        $result = $api->saveAs($request);
	       	$request = new \Aspose\Words\Model\Requests\DownloadFileRequest('test.pdf',NULL,NULL);
	       	$result = $api->downloadFile($request);
			copy($result->getPathName(),$pathPDF);
		}
	}
	private function workDoc2pdf()
	{
		foreach ($this->pathFilesWord as $valueFile) 
		{			
			$pathFolder=explode('/',$valueFile);
			$pathPDF='';
			foreach ($pathFolder as $key => $valueK) 
			{
				if($valueK==TestGenerateDocument::nameGoogleFolder)
				{
					$pathFolder[$key]=TestGenerateDocument::nameWordModFolder;
					break;
				}
			}
			$sourceFile=implode('/',$pathFolder);
			foreach ($pathFolder as $key => $valueK) 
			{
				if($valueK==TestGenerateDocument::nameWordModFolder)
				{
					$pathFolder[$key]=TestGenerateDocument::namePDFFolder;
					break;
				}
			}			
			$pathPDF=implode('/',$pathFolder);
			$pathPDF=str_replace('.docx','.pdf',$pathPDF);
			$cmd=BASE_PATH . "/../".'doc2pdf/build/doc2pdf "'.$sourceFile.'" "'.$pathPDF.'"';
			$output = exec($cmd);	
		}
	}
	private function dwnloadGoogleObject()
	{
		putenv('GOOGLE_APPLICATION_CREDENTIALS='.TestGenerateDocument::pathGoogleKey);
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();       
        $client->setScopes([\Google_Service_Docs::DOCUMENTS,'https://www.googleapis.com/auth/drive']);
        $serviceDisk = new \Google_Service_Drive($client);
        $pathArr=[];
        $pathArr['needFile']=$this->pathSave.TestGenerateDocument::nameGoogleFolder;
        $pathArr['noneedFile']=[
        	$this->pathSave.TestGenerateDocument::nameWordModFolder,
        	$this->pathSave.TestGenerateDocument::namePDFFolder
        ];
        $idParentFolder=TestGenerateDocument::rootFolderID;
        $this->downloadFolderGoogle($serviceDisk,$pathArr,$idParentFolder);
        $this->downloadFileGoogle($serviceDisk,$pathArr['needFile'],$idParentFolder);
	}
	private function downloadFolderGoogle($serviceDisk,$pathArr,$idParentFolder)
	{
		$optParams = [
            'pageSize' => 300,
            'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
            'q' => "mimeType = 'application/vnd.google-apps.folder' and '".$idParentFolder."' in parents"
        ];
        $allFolders=$serviceDisk->files->listFiles($optParams);
        foreach ($allFolders as $key => $value) 
        {
        	$idFolder=$value->getID();
        	$nameFolder=$value->getName();
        	$copyFolders=$pathArr;
        	foreach ($copyFolders['noneedFile'] as $key1=>$path) 
        	{
        		if (!mkdir($path.'/'.$value->getName(), 0777)) 
		        {            
		            return false;
		        }
		        $copyFolders['noneedFile'][$key1]=$path.'/'.$value->getName();
        	}
        	if (!mkdir($copyFolders['needFile'].'/'.$value->getName(), 0777)) 
	        {            
	            return false;
	        }	        
	        $copyFolders['needFile']=$copyFolders['needFile'].'/'.$value->getName();
	        $this->downloadFolderGoogle($serviceDisk,$copyFolders,$idFolder);
	        //теперь создаем внутрянку файлов всех
	        $this->downloadFileGoogle($serviceDisk,$pathArr['needFile'].'/'.$value->getName(),$idFolder);
        }
	}
	private function downloadFileGoogle($serviceDisk,$path,$idParentFolder)
	{
		$optParams = [
            'pageSize' => 300,
            'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
            'q' => "'".$idParentFolder."' in parents"            
        ];
        $allFolders=$serviceDisk->files->listFiles($optParams);
        $numIndexFile=1;
        foreach ($allFolders as $key => $value) 
        {
        	if($value->getFileExtension()=='docx')
        	{
        		$idFILE=$value->getID();
                $nameFile=$value->getName();
                if(strpos($nameFile,'~$')===0)
                {
                    continue;
                }
                if(file_exists($path.'/'.$nameFile))
                {
                    $nameFile=$idFILE.$nameFile;
                }
                $nameFile=mb_eregi_replace('[а-яё]', '',$numIndexFile.'_'.$nameFile);
                $numIndexFile++;

                $content = $serviceDisk->files->get($idFILE, array("alt" => "media"));
                $outHandle = fopen($path.'/'.$nameFile, "w+");
                $this->pathFilesWord[]=$path.'/'.$nameFile;
                while (!$content->getBody()->eof()) 
                {
                    fwrite($outHandle, $content->getBody()->read(1024));
                }        
                fclose($outHandle);
               
        	}
	        
        }
	}
} 
?>