<?php

class Dataenterprise {
    const SIREN_URL = 'https://entreprise.data.gouv.fr/api/sirene/v3/unites_legales/';
    const SIRET_URL = 'https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/';

    public static function GetInfo($code) {
        $code = preg_replace("/[^0-9]/", '', $code);
        $url = self::SIREN_URL . $code;
        $result = self::GetZapros($url);
        $result = json_decode($result, true);
        $data = [];
        $address = !empty($result['unite_legale']['etablissement_siege']['geo_adresse']) ? $result['unite_legale']['etablissement_siege']['geo_adresse'] : null;
        $parsed = [];
        if (!empty($address)) {
          $parsed = self::parseAddress($address);
        }
      	$data['Data'] = [
      		'Adresse' => [
      			'AdresseConcat' => !empty($address) ? $parsed['house_street'] : null,
      			'CodePostal' => !empty($address) ? $parsed['postal_code'] : null,
      			'BureauDistributeur' => !empty($address) ? $parsed['city'] : null,
      		],
      		'Denomination' => !empty($result['unite_legale']['denomination']) ? $result['unite_legale']['denomination'] : null,
          'Etat' => null,
      	];
        return $data;
    }

    public static function GetInfoBySiret($code)
    {
        $code = preg_replace("/[^0-9]/", '', $code);
        $url = self::SIRET_URL . $code;
        $result = self::GetZapros($url);
        $result = json_decode($result, true);
        $data = [];
        $address = !empty($result['etablissement']['unite_legale']['etablissement_siege']['geo_adresse']) ? $result['etablissement']['unite_legale']['etablissement_siege']['geo_adresse'] : null;
        $parsed = [];
        if (!empty($address)) {
          $parsed = self::parseAddress($address);
        }
        $data['Data'] = [
          'Adresse' => [
            'AdresseConcat' => !empty($address) ? $parsed['house_street'] : null,
            'CodePostal' => !empty($address) ? $parsed['postal_code'] : null,
            'BureauDistributeur' => !empty($address) ? $parsed['city'] : null,
          ],
          'Denomination' => !empty($result['etablissement']['unite_legale']['denomination']) ? $result['etablissement']['unite_legale']['denomination'] : null,
          'Etat' => !empty($result['etablissement']['etat_administratif']) ? $result['etablissement']['etat_administratif'] : null,
        ];
        return $data;
    }

    private static function parseAddress($address)
    {
        $result = ['house_street' => null, 'postal_code' => null, 'city' => null];
        $split = preg_split("/(\d{5})/", $address, -1, PREG_SPLIT_DELIM_CAPTURE);
        if (count($split) == 3) {
          $result['house_street'] = $split[0];
          $result['postal_code'] = $split[1];
          $result['city'] = $split[2];
        }
        return $result;
    }

    private static function GetZapros($url) {
      	$ch = curl_init();
      	curl_setopt($ch, CURLOPT_HEADER, 0);
      	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      	curl_setopt($ch, CURLOPT_URL, $url);
      	$data = curl_exec($ch);
      	curl_close($ch);
      	return $data;
    }
}