<?php

use KubAT\PhpSimple\HtmlDomParser;

class ImpotsGouvFr
{
    const BASE_URL = 'https://cfsmsp.impots.gouv.fr/secavis/';
    const VERIFY_URL = 'faces/commun/index.jsf';

    public static function getData($taxNumber, $refNotice) {
        $data = [];
        $result = self::curl(self::BASE_URL, 'GET', [], [], [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => true,
        ]);
        $taxNumber = str_replace(' ', '', $taxNumber);
        $refNotice = str_replace(' ', '', $refNotice);
        $dom = HtmlDomParser::str_get_html($result['response']);
        if ($dom) {
            $request = [];
            $submit = $dom->find('[name=j_id_7:j_id_l]', 0);
            if (!empty($submit)) {
                $request['j_id_7:j_id_l'] = $submit->value;
            }
            $submit = $dom->find('[name=j_id_7_SUBMIT]', 0);
            if (!empty($submit)) {
                $request['j_id_7_SUBMIT'] = $submit->value;
            }
            $view = $dom->find('[name=javax.faces.ViewState]', 0);
            if (!empty($view)) {
                $request['javax.faces.ViewState'] = $view->value;
            }
            $request['j_id_7:spi'] = $taxNumber;
            $request['j_id_7:num_facture'] = $refNotice;
            $response = self::curl(self::BASE_URL . self::VERIFY_URL, 'POST', $request, [], [
                CURLOPT_COOKIE => $result['cookies'],
                CURLOPT_RETURNTRANSFER => true,
            ], false);
            $dom = HtmlDomParser::str_get_html($response);
            if ($dom) {
                $table = $dom->find('#principal table', 0);
                if (!empty($table)) {
                    $prevTds = null;
                    $data['declarant1'] = [];
                    $data['declarant2'] = [];
                    $data['common'] = [];
                    $trs = $table->find('tr');
                    foreach ($trs as $tr) {
                        $tds = $tr->find('td');
                        $trColumn = trim(preg_replace('/\s\s+/', ' ', $tds[0]->plaintext));
                        $column = $trColumn;
                        $declarants = false;
                        if ($column == '' && !is_null($prevTds)) {
                            $column = trim(preg_replace('/\s\s+/', ' ', $prevTds[0]->plaintext));
                            $declarants = count($tds) > 1;
                        }
                        $column = strtr(utf8_decode($column), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
                        if (count($tds) == 3 || $declarants) {
                            if ($column != '') {
                                if (isset($data['declarant1'][$column])) {
                                    $data['declarant1'][$column] .= ' ' . trim($tds[1]->plaintext);
                                } else {
                                    $data['declarant1'][$column] = trim($tds[1]->plaintext);
                                }
                                if (isset($data['declarant2'][$column])) {
                                    $data['declarant2'][$column] .=  isset($tds[2]) ? ' ' . trim($tds[2]->plaintext) : '';
                                } else {
                                    $data['declarant2'][$column] = isset($tds[2]) ? trim($tds[2]->plaintext) : '';
                                }
                            }
                            if ($trColumn != '') {
                                $prevTds = $tds;
                            }
                        } else if (count($tds) == 2) {
                            $data['common'][$column] = trim($tds[1]->plaintext);
                        }
                    }
                }
                $div = $dom->find('.titre_affiche_avis', 0);
                if (!empty($div)) {
                    $text = $div->plaintext;
                    preg_match('/Impôt\s+(\d{4})\s+sur/', $text, $match);
                    if (!empty($match)) {
                        $data['common']['Year'] = $match[1];
                    }
                }
            }
        }
        return $data;
    }

    private static function curl(string $url, string $method = 'GET', array $request = [], array $headers = [], array $options = [], bool $getCookies = true) {
        if ($method == 'GET' && !empty($request)) {
            $url .= http_build_query($request);
        }

        $curl = curl_init($url);

        if (($method == 'POST' || $method == 'PUT') && !empty($request)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($request));
        }
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        if (!empty($headers)) {
            $_headers = [];
            foreach ($headers as $key => $header) {
                $_headers[] = $key . ': ' . $header;
            }
            curl_setopt($curl, CURLOPT_HTTPHEADER, $_headers);
        }
        foreach ($options as $key => $option) {
            curl_setopt($curl, $key, $option);
        }
        $response = curl_exec($curl);
        if ($getCookies) {
            $headers_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $headers = substr($response, 0, $headers_size);
            $response = substr($response, $headers_size);
            preg_match_all('|Set-Cookie: (.*);|U', $headers, $matches);
            $cookies = implode('; ', $matches[1]);
            $result = ['response' => $response, 'cookies' => $cookies];
        } else {
            $result = $response;
        }
        curl_close($curl);
        return $result;
    }
}
