<?php

use KubAT\PhpSimple\HtmlDomParser;

class SigVilleGouvFr
{
    const BASE_URL = 'https://sig.ville.gouv.fr';
    const ADDRESSES_URL = '/adresses/autocompleteTpl';
    const QPV_SEARCH_URL = '/recherche-adresses-qp-polville-v2019';
    const NOT_FOUND_RESULTS = 'Aucun résultat trouvé';

    public static function getCode($address) {
        $data = [];
        $result = self::curl(
            self::BASE_URL . self::ADDRESSES_URL,
            'POST',
            [
                'term' => $address,
                'maximumResponses' => 1,
            ],
            [
                'X-Requested-With' => 'XMLHttpRequest',
            ],
            [
                CURLOPT_RETURNTRANSFER => true,
            ],
            false
        );
        if (!empty($result) && strpos($result, self::NOT_FOUND_RESULTS) === false) {
            $script = null;
            $result = str_replace("\n", ' ', $result);
            preg_match('/onclick="(.*)">/', $result, $match);
            if (!empty($match)) {
                $script = $match[1];
            }
            if (!empty($script)) {
                $inseecom = null;
                preg_match('/.data\(\'inseecom\', \'(\d+)\'\)/', $script, $match);
                if (!empty($match)) {
                    $inseecom = $match[1];
                }

                $postal = null;
                preg_match('/.data\(\'codepostal\', \'(\d+)\'\)/', $script, $match);
                if (!empty($match)) {
                    $postal = $match[1];
                }

                $scripts = explode(';', $script);

                $commune = null;
                if (!empty($scripts[3])) {
                    $commune = trim(str_replace(["$('#addressInput, #secondaryAddressInput').data('nomcommune', '", "')"], '', $scripts[3]));
                }

                $voie = null;
                if (!empty($scripts[4])) {
                    $voie = trim(str_replace(["$('#addressInput, #secondaryAddressInput').data('nomrue', '", "')"], '', $scripts[4]));
                }

                $num = null;
                if (!empty($scripts[7])) {
                    $num = trim(str_replace(["$('#addressInput, #secondaryAddressInput').data('numadresse', '", "')"], '', $scripts[7]));
                }
                if (!is_null($inseecom) && !is_null($postal) && !is_null($commune) && !is_null($voie) && !is_null($num)) {
                    $result = self::curl(
                        self::BASE_URL . self::QPV_SEARCH_URL,
                        'POST',
                        [
                            'insee_com' => $inseecom,
                            'code_postal' => $postal,
                            'nom_commune' => $commune,
                            'num_adresse' => $num,
                            'nom_voie' => $voie,
                        ],
                        [
                            'X-Requested-With' => 'XMLHttpRequest',
                        ],
                        [
                            CURLOPT_RETURNTRANSFER => true,
                        ],
                        false
                    );
                    $result = json_decode($result, true);
                    $dom = HtmlDomParser::str_get_html($result['popoverResponseTpl']);
                    if (!empty($dom)) {
                        $anchor = $dom->find('a[href]', 0);
                        if (!empty($anchor)) {
                            $code = str_replace('/Territoire/', '', $anchor->href);
                            if (!empty($code)) {
                                $data['code'] = $code;
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }

    private static function curl(string $url, string $method = 'GET', array $request = [], array $headers = [], array $options = [], bool $getCookies = true) {
        if ($method == 'GET' && !empty($request)) {
            $url .= http_build_query($request);
        }

        $curl = curl_init($url);

        if (($method == 'POST' || $method == 'PUT') && !empty($request)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($request));
        }
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        if (!empty($headers)) {
            $_headers = [];
            foreach ($headers as $key => $header) {
                $_headers[] = $key . ': ' . $header;
            }
            curl_setopt($curl, CURLOPT_HTTPHEADER, $_headers);
        }
        foreach ($options as $key => $option) {
            curl_setopt($curl, $key, $option);
        }
        $response = curl_exec($curl);
        if ($getCookies) {
            $headers_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $headers = substr($response, 0, $headers_size);
            $response = substr($response, $headers_size);
            preg_match_all('|Set-Cookie: (.*);|U', $headers, $matches);
            $cookies = implode('; ', $matches[1]);
            $result = ['response' => $response, 'cookies' => $cookies];
        } else {
            $result = $response;
        }
        curl_close($curl);
        return $result;
    }
}