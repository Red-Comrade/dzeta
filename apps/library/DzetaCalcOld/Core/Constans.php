<?php
namespace DzetaCalcParser;
class Constans{

	const ECRAN_SYMBOL=['"'];

	const OPERATIONS_LOW = ['+', '-'];
	const OPERATIONS_HIGH = ['/', '*'];
	const OPERATIONS_VERY_HIGH=['^','++','--'/*,'~','!'*/];
	const OPERATIONS_HIGH_LOGIC=['||','&&'];
	const OPERATIONS_LOGIC=['>','<','>=','<=','!=','=='];
	
	const BRACKETS_SIMPLE=['(',')'];
	const BRACKETS_ARRAY_SIMPLE=['[',']'];
	const BRACKETS_BLOCK_SIMPLE=['{','}'];

	const DELIMITER_FUNCTION=',';
	const DELIMITER_VAR_EXPR='=';
	const DELIMITER_NUMBER_AND_METHOD='.';

	const START_VARIABLE='$';
	const SYMBOL_STRING='"';
	const SYMBOL_ECRAN='\\';
	const SYMBOL_END_EXPRESSION=';';
	

	const SYMBOL_P=[Constans::DELIMITER_NUMBER_AND_METHOD,' ',Constans::DELIMITER_FUNCTION,Constans::DELIMITER_VAR_EXPR,Constans::START_VARIABLE,...Constans::OPERATIONS_LOW,...Constans::OPERATIONS_HIGH,...Constans::OPERATIONS_VERY_HIGH,...Constans::OPERATIONS_HIGH_LOGIC,...Constans::OPERATIONS_LOGIC,...Constans::BRACKETS_SIMPLE,...Constans::BRACKETS_ARRAY_SIMPLE,...Constans::BRACKETS_BLOCK_SIMPLE];


	const TYPE_STRING='string';	
	const TYPE_NUMBER='number';
	const TYPE_ARRAY='array';
	const TYPE_OBJECT='object';
	const TYPE_BOOL='bool';
	const TYPE_NULL='NULL';
	const TYPE_FOR_SPEC='for';

	const TYPE_EXPRESSION_VARIABLE='variable';
	const TYPE_EXPRESSION_NOT_VARIABLE='not-variable';
	
	const TYPE_OPER_SPC='operation';
	const TYPE_EXPR_SPC='expression';
	const TYPE_DELIM_VAR='delim_var';
	const TYPE_DELIM_FUNC='delim_func';	
	const TYPE_VARIABLE='variable';
	const TYPE_FUNCTION='function';
	const TYPE_VARIABLE_EXP='variable_exp';
	const TYPE_BRACKETS_SIMPLE='brackets_simple';
	const TYPE_BRACKETS_ARRAY='brackets_array';

	const TYPE_ACTION_SIMPLE='simple';
	const TYPE_ACTION_FOR='for';
	const TYPE_ACTION_FOR_BREAK='break';
	const TYPE_ACTION_FOR_CONTINUE='continue';
	const TYPE_ACTION_IF='if';
	const TYPE_ACTION_ELSE='else';
	const TYPE_ACTION_ELSEIF='elseif';
	const TYPE_ACTION_SAVE_CONTEXT='saveContext';
	const TYPE_ACTION_SAVE_NOCONTEXT='no_saveContext';//это когда поступила команда на сохранение контекста, но он еше не в JSON

	const SPEC_VALUES=[
		'true'=>[
			'type'=>Constans::TYPE_BOOL,
			'value'=>true
		],
		'false'=>[
			'type'=>Constans::TYPE_BOOL,
			'value'=>false
		],
		'NULL'=>[
			'type'=>Constans::TYPE_NULL,
			'value'=>null
		],
		'break'=>[
			'type'=>Constans::TYPE_FOR_SPEC,
			'value'=>Constans::TYPE_ACTION_FOR_BREAK
		],
		'continue'=>[
			'type'=>Constans::TYPE_FOR_SPEC,
			'value'=>Constans::TYPE_ACTION_FOR_CONTINUE
		]

	];
	
}