<?php

namespace DzetaCalcParser;

class HelperFunctions
{

  public static function AnalisisFunction($dzetaSD,$var,$dopInfo)
  {

  }
  public static function AnalisisVariable($dzetaSD,$var,$dopInfo)
  {
      return;
      //$analisys
      $svvArray=$dzetaSD->GetAllSVarray();
      if(isset($dopInfo['postfixArr']))
      {
        $pf=$dopInfo['postfixArr'];
        $temp = [];
        $hold = 0;
        $arrM=array_merge(Constants::OPERATIONS_HIGH, Constants::OPERATIONS_LOW, Constants::OPERATIONS_COMPARE, Constants::OPERATIONS_LOGIC,Constants::ST);
        $typeFinal='';
        for ($i = 0,$lenPF=count($pf); $i < $lenPF; $i++)
        {

            if (!in_array($pf[$i], $arrM)) {
              //  $temp[$hold++] =$this->SolvePFValaue($pf[$i], $pfVIn);
                if(isset($svvArray[$pf[$i]['value']]))
                {
                  $temp[$hold++] =$svvArray[$pf[$i]['value']]->Type;
                }
            } else {
                if (!isset($temp[$hold - 2])&&!isset($temp[$hold - 1])) {
                //    return $this->GetSysValueForVar("NULL");
                }
               else {
                 //$temp[$hold - 2], $temp[$hold - 1]
                      \DZ::tempH([$temp[$hold - 2], $temp[$hold - 1]]);
                    switch ($pf[$i]) {
                        case '+':
                            $elem1=$temp[$hold - 2];
                            $elem2=$temp[$hold - 1];
                            if($elem1==SystemValue::STRING_TYPE||$elem2==SystemValue::STRING_TYPE)
                            {
                                $temp[$hold - 2] = SystemValue::STRING_TYPE;
                            }elseif(($elem1==SystemValue::STRING_TYPE&&$elem2==SystemValue::NUMBER_TYPE)||
                            ($elem1==SystemValue::NUMBER_TYPE&&$elem2==SystemValue::STRING_TYPE))
                            {
                                $temp[$hold - 2] = SystemValue::STRING_TYPE;
                            }
                        break;
                      }
                    $hold = $hold - 1;
                }
            }
        }
        if (isset($temp[$hold - 1])) {
            $typeFinal=$temp[$hold - 1];
        }
        $dzetaSD->analisys[$var][]=['type'=>$typeFinal];
      }
  }



  /**
   *  The function does the substitution of strings for special values
   *
   * @param DzetaCalc $dzetaSD
   *
   *
   */
    public static function ReplaceStrValue($dzetaSD)
    {

        $frml=$dzetaSD->frmlMain;
        $frml=str_replace('\\"', 'ecranbgjfggfgfgffg',$frml );
        $lengthStroka=mb_strlen($frml);
        $symbolStr='"';
        $newFrml='';
        $strTmp='';
        $flagStr=false;

        for ($indexStr=0;$indexStr<$lengthStroka;$indexStr++) {
            $symb=mb_substr($frml,$indexStr,1);
		//	echo $symb;
			
            if ($symb==$symbolStr) {
                if ($flagStr) {
                    $strTmp=str_replace('ecranbgjfggfgfgffg','"', $strTmp );
                    $newFrml.=$dzetaSD->AddSysVal(new SystemValue($strTmp, SystemValue::STRING_TYPE), false);
                    $strTmp='';
                    $flagStr=false;
                } else {
                    $flagStr=true;
                    $strTmp='';
                }
            } elseif ($flagStr) {
                $strTmp.=$symb;
            } else {
                $newFrml.=$symb;
            }
        }
        if ($flagStr) {
            $newFrml.=$dzetaSD->AddSysVal(new SystemValue($strTmp, SystemValue::STRING_TYPE), false);
            $strTmp='';
            $flagStr=false;
        }
        $newFrml=str_replace('[', '.[', $newFrml);
        $newFrml=str_replace(' ', '', $newFrml);
        $newFrml=str_replace("\n", '', $newFrml);
        $newFrml=str_replace("==", '!!', $newFrml);
        $newFrml=str_replace("!=", '@@', $newFrml);
        
     $dzetaSD->frmlPars=$newFrml;
    
  //   $dzetaSD->frmlPars='$x=5';
    }


    /**
     *  Builds a complete calculation tree
     * @param DzetaCalc $DzetaCalc
     * @param string    $Formula
     * @param string    $strPath
     *
     *  @return array[]
     */
    public static function createTreeFormula($DzetaCalc, $Formula, $strPath = "")
    {
        $treeArray = array();
        while (1) {
            $positionTC = mb_strpos($Formula, ";");
            $positionTC = $positionTC === false ? mb_strlen($Formula) : $positionTC;

            $positionSC = mb_strpos($Formula, "{");
            $positionSC = empty($positionSC) ? -2 : $positionSC;

            if (mb_strlen($Formula) == 0) {
                break;
            }
            if (($positionSC < $positionTC && $positionSC > 0)) {
                $t1 = self::find_closing_bracket($Formula, $positionSC, '{}');
                $vnutr = mb_substr($Formula, $positionSC + 1, $t1 - $positionSC - 1);
                $startV = $positionSC - 2;
                $positionTT = mb_strpos($Formula, "(");
                $t = self::find_closing_bracket($Formula, $positionTT, '()');
                $expre = mb_substr($Formula, $positionTT + 1, $t - $positionTT - 1);
                $nameF = mb_substr($Formula, 0, $positionTT);
                $Formula = mb_substr($Formula, $t1 + 1);
                if ($nameF == "if") {
                    $possssEnd = $t1;
                    $dopIF = array();
                    $arrIF = array("type" => "if",
                        "condition" => self::createTreeFormula($DzetaCalc, $expre, $strPath . count($treeArray) . "_condition-"),
                        "expression" => self::createTreeFormula($DzetaCalc, $vnutr, $strPath . count($treeArray) . "_expression-"));
                    while (1) {
                        $flagiiff = false;
                        if (mb_strlen($Formula) == 0) {
                            break;
                        }
                        $str1 = "else{";
                        $tyuuu = mb_strpos($Formula, $str1);
                        if ($tyuuu === 0) {
                            $positionSC = mb_strpos($Formula, "{");
                            $t1 = self::find_closing_bracket($Formula, $positionSC, '{}');
                            $vnutr = mb_substr($Formula, $positionSC + 1, $t1 - $positionSC - 1);
                            $Formula = mb_substr($Formula, $t1 + 1);
                            $tmpV = mb_substr($Formula, 1);
                            if ($tmpV == ";") {
                                $Formula = mb_substr($Formula, 1);
                            }
                            $dopIF[] = array("type" => "else",
                                "expression" => self::createTreeFormula($DzetaCalc, $vnutr, $strPath . count($treeArray) . "_dopif-" . count($dopIF) . "_expression-"));
                            $possssEnd = $t1 + 1;
                            $flagiiff = true;
                        }
                        $str1 = "elseif(";
                        $tyuuu = mb_strpos($Formula, $str1);
                        if ($tyuuu === 0) {
                            $positionSC = mb_strpos($Formula, "{");
                            $t1 = self::find_closing_bracket($Formula, $positionSC, '{}');
                            $vnutr = mb_substr($Formula, $positionSC + 1, $t1 - $positionSC - 1);
                            $positionTT = mb_strpos($Formula, "(");
                            $t = self::find_closing_bracket($Formula, $positionTT, '()');
                            $expre = mb_substr($Formula, $positionTT + 1, $t - $positionTT - 1);
                            $nameF = mb_substr($Formula, 0, $positionTT);
                            $Formula = mb_substr($Formula, $t1 + 1);
                            $tmpV = mb_substr($Formula, 1);
                            if ($tmpV == ";") {
                                $Formula = substr($Formula, 1);
                            }
                            $dopIF[] = array("type" => "elseif",
                                "condition" => self::createTreeFormula(
                                    $DzetaCalc,
                                    $expre,
                                    $strPath . count($treeArray) . "_dopif-" . count($dopIF) . "_conditional-"
                                ),
                                "expression" => self::createTreeFormula(
                                    $DzetaCalc,
                                    $vnutr,
                                    $strPath . count($treeArray) . "_dopif-" . count($dopIF) . "_expression-"
                                ));
                            $possssEnd = $t1 + 1;
                            $flagiiff = true;
                        }
                        if (empty($flagiiff)) {
                            break;
                        }
                    }
                    $arrIF["dopif"] = $dopIF;
                    $treeArray[] = $arrIF;
                }

                if ($nameF == "for") {
                    $cond = self::explode_func_args($expre, ',');
                    $treeVnutrfor = self::createTreeFormula($DzetaCalc, $vnutr, $strPath . count($treeArray) . "_body-");
                    if (count($cond) == 3) {
                        $treeArray[] = array("type" => "for",
                            "declaration" =>self::createTreeFormula($DzetaCalc, $cond[0], $strPath . count($treeArray) . "_declaration-"),
                            "conditional" => self::createTreeFormula($DzetaCalc, $cond[1], $strPath . count($treeArray) . "_conditional-"),
                            "step" => self::createTreeFormula($DzetaCalc, $cond[2], $strPath . count($treeArray) . "_step-"),
                            "body" => $treeVnutrfor);
                    } else {
                        Errors::AddError($DzetaCalc, ['ns'=>'for','func'=>'global','num'=>0]);
                        return null;
                    }
                }
            } else {
                $positionTC = $positionTC === false ? mb_strlen($Formula) : $positionTC;
                $expression = mb_substr($Formula, 0, $positionTC);
                $expressionM=$expression;
                $newFrmul='';
                $arrTmp=[];

                $st1=Constants::OPERATIONS_HIGH;
                $st2=Constants::OPERATIONS_LOW;
                $st3=Constants::OPERATIONS_COMPARE;
                $st4=Constants::OPERATIONS_LOGIC;
                $st0=array('^', '!', '~');
                $symblP=array_merge(Constants::OPERATIONS_HIGH, Constants::OPERATIONS_LOW, Constants::OPERATIONS_COMPARE, Constants::OPERATIONS_LOGIC,['!']);
                $symblPLine=Constants::BRACKETS_LINE;
                $posRavno=mb_strpos($expression, "=");
                $varGlobal=null;
                $actionsVarGlobal=[];
                if ($posRavno===false) {
                    $varGlobal=null;
                } else {
                    $varGlobal=mb_substr($expression, 0, $posRavno);
                    \DZ::varrrt($varGlobal);
                    $expression=mb_substr($expression, $posRavno+1);
                    $posT=mb_strpos($varGlobal, ".");
                    if ($posT!==false) {//тут не повезло
                        $arrF=[];
                        $tmpFrm='';
                        $lenVar=mb_strlen($varGlobal);
                        $indVa=$posT+1;
                        while (1) {
                            $symb=mb_substr($varGlobal, $indVa, 1);
                            if ($symb=='.') {
                                $tmpFrm='';
                            } elseif ($symb=='[') {
                                $cls=self::find_closing_bracket($varGlobal, $indVa, '[]');
                                $tmpFrmm=mb_substr($varGlobal, $indVa+1, $cls-$indVa-1);
                                $arrF[]='elemArrIndex('.$tmpFrmm.')';
                                $indVa=$cls;
                            } elseif ($symb=='(') {
                                $cls=self::find_closing_bracket($varGlobal, $indVa);
                                $tmpFrmm=mb_substr($varGlobal, $indVa+1, $cls-$indVa-1);
                                $arrF[]=$tmpFrm.'('.$tmpFrmm.')';
                                $indVa=$cls;
                            } else {
                                $tmpFrm.=$symb;
                            }
                            $indVa++;
                            if ($indVa>=$lenVar) {
                                break;
                            }
                        }
                        foreach ($arrF as $valDopF) {
                            $actionsVarGlobal=array_merge($actionsVarGlobal, self::createTreeFormula(
                          $DzetaCalc,
                          $valDopF,
                          $strPath . (count($actionsVarGlobal)+1) . "_var-"
                      ));
                        }

                        $varGlobal=mb_substr($varGlobal, 0, $posT);
                    }
                }

                $len=mb_strlen($expression);

                for ($i=0,$len=mb_strlen($expression);$i<$len;$i++) {
                    $symb=mb_substr($expression, $i, 1);
                    if ($symb=='('||$symb==')') {
                        $newFrmul.=$symb;
                    } elseif ($symb=="'") {
                        //тут что заэкранированное
                        $actionsVar=[];
                        $var="'";
                        $i++;
                        $brak=0;

                        while (1) {
                            $symb=mb_substr($expression, $i, 1);
                            if (in_array($symb, $symblP)&&$brak==0) {
                                $i--;
                                break;
                            }
                            if ($symb==')'&&$brak==0) {
                                $i--;
                                break;
                            }

                            if ($symb=='(') {
                                $brak++;
                            } elseif ($symb==')') {
                                $brak--;
                            }
                            $var.=$symb;
                            $i++;
                            if ($i>=$len) {
                                break;
                            }
                        }

                        $posT=mb_strpos($var, ".");
                        if ($posT!==false) {//тут не повезло
                            $indVa=$posT+1;
                            $arrF=[];
                            $tmpFrm='';
                            $lenVar=mb_strlen($var);
                            while (1) {
                                $symb=mb_substr($var, $indVa, 1);
                                if ($symb=='.') {
                                    $tmpFrm='';
                                } elseif ($symb=='(') {
                                    $cls=self::find_closing_bracket($var, $indVa);
                                    $tmpFrmm=mb_substr($var, $indVa+1, $cls-$indVa-1);
                                    $arrF[]=$tmpFrm.'('.$tmpFrmm.')';
                                    $indVa=$cls;
                                } else {
                                    $tmpFrm.=$symb;
                                }
                                $indVa++;
                                if ($indVa>=$lenVar) {
                                    break;
                                }
                                if ($i>=$len) {
                                    break;
                                }
                            }

                            foreach ($arrF as $valDopF) {
                                $actionsVar[]=self::createTreeFormula($DzetaCalc, $valDopF, $strPath . (count($actionsVar)+1) . "_var-");
                            }

                            $var=mb_substr($var, 0, $posT);
                        }
                        $arrTmp[]=['value'=>$var,'actions'=>$actionsVar];
                        $newFrmul.="'".(count($arrTmp)-1)."'";

                    } elseif ($symb=='$') {
                        //начало переменной идет
                        /*
                        1-Выделяем полностью выражение где есть переменная
                        2-делаем разделение по точке, чтобы понять что есть дополнительные функции, их выуживаем по одной
                        */
                        $var='';
                        $actionsVar=[];
                        while (1) {
                            $symb=mb_substr($expression, $i, 1);
                            if (in_array($symb, $symblP)) {
                                $i--;
                                break;
                            }
                            $var.=$symb;
                            $i++;
                            if ($i>=$len) {
                                break;
                            }
                        }

                        $posT=mb_strpos($var, ".");
                        $varT=$var;

                        if ($posT!==false) {//тут не повезло
                            $var=mb_substr($var, 0, $posT);
                            $indVa=$posT+1;
                            $arrF=[];
                            $tmpFrm='';
                            $lenVar=mb_strlen($varT);
                            while (1) {
                                $symb=mb_substr($varT, $indVa, 1);
                                if ($symb=='.') {
                                    $tmpFrm='';
                                } elseif ($symb=='[') {
                                    $cls=self::find_closing_bracket($varT, $indVa, '[]');
                                    $tmpFrmm=mb_substr($varT, $indVa+1, $cls-$indVa-1);

                                    $arrF[]='elemArr('.$tmpFrmm.')';
                                    $indVa=$cls;
                                } elseif ($symb=='(') {
                                    $cls=self::find_closing_bracket($varT, $indVa);
                                    $tmpFrmm=mb_substr($varT, $indVa+1, $cls-$indVa-1);
                                    $arrF[]=$tmpFrm.'('.$tmpFrmm.')';
                                    $indVa=$cls;
                                } else {
                                    $tmpFrm.=$symb;
                                }
                                $indVa++;
                                if ($indVa>=$lenVar) {
                                    break;
                                }
                            }
                            foreach ($arrF as $valDopF) {
                                $actionsVar[]=self::createTreeFormula($DzetaCalc, $valDopF, $strPath . (count($actionsVar)+1) . "_var-");
                            }


                        } else {
                            //TODO тут еще доьавим на валидность переменные
                        }
                       
                        $arrTmp[]=['variable'=>$var,'actions'=>$actionsVar];
                        $newFrmul.="'".(count($arrTmp)-1)."'";
                    } elseif (is_numeric($symb)) {
                        /*
                        Выделение чисел
                        1. Выделяем полностью конструкцию
                        2. Выделяем именно число
                        3. Парсим на кусочки
                        */
                        $num='';
                        $brak=0;
                        while (1) {
                            $symb=mb_substr($expression, $i, 1);
                            if (in_array($symb, $symblP)&&$brak==0) {
                                $i--;
                                break;
                            }
                            if ($symb==')'&&$brak==0) {
                                $i--;
                                break;
                            }

                            if ($symb=='(') {
                                $brak++;
                            } elseif ($symb==')') {
                                $brak--;
                            }

                            $num.=$symb;
                            $i++;
                            if ($i>=$len) {
                                break;
                            }
                        }
                        $flagDot=false;
                        $number='';
                        for ($indexStr=0,$lenNum=mb_strlen($num);$indexStr<$lenNum;$indexStr++) {
                            $symbNum=$num[$indexStr];
                            if ($symbNum=='.'&&!$flagDot) {
                                $number.='.';
                                $flagDot=true;
                                //надо смотреть след символ
                                if (isset($num[$indexStr+1])&&is_numeric($num[$indexStr+1])) {
                                    //  все ок идем далее
                                } else {
                                    break;
                                }
                            } elseif ($symbNum=='.'&&$flagDot) {
                                //тут стоп
                                break;
                            } elseif (is_numeric($symbNum)) {
                                $number.=$symbNum;
                            } else {
                                //TODO ERRR
                                Errors::AddError($Dzeta, ['ns'=>'HelperFunctions','func'=>'createTreeFormula','num'=>0],[$lenNum]);
                                break;
                            }
                        }
                        $indVa=$indexStr+1;
                        $arrF=[];
                        $tmpFrm='';
                        $lenVar=mb_strlen($num);
                        while (1) {
                            $symb=mb_substr($num, $indVa, 1);
                            if ($symb=='.') {
                                $tmpFrm='';
                            } elseif ($symb=='(') {
                                $cls=self::find_closing_bracket($num, $indVa);
                                $tmpFrmm=mb_substr($num, $indVa+1, $cls-$indVa-1);
                                $arrF[]=$tmpFrm.'('.$tmpFrmm.')';
                                $indVa=$cls;
                            } else {
                                $tmpFrm.=$symb;
                            }
                            $indVa++;
                            if ($indVa>=$lenVar) {
                                break;
                            }
                        }
                        $actionsVar=[];
                        foreach ($arrF as $valDopF) {
                            $actionsVar[]=self::createTreeFormula($DzetaCalc, $valDopF, $strPath . (count($actionsVar)+1) . "_num-");
                        }
                        $arrTmp[]=['value'=>$DzetaCalc->AddSysVal(new SystemValue((float)$num, SystemValue::NUMBER_TYPE),false),
                      'actions'=>$actionsVar];

                        $newFrmul.="'".(count($arrTmp)-1)."'";
                    } elseif (in_array($symb, $symblP)) {
                        $newFrmul.=$symb;
                    } else {
                        $func='';
                        $flagFunc=false;
                        while (1) {
                            $symb=mb_substr($expression, $i, 1);
                            if ($symb=='(') {
                                $flagFunc=true;
                                break;
                            }
                            $func.=$symb;
                            $i++;
                            if ($i>=$len) {
                                break;
                            }
                        }


                        if ($flagFunc) {
                            $cls=self::find_closing_bracket($expression, $i);
                            $tmpFrmm=mb_substr($expression, $i+1, $cls-$i-1);

                            $yhyhj=self::explode_func_args($tmpFrmm);
                           
                            if($func=='test_setGroup1')
                            {                                
                                $arrTmp[]=["func"=>$func,'params'=>[]];
                                $newFrmul.="'".(count($arrTmp)-1)."'";
                                $i=$cls;
                                $DzetaCalc->testAttrGroups[]=['nameAttr'=>$DzetaCalc->SV_ARRAY[$yhyhj[0]]->Value,'group'=>1];
                            }else{
                                $paramsF=[];
                                foreach ($yhyhj as $key => $value) {
                                    $paramsF=array_merge($paramsF, self::createTreeFormula($DzetaCalc, $value, $strPath . (count($paramsF)+1) . "_func-"));
                                }
                                $arrTmp[]=["func"=>$func,'params'=>$paramsF];
                                $newFrmul.="'".(count($arrTmp)-1)."'";
                                $i=$cls;
                            }
                            
                        } else {
                            // тут значит простые выражения типа true, false, break
                            if ($func=='true'||$func=='false'||$func=='NULL') {
                                $arrTmp[]=["variable"=>$func,'params'=>[]];
                                $newFrmul.="'".(count($arrTmp)-1)."'";
                            } else {
                                $arrTmp[]=["func"=>$func,'params'=>[]];
                                $newFrmul.="'".(count($arrTmp)-1)."'";
                            }
                        }
                    }
                }


                $pf = array();
                $ops = new Stack();
                $infix = preg_replace("/\s/", "", $newFrmul);
                $pfIndex = 0;
                $lChar = '';
                $length=mb_strlen($infix);

                $pf=[];

                for ($i = 0; $i < $length; $i++) {
                    if (($length - $i) > 2) {
                        $chr = mb_substr($infix, $i, 1);
                        $chr1 = mb_substr($infix, $i + 1, 1);
                        if (in_array($chr . $chr1, array_merge($st3, $st4))) {
                            $chr = $chr . $chr1;
                        }
                    } else {
                        $chr = mb_substr($infix, $i, 1);
                    }

                    if ($chr == '\'') {
                        $pf[$pfIndex]=isset($pf[$pfIndex])?$pf[$pfIndex]:'';
                        $pf[$pfIndex]= "'";
                        $i++;
                        $chr = mb_substr($infix, $i, 1);
                        $tempZ = 0;
                        while ($chr != '\'') {

                            $tempZ++;
                            $pf[$pfIndex] .= $chr;
                            $i++;
                            $chr =mb_substr($infix, $i, 1);
                            if ($i >= mb_strlen($infix)) {
                              Errors::AddError($Dzeta, ['ns'=>'HelperFunctions','func'=>'createTreeFormula','num'=>1],['fb341ef0661']);
                              //  return array();
                            }
                        }
                        $pf[$pfIndex] .= $chr;
                    } elseif (in_array($chr, ["("])) {
                        $ops->push($chr);
                    } elseif (in_array($chr, [")"])) {
                        $key = array_search($chr, [")"]);
                        $ic = 0;
                        while ($ops->peek() != ["("][$key]) {
                            $ic++;
                            if ($ic == 100) {
                              Errors::AddError($Dzeta, ['ns'=>'HelperFunctions','func'=>'createTreeFormula','num'=>1],['0916fc2fbc']);
                              //  return array();
                            }
                            $nchr = $ops->pop();
                            if ($nchr) {
                                $pf[++$pfIndex] = $nchr;
                            } else {
                              Errors::AddError($Dzeta, ['ns'=>'HelperFunctions','func'=>'createTreeFormula','num'=>1],['0f2358681']);
                            }
                        }
                        $ops->pop();
                    } elseif (in_array($chr, $st0)) {
                        while (in_array($ops->peek(), $st0)) {
                            $pf[++$pfIndex] = $ops->pop();
                        }
                        $ops->push($chr);
                        $pfIndex++;
                    } elseif (in_array($chr, $st1)) {
                        while (in_array($ops->peek(), $st1) || in_array($ops->peek(), $st0)) {
                            $pf[++$pfIndex] = $ops->pop();
                        }

                        $ops->push($chr);
                        $pfIndex++;
                    } elseif (in_array($chr, $st3)) {
                        while (in_array($ops->peek(), $st3) ||
                        in_array($ops->peek(), $st0) ||
                        in_array($ops->peek(), $st1) ||
                        in_array($ops->peek(), $st2)) {
                            $pf[++$pfIndex] = $ops->pop();
                        }
                        $ops->push($chr);
                        $pfIndex++;
                        if (strlen($chr) == 2) {
                            $i++;
                        }
                    } elseif (in_array($chr, $st4)) {
                        while (in_array($ops->peek(), $st4) ||
                        in_array($ops->peek(), $st0) ||
                        in_array($ops->peek(), $st1) ||
                        in_array($ops->peek(), $st2)) {
                            $pf[++$pfIndex] = $ops->pop();
                        }
                        $ops->push($chr);
                        $pfIndex++;
                        if (strlen($chr) == 2) {
                            $i++;
                        }
                    } elseif (in_array($chr, $st2)) {
                     if ((in_array($lChar, array_merge($st1, $st2, $st0, ["("])) || $lChar == "") && $chr == "-") {
                         while (in_array($ops->peek(), $st0)) {
                             $pf[++$pfIndex] = $ops->pop();
                         }
                         $ops->push('?-');
                         $pfIndex++;
                       } else {
                            while (in_array($ops->peek(), array_merge($st1, $st2, $st0))) {
                                $pf[++$pfIndex] = $ops->pop();
                            }
                            $ops->push($chr);
                            $pfIndex++;
                        }
                    } else {
                      Errors::AddError($Dzeta, ['ns'=>'HelperFunctions','func'=>'createTreeFormula','num'=>1],['091hth2fbc']);

                    }

                    $lChar = $chr;
                }

                while (($tmp = $ops->pop()) !== false) {
                    $pf[++$pfIndex] = $tmp;
                }
                $pf = array_values($pf);


                if ($expression != "") {
                    $newArrFin=[];

                    foreach ($pf as $key => $valpf) {
                        $tm=str_replace("'", '', $valpf);
                        if (is_numeric($tm)) {
                            $newArrFin[]=$arrTmp[$tm];
                        } else {
                            $newArrFin[]=$valpf;
                        }
                    }
                    self::AnalisisVariable($DzetaCalc,$varGlobal,
                    ["postfixArr"=>$newArrFin,
                    "actionsVar"=>$actionsVarGlobal]);
                    $treeArray[] = array("type" => "simple",
                      "postfixArr"=>$newArrFin,
                      "var"=>$varGlobal,
                      "actionsVar"=>$actionsVarGlobal,
                     "expression" => $expressionM, "path" => $strPath . count($treeArray) . "-");
                }
                $Formula = mb_substr($Formula, $positionTC + 1);
            }
        }
        return $treeArray;
    }


    /**
     *  Find closing bracket for formula
     *
     * @param string $expression
     * @param int $pos
     * @param string $brackets
     *
     *  @return int|NULL
     */
    public static function find_closing_bracket($expression, $pos = 0, $brackets = '()')
    {
        $closePos = null;
        $counter = 1;
        for ($br = $pos + 1, $strlen = mb_strlen($expression); $br < $strlen; $br++) {
            $counter += $expression[$br] == $brackets[0]  ? 1 : 0;
            $counter += $expression[$br] == $brackets[1]  ? -1 : 0;
            if ($counter === 0) {
                $closePos = $br;
                break;
            }
        }
        return $closePos;
    }

    /**
     *  Explode formula on arguments
     *
     * @param string $formula
     * @param string $sign
     *
     *
     *  @return array[]
     */

    public static function explode_func_args($formula, $sign = ",")
    {
        $args_array = array();
        $brak = 0;
        $lastV = 0;
        for ($i = 0,$len = mb_strlen($formula);$i < $len; $i++) {
            $charFrm = mb_substr($formula, $i, 1);
            if (($i + 1) == $len) {
                $args_array[] = mb_substr($formula, ($lastV == 0 ? $lastV : $lastV + 1), $len - $lastV);
            }
            if ($charFrm == $sign && $brak == 0) {
                $args_array[] = mb_substr($formula, ($lastV == 0 ? $lastV : $lastV + 1), ($lastV == 0 ? $i - $lastV : $i - $lastV - 1));
                // $i++;
                $lastV = $i;
                if (($i + 1) == $len) {
                    $i--;
                }
            }
            if ($charFrm=='(') {
                $brak++;
            }
            if ($charFrm==')') {
                $brak--;
            }
        }
        return $args_array;
    }
}
