<?php

namespace DzetaCalcParser;

class Constants
{
    const ST = ['~','!','^','?-','@'];
    const OPERATIONS_HIGH = ['/', '*','%'];
    const OPERATIONS_LOW = ['+', '-'];
    const OPERATIONS_COMPARE = ['>=', '<=', '!!', '>', '<', '!=','@@'];
    const OPERATIONS_LOGIC = ['&&', '||'];
    const BRACKETS_LINE = ['(', ')'];

    const EXPRESSION_ERROR='ERR';

    const ERROR_CRITICAL='critical';
    const ERROR_WARNING='warning';

    const BREAK_FLAG    = 'break';
    const CONTINUE_FLAG = 'continue';

}
