<?php

namespace DzetaCalcParser;

abstract class BaseFunctions
{
    abstract protected static function DoGetFunctions();
    abstract protected static function DoGetMethods();

    public static function GetFunctions($onlyNames = true)
    {
        $functions = static::DoGetFunctions();
        foreach ($functions as $key => $function) {
            if ($onlyNames) {
                $functions[$key] = static::GetFullFunctionName($function['name']);
            } else {
                $functions[$key]['name'] = static::GetFullFunctionName($function['name']);
            }
        }
        return $functions;
    }

    public static function GetFunctionsCompletions()
    {
        $methods = static::DoGetFunctions();
        $category = static::GetClassName();
        foreach ($methods as $key => $method) {
            if (isset($method['completion'])) {
                $methods[$key] = $method['completion'];
                $methods[$key]['name'] = $key;
            } else {
                unset($methods[$key]);
            }
        }
        return ['category' => $category, 'functions' => array_values($methods)];
    }

    public static function GetMethods($onlyNames = true)
    {
        $methods = static::DoGetMethods();
        foreach ($methods as $key => $method) {
            if ($onlyNames) {
                $methods[$key] = static::GetFullFunctionName($method['name']);
            } else {
                $methods[$key]['name'] = static::GetFullFunctionName($method['name']);
            }
        }
        return $methods;
    }

    public static function GetMethodsCompletions()
    {
        $methods = static::DoGetMethods();
        foreach ($methods as $key => $method) {
            if (isset($method['completion'])) {
                $methods[$key] = $method['completion'];
                $methods[$key]['name'] = $key;
            } else {
                unset($methods[$key]);
            }
        }
        return array_values($methods);
    }

    protected static function GetNameFunctions($function)
    {
        $functions = array_merge(static::GetFunctions());
        return array_search($function, $functions);
    }

    protected static function GetNameMethods($method)
    {
        $methods = array_merge(static::GetMethods());
        return array_search($method, $methods);
    }

    protected static function GetClassName()
    {
        $reflect = new \ReflectionClass(get_called_class());
        $class = $reflect->getShortName();
        return $class;
    }

    protected static function GetFullFunctionName($function, $class = null)
    {
        $class = is_null($class) ? self::GetClassName() : $class;
        $fullname = __NAMESPACE__ . "\\$class::$function";
        return $fullname;
    }
}
