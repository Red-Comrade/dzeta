<?php
namespace DzetaCalcParser;

class SystemValue
{
    const NUMBER_TYPE = 'number';
    const STRING_TYPE = 'string';
    const ARRAY_TYPE  = 'array';
    const OBJECT_TYPE = 'object';
    const DATETIME_TYPE = 'datetime';
    const BOOL_TYPE = 'bool';
    const NULL_TYPE = 'null';

    const  DATETIME_FORMAT='d/m/Y H:i:s';

    public $Type;
    public $Value;
    public $dopSett=[];

    public function __construct($value, $type,$dopSett=[])
    {
        $this->Value=$value;
        $this->Type=$type;
        $this->dopSett=$dopSett;
    }
    public function GetValueToString()
    {
        if($this->Type==self::DATETIME_TYPE)
        {
           return  $this->Value->format(self::DATETIME_FORMAT);
        }
        return $this->Value;
    }
    public function GetValue()
    {
        return $this->Value;
    }
    public function GetType()
    {
        return $this->Type;
    }
    public function GetRawValue()
    {
        $type = $this->GetType();
        if ($type == self::ARRAY_TYPE) {
            $array = [];
            foreach ($this->GetValue() as $value) {
                $array[] = $value->GetRawValue();
            }
            $raw = $array;
        } else if ($type == self::NULL_TYPE) {
            $raw = null;
        } else {
            $raw = $this->GetValue();
        }
        return $raw;
    }
}
