<?php

namespace DzetaCalcParser;

class Errors
{
    //abcdefghijklmnopqrstuvwxyz

    const Errors=[


    'ArrFunc'=>[
        'getElem'=>[
                    ['en'=>'Getting an element not at the array',
                     'ru'=>'Получение элемента не у массива',
                     'fr'=>"Obtenir un élément qui n'est pas dans le tableau",
                     'type'=>Constants::ERROR_WARNING
                   ],
                    ['en'=>'Missing element in array',
                     'ru'=>'Отсутсвует элемент в массиве',
                     'fr'=>"Élément manquant dans le tableau",
                     'type'=>Constants::ERROR_WARNING
                    ]
                 ]
    ],
    'Core'=>[
        'GetValueForVar'   =>[[ 'en'=>'Error getting variable value',
                             'ru'=>'Ошибка при получении значения переменной',
                             'fr'=>"Erreur lors de l'obtention de la valeur variable",
                             'type'=>Constants::ERROR_CRITICAL
                      ]],
        'GetSysValueForVar'=>[[  'en'=>'Error getting variable sys-value',
                                 'ru'=>'Ошибка при получении системного-значения переменной',
                                 'fr'=>"Erreur lors de l'obtention de la valeur sys-variable",
                                 'type'=>Constants::ERROR_CRITICAL
                      ]],
      ],
      'DB'=>[
          'class'   =>[[     'en'=>'Class {0} not found',
                             'ru'=>'Класс {0} не найден',
                             'fr'=>"Classe {0} introuvable",
                             'type'=>Constants::ERROR_CRITICAL
                      ]],
          'getAttr'   =>[[     'en'=>'Parameter {0} not found',
                               'ru'=>'Параметр {0} не найден',
                               'fr'=>"Paramètre {0} introuvable",
                               'type'=>Constants::ERROR_CRITICAL
                        ]],
          'GetSysValueForVar'=>[[  'en'=>'Error getting variable sys-value',
                                   'ru'=>'Ошибка при получении системного-значения переменной',
                                   'fr'=>"Erreur lors de l'obtention de la valeur sys-variable",
                                   'type'=>Constants::ERROR_CRITICAL
                        ]],
        ],
      'for'=>[
          'cond'   =>[[ 'en'=>'You need to simplify the execution condition in for',
                       'ru'=>'Необходимо упростить условие выполнения в for',
                       'fr'=>"Vous devez simplifier la condition d'exécution pour",
                       'type'=>Constants::ERROR_CRITICAL
                ]],
        'declaration'   =>[[ 'en'=>'Need to simplify declaration in for',
                             'ru'=>'Необходимо упростить объявление в for',
                             'fr'=>"Besoin de simplifier la déclaration pour",
                             'type'=>Constants::ERROR_CRITICAL
                      ]],
        'global'   =>[[ 'en'=>'Error in the description of the for',
                             'ru'=>'Ошибка в описании цикла for',
                             'fr'=>"Erreur dans la description du for",
                             'type'=>Constants::ERROR_CRITICAL
                      ]],

        ],
      'Global'=>[
          'Func'   =>[  [  'en'=>'Invalid number of parameters in method {0}',
                               'ru'=>'{0} не используется как метод',
                               'fr'=>"Nombre de paramètres non valide dans la méthode {0}",
                               'type'=>Constants::ERROR_CRITICAL
                        ],
                        [  'en'=>'{0} method not known',
                           'ru'=>'Метод {0} не известен',
                           'fr'=>"La méthode {0} inconnue",
                           'type'=>Constants::ERROR_CRITICAL
                        ],
                        [  'en'=>'{0} function not known',
                           'ru'=>'Функция {0} не известна',
                           'fr'=>"Fonction {0} inconnue",
                           'type'=>Constants::ERROR_CRITICAL
                        ],

                    ],
          'Method'   =>[  [  'en'=>'In {0} passed {1} value of invalid type',
                               'ru'=>'В {0}  передано {1} значение недопустимого типа',
                               'fr'=>"Dans {0} passé {1} valeur de type non valide",
                               'type'=>Constants::ERROR_CRITICAL
                        ],
                        [  'en'=>'Invalid number of parameters passed to method {0}',
                           'ru'=>'В метод {0}  передано неверное количество параметров',
                           'fr'=>"Nombre de paramètres transmis à la méthode {0} non valide",
                           'type'=>Constants::ERROR_CRITICAL
                    ]
                    ],
          'ParamsFunc'   =>[[ 'en'=>'{0} not used as a method',
                             'ru'=>'Неверное количество параметров в функции {0}',
                             'fr'=>"{0} pas utilisé comme méthode",
                             'type'=>Constants::ERROR_CRITICAL
                        ],
                        [  'en'=>'{1} parameter in the function {0} is of the wrong type',
                           'ru'=>'{1} параметр в функции {0} имеет неверный тип',
                           'fr'=>"{1} paramètre dans la fonction {0} est du mauvais type",
                           'type'=>Constants::ERROR_CRITICAL
                        ],
                        [  'en'=>'Method {0} is not supported for this value',
                           'ru'=>'Метод {0} не поддерживается у данного значения',
                           'fr'=>"La méthode {0} n'est pas prise en charge pour cette valeur",
                           'type'=>Constants::ERROR_CRITICAL
                        ],
                        [  'en'=>'{0} value not supported by method {1}',
                           'ru'=>'Значение {0} не поддерживается у метода {1}',
                           'fr'=>"Valeur de {0} non prise en charge par la méthode {1}",
                           'type'=>Constants::ERROR_CRITICAL
                        ],
                        [  'en'=>'{0} value not supported by method {1}',
                           'ru'=>'В ',
                           'fr'=>"Valeur de {0} non prise en charge par la méthode {1}",
                           'type'=>Constants::ERROR_CRITICAL
                        ],
                      ],
        ],
        'HelperFunctions'=>[
          'createTreeFormula'   =>[[ 'en'=>'Error in number {0} record format',
                               'ru'=>'Ошибка в формате записи числа {0}',
                               'fr'=>"Erreur dans le format d'enregistrement numérique {0}",
                               'type'=>Constants::ERROR_CRITICAL
                        ],
                        [ 'en'=>'Error parsing formula {0}',
                                             'ru'=>'Ошибка при парсинге формулы {0}',
                                             'fr'=>"Erreur d'analyse de la formule {0}",
                                             'type'=>Constants::ERROR_CRITICAL
                                      ]
                      ],
        ],
      'MathOperations'=>[
          'Division'=>[['en'=>'Division by 0 is forbidden',
                       'ru'=>'Деление на 0 запрещено',
                       'fr'=>'La division par 0 est interdite',
                       'type'=>Constants::ERROR_CRITICAL
                     ],
                     'Global'=>[['en'=>'Subtraction allowed only for numbers',
                                  'ru'=>'Для операции {0} разрешены только числа',
                                  'fr'=>'Soustraction autorisée uniquement pour les nombres',
                                  'type'=>Constants::ERROR_CRITICAL
                                ]],
           'Mod'=>[['en'=>'Division by 0 is forbidden',
                        'ru'=>'Деление на 0 запрещено',
                        'fr'=>'La division par 0 est interdite',
                        'type'=>Constants::ERROR_CRITICAL
                      ],
                   ],
           'Subtraction'=>[],
      ],
      'NumberFunctions'=>[
          'GetArrIter'=>[['en'=>'Invalid type passed to function {0}',
                       'ru'=>'В функции {0} передано значение не верного типа',
                       'fr'=>'Type non valide transmis à la fonction {0}',
                       'type'=>Constants::ERROR_WARNING
                      ]]
      ]]


  ];

    //['ns'=>$namespace,'func'=>$key,'num'=>$num]
    public static function AddError($dzetaCalc, $coord, $dopInfo=[])
    {
        if (isset(self::Errors[$coord['ns']][$coord['func']][$coord['num']])) {
            $err=self::Errors[$coord['ns']][$coord['func']][$coord['num']];
            \DZ::lkikf([$coord,$dopInfo]);
            foreach ($dopInfo as $key=> $val) {
                $err=str_replace('{'.$key.'}', $val, $err);
            }
            $dzetaCalc->Errors[$err['type']][]=$err['ru'];

            if ($err['type']==Constants::ERROR_CRITICAL) {
                $dzetaCalc->ExpressionMetka=Constants::EXPRESSION_ERROR;
            }
        }
    }
}
