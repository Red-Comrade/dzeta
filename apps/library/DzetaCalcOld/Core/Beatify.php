<?php

namespace DzetaCalcParser;

class Beatify
{
    public static function GetAnswerAllTree($dzeta)
    {
      $SVArray=$dzeta->GetAllSVarray();
      $Varray=$dzeta->GetAllVarArray();
      $arrStrValue=[];
      \DZ::vArrrr($dzeta);
      foreach ($Varray as $key => $value) {
          if ($key[0]=='$'&&$key!='$this') {
              if (isset($SVArray[$value])) {
                  $arrStrValue[$key]=self::GetTXTValue($SVArray[$value]);
              }
          }
      }
      return $arrStrValue;
    }
    public static function GetAnswerAllText($dzeta)
    {
        $SVArray=$dzeta->GetAllSVarray();
        $Varray=$dzeta->GetAllVarArray();
        $arrStrValue=[];
        foreach ($Varray as $key => $value) {
            if ($key[0]=='$') {
                if (isset($SVArray[$value])) {
                    $arrStrValue[]=$key.'='.self::GetTXTValue($SVArray[$value]);
                }
            }
        }

        echo implode(PHP_EOL, $arrStrValue);
    }
    private static function GetTXTValue($dzValue)
    {
        $txt='';
        if ($dzValue->Type==SystemValue::NUMBER_TYPE) {
            $txt.=$dzValue->Value;
        } elseif ($dzValue->Type==SystemValue::STRING_TYPE) {
            $txt.='"'.$dzValue->Value.'"';
        }elseif ($dzValue->Type==SystemValue::DATETIME_TYPE) {
            $txt.=$dzValue->GetValueToString();
        } elseif ($dzValue->Type==SystemValue::ARRAY_TYPE) {
            $txt.='[';
            $arr=[];
            foreach ($dzValue->Value as $key => $value) {
                $arr[]=$key.'-'.self::GetTXTValue($value);
            }
            $txt.=implode(",", $arr).']';
        }elseif ($dzValue->Type=='null') {
            $txt.='"NULL"';
        } else if ($dzValue->Type == SystemValue::BOOL_TYPE) {
          $txt .= $dzValue->Value ? 'true' : 'false';
        }
        return $txt;
    }

    public static function getFormData($dzValue)
    {
        $formData=$dzValue->FormData;
        $returnData=[];
        foreach ($formData as $key => $valAttr)
        {
            $obj = new \Dzeta\Core\Instance($key, '');
            $attrs = [];
            foreach ($valAttr as $keyAttr=> $value) {
              //  $isArray = is_array($value['value']);
                $attr = new \Dzeta\Core\Type\Attribute($keyAttr, '',2, false, false);
                $attr->setValue(['value'=>$value->Value]);
                $attrs[] = $attr;
            }
          //  $obj->setAttributes($attrs);
            $returnData[$key]=$attrs;
        }
        return $returnData;
    }
}
