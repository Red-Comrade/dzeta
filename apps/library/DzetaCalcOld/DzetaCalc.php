<?php
namespace DzetaCalcParser;

define('DzetaCalcParser', realpath(dirname(__FILE__)));

require_once(DzetaCalcParser.'/Core/BaseFunctions.php');
require_once(DzetaCalcParser.'/Core/Beatify.php');
require_once(DzetaCalcParser.'/Core/Constants.php');
require_once(DzetaCalcParser.'/Core/Errors.php');
require_once(DzetaCalcParser.'/Core/HelperFunctions.php');
require_once(DzetaCalcParser.'/Core/SystemValue.php');
require_once(DzetaCalcParser.'/Core/Stack.php');


require_once(DzetaCalcParser.'/Functions/ArrFunctions.php');
require_once(DzetaCalcParser.'/Functions/APIfunction.php');
require_once(DzetaCalcParser.'/Functions/DateTimeFunctions.php');
require_once(DzetaCalcParser.'/Functions/DBFunctions.php');
require_once(DzetaCalcParser.'/Functions/LogicOperations.php');
require_once(DzetaCalcParser.'/Functions/MathOperations.php');
require_once(DzetaCalcParser.'/Functions/NumberFunctions.php');
require_once(DzetaCalcParser.'/Functions/SpecFunctions.php');
require_once(DzetaCalcParser.'/Functions/StrFunctions.php');
//

class DzetaCalc
{
    /**
     * Original Formula text
     *
     * @var string
     */
    public $frmlMain='';
    /**
   * Original ParseFormula
   *
   * @var string
   */
    public $frmlPars='';
    /**
    * Array variable-sysValue
    *
    * @var Array[]
    */
    public $VAR_ARRAY = array();
    /**
    * Array sysValue
    *
    * @var SystemValue[]
    */
    public $SV_ARRAY = array();

    /**
    * Array tree Formula
    *
    * @var Array[]
    */
    public $mainTreeCalc = array();

    private $DelArr=[];
    protected $Config = null;

    public $Errors=['critical'=>[],'warning'=>[]];

    private $METHODS=[];
    private $FUNCTIONS=[];

    /**
    *FLAG, which marks the expression (break,error,continue)
    */
    public $ExpressionMetka=null;

    /**
    * Number last SV value
    *
    * @var int
    */
    public $lastNum=0;


    public $dbCache=[];

    public $analisys=[];

    public $FormData=[];

    public $testAttrGroups=[];

    public $userUid="";

    public function __construct($Setting)
    {
      $time=microtime(true);
        $this->frmlMain=str_replace("\r",'',$Setting['Formula']);

        $this->initializeDefValue();
        if(!empty($Setting['trigger']))
        {
            $this->AddVariableValue('$this', "object", $Setting['trigger']['obj']->getUid(),
              ['type'=>$Setting['trigger']['obj']->getType()->getUid()]);
               //$res=$resData[$params['obj']];
            //   $dzetaCalc->dbCache['ObjValue'][$params['obj']][$params['attr']]=$res;
              $res=DBFunctions::ConvertDBValueToSysValue($this, [$Setting['trigger']['obj']]);

              foreach ($res as $key => $valObj) {
                foreach ($valObj as  $valu) {
                //  $dzetaCalc->dbCache['ObjValue'][$params['obj']][$params['attr']]
                  if(isset($valu->dopSett['attr']))
                  {
                      $this->dbCache['ObjValue'][$key][$valu->dopSett['attr']]=$valu;
                  }

                }
              }
        }
        if(!empty($Setting['Obj']))
        {
            $this->AddVariableValue('$this', "object", $Setting['Obj']->getUid(),['type'=>$Setting['Obj']->getType()->getUid()]);
            $this->analisys['$this'][]=['class'=>$Setting['Obj']->getType()->getUid(),
                                      'type'=>'object'];
        }
        if(!empty($Setting['cacheDB']))
        {
            //тут у нас будет передаваться кэш базы данных известный
            $res=DBFunctions::ConvertDBValueToSysValue($this,$Setting['cacheDB']);
         foreach ($res as $key => $valObj) {
                foreach ($valObj as  $valu) {
                    if(isset($valu->dopSett['attr']))
                    {
                        $this->dbCache['ObjValue'][$key][$valu->dopSett['attr']]=$valu;
                    }
                }
            }
        }
      // $this->AddVariableValue('$cellValue', "number",5);
        if(!empty($Setting['argFunctionTMP']))
        {
            //тут у нас будет передаваться кэш базы данных известный
             $res=DBFunctions::ConvertDBValueToSysValue($this,[$Setting['argFunctionTMP']['value']]);
             if(empty($res))
             {
                return;
             }
             $res=array_pop($res)[0];
            
             $this->AddVariableValue('$cellValue', $res->Type,$res->Value,$res->dopSett);
            
            
        }   
        if(isset($Setting['UserUid'])) 
        {
            $this->userUid=$Setting['UserUid'];
        }   
        $this->METHODS=array_merge(NumberFunctions::GetMethods(), ArrFunctions::GetMethods(),DBFunctions::GetMethods(),DateTimeFunctions::GetMethods(),SpecFunctions::GetMethods(),StrFunctions::GetMethods());
        $this->FUNCTIONS=$this->GetFunctions();


        HelperFunctions::ReplaceStrValue($this);
        $this->mainTreeCalc=HelperFunctions::createTreeFormula($this, $this->frmlPars);
       // $this->testGrooups();
        if(empty($Setting['noStart']))
        {
            $this->Start($this->mainTreeCalc);
        }
    }

    public static function GetFunctions($onlyNames = true)
    {
        return array_merge(
          NumberFunctions::GetFunctions($onlyNames),
          ArrFunctions::GetFunctions($onlyNames),
          SpecFunctions::GetFunctions($onlyNames),
          DateTimeFunctions::GetFunctions($onlyNames),
          APIfunction::GetFunctions($onlyNames),
          StrFunctions::GetFunctions($onlyNames),
          DBFunctions::GetFunctions($onlyNames)
        );
    }

    public static function GetFunctionsCompletions()
    {
        return [
          NumberFunctions::GetFunctionsCompletions(),
          ArrFunctions::GetFunctionsCompletions(),
          SpecFunctions::GetFunctionsCompletions(),
          DateTimeFunctions::GetFunctionsCompletions(),
          APIfunction::GetFunctionsCompletions(),
          StrFunctions::GetFunctionsCompletions(),
          DBFunctions::GetFunctionsCompletions()
        ];
    }

    public function testGrooups()
    {

       $groupDB=[];
        $curThis=$this->GetValueForVar('$this');
        $curClass=new \Dzeta\Core\Type\Type($curThis->dopSett['type'],'');
        $_attr=new \Dzeta\Models\Attr();
       
        
        foreach ($this->testAttrGroups as $key => $value) {
            if($value['group']==1)
            {
                $attr=new \Dzeta\Core\Type\Attribute('', $value['nameAttr'], 1, false, false);
                $attr->setParent($curClass);
                $res=$_attr->GetIDByName([$attr]);
                if($res['result']==1)
                {
                   $groupDB[]=['Attr'=>$attr,'Group'=>1];
                }
            }           
        } 

        \DZ::gfbv($groupDB);
        return $groupDB;
    }

    /**
    *  Get SystemValue for variable name
    *  @param string $var  variable name
    *
    *  @return SystemValue
    */
    public function GetValueForVar($var)
    {

        if (isset($this->VAR_ARRAY[$var])) {
            return $this->SV_ARRAY[$this->VAR_ARRAY[$var]];
        } else {
            Errors::AddError($this, ['ns'=>'Core','func'=>'GetValueForVar','num'=>0]);
            return $this->GetValueForVar("NULL");
        }
    }

    /**
    *  Get SV for variable name
    *  @param string $var  variable name
    *
    *  @return string
    */
    public function GetSysValueForVar($var)
    {
        if (isset($this->VAR_ARRAY[$var])) {
            return $this->VAR_ARRAY[$var];
        } else {
            Errors::AddError($this, ['ns'=>'Core','func'=>'GetSysValueForVar','num'=>0]);
            return $this->GetSysValueForVar("NULL");
        }
    }

    /**
     *  The function initialize Reserved variable
     *  @return void
     */
    private function initializeDefValue()
    {
        $this->AddVariableValue("false", "bool", false);
        $this->AddVariableValue("true", "bool", true);

        $this->AddVariableValue("NULL", "null", null);
        $this->AddVariableValue("void", "empty", "void");
    }

    /**
     *  The function add system value.
     * @param SystemValue $value
     * @param bool $flagTMP, Indicates whether the value should be deleted after
     * @return string
     */
    public function AddSysVal($value, $flagTMP=true)
    {
        $num=$this->lastNum;
        $this->lastNum++;
        if ($flagTMP) {
            $this->DelArr[]="'$num'";
        }
        $this->SV_ARRAY["'$num'"]=$value;
        return "'$num'";
    }

    /**
     *  The function add variable value
     * @param string $nameVar
     * @param string $type
     * @param string $value
     * @param Array[] $Info
     * @return void
     */
    public function AddVariableValue($nameVar, $type, $value, $Info = array())
    {
        if (isset($this->VAR_ARRAY["$nameVar"])) {
            $this->DelArr[]=$this->VAR_ARRAY["$nameVar"];
        }
        $this->VAR_ARRAY["$nameVar"]=$this->AddSysVal(new SystemValue($value, $type,$Info), false);
    }




    private function SolvePFValaue($pfVal, $pfVIn=null)
    {
        //тут решаем все до простых значений
        if (isset($pfVal['postfixArr'])) {
            $indx=[];
            if (isset($pfVal['var'])) {
                if (!empty($pfVal['actionsVar'])) {
                    foreach ($pfVal['actionsVar'] as $valAct) {
                        $indx[]= $this->SolvePFValaue($valAct);
                    }
                }
            }

            $v=$this->solvePostForm($pfVal['postfixArr'], $pfVIn);
            if (isset($pfVal['var'])) {
                if (!empty($indx)) {
                    $this->setIndexArr($this->SV_ARRAY[$this->VAR_ARRAY[$pfVal['var']]], $indx, $v);
                   
                } else {
                 
                    $this->AddVariableValue($pfVal['var'], $v->Type, $v->Value,$v->dopSett);
                }
            }
            return $v;
        } else {
            if (isset($pfVal['value'])) {
                foreach ($pfVal['actions'] as $valAct) {
                    $pfVal['value']= $this->solvePostForm($valAct, $pfVal['value']);
                }
                return   $this->SV_ARRAY[$pfVal['value']];
            } elseif (isset($pfVal['variable'])) {
                if (isset($this->VAR_ARRAY[$pfVal['variable']])) {
                    if (isset($this->SV_ARRAY[$this->VAR_ARRAY[$pfVal['variable']]])) {
                        $tmpV=$this->SV_ARRAY[$this->VAR_ARRAY[$pfVal['variable']]];
                        if(!empty($pfVal['actions']))
                        {
                           foreach ($pfVal['actions'] as $valAct) {
                                $tmpV= $this->solvePostForm($valAct, $tmpV);
                            } 
                        }
                        return  $tmpV;
                    } else {
                        // ERR

                    }
                } else {


                }
            } elseif (isset($pfVal['func'])) {
                $nameFinc=$pfVal['func'];
                $pParams=[];
                foreach ($pfVal['params'] as $key => $valPfunc) {
                    $pParams[]= $this->SolvePFValaue($valPfunc, $pfVIn);
                }

                if (!is_null($pfVIn)) {
                    if(isset($this->METHODS[$nameFinc]))
                    {
                        $ans = call_user_func_array($this->METHODS[$nameFinc], array($this,$pParams,[$pfVIn]));
                        return $ans;
                    }else {
                      Errors::AddError($this, ['ns'=>'Global','func'=>'Func','num'=>1], [$nameFinc]);
                      return $this->GetValueForVar("NULL");
                    }

                } else {
                   
                    if(isset($this->FUNCTIONS[$nameFinc]))
                    {
                      $ans = call_user_func_array($this->FUNCTIONS[$nameFinc], array($this,$pParams,null));
                      return $ans;
                    }else {

                      Errors::AddError($this, ['ns'=>'Global','func'=>'Func','num'=>2], [$nameFinc]);
                      return $this->GetValueForVar("NULL");
                    }

                }
            }
        }
    }

    private function setIndexArr(&$arr, $arrIndex, $val)
    {
        $ind=array_shift($arrIndex);
        if (empty($arrIndex)) {
            $arr->Value[$ind->Value]=$val;
        } else {
            $this->setIndexArr($arr->Value[$ind->Value], $arrIndex, $val);
        }
    }

    private function solvePostForm($pf, $pfVIn=null)
    {
        if (!is_null($this->ExpressionMetka)) {
            return $this->GetValueForVar("NULL");
        }

        $temp = [];
        $hold = 0;
        $arrM=array_merge(Constants::OPERATIONS_HIGH, Constants::OPERATIONS_LOW, Constants::OPERATIONS_COMPARE, Constants::OPERATIONS_LOGIC,Constants::ST);

        for ($i = 0,$lenPF=count($pf); $i < $lenPF; $i++) {
            if (!in_array($pf[$i], $arrM)) {
                $temp[$hold++] =$this->SolvePFValaue($pf[$i], $pfVIn);
            } else {
                if (!isset($temp[$hold - 2])&&!isset($temp[$hold - 1])) {
                    return $this->GetSysValueForVar("NULL");
                }
               else {

                    switch ($pf[$i]) {
                     case '+':
                        $temp[$hold - 2] = MathOperations::Additional($temp[$hold - 2], $temp[$hold - 1], $this);
                        break;
                     case '-':
                        $temp[$hold - 2] = MathOperations::Subtraction($temp[$hold - 2], $temp[$hold - 1], $this);
                        break;
                     case '*':
                        $temp[$hold - 2] = MathOperations::Multiplication($temp[$hold - 2], $temp[$hold - 1], $this);
                        break;
                     case '/':
                        $temp[$hold - 2] = MathOperations::Division($temp[$hold - 2], $temp[$hold - 1], $this);
                        break;
                      case '%':
                         $temp[$hold - 2] = MathOperations::Mod($temp[$hold - 2], $temp[$hold - 1], $this);
                         break;
                     case '<':
                        $temp[$hold - 2] = LogicOperations::Less($temp[$hold - 2], $temp[$hold - 1], $this);
                        break;
                     case '>':
                        $temp[$hold - 2] = LogicOperations::Great($temp[$hold - 2], $temp[$hold - 1], $this);
                        break;
                    case '?-':
                    //это когда выражение вида -4
                         $temp[$hold - 1] = MathOperations::Subtraction($temp[$hold - 1], null, $this);
                         $hold++;
                         break;
                    case '!!':                   
                         $temp[$hold - 1] = LogicOperations::Equal($temp[$hold - 2], $temp[$hold - 1], $this);

                         $hold++;
                         break;
                     case '@@':                   
                         $temp[$hold - 1] = LogicOperations::nEqual($temp[$hold - 2], $temp[$hold - 1], $this);

                         $hold++;
                         break;
                    
                      }
                    $hold = $hold - 1;
                }
            }
        }

        if (isset($temp[$hold - 1])) {
            return $temp[$hold - 1];
        }

        return $this->GetValueForVar("false");
    }



    /**
     *  The function compute the calc-tree
     * @param Array[] $treeFormula
     * @return void
     */
    private function Start($treeFormula)
    {
        for ($i=0,$countFormula=count($treeFormula); $i < $countFormula; $i++) {
            if (count($this->DelArr)>10) {

                foreach ($this->DelArr as  $valueDel) {
                 unset($this->SV_ARRAY[$valueDel]);
                }
                $this->DelArr=[];
            }

            if ($this->ExpressionMetka==Constants::BREAK_FLAG||
            $this->ExpressionMetka==Constants::CONTINUE_FLAG||
            $this->ExpressionMetka==Constants::EXPRESSION_ERROR) {
                break;
            }

            $frmla = $treeFormula[$i];
            if ($frmla['type'] == "simple") {
                $this->SolvePFValaue($frmla);
            } elseif ($frmla['type'] == "for") {
                $declr = $frmla['declaration'];
                if (!empty($declr)) {
                    if (count($declr) == 1) {
                        if ($declr[0]['type'] == "simple") {
                            $this->SolvePFValaue($declr[0]);
                        } else {
                            Errors::AddError($Dzeta, ['ns'=>'for','func'=>'declaration','num'=>0]);
                            break;
                        }
                    } else {
                        Errors::AddError($Dzeta, ['ns'=>'for','func'=>'declaration','num'=>0]);
                        break;
                    }
                }
                while (1) {
                    $cond = $frmla['conditional'];
                    if (!empty($cond)) {
                        if (count($cond) == 1) {
                            if ($cond[0]['type'] == "simple") {
                                $cond = $this->SolvePFValaue($cond[0]);
                                $cond=$cond->Value;
                            } else {
                                Errors::AddError($Dzeta, ['ns'=>'for','func'=>'cond','num'=>0]);
                                break;
                            }
                        } else {
                            Errors::AddError($Dzeta, ['ns'=>'for','func'=>'cond','num'=>0]);
                            break;
                        }
                    } else {
                        $cond = false;
                    }
                    if ($cond) {
                        $this->Start($frmla['body']);
                        if ($this->ExpressionMetka==Constants::BREAK_FLAG) {
                            $this->ExpressionMetka=null;
                            break;
                        }
                        if ($this->ExpressionMetka==Constants::CONTINUE_FLAG) {
                            $this->ExpressionMetka=null;
                        }
                        $this->Start($frmla['step']);
                    } else {
                        break;
                    }
                }
            } elseif ($frmla['type'] == "if") {
                $cond = $frmla['condition'];
                if (!empty($cond)) {
                    if (count($cond) == 1) {
                        if ($cond[0]['type'] == "simple") {
                            $cond = $this->SolvePFValaue($cond[0]);
                            $cond=$cond->Value;
                        } else {
                        }
                    } else {
                    }
                } else {
                    $cond = false;
                }
                if ($cond) {
                    $this->Start($frmla['expression']);
                } else {
                    for ($indexIf=0,$lengthIF = count($frmla['dopif']);
                 $indexIf < $lengthIF; $indexIf++) {
                        $frml = $frmla['dopif'][$indexIf];
                        if ($frml['type'] == "elseif") {
                            $cond = $frml['condition'];
                            if (!empty($cond)) {
                                if (count($cond) == 1) {
                                    if ($cond[0]['type'] == "simple") {
                                        $cond = $this->SolvePFValaue($cond[0]);
                                        $cond=$cond->Value;
                                    } else {
                                    }
                                } else {
                                }
                            } else {
                                $cond = false;
                            }
                        } elseif ($frml['type'] == "else") {
                            $cond = true;
                        }
                        if ($cond) {
                            $this->Start($frml['expression']);
                            break;
                        }
                    }
                }
            }
        }
    }

    public function GetAllSVarray()
    {
      return $this->SV_ARRAY;
    }
    public function GetAllVarArray()
    {
      return $this->VAR_ARRAY;
    }

    public function GetAnswer()
    {
        $copy=$this->VAR_ARRAY;
         $tmp=array_keys($copy);
        $lastVarValue=array_pop($copy);

        $lastVarName=array_pop($tmp);
       return ['var'=>$lastVarName,
       'val'=>$this->SV_ARRAY[$lastVarValue]->Value,
       'type'=>$this->SV_ARRAY[$lastVarValue]->Type];
      
    }

}
