<?php

namespace DzetaCalcParser;

class ApiFunctions extends BaseFunctions
{
    protected static function DoGetMethods()
    {
        $ret = [];
        return $ret;
    }

    protected static function DoGetFunctions()
    {
        $ret = [
            'apiDatasocieteinfocom' => [
                'name' => 'DatasocieteinfocomF',
                'completion' => [
                    'label' => 'apiDatasocieteinfocom(string siren|siret)',
                    'kind' => 'function',
                    'insertText' => 'apiDatasocieteinfocom(${1:siren|siret})',
                ]
            ],
            'apiDatainfogreffe' => [
                'name' => 'Datainfogreffe',
                'completion' => [
                    'label' => 'apiDatainfogreffe(string siren|siret)',
                    'kind' => 'function',
                    'insertText' => 'apiDatainfogreffe(${1:siren|siret})',
                ]
            ],
        ];

        return $ret;
    }
    public static function Datainfogreffe($dzetaCalc, $paramsFunc, $paramProc)
    {
      if (is_null($paramProc)) {
        $loader = new \Phalcon\Loader();
        $loader->registerClasses(
            [
              'Datainfogreffe' => APP_PATH . '/library/projFR/'.'Datainfogreffe.php'
            ]
        );
        $loader->register();
        if(count($paramsFunc)!=1)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>1], [self::GetNameMethods(__METHOD__)]);
            return $dzetaCalc->GetValueForVar("null");
        }

        if($paramsFunc[0]->Type!=SystemValue::STRING_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }
        $code = preg_replace("/[^0-9]/", '', $paramsFunc[0]->Value);
        $len=strlen($code);
        $resArr=[];
        if($len==9||$len==14)
        {
            $code=substr($code,0,9);
            $tmp=\Datainfogreffe::GetInfo($code);
            $jsn=json_decode($tmp,true);

            if(isset($jsn['Data']))
            {
            /*  $resArr['Adresse']=$jsn['Data']['Adresse']['AdresseConcat'];
              $resArr['Code postale']=$jsn['Data']['Adresse']['CodePostal'];
              $resArr['Ville']=$jsn['Data']['Adresse']['BureauDistributeur'];
              $resArr['Raison Socliale']=$jsn['Data']['Denomination'];*/
                $resArr['Adresse']=new SystemValue($jsn['Data']['Adresse']['AdresseConcat'], SystemValue::STRING_TYPE);
                $resArr['Code postale']=new SystemValue($jsn['Data']['Adresse']['CodePostal'], SystemValue::STRING_TYPE);
                $resArr['Ville']=new SystemValue($jsn['Data']['Adresse']['BureauDistributeur'], SystemValue::STRING_TYPE);
                $resArr['Raison Socliale']=new SystemValue($jsn['Data']['Denomination'], SystemValue::STRING_TYPE);
            /*    $resArr['Capital social MO']=new SystemValue($jsn['result']['organization']['capital'], SystemValue::STRING_TYPE);
                $resArr['PRENOM MO']=new SystemValue($jsn['result']['contacts']['main_corporate_officier']['firstName'], SystemValue::STRING_TYPE);
                $resArr['NOM MO']=new SystemValue($jsn['result']['contacts']['main_corporate_officier']['lastName'], SystemValue::STRING_TYPE);
                $resArr['FONCTION MO']=new SystemValue($jsn['result']['contacts']['main_corporate_officier']['role'], SystemValue::STRING_TYPE);*/
            }
            /*$resArr['Adresse']=new SystemValue('test1', SystemValue::STRING_TYPE);
            $resArr['Code postale']=new SystemValue('test2', SystemValue::STRING_TYPE);
            $resArr['Ville']=new SystemValue('test3', SystemValue::STRING_TYPE);
            $resArr['Raison Socliale']=new SystemValue('test4', SystemValue::STRING_TYPE);
            $resArr['Capital social MO']=new SystemValue('test5', SystemValue::STRING_TYPE);
            $resArr['PRENOM MO']=new SystemValue('test6', SystemValue::STRING_TYPE);
            $resArr['NOM MO']=new SystemValue('test7', SystemValue::STRING_TYPE);
            $resArr['FONCTION MO']=new SystemValue('test8', SystemValue::STRING_TYPE);*/
        }



        return new SystemValue($resArr, SystemValue::ARRAY_TYPE);
      }else {
          return $dzetaCalc->GetValueForVar("null");

      }
    }
    public static function DatasocieteinfocomF($dzetaCalc, $paramsFunc, $paramProc)
    {
      if (is_null($paramProc)) {
        $loader = new \Phalcon\Loader();
        $loader->registerClasses(
            [
              'Datasocieteinfocom'=> APP_PATH . '/library/projFR/'.'Datasocieteinfocom.php'
            ]
        );
        $loader->register();
        if(count($paramsFunc)!=1)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>1], [self::GetNameMethods(__METHOD__)]);
            return $dzetaCalc->GetValueForVar("null");
        }

        if($paramsFunc[0]->Type!=SystemValue::STRING_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }
        $code = preg_replace("/[^0-9]/", '', $paramsFunc[0]->Value);
        $len=strlen($code);
          $resArr=[];
        if($len==9||$len==14)
        {
            $tmp=\Datasocieteinfocom::GetInfo($code);
            $jsn=json_decode($tmp,true);
            if(isset($jsn['result']))
            {
                $resArr['Adresse']=new SystemValue($jsn['result']['organization']['address']['street'], SystemValue::STRING_TYPE);
                $resArr['Code postale']=new SystemValue($jsn['result']['organization']['address']['postal_code'], SystemValue::STRING_TYPE);
                $resArr['Ville']=new SystemValue($jsn['result']['organization']['address']['city'], SystemValue::STRING_TYPE);
                $resArr['Raison Socliale']=new SystemValue($jsn['result']['organization']['name'], SystemValue::STRING_TYPE);
                $resArr['Capital social MO']=new SystemValue($jsn['result']['organization']['capital'], SystemValue::STRING_TYPE);
                $resArr['PRENOM MO']=new SystemValue($jsn['result']['contacts']['main_corporate_officier']['firstName'], SystemValue::STRING_TYPE);
                $resArr['NOM MO']=new SystemValue($jsn['result']['contacts']['main_corporate_officier']['lastName'], SystemValue::STRING_TYPE);
                $resArr['FONCTION MO']=new SystemValue($jsn['result']['contacts']['main_corporate_officier']['role'], SystemValue::STRING_TYPE);
            }
            /*$resArr['Adresse']=new SystemValue('test1', SystemValue::STRING_TYPE);
            $resArr['Code postale']=new SystemValue('test2', SystemValue::STRING_TYPE);
            $resArr['Ville']=new SystemValue('test3', SystemValue::STRING_TYPE);
            $resArr['Raison Socliale']=new SystemValue('test4', SystemValue::STRING_TYPE);
            $resArr['Capital social MO']=new SystemValue('test5', SystemValue::STRING_TYPE);
            $resArr['PRENOM MO']=new SystemValue('test6', SystemValue::STRING_TYPE);
            $resArr['NOM MO']=new SystemValue('test7', SystemValue::STRING_TYPE);
            $resArr['FONCTION MO']=new SystemValue('test8', SystemValue::STRING_TYPE);*/
        }



        return new SystemValue($resArr, SystemValue::ARRAY_TYPE);
      }else {
          return $dzetaCalc->GetValueForVar("null");

      }
    }
  }
