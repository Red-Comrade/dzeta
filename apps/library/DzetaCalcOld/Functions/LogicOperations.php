<?php

namespace DzetaCalcParser;

class LogicOperations
{
  public static function Less($val1,$val2,$Dzeta)
  {
	
    if($val1->Value<$val2->Value)
    {
      return $Dzeta->GetValueForVar("true");
    }else {
      return $Dzeta->GetValueForVar("false");
    }
  }

  public static function Great($val1,$val2,$Dzeta)
  {    
    if($val1->Value>$val2->Value)
    {
      return $Dzeta->GetValueForVar("true");
    }else {
      return $Dzeta->GetValueForVar("false");
    }
  }

   public static function Equal($val1,$val2,$Dzeta)
  {    

    if($val1->Value==$val2->Value)
    {
      return $Dzeta->GetValueForVar("true");
    }else {
      return $Dzeta->GetValueForVar("false");
    }
  }
   public static function nEqual($val1,$val2,$Dzeta)
  {    

    if($val1->Value==$val2->Value)
    {
      return $Dzeta->GetValueForVar("false");
    }else {
      return $Dzeta->GetValueForVar("true");
    }
  }
}
?>
