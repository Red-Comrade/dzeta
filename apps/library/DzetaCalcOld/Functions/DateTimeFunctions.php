<?php

namespace DzetaCalcParser;

class DateTimeFunctions extends BaseFunctions
{
    protected static function DoGetMethods()
    {
        $ret = [
          'setYear' => [
            'name' => 'setYearF',
          ],
        ];

        return $ret;
    }

    protected static function DoGetFunctions()
    {
        $ret = [
          'dateTime' => [
            'name' => 'dateTimeF',
            'completion' => [
                'label' => 'dateTime(number day, number month, number year, number hour, number minute, number second)',
                'kind' => 'function',
                'insertText' => 'dateTime(${1:day}, ${2:month}, ${3:year}, ${4:hour}, ${5:minute}, ${6:second})',
            ]
          ],
        ];

        return $ret;
    }

    public static function setYearF($dzetaCalc, $paramsFunc, $paramProc)
    {
      if (is_null($paramProc)) {

      }else {
        //return new SystemValue($paramProc[0]->GetValueToString(), SystemValue::STRING_TYPE);

          if($paramProc[0]->Type!=SystemValue::DATETIME_TYPE)
          {
              Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
              return $dzetaCalc->GetValueForVar("null");
          }

          if(count($paramsFunc)!=1)
          {
              Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>1], [self::GetNameMethods(__METHOD__)]);
              return $dzetaCalc->GetValueForVar("null");
          }
          if($paramsFunc[0]->Type!=SystemValue::NUMBER_TYPE)
          {
            //  Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
              return $dzetaCalc->GetValueForVar("null");
          }

          \DZ::j7y($paramProc);
          return $dzetaCalc->GetValueForVar("null");

      }
    }

    public static function dateTimeF($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (is_null($paramProc)) {
            if (count($paramsFunc)>6||!(in_array(count($paramsFunc), [0,3,6]))) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("null");
            }
            $date=new \DateTime();
            $year=1995;
            $month=07;
            $day=18;
            $hour=0;
            $minute=0;
            $second=0;
            if (count($paramsFunc)==0) {
            } else {
                if ($paramsFunc[0]->Type!=SystemValue::NUMBER_TYPE) {
                    Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),1]);
                    return $dzetaCalc->GetValueForVar("NULL");
                }
                $day=$paramsFunc[0]->Value;

                if ($paramsFunc[1]->Type!=SystemValue::NUMBER_TYPE) {
                    Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),2]);
                    return $dzetaCalc->GetValueForVar("NULL");
                }
                $month=$paramsFunc[1]->Value;

                if ($paramsFunc[2]->Type!=SystemValue::NUMBER_TYPE) {
                    Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),3]);
                    return $dzetaCalc->GetValueForVar("NULL");
                }
                $year=$paramsFunc[2]->Value;

                if (count($paramsFunc)==6) {
                    if ($paramsFunc[3]->Type!=SystemValue::NUMBER_TYPE) {
                        Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),4]);
                        return $dzetaCalc->GetValueForVar("NULL");
                    }
                    $hour=$paramsFunc[3]->Value;

                    if ($paramsFunc[4]->Type!=SystemValue::NUMBER_TYPE) {
                        Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),5]);
                        return $dzetaCalc->GetValueForVar("NULL");
                    }
                    $minute=$paramsFunc[4]->Value;

                    if ($paramsFunc[5]->Type!=SystemValue::NUMBER_TYPE) {
                        Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),6]);
                        return $dzetaCalc->GetValueForVar("NULL");
                    }
                    $second=$paramsFunc[5]->Value;
                }
                $date->setDate($year, $month, $day);
                $date->setTime($hour, $minute, $second);
            }
            return new SystemValue($date, SystemValue::DATETIME_TYPE);
        } else {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Func','num'=>0], [self::GetNameFunctions(__METHOD__)]);
            return $dzetaCalc->GetValueForVar("null");
        }
    }
}
