<?php

namespace DzetaCalcParser;

class NumberFunctions extends BaseFunctions
{
    protected static function DoGetMethods()
    {
        $ret = [];
        return $ret;
    }

    protected static function DoGetFunctions()
    {
        $ret = [
            'abs' => [
                'name' => '_abs',
                'completion' => [
                    'label' => 'abs(number value)',
                    'kind' => 'function',
                    'insertText' => 'abs(${1:value})',
                ]
            ],
            'ceil' => [
                'name' => '_ceil',
                'completion' => [
                    'label' => 'ceil(number value)',
                    'kind' => 'function',
                    'insertText' => 'ceil(${1:value})',
                ]
            ],
            'floor' => [
                'name' => '_floor',
                'completion' => [
                    'label' => 'floor(number value)',
                    'kind' => 'function',
                    'insertText' => 'floor(${1:value})',
                ]
            ],
            'round' => [
                'name' => '_round',
                'completion' => [
                    'label' => 'round(number value)',
                    'kind' => 'function',
                    'insertText' => 'round(${1:value})',
                ]
            ],
        ];

        return $ret;
    }

    public static function _round($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {
            //как функция выполняется
            if (!(count($paramsFunc)==2||count($paramsFunc)==1)) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $v=$paramsFunc[0];
            $precDefault=0;
            if(isset($paramsFunc[1]))
            {
              $prec=$paramsFunc[1];
              if($prec->Type!=SystemValue::NUMBER_TYPE)
              {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
              }
              $precDefault=$prec->Value;
            }
            if ($v->Type==SystemValue::NUMBER_TYPE) {
                return new SystemValue(round($v->Value,$precDefault), SystemValue::NUMBER_TYPE);
            } elseif ($v->Type==SystemValue::ARRAY_TYPE) {
                $newA=self::GetArrIter($v, $dzetaCalc, ['name'=>'round','precision'=>$precDefault]);
                return new SystemValue($newA, SystemValue::ARRAY_TYPE);
            }
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }

    public static function _floor($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {
            //как функция выполняется
            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $v=$paramsFunc[0];

            if ($v->Type==SystemValue::NUMBER_TYPE) {
                return new SystemValue(floor($v->Value), SystemValue::NUMBER_TYPE);
            } elseif ($v->Type==SystemValue::ARRAY_TYPE) {
                $newA=self::GetArrIter($v, $dzetaCalc, ['name'=>'floor']);
                return new SystemValue($newA, SystemValue::ARRAY_TYPE);
            }
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }

    public static function _ceil($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {
            //как функция выполняется
            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $v=$paramsFunc[0];

            if ($v->Type==SystemValue::NUMBER_TYPE) {
                return new SystemValue(ceil($v->Value), SystemValue::NUMBER_TYPE);
            } elseif ($v->Type==SystemValue::ARRAY_TYPE) {
                $newA=self::GetArrIter($v, $dzetaCalc, ['name'=>'ceil']);
                return new SystemValue($newA, SystemValue::ARRAY_TYPE);
            }
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }
    public static function _abs($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {
            //как функция выполняется
            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $v=$paramsFunc[0];

            if ($v->Type==SystemValue::NUMBER_TYPE) {
                return new SystemValue(abs($v->Value), SystemValue::NUMBER_TYPE);
            } elseif ($v->Type==SystemValue::ARRAY_TYPE) {
                $newA=self::GetArrIter($v, $dzetaCalc,['name'=>'abs'] );
                return new SystemValue($newA, SystemValue::ARRAY_TYPE);
            }
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }




    private static function GetArrIter($ElemArr, $dzetaCalc, $func=[])
    {
        $tmp=[];
        foreach ($ElemArr->Value as $valAr) {
            if ($valAr->Type==SystemValue::ARRAY_TYPE) {
                $tmp[]=new SystemValue(self::GetArrIter($valAr, $dzetaCalc, $func), SystemValue::ARRAY_TYPE);
            } elseif ($valAr->Type==SystemValue::NUMBER_TYPE) {
                if(isset($func['name']))
                {
                  switch ($func['name']) {
                    case 'abs':
                      $tmp[]=new SystemValue(abs($valAr->Value), SystemValue::NUMBER_TYPE);
                      break;
                    case 'ceil':
                      $tmp[]=new SystemValue(ceil($valAr->Value), SystemValue::NUMBER_TYPE);
                      break;
                    case 'floor':
                      $tmp[]=new SystemValue(floor($valAr->Value), SystemValue::NUMBER_TYPE);
                      break;
                    case 'round':
                      $tmp[]=new SystemValue(round($valAr->Value,$func['precision']), SystemValue::NUMBER_TYPE);
                      break;
                    default:
                      $tmp[]=new SystemValue($valAr->Value, $valAr->Type);
                      break;

                  }
                }

            } else {
                Errors::AddError($dzetaCalc, ['ns'=>'NumberFunctions','func'=>'GetArrIter','num'=>0], [$func['name']]);
                $tmp[]=$dzetaCalc->GetValueForVar("NULL");
            }
        }
        return $tmp;
    }
}
