<?php

namespace DzetaCalcParser;

class SpecialFunctions extends BaseFunctions
{
  protected static function DoGetMethods()
  {
      $ret = [
          'toString' => [
              'name' => 'toStringF',
          ],
          'parseDB' => [
              'name' => 'parseDBF',
          ],
      ];

      return $ret;
  }

  protected static function DoGetFunctions()
  {
      $ret = [
          'break' => [
              'name' => 'breakF',
          ],
          'continue' => [
              'name' => 'continueF',
          ],
          'toString' => [
              'name' => 'toStringF',
              'completion' => [
                  'label' => 'toString(mixed value)',
                  'kind' => 'function',
                  'insertText' => 'toString(${1:value})',
              ]
          ],
          'sendMail' => [
              'name' => 'sendMailF',
              'completion' => [
                  'label' => 'sendMail(string sendTo, string subject, string text, string from)',
                  'kind' => 'function',
                  'insertText' => 'sendMail(${1:sendTo}, ${2:subject}, ${3:text}, ${4:from})',
              ]
          ],
          'test_setGroup1' => [
              'name' => 'test_setGroup1F',
          ],
          'test_createObj' => [
              'name' => 'test_createObjF',
          ],
          'sendJsonDataBigle' => [
              'name' => 'sendJsonDataBigle',
          ],
      ];

      return $ret;
  }

  public static function test_createObjF($dzetaCalc, $paramsFunc, $paramProc)
  {
    if(is_null($paramProc))
    {
      if($paramsFunc[0]->Type!=SystemValue::STRING_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }
        $_obj=new \Obj();
         $obj = new \Dzeta\Core\Instance($paramsFunc[0]->Value, '');
        $type=$_obj->GetClassForObject($obj);
       $tmp=new SystemValue($paramsFunc[0]->Value,SystemValue::OBJECT_TYPE,
        ['type'=>$obj->getType()->getUid()]);
      
       return $tmp;

    }
  }
  public static function sendJsonDataBigle($dzetaCalc, $paramsFunc, $paramProc)
  {
    if(is_null($paramProc))
    {
        if($paramsFunc[0]->Type!=SystemValue::STRING_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }
        $type=$paramsFunc[0]->Value;

        if($paramsFunc[1]->Type!=SystemValue::STRING_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }
        $id=$paramsFunc[1]->Value;

        if($paramsFunc[2]->Type!=SystemValue::STRING_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }
        $action=$paramsFunc[2]->Value;

        if($paramsFunc[3]->Type!=SystemValue::ARRAY_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }
        $field=$paramsFunc[3]->Value;
        $fieldData=[];
        foreach ($field as $key => $value) {
          if($value->Type==SystemValue::ARRAY_TYPE)
          {
            foreach ($value->Value as $val) {
              $fieldData[$key][]=$val->Value;
            }
          }else{
             $fieldData[$key]=$value->Value;
          }
         
        }

        $returnData=[];
      
           $returnData['entity']=$type;
           $returnData['id']= $id;
           $returnData['action']= $action;
           $returnData['fields']= $fieldData;
           $tttt=http_build_query(['data'=>json_encode($returnData)]);
           $pathFile=BASE_PATH.'/config/dop/bigle/bigle.ini';
          $sett=parse_ini_file($pathFile,true);
          if(isset($sett['biglejson']['url']))
          {
           $url=$sett['biglejson']['url'].'&'.$tttt;
       
           $res=file_get_contents($url);
           $res=json_encode($res,true);         
           if(!empty( $res['result']))
           {
           
             return new SystemValue($url, SystemValue::STRING_TYPE);;
           }
          }
        
         return new SystemValue($url, SystemValue::STRING_TYPE);;

    }else{
      return $dzetaCalc->GetValueForVar("NULL");
    }
  }
  public static function test_setGroup1F($dzetaCalc, $paramsFunc, $paramProc){
    
  }

  public static function parseDBF($dzetaCalc, $paramsFunc, $paramProc){
    /*парсим строку и вытягиваем наши спец метки, чтобы потом все сделать*/
    if(!is_null($paramProc))
    {
        //
        if($paramProc[0]->Type!=SystemValue::STRING_TYPE)
        {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
            return $dzetaCalc->GetValueForVar("null");
        }

        $str=$paramProc[0]->Value;
        $attrCur=[];
        $attrTree=[];
        for($i=0,$len=mb_strlen($str);$i<$len;$i++)
        {
            $chr=mb_substr($str,$i,1);
            if($chr=='{')
            {
               $flagB1=$i;
            }elseif($chr=='}')
            {
              $lennnStr=mb_substr($str,$flagB1+1,$i-$flagB1-1);
              for($j=0;$j<mb_strlen($lennnStr);$j++)
              {
                  $chr=mb_substr($lennnStr,$j,1);
                  if($chr=='.')
                  {
                      $typeObj=mb_substr($lennnStr,0,$j);
                      $typeName='';
                      $objName='';
                      for($j1=0;$j1<mb_strlen($typeObj);$j1++)
                      {
                        $chr=mb_substr($typeObj,$j1,1);
                        if($chr=='(')
                        {
                          $typeName= mb_substr($typeObj,0,$j1);
                          $objName=mb_substr($typeObj,$j1+1,mb_strlen($typeObj)-$j1-2);                            
                        }
                      }
                      $attr=mb_substr($lennnStr,$j+1);
                      $attrTree[]=['type'=>$typeName,'objname'=> $objName,'attr'=>$attr];
                  }
              }
            }elseif($chr=='[')
            {
              $flagB2=$i;
            }elseif($chr==']')
            {
              $attrCur[]=['attr'=>mb_substr($str,$flagB2+1,$i-$flagB2-1)];
            }
        }
        $replArr=[];
        $_obj=new \Obj();
        $_attr=new \Attr();
        foreach ($attrTree as $key => $value)
        {          
            $class=new \Dzeta\Core\Type\Type('',$value['type']);
            $_class=new \Type();
            $res=$_class->GetIDByName($class);
            if($res==1)
            {                
                $attr=new \Dzeta\Core\Type\Attribute('', $value['attr'], 1, false, false);
                $attr->setParent($class);
                $res=$_attr->GetIDByName([$attr]);                
                $objss=$_obj->getObjsByClass($class, $value['objname']);
                if($objss['result']==1&&!empty($objss['objs']))
                {
                    $ob=$objss['objs'][0];
                    $ob->setAttributes([$attr]);
                    $_obj->getAllValues([$ob]);
                    $vall=DBFunctions::ConvertDBValueToSysValue($dzetaCalc,[$ob]);
                    if(isset($vall[$ob->getUid()]))
                    {
                        $replVal=$vall[$ob->getUid()][0]->Value;
                        $replArr[]=['replace'=>'{'.$value['type'].'('.$value['objname'].')'.'.'.$value['attr'].'}','withReplace'=>$replVal];
                    }                   
                }
            }
        }
        $curThis=$dzetaCalc->GetValueForVar('$this');
        $curClass=new \Dzeta\Core\Type\Type($curThis->dopSett['type'],'');
        $objThis=new \Dzeta\Core\Instance($curThis->Value, '', $curClass);
        $attrs=[];

        foreach ($attrCur as $key => $value)
        {
           $attr=new \Dzeta\Core\Type\Attribute('', $value['attr'], 1, false, false);
           $attr->setParent($curClass);
           $res=$_attr->GetIDByName([$attr]);   

           if($res['result']==1)
           {             
              $attrs[]=$attr;
              $attrCur[$key]['attruid']=$attr->getUid();
           }
          
        }

        $objThis->setAttributes($attrs);
        $_obj->getAllValues([$objThis]);
        $vall=DBFunctions::ConvertDBValueToSysValue($dzetaCalc,[$objThis]);
        $arrUid=array_column($attrCur, 'attruid');  

        foreach ($vall[$objThis->getUid()] as $key => $value)
        {           
            if(isset($value->dopSett['attr']))
            {
              $kk=array_search($value->dopSett['attr'], $arrUid);
            if($kk!==false)
            {
              $replArr[]=['replace'=>'['.$attrCur[$kk]['attr'].']','withReplace'=>$value->Value];
            }
            }
            
        }
        foreach ($replArr as $key => $value) {
           $str=str_replace( $value['replace'],  $value['withReplace'], $str);
        }
        
        return new SystemValue( $str, SystemValue::STRING_TYPE);
    }
  }

  public static function sendMailF($dzetaCalc, $paramsFunc, $paramProc)
  {
     if (is_null($paramProc)) {
          //тут из метода, и первый параметр это наше
            $loader = new \Phalcon\Loader();
            $loader->registerClasses(
                [
                  'ElasticEmail' => APP_PATH . '/library/ElasticEmail.php'
                ]
            );
            $loader->register();
            $sendTo='';
            if($paramsFunc[0]->Type!=SystemValue::STRING_TYPE)
            {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("null");
            }
            $sendTo=$paramsFunc[0]->Value;

            $subject='';
            if($paramsFunc[1]->Type!=SystemValue::STRING_TYPE)
            {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("null");
            }
            $subject=$paramsFunc[1]->Value;

            $text='';
            if($paramsFunc[2]->Type!=SystemValue::STRING_TYPE)
            {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'Method','num'=>0], [self::GetNameMethods(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("null");
            }
            $text=$paramsFunc[2]->Value;
            $from=null;
            if(isset($paramsFunc[3]))
            {
              if($paramsFunc[3]->Type==SystemValue::STRING_TYPE)
              {
                 $from=$paramsFunc[3]->Value;
              }
            }

            $mail=\ElasticEmail::sendMail($subject,$sendTo,$text,'',$from);
           // var_dump($mail);
            if($mail)
            {
               return new SystemValue($mail, SystemValue::STRING_TYPE);
            }else{
               return $dzetaCalc->GetValueForVar("false");
            }

         
      } else {

          return $dzetaCalc->GetValueForVar("NULL");
      }
  }


  public static function toStringF($dzetaCalc, $paramsFunc, $paramProc)
  {
      if (is_null($paramProc)) {
          //тут из метода, и первый параметр это наше
           return new SystemValue($paramsFunc[0]->GetValueToString(), SystemValue::STRING_TYPE);
      } else {

           return new SystemValue($paramProc[0]->GetValueToString(), SystemValue::STRING_TYPE);
      }
  }

  public static function breakF($dzetaCalc,$paramsFunc,$paramProc)
  {
      if(!is_null($paramProc))
      {
          return $dzetaCalc->GetValueForVar("NULL");
      }else {
          $dzetaCalc->ExpressionMetka=Constants::BREAK_FLAG;
      }
      return $dzetaCalc->GetValueForVar("NULL");
  }

  public static function continueF($dzetaCalc,$paramsFunc,$paramProc)
  {
      if(!is_null($paramProc))
      {
          return $dzetaCalc->GetValueForVar("NULL");
      }else {
          $dzetaCalc->ExpressionMetka=Constants::CONTINUE_FLAG;
      }
      return $dzetaCalc->GetValueForVar("NULL");
  }
}
