<?php

namespace DzetaCalcParser;

class ArrayFunctions extends BaseFunctions
{
    protected static function DoGetMethods()
    {
        $ret = [
            'elemArr' => [
                'name' => 'elemArrM'
            ],
        ];

        return $ret;
    }

    protected static function DoGetFunctions()
    {
        $ret = [
            'array' => [
                'name' => 'arrayF',
                'completion' => [
                    'label' => 'array(mixed ...values)',
                    'kind' => 'function',
                    'insertText' => 'array(${1:...values})',
                ]
            ],
            'elemArrIndex' => [
                'name' => 'elemArrIndexM',
                'completion' => [
                    'label' => 'elemArrIndex(mixed value)',
                    'kind' => 'function',
                    'insertText' => 'elemArrIndex(${1:value})',
                ]
            ],
            'count' => [
                'name' => 'countF',
                'completion' => [
                    'label' => 'count(array value)',
                    'kind' => 'function',
                    'insertText' => 'count(${1:value})'
,                ]
            ],
            'json_decode' => [
                'name' => 'jsondecodeF',
                'completion' => [
                    'label' => 'json_decode(string json)',
                    'kind' => 'function',
                    'insertText' => 'json_decode(${1:json})',
                ]
            ],
            'json_encode' => [
                'name' => 'jsonencodeF',
                'completion' => [
                    'label' => 'json_encode(mixed value)',
                    'kind' => 'function',
                    'insertText' => 'json_encode(${1:value})',
                ]
            ],
            'inArray' => [
                'name' => 'inArrayF',
                'completion' => [
                    'label' => 'inArray(needle, haystack)',
                    'kind' => 'function',
                    'insertText' => 'inArray(${1:needle}, ${2:haystack})',
                ]
            ],
        ];

        return $ret;
    }

    public static function testJson($arr,$dzetaCalc)
    {
        $arrTest=[];
        foreach ($arr as $key => $value) {
           if(is_array($value))
           {
                $arrTest[$key]=new SystemValue(self::testJson( $value,$dzetaCalc),SystemValue::ARRAY_TYPE);
           }else{
                $arrTest[$key]=new SystemValue($value, SystemValue::STRING_TYPE);
           }
        }
        return $arrTest;
    }
    public static function testJson1($arr,$dzetaCalc)
    {
        $arrTest=[];
        foreach ($arr as $key => $value) {
           if($value->Type==SystemValue::ARRAY_TYPE)
           {
                $arrTest[$key]=self::testJson1($value->Value,$dzetaCalc);
           }else{
                $arrTest[$key]=$value->Value;
           }
        }
        return $arrTest;
    }

    public static function inArrayF($dzetaCalc, $paramsFunc, $paramProc) {
        if (count($paramsFunc) != 2) {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0]);
            return $dzetaCalc->GetValueForVar("false");
        }
        if ($paramsFunc[1]->GetType() != SystemValue::ARRAY_TYPE) {
            Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1]);
            return $dzetaCalc->GetValueForVar("false");
        }
        $haystack = $paramsFunc[1]->GetRawValue();
        $needle = $paramsFunc[0]->GetRawValue();
        $result = in_array($needle, $haystack);
        return $result ? $dzetaCalc->GetValueForVar("true") : $dzetaCalc->GetValueForVar("false");
    }


    public static function jsonencodeF($dzetaCalc, $paramsFunc, $paramProc){
         if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {

            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0]);
                return $dzetaCalc->GetValueForVar("false");
            }
           
          
            return new SystemValue(json_encode(self::testJson1($paramsFunc[0]->Value,$dzetaCalc)),SystemValue::STRING_TYPE);
           // return new SystemValue(count($paramsFunc[0]->Value), SystemValue::NUMBER_TYPE);
        }
    }


    public static function jsondecodeF($dzetaCalc, $paramsFunc, $paramProc){
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {

            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0]);
                return $dzetaCalc->GetValueForVar("false");
            }
           

            return new SystemValue(self::testJson(json_decode($paramsFunc[0]->Value,true),$dzetaCalc),SystemValue::ARRAY_TYPE);
           // return new SystemValue(count($paramsFunc[0]->Value), SystemValue::NUMBER_TYPE);
        }
    }


    public static function elemArrIndexM($dzetaCalc, $paramsFunc, $paramProc)
    {
		$tgh=new SystemValue($paramsFunc[0]->Value, $paramsFunc[0]->Type,isset($paramsFunc[0]->dopSett)?$paramsFunc[0]->dopSett:[] );
		
        return $tgh;
    }

    public static function elemArrM($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            if ($paramProc[0]->Type!=SystemValue::ARRAY_TYPE) {
                Errors::AddError($Dzeta, ['ns'=>'ArrFunc','func'=>'getElem','num'=>0]);
                return $dzetaCalc->GetValueForVar("null");
            }
            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0]);
                return $dzetaCalc->GetValueForVar("false");
            }
            $el=$paramsFunc[0];
            if (isset($paramProc[0]->Value[$el->Value])) {
                return new SystemValue($paramProc[0]->Value[$el->Value]->Value, $paramProc[0]->Value[$el->Value]->Type,isset($paramProc[0]->Value[$el->Value]->dopSett)?$paramProc[0]->Value[$el->Value]->dopSett:[]);
            } else {
                Errors::AddError($dzetaCalc, ['ns'=>'ArrFunc','func'=>'getElem','num'=>1]);
                return $dzetaCalc->GetValueForVar("null");
            }
        } else {
            return $dzetaCalc->GetValueForVar("null");
        }
    }
    public static function countF($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {

            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0]);
                return $dzetaCalc->GetValueForVar("false");
            }
            if ($paramsFunc[0]->Type!=SystemValue::ARRAY_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'ArrFunc','func'=>'getElem','num'=>0]);
                return $dzetaCalc->GetValueForVar("null");
            }
            return new SystemValue(count($paramsFunc[0]->Value), SystemValue::NUMBER_TYPE);
        }
    }

    public static function arrayF($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {
            $tmpValue=[];
            foreach ($paramsFunc as $valuElem) {
                if ($valuElem->Type==SystemValue::ARRAY_TYPE) {
                    $tmpValue[]=new SystemValue(self::testG($valuElem, $dzetaCalc), SystemValue::ARRAY_TYPE);
                } else {
                    $tmpValue[]=$valuElem;
                }
            }
            return new SystemValue($tmpValue, SystemValue::ARRAY_TYPE);
        }
    }

    private static function testG($ElemArr, $dzetaCalc)
    {
        $tmp=[];
        foreach ($ElemArr->Value as $valAr) {
            if ($valAr->Type==SystemValue::ARRAY_TYPE) {
                $tmp[]=new SystemValue(self::testG($valAr, $dzetaCalc), SystemValue::ARRAY_TYPE);
            } else {
                $tmp[]=$valAr;
            }
        }
        return $tmp;
    }
}
