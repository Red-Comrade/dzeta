<?php

namespace DzetaCalcParser;

class MathOperations
{
    public static function Additional($val1, $val2, $Dzeta)
    {
        
        
        if($val1->Type!=SystemValue::NUMBER_TYPE||$val2->Type!=SystemValue::NUMBER_TYPE)
        {
            $oper1="";
            $oper2="";
            if(!is_null($val1->Value))
            {
                $oper1=$val1->Value;
            }
            if(!is_null($val2->Value))
            {
                $oper2=$val2->Value;
            }
             return new SystemValue($oper1.$oper2, SystemValue::STRING_TYPE);
            
        }
        $newV=$val1->Value+$val2->Value;
        return new SystemValue($newV, SystemValue::NUMBER_TYPE);
    }

    public static function Subtraction($val1, $val2, $Dzeta)
    {
        if (is_null($val2)) {
            if($val1->Type!==SystemValue::NUMBER_TYPE)
            {
                Errors::AddError($Dzeta, ['ns'=>'MathOperations','func'=>'Global','num'=>0]);
                return new SystemValue(1, SystemValue::NUMBER_TYPE);
            }
            return new SystemValue($val1->Value*(-1), SystemValue::NUMBER_TYPE);
        }
        if($val1->Type!==SystemValue::NUMBER_TYPE||$val2->Type!==SystemValue::NUMBER_TYPE)
        {
            Errors::AddError($Dzeta, ['ns'=>'MathOperations','func'=>'Global','num'=>0]);
            return new SystemValue(1, SystemValue::NUMBER_TYPE);
        }
        $newV=$val1->Value-$val2->Value;
        return new SystemValue($newV, SystemValue::NUMBER_TYPE);
    }

    public static function Multiplication($val1, $val2, $Dzeta)
    {
        if($val1->Type!==SystemValue::NUMBER_TYPE||$val2->Type!==SystemValue::NUMBER_TYPE)
        {
            Errors::AddError($Dzeta, ['ns'=>'MathOperations','func'=>'Global','num'=>0]);
            return new SystemValue(1, SystemValue::NUMBER_TYPE);
        }
        $newV=$val1->Value*$val2->Value;
        return new SystemValue($newV, SystemValue::NUMBER_TYPE);
    }

    public static function Division($val1, $val2, $Dzeta)
    {
        if($val1->Type!==SystemValue::NUMBER_TYPE||$val2->Type!==SystemValue::NUMBER_TYPE)
        {
            Errors::AddError($Dzeta, ['ns'=>'MathOperations','func'=>'Global','num'=>0]);
            return new SystemValue(1, SystemValue::NUMBER_TYPE);
        }
        if ($val2->Value==0) {
            Errors::AddError($Dzeta, ['ns'=>'MathOperations','func'=>'Division','num'=>0]);
            return new SystemValue(1, SystemValue::NUMBER_TYPE);
        }
        $newV=$val1->Value/$val2->Value;
        return new SystemValue($newV, SystemValue::NUMBER_TYPE);
    }
    public static function Mod($val1, $val2, $Dzeta)
    {
        if($val1->Type!==SystemValue::NUMBER_TYPE||$val2->Type!==SystemValue::NUMBER_TYPE)
        {
            Errors::AddError($Dzeta, ['ns'=>'MathOperations','func'=>'Global','num'=>0]);
            return new SystemValue(1, SystemValue::NUMBER_TYPE);
        }
        if ($val2->Value==0) {
            Errors::AddError($Dzeta, ['ns'=>'MathOperations','func'=>'Mod','num'=>0]);
            return new SystemValue(1, SystemValue::NUMBER_TYPE);
        }
        $newV=$val1->Value%$val2->Value;
        return new SystemValue($newV, SystemValue::NUMBER_TYPE);
    }
}
