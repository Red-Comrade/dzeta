<?php

namespace DzetaCalcParser;

use Dzeta\Models\Obj;
use Dzeta\Models\Attr;
use Dzeta\Models\Type;
use Dzeta\Models\View;

class DatabaseFunctions extends BaseFunctions
{
    protected static function DoGetMethods()
    {
        $ret = [
          'getAttr' => [
            'name' => 'getAttr'
          ],
          'getAgrChild' => [
            'name' => 'getAgrChild'
          ],
          'setFormAttr' => [
            'name' => 'setFormAttr',
          ],
          'getName' => [
            'name' => 'getObjName',
          ],
          'getUid' => [
            'name' => 'getUid',
          ],
          'parentObj' => [
            'name' => 'parentObj',
          ],
          'createObj' => [
            'name' => 'createObj',
          ],
          'getChild' => [
            'name' => 'getChild',
          ],
          'updateObj' => [
            'name' => 'updateObj',
          ],
        ];

        return $ret;
    }

    protected static function DoGetFunctions()
    {
        $ret =[
          // 'toString'  =>__NAMESPACE__ . '\NumberFunctions::toStringF', //
        	'searchObj' => [
            'name' => 'searchObjParam',
          ],
          'getViewData_t' => [
            'name' => 'getViewDataF'
          ],
        ];

        return $ret;
    }


    public static function updateObj($dzetaCalc, $paramFunc, $paramProc)
    {
       if (!is_null($paramProc)) {

            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

           $_obj=new Obj();
         $obj = new \Dzeta\Core\Instance($paramProc[0]->Value, '');
         // $_obj->getParent($obj);
        //  $typeName=$paramFunc[0]->Value;
          $attrsName=$paramFunc[0]->Value;
           $Values=$paramFunc[1]->Value;
          
            $class=new \Dzeta\Core\Type\Type($paramProc[0]->dopSett['type'],'');
            $_class=new Type();
           /* $res=$_class->GetIDByName($class);
            if ($res!=1) {
               // Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'class','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }*/
            $attrs=[];
            foreach ($attrsName as $key => $value) {
               $_attr=new Attr();
               $attr=new \Dzeta\Core\Type\Attribute('',$value->Value, 1, false, false);
               $attr->setParent($class);

               $res=$_attr->GetIDByName([$attr]);
               $attrs[]=$attr;
            }
            $_attr->getAttributesByList($attrs);
          
           $obj->setType($class);

            foreach ($attrs as $key => $value) {
             $attrs[$key]->setValue(['value'=>$Values[$key]->Value]);
            }
            $obj->setAttributes($attrs);
           
         
            $results = $_obj->addValues([$obj], $dzetaCalc->userUid);
          //  echo $objChild->getUid();
           // echo $objChild->getType()->getUid();
             return new SystemValue($obj->getUid(),SystemValue::OBJECT_TYPE,
        ['type'=>$obj->getType()->getUid()]);
           
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }

    public static function getViewDataF($dzetaCalc, $paramFunc, $paramProc)
    {
      if(is_null($paramProc))
      {
        $uidView=$paramFunc[0]->Value; 
        $uidParam=$paramFunc[1]->Value; 
        $num=1000;  
        if(isset($paramFunc[2]))
        {
          $num=$paramFunc[2]->Value;
        }
        $_view=new View();
        $view=new \Dzeta\Core\App\View($uidView,'test');
        $uidSortAttr = $uidParam;
        $typeSort = 0;  
        $start = 1; 
          
        $view->setDopSett('attrSort', new \Dzeta\Core\Type\Attribute($uidSortAttr, '', 1, false, false));
        $view->setDopSett('sort', $typeSort);
        $view->setDopSett('num', $num-1);
        $view->setDopSett('start', $start);

       $data= $_view->getDataTable($view,$dzetaCalc->userUid);
       $resultData=[];
       if(isset( $data['data']))
       {

          foreach ($data['data'] as $key => $value)
          {
              $arrV=[];
              foreach ($value as $key => $val) {  
                if(isset($val['v']))     
                {
                  $arrV[]=new SystemValue($val['v'], SystemValue::STRING_TYPE);
                } else{
                  $arrV[]=$dzetaCalc->GetValueForVar("NULL");;
                }        
                
              }
              $resultData[]=new SystemValue($arrV, SystemValue::ARRAY_TYPE);;
          }
       }
      
       return new SystemValue($resultData, SystemValue::ARRAY_TYPE);
      }
    }

    public static function searchObjParam($dzetaCalc, $paramFunc, $paramProc){

    	if(is_null($paramProc))
    	{
    		
    		$typeName=$paramFunc[0]->Value;	     
            $class=new \Dzeta\Core\Type\Type('', $typeName);
            $_class=new Type();
            $res=$_class->GetIDByName($class); 
            if ($res!=1) {
               // Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'class','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $cond=$paramFunc[1]->Value;	
           $newUsl=[];
           $_attr=new Attr();
           foreach ($cond as $key => $value) {
           		if($value->Type==SystemValue::ARRAY_TYPE)
           		{
           			$val=$value->Value;
           			if(isset($val['name'])&&isset($val['value'])&&isset($val['cond']))
           			{

               			$attr=new \Dzeta\Core\Type\Attribute('',$val['name']->Value, 1, false, false);
               			$attr->setParent($class);
               			$res=$_attr->GetIDByName([$attr]);               			
               			if($res['result']==1)
               			{
               				$v='';
               				if($val['value']->Type==SystemValue::DATETIME_TYPE)
               				{
               					$v=$val['value']->Value->format('m/d/Y');
               				}else{
               					$v=$val['value']->Value;
               				}
               				$newUsl[]='['.$attr->getUid().']'.$val['cond']->Value.'#'.$v.'#';
               			}
           			}
           		}
           }
           $objsDelim=[];
           if(!isset($paramFunc[2]->Value))
           {
           		//значит корень
           		$objsDelim[]=new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, '');
           }else{

           		$objsDelim[]=new \Dzeta\Core\Instance($paramFunc[2]->Value, '');
           }
            $_obj=new Obj();

          $child=$_obj->searchObj($class,implode(' AND ',$newUsl),$objsDelim,1);

          $chldObjCalc=[];
           	foreach ($child['data'] as $key => $value) {           		
           		
           		$chldObjCalc[]=new SystemValue(
           			$value->getUid(),
           			SystemValue::OBJECT_TYPE,
        			['type'=>$value->getType()->getUid(),'objName'=>$value->getName()]);
           	}
         
           
          //  echo $objChild->getUid();
           // echo $objChild->getType()->getUid();
             return new SystemValue($chldObjCalc, SystemValue::ARRAY_TYPE);
    	}
    }


    public static function getChild($dzetaCalc, $paramFunc, $paramProc)
    {
       if (!is_null($paramProc)) {

            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            $_obj=new Obj();
         	$obj = new \Dzeta\Core\Instance($paramProc[0]->Value, '');
	    
	        $typeName=$paramFunc[0]->Value;
	     
            $class=new \Dzeta\Core\Type\Type('', $typeName);
            $_class=new Type();
            $res=$_class->GetIDByName($class);
           

            if ($res!=1) {
               // Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'class','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $child=$_obj->getChildrenByType($obj,$class);
            $chldObjCalc=[];
           	foreach ($child as $key => $value) {
           		
           		if(!empty($paramFunc[1]->Value))
           		{
           			if($paramFunc[1]->Value<=$key)
           			{
           				continue;
           			}
           		}
           		$chldObjCalc[]=new SystemValue(
           			$value->getUid(),
           			SystemValue::OBJECT_TYPE,
        			['type'=>$value->getType()->getUid(),'objName'=>$value->getName()]);
           	}
         
           
          //  echo $objChild->getUid();
           // echo $objChild->getType()->getUid();
             return new SystemValue($chldObjCalc, SystemValue::ARRAY_TYPE);
           
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }
    public static function createObj($dzetaCalc, $paramFunc, $paramProc)
    {
       if (!is_null($paramProc)) {

            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

           $_obj=new Obj();
         $obj = new \Dzeta\Core\Instance($paramProc[0]->Value, '');
          $_obj->getParent($obj);
          $typeName=$paramFunc[0]->Value;
          $attrsName=$paramFunc[1]->Value;
           $Values=$paramFunc[2]->Value;
            $class=new \Dzeta\Core\Type\Type('', $typeName);
            $_class=new Type();
            $res=$_class->GetIDByName($class);
            if ($res!=1) {
               // Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'class','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $attrs=[];
            foreach ($attrsName as $key => $value) {
               $_attr=new Attr();
               $attr=new \Dzeta\Core\Type\Attribute('',$value->Value, 1, false, false);
               $attr->setParent($class);

               $res=$_attr->GetIDByName([$attr]);
               $attrs[]=$attr;
            }
            $_attr->getAttributesByList($attrs);
           $objChild = new \Dzeta\Core\Instance('', '');
            $objChild->setType($class)->setParent($obj);

            foreach ($attrs as $key => $value) {
             $attrs[$key]->setValue(['value'=>$Values[$key]->Value]);
            }
            $objChild->setAttributes($attrs);
           
         
            $results = $_obj->checkUniqueAndAddWithValues($objChild, \Dzeta\Core\Instance::ADDED_BY_CALC, $dzetaCalc->userUid);
          //  echo $objChild->getUid();
           // echo $objChild->getType()->getUid();
             return new SystemValue($objChild->getUid(),SystemValue::OBJECT_TYPE,
        ['type'=>$objChild->getType()->getUid()]);
           
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }

    public static function parentObj($dzetaCalc, $paramsFunc, $paramProc)
    {
       if (!is_null($paramProc)) {

            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

           $_obj=new Obj();
         $obj = new \Dzeta\Core\Instance($paramProc[0]->Value, '');
          $_obj->getParent($obj);

           return new SystemValue($obj->getParent()->getUid(),SystemValue::OBJECT_TYPE,
        ['type'=>$obj->getParent()->getType()->getUid(),'objName'=>$obj->getParent()->getName()]);
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }
    public static function getUid($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {

            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            if (count($paramsFunc)!=0) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            } 
            return new SystemValue($paramProc[0]->Value, SystemValue::STRING_TYPE);
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }

    public static function getAgrChild($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            if (count($paramsFunc)!=3) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            if ($paramsFunc[0]->Type!=SystemValue::STRING_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            $class=new \Dzeta\Core\Type\Type('', $paramsFunc[0]->Value);
            $_class=new Type();
            $res=$_class->GetIDByName($class);
            if ($res!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'class','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            if ($paramsFunc[1]->Type!=SystemValue::STRING_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            $_attr=new Attr();

            $attr=new \Dzeta\Core\Type\Attribute('', $paramsFunc[1]->Value, 1, false, false);
            $attr->setParent($class);

            $res=$_attr->GetIDByName([$attr]);
            if ($res!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'getAttr','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            if ($paramsFunc[2]->Type!=SystemValue::STRING_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>3], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $typess=$paramsFunc[2]->Value;
            $typeAgr=null;
            switch (mb_strtolower($typess)) {
              case 'max':
                $typeAgr='MAX';
                break;
              case 'min':
                $typeAgr='MIN';
                break;
              case 'sum':
                $typeAgr='SUM';
                break;
            }
            if(is_null($typeAgr))
            {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>3], [$typess,self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            $objjj=new Obj();
            $obj=new \Dzeta\Core\Instance($paramProc[0]->Value, '');
            return new SystemValue($objjj->aggr($obj->getUid(), $class->getUid(), $attr->getUid(), $typeAgr), SystemValue::NUMBER_TYPE);
        } else {
            //тут типа как функция работаем
          //  \DZ::yter5465([$paramsFunc,$paramProc]);
        //    return new SystemValue($paramsFunc[0]->Value, SystemValue::STRING_TYPE);
        }
        
    }

    public static function setFormAttr($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            if (count($paramsFunc)!=2) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            if ($paramsFunc[0]->Type!=SystemValue::STRING_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            $_obj=new Obj();
            $_attr=new Attr();
            $attr=new \Dzeta\Core\Type\Attribute('', $paramsFunc[0]->Value, 1, false, false);
            $attr->setParent(new \Dzeta\Core\Type\Type($paramProc[0]->dopSett['type'], ''));

            $res=$_attr->GetIDByName([$attr]);
            if ($res['result']!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'getAttr','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $dzetaCalc->FormData[$paramProc[0]->Value][$attr->getUid()]=$paramsFunc[1];
            return $dzetaCalc->GetValueForVar("true");
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }


    public static function getAttr($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            if (count($paramsFunc)!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            if ($paramsFunc[0]->Type!=SystemValue::STRING_TYPE) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>1], [self::GetNameFunctions(__METHOD__),1]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            $_obj=new Obj();
            $_attr=new Attr();
            $attr=new \Dzeta\Core\Type\Attribute('', $paramsFunc[0]->Value, 1, false, false);
            $attr->setParent(new \Dzeta\Core\Type\Type($paramProc[0]->dopSett['type'], ''));

            $res=$_attr->GetIDByName([$attr]);

            if ($res['result']!=1) {
                Errors::AddError($dzetaCalc, ['ns'=>'DB','func'=>'getAttr','num'=>0], [$paramsFunc[0]->Value]);
                return $dzetaCalc->GetValueForVar("NULL");
            }

            $resData=self::DBCache(['mode'=>'GetValue','obj'=>$paramProc[0]->Value,'attr'=>$attr->getUid()],$dzetaCalc);           
            return $resData[0];
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }

    public static function getObjName($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {

            if ($paramProc[0]->Type==SystemValue::ARRAY_TYPE) {
               $t=[];
               foreach ($paramProc[0]->Value as  $value) {
               
                    $t[]=new SystemValue($value->dopSett['objName'], SystemValue::STRING_TYPE);
               }
               return new SystemValue($t, SystemValue::ARRAY_TYPE);
            }
            if ($paramProc[0]->Type!=SystemValue::OBJECT_TYPE) {
               /* Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>2], [self::GetNameFunctions(__METHOD__),1]);*/
                return $dzetaCalc->GetValueForVar("NULL");
            }

            if (count($paramsFunc)!=0) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }           
            if(isset($paramProc[0]->dopSett['objName']))
            {
                
                 return new SystemValue($paramProc[0]->dopSett['objName'], SystemValue::STRING_TYPE);
             }else{
                 return new SystemValue("Noname", SystemValue::STRING_TYPE);
             }
           
           
        } else {
            //тут типа как функция работаем
            return $dzetaCalc->GetValueForVar("NULL");
        }
    }


    private static function DBCache($params,$dzetaCalc)
    {
      switch ($params['mode']) {
        case 'GetValue':
        \DZ::resData($dzetaCalc->dbCache);
          if(isset($dzetaCalc->dbCache['ObjValue'][$params['obj']][$params['attr']]))
          {
              return $dzetaCalc->dbCache['ObjValue'][$params['obj']][$params['attr']];
          }else {
            $attr=new \Dzeta\Core\Type\Attribute($params['attr'],'',  1, false, false);
            $obj=new \Dzeta\Core\Instance($params['obj'], '');
            $obj->setAttributes([$attr]);
            $_obj=new Obj();
            $_obj->getAllValues([$obj]);
            $resData=self::ConvertDBValueToSysValue($dzetaCalc,[$obj]);
            $res=$resData[$params['obj']];
            $dzetaCalc->dbCache['ObjValue'][$params['obj']][$params['attr']]=$res;
            return $res;
          }
          break;

        default:
          // code...
          break;
      }
    }
    public static function ConvertDBValueToSysValue($dzetaCalc, array $objsA)
    {
         $sysValueArr=[];
    //echo "777_";
    $_obj=new Obj();
        foreach ($objsA as $objV) {
      if(empty($objV)) 
      {
        //echo 9000;
        $tmpV[]=$dzetaCalc->GetValueForVar("NULL");
        $sysValueArr[]=$tmpV;
        continue;
      }
      if($objV->getType()){
        
        $_obj->GetClassForObject($objV);
      }
      // 
          $attrs=$objV->getAttributes();
          $uidObj=$objV->getUid();
      if($objV->getType()==null)
      {
        $uidType=null;
      }else{
        $uidType=$objV->getType()->getUid();
      }
          $sysValueArr[$uidObj]=[];
          $tmpV=[];
     
          \DZ::attrssd($attrs);
          foreach ($attrs as $key => $value) {
              $typeV=$value->getDatatype();
              $valueV=$value->getValue();
              $dopSettA=['attr'=>$value->getUid(),'obj'=>$uidObj,'type'=> $uidType];
              if (is_null($valueV)) {
                  $tmpV[]=$dzetaCalc->GetValueForVar("NULL");
              } else {
                  switch ($typeV) {
                    case \Dzeta\Core\Type\DataType::TYPE_NUMBER:
                      $tmpV[]= new SystemValue($valueV->getValue(), SystemValue::NUMBER_TYPE,$dopSettA);
                      break;
                    case \Dzeta\Core\Type\DataType::TYPE_TEXT:
                      $tmpV[]= new SystemValue($valueV->getValue(), SystemValue::STRING_TYPE,$dopSettA);
                      break;
                    case \Dzeta\Core\Type\DataType::TYPE_OBJECT:
                      if($value->isArray())
                      {
                        $t=[];                       
                        foreach ($value->getValue() as $val) {
                             $dopSettA['objName']=$val->getName();
                             if(empty($value->getParent()))
                             {
                              $_obj->GetClassForObject($val);
                              $dopSettA['type']= $val->getType()->getUid();
                             }else{
                              $dopSettA['type']= $value->getParent()->getUid();
                             }
                          
                            
                            $t[]= new SystemValue($val->getUid(), SystemValue::OBJECT_TYPE,$dopSettA);
                        }
                        $tmpV[]= new SystemValue($t, SystemValue::ARRAY_TYPE);
                      }else{
                         $dopSettA['objName']=$valueV->getName();
                         if(empty($value->getParent()))
                         { 
                       //   echo 77777;
                          $_obj->GetClassForObject($valueV);
                           $dopSettA['type']= $valueV->getType()->getUid();
                         
                         }else{
                          $dopSettA['type']= $value->getParent()->getUid();
                         }
                         
                      
                        $tmpV[]= new SystemValue($valueV->getUid(), SystemValue::OBJECT_TYPE,$dopSettA);
                      }
                     
                      break;
                    default:
                      // code...
                      break;
                }
              }
          }
          $sysValueArr[$uidObj]=$tmpV;
        }
    /* echo PHP_EOL;
        echo json_encode($sysValueArr);
        echo PHP_EOL;*/
    return $sysValueArr;
    }
}
