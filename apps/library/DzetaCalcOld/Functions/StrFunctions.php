<?php

namespace DzetaCalcParser;

class StringFunctions extends BaseFunctions
{
    protected static function DoGetMethods()
    {
        $ret = [];
        return $ret;
    }

    protected static function DoGetFunctions()
    {
        $ret = [
            'str_replace' => [
                'name' => 'strreplace',
                'completion' => [
                    'label' => 'str_replace(string|array search, string|array replace, string|array subject)',
                    'kind' => 'function',
                    'insertText' => 'str_replace(${1:search}, ${2:replace}, ${3:subject})',
                ]
            ],
            'strpos' => [
                'name' => 'strposF',
                'completion' => [
                    'label' => 'strpos(string haystack, array needle)',
                    'kind' => 'function',
                    'insertText' => 'strpos(${1:haystack}, ${2:needle})',
                ]
            ],
        ];

        return $ret;
    }

    public static function strreplace($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {
            //как функция выполняется
            if (!(count($paramsFunc)==3)) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $search=$paramsFunc[0]->Value;
            $replace=$paramsFunc[1]->Value;
            $str=$paramsFunc[2]->Value;
            $str=str_replace($search, $replace, $str);

            return new SystemValue($str, SystemValue::STRING_TYPE);
        }
    }
    public static function strposF($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (!is_null($paramProc)) {
            return $dzetaCalc->GetValueForVar("NULL");
        } else {
            //как функция выполняется
            if (!(count($paramsFunc)==2)) {
                Errors::AddError($dzetaCalc, ['ns'=>'Global','func'=>'ParamsFunc','num'=>0], [self::GetNameFunctions(__METHOD__)]);
                return $dzetaCalc->GetValueForVar("NULL");
            }
            $search=$paramsFunc[0]->Value;
            $replace=$paramsFunc[1]->Value;
            $str=mb_strpos($search,$replace);
            if($str===false)
            {
                return new SystemValue(-1, SystemValue::NUMBER_TYPE);
            }else{
               return new SystemValue( $str, SystemValue::NUMBER_TYPE);
            }

        }
    }
}