<?php

class MockDb
{
	private const DATA = [
		'Attr_AddNew' => [
			'data' => [
				[ 'IDc' => '00000000-test-attr-test-000000000000', 'Rcode' => 1 ]
			]
		],
		'Attr_EditSettings' => [
			'data' => [
				[ 'Rcode' => 1 ]
			],
		],
		'Attr_GetByClassGroup' => [
			'data' => [
				[
		            'IDc' => '00000000-test-attr-test-000000000000',
		            'Name' => 'testAttr',
		            'TypeOfStoredData' => 1,
		            'GroupID' => '00000000-test-attr-test-000000000000',
		            'NumInGroup' => 0,
		            'Array' => 1,
		            'Uniq' => 1
		        ]
			]
		],
		'Attr_group_AddNew' => [
			'data' => [
				[ 'IDc' => '00000000-test-attr-test-000000000000', 'Rcode' => 1 ]
			]
		],
		'Attr_group_Rename' => [
			'data' => [
				[ 'Rcode' => 1 ]
			]
		],
		'Attr_group_GetByClass' => [
			'data' => [
				[ 'ID' => '00000000-test-attr-grop-000000000000', 'Name' => 'Просто тестовая группа']
          	]
		],
		'Class_AddNew' => [
			'data' => [
				[ 'IDc' => '00000000-test-test-test-000000000000', 'Rcode' => 1 ]
			]
		],
		'ClassRename' => [
			'data' => [
				[ 'Rcode' => 1 ]
			]
		],
		'Class_GetByBaseClass' => [
			'data' => [
				[ 'IDc' => '00000000-test-test-test-000000000000', 'ClassName' => 'test' , 'IsSystemClass' => 1 ],
				[ 'IDc' => '00000000-test-test-test-000000000001', 'ClassName' => 'test1' , 'IsSystemClass' => 0 ]
			]
		],
		'Pass_Check'=> [
			'data' => [
				[ 'User' => '00000000-test-user-test-000000000000','Rcode'=>1 ]

			]],
		'Pass_AddNew'=>[
			'data' => [
				['Rcode'=>1 ]
			],
		],
	];

	public static function getData($name) {
		$data = [];

		if (isset(self::DATA[$name]['data'])) {
			$data = self::DATA[$name]['data'];
		}

		return $data;
	}
}
