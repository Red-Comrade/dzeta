<?php
declare(strict_types=1);

namespace Dzeta\Modules\Frontend\Controllers;

use Dzeta\Type\Type;
use Dzeta\Controllers\ControllerBase;
use Dzeta\Helpers\ViewHelper;

class IndexController extends ControllerBase
{


    public function indexAction()
    {
        $this->view->nameSystem = $this->config->dzeta->nameSystem;
        $this->view->scriptV=empty($this->config->dzeta->scriptVersion)? time():$this->config->dzeta->scriptVersion;
        $this->view->background_login = $this->config->dzeta->background_login;

        $this->view->defaultTheme = $this->config->dzeta->defaultTheme;
        $this->view->favicon = $this->config->dzeta->favicon;
        $this->view->version = $this->config->dzeta->version;
        $this->view->color_login = $this->config->dzeta->color_login;
        $this->view->img_logo_url = $this->config->dzeta->img_logo_url;
        $this->view->img_logo_alt = $this->config->dzeta->img_logo_alt;
        $this->view->img_logo_min_url = $this->config->dzeta->img_logo_min_url;
        $this->view->img_logo_min_alt = $this->config->dzeta->img_logo_min_alt;
        $this->view->img_logo_sidebar_url = $this->config->dzeta->img_logo_sidebar_url;
        $this->view->img_logo_sidebar_alt = $this->config->dzeta->img_logo_sidebar_alt;

        $this->view->registration_group = $this->config->dzeta->registration_group;


        $this->view->loaderHTML = ViewHelper::getLoaderHTML($this->config->dzeta);
    }

    public function getMainInfoAction()
    {
      //name
      $user=$this->session->get('logged')['User'];
      $nameUser=$user->getName();
      $data=[];
      $data['module']=array_column($user->getPermissionSession('module'),'name');
      $data['nameUser']=$nameUser;
      echo json_encode(['result'=>1,'nameUser'=>$nameUser,'data'=>$data]);
    }

}
