<?php

namespace Dzeta\Modules\Frontend\Controllers;

use Phalcon\Mvc\Controller;

use Dzeta\Core;
use Dzeta\Core\User\UserGroup;
use Dzeta\Controllers\ControllerBase;
use Dzeta\Helpers\ViewHelper;


class LoginController extends Controller
{
    public function indexAction()
    {
        if ($this->session->has('logged')) {
            $last_url_g = $this->request->get('last_url');
            if (isset($last_url_g)) {
                $last_url = urldecode($last_url_g);
                $this->response->redirect('index' . $last_url);
            }
            else {
                if (!isset($this->config->dzeta->defaultLocation)) {
                    $this->response->redirect('index');
                } elseif ($this->config->dzeta->defaultLocation == 1) {
                    $user = $this->session->get('logged')['User'];
                    $modules = $user->getPermissionSession('module');
                    $this->response->redirect($modules[0]['path'] );
                }
                else {
                    $this->response->redirect($this->config->dzeta->defaultLocation);
                }
            }
        }
        
        $this->view->nameSystem = $this->config->dzeta->nameSystem;
        $this->view->scriptV=$this->config->dzeta->scriptVersion;

        $this->view->favicon = $this->config->dzeta->favicon;
        $this->view->version = $this->config->dzeta->version;
        $this->view->defaultTheme = $this->config->dzeta->defaultTheme;
        $this->view->background_login = $this->config->dzeta->background_login;
        $this->view->color_login = $this->config->dzeta->color_login;
        $this->view->img_logo_url = $this->config->dzeta->img_logo_url;
        $this->view->img_logo_alt = $this->config->dzeta->img_logo_alt;
        $this->view->img_logo_min_url = $this->config->dzeta->img_logo_min_url;
        $this->view->img_logo_min_alt = $this->config->dzeta->img_logo_min_alt;
        $this->view->img_logo_sidebar_url = $this->config->dzeta->img_logo_sidebar_url;
        $this->view->img_logo_sidebar_alt = $this->config->dzeta->img_logo_sidebar_alt;
        $this->view->forgotPWDEmail=empty($this->config->dzeta->forgotPasswordEmail)? false:true;
        $this->view->SortAttrDir = !is_null($this->config->dzeta->SortAttrDir) ? $this->config->dzeta->SortAttrDir : 0;
        $this->view->registration_group = $this->config->dzeta->registration_group;


        if($this->config->dzeta->AppUID) {
            $this->view->isAppLogin = true;
            $this->view->AppUID = $this->config->dzeta->AppUID;
            $this->view->ViewUIDparrent = $this->config->dzeta->ViewUIDparrent ? $this->config->dzeta->ViewUIDparrent : null;
            $this->view->ViewUID = $this->config->dzeta->ViewUID ? $this->config->dzeta->ViewUID : null;
            $this->view->SortAttrUID = $this->config->dzeta->SortAttrUID ? $this->config->dzeta->SortAttrUID : null;
        }
        else {
            $this->view->isAppLogin = false;
        }

        $this->view->loaderHTML = ViewHelper::getLoaderHTML($this->config->dzeta);
    }

    public function loginAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $login = $this->request->getPost('login');
            $password = $this->request->getPost('pwd');
            $ip = $this->request->getClientAddress();

            $user = new Core\User\User('', $login);
            $user->setPassword($password)->setIP($ip);

            $_user = new User();
            $result = $_user->checkPassword($user);
            if ($result['result'] == 1)  
            {
                $_perm=new Permission();
                $perm=$_perm->getUserSessPermission($user);
                if(!empty($perm['modules']))
                {
                    $user->setPermissionSession("module",$perm['modules']);
                    $user->setPermissionSession("controller",$perm['controllers']);
                    $dataResult['result']=1;
                    $this->session->set('logged', [
                          'User' => $user
                    ]);
                }else 
                {
                    $dataResult['result']=0;
                }
            }else
            {
                $dataResult['result']= $result['result'];
                $dataResult['errors']= $result['error'];
            }
        }
        echo json_encode($dataResult);
    }

    public function logoutAction()
    {
        $this->session->destroy();
        $this->response->redirect('login');
    }

    public function forgotPasswordAction()
    {
        $result=-1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
    				$user = new Core\User\User('',$this->request->get('login'));
    				$_user = new User();
    				$res = $_user->forgotPasswordEmail($user);
    				if($res['result']==1)
    				{
    				}else {
    						$dataResult['errors']=$res['error'];
    				}
    				$dataResult['result']= $res['result'];
    		}

    		echo json_encode($dataResult);

    }
}
