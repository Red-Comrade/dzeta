<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Core\BaseFunctions;

use Dzeta\Modules\Enemat\Models\Specialenemat;

class EnematFunctions extends BaseFunctions
{
	protected static $methods = [];
	protected static $functions = [
		'apiDatasocieteinfocom' => [
            'name' => 'getDatasocieteinfocom',
            'completion' => [
                'label' => 'apiDatasocieteinfocom',
                'detail' => 'apiDatasocieteinfocom(string siren|siret)',
                'kind' => 'function',
                'insertText' => 'apiDatasocieteinfocom(${1:siren|siret})',
            ],
            'arguments' => [
            	[
            		'type' => Constants::TYPE_STRING,
            	]
            ]
        ],
        'apiDatainfogreffe' => [
            'name' => 'getDatainfogreffe',
            'completion' => [
                'label' => 'apiDatainfogreffe',
                'detail' => 'apiDatainfogreffe(string siren|siret)',
                'kind' => 'function',
                'insertText' => 'apiDatainfogreffe(${1:siren|siret})',
            ],
            'arguments' => [
            	[
            		'type' => Constants::TYPE_STRING,
            	]
            ]
        ],
        'testRF'=>[
			'name' => 'testRF',
			'completion' => [
				'label' => 'testRF',
				'detail' => 'testRF()',
				'kind' => 'function',
				'insertText' => 'testRF()'
			],
			'arguments' => [
				
			]
		],
		'getCoefuserEnemat' => [
			'name' => 'getCoefuserEnemat',
			'completion' => [
				'label' => 'getCoefuserEnemat',
				'detail' => 'getCoefuserEnemat(string uidOrganization,string uidOperation,date date,int typeUser,string uidUser)',
				'kind' => 'function',
				'insertText' => 'getCoefuserEnemat(${1:value},${2:value},${3:value},${4:value},${5:value})'
			],
			'arguments' => [
				[
					'type' => Constants::TYPE_STRING
				],
				[
					'type' => Constants::TYPE_STRING
				],
				[
					'type' => Constants::TYPE_STRING
				],
				[
					'type' => Constants::TYPE_NUMBER
				],
				[
					'type' => Constants::TYPE_STRING
				]
			]
		]
	];

	public static function testRF($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		return [
            'type' => Constants::TYPE_STRING,
            'value' => file_get_contents(BASE_PATH.'/ufiles/1/testR.json'),
            'ps' => $position['ps'],
            'pf' => $position['pf'],
            'rows' => $position['rows']
        ];
	}

	public static function getCoefuserEnemat($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
	            $uidOrgan=$paramsFunc[0]['value'];
	            $uidScen=$paramsFunc[1]['value'];	            
	            $date=date_create_from_format('d/m/Y',$paramsFunc[2]['value'])->format('Y/m/d H:i:s');
	            $flagFizUR=$paramsFunc[3]['value'];

	            $uidUser=$paramsFunc[4]['value'];
	            $_specModel=new Specialenemat();	           
	            $dataRes=$_specModel->getCoefficent($uidUser,$uidOrgan,$uidScen,$date,$flagFizUR);

	            $result=isset($dataRes['result'])? $dataRes['result']:0;

	            if($result==1)
	            {
	                return [
	                    'type' => Constants::TYPE_NUMBER,
	                    'value' => $dataRes['data']['coef'],
	                    'ps' => $position['ps'],
	                    'pf' => $position['pf'],
	                    'rows' => $position['rows']
	                ];
	            }	                        
			}
		}
		return Helper::makeNullVariable([
			'ps' => $position['ps'],
			'pf' => $position['pf'],
			'rows' => $position['rows']
		]);	
	}

	public static function getDatasocieteinfocom($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$loader = new \Phalcon\Loader();
		        $loader->registerClasses([
					'Datasocieteinfocom' => APP_PATH . '/library/projFR/Datasocieteinfocom.php'
				]);
		        $loader->register();
		        $code = preg_replace("/[^0-9]/", '', $paramsFunc[0]['value']);
		        $len = strlen($code);
				$answer = [];
		        if ($len == 9 || $len == 14)
		        {
		            $json = \Datasocieteinfocom::GetInfo($code);
		            $result = json_decode($json, true);
		            if (isset($result['result']))
		            {
		                $answer['Adresse'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['organization']['address']['street'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['Code postale'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['organization']['address']['postal_code'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['Ville'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['organization']['address']['city'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['Raison Socliale'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['organization']['name'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['Capital social MO'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['organization']['capital'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['PRENOM MO'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['contacts']['main_corporate_officier']['firstName'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['NOM MO'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['contacts']['main_corporate_officier']['lastName'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['FONCTION MO'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['result']['contacts']['main_corporate_officier']['role'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		            }
		        }
		        return [
					'type' => Constants::TYPE_ARRAY,
					'value' => $answer,
					'ps' => $position['ps'],
					'pf' => $position['pf'],
					'rows' => $position['rows'],
				];
			}
		}
	}

	public static function getDatainfogreffe($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if (empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$loader = new \Phalcon\Loader();
		        $loader->registerClasses([
              		'Datainfogreffe' => APP_PATH . '/library/projFR/Datainfogreffe.php'
            	]);
		        $loader->register();
		        $code = preg_replace("/[^0-9]/", '', $paramsFunc[0]['value']);
		        $len = strlen($code);
				$answer = [];
		        if ($len == 9 || $len == 14)
		        {
		        	$code = substr($code, 0, 9);
		            $json = \Datainfogreffe::GetInfo($code);
		            $result = json_decode($json, true);
		            if (isset($result['Data']))
		            {
		                $answer['Adresse'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['Data']['Adresse']['AdresseConcat'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['Code postale'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['Data']['Adresse']['CodePostal'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['Ville'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['Data']['Adresse']['BureauDistributeur'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		                $answer['Raison Socliale'] = [
		                	'type' => Constants::TYPE_STRING,
		                	'value' => $result['Data']['Denomination'],
		                	'ps' => $position['ps'],
		                	'pf' => $position['pf'],
		                	'rows' => $position['rows'],
		                ];
		            }
		        }
		        return [
					'type' => Constants::TYPE_ARRAY,
					'value' => $answer,
					'ps' => $position['ps'],
					'pf' => $position['pf'],
					'rows' => $position['rows'],
				];
			}
		}
	}
}