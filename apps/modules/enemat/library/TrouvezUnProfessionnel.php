<?php

namespace Dzeta\Modules\Enemat\Library;

use Dzeta\Helpers\CurlHelper;

class TrouvezUnProfessionnel
{
	const BASE_URL = 'https://www.faire.gouv.fr/api/rge';
	const SUGGEST_URL = '/suggest/';
	const COMPANY_URL = '/company';
	const CURL_OPTIONS = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    ];

    public static function getDomaines(string $code, ?string $date = null) {
    	$domaines = [];
        $siret = preg_replace("/[^0-9]/", '', $code);
    	$suggest = self::getSuggestion($siret);
    	if (!empty($suggest)) {
            $request = [
                'siret' => $suggest
            ];
            if (!empty($date)) {
                $request['date'] = $date;
            }
    		$curl = new CurlHelper(
	            self::BASE_URL . self::COMPANY_URL,
	            'POST',
	            $request,
	            [
	            	'content-type' => 'application/json'
	            ],
	            self::CURL_OPTIONS,
	        );
	        $result = $curl->json();
	        if (!empty($result['Company'][0]['domaines'])) {
                foreach ($result['Company'][0]['domaines'] as $domain) {
                    if (isset($domain['nom'])) {
                        $domaines[] = $domain['nom'];
                    } else if (isset($domain['name'])) {
                        $domaines[] = $domain['name'];
                    }
                }
	        }
    	}
    	return $domaines;
    }

    public static function getSuggestion(string $code) {
    	$data = null;
        $curl = new CurlHelper(
            self::BASE_URL . self::SUGGEST_URL . $code,
            'GET',
            [],
            [],
            self::CURL_OPTIONS,
        );
        $result = $curl->json();
        if (!empty($result)) {
            $keys = array_keys($result);
            if (!empty($keys[0])) {
            	$data = $keys[0];
            }
        }
        return $data;
    }
}