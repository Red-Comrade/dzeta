<?php

namespace Dzeta\Modules\Enemat\Library;

use Dzeta\Helpers\CurlHelper;

class ProfessionnelsRge
{
    const API_URL = 'https://koumoul.com/s/data-fair/api/v1/datasets/liste-des-entreprises-rge/lines?';
    const CURL_OPTIONS = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    ];

    public static function getLines($siret, $all = false) {
        $data = [];
        $siret = preg_replace("/[^0-9]/", '', $siret);
        $curl = new CurlHelper(
            self::API_URL,
            'GET',
            [
                'format' => 'json',
                'q' => $siret,
            ],
            [],
            self::CURL_OPTIONS,
        );
        $result = $curl->json();
        if (!empty($result)) {
            if ($all) {
                $data = isset($result['results']) ? $result['results'] : $data;
            } else {
                $data = isset($result['results'][0]) ? $result['results'][0] : $data;
            }
        }
        return $data;
    }
}