<?php

namespace Dzeta\Modules\Enemat\Library;

use KubAT\PhpSimple\HtmlDomParser;

use Dzeta\Helpers\CurlHelper;

class AnnuaireEntreprises {
    const SIREN_URL = 'https://annuaire-entreprises.data.gouv.fr/entreprise/';
    const SIRET_URL = 'https://annuaire-entreprises.data.gouv.fr/etablissement/';
    const CURL_OPTIONS = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => 0,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    ];

    public static function GetInfo($code) {
        $code = preg_replace("/[^0-9]/", '', $code);
        $url = self::SIREN_URL . $code;
        $curl = new CurlHelper(
            $url,
            'GET',
            [],
            [],
            self::CURL_OPTIONS,
        );
        $html = $curl->raw();
        $data = [];
        $dom = HtmlDomParser::str_get_html($html);
        $result = [];
        $address = null;
        $parsed = [];
        if ($dom) {
            $subjectTable = $dom->find('#entreprise table tbody', 0);
            if ($subjectTable) {
                $tr = $subjectTable->find('tr', 0);
                if ($tr) {
                    $td = $tr->find('td', 1);
                    if ($td) {
                        $span = $td->find('span', 0);
                        if ($span) {
                            $result['denomination'] = $span->plaintext;
                        }
                    }
                }
            }
            $addressTable = $dom->find('#contact table tbody', 0);
            if ($addressTable) {
                $tr = $addressTable->find('tr', 0);
                if ($tr) {
                    $td = $tr->find('td', 1);
                    if ($td) {
                        $span = $td->find('span', 0);
                        if ($span) {
                            $address = trim($span->plaintext);
                            if (!empty($address)) {
                                $parsed = self::parseAddress($address);
                            }
                        }
                    }
                }
            }
        }
      	$data['Data'] = [
      		'Adresse' => [
      			'AdresseConcat' => !empty($address) ? $parsed['house_street'] : null,
      			'CodePostal' => !empty($address) ? $parsed['postal_code'] : null,
      			'BureauDistributeur' => !empty($address) ? $parsed['city'] : null,
      		],
      		'Denomination' => !empty($result['denomination']) ? $result['denomination'] : null,
            'Etat' => null,
      	];
        return $data;
    }

    public static function GetInfoBySiret($code)
    {
        $code = preg_replace("/[^0-9]/", '', $code);
        $url = self::SIRET_URL . $code;
        $curl = new CurlHelper(
            $url,
            'GET',
            [],
            [],
            self::CURL_OPTIONS,
        );
        $html = $curl->raw();
        $data = [];
        $dom = HtmlDomParser::str_get_html($html);
        $result = [];
        $address = null;
        $parsed = [];
        if ($dom) {
            $subtitle = $dom->find('main.fr-container .sub-title', 0);
            if ($subtitle) {
                $h2 = $subtitle->find('h2', 0);
                if ($h2) {
                    $a = $h2->find('a', 0);
                    if ($a) {
                        $result['denomination'] = $a->plaintext;
                    }
                }
                $span = $subtitle->find('span.tag', 0);
                if ($span) {
                    $status = trim($span->plaintext);
                    if ($status == 'fermé') {
                        $result['status'] = 'F';
                    } else {
                        $result['status'] = 'A';
                    }
                }
            }
            $addressTable = $dom->find('#contact table tbody', 0);
            if ($addressTable) {
                $tr = $addressTable->find('tr', 0);
                if ($tr) {
                    $td = $tr->find('td', 1);
                    if ($td) {
                        $span = $td->find('span', 0);
                        if ($span) {
                            $address = trim($span->plaintext);
                            if (!empty($address)) {
                                $parsed = self::parseAddress($address);
                            }
                        }
                    }
                }
            }
        }
        $data['Data'] = [
            'Adresse' => [
                'AdresseConcat' => !empty($address) ? $parsed['house_street'] : null,
                'CodePostal' => !empty($address) ? $parsed['postal_code'] : null,
                'BureauDistributeur' => !empty($address) ? $parsed['city'] : null,
            ],
            'Denomination' => !empty($result['denomination']) ? $result['denomination'] : null,
            'Etat' => !empty($result['status']) ? $result['status'] : null,
        ];
        return $data;
    }

    private static function parseAddress($address)
    {
        $result = ['house_street' => null, 'postal_code' => null, 'city' => null];
        $split = preg_split("/(\d{2} \d{3})/", $address, -1, PREG_SPLIT_DELIM_CAPTURE);
        if (count($split) == 3) {
          $result['house_street'] = trim($split[0], " ,");
          $result['postal_code'] = str_replace(' ', '', $split[1]);
          $result['city'] = $split[2];
        }
        return $result;
    }
}