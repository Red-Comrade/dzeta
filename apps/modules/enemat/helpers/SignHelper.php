<?php

namespace Dzeta\Modules\Enemat\Helpers;

use setasign\Fpdi\Fpdi;

use Phalcon\Config\Adapter\Json;

use Dzeta\Core;
use Dzeta\Models\Obj;
use Dzeta\Core\User\User;

use Dzeta\Helpers\CurlHelper;
use Dzeta\Library\SignDoc;
use Dzeta\Library\SignDoc\Yousign;
use Dzeta\Library\SignDoc\Docage;

class SignHelper
{
    const MM_IN_POINT = 0.352777778;
    const YOUSIGN_SERVICE = 'yousign';
    const DOCAGE_SERVICE = 'docage';

	public static function signDocument($id, $type, $url, $files, $loggedUser) {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/SignDocConfig.json');
        $result = ['result' => -1, 'data' => [], 'errors' => []];
        $serviceName = isset($config['service']) ? $config['service'] : self::YOUSIGN_SERVICE;
        $hook = $url . $config['hook'] . '/' . $config['hook_token'] . '/' . $serviceName . '|' . $type;
        $service = null;
        $serviceConfig = [];
        switch ($serviceName) {
            case self::YOUSIGN_SERVICE:
                $service = new Yousign($config['yousign_api_key']);
                $serviceConfig = [
                    'email' => [
                        'member.started' => [
                            [
                                'subject' => 'Hey! You are invited to sign!',
                                'message' => 'Hello <tag data-tag-type="string" data-tag-name="recipient.firstname"></tag> <tag data-tag-type="string" data-tag-name="recipient.lastname"></tag>, <br><br> You have ben invited to sign a document, please click on the following button to read it => <tag data-tag-type="button" data-tag-name="url" data-tag-title="Access to documents">Access to documents</tag>',
                                'to' => ['@member']
                            ]
                        ],
                    ],
                    'webhook' => [
                        'procedure.finished' => [
                            [
                                'url' => $hook,
                                'method' => 'POST',
                                'headers' => [
                                    'X-Doc-Type' => $type
                                ]
                            ]
                        ]
                    ]
                ];
                break;
            case self::DOCAGE_SERVICE:
                $service = new Docage($config['docage_api_key'], $config['docage_email']);
                $serviceConfig = [
                    'hook' => $hook,
                ];
                break;
        }
        if (!empty($service)) {
            $checkStamp = true;
            if (!empty($files)) {
                $stampFile = $files[0];
                $types = explode('/', $stampFile->getRealType());
                if ($types[0] != 'image') {
                    $checkStamp = false;
                    $result['errors'][] = 'Wrong format of stamp image';
                }
            }
            if (!empty($id) && !empty($type) && $checkStamp) {
                $_obj = new Obj();
                $class = new Core\Type\Type($config['types'][$type]['class'], '');
                $obj = new Core\Instance($id, '', $class);
                if ($type == 'devis') {
                    $devisObj = $obj;
                    $_obj->getAllValues([$obj]);
                } else {
                    $_obj->getParent($obj);
                    $children = $_obj->getChildrenByType($obj->getParent(), new Core\Type\Type($config['types']['devis']['class'], ''));
                    if (count($children) == 1) {
                        $devisObj = $children[0];
                        $_obj->getAllValues([$obj, $devisObj]);
                    }
                }

                if (!empty($devisObj)) {
                    $email = $devisObj->getAttribute($config['types']['devis']['attrs']['email'])->getValue();
                    $phone = $devisObj->getAttribute($config['types']['devis']['attrs']['phone'])->getValue();
                    $name = $devisObj->getAttribute($config['types']['devis']['attrs']['name'])->getValue();
                    $surname = $devisObj->getAttribute($config['types']['devis']['attrs']['surname'])->getValue();
                    $file = $obj->getAttribute($config['types'][$type]['attrs']['file'])->getValue();

                    $userCheck = true;
                    if ($type == 'AH') {
                        $userCheck = false;
                        $user = new Core\Instance($loggedUser->getUid(), '', new Core\Type\Type(Core\User\User::USER_TYPE_UID, ''));
                        $_obj->getAllValues([$user]);
                        $userEmail = $user->getAttribute($config['user']['attrs']['email'])->getValue();
                        $userPhone = $user->getAttribute($config['user']['attrs']['phone'])->getValue();
                        $userName = $user->getAttribute($config['user']['attrs']['name'])->getValue();
                        $userSurname = $user->getAttribute($config['user']['attrs']['surname'])->getValue();
                        $userStamp = $user->getAttribute($config['user']['attrs']['stamp'])->getValue();
                        if (!empty($userEmail) && !empty($userName) && !empty($userSurname) && !empty($userPhone)) {
                            $userCheck = true;
                        }
                    }

                    if (!empty($email) && !empty($name) && !empty($surname) && !empty($file) && !empty($phone) && $userCheck) {

                        $creationDate = $obj->getAttribute($config['types'][$type]['attrs']['date_creation'])->getValue()->getValue();
                        $title = "Sign $type $creationDate";
                        $description = $title . ' by ' . $name->getValue() . ' ' . $surname->getValue();

                        $procedure = $service->createProcedure($title, $description, $serviceConfig);
                        $options = $procedure->getOptions();
                        if (!isset($options['detail'])) {
                            $typeSearch = $config['types'][$type]['search']->toArray();
                            $userSearch = $config['user']['search']->toArray();
                            $datesSearch = $config['types'][$type]['date']['search']->toArray();
                            $search = array_merge($typeSearch, $userSearch, $datesSearch);
                            $clientSignsCoordinates = [];
                            $userSignsCoordinates = [];
                            $datesCoordinates = [];
                            $results = self::callGetTextPositionsFromPdf($config, $file->getPath(), $search);
                            foreach ($typeSearch as $value) {
                                if (isset($results[$value])) {
                                    foreach ($results[$value]['positions'] as $position) {
                                        $coordinates = [
                                            'x' => round($position['x'], 0),
                                            'y' => round($position['y'], 0),
                                        ];
                                        $signSizes = $config['types'][$type]['sizes']['sign'];
                                        $x_start_sign = $coordinates['x'];
                                        $y_start_sign = $coordinates['y'] - $signSizes['y_start'];
                                        $y_start_sign = $y_start_sign < 0 ? 10 : $y_start_sign;
                                        $x_end_sign = $coordinates['x'] + $signSizes['x_end'];
                                        $x_end_sign = $x_end_sign < 0 ? 10 : $x_end_sign;
                                        $y_end_sign = $coordinates['y'] - $signSizes['y_end'];
                                        $y_end_sign = $y_end_sign < 0 ? 10 : $y_end_sign;
                                        $stampSizes = $config['types'][$type]['sizes']['stamp'];
                                        $x_start_stamp = $coordinates['x'];
                                        $y_start_stamp = $coordinates['y'] - $stampSizes['y_start'];
                                        $y_start_stamp = $y_start_stamp < 0 ? 10 : $y_start_stamp;
                                        $x_end_stamp = $coordinates['x'] + $stampSizes['x_end'];
                                        $x_end_stamp = $x_end_stamp < 0 ? 10 : $x_end_stamp;
                                        $y_end_stamp = $coordinates['y'] - $stampSizes['y_end'];
                                        $y_end_stamp = $y_end_stamp < 0 ? 10 : $y_end_stamp;
                                        $clientSignsCoordinates[$position['page']][] = [
                                            'x_start_sign' => $x_start_sign,
                                            'y_start_sign' => $y_start_sign,
                                            'x_end_sign' => $x_end_sign,
                                            'y_end_sign' => $y_end_sign,
                                            'x_start_stamp' => $x_start_stamp,
                                            'y_start_stamp' => $y_start_stamp,
                                            'x_end_stamp' => $x_end_stamp,
                                            'y_end_stamp' => $y_end_stamp,
                                        ];
                                    }
                                }
                            }
                            foreach ($userSearch as $value) {
                                if (isset($results[$value])) {
                                    foreach ($results[$value]['positions'] as $position) {
                                        $coordinates = [
                                            'x' => round($position['x'], 0),
                                            'y' => round($position['y'], 0),
                                        ];
                                        $signSizes = $config['user']['sizes']['sign'];
                                        $x_start_sign = $coordinates['x'];
                                        $y_start_sign = $coordinates['y'] - $signSizes['y_start'];
                                        $y_start_sign = $y_start_sign < 0 ? 10 : $y_start_sign;
                                        $x_end_sign = $coordinates['x'] + $signSizes['x_end'];
                                        $x_end_sign = $x_end_sign < 0 ? 10 : $x_end_sign;
                                        $y_end_sign = $coordinates['y'] - $signSizes['y_end'];
                                        $y_end_sign = $y_end_sign < 0 ? 10 : $y_end_sign;
                                        $stampSizes = $config['user']['sizes']['stamp'];
                                        $x_start_stamp = $coordinates['x'];
                                        $y_start_stamp = $coordinates['y'] - $stampSizes['y_start'];
                                        $y_start_stamp = $y_start_stamp < 0 ? 10 : $y_start_stamp;
                                        $x_end_stamp = $coordinates['x'] + $stampSizes['x_end'];
                                        $x_end_stamp = $x_end_stamp < 0 ? 10 : $x_end_stamp;
                                        $y_end_stamp = $coordinates['y'] - $stampSizes['y_end'];
                                        $y_end_stamp = $y_end_stamp < 0 ? 10 : $y_end_stamp;
                                        $userSignsCoordinates[$position['page']][] = [
                                            'x_start_sign' => $x_start_sign,
                                            'y_start_sign' => $y_start_sign,
                                            'x_end_sign' => $x_end_sign,
                                            'y_end_sign' => $y_end_sign,
                                            'x_start_stamp' => $x_start_stamp,
                                            'y_start_stamp' => $y_start_stamp,
                                            'x_end_stamp' => $x_end_stamp,
                                            'y_end_stamp' => $y_end_stamp,
                                        ];
                                    }
                                }
                            }
                            foreach ($datesSearch as $value) {
                                if (isset($results[$value])) {
                                    foreach ($results[$value]['positions'] as $position) {
                                        $coordinates = [
                                            'x' => round($position['x'], 0),
                                            'y' => round($position['y'], 0),
                                            'text' => $value,
                                        ];
                                        $datesCoordinates[$position['page']][] = $coordinates;
                                    }
                                }
                            }

                            $fpdf = new Fpdi();
                            $tempfile = tempnam(sys_get_temp_dir(), 'signdoc-enemat');
                            if (!empty($userStamp)) {
                              $userTempStamp = tempnam(sys_get_temp_dir(), 'signdoc-enemat-stamp');
                              rename($userTempStamp, $userTempStamp .= '.' . $userStamp->getName());
                              file_put_contents($userTempStamp, file_get_contents($userStamp->getPath()));
                            }
                            if (!empty($stampFile)) {
                              $clientTempStamp = tempnam(sys_get_temp_dir(), 'signdoc-enemat-stamp');
                              rename($clientTempStamp, $clientTempStamp .= '.' . $stampFile->getExtension());
                              file_put_contents($clientTempStamp, file_get_contents($stampFile->getTempName()));
                            }
                            $pagesCount = $fpdf->setSourceFile($file->getPath());
                            for ($i = 1; $i <= $pagesCount; $i++) {
                                $fpdf->AddPage();
                                $template = $fpdf->importPage($i);
                                $fpdf->useTemplate($template);
                                $pageHeight = $fpdf->GetPageHeight();
                                if (isset($clientSignsCoordinates[$i]) && !empty($clientTempStamp)) {
                                    foreach ($clientSignsCoordinates[$i] as $coordinates) {
                                        $x = $coordinates['x_start_stamp'] * self::MM_IN_POINT;
                                        $width = abs(($coordinates['x_end_stamp'] * self::MM_IN_POINT) - $x);
                                        if ($config['types'][$type]['sizes']['stamp']['y_start'] < 0) {
                                            $y = $coordinates['y_start_stamp'] * self::MM_IN_POINT;
                                            $height = abs($y - ($coordinates['y_end_stamp'] * self::MM_IN_POINT));
                                        } else {
                                            $y = $coordinates['y_end_stamp'] * self::MM_IN_POINT;
                                            $height = abs($y - ($coordinates['y_start_stamp'] * self::MM_IN_POINT));
                                        }
                                        $fpdf->Image($clientTempStamp, $x, $pageHeight - $y, 0, $height);
                                    }
                                }
                                if (isset($userSignsCoordinates[$i]) && !empty($userTempStamp)) {
                                    foreach ($userSignsCoordinates[$i] as $coordinates) {
                                        $x = $coordinates['x_start_stamp'] * self::MM_IN_POINT;
                                        $width = abs(($coordinates['x_end_stamp'] * self::MM_IN_POINT) - $x);
                                        if ($config['types'][$type]['sizes']['stamp']['y_start'] < 0) {
                                            $y = $coordinates['y_start_stamp'] * self::MM_IN_POINT;
                                            $height = abs($y - ($coordinates['y_end_stamp'] * self::MM_IN_POINT));
                                        } else {
                                            $y = $coordinates['y_end_stamp'] * self::MM_IN_POINT;
                                            $height = abs($y - ($coordinates['y_start_stamp'] * self::MM_IN_POINT));
                                        }
                                        $fpdf->Image($userTempStamp, $x, $pageHeight - $y, 0, $height);
                                    }
                                }
                                if (isset($datesCoordinates[$i])) {
                                    foreach ($datesCoordinates[$i] as $coordinates) {
                                        $date = new \DateTime('NOW');
                                        $beforeSymbolsCount = strpos($coordinates['text'], $config['types'][$type]['date']['search'][0]);
                                        $x = ($coordinates['x'] + $beforeSymbolsCount * $config['types'][$type]['date']['x_shift']) * self::MM_IN_POINT;
                                        $y = ($coordinates['y'] + $config['types'][$type]['date']['y_shift']) * self::MM_IN_POINT;
                                        $fpdf->SetFont(
                                            $config['types'][$type]['date']['font']['family'],
                                            $config['types'][$type]['date']['font']['style'],
                                            $config['types'][$type]['date']['font']['size']
                                        );
                                        $fpdf->Text($x, $pageHeight - $y, $date->format($config['types'][$type]['date']['format']));
                                    }
                                }
                            }
                            $fpdf->Output($tempfile, "F");

                            $procedureFile = $service->addFileToProcedure($procedure, $tempfile, $file->getName());
                            $options = $procedureFile->getOptions();
                            if (!isset($options['detail'])) {
                                $member = $service->addExternalMemberToProcedure($procedure, trim($name->getValue()), trim($surname->getValue()), $phone->getValue(), $email->getValue());
                                $options = $member->getOptions();
                                if (!isset($options['detail'])) {
                                    foreach ($clientSignsCoordinates as $key => $signCoordinates) {
                                        foreach ($signCoordinates as $coordinates) {
                                            if ($serviceName == self::DOCAGE_SERVICE) {
                                                $coordinates['x_end_sign'] = $coordinates['x_end_sign'] - $coordinates['x_start_sign'];
                                                $coordinates['y_end_sign'] = $coordinates['y_end_sign'] - $coordinates['y_start_sign'];
                                            }
                                            $fileObject = $service->addMemberFileObject(
                                                $procedureFile,
                                                $member,
                                                "{$coordinates['x_start_sign']},{$coordinates['y_start_sign']},{$coordinates['x_end_sign']},{$coordinates['y_end_sign']}",
                                                $key,
                                                [
                                                    'mention' => 'Read and approved',
                                                    'mention2' => 'Signed by ' . $name->getValue() . ' ' . $surname->getValue(),
                                                    'reason' => 'Signed by ' . $name->getValue() . ' ' . $surname->getValue() . ' (Yousign)'
                                                ]
                                            );

                                            $options = $fileObject->getOptions();
                                            if (isset($options['detail'])) {
                                                break 2;
                                            }
                                        }
                                    }
                                    $options = !empty($fileObject) ? $fileObject->getOptions() : [];
                                    if (!isset($options['detail'])) {
                                        if ($type == 'AH') {
                                            $userMember = $service->addExternalMemberToProcedure($procedure, $userName->getValue(), $userSurname->getValue(), $userPhone->getValue(), $userEmail->getValue());
                                            $options = $userMember->getOptions();
                                            if (!isset($options['detail'])) {
                                                foreach ($userSignsCoordinates as $key => $signCoordinates) {
                                                    foreach ($signCoordinates as $coordinates) {
                                                        if ($serviceName == self::DOCAGE_SERVICE) {
                                                            $coordinates['x_end_sign'] = $coordinates['x_end_sign'] - $coordinates['x_start_sign'];
                                                            $coordinates['y_end_sign'] = $coordinates['y_end_sign'] - $coordinates['y_start_sign'];
                                                        }
                                                        $userFileObject = $service->addMemberFileObject(
                                                            $procedureFile,
                                                            $userMember,
                                                            "{$coordinates['x_start_sign']},{$coordinates['y_start_sign']},{$coordinates['x_end_sign']},{$coordinates['y_end_sign']}",
                                                            $key,
                                                            [
                                                                'mention' => 'Read and approved',
                                                                'mention2' => 'Signed by ' . $userName->getValue() . ' ' . $userSurname->getValue(),
                                                                'reason' => 'Signed by ' . $userName->getValue() . ' ' . $userSurname->getValue() . ' (Yousign)'
                                                            ]
                                                        );

                                                        $options = $userFileObject->getOptions();
                                                        if (isset($options['detail'])) {
                                                            break 2;
                                                        }
                                                    }
                                                }
                                                $options = !empty($userFileObject) ? $userFileObject->getOptions() : [];
                                                if (!isset($options['detail'])) {
                                                    $p = $service->startProcedure($procedure);
                                                    $options = $p->getOptions();
                                                    if (!isset($options['detail'])) {
                                                        $proc = $obj->getAttribute($config['types'][$type]['attrs']['procedure']);
                                                        if (!empty($proc)) {
                                                            $status = $obj->getAttribute($config['types'][$type]['attrs']['status']);
                                                            if (!empty($status)) {
                                                                $status->setValue(['value' => 'Signing']);
                                                            }
                                                            $proc->setValue(['value' => $procedure->getId()]);
                                                            $_obj->checkUniqueAndUpdateValues($obj, $loggedUser);
                                                        }
                                                        $result['result'] = 1;
                                                    }
                                                }
                                            }
                                        } else {
                                            $p = $service->startProcedure($procedure);
                                            $options = $p->getOptions();
                                            if (!isset($options['detail'])) {
                                                $proc = $obj->getAttribute($config['types'][$type]['attrs']['procedure']);
                                                if (!empty($proc)) {
                                                    $proc->setValue(['value' => $procedure->getId()]);
                                                    $status = $obj->getAttribute($config['types'][$type]['attrs']['status']);
                                                    if (!empty($status)) {
                                                        $status->setValue(['value' => 'Signing']);
                                                    }
                                                    $_obj->checkUniqueAndUpdateValues($obj, $loggedUser);
                                                }
                                                $result['result'] = 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (isset($options['detail'])) {
                            $result['errors'][] = htmlentities($options['detail']);
                        }
                    } else {
                        $result['errors'][] = 'Not all required attributes are filled';
                    }
                }
            }
        } else {
            $result['errors'][] = 'Wrong signing service';
        }
		return $result;
	}

    public static function getDocument($type, $serviceName, $result) {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/SignDocConfig.json');
        $service = null;
        $procedureId = null;
        $procedureFile = null;
        $date = null;
        switch ($serviceName) {
            case self::YOUSIGN_SERVICE:
                $service = new Yousign($config['yousign_api_key']);
                $procedureId = '/procedures/' . $result['procedure']['id'];
                $procedureFile = SignDoc\Core\File::fromYousignRequest($result['procedure']['files'][0]);
                $date = \DateTime::createFromFormat(DATE_ATOM, $result['procedure']['updatedAt']);
                break;
            case self::DOCAGE_SERVICE:
                $service = new Docage($config['docage_api_key'], $config['docage_email']);
                if ($result['Status'] == Docage::SIGNED_STATUS) {
                    $procedureId = $result['Id'];
                    $procedure = SignDoc\Core\Procedure::fromDocageRequest($procedureId);
                    $files = $service->getFiles($procedure);
                    $procedureFile = isset($files[0]) ? $files[0] : null;
                    $date = \DateTime::createFromFormat('Y-m-d\TH:i:s.u+', $result['ModificationDate']);
                }
                break;
        }
        if ($service && $procedureId) {
            $class = new \Dzeta\Core\Type\Type($config['types'][$type]['class'], '');
            $_obj = new Obj();
            $parents = [];
            $search = '[' . $config['types'][$type]['attrs']['procedure'] . ']=#' . $procedureId . '#';
            $objs = $_obj->searchObj($class, $search, $parents, 1);
            if (count($objs['data']) == 1) {
                $_obj->getAllValues($objs['data']);
                $obj = $objs['data'][0];
                $proc = $obj->getAttribute($config['types'][$type]['attrs']['procedure']);
                $file = $obj->getAttribute($config['types'][$type]['attrs']['signed_file']);
                $signDate = $obj->getAttribute($config['types'][$type]['attrs']['date_signature']);
                $status = $obj->getAttribute($config['types'][$type]['attrs']['status']);
                if (!empty($proc) && !empty($file) && !empty($signDate)) {
                    $path = tempnam(sys_get_temp_dir(), 'signdoc-enemat');
                    $url = $service->downloadFile($procedureFile, $path);
                    $fileUid = \Dzeta\Core\Value\File::GenerateUIDFile();
                    if (!is_null($fileUid)) {
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $filetype = finfo_file($finfo, $path);
                        finfo_close($finfo);
                        $realName = $procedureFile->getName();
                        $size = filesize($path);
                        $filePath = BASE_PATH . \Dzeta\Core\Value\File::FILE_FOLDER . $fileUid . \Dzeta\Core\Value\File::FILE_EXT;
                        $move = rename($path, $filePath);
                        if ($move) {
                            $filetype = substr($filetype, 0, strpos($filetype, '/'));
                            $previewUid = \Dzeta\Core\Value\File::GenerateUIDFile();
                            if (!is_null($previewUid)) {
                                if ($filetype == 'image') {
                                    $image = new \Phalcon\Image\Adapter\Imagick($filePath);
                                    $w = $image->getWidth();
                                    $h = $image->getHeight();
                                    $size = 1000;
                                    if ($w <= $size && $h <= $size) {
                                        $previewUid = $fileUid;
                                    } else {
                                        if ($w <= $size && $h > $size || $h > $w) {
                                            $image->resize($w / $h * $size, $size);
                                        } elseif ($w > $size && $h <= $size || $w > $h) {
                                            $image->resize($size, $h / $w * $size);
                                        }
                                        $image->getInternalImInstance()->writeImage(BASE_PATH . \Dzeta\Core\Value\File::FILE_FOLDER . $previewUid . \Dzeta\Core\Value\File::FILE_EXT);
                                    }
                                } else {
                                    $previewUid = null;
                                }
                                $fileValue = new \Dzeta\Core\Value\File($fileUid, $realName, $previewUid, $size);
                                $file->setValueS($fileValue);
                                $signDate->setValue(['value' => $date->format(Core\Value\DateTimeV::DATE_FORMAT)]);
                                if (!empty($status)) {
                                    $status->setValue(['value' => null]);
                                }
                                $user = new User(User::SYSTEM_USER_UID, '');
                                $_obj->checkUniqueAndUpdateValues($obj, $user);
                                $obj->setAttributes([$file]);
                                $_obj->AddFiles($obj, $user);
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getTextPositionsFromPdf($file, $text) {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/SignDocConfig.json');
        $positions = [];
        if (!empty($config['pages4text'])) {
            $pages4text = BASE_PATH . $config['pages4text']['path'];
            $result = exec($pages4text . ' "' . $file . '" ' . '"' . $text . '"');
            $positions = @json_decode($result, true);
            $positions = !empty($positions) ? $positions : [];
        }
        return $positions;
    }

    private static function callGetTextPositionsFromPdf($config, $file, $texts) {
        $curl = new CurlHelper(
            $config['pages4text']['url'] . '/' . $config['hook_token'],
            'POST',
            [
                'file' => base64_encode(file_get_contents($file)),
                'texts' => $texts
            ],
            [],
            [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => false,
            ]
        );
        $result = $curl->json();
        return isset($result['data']) ? $result['data'] : [];
    }
}