<?php

namespace Dzeta\Modules\Enemat\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Specialenemat extends Model
{
	 const PROCEDURES = [
        'getCoefficent' => [
            'name' => 'Enemat_GetCoef',
            'params' => [
                'User'    =>         [ 'type' => DataType::TYPE_STRING ],
                'Organization'  =>   [ 'type' => DataType::TYPE_STRING ],
                'Operation'=>        [ 'type' => DataType::TYPE_STRING ],
                'Date'=>             [ 'type' => DataType::TYPE_STRING ],
                'MoraleOrPhysique'=> [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ]
    ];


    public function getCoefficent($uidUser,$uidOrg,$uidOper,$date,$typeU)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'User'      => $uidUser,
          'Organization'=> $uidOrg,
          'Operation'  => $uidOper,
          'Date'  => $date,
          'MoraleOrPhysique'  => $typeU,
        ]);
        
        if (!empty($data))
        {           
            $result=1;
            $dataResult['result']=1;
            $dataResult['data']=['coef'=>(float)$data[0]['coef']];        
        }
        if ($result!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)]; 
        }
        return $dataResult;
    }
}