<?php
declare(strict_types=1);

namespace Dzeta\Modules\Enemat\Controllers;

use setasign\Fpdi\Fpdi;

use Phalcon\Config\Adapter\Json;

use DzetaCalcParser\DzetaCalc;
use Dzeta\Core;
use Dzeta\Core\Progress;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\Export\ExportView as Export;
use Dzeta\Models\Obj;
use Dzeta\Models\View;
use Dzeta\Models\Files;
use Dzeta\Models\AttrGroup;
use Dzeta\Models\Type;
use Dzeta\Modules\Enemat\Models\Specialenemat;
use Dzeta\Modules\Enemat\Helpers\SignHelper;
use Dzeta\Controllers\ControllerBase;
use Dzeta\Modules\Enemat\Library\ProfessionnelsRge;
use Dzeta\Modules\Enemat\Library\TrouvezUnProfessionnel;
use Dzeta\Modules\Enemat\Library\AnnuaireEntreprises;

class DopController extends ControllerBase
{
    private $CONFIG_SETT=[
      "№devis"=>[
        'obj'=>'066d9258-73c5-4953-b5d1-77d9d2389d75',
        'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'Tax'=>[
        'obj'=>'a103001b-d249-4bb9-94ef-9682295cdddb',
        'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'Sous-traite'=>[
        'obj'=>'1f82df2e-30c3-4b59-8293-908be5e82075',
        'attr'=>'1e73bc31-db9a-4e0e-8a26-659bc7607b84',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'Default-unit'=>[
        'obj'=>'4729181c-b16a-4ce0-a94c-0322e77a2b5f',
        'attr'=>'1e73bc31-db9a-4e0e-8a26-659bc7607b84',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'myPranovLimit'=>[
        'obj'=>'1be5b1f4-ef5d-427c-af62-181fe29639b6',
        'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'myPranovBlue'=>[
        'obj'=>'0f9fbd61-7b97-4b28-9cdb-0a84fda94be3',
        'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'myPranovJaune'=>[
        'obj'=>'9ba87caf-8ef8-4499-bed5-fd07d6af9cec',
        'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'myPranovViolet'=>[
        'obj'=>'99c658f3-6e53-4d9f-9e16-f74958493e4e',
        'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'myPranovRose'=>[
        'obj'=>'98e81afa-c56f-45fc-8aec-8949c2f08c99',
        'attr'=>'a7bf9b7a-2be3-44c8-a92a-28b17a2e9caa',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ],
      'SUBVENTION MAPRIMERENOV'=>[
        'obj'=>'3b770147-5b10-4df6-b71e-9bc90f2cae1c',
        'attr'=>'51e5edfb-f380-4d33-b392-597ad95042a3',
        'type'=>'07d865ce-a217-46cb-a571-5cf065f572b2'
      ]
    ];

    public function signAction() {
        $result = ['result' => -1, 'data' => [], 'errors' => []];

        if ($this->request->isPost()) {
            $id = $this->request->get('id');
            $type = $this->request->get('type');
            $files = $this->request->getUploadedFiles();
            $domain = $this->request->getServer('SERVER_NAME');
            $isHttps = $this->request->getServer('HTTPS');
            $hook = ($isHttps ? 'https://' : 'http://') . $domain;
            $user = $this->session->get('logged')['User'];
            $result = SignHelper::signDocument($id, $type, $hook, $files, $user);
        }

        echo json_encode($result);
    }

    public function verifyTaxAction() {
        $result = ['result' => -1, 'data' => [], 'errors' => []];

        if ($this->request->isPost()) {
            $Data_tax=$this->request->get('Data');
            foreach ($Data_tax as $data) {
              if (!empty($data['number']) && !empty($data['reference'])) {
                  $loader = new \Phalcon\Loader();
                  $loader->registerClasses([
                    'ImpotsGouvFr' => APP_PATH . '/library/projFR/ImpotsGouvFr.php',
                  ]);
                  $loader->register();
                  $verify = \ImpotsGouvFr::getData($data['number'], $data['reference']);
                  $result['data'][] = $verify;
                  $result['result'] = 1;
              }
            }

        }
        echo json_encode($result);
    }

    public function verifyTaxesAction() {
        $export = new Export();
        if ($this->request->hasFiles() && $this->request->hasPost('settings') && $this->request->hasPost('uid')) {
            $file = $this->request->getUploadedFiles()[0];
            if ($file->getRealType() == ImportController::EXCEL_FILE_TYPE) {
                $uid = $this->request->getPost('uid');
                $settings = $this->request->getPost('settings');
                $params = array_combine(array_column($settings, 'attribute'), array_column($settings, 'column'));

                $import = new Core\Import\Import($file->getTempName(), $settings);
                $progress = new Progress($uid);
                $totalRows = $import->getTotalRows();
                $progress->setCount($totalRows);

                $loader = new \Phalcon\Loader();
                $loader->registerClasses([
                  'ImpotsGouvFr' => APP_PATH . '/library/projFR/ImpotsGouvFr.php',
                ]);
                $loader->register();
                $export->writeHeadersOptions(['string', 'string'], ['suppress_row' => true]);
                $export->writeRow([
                    'Numéro fiscal',
                    'Référence de l\'avis',
                    'Déclarant 1', '', '', '', '',
                    'Déclarant 2', '', '', '', '',
                    'Date de mise en recouvrement de l\'avis d\'impôt',
                    'Date d\'etablissement',
                    'Nombre de part(s)',
                    'Situation de famille',
                    'Nombre de personne(s) à charge',
                    'Revenu brut global',
                    'Revenu imposable',
                    'Impôt sur le revenu net avant corrections',
                    'Montant de l\'impôt',
                    'Revenu fiscal de référence'
                ], true, true, true, true);
                $export->mergeCells(0, 2, 0, 6);
                $export->mergeCells(0, 7, 0, 11);
                $export->writeRow([
                    '',
                    '',
                    'Nom',
                    'Nom de naissance',
                    'Prénom(s)',
                    'Date de naissance',
                    'Adresse déclarée au 1er janvier 2019',
                    'Nom',
                    'Nom de naissance',
                    'Prénom(s)',
                    'Date de naissance',
                    'Adresse déclarée au 1er janvier 2019',
                    '', '', '', '', '', '', '', '', '', ''
                ], true, true, true, true);
                $merge = [0, 1, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21];
                foreach ($merge as $col) {
                    $export->mergeCells(0, $col, 1, $col);
                }
                if (isset($params['Numéro fiscal']) && isset($params['Référence de l\'avis'])) {
                    while (($rows = $import->run()) != []) {
                        foreach ($rows as $values) {
                            if (isset($values[$params['Numéro fiscal']]) && isset($values[$params['Référence de l\'avis']])) {
                                $verify = \ImpotsGouvFr::getData($values[$params['Numéro fiscal']], $values[$params['Référence de l\'avis']]);
                                $export->writeRow([
                                    $values[$params['Numéro fiscal']],
                                    $values[$params['Référence de l\'avis']],
                                    isset($verify['declarant1']['Nom']) ? $verify['declarant1']['Nom'] : '',
                                    isset($verify['declarant1']['Nom de naissance']) ? $verify['declarant1']['Nom de naissance'] : '',
                                    isset($verify['declarant1']['Prenom(s)']) ? $verify['declarant1']['Prenom(s)'] : '',
                                    isset($verify['declarant1']['Date de naissance']) ? $verify['declarant1']['Date de naissance'] : '',
                                    isset($verify['declarant1']['Adresse declaree au 1er janvier 2019']) ? $verify['declarant1']['Adresse declaree au 1er janvier 2019'] : '',
                                    isset($verify['declarant2']['Nom']) ? $verify['declarant2']['Nom'] : '',
                                    isset($verify['declarant2']['Nom de naissance']) ? $verify['declarant2']['Nom de naissance'] : '',
                                    isset($verify['declarant2']['Prenom(s)']) ? $verify['declarant2']['Prenom(s)'] : '',
                                    isset($verify['declarant2']['Date de naissance']) ? $verify['declarant2']['Date de naissance'] : '',
                                    isset($verify['declarant2']['Adresse declaree au 1er janvier 2019']) ? $verify['declarant2']['Adresse declaree au 1er janvier 2019'] : '',
                                    isset($verify['common']['Date de mise en recouvrement de l\'avis d\'impot']) ? $verify['common']['Date de mise en recouvrement de l\'avis d\'impot'] : '',
                                    isset($verify['common']['Date d\'etablissement']) ? $verify['common']['Date d\'etablissement'] : '',
                                    isset($verify['common']['Nombre de part(s)']) ? $verify['common']['Nombre de part(s)'] : '',
                                    isset($verify['common']['Situation de famille']) ? $verify['common']['Situation de famille'] : '',
                                    isset($verify['common']['Nombre de personne(s) a charge']) ? $verify['common']['Nombre de personne(s) a charge'] : '',
                                    isset($verify['common']['Revenu brut global']) ? $verify['common']['Revenu brut global'] : '',
                                    isset($verify['common']['Revenu imposable']) ? $verify['common']['Revenu imposable'] : '',
                                    isset($verify['common']['Impot sur le revenu net avant corrections']) ? $verify['common']['Impot sur le revenu net avant corrections'] : '',
                                    isset($verify['common']['Montant de l\'impot']) ? $verify['common']['Montant de l\'impot'] : '',
                                    isset($verify['common']['Revenu fiscal de reference']) ? $verify['common']['Revenu fiscal de reference'] : '',
                                ], false, false, true, true);
                            }
                            $progress->increaseCurrent();
                        }
                    }
                }
            }
        }
        $response = $this->response;
        $response->setHeader("Content-Type", 'application/vnd.ms-excel');
        $response->setHeader("Content-Disposition", 'attachment; filename="export.xlsx"');
        readfile($export->save());
    }

    public function territoryCodeAction() {
        $result = ['result' => -1, 'data' => [], 'errors' => []];

        if ($this->request->isGet()) {
            $address = $this->request->get('address');
            $loader = new \Phalcon\Loader();
            $loader->registerClasses([
                'SigVilleGouvFr' => APP_PATH . '/library/projFR/SigVilleGouvFr.php',
            ]);
            $loader->register();
            $code = \SigVilleGouvFr::getCode($address);
            $result['data']['code'] = $code;
            $result['result'] = 1;
        }
        echo json_encode($result);
    }

    public function getQualificationAction()
    {
      $result = ['result' => -1, 'data' => [], 'errors' => []];
      if ($this->request->isGet())
      {
        $siret = $this->request->get('siret');
        if($siret==null)
        {
          $_obj= new Obj();
          $dataUser=$_obj->getAllValues([$this->session->get('logged')['User']])[$this->session->get('logged')['User']->getUid()];
          foreach ($dataUser as $key => $value) 
          {
            switch ($value->getUid()) 
            {
              case '8d2658e0-533a-4e1d-a065-ccaaf5b30b43':
                if(!is_null($value->getValue()))
                {
                    $siret=$value->getValue()->getValue();
                }                
                break;              
            }
          }
        }
        $date = $this->request->get('date', null, null);
        $domaines = TrouvezUnProfessionnel::getDomaines($siret, $date);
        $result['data']['qualifications'] = $domaines;
        if (empty($domaines)) {
          $lines = ProfessionnelsRge::getLines($siret, true);
          $result['data']['qualifications'] = array_column($lines, 'DOMAINE_TRAVAUX');
        }
        $result['result'] = 1;
      }
      echo json_encode($result);
    }

    public function stepFormGetDataJson()
    {
     // echo json_encode($_REQUEST);
     // return;
         $readSettFile=true;
          $pathFileSetting=BASE_PATH.'/config/dop/DataEnemat/settEnemat.json';
          $dataSett=[];
             if(file_exists($pathFileSetting))
             {
                $fileSetting=file_get_contents($pathFileSetting);
                $dataSett=json_decode($fileSetting,true);
              }
        //getAllValues(array $objs)
        $userID=$this->session->get('logged')['User'];
        $obj = new \Dzeta\Core\Instance($userID->getUid(), '');
        $_obj = new Obj();
        $data = $_obj->getAllValues([$obj])[$userID->getUid()];
        $dataUserValue=$data;
        $view = new Core\App\View('6f6e2d17-9a9e-47e1-beea-9af82e8ee975', '', []);
          $view->setDopSett('attrSort', new Core\Type\Attribute('e6141b74-7a8d-4dc5-99ac-e5f3c672550e', '', 1, false, false));
          $view->setDopSett('sort', 1);
          $view->setDopSett('num', 100000);
          $view->setDopSett('start', 1);
          $_view=new View();
          $dt=$_view->View_GetData($view,$this->session->get('logged')['User']->getUid());
          $dataInt=[];
          if(!empty($dt[0])&&!empty($dt[0]['dt']))
          {

            $dataInt=json_decode($dt[0]['dt'],true);
          }

          $dtatCoefIntr=[];

          foreach ($dataInt as $key => $value) {

            $dtatCoefIntr[$value['e6141b74-7a8d-4dc5-99ac-e5f3c672550e']['v']]=[
              'AE'=>$value['f758f57d-f5be-47c2-b78d-1cf8ae6ba442']['v'],
              'AD'=>$value['3e104fc9-e8e7-4d5d-b4e5-7894b62e79ac']['v']
            ];
          }

        $dataUserr=[
          'Login'=>null,
          'RAISON SOCIALE MO'=>null,
          'SIRET MO'=>null,
          'ADRESSE MO'=>null,
          'CP MO'=>null,
          'VILLE MO'=>null,
          'Tel MO'=>null,
          'MOBILE MO'=>null,
          'EMAIL MO'=>null,
          'NOM MO'=>null,
          'PRENOM MO'=>null,
          'FONCTION MO'=>null,
          'LOGO'=>null,
          'Capital social MO'=>null,
          'COEF'=>null,
          'Logo upload'=>null,
          'site internet'=>null,

        ];
        foreach ($data as $key => $value)
        {
          switch ($value->getUid())
              {
                case '106fc108-41d1-4474-9fe4-04b9536e8bc4':
                    $dataUserr['Login'] = $value;
                    $dataUserr['EMAIL MO'] = $value;
                break;
                case 'e845ff95-991e-4be3-817a-9058cdd5fcda':
                    $dataUserr['RAISON SOCIALE MO'] = $value;
                break;
                case '8d2658e0-533a-4e1d-a065-ccaaf5b30b43':
                    $dataUserr['SIRET MO'] = $value;
                break;
                case '04dc8310-1f6e-4199-a6b5-f2c0838363f6':
                    $dataUserr['ADRESSE MO'] = $value;
                break;
                case '9c1170a6-f0a9-49d0-92db-055f318046d0':
                    $dataUserr['CP MO'] = $value;
                break;
                case 'a94a5300-6f7e-4265-8191-f0565be63e7a':
                    $dataUserr['VILLE MO'] = $value;
                break;
                case 'a477fccb-de27-45c3-8761-594636e049d6':
                    $dataUserr['Tel MO'] = $value;
                break;
                case 'e19311fe-79d2-4e90-9e60-41eacd59f1cf':
                    $dataUserr['MOBILE MO'] = $value;
                break;
              /*  case '20926719-6376-4ed7-9ef3-a2ed4юc03d94d':
                    $dataUserr['EMAIL MO'] = $value;
                break;*/
                case '31ac52ec-739d-444f-a35d-bcf888c9e7ad':
                    $dataUserr['NOM MO'] = $value;
                break;
                case 'd45023e2-8e6b-4693-a6c3-8368f526c88d':
                    $dataUserr['PRENOM MO'] = $value;
                break;
                case '848f8242-d836-442f-845f-a324af4d198b':
                    $dataUserr['FONCTION MO'] = $value;
                break;
                case '6e8c9fcd-1d93-4395-8b90-cf7f4cd27bb6':
                    $dataUserr['LOGO'] = $value;
                break;
                case '5d0b982c-a8e6-4020-959a-0190fa60826e':
                    $dataUserr['Capital social MO'] = $value;
                break;
                case '9ba785f0-6f74-4dd7-8e54-3c55712fd720':
                    if(!empty($value->getValue()))
                    {
                      $dataUserr['COEF'] = $value->getValue()->getValue();
                    }

                break;
                case '609972d3-9d0a-4d7a-8380-e55617b49873':
                    $dataUserr['Logo upload'] = $value;
                break;
                case '251a6ac4-50cb-448a-bc74-bdd37ceaf97d':
                    $dataUserr['site internet'] = $value;
                break;

              }
        }

        //b6dd2cfb-6256-43d9-951d-6edc2838a2bb
        $premiumCoefUser=$_obj->getChildrenByType($obj, new Core\Type\Type('b6dd2cfb-6256-43d9-951d-6edc2838a2bb', ''));
        $dtOperChildRangeV = $_obj->getAllValues($premiumCoefUser);
        $userCoef=[];
        foreach ($dtOperChildRangeV as  $attrsAll) {
           $tmp=[];
            foreach ($attrsAll as  $valAtttr)
            {
                $attrUid=$valAtttr->getUid();
                if($attrUid=='06b7e94f-31ae-4cd0-8b5a-30d4eeb8c8b0')
                {

                   $tmp['nameScen']=$valAtttr->getValue()->getName();
                }elseif($attrUid=='1390beb3-7f7d-4eeb-a737-b40501d017cc')
                {
                  $tmp['coef']=$valAtttr->getValue()->getValue();
                }
            }
            $userCoef[$tmp['nameScen']]=$tmp;
         }
        $result=-1;
        $dtF=[];
        $key=0;
        //массив в котором данные для последнего объекта
        $attrDataFinalData=[];

        if ($this->request->isPost()) {
          $operations=!empty($this->request->get('operation'))?$this->request->get('operation'):[] ;
          $beneficiaire=$this->request->get('beneficiaire');
          $organisation=$this->request->get('organization');
          $Sous_traitance=!empty($this->request->get('sous_traitance'))?$this->request->get('sous_traitance'):['nom'=>'','prenom'=>'','raison_sociale'=>'','siret'=>''];
          $dateClint=$this->request->get('date');
          $information_lieu_travaux=$this->request->get('information_lieu_travaux');
          $urlPage1=[];
          $urlPage2=[];
          $urlPrevisite=[];
          $urlGeneralConditions=[];
          $urlGeneralOperationATTESTATIOSIMPLIFIEE=[];
          $operFirst=null;
        //  $acode=count($operations)>1? true:false;
          $acode=false;
          if(count($operations)>1)
          {
            $acode=true;
          }elseif(count($operations)==1)
          {
            if(!empty($operations[0]['isolant']))
            {
              if(count($operations[0]['isolant'])>1)
              {
                $acode=true;
              }
            }
          }
          $preEN101='';
          $preEN103='';
          $preVisitteGlobal=empty($beneficiaire['previsite'])? '':$beneficiaire['previsite'];
          $isolant=null;
          $buro=null;

          $surFace=[];
          $referencerapport=[];
          $rSIREN=[];$DateVal=[];$norganisme=[];$namePD=[];
          $typeDateRange=new Core\Type\Type('89a9922b-5c91-42bb-b8bf-19f14b0bb20f', '');
          $dateDeviss=!empty($dateClint['date_de_signature'])?\DateTime::createFromFormat(\Dzeta\Core\Value\DateTimeV::DATE_FORMAT_new,$dateClint['date_de_signature']) :new DateTime();
          $attrOperations=[];
          //тут пишется данные из prime
          $IncitationArr=[];
          //это наш Cumac
          $CumacArr=[];
          $OperationTable=[
            'operTable'=>[
              'Table.Operation.Title'=>[],
              'Table.Operation.Surface'=>[],
              'Table.Incitation'=>[],
              'Table.TTC'=>[]
            ]
          ];
          $operTitle=[];
          foreach($operations as $voper)
          {
            $operTitle[]=$voper['operation']['name'];
            $curGlobalData=[];
            $resThermiqArr=[];//[];
            $EpaisseurArr=[];//[];
             $resMarqueArr=[];
                  $ReferenceArr=[];
                   $SurfaceArr=[];
            $isolant=null;
            $buro=null;
            $resThermiq='';//[];
            $surFace='';
             $MarqueTmp  =''   ;
             $ReferenceTmp='';
            $referencerapport='';
            $rSIREN='';$DateVal='';$norganisme='';$namePD='';
            $preEN103='';
            $uidO=$voper['operation']['uid'];
            $operFirst1=$voper['operation']['name'];
            $IncitationArr[]=(float)$voper['cost_client'];
            $CumacArr[]=$voper['value']*$voper['surface'];
            $checkArr=[];
            $attrIsolant=[];
            $Secteurd='';
            $Nergie_de_chauffage='';
            $Type_de_pose='';
            $specOperIsolant=null;
            $sectDeActivity=null;
            if(!empty($voper['objUid']))
            {
              foreach ($voper['objUid'] as  $valCheck)
              {
                $checkArr[]='Check.'.$valCheck['label'].'.'.$valCheck['name'];
              //  $checkArr[]='Check. '.$valCheck['label'].'.'.$valCheck['name'];
               if($valCheck['name']=='Électricité')
               {
                $specOperIsolant=0;
               }elseif($valCheck['name']=='Combustible'){
                 $specOperIsolant=1;
               }
               if($operFirst1=='BAR-TH-160'||$operFirst1=='BAT-TH-146')
               {
                $specOperIsolant=1;
               }
                if($valCheck['label']=='Énergie de chauffage'&&$Nergie_de_chauffage=='')
                {
                  $Nergie_de_chauffage=$valCheck['name'];
                }
                if($valCheck['label']=='Type de pose')
                {
                  $Type_de_pose=$valCheck['name'];
                }//Type de pose
                if($valCheck['label']=='Secteur d’activité')
                {
                  $sectDeActivity=$valCheck['name'];
                  switch ($sectDeActivity) {
                    case 'Autres secteurs':
                      $sectDeActivity=3;
                      break;
                    case 'Bureaux':
                      $sectDeActivity=0;
                      break;
                    case 'Enseignement':
                      $sectDeActivity=0;
                      break;
                    case 'Hôtellerie / Restauration':
                      $sectDeActivity=1;
                      break;
                    case 'Santé':
                      $sectDeActivity=2;
                      break;
                    default:
                       $sectDeActivity=null;
                      break;
                  }
                }
              }
            }

            if(empty($operFirst))
            {
              $operFirst=substr($operFirst1, 0, 3);
            }
           /* if(strpos($operFirst1,'EN-101'))
            {
              $preEN103=$voper['previsite'];
            }
            if(strpos($operFirst1,'EN-103'))
            {
              $preEN103=$voper['previsite'];
            }
            if(empty($preVisitteGlobal))
            {
              $preVisitteGlobal=$preEN103;
            }*/
            if(!empty($voper['isolant']))
            {
              foreach ($voper['isolant'] as $key => $valIsolant)
              {
                   $surfaceIsolant= $valIsolant['surface'] ;
                    $resThermiqTmp='';
                    $EpaisseurTmp='';

                    $MarqueTmp='';
                    $ReferenceTmp='';
                    $AcermiTmp='';
                  if($valIsolant['value']==-2)
                  {
                    //advParams
                    foreach ($valIsolant['advParams'] as  $valueIsolantStat)
                    {
                        if($valueIsolantStat['title']=='Marque')
                        {
                           $MarqueTmp=$valueIsolantStat['value'];
                        }
                        if($valueIsolantStat['title']=='Reference')
                        {
                           $ReferenceTmp=$valueIsolantStat['value'];
                        }
                        if($valueIsolantStat['title']=='Epaisseur'||$valueIsolantStat['title']=='Epaisseur:')
                        {
                           $EpaisseurTmp=$valueIsolantStat['value'];
                        }
                        if($valueIsolantStat['title']=='Résistance thermique:'||$valueIsolantStat['title']=='Résistance thermique')
                        {
                           $resThermiqTmp=$valueIsolantStat['value'];
                        }

                    }
                  }else{
                    $isolant=new \Dzeta\Core\Instance($valIsolant['value'], '');
                    $isolant=$_obj->getAllValues([$isolant])[$valIsolant['value']];

                    if(!empty($isolant))
                    {
                      foreach ($isolant as  $valIsolant1)
                      {
                        if($valIsolant1->getUid()=='1ff21b10-95c3-4557-aa9e-6e46a3d17c27')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                            $resThermiqTmp=$valIsolant1->getValue()->getValue();
                          }
                        }elseif($valIsolant1->getUid()=='86eeb716-de19-46a2-8b63-636aa8ae6da0')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                            $EpaisseurTmp=$valIsolant1->getValue()->getValue();
                          }
                        }elseif($valIsolant1->getUid()=='202fd14c-ea8e-4043-aa5c-a616641b7bae')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                            $MarqueTmp=$valIsolant1->getValue()->getName();
                          }
                        }elseif($valIsolant1->getUid()=='95cf9bce-3782-4b4c-bb4d-bb98a3708e58')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                             $ReferenceTmp=$valIsolant1->getValue()->getValue();
                          }
                        }elseif($valIsolant1->getUid()=='3c65e6db-8289-4d86-8e9d-66e9c39cc47f')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                             $AcermiTmp=$valIsolant1->getValue()->getValue();
                          }
                        }

                      }
                      $attrIsolant[]=[
                      [
                        'attr'=>'f1003f85-7512-4a73-88e6-589804ef9bea',
                        'name'=>'Isolant',
                        'datatype'=>'object',
                        'key'=>true,
                        'value'=>$valIsolant['value']
                      ],
                      [
                        'attr'=>'c9d1ae62-63cb-43b7-ba93-3eac1f9a3082',
                        'name'=>'Surface',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>$valIsolant['surface']
                      ],

                    ];
                  }



                  }
                   $curGlobalData[]=[
                        'Isolant-Surface (m2)'=>$valIsolant['surface'],
                        'Isolant-Résistance Thermique'=>$resThermiqTmp,
                        'Isolant-Energie de chauffage'=>$EpaisseurTmp,
                        'Isolant-Marque'=> $MarqueTmp,
                       'Isolant-Reference'=> $ReferenceTmp,


                    ];
                  $resThermiqArr[]=$resThermiqTmp;
                  $EpaisseurArr[]=$EpaisseurTmp;
                  $resMarqueArr[]=$MarqueTmp;
                  $ReferenceArr[]=$ReferenceTmp;
                  $SurfaceArr[]=$surfaceIsolant;
              }

            }
            if(!empty($voper['bureaudecontrole']))
            {
              $buro=new \Dzeta\Core\Instance($voper['bureaudecontrole'], '');
              $buro=$_obj->getAllValues([$buro])[$voper['bureaudecontrole']];
              if(!empty($buro))
              {
                foreach ($buro as  $valueBuro)
                {
                  if($valueBuro->getUid()=='d1ce27ae-6927-4907-ae2c-5ef2e8f2730e')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $rSIREN=substr(str_replace(' ','',$valueBuro->getValue()->getValue()),0,9);
                    }
                  }
                  if($valueBuro->getUid()=='0ace0f27-8a56-4e63-aca1-db94f39a87df')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $DateVal=$valueBuro->getValue()->getValueN()->format('d/m/Y');
                    }
                  }
                  if($valueBuro->getUid()=='d64797ca-4088-4f12-a190-e66ce3add196')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $norganisme=$valueBuro->getValue()->getValue();
                    }
                  }
                  if($valueBuro->getUid()=='df69b461-c591-4a7d-813d-99265abc7b73')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $namePD=$valueBuro->getValue()->getValue();
                    }
                  }
                }
              }
            }
            if(isset($voper['surface']))
            {
              $surFace=$voper['surface'];
            }
            if(isset($voper['referencerapport']))
            {
              $referencerapport=$voper['referencerapport'];
            }

            $o11=new \Dzeta\Core\Instance($uidO, '');
            $dataO11 = $_obj->getAllValues([$o11])[$voper['operation']['uid']];
            $dataPage1Oper=null;
            $dataPage2Oper=null;
             $OPERtITLE='';
            $oPERtITLEtEXT='';
            foreach ($dataO11 as $key => $valAttrrr) {
              if($valAttrrr->getUid()=='7762042b-7025-4a1b-aef9-a8be70e9edb6')
              {
                $dataPage1Oper=$valAttrrr->getValue();
              }
              if($valAttrrr->getUid()=='d6c7a437-23a6-49f1-8605-22312c8a7cc2')
              {
                $dataPage2Oper=$valAttrrr->getValue();
              }
              if($valAttrrr->getUid()=='f5d60a0a-bb00-4bc2-a7db-f1cec9c4e7f4')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $OPERtITLE=$valAttrrr->getValue()->getValue();
                }

              }
              if($valAttrrr->getUid()=='296185ab-746d-47fa-bf74-cf9b881172cc')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $oPERtITLEtEXT=$valAttrrr->getValue()->getValue();
                }

              }
            }
            $dtOperChildRange=$_obj->getChildrenByType($o11, $typeDateRange);
            $flP1=true;$flP2=true;
            $operArr=  [
                'Operations.Bureau de controle.Date de validite'=>$DateVal,
                'Operations.Bureau de controle.Numero accreditation cofrac'=>$norganisme,
                'Operations.Bureau de controle.RAISON SOCIALE'=>$namePD,
                'Operations.Bureau de controle.SIREN'=>substr(str_replace(' ','',$rSIREN),0,9),
                'Operations.Isolant.Résistance thermique'=>$resThermiq,
                'Operations.Référence rapport'=>$referencerapport,
                'Operations.Surface'=>$surFace,
                'Operations.Pre visite'=>$preVisitteGlobal,
                'Incitation'=>str_replace('.', ',', $voper['cost_client']),
                'Operations.CheckArray'=>$checkArr,
                'Operation.Title'=>$OPERtITLE,
                'Operation.Text title'=>$oPERtITLEtEXT,


            ];
            //тут мы добавляем блок по обработке additionalBlocks
            if(!empty($voper['additional_blocks']))
            {
              $addSettMetk=[];
              if(isset($dataSett[0]['value'][4]['value'][0]['value']))
              {
                $addSettMetk=$dataSett[0]['value'][4]['value'][0]['value'];
              }
              foreach ($voper['additional_blocks'] as $valueData) {
                  $nameP=$valueData['name'];
                  $uidPP=$valueData['uid'];
                  $childrenData=isset($valueData['data'])? $valueData['data']:[];
                  if(empty($addSettMetk[$nameP]))
                  {
                    continue;
                  }
                  //тут делаем запрос в бд и берем значение
                  $valMetkDoppp='';
                  $oAddDop=new \Dzeta\Core\Instance($uidPP, '');
                  $dataAddDop = $_obj->getAllValues([$oAddDop])[$uidPP];
                  foreach ($dataAddDop as  $valuedataAddDop) {
                    switch ($valuedataAddDop->getUid()) {
                      case '8757ba2d-8b76-488d-b767-a15fec0f53af':
                        if(!is_null($valuedataAddDop->getValue()))
                        {
                          $valMetkDoppp=$valuedataAddDop->getValue()->getValue();
                        }
                        break;
                    }
                  }
                
                        $operArr[$addSettMetk[$nameP]['metka']]=$valMetkDoppp;
                        $OperationTable['operTable'][$addSettMetk[$nameP]['metka']]=$valMetkDoppp;
                        $dataDopChildren=[];

                        if(isset($addSettMetk[$nameP]['value']['Block elements']['value']))
                        {
                          $dataDopChildren=$addSettMetk[$nameP]['value']['Block elements']['value'];

                        }
                        foreach ($childrenData as $valuechildrenData) {
                            if(isset($dataDopChildren[$valuechildrenData['name']]))
                            {
                              $valMetkDoppp='';
                              $oAddDop=new \Dzeta\Core\Instance($valuechildrenData['uid'], '');
                              $dataAddDop = $_obj->getAllValues([$oAddDop])[$valuechildrenData['uid']];
                              foreach ($dataAddDop as  $valuedataAddDop) {
                                switch ($valuedataAddDop->getUid()) {
                                  case '77e083c5-0fb4-44af-979e-1bab31dcdab6':
                                    if(!is_null($valuedataAddDop->getValue()))
                                    {
                                      $valMetkDoppp=$valuedataAddDop->getValue()->getValue();
                                    }
                                    break;
                                }
                              }
                              $operArr[$dataDopChildren[$valuechildrenData['name']]['metka']]=$valMetkDoppp;
                              $OperationTable['operTable'][$dataDopChildren[$valuechildrenData['name']]['metka']]=$valMetkDoppp;
                            }
                        }
                    }
            }
                
            // $dataSett
            $OperationTable['operTable']['Table.Operation.Title'][]=$voper['operation']['name'];
            $OperationTable['operTable']['Table.Operation.Surface'][]=$surFace;
            $OperationTable['operTable']['Table.Incitation'][]=$voper['cost_client'];
            $OperationTable['operTable']['Table.TTC'][]=empty($surFace)?null:round($voper['cost_client']/$surFace,2);
            if(empty($curGlobalData))
            {
              $curGlobalData[]=[
                  'operation-Operation'=>$voper['operation']['name'],
                  'operation-PrimeClient'=>$voper['cost_client'],
                  'operation-Prime'=>$voper['cost'],
                   'buro-RS'=>$namePD,
                    'buro-siren'=>substr(str_replace(' ','',$rSIREN),0,9),
                    'specOperIsolant'=> $specOperIsolant,
                    'type_de_pose'=>$Type_de_pose,
                    'Operation.DeActivitii'=>$sectDeActivity
              ];
            }else{
              foreach ($curGlobalData as $keycurGlobalData => $valuecurGlobalData) {
                $curGlobalData[$keycurGlobalData]['operation-Operation']=$voper['operation']['name'];
                $curGlobalData[$keycurGlobalData]['operation-PrimeClient']=$voper['cost_client'];
                $curGlobalData[$keycurGlobalData]['operation-Prime']=$voper['cost'];
                $curGlobalData[$keycurGlobalData]['buro-RS']=$namePD;
                $curGlobalData[$keycurGlobalData]['buro-siren']=substr(str_replace(' ','',$rSIREN),0,9);
                $curGlobalData[$keycurGlobalData]['specOperIsolant']=$specOperIsolant;
              }
                $tmpV=[];
               $tmpV[]=[
                  'operation-Operation'=>$voper['operation']['name'],
                  'operation-PrimeClient'=>$voper['cost_client'],
                  'operation-Prime'=>$voper['cost'],
                  'buro-RS'=>$namePD,
                  'buro-siren'=>substr(str_replace(' ','',$rSIREN),0,9),
                  'specOperIsolant'=> $specOperIsolant,
                  'Isolant-Surface (m2)'=>array_sum(array_column($curGlobalData, 'Isolant-Surface (m2)')),
                  'Isolant-Résistance Thermique'=>implode('/',array_column($curGlobalData, 'Isolant-Résistance Thermique')),
                  'Isolant-Energie de chauffage'=>implode('/',array_column($curGlobalData, 'Isolant-Energie de chauffage')),
                  'Isolant-Marque'=>implode('/',array_column($curGlobalData, 'Isolant-Marque')),
                  'Isolant-Reference'=>implode('/',array_column($curGlobalData, 'Isolant-Reference')),
                   'type_de_pose'=>$Type_de_pose,
                   'Operation.DeActivitii'=>$sectDeActivity
              ];
              $curGlobalData=$tmpV;
            }
            foreach ($curGlobalData as $keycurGlobalData => $valuecurGlobalData){
                $attrDataFinalData[]=$valuecurGlobalData;
            }

            $attrOperations[]=[
              [
                'attr'=>'abaf2816-48a4-4c30-9f27-f86ed7ec9a26',
                'name'=>'Operation',
                'datatype'=>'object',
                'key'=>true,
                'value'=>$uidO
              ],
              [
                'attr'=>'6b4fca5e-9d7c-4d59-a080-dd7fc9132b4e',
                'name'=>'Pre visite',
                'datatype'=>'datetime',
                'key'=>false,
                'value'=>$preVisitteGlobal
              ],
              [
                'attr'=>'b5e3f648-a3a0-4600-a2b4-ce448afa7752',
                'name'=>'Bureau de controle',
                'datatype'=>'object',
                'key'=>false,
                'value'=>!empty($voper['bureaudecontrole'])? $voper['bureaudecontrole']:''
              ],
              [
                'attr'=>'84a47350-1869-45c4-b5d7-0dc60b2097be',
                'name'=>'Référence rapport',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$referencerapport
              ],
             /* [
                'attr'=>'3667fcc9-1e5b-474f-b6a4-3e0e8f69a0ae',
                'name'=>'Isolant',
                'datatype'=>'object',
                'key'=>false,
                'value'=>!empty($voper['isolant'])? $voper['isolant']:null
              ],*/
              [
                'attr'=>'7341b4d2-44ce-44f4-9116-e49cdec4c852',
                'name'=>'Surface',
                'datatype'=>'number',
                'key'=>false,
                'value'=>$surFace
              ],
              [
                'attr'=>'95cbc4b1-d4e4-46a2-a898-29447a04e00c',
                'name'=>'Prime Client, €',
                'datatype'=>'number',
                'key'=>false,
                'value'=>$voper['cost_client']
              ],
              [
                'attr'=>'f3eb825c-0eb7-49f3-ab2b-246dcea0826f',
                'name'=>'Prime Pro, €',
                'datatype'=>'number',
                'key'=>false,
                'value'=>$voper['cost_pro']
              ],
              'childIsolant'=>$attrIsolant

            ];
            if(empty($resThermiqArr))
            {
              $resThermiqArr[]='';
                $EpaisseurArr[]='';
              $resMarqueArr[]='';
              $ReferenceArr[]='';
               $SurfaceArr[]='';
            }

            if(!empty($dtOperChildRange))
            {
              $dtOperChildRangeV = $_obj->getAllValues($dtOperChildRange);

              foreach ($dtOperChildRangeV as  $attrsAll) {
                  $dateStartDateRange=null;
                  $dateFinishDateRange=null;
                  $Page1DateRange=null;
                  $Page2DateRange=null;
                  foreach ($attrsAll as  $valAtttr)
                  {
                      $attrUid=$valAtttr->getUid();
                      if($attrUid=='06e55dbd-1574-4aa5-9b8e-f062b73aad62')
                      {
                        $dateStartDateRange=$valAtttr->getValue();
                      }elseif($attrUid=='23361d2b-5c69-432e-a7f5-34efa1168d4b')
                      {
                        $dateFinishDateRange=$valAtttr->getValue();
                      }elseif($attrUid=='065b778e-3b21-4424-a3fd-ad40eb25c368')
                      {
                        $Page1DateRange=$valAtttr->getValue();
                      }elseif($attrUid=='490b7211-75d5-404a-896b-de527ed70070')
                      {
                        $Page2DateRange=$valAtttr->getValue();
                      }
                  }

                  if(!is_null($dateStartDateRange)&&!is_null($dateFinishDateRange))
                  {
                    if($dateStartDateRange->getValueN()<$dateDeviss&&
                    $dateFinishDateRange->getValueN()>$dateDeviss)
                    {
                      //вот оно счастье и смотрим P1 и P2
                      if(!is_null($Page1DateRange))
                      {
                       /* foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                        {
                          $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                          $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
                          //$EpaisseurArr

                        }*/
                        $urlPage1[]=['url'=>$Page1DateRange->getValue(),'pages'=>1,'dopData'=>$operArr];

                      }else
                      {
                        if(!is_null($dataPage1Oper))
                        {

                        /*  foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                          {
                            $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                            $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];

                          }*/
                          $urlPage1[]=['url'=>$dataPage1Oper->getValue(),'pages'=>1,'dopData'=>$operArr];
                        }

                      }
                      if(!is_null($Page2DateRange))
                      {

                        foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                          {
                            //Operations.Isolant.Surface
                            $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                            $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Surface']=str_replace('.',',',round($SurfaceArr[$keyResThermiq],2).'');
                            $urlPage2[]=['url'=>$Page2DateRange->getValue(),'pages'=>1,'aCode'=>$acode,'sPage'=>1,'dopData'=>$operArr];
                          }
                      }else
                      {
                        if(!is_null($dataPage2Oper))
                        {

                          foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                          {
                            $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                            $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
                            $operArr['Operations.Isolant.Surface']=str_replace('.',',',round($SurfaceArr[$keyResThermiq],2).'');
                            $urlPage2[]=['url'=>$dataPage2Oper->getValue(),'pages'=>1,'aCode'=>$acode,'sPage'=>1,'dopData'=>$operArr];
                          }
                        }

                      }
                      $flP1=false;$flP2=false;
                      break;
                    }
                  }

              }

            }

            if($flP1)
            {
              if(!is_null($dataPage1Oper))
              {
               /* foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                {
                  $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                  $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                  $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
              $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];

                }*/
                 $urlPage1[]=['url'=>$dataPage1Oper->getValue(),'pages'=>1,'dopData'=>$operArr];

              }
            }
            if($flP2)
            {
              if(!is_null($dataPage2Oper))
              {
                foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                {
                  $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                  $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                  $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                  $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
                  $operArr['Operations.Isolant.Surface']=str_replace('.',',',round($SurfaceArr[$keyResThermiq],2).'');

                  $urlPage2[]=['url'=>$dataPage2Oper->getValue(),'pages'=>1,'aCode'=>$acode,'sPage'=>1,'dopData'=>$operArr];
                }

              }
            }
          }



          $urllGlob=[];
          $opCoopAve='';
          $imgSELogo='';
          $SE_ORGANIZATION='';
          $SE_ORGANIZATION_EMMY='';
          $SE_SIREN='';
          $SE_MENTION='';
          $SE_Addresse='';
          $orguid=$organisation['uid'];
          $orguid=empty($orguid)?'b63dd855-2b40-4cef-ba4a-201e76966136':$orguid;
          if(!empty($orguid))
          {
              $objS = new \Dzeta\Core\Instance($orguid, '');
              $dataZ = $_obj->getAllValues([$objS])[$orguid];
              foreach ($dataZ as  $valueORG)
              {
                  if($valueORG->getUid()=='896fe297-be94-4056-8ee8-07ae2635eada')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $urll=BASE_PATH.'/ufiles/'.$valueORG->getValue()->getValue().'.file';
                      $imgSELogo=['image'=>$urll];
                    }
                  }
                  if($valueORG->getUid()=='e5b7abf3-7c4d-4a82-bc06-939f6fba616b')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_ORGANIZATION=$valueORG->getValue()->getValue();
                    }
                  }
                  if($valueORG->getUid()=='85805457-9377-45c9-81fa-ddf27c9b6e35')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_ORGANIZATION_EMMY=$valueORG->getValue()->getValue();
                    }
                  }
                  if($valueORG->getUid()=='b264a8e8-2b7e-48ac-adc8-bd7a9646ee85')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_SIREN=substr(str_replace(' ','',$valueORG->getValue()->getValue()),0,9);
                    }
                  }
                  if($valueORG->getUid()=='1f225555-f8e9-4d39-98be-b0828a6d2f29')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_MENTION=$valueORG->getValue()->getValue();
                    }
                  }
                  if($valueORG->getUid()=='457ac944-e70e-4270-8a7f-94902aa1c2aa')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_Addresse=$valueORG->getValue()->getValue();
                    }
                  }
              }
          }

          if(!empty($information_lieu_travaux["operationavec"]))
          {
            $opCoopAvec=$information_lieu_travaux["operationavec"];
            switch($opCoopAvec)
            {
              case 'Operation avec precarite':
                $opCoopAve='OPERATION AVEC PRECARITE';
              break;
              case 'QPV':
                $opCoopAve='QPV';
              break;
             case 'Bailleur Social':
                $opCoopAve='Bailleur Social';
              break;
            }
          }
          if($operFirst=='BAR')
          {
            $barO=new \Dzeta\Core\Instance('3ed9bac7-2d62-4b3f-8e22-e344268a1abc', '');
          }elseif($operFirst=='BAT')
          {

            $barO=new \Dzeta\Core\Instance('ddc8c275-8615-4aaf-8e96-54567893c004', '');
          }else
          {
            return;
          }

          $dtChildrenPS=$_obj->getChildrenByType($barO, new Core\Type\Type('a657749a-7362-40ec-a1fd-4b678fc27f87', ''));
          $dtOperChildRangeV = $_obj->getAllValues($dtChildrenPS);

          //возможно Катя пришлет детей не по имени
          foreach($dtChildrenPS as $keyO=> $attrsAll1)
          {
            $attrsAll=$attrsAll1->getAttributes();
            $pageTypee=null;
            foreach ($attrsAll as  $vaPT) {
              switch ($vaPT->getUid()) {
                case 'c8d0397d-281a-4ce6-ac74-aa1e94dc76d2':
                  $pageTypee=$vaPT;
                  break;
              }
            }
            if(!is_null($pageTypee->getValue()))
            {
              $valO=$pageTypee->getValue()->getUid();
              if($valO=='beefdb51-0ac8-48ae-90d7-e3d23d89e451')
              {
                //тут печаль, и надо искать Version
                $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                $attrVPS=$_obj->getAllValues($dtVersPS);
                foreach($attrVPS as $opVSAttr)
                {
                  $urlVersion=null;
                  $date1Version=null;
                  $date2Version=null;
                  $sPageVersion=null;
                  foreach ($opVSAttr as $key => $valAttrrr) {
                    $attrrUi=$valAttrrr->getUid();
                    switch ($attrrUi) {
                      case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                        $date1Version=$valAttrrr;
                        break;
                      case '38707166-2c4e-4b73-838f-a5f3fec05351':
                        $date2Version=$valAttrrr;
                        break;
                      case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                      $urlVersion  =$valAttrrr;
                        break;
                      case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                        $sPageVersion=$valAttrrr;
                        break;
                    }
                  }
                  if(!is_null($date1Version->getValue())&&!is_null($date2Version->getValue()))
                  {
                    if($date1Version->getValue()->getValueN()<$dateDeviss&&
                    $date2Version->getValue()->getValueN()>$dateDeviss)
                    {
                      if(!is_null($urlVersion->getValue()))
                      {
                        $arrP=['url'=>$urlVersion->getValue()->getValue()];

                        if(!is_null($sPageVersion->getValue()))
                        {
                          $arrP['sPage']=$sPageVersion->getValue()->getValue();
                        }
                        $urllGlob[]=$arrP;
                      }
                    }
                  }
                }
              }elseif($valO=='ee11791e-2585-4a8e-b367-2edd5f13b9e3')
              {
                //P1
                foreach($urlPage1 as $p1)
                {
                  $urllGlob[]=$p1;
                }

              }elseif($valO=='ed6b2955-ebfb-4927-a251-2caf9fa11f31')
              {
                //P2
                foreach($urlPage2 as $p2)
                {
                  $urllGlob[]=$p2;
                }
              }elseif($valO=='e782fe9e-881b-4aa4-b423-d6b7281e0fe0'||$valO=='0518aa50-b40b-4168-8c6a-452b1e79af5b'||$valO=='e9861f78-a303-46be-9839-c665adb8ef64')
              {

                if(($opCoopAve=='QPV'&&$valO=='e782fe9e-881b-4aa4-b423-d6b7281e0fe0')||
                  ($opCoopAve=='OPERATION AVEC PRECARITE'&&$valO=='0518aa50-b40b-4168-8c6a-452b1e79af5b')||
                  ($opCoopAve=='Bailleur Social'&&$valO=='4b43d186-dcae-419d-b77d-51d478ccf7c8'))
                {
                  $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                  new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                  $attrVPS=$_obj->getAllValues($dtVersPS);
                  foreach($attrVPS as $opVSAttr)
                  {
                    $dateFrom=null;
                    $dateTo=null;
                    $linkkk=null;
                    $numberdPages=null;
                    foreach ($opVSAttr as  $valTGYH) {
                      switch ($valTGYH->getUid()) {
                        case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                          $dateFrom=$valTGYH;
                          break;
                        case '38707166-2c4e-4b73-838f-a5f3fec05351':
                          $dateTo=$valTGYH;
                          break;
                        case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                          $linkkk=$valTGYH;
                          break;
                        case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                          $numberdPages=$valTGYH;
                          break;
                      }
                    }
                    if(!is_null($dateFrom->getValue())&&!is_null($dateTo->getValue()))
                    {
                      if($dateFrom->getValue()->getValueN()<$dateDeviss&&
                      $dateTo->getValue()->getValueN()>$dateDeviss)
                      {
                        if(!is_null($linkkk->getValue()))
                        {
                          $arrP=['url'=>$linkkk->getValue()->getValue()];

                          if(!is_null($numberdPages->getValue()))
                          {
                            $arrP['sPage']=$numberdPages->getValue()->getValue();
                          }
                          $urllGlob[]=$arrP;
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          $img=null;
          if(!empty($dataUserr['Logo upload']->getValue()))
          {
            $urll=BASE_PATH.'/ufiles/'.$dataUserr['Logo upload']->getValue()->getValue().'.file';
            $img=['image'=>$urll];
          }elseif(!empty($dataUserr['LOGO']->getValue()))
          {
            $ur=$dataUserr['LOGO']->getValue()->getValue();
            preg_match('/\/d\/(.*?)\//', $ur, $output_array);
            if(empty($output_array))
            {
                   $img=null;
            }else
            {
                   $urll="https://drive.google.com/u/0/uc?id={$output_array[1]}&export=download";
                   $img= ['image'=>$urll];
            }
          }

          $globalCheck=[];
          $flagSous_traitance=false;
          $flagSous_traitance=empty($Sous_traitance['sous_traitance'])? false:true;
         /* foreach ($Sous_traitance as  $val) {
            if($val!='')
            {
              $flagSous_traitance=true;
            }
          }  */
          //sous_traitance
          if($flagSous_traitance)
          {
            $globalCheck[]='Check.Sous traitance.Oui';
          }else{
            $globalCheck[]='Check.Sous traitance.Non';
          }


        $arrGl=[
          'Organization.Organization'=>$SE_ORGANIZATION,
          'Organization.OrganizationEmmy'=>$SE_ORGANIZATION_EMMY,
          'Organization.Logo'=>$imgSELogo,
          'Organization.Adresse'=>$SE_Addresse,
          'Organization.Mentions finales'=>$SE_MENTION,
          'Organization.SIREN'=>substr(str_replace(' ','',$SE_SIREN),0,9),

          'Bénéficiaire.Adresse'=>isset($beneficiaire['adresse'])? $beneficiaire['adresse']:'',
          'Bénéficiaire.Code postale'=>isset($beneficiaire['code_postale'])? $beneficiaire['code_postale']:'',
          'Bénéficiaire.Email'=>isset($beneficiaire['email'])? $beneficiaire['email']:'',
          'Bénéficiaire.Fonction'=>isset($beneficiaire['fonction'])? $beneficiaire['fonction']:'',
          'Bénéficiaire.Nom'=>isset($beneficiaire['nom'])? $beneficiaire['nom']:'',
          'Bénéficiaire.Phone'=>isset($beneficiaire['phone'])? $beneficiaire['phone']:'',
          'Bénéficiaire.Prénom'=>isset($beneficiaire['prenom'])? $beneficiaire['prenom']:'',
          'Bénéficiaire.Raison sociale'=>isset($beneficiaire['raison_sociale'])? $beneficiaire['raison_sociale']:'',
          'Bénéficiaire.SIREN'=>isset($beneficiaire['siern'])? substr(str_replace(' ','',$beneficiaire['siern']),0,9):'',
          'Bénéficiaire.Ville'=>isset($beneficiaire['ville'])? $beneficiaire['ville']:'',

          'Date.Date de la facture'=>isset($dateClint['date_facture'])? $dateClint['date_facture']:'',
          'Date.Date de signature du devis'=>isset($dateClint['date_de_signature'])? $dateClint['date_de_signature']:'',
          'Date.Numéro de facture'=>isset($dateClint['numero_facture'])? $dateClint['numero_facture']:'',

          'Information lieu travaux.Adresse'=>isset($information_lieu_travaux['adresse_travaux'])? $information_lieu_travaux['adresse_travaux']:'',
          'Information lieu travaux.Code postale'=>isset($information_lieu_travaux['code_postale_travaux'])? $information_lieu_travaux['code_postale_travaux']:'',
          'Information lieu travaux.Nom de la copropriété'=>isset($information_lieu_travaux['nom_travaux'])? $information_lieu_travaux['nom_travaux']:'',
          'Information lieu travaux.OPERATION AVEC PRECARITE ou QPV'=>$opCoopAve,
          'Information lieu travaux.Ville'=>isset($information_lieu_travaux['ville_travaux'])? $information_lieu_travaux['ville_travaux']:'',

          'Users.ADRESSE MO'=>is_null($dataUserr['ADRESSE MO']->getValue())? '':$dataUserr['ADRESSE MO']->getValue()->getValue(),
          'Users.Capital social MO'=>is_null($dataUserr['Capital social MO']->getValue())? '':$dataUserr['Capital social MO']->getValue()->getValue(),
          'Users.CP MO'=>is_null($dataUserr['CP MO']->getValue())? '':$dataUserr['CP MO']->getValue()->getValue(),
          'Users.EMAIL MO'=>is_null($dataUserr['EMAIL MO']->getValue())? '':$dataUserr['EMAIL MO']->getValue()->getValue(),
          'Users.FONCTION MO'=>is_null($dataUserr['FONCTION MO']->getValue())? '':$dataUserr['FONCTION MO']->getValue()->getValue(),
          'Users.LOGO'=>$img,
          'Users.MOBILE MO'=>is_null($dataUserr['MOBILE MO']->getValue())? '':$dataUserr['MOBILE MO']->getValue()->getValue(),
          'Users.NOM MO'=>is_null($dataUserr['NOM MO']->getValue())? '':$dataUserr['NOM MO']->getValue()->getValue(),
          'Users.PRENOM MO'=>is_null($dataUserr['PRENOM MO']->getValue())? '':$dataUserr['PRENOM MO']->getValue()->getValue(),
          'Users.RAISON SOCIALE MO'=>is_null($dataUserr['RAISON SOCIALE MO']->getValue())? '':$dataUserr['RAISON SOCIALE MO']->getValue()->getValue(),
          'Users.SIRET MO'=>is_null($dataUserr['SIRET MO']->getValue())? '':$dataUserr['SIRET MO']->getValue()->getValue(),
          'Users.Tel MO'=>is_null($dataUserr['Tel MO']->getValue())? '':$dataUserr['Tel MO']->getValue()->getValue(),
          'Users.VILLE MO'=>is_null($dataUserr['VILLE MO']->getValue())? '':$dataUserr['VILLE MO']->getValue()->getValue(),
          'Users.site internet'=>is_null($dataUserr['site internet']->getValue())? '':$dataUserr['site internet']->getValue()->getValue(),

          'Operations.Pre visite'=>$preVisitteGlobal,
          'Incitation_Global'=>array_sum($IncitationArr),
          //'Cumac_Global'=>array_sum($CumacArr).' kWhC',
          'SYSTEME'=>['tble'=>$OperationTable],
          'TOTAL'=>round(array_sum($IncitationArr)/1.05,2),
          'TVA'=>round(array_sum($IncitationArr)/1.05*0.05,2),//</w:t></w:r></w:p><w:p w:rsidR="00D11242"><w:r><w:t>
          'List.Operations.Title'=>implode('</w:t><w:br/><w:t xml:space="preserve">',$operTitle),
          'Operations.Secteur d’activité'=>$Secteurd,
          'Operations.Énergie de chauffage'=>$Nergie_de_chauffage,


          //'Sous traitance.Nom'=>empty($flagSous_traitance)?'': $Sous_traitance['nom'],
          //'Sous traitance.Prenom'=>empty($flagSous_traitance)?'':$Sous_traitance['prenom'],
          'Sous traitance.Raison sociale'=>empty($flagSous_traitance)?'':$Sous_traitance['raison_sociale'],
          'Sous traitance.SIRET'=>empty($flagSous_traitance)?'':$Sous_traitance['siret'],

          'Global.Check'=>$globalCheck,

          'Information lieu travaux-Bailleur Social.Numéro SIREN du bailleur social'=>
          isset($information_lieu_travaux['adv_field']['siern'])? substr(str_replace(' ','',$information_lieu_travaux['adv_field']['siern']),0,9):'',
          'Information lieu travaux-Bailleur Social.Raison sociale du bailleur social'=>
          isset($information_lieu_travaux['adv_field']['raison_sociale'])? $information_lieu_travaux['adv_field']['raison_sociale']:'',
          'Information lieu travaux-Bailleur Social.Nom du signataire'=>
          isset($information_lieu_travaux['adv_field']['nom'])? $information_lieu_travaux['adv_field']['nom']:'',
          'Information lieu travaux-Bailleur Social.Prénom du signataire'=>
          isset($information_lieu_travaux['adv_field']['prenom'])? $information_lieu_travaux['adv_field']['prenom']:'',
          'Information lieu travaux-Bailleur Social.Fonction du signataire'=>
          isset($information_lieu_travaux['adv_field']['fonction'])? $information_lieu_travaux['adv_field']['fonction']:'',
          'Information lieu travaux-Bailleur Social.Telephone'=>
          isset($information_lieu_travaux['adv_field']['phone'])? $information_lieu_travaux['adv_field']['phone']:'',
          'Information lieu travaux-Bailleur Social.Adress'=>
          isset($information_lieu_travaux['adv_field']['adresse'])? $information_lieu_travaux['adv_field']['adresse']:'',
          'Information lieu travaux-Bailleur Social.Code postal'=>
          isset($information_lieu_travaux['adv_field']['code_postale'])? $information_lieu_travaux['adv_field']['code_postale']:'',
          'Information lieu travaux-Bailleur Social.Ville'=>
          isset($information_lieu_travaux['adv_field']['ville'])? $information_lieu_travaux['adv_field']['ville']:'',
          "Information lieu travaux-Bailleur Social.le nombre total de ménages concernés par l'opération"=>
          isset($information_lieu_travaux['adv_field']['nombre_total'])? $information_lieu_travaux['adv_field']['nombre_total']:'',
           "Information lieu travaux-Bailleur Social.Nombre total de ménages"=>
          isset($information_lieu_travaux['adv_field']['nombre_total_menages'])? $information_lieu_travaux['adv_field']['nombre_total_menages']:'',
          "Information lieu travaux-QPV.Code quartier du quartier prioritaire de la politique de la ville"=>
          isset($information_lieu_travaux['adv_field']['code_quartier'])? $information_lieu_travaux['adv_field']['code_quartier']:'',

        ];
        //$arrGl['Users.NOM MO']= $arrGl['Users.NOM MO'].'${Date.Date de la facture}testMetkkk';
        $obj = new \Dzeta\Core\Instance('', '');
        //Это у нас объект типа Results
        $obj->setType(new Core\Type\Type('e1e563da-e41b-4a04-b73e-db4903243d91', ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Type\Type::TYPE_ROOT, ''));
        $attrValues=[
            [
                'attr'=>'d0109481-37b5-46a6-99ff-8949257619ec',
                'name'=>'Date de signature du devis',
                'datatype'=>'datetime',
                'key'=>true,
                'value'=>$arrGl['Date.Date de signature du devis']
            ],
            [
                'attr'=>'424b0340-843a-46c5-bdad-a3b0e8486d9d',
                'name'=>'Nom de la copropriété',
                'datatype'=>'text',
                'key'=>true,
                'value'=>$arrGl['Information lieu travaux.Nom de la copropriété']
            ],
            [
                'attr'=>'82e773b0-4346-4061-91d6-0ef58602a54d',
                'name'=>'Date de la facture',
                'datatype'=>'datetime',
                'key'=>false,
                'value'=>$arrGl['Date.Date de la facture']
            ],
            [
                'attr'=>'719cce4f-68de-4e9f-abd8-0e470ab2a600',
                'name'=>'Numéro de facture',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Date.Numéro de facture']
            ],
            [
                'attr'=>'a8de391e-0d9f-4ee9-9d22-9428288c56af',
                'name'=>'Information lieu travaux - SIREN',
                'datatype'=>'text',
                'key'=>false,
                'value'=>isset($information_lieu_travaux['siern_travaux'])? $information_lieu_travaux['siern_travaux']:''
            ],
            [
                'attr'=>'ce646665-730b-44e7-a5e3-bd08d0653d79',
                'name'=>'Information lieu travaux - Adresse',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Information lieu travaux.Adresse']
            ],
            [
                'attr'=>'465194ae-3647-45a4-bf1c-93abb4492d0f',
                'name'=>'Information lieu travaux - Code postale',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Information lieu travaux.Code postale']
            ],
            [
                'attr'=>'4c951884-d3ce-4c11-8ae8-fcdeb2a860bb',
                'name'=>'Information lieu travaux - Ville',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Information lieu travaux.Ville']
            ],
            [
                'attr'=>'54bd8507-82a6-46e1-b283-60d26dc9bdb9',
                'name'=>'Bénéficiaire - SIREN',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.SIREN']
            ],
            [
                'attr'=>'0719459c-4b0e-48b1-a258-7b2202a822a1',
                'name'=>'Bénéficiaire - Nom',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Nom']
            ],
            [
                'attr'=>'ce8e0df8-3883-4596-adc3-e373200a33b6',
                'name'=>'Bénéficiaire - Prénom',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Prénom']
            ],
            [
                'attr'=>'b27fb677-bf16-41e0-b3e6-b6142c164aad',
                'name'=>'Bénéficiaire - Fonction',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Fonction']
            ],
            [
                'attr'=>'f4c3d88a-d986-438e-805b-d7d200b50a50',
                'name'=>'Bénéficiaire - Phone',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Phone']
            ],
            [
                'attr'=>'a543b47d-9cb1-4463-9998-08b4f92ee68c',
                'name'=>'Bénéficiaire - Email',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Email']
            ],
            [
                'attr'=>'4282fd54-74cc-4e0f-9032-538381d6fc89',
                'name'=>'Bénéficiaire - Raison sociale',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Raison sociale']
            ],
            [
                'attr'=>'0747b7dd-a1ed-4b3e-914c-0a50aaeb8046',
                'name'=>'Bénéficiaire - Adresse',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Adresse']
            ],
            [
                'attr'=>'cbff3dab-a039-4409-ad5b-215688dfb314',
                'name'=>'Bénéficiaire - Code postale',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Code postale']
            ],
            [
                'attr'=>'2d79d8e4-70c3-4e8e-8799-d768151a0bf4',
                'name'=>'Bénéficiaire - Ville',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Ville']
            ],
            [
                'attr'=>'058dcb7e-5c92-4b60-99ba-1d4ab039d413',
                'name'=>'Organizatio',
                'datatype'=>'object',
                'key'=>false,
                'value'=>$orguid
            ],
        ];
        $attrs = [];
        foreach ($attrValues as $value) {
            $isArray = is_array($value['value']);
            $attr = new \Dzeta\Core\Type\Attribute($value['attr'], '', \Dzeta\Core\Type\DataType::getIndexFromType($value['datatype']), $value['key'], $isArray);
            if($isArray)
            {
              $tmpV=[];
              foreach ($value['value'] as  $valA) {
                $tmpV[]=['value'=>$valA['value']];
              }
              $attr->setValue(['value'=>$tmpV]);
            }else {
              $attr->setValue(['value'=>$value['value']]);
            }
            $attrs[] = $attr;
        }
        $obj->setAttributes($attrs);
        $_obj = new Obj();

        $results123 = $_obj->checkUniqueAndAddWithValues($obj,\Dzeta\Core\Instance::ADDED_BY_USER, $this->session->get('logged')['User']);

        foreach ($attrOperations as $vAttrOper)
        {
            $attrs = [];
            $obj1 = new \Dzeta\Core\Instance('', '');
            //Это у нас объект типа operations
            //echo 'TMP'.$obj->getUid();       fb0219b2-b1cc-47fd-95e3-827b8c9d807a
            $obj1->setType(new Core\Type\Type('9352f443-4851-4bb1-8e91-1d665a0bb93f', ''))->setParent($obj);
            $isolants=[];
        //   echo json_encode($vAttrOper);
         //  echo PHP_EOL;
            foreach ($vAttrOper as $key=> $value)
            {

                if($key==='childIsolant')
                {

                  $isolants=$value;
                  continue;
                }
                $isArray = is_array($value['value']);
                $attr = new \Dzeta\Core\Type\Attribute($value['attr'], '', \Dzeta\Core\Type\DataType::getIndexFromType($value['datatype']), $value['key'], $isArray);
                if($isArray)
                {
                  $tmpV=[];
                  foreach ($value['value'] as  $valA) {
                    $tmpV[]=['value'=>$valA['value']];
                  }
                  $attr->setValue(['value'=>$tmpV]);
                }else {
                  $attr->setValue(['value'=>$value['value']]);
                }
                $attrs[] = $attr;
            }
            $obj1->setAttributes($attrs);
            $_obj->checkUniqueAndAddWithValues($obj1,\Dzeta\Core\Instance::ADDED_BY_USER, $this->session->get('logged')['User']);

            $objI = new \Dzeta\Core\Instance('', '');
            //Это у нас объект типа isolant
            $objI->setType(new Core\Type\Type('fb0219b2-b1cc-47fd-95e3-827b8c9d807a', ''))->setParent($obj1);
            foreach ($isolants as $valIsolant)
            {
              $attrs=[];
              foreach ($valIsolant as  $attrValueIsolant) {
                $isArray = is_array($attrValueIsolant['value']);
                $attr = new \Dzeta\Core\Type\Attribute($attrValueIsolant['attr'], '', \Dzeta\Core\Type\DataType::getIndexFromType($attrValueIsolant['datatype']), $attrValueIsolant['key'], $isArray);
                if($isArray)
                {
                  $tmpV=[];
                  foreach ($attrValueIsolant['value'] as  $valA) {
                    $tmpV[]=['value'=>$valA['value']];
                  }
                  $attr->setValue(['value'=>$tmpV]);
                }else {
                  $attr->setValue(['value'=>$attrValueIsolant['value']]);
                }
                $attrs[] = $attr;
              }
              $objI->setAttributes($attrs);
              $_obj->checkUniqueAndAddWithValues($objI,\Dzeta\Core\Instance::ADDED_BY_USER, $this->session->get('logged')['User']);
            }
        }
         $arrGl1=[];
        if($readSettFile)
        {

             $arrGl1=$arrGl;
             $arrGl=[];
             $pathFileSetting=BASE_PATH.'/config/dop/DataEnemat/settEnemat.json';
             if(file_exists($pathFileSetting))
             {
                $fileSetting=file_get_contents($pathFileSetting);
                $dataSett=json_decode($fileSetting,true);

                $resDataSett=[];
                $metkSettZam=[];
                  $usersBlock=[];
                  if(isset($dataSett[0]['value'][1]['value'][1]))
                  {
                    $usersBlock=$dataSett[0]['value'][1]['value'][1]['value'];
                    $uidUserBlock=array_column($usersBlock, 'uid');
                    foreach ($dataUserValue as $key => $value) {
                      // $dataUserr['Tel MO'] = $value; getValue()->getValue()
                      $uidP=$value->getUid();
                      if($uidP=='6e8c9fcd-1d93-4395-8b90-cf7f4cd27bb6'||$uidP=='609972d3-9d0a-4d7a-8380-e55617b49873')
                      {
                        //это лого и лого имг
                        continue;
                      }
                      $kkk=array_search($uidP, $uidUserBlock);
                      if($kkk!==false)
                      {
                        if(!empty($usersBlock[$kkk]['metka']))
                        {
                          if(!is_null($value->getValue()))
                          {
                            $arrGl1[$usersBlock[$kkk]['metka']]=$value->getValue()->getValue();
                          }else{
                            $arrGl1[$usersBlock[$kkk]['metka']]='';
                          }
                        }
                      }
                    }
                  }
                $this->testSettData($dataSett,$resDataSett,$metkSettZam);
               
                foreach ($metkSettZam as $key => $value) {  
                      //тут теперь перебираем все
                    
                    foreach ($arrGl1 as $keyarrGl1 => $arrGl1Value)
                    {
                      if(mb_strtolower($keyarrGl1)==mb_strtolower($key))
                      {                       
                          foreach ($value as $val)
                          {
                              if(isset($arrGl1[$val]))
                              {                                 
                                $arrGl1[$keyarrGl1]=str_replace('{{'.$val.'}}',$arrGl1[$val] , $arrGl1[$keyarrGl1]);
                              }
                          }
                      }
                    }
                    //теперь перебираем табличные вкладки на значения
                    if(isset($arrGl1['SYSTEME']['tble']['operTable']))
                    {
                      foreach ($arrGl1['SYSTEME']['tble']['operTable'] as $keyTable => $valueTable) {
                       if(mb_strtolower($keyTable)==mb_strtolower($key))
                       {
                        //значит совпало и надо что-то менять
                          foreach ($valueTable as $keyVVV=>  $valueTablee) {
                            foreach ($value as $val)
                            {
                              if(isset($arrGl1[$val]))
                              { 
                                  $arrGl1['SYSTEME']['tble']['operTable'][$keyTable][ $keyVVV]=str_replace('{{'.$val.'}}',$arrGl1[$val] ,$arrGl1['SYSTEME']['tble']['operTable'][$keyTable][ $keyVVV]);
                              }
                            }
                          }
                       }
                      }
                    }
                    foreach ($urllGlob as $keyurllGlob => $valueurllGlob) {
                      if(isset($valueurllGlob['dopData']))
                      {
                        foreach ($valueurllGlob['dopData'] as $keyDopData => $valueDopData) {
                          if(mb_strtolower($keyDopData)==mb_strtolower($key)){
                            foreach ($value as $val)
                            {
                                if(isset($arrGl1[$val]))
                                {                                 
                                  $urllGlob[$keyurllGlob]['dopData'][$keyurllGlob][$keyDopData]=str_replace('{{'.$val.'}}',$arrGl1[$val] , $urllGlob[$keyurllGlob]['dopData'][$keyurllGlob][$keyDopData]);
                                }
                            }
                          }
                        }
                      }
                    }
                }
               
                foreach ($arrGl1 as $key=> $value)
                {
                  if(isset($resDataSett[mb_strtolower($key)]))
                  {
                      $arrGl[$resDataSett[mb_strtolower($key)]]=$value;
                  }else{
                      if($key=='Global.Check')
                      {
                        foreach ($value as  $val) {
                          if(isset($arrGl[$val]))
                          {
                            $arrGl[$key][]=$arrGl[$val];
                          }else{
                            $arrGl[$key][]=$val;
                          }
                        }
                      }else{
                        $arrGl[$key]=$value;
                      }

                  }
                }


                foreach ($urllGlob as $key => $value) {
                    if(isset($value['dopData']['Operations.CheckArray']))
                    {
                      foreach ($value['dopData']['Operations.CheckArray'] as $keye => $valu) {
                        if(isset($resDataSett[$valu]))
                        {

                          unset($urllGlob[$key]['dopData']['Operations.CheckArray'][$keye]);
                          $urllGlob[$key]['dopData']['Operations.CheckArray'][]= $resDataSett[$valu];
                        }
                      }

                    }
                }


             }


        }

        
        $dataExcGlobal=[];
        $allOperations=$this->request->get('operation');
        $indGlobIsolant=0;
        $newattrDataFinalData=[];
        foreach ($allOperations as $key => $operation) 
        {
          $isolants=$operation['isolant'];     
          $nameOper=$operation['operation']['name'];
          foreach ($isolants as $isolant) 
          {
            if($nameOper=='BAR-TH-161'||$nameOper=='BAT-TH-155'||
              $nameOper=='BAR-TH-161 - échangeur à plaques'||$nameOper=='BAT-TH-155 - échangeur à plaques')
            {
              //теперь блок на температуру реагируем для заполнения
              
              if(isset($operation['objUid']))
              {
                $temp=null;
                foreach ($operation['objUid'] as $valueObj) 
                {
                  if($valueObj['label']=='La température du fluide caloporteur')
                  {
                    if($valueObj['name']=='50°C ≤ Tfluide ≤ 120°C')
                    {
                      $temp=0;
                    }else if($valueObj['name']=='Tfluide > 120°C')
                    {
                      $temp=1;
                    }
                  }
                }
                if(!is_null($temp))
                {
                  $attrDataFinalData[$indGlobIsolant]['diamIsolantTemp']=$temp;
                }
              }

              if($nameOper=='BAR-TH-161 - échangeur à plaques'||$nameOper=='BAT-TH-155 - échangeur à plaques')
              {
                $attrDataFinalData[$indGlobIsolant]['Type dinstallation']=1;
              }
              if($nameOper=='BAR-TH-161'||$nameOper=='BAT-TH-155')
              {
                $attrDataFinalData[$indGlobIsolant]['Type dinstallation']=0;
                $attrDataFinalData[$indGlobIsolant]['Isolant-Surface (m2)']=$isolant['surface'];
                if(isset($isolant['advParams']))
                {
                  foreach ($isolant['advParams'] as $keyAdvParams => $valueAdvParams) 
                  {
                    switch ($keyAdvParams) {
                      case 'Marque':
                        $attrDataFinalData[$indGlobIsolant]['Isolant-Marque']=$valueAdvParams['value'];
                        break;
                      case 'Reference':
                        $attrDataFinalData[$indGlobIsolant]['Isolant-Reference']=$valueAdvParams['value'];
                        break;
                      case 'Attribute 1':
                        $attrDataFinalData[$indGlobIsolant]['Isolant-Energie de chauffage']=$valueAdvParams['value'];
                        break;
                      case 'Attribute 2':
                        $attrDataFinalData[$indGlobIsolant]['Isolant-Résistance Thermique']=$valueAdvParams['value'];
                        break;
                    }
                  }
                }
                if(isset($isolant['adv_size']))
                {
                  $groupsDiam=['20_65'=>0,'65_100'=>0,'100'=>0];
                  foreach ($isolant['adv_size'] as $valueAdvSize) 
                  {
                    if(20<=$valueAdvSize['d']&&$valueAdvSize['d']<=65)
                    {
                      $groupsDiam['20_65']+=$valueAdvSize['l'];
                    }elseif(65<$valueAdvSize['d']&&$valueAdvSize['d']<=100)
                    {
                      $groupsDiam['65_100']+=$valueAdvSize['l'];
                    }elseif($valueAdvSize['d']>100)
                    {
                      $groupsDiam['100']+=$valueAdvSize['l'];
                    }
                  }
                  foreach ($groupsDiam as $keyD => $valueD) 
                  {
                    if(!empty($valueD))
                    {
                      switch ($keyD) 
                      {
                        case '20_65':
                          $copyy=$attrDataFinalData[$indGlobIsolant];
                          $copyy['DN aller']=0;
                          $copyy['Isolant-Surface (m2)']=$valueD;
                          $newattrDataFinalData[]=$copyy;
                          break;
                        case '65_100':
                          $copyy=$attrDataFinalData[$indGlobIsolant];
                          $copyy['DN aller']=1;
                          $copyy['Isolant-Surface (m2)']=$valueD;
                          $newattrDataFinalData[]=$copyy;
                          break;
                        case '100':
                          $copyy=$attrDataFinalData[$indGlobIsolant];
                          $copyy['DN aller']=2;
                          $copyy['Isolant-Surface (m2)']=$valueD;
                          $newattrDataFinalData[]=$copyy;
                          break;
                      }
                    }                    
                  }
                }
              }else
              {
                $newattrDataFinalData[]=$attrDataFinalData[$indGlobIsolant];
              }             
            }else
            {
              $newattrDataFinalData[]=$attrDataFinalData[$indGlobIsolant];
            }  
            
          }
          $indGlobIsolant++;
        }
        /*echo '<br/>';echo '<br/>';
        echo json_encode($newattrDataFinalData);
        echo '<br/>';echo '<br/>';*/
        foreach ($newattrDataFinalData as  $valueAttrr)
        {
            $dataExcel=[];
            $attrsGlob=[];
            $operrText='';

            foreach ($valueAttrr as $key => $value) {
                switch ($key) {

                  case 'DN aller':
                    $attrsGlob[]=[
                      'attr'=>'421af247-5757-4968-8eca-419b1385b040',
                      'name'=>'DN aller',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>$value
                    ];
                    $dataExcel['BO']=$value;
                    break;
                case 'diamIsolantTemp':
                    $attrsGlob[]=[
                      'attr'=>'d4394f28-9f27-4061-8c83-9c5961c94213',
                      'name'=>'Température de la source (en °C)',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>$value
                    ];
                    $dataExcel['CV']=$value;
                    break;
                case 'Type dinstallation':
                    $attrsGlob[]=[
                      'attr'=>'e3d0c7d8-f270-4b90-80ef-c6335a5f741e',
                      'name'=>'Type dinstallation',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>$value
                    ];
                    $dataExcel['DJ']=$value;
                    break;
                  case 'operation-Operation':                  
                    $value=substr($value, 0,10);
                    $attrsGlob[]=[
                      'attr'=>'7ffdd052-eb47-4cee-b8f0-4e0445e36649',
                      'name'=>'OPERATION',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$value
                    ];
                    $dataExcel['A']=$value;
                    $operrText=$value;
                    break;
                  case 'Operation.DeActivitii':
                    $attrsGlob[]=[
                      'attr'=>'f82511ae-1565-4558-afab-ad00930e11e3',
                      'name'=>'Secteur',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>$value
                    ];
                    //$dataExcel['BB']=$value;
                    $dataExcel['BC']=$value;

                    break;
                 case 'type_de_pose':
                    $attrsGlob[]=[
                  'attr'=>'17af2f59-2a0c-4bfe-9870-5d1756aa0839',
                  'name'=>'operation-complement-1',
                  'datatype'=>'text',
                  'key'=>false,
                  'value'=>$value
                ];
                // $dataExcel['GW']=$value;   
                 $dataExcel['GX']=$value;  
                    break;

                  case 'Isolant-Résistance Thermique':
                     /* $attrsGlob[]=[
                        'attr'=>'7e3c1f00-029c-4766-b311-a2b2d01777bb',
                        'name'=>'Résistance thermique',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>str_replace('.',',',$value)
                      ];
                      $dataExcel['HG']=str_replace('.',',',$value);  */
                      break;
                   case 'Isolant-Surface (m2)':
                   /*$nameOper=='BAR-TH-161'||$nameOper=='BAR-TH-155'||
              $nameOper=='BAR-TH-161 - échangeur à plaques'||$nameOper=='BAT-TH-155 - échangeur à plaques'*/
                      if($operrText=='BAR-TH-161'||$operrText=='BAT-TH-155'||
                        $operrText=='BAR-TH-161 - échangeur à plaques'||$operrText=='BAT-TH-155 - échangeur à plaques')
                      {
                          $attrsGlob[]=[
                            'attr'=>'c038a4a9-7af3-4236-818e-e82d1ad41331',
                            'name'=>'Nombre dentités',
                            'datatype'=>'number',
                            'key'=>false,
                            'value'=>$value
                          ]; 
                          $dataExcel['GB']=(float)$value; 
                      }elseif($operrText!='BAR-TH-160'&&$operrText!='BAT-TH-146')
                      {
                          $attrsGlob[]=[
                            'attr'=>'725a9941-628e-4356-b23e-01a7a165f65a',
                            'name'=>'Surface',
                            'datatype'=>'number',
                            'key'=>false,
                            'value'=>$value
                          ];
                          //$dataExcel['AP']=(float)$value; 
                          $dataExcel['AQ']=(float)$value; 
                      }else
                      {
                          $attrsGlob[]=[
                            'attr'=>'8e0850a5-abe3-48f1-8edd-895b88353ce5',
                            'name'=>'Metre lineaire',
                            'datatype'=>'number',
                            'key'=>false,
                            'value'=>$value
                          ];
                          // $dataExcel['AX']=(float)$value;
                         $dataExcel['AY']=(float)$value;
                      }

                      break;
                   case 'operation-PrimeClient':
                          $attrsGlob[]=[
                            'attr'=>'d8abe42e-9347-41df-bd03-4a5269bd969e',
                            'name'=>'PrimeClient',
                            'datatype'=>'number',
                            'key'=>false,
                            'value'=>$value
                          ];
                          // $dataExcel['HA']=(float)$value;
                           $dataExcel['HB']=(float)$value;
                      break;
                       case  'Isolant-Energie de chauffage':
                        /*  $attrsGlob[]=[
                            'attr'=>'c07ded0c-dfe8-496d-93a6-01e0b3858646',
                            'name'=>'Epaisseur',
                            'datatype'=>'text',
                            'key'=>false,
                            'value'=>$value
                          ];
                           $dataExcel['HF']=$value;*/
                      break;

                   case 'specOperIsolant':
                      if(!is_null($value))
                      {
                           $attrsGlob[]=[
                        'attr'=>'f940f7cc-887c-4130-8582-53a034d97ceb',
                        'name'=>'Energie de chauffage',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>$value
                      ];
                      // $dataExcel['AO']=$value; 
                      $dataExcel['AP']=$value; 
                      }
                      break;
                    case 'buro-siren':
                      $attrsGlob[]=[
                        'attr'=>'307327aa-0bd1-4d7e-adb7-eeabc50979fe',
                        'name'=>'BUREAU DE CONTROLE SIREN',
                        'datatype'=>'text',
                        'key'=>false,
                        'value'=>$value
                      ];
                      $dataExcel['V']=$value;
                      break;

                    case 'buro-RS':
                      $attrsGlob[]=[
                        'attr'=>'e3e5a875-7b60-4178-a00e-96a109456878',
                        'name'=>'BUREAU DE CONTROLE RAISON SOCIALE',
                        'datatype'=>'text',
                        'key'=>false,
                        'value'=>$value
                      ];
                      $dataExcel['W']=$value;
                      break;
                    case 'Isolant-Marque':
                      $attrsGlob[]=[
                        'attr'=>'4b4ff266-671a-4759-bd69-6701f3949eb9',
                        'name'=>'Marque',
                        'datatype'=>'text',
                        'key'=>false,
                        'value'=>$value
                      ];
                      //  $dataExcel['HD']=$value;    
                        $dataExcel['HE']=$value; 
                      break;
                   case 'Isolant-Reference':
                     $attrsGlob[]=[
                        'attr'=>'807ab2e0-4113-41f6-8347-9d0e1727bd94',
                        'name'=>'Reference',
                        'datatype'=>'text',
                        'key'=>false,
                        'value'=>$value
                      ];
                        // $dataExcel['HE']=$value;  
                        $dataExcel['HF']=$value; 
                      break;
                      //FORME CEE


                }
            }
              if(isset($userCoef[$operrText])){

                   $attrsGlob[]=[
                      'attr'=>'5df4deb6-ff75-4358-8861-df5e2ba3a11e',
                      'name'=>'tarif-kilo-cumac',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>$userCoef[$operrText]['coef']
                ];
               //  $dataExcel['GZ']=(float)$userCoef[$operrText]['coef'];
                 $dataExcel['HA']=(float)$userCoef[$operrText]['coef'];

              }else{

                   $attrsGlob[]=[
                      'attr'=>'5df4deb6-ff75-4358-8861-df5e2ba3a11e',
                      'name'=>'DATE FIN',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$dataUserr['COEF']
                ];
                //   $dataExcel['GZ']=(float)$dataUserr['COEF'];
              $dataExcel['HA']=(float)$dataUserr['COEF'];
              }

             $attrsGlob[]=[
                      'attr'=>'3fcc7495-c774-4476-9b01-9b3b0e1167b4',
                      'name'=>'DATE FIN',
                      'datatype'=>'datetime',
                      'key'=>false,
                      'value'=>$arrGl['Date.Date de la facture']
              ];
              $dataExcel['C']=$arrGl['Date.Date de la facture'];
              $attrsGlob[]=[
                      'attr'=>'aa856a52-d033-49b0-956e-2783473d9227',
                      'name'=>'DATE Devis',
                      'datatype'=>'datetime',
                      'key'=>false,
                      'value'=>$arrGl['Date.Date de signature du devis']
              ];
              $dataExcel['B']=$arrGl['Date.Date de signature du devis'];

              $attrsGlob[]=[
                      'attr'=>'210fe17e-7ec9-4495-8b32-7535ed13aa68',
                      'name'=>'SIREN BENEFICAIRE',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>str_replace(' ','',$arrGl['Bénéficiaire.SIREN'])
              ];
              $dataExcel['L']=str_replace(' ','',$arrGl['Bénéficiaire.SIREN']);


              $attrsGlob[]=[
                      'attr'=>'3a1d8230-d7aa-44b5-9297-a5b5f7cb5fff',
                      'name'=>'VILLE BENEFICIAIRE',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$arrGl['Bénéficiaire.Ville']
              ];
              $dataExcel['O']=$arrGl['Bénéficiaire.Ville'];


              $attrsGlob[]=[
                      'attr'=>'246787e2-d5b9-4cf9-a1d9-dd0e20c2900d',
                      'name'=>'SIREN PRO',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>str_replace(' ','',$arrGl['Users.SIRET MO'])
              ];
              $dataExcel['P']=str_replace(' ','',$arrGl['Users.SIRET MO']);

               //это новая колонка
             $attrsGlob[]=[
                      'attr'=>'ffb583fe-931e-424d-95b8-caf5dc7aa7d6',
                      'name'=>'SIRET worker',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>empty($arrGl['Sous traitance.SIRET'])?str_replace(' ','',$arrGl['Users.SIRET MO']):str_replace(' ','',$arrGl['Sous traitance.SIRET'])
              ];        
          
              $dataExcel['X']=empty($arrGl['Sous traitance.SIRET'])?str_replace(' ','',$arrGl['Users.SIRET MO']):str_replace(' ','',$arrGl['Sous traitance.SIRET']);

              $attrsGlob[]=[
                      'attr'=>'b73d73ac-d093-4d51-911c-860614d98c12',
                      'name'=>'RAISON SOCIALE PRO',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$arrGl['Users.RAISON SOCIALE MO']
              ];
              $dataExcel['Q']=$arrGl['Users.RAISON SOCIALE MO'];

              $attrsGlob[]=[
                      'attr'=>'e5218f93-2cac-4ba0-9f1e-fba81f17ee61',
                      'name'=>'RAISON SOCIALE SOUS TRAITANT',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$arrGl['Sous traitance.Raison sociale']
              ];
              $dataExcel['T']=$arrGl['Sous traitance.Raison sociale'];

              $attrsGlob[]=[
                      'attr'=>'f3c267d7-aa0f-4484-a1ee-83e7529575a5',
                      'name'=>'SIREN SOUS TRAITANT',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>substr(str_replace(' ','',$arrGl['Sous traitance.SIRET']),0,9)
              ];
              $dataExcel['S']=substr(str_replace(' ','',$arrGl['Sous traitance.SIRET']),0,9);


               $attrsGlob[]=[
                      'attr'=>'a4559b32-cc30-4281-8660-c9562cd104f4',
                      'name'=>'Departement',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>substr($arrGl['Information lieu travaux.Code postale'],0,2)
              ];
               // $dataExcel['AJ']=substr($arrGl['Information lieu travaux.Code postale'],0,2);
               $dataExcel['AK']=substr($arrGl['Information lieu travaux.Code postale'],0,2);

              $attrsGlob[]=[
                      'attr'=>'8dbc8185-c294-4b27-92fa-957c677a7438',
                      'name'=>'Altitude',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>0
              ];
              // $dataExcel['AK']=0;
                $dataExcel['AL']=0;

               $attrsGlob[]=[
                      'attr'=>'8569b823-bf53-4ebe-9f28-9c0305767b79',
                      'name'=>'Iles',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>0
              ];
              // $dataExcel['AL']=0;
                $dataExcel['AM']=0;

              $attrsGlob[]=[
                      'attr'=>'bb319f47-8818-48b5-9ba0-6c981a36b874',
                      'name'=>'Nombre operation',
                      'datatype'=>'number',
                      'key'=>false,
                      'value'=>1
              ];
              // $dataExcel['AM']=1;
                $dataExcel['AN']=1;


               $attrsGlob[]=[
                      'attr'=>'66d6d20d-0d72-48f2-82c2-380a7ed5ca7b',
                      'name'=>'MANDANT',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$arrGl['Organization.OrganizationEmmy']
              ];
               // $dataExcel['GP']=$arrGl['Organization.OrganizationEmmy'];
                $dataExcel['GQ']=$arrGl['Organization.OrganizationEmmy'];

                $attrsGlob[]=[
                      'attr'=>'26b109ea-8da0-462d-bd48-ea5744e366bb',
                      'name'=>'TELEPHONE BENEFICAIRE',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$arrGl['Bénéficiaire.Phone']
              ];
              //     $dataExcel['GU']=$arrGl['Bénéficiaire.Phone'];
                 $dataExcel['GV']=$arrGl['Bénéficiaire.Phone'];


              $attrsGlob[]=[
                      'attr'=>'ce510936-7d91-4825-b55e-611496134ef5',
                      'name'=>'EMAIL BENEFICIAIRE',
                      'datatype'=>'text',
                      'key'=>false,
                      'value'=>$arrGl['Bénéficiaire.Email']
              ];
              // $dataExcel['GV']=$arrGl['Bénéficiaire.Email'];
                $dataExcel['GW']=$arrGl['Bénéficiaire.Email'];



               $attrsGlob[]=[
                'attr'=>'0b6b0515-a109-4b33-93b0-a2868b2aca38',
                'name'=>'FORME CEE',
                'datatype'=>'text',
                'key'=>false,
                'value'=>'PRIME'
              ];
              $dataExcel['U']='PRIME';

               $attrsGlob[]=[
                'attr'=>'c8dc12ad-bfe1-436c-9f52-ff3499451cc8',
                'name'=>'Nom du site des travaux',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Information lieu travaux.Nom de la copropriété']
              ];
              $dataExcel['F']=$arrGl['Information lieu travaux.Nom de la copropriété'];

               $attrsGlob[]=[
                'attr'=>'bae1aa71-1c31-403b-94df-6a1cb841cff9',
                'name'=>'Code postal des travaux',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Information lieu travaux.Code postale']
              ];
              $dataExcel['I']=$arrGl['Information lieu travaux.Code postale'];
               if($_REQUEST['persone']=='Personne physique')
               {
                //тут отдеьно делаем для данных
                  $attrsGlob[]=[
                     'attr'=>'457aaa33-eaf9-437e-9d1c-019e1b7a679f',
                     'name'=>'Bonus coup de pouce',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>4
                  ];
                  $dataExcel['AA']=4; 
                  //AB
                  $numberPers=0;
                  $numberPersS=0;
                  if(isset($beneficiaire['persone_phys_data']))
                  {
                     $numberPers=count($beneficiaire['persone_phys_data']);
                     $numberPersS=$numberPers>1? 4:0;
                  }
                  $attrsGlob[]=[
                     'attr'=>'70dd7015-6bec-4aaa-9b9c-78ad0e641343',
                     'name'=>'Cas precarite',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>$numberPersS
                  ];                
                  $dataExcel['AB']=$numberPersS;

                  $attrsGlob[]=[
                     'attr'=>'4bd9e2bb-0e24-4704-8690-cecc7adfc001',
                     'name'=>'Nombre total de ménages',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>$numberPers
                  ];
                  $dataExcel['AC']=$numberPers;
                  $colAE=0;$colAF=0;
                  if(!empty($beneficiaire['TableAflag']))
                  {
                     $colAE=$numberPers;
                     $colAF=$numberPers;
                  }else if(!empty($beneficiaire['TableBflag']))
                  {
                     $colAE=$numberPers;
                     $colAF=0;
                  }
                  $attrsGlob[]=[
                     'attr'=>'fd677bba-52c3-4660-8bab-eb921ad3f931',
                     'name'=>'Nombre de ménages en situation de précarité',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>$colAE
                  ];
                  $dataExcel['AE']=$colAE;

                  $attrsGlob[]=[
                     'attr'=>'3ad6ae8d-ffd0-44fd-bb8d-fd34772e2d73',
                     'name'=>'Nombre de ménages en situation de grande précarité',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>$colAF
                  ];
                  $dataExcel['AF']=$colAF;
               }else
               {
                  if($operrText=='BAR-EN-101'||$operrText=='BAR-EN-103')
                  {
                     $attrsGlob[]=[
                        'attr'=>'457aaa33-eaf9-437e-9d1c-019e1b7a679f',
                        'name'=>'Bonus coup de pouce',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>3
                     ];
                     $dataExcel['AA']=3; 
                  }else
                  {
                     $attrsGlob[]=[
                        'attr'=>'457aaa33-eaf9-437e-9d1c-019e1b7a679f',
                        'name'=>'Bonus coup de pouce',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>0
                     ];
                     $dataExcel['AA']=0;
                  }
                  $attrsGlob[]=[
                     'attr'=>'4bd9e2bb-0e24-4704-8690-cecc7adfc001',
                     'name'=>'Nombre total de ménages',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>1
                  ];
                  $dataExcel['AC']=1;
                  if($arrGl['Information lieu travaux.OPERATION AVEC PRECARITE ou QPV']=='QPV')
                  {
                     $attrsGlob[]=[
                        'attr'=>'70dd7015-6bec-4aaa-9b9c-78ad0e641343',
                        'name'=>'Cas precarite',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>1
                     ];                
                     $dataExcel['AB']=1;
                  }
                  else
                  {
                     $attrsGlob[]=[
                        'attr'=>'70dd7015-6bec-4aaa-9b9c-78ad0e641343',
                        'name'=>'Cas precarite',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>0
                     ];
                     $dataExcel['AB']=0;
                  }
                  $cdPost2num=substr($arrGl['Information lieu travaux.Code postale'], 0,2);
                  $colAD='';
                  $colAE='';
                  if(isset($dtatCoefIntr[$cdPost2num]))
                  {
                     $colAD=$dtatCoefIntr[$cdPost2num]['AD'];
                     $colAE=$dtatCoefIntr[$cdPost2num]['AE'];
                  }
                  $attrsGlob[]=[
                     'attr'=>'fd677bba-52c3-4660-8bab-eb921ad3f931',
                     'name'=>'Nombre de ménages en situation de précarité',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>$colAD
                  ];
                  $dataExcel['AE']=(float)$colAD;

                  $attrsGlob[]=[
                     'attr'=>'3ad6ae8d-ffd0-44fd-bb8d-fd34772e2d73',
                     'name'=>'Nombre de ménages en situation de grande précarité',
                     'datatype'=>'number',
                     'key'=>false,
                     'value'=>$colAE
                  ];
                  $dataExcel['AF']=(float)$colAE;
               }
              

              
              $attrsGlob[]=[
                  'attr'=>'cb96f2b5-bf76-45c8-9fb9-b24cd8abe8e5',
                  'name'=>'Resistance Thermique',
                  'datatype'=>'number',
                  'key'=>false,
                  'value'=>null
                ];
                 // $dataExcel['AN']=null;
                 $dataExcel['AO']=null;
                
                //'Operations.Type_de_pose'


                 $attrsGlob[]=[
                'attr'=>'26206931-2da5-416c-b25d-8c8c2b9a7173',
                'name'=>'Adresse des travaux',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Information lieu travaux.Adresse']
              ];
              $dataExcel['H']=$arrGl['Information lieu travaux.Adresse'];

                $attrsGlob[]=[
                'attr'=>'910f15cd-fad6-4281-96eb-99303f4cab0b',
                'name'=>'Ville des travaux',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Information lieu travaux.Ville']
              ];
              $dataExcel['J']=$arrGl['Information lieu travaux.Ville'];

                $attrsGlob[]=[
                'attr'=>'a6c3761a-8aac-4987-8e2b-ad460bac7f8c',
                'name'=>'ADRESSE BENEFICAIRE',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Adresse']
              ];
              $dataExcel['M']=$arrGl['Bénéficiaire.Adresse'];

                $attrsGlob[]=[
                'attr'=>'1bd74232-0b40-4a04-9ec2-6437fa0055c6',
                'name'=>'CODE POSTAL BENEFICIAIRE',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.Code postale']
              ];
              $dataExcel['N']=$arrGl['Bénéficiaire.Code postale'];

            /*  $attrsGlob[]=[
                'attr'=>'210fe17e-7ec9-4495-8b32-7535ed13aa68',
                'name'=>'SIREN BENEFICAIRE',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$arrGl['Bénéficiaire.SIREN']
              ];
              $dataExcel['L']=$arrGl['Bénéficiaire.SIREN'];
*/
              


              //

              $frmlVvv='';
              if(!empty($arrGl['Information lieu travaux.Nom de la copropriété']))
              {
                  //тут формула
                  if(isset($dataExcel['A']))
                  {
                     $frmlVvv.=substr($dataExcel['A'],-3,3);
                  }
                 if(isset($dataExcel['F']))
                  {
                      $frmlVvv.=substr($dataExcel['F'],-10,10);
                  }
                 if(isset($dataExcel['I']))
                  {
                       $frmlVvv.=substr($dataExcel['I'],0,2);
                  }
                 if(isset($dataExcel['G']))
                  {
                       $frmlVvv.=substr($dataExcel['G'],0,3);
                  }
                 if(isset($dataExcel['C']))
                  {
                       $v=explode('-',$dataExcel['C']);

                       if(isset($v[1]))
                       {
                          $frmlVvv.=substr($dataExcel['C'],2,2).$v[1];
                       }
                  }
                  /*  if(isset($dataExcel['HD']))
                  {
                       $frmlVvv.=substr($dataExcel['HD'],0,2);
                  }*/
                  if(isset($dataExcel['HE']))
                  {
                       $frmlVvv.=substr($dataExcel['HE'],0,2);
                  }
                  /* if(isset($dataExcel['AP']))
                  {
                       $frmlVvv.=ceil($dataExcel['AP']);
                  }*/
                  if(isset($dataExcel['AQ']))
                  {
                       $frmlVvv.=ceil($dataExcel['AQ']);
                  }
                  /* if(isset($dataExcel['AX']))
                  {
                       $frmlVvv.=ceil($dataExcel['AX']);
                  }*/
                  if(isset($dataExcel['AY']))
                  {
                       $frmlVvv.=ceil($dataExcel['AY']);
                  }
                  if(isset($dataExcel['GB']))
                  {
                       $frmlVvv.=ceil($dataExcel['GB']);
                  }
                  //$frmlVvv.=substr($dataExcel['G'],0,3);
                //  $frmlVvv.=substr($dataExcel['C'],0,3);


              }
                $attrsGlob[]=[
                'attr'=>'0b205bed-e6fe-4b99-95e7-f35d3364b399',
                'name'=>'Reference',
                'datatype'=>'text',
                'key'=>false,
                'value'=>$frmlVvv
              ];
              $dataExcel['E']= $frmlVvv;

              $dataExcGlobal[]=$dataExcel;
             $obj11 = new \Dzeta\Core\Instance('', '');
            //Это у нас объект типа Results EMMY Correct

            $obj11->setType(new Core\Type\Type('86731ded-a598-45e2-95dc-3eedc337d176', ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Type\Type::TYPE_ROOT, ''));
            $attrs = [];
            foreach ($attrsGlob as $value) {
              $isArray = is_array($value['value']);
              $attr = new \Dzeta\Core\Type\Attribute($value['attr'], $value['name'], \Dzeta\Core\Type\DataType::getIndexFromType($value['datatype']), $value['key'], $isArray);
              if($isArray)
              {
                $tmpV=[];
                foreach ($value['value'] as  $valA) {
                  $tmpV[]=['value'=>$valA['value']];
                }
                $attr->setValue(['value'=>$tmpV]);
              }else {
                $attr->setValue(['value'=>$value['value']]);
              }
              $attrs[] = $attr;
            }


            $obj11->setAttributes($attrs);
          //  echo json_encode($attrs);
            //return;
            $_obj = new Obj();
            $results123 = $_obj->checkUniqueAndAddWithValues($obj11,\Dzeta\Core\Instance::ADDED_BY_USER, $this->session->get('logged')['User']);

        }

        $this->tefd($dataExcGlobal);

        /*$thtg=new FillFile($urllGlob,$arrGl);
        //f5191bc5-fb49-4017-8750-e2c3d9963c26


        if(!empty($thtg->uid))
        {
            $result=1;
            $key=$thtg->uid;
            //Если все ок, то пишем файл в параметр еще
            $uuidd=\Dzeta\Core\Value\File::GenerateUIDFile();
            $fileOrigPath=BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.'fillFileFr/'.$key.'/result.pdf';
            $res=copy($fileOrigPath, BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuidd.\Dzeta\Core\Value\File::FILE_EXT);

            
            if($res)
            {

                $attrFiles=  [
                      'attr'=>'8627c592-5b35-4c88-8ea2-069275152cb9',
                      'name'=>'PDF generated',
                      'datatype'=>'file',
                      'key'=>false
                  ];
                $nameFile=$arrGl['Information lieu travaux.Nom de la copropriété'].' '.
                $arrGl['Date.Date de signature du devis'].'.pdf';

                $filePDF=new \Dzeta\Core\Value\File($uuidd, $nameFile, null, filesize($fileOrigPath));
                $attr = new \Dzeta\Core\Type\Attribute($attrFiles['attr'], '',
                 \Dzeta\Core\Type\DataType::getIndexFromType($attrFiles['datatype']), $attrFiles['key'], false);
                $attr->setValueS($filePDF);
                $obj->setAttributes([$attr]);
                $results = $_obj->AddFiles($obj, $this->session->get('logged')['User']);

            }

        }*/


        }
        return;
        echo json_encode(['result'=>$result,'data'=>['key'=>$uuidd],'pg'=>$urllGlob,'dtt'=>[$arrGl,$arrGl1]]);
    }

  public function stepFormGetFileAction($key)
  {
     $file=new \Dzeta\Core\Value\File($key, '');
    $_file=new Files();
    $res=$_file->getInfoFile($file);
    if ($res==1) {
        \Dzeta\Core\Value\File::downloadFile($file);
    }
  }

    public function stepFormGetCoefUserAction()
    {
        $result=-1;
        $resArr=['coef'=>-1];
        if($this->request->isGet())
        {
            $uidOrgan=$this->request->get('organization');
            $uidScen=$this->request->get('scenario');
            $flagFizUR=$this->request->get('persone');
            $date=$this->request->get('date');
            $date=date_create_from_format('d/m/Y',$date)->format('Y/m/d H:i:s');

            $uidUser=$this->session->get('logged')['User']->getUid();
            $_specModel=new Specialenemat();
            $dataRes=$_specModel->getCoefficent($uidUser,$uidOrgan,$uidScen,$date,$flagFizUR);
            $result=$dataRes['result'];
            if($result==1)
            {
                $resArr['coef']=$dataRes['data']['coef'];
            }
        }
        echo json_encode(['result' => $result, 'data' => $resArr]);
    }
    public function stepFormGetCoefsUserAction()
    {
        $result=-1;
        $resArr=['coef'=>[]];
        if($this->request->isPost())
        {
            $dataCoeff=$this->request->get('data');
            $uidUser=$this->session->get('logged')['User']->getUid();
            $_specModel=new Specialenemat();
            foreach ($dataCoeff as $key => $value) 
            {
                $uidOrgan=$value['organization'];
                $uidScen=$value['scenario'];
                $flagFizUR=$value['persone'];
                $date=$value['date'];
                $date=date_create_from_format('d/m/Y',$date)->format('Y/m/d H:i:s');
                $dataRes=$_specModel->getCoefficent($uidUser,$uidOrgan,$uidScen,$date,$flagFizUR);
                $result=$dataRes['result'];
                if($result==1)
                {
                    $resArr['coef'][]=$dataRes['data']['coef'];
                }else{
                    $result=-1;
                    $resArr['coef']=[];
                    break;
                }

            }            
        }
        echo json_encode(['result' => $result, 'data' => $resArr]);
    }

    public function stepFormGetInfoPoSirenAction()
    {
        $resArr = [];
        $result = -1;
        if ($this->request->isGet())
        {
            $code = $this->request->get('siren');
            $code = preg_replace("/[^0-9]/", '', $code);
            // $loader = new \Phalcon\Loader();
            // $loader->registerClasses([
                // 'Dataenterprise' => APP_PATH . '/library/projFR/' . 'Dataenterprise.php',
                // 'Datainfogreffe' => BASE_PATH . '/app/library/projFR/'.'Datainfogreffe.php',
                // 'Datasocieteinfocom' => BASE_PATH . '/app/library/projFR/'.'Datasocieteinfocom.php'
                // 'Inseefr' => BASE_PATH . '/app/library/projFR/'.'Inseefr.php'
            // ]);
            // $loader->register();

            $len = strlen($code);
            if ($len == 9 || $len == 14)
            {
                if ($len == 14)
                {
                    $jsn = AnnuaireEntreprises::GetInfoBySiret($code);
                } else
                {
                    $jsn = AnnuaireEntreprises::GetInfo($code);
                }

                /*$tmp = Datainfogreffe::GetInfo($code);
                $jsn=json_decode($tmp,true);
                if(isset($jsn['Data']))
                {
                    $resArr['Adresse'] = $jsn['Data']['Adresse']['AdresseConcat'];
                    $resArr['Code postale'] = $jsn['Data']['Adresse']['CodePostal'];
                    $resArr['Ville'] = $jsn['Data']['Adresse']['BureauDistributeur'];
                    $resArr['Raison Socliale'] = $jsn['Data']['Denomination'];
                    $result = 1;
                }*/

                /*$tmp = Datasocieteinfocom::GetInfo($code);
                $jsn = json_decode($tmp,true);
                if (isset($jsn['result']))
                {
                    $resArr['Adresse'] = $jsn['result']['organization']['address']['street'];
                    $resArr['Code postale'] = $jsn['result']['organization']['address']['postal_code'];
                    $resArr['Ville'] = $jsn['result']['organization']['address']['city'];
                    $resArr['Raison Socliale'] = $jsn['result']['organization']['name'];
                    $result = 1;
                }*/

                // $tmp = Datainfogreffe::GetInfo($code);
                // $jsn = json_decode($tmp,true);
                // if (isset($jsn['Data']))
                // {
                //     $resArr['Adresse'] = $jsn['Data']['Adresse']['AdresseConcat'];
                //     $resArr['Code postale'] = $jsn['Data']['Adresse']['CodePostal'];
                //     $resArr['Ville'] = $jsn['Data']['Adresse']['BureauDistributeur'];
                //     $resArr['Raison Socliale'] = $jsn['Data']['Denomination'];
                //     $result = 1;
                // }

                if (isset($jsn['Data']))
                {
                    if ($jsn['Data']['Etat'] != 'F')
                    {
                      if (is_null($jsn['Data']['Adresse']['AdresseConcat']) && is_null($jsn['Data']['Adresse']['CodePostal']) && is_null($jsn['Data']['Adresse']['BureauDistributeur']) && is_null($jsn['Data']['Denomination']))
                      {
                          $resArr = [
                              'Adresse' => null,
                              'Code postale' => null,
                              'Ville' => null,
                              'Raison Socliale' => null,
                          ];
                          $result = -2;
                      } else
                      {
                          $resArr = [
                              'Adresse' => $jsn['Data']['Adresse']['AdresseConcat'],
                              'Code postale' => $jsn['Data']['Adresse']['CodePostal'],
                              'Ville' => $jsn['Data']['Adresse']['BureauDistributeur'],
                              'Raison Socliale' => $jsn['Data']['Denomination'],
                          ];
                          $result = 1;
                      }
                    } else
                    {
                        $resArr = [
                            'Adresse' => null,
                            'Code postale' => null,
                            'Ville' => null,
                            'Raison Socliale' => null,
                        ];
                        $result = -2;
                    }
                }
            }
        }
        echo json_encode(['result' => $result, 'data' => $resArr]);
    }

  public function GetSettingsStepFormAction()
  {
      echo json_encode(['result' => 1, 'data' => $this->GetSettingsStepForm()]);
  }

  public function SaveSettingsStepFormAction()
  {
      if ($this->request->isPost())
      {
           $json=$this->request->get('json');
           $filePathMain=BASE_PATH.'/config/dop/DataEnemat/settEnemat.json';
           @rename($filePathMain, $filePathMain.'.'.time());
           file_put_contents($filePathMain,json_encode(json_decode($json),JSON_PRETTY_PRINT));
      }
      echo json_encode(['result' => 1]);
  }

  public function GetSettingsStepForm()
  {
      $filePathMain=BASE_PATH.'/config/dop/DataEnemat/settEnemat.json';
      return file_get_contents($filePathMain);
  }

  public function testAction()
  {
      $filePathMain=BASE_PATH.'/config/dop/DataEnemat/test.json';
      $jsonO=json_decode(file_get_contents($filePathMain),true);

      foreach ($jsonO as $key => $value)
      {
        $this->test1( $value);
       $jsonO[$key]=$value;

      }
      echo json_encode($jsonO);

  }

  public function test1(&$data)
  {

      foreach ($data['values'] as $key => $value) {

          if($value['type']=='block')
          {
              $this->test1($value['values']);
          }elseif($value['type']=='object'){
            //тут получаем атрибуты из бд,
            //и смотрим их с существующими
              $typeO=new Core\Type\Type($value['uid'], '');

              $group = new Core\Type\AttributeGroup(Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
              $group->setParent( $typeO, '');

              $_group = new AttrGroup();
              $dataAttr = $_group->getAttributes($group);
              $newArr=[];
              $attrColumn=array_column($value['metkas'], 'uid');
              foreach ($dataAttr as $valueAttr)
              {
                  $attrUid=$valueAttr->getUid();
                  $numElem=array_search($attrUid, $attrColumn);
                  $tmpArrM=[];
                  $tmpArrM['valueM']='';
                  if($numElem!==false)
                  {
                      $tmpArrM['valueM']=$value['metkas'][$numElem]['valueM'];
                  }
                  $tmpArrM['type']='attr';
                  $tmpArrM['name']=$valueAttr->getName();
                  $tmpArrM['uid']=$attrUid;
                  $newArr[]=$tmpArrM;
              }
              $data['values'][$key]['metkas']=$newArr;
          }elseif($value['type']=='static'){
            //тут по сути ничего не трогаем
          }
      }
       \DZ::dtgf($data);
  }


  public function tefd($data)
  {

     

      // Путь к файлу ключа сервисного аккаунта
      //gifted-honor-236118-0330fc9ff24c.json
    $test=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString('A');

      $googleAccountKeyFilePath = BASE_PATH.'/config/dop/gifted-honor-236118-0330fc9ff24c.json';
      putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );

      // Документация https://developers.google.com/sheets/api/
      $client = new \Google_Client();
      $client->useApplicationDefaultCredentials();

      // Области, к которым будет доступ
      // https://developers.google.com/identity/protocols/googlescopes
      $client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );

      $service = new \Google_Service_Sheets( $client );

      // ID таблицы
      //$spreadsheetId = '1Na3GkG1mB1YvSBoYM4qwm8FL1hDUMkLUxNQ0ySFZQ8A';//это орига
      $spreadsheetId = '1pdnlNujnTEYfZ2MsIavJWXNmhL6LLX3FEDPzGHOcFsg';//это копия
      $spreadsheetId=file_get_contents(BASE_PATH.'/config/dop/DataEnemat/googleExcel.txt');
      $response = $service->spreadsheets->get($spreadsheetId);
      $range = 'CRM EXTERNE!A:A';
      $response = $service->spreadsheets_values->get($spreadsheetId, $range);
      $values = $response->getValues();
      $rfrf=count($values);
      $newData=[];
      foreach ($data as $valueData) {
          $tmp=[];
          foreach ($valueData as $key => $value) {
             $tmp[\PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($key)]=$value;
          }
          $t=[];
          for($i=0;$i<232;$i++)
          {
              if(isset($tmp[$i+1]))
              {
                $t[]=$tmp[$i+1];
              }else{
                $t[]='';
              }

          }
          $newData[]=$t;
      }

      $body    = new \Google_Service_Sheets_ValueRange( [ 'values' =>  $newData] );
      $options = array( 'valueInputOption' => 'RAW' );

      $service->spreadsheets_values->update( $spreadsheetId, 'CRM EXTERNE!A'.($rfrf+1), $body, $options );

      /*foreach ($response->getSheets() as $sheet) {

  // Свойства листа
  $sheetProperties = $sheet->getProperties();
  $sheetProperties->title; // Название листа
 var_dump($sheet);
  $gridProperties = $sheetProperties->getGridProperties();
  $gridProperties->columnCount; // Количество колонок
echo  $gridProperties->rowCount; // Количество строк

}*/

  }


    public function getSettingMetkaAction()
    {
        $filePathMain=BASE_PATH.'/config/dop/DataEnemat/settEnemat.json';
        $dt=[];$dataMetk=[];
        $dataMetk=["---"];
        $typesArrayCheckBoxParrent='89a9922b-5c91-42bb-b8bf-19f14b0bb20f';
        $typeMain=new \Dzeta\Core\Type\Type($typesArrayCheckBoxParrent, '');
        $arrResul=[];
        $this->getAllChildTypesLine($typeMain,$arrResul);
        
        if(file_exists($filePathMain))
        {
            $dt=json_decode(file_get_contents($filePathMain),true);
            $checkArrValues=$dt[0]['value'][2]['value'];
            $arrResultUID=[];
            foreach ($arrResul as $value) 
            {
                $arrResultUID[]=$value->getUid();
            }
            foreach ($checkArrValues as $key => $value) 
            {
                if(isset($value['uidClass']))
                {
                    if(in_array($value['uidClass'], $arrResultUID))
                    {
                        $kSearchh=array_search($value['uidClass'], $arrResultUID);
                        $checkArrValues[$key]['specDataRange']=1;
                        $checkArrValues[$key]['name']=$arrResul[$kSearchh]->getName();
                        unset( $arrResul[$kSearchh],$arrResultUID[$kSearchh]);
                    }elseif(isset($value['specDataRange']))
                    {
                        unset($checkArrValues[$key]);
                    }
                }
                
            }
            foreach ($arrResul as $value) 
            {                
                $checkArrValues[]=[
                    'type'=> "blockCheck",
                    "name"=>$value->getName(),
                    "style"=>"h2",
                    "visible"=> "true",
                    "uidClass"=>$value->getUid(),
                    "value"=>[]
                ];
            }
            $dt[0]['value'][2]['value']=array_values($checkArrValues);
            foreach ($dt as $key=>$value) 
            {
                $t=$this->parseConfigSettEnemat($value,$dataMetk);
                $dt[$key]=$t;
            }
        }
        $dataMetk=array_unique($dataMetk);
        sort($dataMetk);
        echo json_encode(["sett"=>$dt,"metks"=>$dataMetk]);
    }
    private function getAllChildTypesLine($typeParent,&$arrRes)
    {
        $_type=new Type();
        $types=$_type->getChildren($typeParent);
        foreach ($types as $key => $type) 
        {
            $arrRes[]=$type;
            $this->getAllChildTypesLine($type,$arrRes);
        }
    }
  private function parseConfigSettEnemat(&$data,&$dataMetk)
  {

      switch ($data['type']) {
        case 'block':
          foreach ($data['value'] as $key=>$value) {
             $data['value'][$key]=$this->parseConfigSettEnemat($value,$dataMetk);
          }
          break;
        case 'blockForm':
          #TODO тут надо с Женей синхрон сделать
        
          foreach ($data['value'] as $key=>$value) {
            
             $data['value'][$key]=$this->parseConfigSettEnemat($value,$dataMetk);
             if(isset($value['type'])&&$value['type']=="elem"&&!empty($value['metka']))
             {
              $dataMetk[]=$value['metka'];
             }
             if(isset($value['type'])&&$value['type']=="elem"&&!empty($value['dopSettMetka']))
             {
                $newDopSett=[];
                foreach($value['dopSettMetka'] as $valueDSM) 
                {
                     if(!empty($valueDSM))
                     {
                        $newDopSett[]=$valueDSM;
                     }

                }
                $data['value'][$key]['dopSettMetka']=$newDopSett;
             }
             
          }
          break;
        case 'blockDB':
          #TODO тут делаем запрос к БД на получение всех параметров, чтобы их сравнить и получить
          #TODO пока так
          $uidGroup='00000000-0000-0000-0000-000000000000';
          $group = new \Dzeta\Core\Type\AttributeGroup($uidGroup, '');
          $group->setParent(new Core\Type\Type($data['uidClass'], ''));
          $_group = new AttrGroup();
          $dataDB = $_group->getAttributes($group);
          $valueDBMetka=[];
          $uidConfigParam=array_column($data['value'],'uid');
         // var_dump($uidConfigParam);
          foreach ($dataDB as $key => $value)
          {
              $uidParam=$value->getUid();
              $key1=array_search($uidParam, $uidConfigParam);
              $tmpV=[];
              $tmpV['uid']=$uidParam;
              $tmpV['type']='elem';
              $tmpV['name']=$value->getName();
              if($key1!==false){
                $tmpV['metka']=$data['value'][$key1]['metka'];
              }else{
                $tmpV['metka']='';

              }
               $dataMetk[]=$tmpV['metka'];
             
              if(isset($tmpV['type'])&&$tmpV['type']=="elem"&&!empty($tmpV['dopSettMetka']))
             {
                $newDopSett=[];
                foreach($tmpV['dopSettMetka'] as $valueDSM) 
                {
                     if(!empty($valueDSM))
                     {
                        $newDopSett[]=$valueDSM;
                     }

                }
               $tmpV['dopSettMetka']=$newDopSett;
             }
              $valueDBMetka[]=$tmpV;
          }
          $data['value']=$valueDBMetka;
          break;
        case 'blockCheck':
          #TODO тут надо чекбоксы
            $_obj = new Obj();
            $dt=$_obj->getAlltest();
            $uidClass= $data['uidClass'];
            $uidConfigParam=array_column($data['value'],'name');
            $valueDBMetka=[];
            $nameArr=[];
            $data['name']='test';            
            foreach ($dt as $key => $value) 
            {
                if($value['Class']==$uidClass)
                {
                    if(!in_array(trim($value['Name']),$nameArr))
                    {
                        $key1=array_search($value['Name'], $uidConfigParam);
                        $tmpV=[];
                        $tmpV['uid']=$value['ID'];
                        $tmpV['type']='elem';
                        $tmpV['name']=$value['Name'];
                        $data['name']=$value['ClassName'];
                        $dataMetk[]="Check.".$value['ClassName'].".".$value['Name'];
                        if($key1!==false)
                        {
                            $tmpV['metka']=$data['value'][$key1]['metka'];
                        }else
                        {
                            $tmpV['metka']='';
                        }
                        if(isset($tmpV['type'])&&$tmpV['type']=="elem"&&!empty($tmpV['dopSettMetka']))
                        {
                            $newDopSett=[];
                            foreach($tmpV['dopSettMetka'] as $valueDSM) 
                            {
                                if(!empty($valueDSM))
                                {
                                    $newDopSett[]=$valueDSM;
                                }
                            }
                            $tmpV['dopSettMetka']=$newDopSett;
                        }
                        $valueDBMetka[]=$tmpV;
                        $dataMetk[]=$tmpV['metka'];
                        $nameArr[]= trim($value['Name']);
                    }
                }
            }
            usort($valueDBMetka, function($a,$b){
                return strcmp($a['name'], $b['name']);
            });
            $data['value']=$valueDBMetka;
            break;
        case 'blockObjwithChild':
            
          //получение значений вместе с дочерними объектами
          $_obj = new Obj();
          $_type=new Type();
          $parentType=new Core\Type\Type($data['uidClass'],'');
          
          $dt = $_obj->getObjsByClass($parentType,'');
          $_type->getNameClass($parentType);

          $objs=[];
          if(!empty($dt['result']))
          {
            foreach ($data['uidClassChild'] as $key => $value) {

              $tmpType=new Core\Type\Type($value['uid'],'');
              $_type->getNameClass($tmpType);
              if($tmpType->getName()=='')
              {
                unset($data['uidClassChild'][$key]);
              }
              $data['uidClassChild'][$key]['type']= $tmpType;
              $data['uidClassChild'][$key]['name']= $tmpType->getName();

            }

            foreach ($dt['objs'] as $key => $value)
            {
              $childObjs=[];
              
              $attrForName = new Core\Type\Attribute('0fd7cb64-29cf-4232-85b1-e81d78682a42','name',1,false,false);
              $value->setAttributes([$attrForName]);
              $attrrr=$_obj->getAllValues([$value])[$value->getUid()][0];
             
              $newName=$attrrr->getValue()->getValue();
              $value->setName($newName);
              if(isset( $objs[$value->getName()]))
              {
                $childObjs=$objs[$value->getName()]['value'];
              }
               $metkaMain='';
               $settMain=[];
               $childMain=[];
               if(isset($data['value'][$value->getName()]))
               {
                  $metkaMain=$data['value'][$value->getName()]['metka'];
                  $dataMetk[]=$data['value'][$value->getName()]['metka'];
                  $childMain=$data['value'][$value->getName()]['value'];
                  $settMain=empty($data['value'][$value->getName()]['dopSettMetka'])? []:$data['value'][$value->getName()]['dopSettMetka'];
                  $newDopSett=[];
                     foreach($settMain as $valueDSM) 
                    {
                         if(!empty($valueDSM))
                         {
                            $newDopSett[]=$valueDSM;
                         }

                    }
                $settMain=$newDopSett;
               }
             //  $dataMetk[]="TableDataBase.".$parentType->getName().".".$value->getName();
              foreach ($data['uidClassChild'] as $valueChildType)
              {

                $childOT= $valueChildType['type'];
                $child=$_obj->getChildrenByType($value,$childOT);   
                $allValuesObjChild= $_obj->getAllValues($child);  
               
                foreach ($child as $valueChildObj) 
                {
                  $childObjs[$valueChildType['type']->getName()]['name']=$valueChildType['type']->getName();
                  $childObjs[$valueChildType['type']->getName()]['type']="blockForm";
                  $metkDop='';
                  $settDop=[];
                    if(isset($allValuesObjChild[$valueChildObj->getUid()]))
                    {                    
                        foreach ($allValuesObjChild[$valueChildObj->getUid()] as $valueChildd)
                        {   
                            if($valueChildd->getUid()=='9f661741-ad54-4a20-a62f-1b84eaae4076')
                            {
                                $valueChildObj->setName($valueChildd->getValue()->getValue());
                                break;
                            }
                        }
                    }
                  if(isset($childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName()]))
                  {
                    $metkDop=$childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName()]['metka'];
                     $dataMetk[]=$childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName()]['metka'];
                    $settDop=empty($childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName()]['dopSettMetka'])? []:$childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName()]['dopSettMetka'];
                    $newDopSett=[];
                     foreach($settDop as $valueDSM) 
                    {
                         if(!empty($valueDSM))
                         {
                            $newDopSett[]=$valueDSM;
                         }

                    }
                $settDop=$newDopSett;
                   
                  }
                  if(isset($allValuesObjChild[$valueChildObj->getUid()]))
                  {
                    $mmetkaDopp='';
                    $mmetkaSett=[];
                    foreach ($allValuesObjChild[$valueChildObj->getUid()] as $valueChildd)
                    {
                      if($valueChildd->getUid()=='104995b9-b651-447f-b483-682a914ec435'&&
                        !is_null($valueChildd->getValue()))
                      {
                          if(isset($childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName().'.'.$valueChildd->getValue()->getName()]))
                          {
                            $mmetkaDopp=$childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName().'.'.$valueChildd->getValue()->getName()]['metka'];
                             $dataMetk[]=$childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName().'.'.$valueChildd->getValue()->getName()]['metka'];
                            $mmetkaSett=empty($childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName().'.'.$valueChildd->getValue()->getName()]['dopSettMetka'])? []:$childMain[$valueChildType['type']->getName()]['value'][$valueChildObj->getName().'.'.$valueChildd->getValue()->getName()]['dopSettMetka'];
                             $newDopSett=[];
                                 foreach($mmetkaSett as $valueDSM) 
                                {
                                     if(!empty($valueDSM))
                                     {
                                        $newDopSett[]=$valueDSM;
                                     }

                                }
                                $mmetkaSett=$newDopSett;
                          }
                           $childObjs[$valueChildType['type']->getName()]['value'][$valueChildObj->getName().'.'.$valueChildd->getValue()->getName()]=[                  
                          
                          "dopSettMetka"=>$mmetkaSett,
                          "name"=>$valueChildObj->getName().'.'.$valueChildd->getValue()->getName(),
                          "metka"=>$mmetkaDopp,
                          "type"=> "elem",
                        ];
                      }
                    }
                  }
                  $childObjs[$valueChildType['type']->getName()]['value'][$valueChildObj->getName()]=[
                  //  "uid"=>$valueChildObj->getUid(),
                    "dopSettMetka"=> $settDop,
                    "name"=>$valueChildObj->getName(),
                    "metka"=>$metkDop,
                    "type"=> "elem",
                  ];
               //   $dataMetk[]="TableDataBase.".$parentType->getName().".".$value->getName().".".$valueChildType['type']->getName().".".$valueChildObj->getName();
                } 
              }  
              
              $objs[$value->getName()]=[
                "value"=>$childObjs,
               "dopSettMetka"=> $settMain,
                //"uid"=>$value->getUid(),
                "name"=>$value->getName(),
                "type"=> "elem",
                "metka"=>$metkaMain
              ];         
            }
          }
           $data['name']=$parentType->getName();
            $data['value']=$objs;
          break;
        case 'blockObj':
          // тут наш конфиг где объекты находятся
          $_obj = new Obj();
        //  $dt=$_obj->getAlltest();
          $dt = $_obj->getObjsByClass(new Core\Type\Type($data['uidClass'], ''),'');
          $uidClass= $data['uidClass'];
          $uidConfigParam=array_column($data['value'],'name');
          $valueDBMetka=[];
          $nameArr=[];
          $data['name']='test';
         
          foreach ($dt['objs'] as $key => $value) {
              
                if(!in_array(trim($value->getName()),$nameArr))
                {
                  $key1=array_search($value->getName(), $uidConfigParam);
                  $tmpV=[];
                  $tmpV['uid']=$value->getUid();
                  $tmpV['type']='elem';
                  $tmpV['name']=$value->getName();
                //  $data['name']=$value['ClassName'];
                  if($key1!==false){
                    $tmpV['metka']=$data['value'][$key1]['metka'];
                  }else{
                    $tmpV['metka']='';
                  }
                  $newDopSett=[];
                foreach($tmpV['dopSettMetka'] as $valueDSM) 
                {
                     if(!empty($valueDSM))
                     {
                        $newDopSett[]=$valueDSM;
                     }

                }
               $tmpV['dopSettMetka']=$newDopSett;
                  $valueDBMetka[]=$tmpV;
                  $nameArr[]= trim($tmpV['name']);

                }
              
          }
          usort($valueDBMetka, function($a,$b){
              return strcmp($a['name'], $b['name']);
          });
           $data['value']=$valueDBMetka;
          break;  
        default:
          # code...
          break;
      }


      return $data;
  }

  public function newAction(){
    //0143ca21-bbf1-450f-bc6d-fc8057e7402b

     $loader = new \Phalcon\Loader();
     $loader->registerClasses(
        [
          'ElasticEmail' => APP_PATH . '/library/ElasticEmail.php'
        ]
    );
    $loader->register();
    $mail=\ElasticEmail::sendMail('test','puss95@yandex.by','testFFFFF');
    $ttt=\ElasticEmail::getStatusEmails(date_create('2020-08-25'),date_create('2020-08-27'));
  }


   public function testSettData($dt,&$resData,&$metkSettZam)
    {
      foreach($dt as $key => $value)
      {
        if($value['type']=='block')
        {
            $this->testSettData($value['value'],$resData,$metkSettZam);
        }else if(isset($value['style'])&&($value['type']=='blockForm'&&$value['style']=='h2'||$value['type']=='blockDB'||$value['type']=='blockObj')){
            foreach ($value['value'] as $val) {
              $resData[mb_strtolower($value['name'].'.'. $val['name'])]=$val['metka'];
              if(!empty($val['dopSettMetka']))
              {
                $metkSettZam[mb_strtolower($value['name'].'.'. $val['name'])]=array_column($val['dopSettMetka'], "selectedValue");
                if(mb_strtolower($value['name'])=='global operation')
                {
                  $metkSettZam[mb_strtolower($val['metka'])]=array_column($val['dopSettMetka'], "selectedValue");
                }
              }
              
            }
        }else if($value['type']=='blockForm'){
           $this->testSettData($value['value'],$resData,$metkSettZam);
        }else if(isset($value['style'])&&($value['type']=='blockCheck')){
           foreach ($value['value'] as $val) {
                $resData['Check.'.$value['name'].'.'. $val['name']]=$val['metka'];
                if(!empty($val['dopSettMetka']))
                {
                  $metkSettZam[mb_strtolower($value['name'].'.'. $val['name'])]=array_column($val['dopSettMetka'], "selectedValue");                
                }
              /*$resData['Check.'.$value['name'].'.'. $val['name']]=[
                'metka'=>$val['metka'],
                'settMetka'=>empty($val['dopSettMetka'])? []:$val['dopSettMetka'] ];*/
            }
        }else if($value['type']=='blockObjwithChild')
        {

          foreach ($value['value']as  $valueVVVVVVV) {
          //  TableDataBase.Additional blocks.Aménagement mis en place.Block elements.Cache spot

            $resData['TableDataBase.'.$value['name'].'.'.$valueVVVVVVV['name']]=$valueVVVVVVV['metka'];
            foreach ($valueVVVVVVV['value']['Block elements']['value'] as $keyvalueVVVVVVV => $valuevalueVVVVVVV) {

                $resData['TableDataBase.'.$value['name'].'.'.'Block elements'.$keyvalueVVVVVVV]=$valuevalueVVVVVVV['metka'];
            }
          }
          
        }
      }
    }


    public function stepFormEniTwoAction()
    {
        /*
        ,
          "883e3bb1-ba24-457f-acd5-9f495db1c347":{
            "type": "form",    
            "name": "global Cumac",
            "keys": ["spec","cumac"]
          },
          "7f78f29c-2165-478f-a0f8-bd1d474bf79f":{
            "type": "form",    
            "name": "Reste à Charge",
            "keys": ["spec","resteAcharge"]
          }
        */
      //  $testR=$this->request;
       
       $readSettFile=true;
        $userID=$this->session->get('logged')['User'];
          $pathFileSetting=BASE_PATH.'/config/dop/DataEnemat/settEnemat.json';
          $dataSett=[];
             if(file_exists($pathFileSetting))
             {
                $fileSetting=file_get_contents($pathFileSetting);
                $dataSett=json_decode($fileSetting,true);
                $dataSett['883e3bb1-ba24-457f-acd5-9f495db1c347']=[
                    "type"=> "form",    
                    "name"=>"global Cumac",
                    "keys"=>["spec","cumac"]
                ];
                 $dataSett['7f78f29c-2165-478f-a0f8-bd1d474bf79f']=[
                    "type"=> "form",    
                    "name"=>"Reste à Charge",
                    "keys"=>["spec","resteAcharge"]
                ];
              }
              $arrrrGlll1=[];
        if($_REQUEST['persone']=='Personne physique')
        {
           $_REQUEST['information_lieu_travaux']['nom_travaux']=$_REQUEST['beneficiaire']['nom'].' '.$_REQUEST['beneficiaire']['prenom']; 

        }
        $numeriFBenifs='';
        $refAvisFBenifs='';
        if(isset($_REQUEST['created_document'])&&$_REQUEST['created_document']=='devisah')
        {
            $_REQUEST['date']['date_de_signature_AH']=$_REQUEST['date']['date_de_signature'];
        }
        $cumacGlobal=0;
        $resteAchargeGlobal=0;
        if(!empty($_REQUEST['operation']))
        {
            $cumacGlobal=array_sum(array_column($_REQUEST['operation'], 'cumac'));
            $resteAchargeGlobal=array_sum(array_column($_REQUEST['operation'], 'reste_a_charge'));
            if($_REQUEST['persone']=='Personne physique')
            {
                $resteAchargeGlobal++;
            }
        }
       
        $requestData=$_REQUEST;
        
        $configParamJSON=file_get_contents(BASE_PATH.'/config/dop/DataEnemat/configDataSpecEnemat.json');
        $configParamData=json_decode($configParamJSON,true);




        $uidCLASS_OBJET='5086f503-d8b2-4a5b-a3d9-e5abc7ccb728';
        $uidCLASS_DEVIS='5a6364ee-e0de-4772-983f-9f992d57a8fb';
        $uidCLASS_AH='0207572e-779a-46eb-953f-90c64f98b697';
        $uidCLASS_FACTURE='cc056b7b-b1ba-45b0-a7e3-446bd321abf9';
        $uidCLASS_OPERATION='9887b9f5-81f9-4a40-9b9b-c0458701fa9d';
        $uidCLASS_ISOLANT_OPER='38e56551-7b85-47a2-985d-21589255b3de';
        $uidCLASS_Blocks_OPER='9a31a433-8b11-41dc-b480-4bee8ec00e57';
        $uidCLASS_ELEME_Blocks_OPER='d7f886f1-0ae0-4e8b-9f2d-66b6b77e07e2';
        $uidCLASS_ISOLANT_Blocks_OPER='10db6093-56c2-4c43-af67-afa8e6436317';
        $uidCLASS_PERSONNE_PHYSYQUE=  'e883e744-8718-4da3-8d09-d4fc500b55a4';
        $dateNow=date('d/m/Y H:i:s');
        //теперь получаем все параметры из БД корневого класса OBJET
        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new Core\Type\Type($uidCLASS_OBJET, ''));
        $_group = new AttrGroup();
        $_obj=new Obj();
        
        $type='';
        if(isset($requestData['created_document']))
        {
            switch ($requestData['created_document']) {
              case 'devis':
                $type='devis';
                break;
              case 'ah':
                 $type='ah';
                break;
              case 'facture':
                $type='facture';
                break;
              case 'devisah':
                $type='devisah';
                break;
              case 'factureah':
                $type='factureah';
                break;
            }
        }elseif(isset($requestData['edited_document'])){
          switch ($requestData['edited_document']) {
              case 'devis':
                $type='devis';
                break;
              case 'ah':
                 $type='ah';
                break;
              case 'facture':
                $type='facture';
                break;
              case 'factureah':
                $type='factureah';
                break;
            }
        }
        if($type=='facture')
        {
          //тепеь надо вытянуть и понять что у нас это factureAH
          $flagFactureAH=true;
          foreach ($_REQUEST['operation'] as $key => $value)        
          {
            $uidOperation=$value['operation']['uid'];
            $objOpeer=new \Dzeta\Core\Instance($uidOperation, '');
            $dataOpers=$_obj->getAllValues([$objOpeer])[$uidOperation];
            $tmpV=null;
            foreach ($dataOpers as  $valueOper) 
            {             
              switch ($valueOper->getUid()) 
              {
                case 'f23e965a-1493-4de1-8a49-dec86103de36':
                  if(!is_null($valueOper->getValue()))
                  {
                    $tmpV=$valueOper->getValue()->getUid();                   
                  }
                  break;
              }              
            }
            if($tmpV!='a29b83eb-495f-4b54-83f3-7ec6e161f5ad')
            {
              $flagFactureAH=false;
            }
          }
          if($flagFactureAH)
          {
            $type='factureah';
          }
        }
        
        $Objet_des_travaux=null;
        $dateArr=['agr_gen'=>'','inv_gen'=>''];
        $objDevisSSS=null;
        
        $group1 = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group1->setParent(new Core\Type\Type($uidCLASS_PERSONNE_PHYSYQUE, ''));
        $dataParamPersonMorale=$_group->getAttributes($group1);

        $dataJsonTaxFrom=[];
        if(isset($_REQUEST['beneficiaire'])&&!empty($_REQUEST['beneficiaire']['persone_phys_data']))
        {
            foreach ($_REQUEST['beneficiaire']['persone_phys_data']as $dataPersoneMorale_value) 
            {
                if(!empty($dataPersoneMorale_value['JSON']))
                {
                    $dataJsonTaxFrom=json_decode($dataPersoneMorale_value['JSON'],true);
                    break;
                }
            }
        }
        if($type=='devis')
        {            
            $this->writeDefBenificiare($_REQUEST['persone']);
          if(!isset($requestData['edited_document']))
          {
            $dataParamOBJET = $_group->getAttributes($group);
            $nDevis=$this->getLastDevis();
            $dataParamOBJET=$this->fillParamForFormData($dataParamOBJET,$configParamData,$requestData,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis]);
            $objOBJET = new \Dzeta\Core\Instance('', '');
            $objOBJET->setType(new Core\Type\Type($uidCLASS_OBJET, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
            $objOBJET->setAttributes($dataParamOBJET);
            $_obj->checkUniqueAndAddWithValues($objOBJET, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
            $Objet_des_travaux=$objOBJET;
           }else{
            $dataParamOBJET = $_group->getAttributes($group);
            $dataParamOBJET=$this->fillParamForFormData($dataParamOBJET,$configParamData,$requestData,['dateNow'=>null]);
            $objOBJET = new \Dzeta\Core\Instance($requestData['object_id'], '');
            $objOBJET->setType(new Core\Type\Type($uidCLASS_OBJET, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
            $objOBJET->setAttributes($dataParamOBJET);
            $_obj->checkUniqueAndAddWithValues($objOBJET, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
            $_obj->getParent($objOBJET);
            $Objet_des_travaux=$objOBJET->getParent();
            
            $objDevis_data1=$_obj->getAllValues([$Objet_des_travaux])[$Objet_des_travaux->getUid()];
            $dateNNN='';
            foreach ($objDevis_data1 as  $valueDevis) 
            {
                switch ($valueDevis->getUid()) 
                {
                   case '2cba0bab-fa5a-44bd-83d2-9f5d07e757e3':
                    if(!is_null($valueDevis->getValue()))
                    {
                      $dateNNN=$valueDevis->getValue()->getValue();
                    }
                    // 
                     break;
                   
                   default:
                     # code...
                     break;
                }
            }

            $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
            $group->setParent(new Core\Type\Type($uidCLASS_OBJET, ''));
            $dataParamOBJET = $_group->getAttributes($group);
            $dataParamOBJET=$this->fillParamForFormData($dataParamOBJET,$configParamData,$requestData,['dateNow'=>$dateNNN]);
            $Objet_des_travaux = new \Dzeta\Core\Instance($Objet_des_travaux->getUid(), '');
            $Objet_des_travaux->setType(new Core\Type\Type($uidCLASS_OBJET, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
            $Objet_des_travaux->setAttributes($dataParamOBJET);
            $_obj->addValues([$Objet_des_travaux],$this->session->get('logged')['User']);
            //тут надо удалять Facture и AH 
            $allAdditionalBloksOprDB=$_obj->getChildrenByType($Objet_des_travaux,new Core\Type\Type('cc056b7b-b1ba-45b0-a7e3-446bd321abf9', ''));
              //удаляем все доп блоки
            foreach ($allAdditionalBloksOprDB as $valueallAdditionalBloksOprDB) 
            {
                $_obj->remove($valueallAdditionalBloksOprDB, $userID);
            }
            $allAdditionalBloksOprDB=$_obj->getChildrenByType($Objet_des_travaux,new Core\Type\Type('0207572e-779a-46eb-953f-90c64f98b697', ''));
              //удаляем все доп блоки
            foreach ($allAdditionalBloksOprDB as $valueallAdditionalBloksOprDB) 
            {
                $_obj->remove($valueallAdditionalBloksOprDB, $userID);
            }
            
           }
           
           $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
           $group->setParent(new Core\Type\Type($uidCLASS_DEVIS, ''));
           $dataParamDEVIS = $_group->getAttributes($group);            
          if(isset($requestData['edited_document']))
          {
            $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>null]);
            $objROOT = new \Dzeta\Core\Instance($requestData['object_id'], '');
            $objROOT->setType(new Core\Type\Type($uidCLASS_DEVIS, ''))->setParent( $objOBJET, '');
            $objROOT->setAttributes($dataParamDEVIS);    
             $_obj->addValues([$objROOT],$this->session->get('logged')['User']);    
          }else{
           
            $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis]);
            $objROOT = new \Dzeta\Core\Instance('', '');
            $objROOT->setType(new Core\Type\Type($uidCLASS_DEVIS, ''))->setParent( $objOBJET, '');
            $objROOT->setAttributes($dataParamDEVIS);
            $_obj->checkUniqueAndAddWithValues($objROOT, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
          }
          $objDevisSSS=$objROOT;
         
          //тут добавляем еще блок Persone Morale
          $allPersonaleMoraleDB=$_obj->getChildrenByType($objDevisSSS,new Core\Type\Type($uidCLASS_PERSONNE_PHYSYQUE, ''));
          foreach ($allPersonaleMoraleDB as $valueallAdditionalBloksOprDB) 
          {
            $_obj->remove($valueallAdditionalBloksOprDB, $userID);
          } 
          if(isset($_REQUEST['beneficiaire'])&&!empty($_REQUEST['beneficiaire']['persone_phys_data']))
          {
            $dataPersoneMorale=$_REQUEST['beneficiaire']['persone_phys_data'];
            foreach ($dataPersoneMorale as $dataPersoneMorale_value) 
            {
                 $personMorale_obj=new \Dzeta\Core\Instance('', '');
                 $personMorale_obj->setType(new Core\Type\Type($uidCLASS_PERSONNE_PHYSYQUE, ''))->setParent($objDevisSSS, '');
                 $copyAttr=$dataParamPersonMorale;
                 if(isset($dataPersoneMorale_value['JSON']))
                 {
                    $json=json_decode($dataPersoneMorale_value['JSON'],true);
                    if(isset($json['common']))
                    {
                        if(isset($json['common']['Nombre de personne(s) a charge']))
                        {
                            $dataPersoneMorale_value['personnes']=$json['common']['Nombre de personne(s) a charge'];
                        }
                        if(isset($json['common']['Revenu fiscal de reference']))
                        {
                            $dataPersoneMorale_value['revenue']=preg_replace("/[^,.0-9]/", '', $json['common']['Revenu fiscal de reference']);
                        }
                    }
                 }
                 $this->fillParamForFormData($copyAttr,$configParamData,$dataPersoneMorale_value,['dateNow'=>$dateNow]);
                 $personMorale_obj->setAttributes($copyAttr);
                 $_obj->checkUniqueAndAddWithValues($personMorale_obj,\Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
            }
          }
        }elseif($type=='ah'){
            
            $this->stepFormGetDataJson();
            if(empty($requestData['edited_document']))
            {
              $objOBJET = new \Dzeta\Core\Instance($requestData['object_id'], '');
              $objOBJET->setType(new Core\Type\Type($uidCLASS_OBJET, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
                $_obj->getParent($objOBJET);
              $objOBJET=$objOBJET->getParent();
            }else{
              $objOBJET = new \Dzeta\Core\Instance($requestData['object_id'], '');
              $objOBJET->setType(new Core\Type\Type($uidCLASS_AH, ''));
              $_obj->getParent($objOBJET);
            }
            
            $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
            $group->setParent(new Core\Type\Type($uidCLASS_AH, ''));
            $dataParamDEVIS = $_group->getAttributes($group);
            if(isset($requestData['edited_document']))
            {
              $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>null]);
              $objROOT = new \Dzeta\Core\Instance($requestData['object_id'], '');
              $objROOT->setType(new Core\Type\Type($uidCLASS_AH, ''))->setParent( $objOBJET, '');
              $objROOT->setAttributes($dataParamDEVIS);
              $_obj->addValues([$objROOT], $this->session->get('logged')['User']);
            }else{
              $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>$dateNow]);
              $objROOT = new \Dzeta\Core\Instance('', '');
              $objROOT->setType(new Core\Type\Type($uidCLASS_AH, ''))->setParent($objOBJET, '');
              $objROOT->setAttributes($dataParamDEVIS);
              $_obj->checkUniqueAndAddWithValues($objROOT, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
            }
            //тут надо в объект objetDeTravaux добавить 1
            //;objOBJET
            $attrCompl = new Attribute('f9fa930f-6ad7-4e9d-a72b-6ff639caf095','Completed',1,false,false);
            $attrCompl->setValue(['value'=>1]);
            $objOBJET->setAttributes([$attrCompl]);
            $_obj->addValues([$objOBJET], $this->session->get('logged')['User']);
        }elseif($type=='facture'||$type=='factureah'){
            if(empty($requestData['edited_document']))
            {
              $objOBJET = new \Dzeta\Core\Instance($requestData['object_id'], '');
              $objOBJET->setType(new Core\Type\Type($uidCLASS_OBJET, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
              $_obj->getParent($objOBJET);
              
              $objOBJET=$objOBJET->getParent();
              $Objet_des_travaux=$objOBJET;
          }else{
              $objFacture=new \Dzeta\Core\Instance($requestData['object_id'], '');
              $objOBJET = new \Dzeta\Core\Instance($requestData['object_id'], '');
              $objOBJET->setType(new Core\Type\Type($uidCLASS_FACTURE, ''));
              $_obj->getParent($objOBJET);
              $Objet_des_travaux=$objOBJET->getParent();
              $allAdditionalBloksOprDB=$_obj->getChildrenByType($Objet_des_travaux,new Core\Type\Type('0207572e-779a-46eb-953f-90c64f98b697', ''));
              //удаляем все доп блоки
              foreach ($allAdditionalBloksOprDB as $valueallAdditionalBloksOprDB) 
              {
                  $_obj->remove($valueallAdditionalBloksOprDB, $userID);
              }
          }
          $attrCompl = new Attribute('f9fa930f-6ad7-4e9d-a72b-6ff639caf095','Completed',1,false,false);
            $attrCompl->setValue(['value'=>0]);
            $Objet_des_travaux->setAttributes([$attrCompl]);

            $_obj->addValues([$Objet_des_travaux], $this->session->get('logged')['User']);
           $objDevis_data=$_obj->getAllValues([$Objet_des_travaux])[$Objet_des_travaux->getUid()];
           $nDevis='';
           foreach ($objDevis_data as  $valueDevis) 
          {
             switch ($valueDevis->getUid()) {
               case 'da552880-9024-4acb-8764-7152df385cd0':
                if(!is_null($valueDevis->getValue()))
                {
                  $nDevis=$valueDevis->getValue()->getValue();
                }
                // 
                 break;
               
               default:
                 # code...
                 break;
             }
          }
            
            $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
            $group->setParent(new Core\Type\Type($uidCLASS_FACTURE, ''));
            $dataParamDEVIS = $_group->getAttributes($group);
            if(isset($requestData['edited_document']))
            {
              $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>null]);
              $objROOT = new \Dzeta\Core\Instance($requestData['object_id'], '');
              $objROOT->setType(new Core\Type\Type($uidCLASS_FACTURE, ''))->setParent( $objOBJET, '');
              $objROOT->setAttributes($dataParamDEVIS);
              $tttt=$_obj->addValues([$objROOT], $this->session->get('logged')['User']);
              $objuidFacturee=$objROOT->getUid();
            }else{
              $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis]);
              $objROOT = new \Dzeta\Core\Instance('', '');
              $objROOT->setType(new Core\Type\Type($uidCLASS_FACTURE, ''))->setParent( $objOBJET, '');
              $objROOT->setAttributes($dataParamDEVIS);
              $_obj->checkUniqueAndAddWithValues($objROOT, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
               $objuidFacturee=$objROOT->getUid();
               $objFacture=$objROOT;
            }
            if($type=='factureah')
            {
              $this->stepFormGetDataJson();             
              
              $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
              $group->setParent(new Core\Type\Type($uidCLASS_AH, ''));
              $dataParamDEVIS = $_group->getAttributes($group);
              
              $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>$dateNow]);
              $objAH = new \Dzeta\Core\Instance('', '');
              $objAH->setType(new Core\Type\Type($uidCLASS_AH, ''))->setParent($Objet_des_travaux, '');
              $objAH->setAttributes($dataParamDEVIS);
              $_obj->checkUniqueAndAddWithValues($objAH, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
              $attrCompl = new Attribute('f9fa930f-6ad7-4e9d-a72b-6ff639caf095','Completed',1,false,false);
              $attrCompl->setValue(['value'=>1]);
              $Objet_des_travaux->setAttributes([$attrCompl]);
              $_obj->addValues([$Objet_des_travaux], $this->session->get('logged')['User']);
            }
           
        }else if($type=='devisah')
        {
            $this->writeDefBenificiare($_REQUEST['persone']);
          $this->stepFormGetDataJson();
          $dataParamOBJET = $_group->getAttributes($group);
           $nDevis=$this->getLastDevis();
          $dataParamOBJET=$this->fillParamForFormData($dataParamOBJET,$configParamData,$requestData,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis,'complAH'=>1]);
          $objOBJET = new \Dzeta\Core\Instance('', '');
          $objOBJET->setType(new Core\Type\Type($uidCLASS_OBJET, ''))->setParent(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''));
          $objOBJET->setAttributes($dataParamOBJET);
          $_obj->checkUniqueAndAddWithValues($objOBJET, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
          $Objet_des_travaux=$objOBJET;
         
          $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
          $group->setParent(new Core\Type\Type($uidCLASS_FACTURE, ''));
          $dataParamDEVIS = $_group->getAttributes($group);
          $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis]);
          $objROOT = new \Dzeta\Core\Instance('', '');
          $objROOT->setType(new Core\Type\Type($uidCLASS_FACTURE, ''))->setParent($objOBJET, '');
          $objROOT->setAttributes($dataParamDEVIS);
          $_obj->checkUniqueAndAddWithValues($objROOT, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
          $objFacture=$objROOT;
          

          $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
          $group->setParent(new Core\Type\Type($uidCLASS_DEVIS, ''));
          $dataParamDEVIS = $_group->getAttributes($group);
         
          $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>$dateNow,'createDevis'=>1,'numDevis'=>$nDevis]);
          $objROOT = new \Dzeta\Core\Instance('', '');
          $objROOT->setType(new Core\Type\Type($uidCLASS_DEVIS, ''))->setParent($objOBJET, '');
          $objROOT->setAttributes($dataParamDEVIS);
          $_obj->checkUniqueAndAddWithValues($objROOT, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
          $objDevis=$objROOT;

         

          $allPersonaleMoraleDB=$_obj->getChildrenByType($objDevis,new Core\Type\Type($uidCLASS_PERSONNE_PHYSYQUE, ''));
          foreach ($allPersonaleMoraleDB as $valueallAdditionalBloksOprDB) 
          {
            $_obj->remove($valueallAdditionalBloksOprDB, $userID);
          } 
          if(isset($_REQUEST['beneficiaire'])&&!empty($_REQUEST['beneficiaire']['persone_phys_data']))
          {
            $dataPersoneMorale=$_REQUEST['beneficiaire']['persone_phys_data'];
            foreach ($dataPersoneMorale as $dataPersoneMorale_value) 
            {
                 $personMorale_obj=new \Dzeta\Core\Instance('', '');
                 $personMorale_obj->setType(new Core\Type\Type($uidCLASS_PERSONNE_PHYSYQUE, ''))->setParent($objDevis, '');
                 $copyAttr=$dataParamPersonMorale;
                 $this->fillParamForFormData($copyAttr,$configParamData,$dataPersoneMorale_value,['dateNow'=>$dateNow]);
                 $personMorale_obj->setAttributes($copyAttr);
                 $_obj->checkUniqueAndAddWithValues($personMorale_obj,\Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
            }
          }
          
          $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
          $group->setParent(new Core\Type\Type($uidCLASS_AH, ''));
          $dataParamDEVIS = $_group->getAttributes($group);

          $dataParamDEVIS=$this->fillParamForFormData($dataParamDEVIS,$configParamData,$requestData,['dateNow'=>$dateNow]);
          $objROOT = new \Dzeta\Core\Instance('', '');
          $objROOT->setType(new Core\Type\Type($uidCLASS_AH, ''))->setParent($objOBJET, '');
          $objROOT->setAttributes($dataParamDEVIS);
          $_obj->checkUniqueAndAddWithValues($objROOT, \Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
          $objAH=$objROOT;
        }
        $numDevissssss='';
        
        if($type=='devisah'||$type=='devis'||$type=='facture'||$type=='factureah')
        {
          //тут надо получить девис тот который там записан
          if($type=='devisah')
          {

             
            
              $oDevis=new \Dzeta\Core\Instance($objDevis->getUid(), '');
            $oDevis->setType(new Core\Type\Type($uidCLASS_DEVIS, ''));
            $objOBJET_data=$_obj->getAllValues([$oDevis])[$oDevis->getUid()];
            foreach ($objOBJET_data as  $valueDevis) 
            {
             
               switch ($valueDevis->getUid()) {
                 case '25e2e72e-b4b3-451a-a1f1-2dcf8c393e65':
                 
                  if(!is_null($valueDevis->getValue()))
                  {                                   
                    $dateArr['agr_gen']=$valueDevis->getValue()->getValueN()->format('d/m/Y');
                  } 
                  break;
                 
                 default:
                   # code...
                   break;
               }
            }
            $oTravauxx=new \Dzeta\Core\Instance($Objet_des_travaux->getUid(), '');
            $oTravauxx->setType(new Core\Type\Type($uidCLASS_OBJET, ''));
             $objDevis_data=$_obj->getAllValues([$oTravauxx])[$oTravauxx->getUid()];
           
             $numDevissssss='';
            foreach ($objDevis_data as  $valueDevis) 
            {
             
               switch ($valueDevis->getUid()) {
                 case 'da552880-9024-4acb-8764-7152df385cd0':
                 
                   if(!is_null($valueDevis->getValue()))
                    {
                      $numDevissssss=$valueDevis->getValue()->getValue();
                    }
                  break;
                  
               }
            }

          }else if($type=='devis')
          {
            
            $oDevis=new \Dzeta\Core\Instance($objDevisSSS->getUid(), '');
            $objDevisSSS->setType(new Core\Type\Type($uidCLASS_DEVIS, ''));
            $objOBJET_data=$_obj->getAllValues([$oDevis])[$oDevis->getUid()];
            
            $oTravauxx=new \Dzeta\Core\Instance($Objet_des_travaux->getUid(), '');
            $oTravauxx->setType(new Core\Type\Type($uidCLASS_OBJET, ''));
             $objDevis_data=$_obj->getAllValues([$oTravauxx])[$oTravauxx->getUid()];
           
             $numDevissssss='';
            

            foreach ($objOBJET_data as  $valueDevis) 
            {
             
               switch ($valueDevis->getUid()) {
                 case '25e2e72e-b4b3-451a-a1f1-2dcf8c393e65':
                 
                  if(!is_null($valueDevis->getValue()))
                  {                                   
                    $dateArr['agr_gen']=$valueDevis->getValue()->getValueN()->format('d/m/Y');
                  } 
                  break;
                  
               }
            }
           
            
            foreach ($objDevis_data as  $valueDevis) 
            {
             
               switch ($valueDevis->getUid()) {
                 case 'da552880-9024-4acb-8764-7152df385cd0':
                 
                   if(!is_null($valueDevis->getValue()))
                    {
                      $numDevissssss=$valueDevis->getValue()->getValue();
                    }
                  break;
                  
               }
            }
          
          }else if($type=='facture'||$type=='factureah')
          {
                $oDevis_=new \Dzeta\Core\Instance($_REQUEST['devis_id'], '');
                $oDevis_data=$_obj->getAllValues([$oDevis_])[$oDevis_->getUid()]; 
                foreach ($oDevis_data as  $valueDevis) 
                {
                 
                   switch ($valueDevis->getUid()) {
                     case '25e2e72e-b4b3-451a-a1f1-2dcf8c393e65':
                     
                      if(!is_null($valueDevis->getValue()))
                      {                                   
                        $dateArr['agr_gen']=$valueDevis->getValue()->getValueN()->format('d/m/Y');
                      } 
                      break;
                     
                     default:
                       # code...
                       break;
                   }
                }
             
            
           
              $objDevisSSS=new \Dzeta\Core\Instance($objuidFacturee, '');
            $objDevisSSS->setType(new Core\Type\Type($uidCLASS_FACTURE, ''));
            $objOBJET_data=$_obj->getAllValues([$objDevisSSS])[$objDevisSSS->getUid()];
            foreach ($objOBJET_data as  $valueDevis) 
            {
             
               switch ($valueDevis->getUid()) {
                 case '135c63bd-2a11-41c7-ba8d-0734ee71cadf':
                 
                  if(!is_null($valueDevis->getValue()))
                  {                                   
                    $dateArr['inv_gen']=$valueDevis->getValue()->getValueN()->format('d/m/Y');
                  } 
                  break;
                 
                 default:
                   # code...
                   break;
               }
            }
            $oTravauxx=new \Dzeta\Core\Instance($Objet_des_travaux->getUid(), '');
            $oTravauxx->setType(new Core\Type\Type($uidCLASS_OBJET, ''));
             $objDevis_data=$_obj->getAllValues([$oTravauxx])[$oTravauxx->getUid()];
           
             $numDevissssss='';
             foreach ($objDevis_data as  $valueDevis) 
            {
             
               switch ($valueDevis->getUid()) {
                 case 'da552880-9024-4acb-8764-7152df385cd0':
                 
                   if(!is_null($valueDevis->getValue()))
                    {
                      $numDevissssss=$valueDevis->getValue()->getValue();
                    }
                  break;
                  
               }
            }

          }
          
        }
        
        //теперь операции, они самые веселые и трудные при создании так как массивы там
        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new Core\Type\Type($uidCLASS_OPERATION, ''));
        $dataParamOPERATION = $_group->getAttributes($group);

        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new Core\Type\Type($uidCLASS_ISOLANT_OPER, ''));
        $dataParamISOLANT = $_group->getAttributes($group);
        $allOperationDB=$_obj->getChildrenByType($objROOT,new Core\Type\Type($uidCLASS_OPERATION, ''));
        $allDB_UID=[];
        foreach ($allOperationDB as  $vue) {
           $allDB_UID[]=$vue->getUid();
        }
        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new Core\Type\Type($uidCLASS_Blocks_OPER, ''));
        $dataParamOperationBlocks=$_group->getAttributes($group);

        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new Core\Type\Type($uidCLASS_ELEME_Blocks_OPER, ''));
        $dataParamOperationElemBlocks=$_group->getAttributes($group);

        $group = new \Dzeta\Core\Type\AttributeGroup(\Dzeta\Core\Type\AttributeGroup::ATTRIBUTE_GROUP_ROOT, '');
        $group->setParent(new Core\Type\Type($uidCLASS_ISOLANT_Blocks_OPER, ''));
        $dataParamIsolantElemBlocks=$_group->getAttributes($group);

        if($_REQUEST['persone']=='Personne physique')
        {
           $_REQUEST['information_lieu_travaux']['nom_travaux']=''; 
           
        }

        if(isset($requestData['operation']))
        {
          if($type=='devisah')
          {
            $objArr=[];
            $objArr[]=$objFacture;
            $objArr[]=$objDevis;
            $objArr[]=$objAH;
          }elseif($type=='factureah')
          {
            $objArr=[];
            $objArr[]=$objFacture;
            $objArr[]=$objAH;
          }else{
            $objArr=[];
            $objArr[]=$objROOT;
          }
          $numDevOrigggg=$numDevissssss;
          foreach($objArr as $numOperrD=> $objROOT)
          {

            $numDevissssss=$numDevOrigggg;
            if($type=='devis'||$type=='devisah')
            {
              $numDevissssss='D'.$numDevissssss;
            }
            if($type=='facture')
            {
              $numDevissssss='F'.$numDevissssss;
              $_REQUEST['date']['numero_facture']=$numDevissssss;
            }
            if($type=='factureah'&&$numOperrD==0)
            {
              $numDevissssss='F'.$numDevissssss;
              $_REQUEST['date']['numero_facture']=$numDevissssss;
            }else if($type=='factureah'&&$numOperrD==1)
            {
              $numDevissssss='D'.$numDevissssss;
            }
            foreach ($requestData['operation'] as $key => $valueOperation) 
            {
              if($type=='factureah'&&$numOperrD==1)
              {
                $valueOperation['linked_obj']=null;
              }
              if(!empty($valueOperation['linked_obj']))
              {
                  $oper=new \Dzeta\Core\Instance($valueOperation['linked_obj'], '');
                  $oper->setType(new Core\Type\Type($uidCLASS_OPERATION, ''))->setParent($objROOT, '');
                  $copyAttr=$dataParamOPERATION ;
                  $this->fillParamForFormData($copyAttr,$configParamData,$valueOperation,['dateNow'=>null]);
              }else{
                  $oper=new \Dzeta\Core\Instance('', '');
                  $oper->setType(new Core\Type\Type($uidCLASS_OPERATION, ''))->setParent($objROOT, '');
                  $copyAttr=$dataParamOPERATION ;
                  $this->fillParamForFormData($copyAttr,$configParamData,$valueOperation,['dateNow'=>$dateNow]);
              }

              $oper->setAttributes($copyAttr);
              if(!empty($valueOperation['linked_obj']))
              {
                 $_obj->addValues([$oper],$this->session->get('logged')['User']);
              }else{
                 $_obj->checkUniqueAndAddWithValues($oper,\Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
              }
              //тут надо создавать доп объекты-
              $allAdditionalBloksOprDB=$_obj->getChildrenByType($oper,new Core\Type\Type($uidCLASS_Blocks_OPER, ''));
              //удаляем все доп блоки
              foreach ($allAdditionalBloksOprDB as $valueallAdditionalBloksOprDB) {
                  $_obj->remove($valueallAdditionalBloksOprDB, $userID);
              }

              if(!empty($valueOperation['additional_blocks']))
              {
                
                  foreach ($valueOperation['additional_blocks'] as  $vlue) {
                    $operBloks=new \Dzeta\Core\Instance('', '');
                    $operBloks->setType(new Core\Type\Type($uidCLASS_Blocks_OPER, ''))->setParent($oper, '');
                    $copyAttr=$dataParamOperationBlocks ;
                    $this->fillParamForFormData($copyAttr,$configParamData,$vlue,['dateNow'=>$dateNow]);
                    $operBloks->setAttributes($copyAttr);
                    $_obj->checkUniqueAndAddWithValues($operBloks,\Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);                   
                    if(!empty($vlue['data']))
                    {
                      foreach ($vlue['data'] as  $valuevlue) {
                      $operBlockElem=new \Dzeta\Core\Instance('', '');
                      $operBlockElem->setType(new Core\Type\Type($uidCLASS_ELEME_Blocks_OPER, ''))->setParent($operBloks, '');
                      $copyAttr=$dataParamOperationElemBlocks ;                    
                      $this->fillParamForFormData($copyAttr,$configParamData,$valuevlue,['dateNow'=>$dateNow]);
                      $operBlockElem->setAttributes($copyAttr);
                      $_obj->checkUniqueAndAddWithValues($operBlockElem,\Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
                    }
                  }
                }
              }
              
              $allIsolant=[];

              $allIsolandDB=$_obj->getChildrenByType($oper,new Core\Type\Type($uidCLASS_ISOLANT_OPER, ''));
              foreach ($allIsolandDB as  $vue) {
                $allDB_UID[]=$vue->getUid();
              }
              $uidSearch=array_search($oper->getUid(), $allDB_UID);
              if($uidSearch!==false)
              {
                unset($allDB_UID[$uidSearch]);
              }
              if(isset($valueOperation['isolant']))
              {

                foreach($valueOperation['isolant'] as $valueIsolant)
                {
                  if($type=='factureah'&&$numOperrD==1)
                  {
                    $valueIsolant['linked_obj']=null;
                  }
                  if(!empty($valueIsolant["linked_obj"]))
                  {
                    $isol=new \Dzeta\Core\Instance($valueIsolant["linked_obj"], '');
                    $isol->setType(new Core\Type\Type($uidCLASS_ISOLANT_OPER, ''))->setParent($oper, '');
                    $copyAttr=$dataParamISOLANT;
                    $this->fillParamForFormData($copyAttr,$configParamData,$valueIsolant,['dateNow'=>null]);
                  }else{
                    $isol=new \Dzeta\Core\Instance('', '');
                    $isol->setType(new Core\Type\Type($uidCLASS_ISOLANT_OPER, ''))->setParent($oper, '');
                    $copyAttr=$dataParamISOLANT;
                    $this->fillParamForFormData($copyAttr,$configParamData,$valueIsolant,['dateNow'=>$dateNow]);                   
                  }               

                  $isol->setAttributes($copyAttr);
                  if(!empty($valueIsolant["linked_obj"]))
                  {
                    $_obj->addValues([$isol],$this->session->get('logged')['User']);
                  }else{
                     $_obj->checkUniqueAndAddWithValues($isol,\Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
                  }
                   $allAdditionalBloksIsolDB=$_obj->getChildrenByType($isol,new Core\Type\Type($uidCLASS_ISOLANT_Blocks_OPER, ''));
                  //удаляем все доп блоки
                  foreach ($allAdditionalBloksIsolDB as $valueallAdditionalBloksOprDB) {
                      $_obj->remove($valueallAdditionalBloksOprDB, $userID);
                  }
                  
                  if(!empty($valueIsolant['adv_size']))
                  {
                    foreach ($valueIsolant['adv_size'] as $valuIsolant) {
                       $IsolBloks=new \Dzeta\Core\Instance('', '');
                    $IsolBloks->setType(new Core\Type\Type($uidCLASS_ISOLANT_Blocks_OPER, ''))->setParent($isol, '');
                    $copyAttr=$dataParamIsolantElemBlocks ;
                    $this->fillParamForFormData($copyAttr,$configParamData,$valuIsolant,['dateNow'=>$dateNow]);

                    $IsolBloks->setAttributes($copyAttr);
                    $_obj->checkUniqueAndAddWithValues($IsolBloks,\Dzeta\Core\Instance::ADDED_BY_USER,$this->session->get('logged')['User']);
                    
                    }
                  }
                  $uidSearch=array_search($isol->getUid(),$allDB_UID);
                  if($uidSearch!==false)
                  {
                    unset($allDB_UID[$uidSearch]);
                  }
                  $allIsolant[]= $isol;

                }
              }
              $allOper[]=['operation'=>$oper,'isolant'=>$allIsolant];
            }
          }   
        }
         $userID=$this->session->get('logged')['User'];
         $delObj=[];
         foreach ($allDB_UID as $value) {
            $_obj->remove(new \Dzeta\Core\Instance($value, ''), $userID);
            
         }
         if(!empty( $delOb))
         {
         // $_obj->removeMany($delObj, $userID);
         }
         
        
        
        //getAllValues(array $objs)
        $requestData=$_REQUEST;
        if(!empty($requestData['operation'])&&($type=='ah'||$type=='devisah'))
        {
          foreach ($requestData['operation'] as $keyO => $value) 
          {            
            if(!(//$value['operation']['name']=='BAT-TH-155 - échangeur à plaques'||
               $value['operation']['name']=='BAT-TH-155'||
               //$value['operation']['name']=='BAR-TH-161 - échangeur à plaques'||  
               $value['operation']['name']=='BAR-TH-161'
              ))
            {
              $newArrIsol=[];
              if(!empty($requestData['operation'][$keyO]['isolant']))
              {
                foreach ($requestData['operation'][$keyO]['isolant'] as $keyI => $valueI) 
                {
                  if(!empty($requestData['operation'][$keyO]['isolant'][$keyI]['adv_size']))
                  {
                    $aSizeU=[];
                    foreach ($requestData['operation'][$keyO]['isolant'][$keyI]['adv_size'] as $keyAS => $valueAS) 
                    {
                      if(isset($valueAS['ep']))
                      {
                        if(isset($aSizeU[$valueAS['ep']]))
                        {
                          $aSizeU[$valueAS['ep']][]=$valueAS;
                        }else{
                          $aSizeU[$valueAS['ep']]=[];
                          $aSizeU[$valueAS['ep']][]=$valueAS;
                        }
                      }
                    }
                    if(!empty($aSizeU))
                    {
                      foreach ($aSizeU as $keyASn => $valueASn) 
                      {
                        $tmpIsol=[];
                        $tmpIsol=$valueI;
                        $tmpIsol['value']=-2;
                        $tmpIsol['surface']=array_sum(array_column($valueASn, 'l'));
                        unset($tmpIsol['adv_size']);
                        unset($tmpIsol['linked_obj']);
                        $tmpIsol['adv_size']=$valueASn;
                        $tmpIsol['advParams']['Epaisseur']['value']=$keyASn;
                        $newArrIsol[]=$tmpIsol;
                      }

                    }else{
                      $newArrIsol[]=$valueI;
                    }
                  }else{
                    $newArrIsol[]=$valueI;
                  }
                }
              }
              $requestData['operation'][$keyO]['isolant']=$newArrIsol;
            }
            
          }
        }

        //$this->request=$requestData;
        
        $obj = new \Dzeta\Core\Instance($userID->getUid(), '');
        $_obj = new Obj();
        $data = $_obj->getAllValues([$obj])[$userID->getUid()];
        $dataUserValue=$data;
        $view = new Core\App\View('6f6e2d17-9a9e-47e1-beea-9af82e8ee975', '', []);
          $view->setDopSett('attrSort', new Core\Type\Attribute('e6141b74-7a8d-4dc5-99ac-e5f3c672550e', '', 1, false, false));
          $view->setDopSett('sort', 1);
          $view->setDopSett('num', 100000);
          $view->setDopSett('start', 1);
          $_view=new View();
          $dt=$_view->View_GetData($view,$this->session->get('logged')['User']->getUid());
          $dataInt=[];
          if(!empty($dt[0])&&!empty($dt[0]['dt']))
          {

            $dataInt=json_decode($dt[0]['dt'],true);
          }

          $dtatCoefIntr=[];

          foreach ($dataInt as $key => $value) {

            $dtatCoefIntr[$value['e6141b74-7a8d-4dc5-99ac-e5f3c672550e']['v']]=[
              'AE'=>$value['f758f57d-f5be-47c2-b78d-1cf8ae6ba442']['v'],
              'AD'=>$value['3e104fc9-e8e7-4d5d-b4e5-7894b62e79ac']['v']
            ];
          }

        $dataUserr=[
          'Login'=>null,
          'RAISON SOCIALE MO'=>null,
          'SIRET MO'=>null,
          'ADRESSE MO'=>null,
          'CP MO'=>null,
          'VILLE MO'=>null,
          'Tel MO'=>null,
          'MOBILE MO'=>null,
          'EMAIL MO'=>null,
          'NOM MO'=>null,
          'PRENOM MO'=>null,
          'FONCTION MO'=>null,
          'LOGO'=>null,
          'Capital social MO'=>null,
          'COEF'=>null,
          'Logo upload'=>null,
          'site internet'=>null,

        ];
        foreach ($data as $key => $value)
        {
          switch ($value->getUid())
              {
                case '106fc108-41d1-4474-9fe4-04b9536e8bc4':
                    $dataUserr['Login'] = $value;
                    $dataUserr['EMAIL MO'] = $value;
                break;
                case 'e845ff95-991e-4be3-817a-9058cdd5fcda':
                    $dataUserr['RAISON SOCIALE MO'] = $value;
                break;
                case '8d2658e0-533a-4e1d-a065-ccaaf5b30b43':
                    $dataUserr['SIRET MO'] = $value;
                break;
                case '04dc8310-1f6e-4199-a6b5-f2c0838363f6':
                    $dataUserr['ADRESSE MO'] = $value;
                break;
                case '9c1170a6-f0a9-49d0-92db-055f318046d0':
                    $dataUserr['CP MO'] = $value;
                break;
                case 'a94a5300-6f7e-4265-8191-f0565be63e7a':
                    $dataUserr['VILLE MO'] = $value;
                break;
                case 'a477fccb-de27-45c3-8761-594636e049d6':
                    $dataUserr['Tel MO'] = $value;
                break;
                case 'e19311fe-79d2-4e90-9e60-41eacd59f1cf':
                    $dataUserr['MOBILE MO'] = $value;
                break;
              /*  case '20926719-6376-4ed7-9ef3-a2ed4c03d94d':
                    $dataUserr['EMAIL MO'] = $value;
                break;*/
                case '31ac52ec-739d-444f-a35d-bcf888c9e7ad':
                    $dataUserr['NOM MO'] = $value;
                break;
                case 'd45023e2-8e6b-4693-a6c3-8368f526c88d':
                    $dataUserr['PRENOM MO'] = $value;
                break;
                case '848f8242-d836-442f-845f-a324af4d198b':
                    $dataUserr['FONCTION MO'] = $value;
                break;
                case '6e8c9fcd-1d93-4395-8b90-cf7f4cd27bb6':
                    $dataUserr['LOGO'] = $value;
                break;
                case '5d0b982c-a8e6-4020-959a-0190fa60826e':
                    $dataUserr['Capital social MO'] =$value;
                break;
                case '9ba785f0-6f74-4dd7-8e54-3c55712fd720':
                    if(!empty($value->getValue()))
                    {
                      $dataUserr['COEF'] = $value->getValue()->getValue();
                    }

                break;
                case '609972d3-9d0a-4d7a-8380-e55617b49873':
                    $dataUserr['Logo upload'] = $value;
                break;
                case '251a6ac4-50cb-448a-bc74-bdd37ceaf97d':
                    $dataUserr['site internet'] = $value;
                break;

              }
        }

        //b6dd2cfb-6256-43d9-951d-6edc2838a2bb
        $premiumCoefUser=$_obj->getChildrenByType($obj, new Core\Type\Type('b6dd2cfb-6256-43d9-951d-6edc2838a2bb', ''));
        $dtOperChildRangeV = $_obj->getAllValues($premiumCoefUser);
        $userCoef=[];
        foreach ($dtOperChildRangeV as  $attrsAll) {
           $tmp=[];
            foreach ($attrsAll as  $valAtttr)
            {
                $attrUid=$valAtttr->getUid();
                if($attrUid=='06b7e94f-31ae-4cd0-8b5a-30d4eeb8c8b0')
                {

                   $tmp['nameScen']=$valAtttr->getValue()->getName();
                }elseif($attrUid=='1390beb3-7f7d-4eeb-a737-b40501d017cc')
                {
                  $tmp['coef']=$valAtttr->getValue()->getValue();
                }
            }
            $userCoef[$tmp['nameScen']]=$tmp;
         }
        $result=-1;
        $dtF=[];
        $key=0;

         $metkSpec_FoundationLenvalNice='';
          $defaultUnit='';
          $defaultTax='';
          $defaultMaxValue=20000;

        $uidObj_settt=$this->CONFIG_SETT['Sous-traite']['obj'];
          $obj_settt=new \Dzeta\Core\Instance($uidObj_settt, '');
          $attrSettings=$_obj->getAllValues([$obj_settt])[$uidObj_settt];
          
          foreach ($attrSettings as $value) {
            switch ( $value->getUid()) {
              case $this->CONFIG_SETT['Sous-traite']['attr']:
                if(!is_null($value->getValue()))
                {
                  $metkSpec_FoundationLenvalNice=$value->getValue()->getValue();
                }
                break;              
            }
          }

          $uidObj_settt=$this->CONFIG_SETT['Default-unit']['obj'];
          $obj_settt=new \Dzeta\Core\Instance($uidObj_settt, '');
          $attrSettings=$_obj->getAllValues([$obj_settt])[$uidObj_settt];
          
          foreach ($attrSettings as $value) {
            switch ( $value->getUid()) {
              case $this->CONFIG_SETT['Default-unit']['attr']:
                if(!is_null($value->getValue()))
                {
                  $defaultUnit=$value->getValue()->getValue();
                }
                break;              
            }
          }

          $uidObj_settt=$this->CONFIG_SETT['Tax']['obj'];
          $obj_settt=new \Dzeta\Core\Instance($uidObj_settt, '');
          $attrSettings=$_obj->getAllValues([$obj_settt])[$uidObj_settt];
          
          foreach ($attrSettings as $value) {
            switch ( $value->getUid()) {
              case $this->CONFIG_SETT['Tax']['attr']:
                if(!is_null($value->getValue()))
                {
                  $defaultTax=$value->getValue()->getValue();
                }
                break;              
            }
          }

           $uidObj_settt=$this->CONFIG_SETT['myPranovLimit']['obj'];
          $obj_settt=new \Dzeta\Core\Instance($uidObj_settt, '');
          $attrSettings=$_obj->getAllValues([$obj_settt])[$uidObj_settt];
          
          foreach ($attrSettings as $value) {
            switch ( $value->getUid()) {
              case $this->CONFIG_SETT['myPranovLimit']['attr']:
                if(!is_null($value->getValue()))
                {
                  $defaultMaxValue=$value->getValue()->getValue();
                }
                break;              
            }
          }

         
        //массив в котором данные для последнего объекта
        $attrDataFinalData=[];
        if ($this->request->isPost()) {
          $operations=!empty($requestData['operation'])?$requestData['operation']:[] ;
          $beneficiaire=$requestData['beneficiaire'];
          $organisation=$requestData['organization'] ;
          $Sous_traitance=!empty($requestData['sous_traitance'])?$requestData['sous_traitance']:['nom'=>'','prenom'=>'','raison_sociale'=>'','siret'=>''];
          $dateClint=$requestData['date'] ;
          $information_lieu_travaux=$requestData['information_lieu_travaux'] ;
          $urlPage1=[];
          $urlPage2=[];
          $urlPrevisite=[];
          $urlGeneralConditions=[];
          $operFirst=null;
        //  $acode=count($operations)>1? true:false;
          $acode=false;
          if(count($operations)>1)
          {
            $acode=true;
          }elseif(count($operations)==1)
          {
            if(!empty($operations[0]['isolant']))
            {
              if(count($operations[0]['isolant'])>1)
              {
                $acode=true;
              }
            }
          }
          
          //теперь надо вычислить цветовую палитру нашего
            $attrBenificiare=null;
            $colorOper=null;
            $colorOperCoef=null;
            $colorOperArr=[];
            if(isset($beneficiaire['number_de_perosne']))
            {
                //1. вычисляем Париж не Париж
                $codePostalBenif=$beneficiaire['code_postale'];
                $codePostalBenifCode = substr($codePostalBenif, 0, 2);
                $parisCode=false;
                if(in_array($codePostalBenifCode, ['75','77','78','91','92','93','94','95']))
                {
                    $parisCode=true;
                }                
                $priceMax=$beneficiaire['revenue'];  
                $dateNownow=new \DateTime();
                //теперь ищем наш цвет
                if(!empty($priceMax))
                {
                    $allObjectRevenus=$_obj->getChildrenByType(new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, ''),new Core\Type\Type('8e641eb0-c9d7-43d6-b650-fb36bd3395e8', ''));
                    $_obj->getAllValues($allObjectRevenus);
                    $uidAttr=[
                        'DateStart'=>'a061ff68-dd56-4d1f-8492-8b04f9a4c6e7',
                        'DateFinis'=>'459ea47e-2b25-4b4e-845c-11f42d55b949'
                    ];
                    $parentUidRevenus=null;
                    foreach($allObjectRevenus as $value) 
                    {
                        $attributesAll=$value->getAttributes();
                        $dtStart=null;
                        $dtFinis=null;
                        foreach ($attributesAll as $v) 
                        {
                            if(!is_null($v->getValue()))
                            {
                                if($v->getUid()==$uidAttr['DateStart'])
                                {
                                    $dtStart=$v->getValue()->getValueN();
                                }elseif($v->getUid()==$uidAttr['DateFinis'])
                                {
                                    $dtFinis=$v->getValue()->getValueN(); 
                                }                            
                            }                            
                        }
                        
                        if(!is_null($dtStart)&&!is_null($dtFinis))
                        {
                            if($dtStart<$dateNownow&&$dateNownow<$dtFinis)
                            {
                                $parentUidRevenus=$value->getUid();
                                break;
                            }
                        }
                    }
                    
                    if(!is_null($parentUidRevenus))
                    {
                        $allPlafondsderevenus=$_obj->getChildrenByType(new \Dzeta\Core\Instance($parentUidRevenus, ''),new Core\Type\Type('6eb8b809-82fb-46cd-a6fe-9d881adcdb1c', ''));
                        $_obj->getAllValues($allPlafondsderevenus);
                        $attrUidsDerevenus=[
                            'countAttr'=>'a5ff96e5-fa00-4d8b-99c5-1243d23fbff7',
                            'parisPriceBleu'=>'f0de6bb2-436e-4f22-8123-9e1750a187f7',
                            'parisPriceJaune'=>'480834bd-221e-4b59-b6f4-854b69f3d5a2',
                            'parisPriceViolet'=>'8c67de11-ea60-4cb8-a4fc-b95e7ecc93c5',
                            'nparisPriceBleu'=>'d759fca7-06ac-40c1-bbac-7f1313798c39',
                            'nparisPriceJaune'=>'86e2af96-2d92-408e-83d8-9715acaa88cf',
                            'nparisPriceViolet'=>'175dec8b-fac7-4aa0-976c-0386eccd8798',
                        ];
                        $indArr=[];

                        foreach ($allPlafondsderevenus as  $value) 
                        {
                            $attributesAll=$value->getAttributes();
                            $countAttr=null;
                            $priceBleu=null;
                            $priceJaune=null;
                            $priceViolet=null;
                            foreach ($attributesAll as $val) 
                            {
                                if(!is_null($val->getValue()))
                                {
                                    if($val->getUid()==$attrUidsDerevenus['countAttr'])
                                    {
                                        $countAttr=$val->getValue()->getValue();                                    
                                    }else if(($val->getUid()==$attrUidsDerevenus['parisPriceBleu'])&&$parisCode)
                                    {
                                        $priceBleu=$val->getValue()->getValue(); 
                                    }else if(($val->getUid()==$attrUidsDerevenus['parisPriceJaune'])&&$parisCode)
                                    {
                                        $priceJaune=$val->getValue()->getValue(); 
                                    }else if(($val->getUid()==$attrUidsDerevenus['parisPriceViolet'])&&$parisCode)
                                    {
                                        $priceViolet=$val->getValue()->getValue(); 
                                    }else if(($val->getUid()==$attrUidsDerevenus['nparisPriceBleu'])&&!$parisCode)
                                    {
                                        $priceBleu=$val->getValue()->getValue(); 
                                    }else if(($val->getUid()==$attrUidsDerevenus['nparisPriceJaune'])&&!$parisCode)
                                    {
                                        $priceJaune=$val->getValue()->getValue(); 
                                    }else if(($val->getUid()==$attrUidsDerevenus['parisPriceViolet'])&&!$parisCode)
                                    {
                                        $priceViolet=$val->getValue()->getValue(); 
                                    }
                                }                            
                            }

                            if(!is_null($countAttr)&&!is_null($priceBleu)&&!is_null($priceJaune)&&!is_null($priceViolet))
                            {
                                $indArr[$countAttr]=['blue'=>$priceBleu,'jaune'=>$priceJaune,'violet'=>$priceViolet];
                            }
                        }
                       
                        $priceMaxColor=null;
                        if(!empty($indArr))
                        {
                            //теперь считаем сумму                        
                            if(isset($indArr[$beneficiaire['number_de_perosne']]))
                            {
                                $priceMaxColor=$indArr[$beneficiaire['number_de_perosne']];
                            }else
                            {
                                $arrKeyss=array_keys($indArr);
                                $maxKey=max($arrKeyss);
                                if($maxKey<$beneficiaire['number_de_perosne'])
                                {
                                    $delta=$beneficiaire['number_de_perosne']-$maxKey;
                                    $priceMaxColor['blue']=$delta*$indArr[0]['blue']+$indArr[$maxKey]['blue'];
                                    $priceMaxColor['jaune']=$delta*$indArr[0]['jaune']+$indArr[$maxKey]['jaune'];
                                    $priceMaxColor['violet']=$delta*$indArr[0]['violet']+$indArr[$maxKey]['violet'];
                                }
                            }
                        }
                        
                        if(!is_null($priceMaxColor))
                        {
                          $keyColorOperr=null;
                            if($priceMaxColor['blue']>=$priceMax)
                            {
                                $colorOper='881163b7-4558-4d7c-ac64-62aeceab12d3';
                                $keyColorOperr='myPranovBlue';
                            }elseif($priceMaxColor['blue']<$priceMax&&$priceMaxColor['jaune']>=$priceMax)
                            {
                                $colorOper='d9939a08-0026-4177-b1be-d470eba6cb56';
                                $keyColorOperr='myPranovJaune';
                            }elseif($priceMaxColor['jaune']<$priceMax&&$priceMaxColor['violet']>=$priceMax)
                            {
                                $colorOper='7ba6bfc0-f73c-4486-a157-e02723d5474f';
                                $keyColorOperr='myPranovViolet';
                            }elseif($priceMaxColor['violet']<$priceMax)
                            {
                                $colorOper='acdef3c0-3fd4-4cd9-b927-387e6e6aef62';
                                $keyColorOperr='myPranovRose';
                            }
                           
                            if(!is_null($keyColorOperr))
                            {
                              $uidObj_settt=$this->CONFIG_SETT[$keyColorOperr]['obj'];
                              $obj_settt=new \Dzeta\Core\Instance($uidObj_settt, '');
                              $attrSettings=$_obj->getAllValues([$obj_settt])[$uidObj_settt];
                              
                              foreach ($attrSettings as $value) 
                              {
                                switch ( $value->getUid()) {
                                  case $this->CONFIG_SETT[$keyColorOperr]['attr']:
                                    if(!is_null($value->getValue()))
                                    {
                                      $colorOperCoef=$value->getValue()->getValue();
                                    }
                                    break;              
                                }
                              }
                            }
                          
                        }                            
                    }
                }
                
            }
          
          
          $numDevis=null;
          
          $preEN101='';
          $preEN103='';
          $preVisitteGlobal=empty($beneficiaire['previsite'])? '':$beneficiaire['previsite'];
          $isolant=null;
          $buro=null;
          $taxFirst=null;
          $surFace=[];
          $referencerapport=[];
          $rSIREN=[];$DateVal=[];$norganisme=[];$namePD=[];
          $typeDateRange=new Core\Type\Type('89a9922b-5c91-42bb-b8bf-19f14b0bb20f', '');
          $dateDeviss=!empty($dateClint['date_de_signature'])?\DateTime::createFromFormat(\Dzeta\Core\Value\DateTimeV::DATE_FORMAT_new,$dateClint['date_de_signature']) :new DateTime();
          $attrOperations=[];
          //тут пишется данные из prime
          $IncitationArr=[];
          //это наш Cumac
          $CumacArr=[];
          $OperationTable=[
            'operTable'=>[
              'Table.Operation.Title'=>[],
              'Table.Operation.Text for Agreement and Invoice'=>[],//это берем из бд
              'Table.Operations.Isolant.Marque'=>[],//это из изолянта
              'Table.Operations.Isolant.Reference'=>[],//это из изолянта
              'Table.Operations.Isolant.Diametre'=>[],//это пока не заполняем
              'Table.Operations.Isolant.Epaisseur'=>[],//это из изолянта
              'Table.Operations.Isolant.Résistance thermique'=>[],//это из изолянта
              'Table.Operations.Isolant.ACERMI'=>[],//это из изолянта
              'Table.Operation.Surface'=>[],
              'Table.Operations.Isolant.Surface'=>[],
              'Table.Operations.Isolant.Incitation'=>[],
              'Table.Incitation'=>[],
              'Table.TTC'=>[]
            ]
          ];
       /*   $OperationTable['operTable']['Table.Operation.Title'][]=$voper['operation']['name'];
                  $OperationTable['operTable']['Table.Operations.Isolant.Surface'][]=$voper['operation']['name']=='BAR-TH-160'||$voper['operation']['name']=='BAR-TH-146'? '': $surfaceIsolant;
                  $OperationTable['operTable']['Table.Operations.Isolant.Incitation'][]=!empty($ttc)?$ttc*$surfaceIsolant:'';
                  $OperationTable['operTable']['Table.TTC'][]= $ttc;
                  $OperationTable['operTable']['Table.Operations.Isolant.Marque'][]=$MarqueTmp;
                  $OperationTable['operTable']['Table.Operations.Isolant.Reference'][]=$ReferenceTmp;
                  $OperationTable['operTable']['Table.Operations.Isolant.Epaisseur'][]=$EpaisseurTmp;
                  $OperationTable['operTable']['Table.Operations.Isolant.Diametre']*/
          $operTitle=[];
          $numOperattt=-1;
          $flagTax=false;
         $urlEXCEL=[];
          
          $operationFirstMetka=[];
          foreach($operations as  $voper)
          {
              $numOperattt++;
            $operTitle[]=$voper['operation']['name'];
            $curGlobalData=[];
            $resThermiqArr=[];//[];
            $EpaisseurArr=[];//[];
             $resMarqueArr=[];
            $Attribute3Arr=[];
              $AcermiArr=[];
                  $ReferenceArr=[];
                   $SurfaceArr=[];
            $isolant=null;
            $buro=null;
            $resThermiq='';//[];
            $surFace='';
             $MarqueTmp  =''   ;
             $ReferenceTmp='';
            $referencerapport='';
            $rSIREN='';$DateVal='';$norganisme='';$namePD='';
            $preEN103='';
            $uidO=$voper['operation']['uid'];
            $operFirst1=$voper['operation']['name'];
            $IncitationArr[]=$voper['cost_client'];
            $CumacArr[]=$voper['value']*$voper['surface'];
            $checkArr=[];
            $attrIsolant=[];
            $Secteurd='';
            $Nergie_de_chauffage='';
            $Type_de_pose='';
            $specOperIsolant=null;
            $sectDeActivity=null;
            $sevtDeActivityText='';
            $checkText=[
                'Secteur d’activité'=>'',
                'Type de pose'=>'',
                'Énergie de chauffage'=>'',
                'Type de logement'=>'',
                'Zone climatique'=>'',
                'Type'=>'',
                'Montant en kWh cumac par m2 d’isolant posé'=>'',
                'La température du fluide caloporteur'=>''
            ];
             if(empty($operFirst))
            {
              $operFirst=substr($operFirst1, 0, 3);
            }
            if(!empty($voper['objUid']))
            {
              foreach ($voper['objUid'] as  $valCheck)
              {
                $checkArr[]='Check.'.$valCheck['label'].'.'.$valCheck['name'];
              //  $checkArr[]='Check. '.$valCheck['label'].'.'.$valCheck['name'];
               if($valCheck['name']=='Électricité')
               {
                $specOperIsolant=0;
               }elseif($valCheck['name']=='Combustible'){
                 $specOperIsolant=1;
               }
               if($operFirst1=='BAR-TH-160'||$operFirst1=='BAT-TH-146')
               {
                $specOperIsolant=1;
               }
                if($valCheck['label']=='Énergie de chauffage'&&$Nergie_de_chauffage=='')
                {
                  $Nergie_de_chauffage=$valCheck['name'];
                }
                if($valCheck['label']=='Type de pose')
                {
                  $Type_de_pose=$valCheck['name'];
                }//Type de pose
                if($valCheck['label']=='Secteur d’activité')
                {
                  $sectDeActivity=$valCheck['name'];
                  $sevtDeActivityText=$valCheck['name'];
                  switch ($sectDeActivity) {
                    case 'Autres secteurs':
                      $sectDeActivity=3;
                      break;
                    case 'Bureaux':
                      $sectDeActivity=0;
                      break;
                    case 'Enseignement':
                      $sectDeActivity=0;
                      break;
                    case 'Hôtellerie / Restauration':
                      $sectDeActivity=1;
                      break;
                    case 'Santé':
                      $sectDeActivity=2;
                      if($operFirst=='BAT')
                      {
                          //$flagTax=true;
                      }
                      break;
                    default:
                       $sectDeActivity=null;
                      break;
                  }
                }
                if(isset($checkText[$valCheck['label']]))
                {
                    $checkText[$valCheck['label']]=$valCheck['name'];
                }
              }
            }
            if($_REQUEST['persone']!='Personne physique'&&$operFirst=='BAT')
            {
                //значит юрик  и смотрим такс фрии
                if(isset($beneficiaire['raison_sociale']))
                {
                    $textForTax='EHPAD';
                    $textSpec=mb_strtoupper($beneficiaire['raison_sociale']);
                    $textSpecArr=explode(' ',$textSpec);
                    if(in_array($textForTax, $textSpecArr))
                    {                   
                        $flagTax=true;
                    }
                    $textForTax='(EHPAD)';
                    $textSpec=mb_strtoupper($beneficiaire['raison_sociale']);
                    $textSpecArr=explode(' ',$textSpec);
                    if(in_array($textForTax, $textSpecArr))
                    {                   
                        $flagTax=true;
                    }
                }
                
            }
           
            if(isset($voper['surface']))
            {
              $surFace=$voper['surface'];
            }
            $o11=new \Dzeta\Core\Instance($uidO, '');
            $dataO11 = $_obj->getAllValues([$o11])[$voper['operation']['uid']];
            $dataPage1Oper=null;
            $dataPage2Oper=null;
             $OPERtITLE='';
            $oPERtITLEtEXT='';
            $Text_for_Agreement_and_Invoice='';
            $Text_for_Diameter_and_Length='';
            $IsolantDiametre=[];
            $IsolantLentgh=[];
            $IsolantEpass=[];
            $operation_TAX=$defaultTax;
            $operation_Unit=$defaultUnit;
            $urllPrevisitte=null;
            $urllGenCond=null;
            $urllGeneralOperationATTESTATIOSIMPLIF=null;
            foreach ($dataO11 as $key => $valAttrrr) {
              if($valAttrrr->getUid()=='7762042b-7025-4a1b-aef9-a8be70e9edb6')
              {
                $dataPage1Oper=$valAttrrr->getValue();
              }
              if($valAttrrr->getUid()=='d6c7a437-23a6-49f1-8605-22312c8a7cc2')
              {
                $dataPage2Oper=$valAttrrr->getValue();
              }
              if($valAttrrr->getUid()=='f5d60a0a-bb00-4bc2-a7db-f1cec9c4e7f4')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $OPERtITLE=$valAttrrr->getValue()->getValue();
                }

              }
              if($valAttrrr->getUid()=='296185ab-746d-47fa-bf74-cf9b881172cc')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $oPERtITLEtEXT=$valAttrrr->getValue()->getValue();
                }

              }
              if($valAttrrr->getUid()=='b38f8a48-9886-40ca-bb80-c5953104a070')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $Text_for_Agreement_and_Invoice=$valAttrrr->getValue()->getValue();
                }

              }
              if($valAttrrr->getUid()=='0a951c6b-3f44-4694-b0a8-7687f1890540')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $Text_for_Diameter_and_Length=$valAttrrr->getValue()->getValue();
                }
              }
              if($valAttrrr->getUid()=='b14c4168-d6d4-453a-8d9d-ef4560751fbe')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $operation_TAX=$valAttrrr->getValue()->getValue();
                }
              }
              if($valAttrrr->getUid()=='704e7d7f-2a1c-4848-8ae0-0a59b488e175')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                   $operation_Unit=$valAttrrr->getValue()->getValue();
                }
              }
              if($valAttrrr->getUid()=='a8f77ef2-d278-470f-8313-bc2eebb48340')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                    $urllPrevisitte=$valAttrrr->getValue()->getValue();
                }
              }
              if($valAttrrr->getUid()=='c55004bd-6914-432b-9e65-725909262a7a')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                    $urllGenCond=$valAttrrr->getValue()->getValue();
                }
              }
              if($valAttrrr->getUid()=='42aa0f47-06b1-4464-8a3b-47127515b570')
              {
                if(!is_null($valAttrrr->getValue()))
                {
                    $urllGeneralOperationATTESTATIOSIMPLIF=$valAttrrr->getValue()->getValue();
                }
              }
            }
            if($flagTax)
            {
                $operation_TAX=5.5;
            }
            $resThermiqArr=[];
              $EpaisseurArr=[];
              $resMarqueArr=[];
              $ReferenceArr=[];
              $SurfaceArr=[];
              $AcermiArr=[];
              $Attribute3Arr=[];
            $EpasseurBlockSpecArr=[];
            $countDiametre=[];
            $dtOperChildRange=$_obj->getChildrenByType($o11, $typeDateRange);
            $priceOper=null;
            $priceMaprenov=null;
            if(!empty($dtOperChildRange))
            {
              $dtOperChildRangeV = $_obj->getAllValues($dtOperChildRange);
              foreach ($dtOperChildRangeV as  $attrsAll) 
              {                
                $priceOper=null;
                foreach ($attrsAll as  $valAtttr)
                {
                  $attrUid=$valAtttr->getUid();
                    if($attrUid==$colorOper)
                    {
                        $priceOper=$valAtttr->getValue();
                    }
                }
              }
            }
            if(!is_null($priceOper)&&!is_null($colorOperCoef))
            {
              //тут надо аккуратно считать
              $price1=$colorOperCoef*$voper['reste_a_charge'];
              $price2=$priceOper->getValue()*$voper['surface'];                        
              $priceMaprenov=$price1>$price2? $price2:$price1;
            }
            if(!empty($voper['isolant']))
            {
              foreach ($voper['isolant'] as $key => $valIsolant)
              {
                   $surfaceIsolant= $valIsolant['surface'] ;
                    $resThermiqTmp='';
                    $EpaisseurTmp='';
                    $attrribute3TMP='';
                    $MarqueTmp='';
                    $ReferenceTmp='';
                    $ACERMItmp='';
                    $IsolantDiametre=[];
                    $IsolantLentgh=[];
                    $IsolantEpass=[];
                    $countForDiameter=
                    [
                      '100'=>
                      [
                        '20_65'=>0,
                        '65_100'=>0,
                        '100'=>0,
                        'countD'=>0
                      ],
                      '200'=>
                      [
                        '20_65'=>0,
                        '65_100'=>0,
                        '100'=>0,
                        'countD'=>0
                      ]
                    ];
                    if(!empty($valIsolant['adv_size']))
                    {
                        $lastEpp=null;
                        $tSpecnew=0;
                        if(isset($voper['objUid']))
                        {
                          foreach ($voper['objUid'] as $valueOuid) 
                          {
                            if($valueOuid['name']=='50°C ≤ Tfluide ≤ 120°C')
                            {
                              $tSpecnew=100;
                            }elseif($valueOuid['name']=='Tfluide > 120°C')
                            {
                              $tSpecnew=200;
                            }
                          }
                        }
                        //теперь смена идет температуры ее надо брать в другом месте
                      foreach ($valIsolant['adv_size'] as  $valueAdvSize) 
                      {
                        $IsolantDiametre[]=$valueAdvSize['d'];
                        $IsolantLentgh[]=$valueAdvSize['l'];
                        
                        if(20<=$valueAdvSize['d']&&$valueAdvSize['d']<=65)
                        {
                          if($tSpecnew==100)
                          {
                            $countForDiameter['100']['20_65']+=$valueAdvSize['l'];
                          }else if($tSpecnew==200)
                          {
                            $countForDiameter['200']['20_65']+=$valueAdvSize['l'];
                          }
                        }else if(65<$valueAdvSize['d']&&$valueAdvSize['d']<=100)
                        {
                          if($tSpecnew==100)
                          {
                            $countForDiameter['100']['65_100']+=$valueAdvSize['l'];
                          }else if($tSpecnew==200)
                          {
                            $countForDiameter['200']['65_100']+=$valueAdvSize['l'];
                          }
                        }else if($valueAdvSize['d']>100)
                        {
                          if($tSpecnew==100)
                          {
                            $countForDiameter['100']['100']+=$valueAdvSize['l'];
                          }else if($tSpecnew==200)
                          {
                            $countForDiameter['200']['100']+=$valueAdvSize['l'];
                          }
                        }
                        /*if($tSpecnew==100)
                        {
                          $countForDiameter['100']['countD']+=$valueAdvSize['l'];
                        }else if($tSpecnew==200)
                        {
                          $countForDiameter['200']['countD']+=$valueAdvSize['l'];
                        }*/
                        if($lastEpp!=$valueAdvSize['ep'])
                        {
                            $EpasseurBlockSpecArr[]=$valueAdvSize['ep'];
                            $lastEpp=$valueAdvSize['ep'];
                        }
                        if(//$OPERtITLE=='BAT-TH-155 - échangeur à plaques'||
                           $OPERtITLE=='BAT-TH-155'||
                          // $OPERtITLE=='BAR-TH-161 - échangeur à plaques'||
                           $OPERtITLE=='BAR-TH-161'
                         )
                        {
                          if($tSpecnew==100)
                          {
                            $IsolantEpass[]='50°C ≤ T ≤ 120°C';
                          }else if($tSpecnew==200)
                          {
                            $IsolantEpass[]='T > 120°C';
                          }else
                          {
                            $IsolantEpass[]=$valueAdvSize['ep'];
                          }
                        }else
                        {
                          $IsolantEpass[]=$valueAdvSize['ep'];
                        }
                        
                      }
                    
                    }
                    //BAR-TH-161 - échangeur à plaques BAT-TH-155 - échangeur à plaques
                    if(isset($voper['objUid'])&&($OPERtITLE=='BAR-TH-161 - échangeur à plaques'||$OPERtITLE=='BAT-TH-155 - échangeur à plaques'))
                    {
                      $tSpecnew=0;
                      foreach ($voper['objUid'] as $valueOuid) 
                      {
                        if($valueOuid['name']=='50°C ≤ Tfluide ≤ 120°C')
                        {
                          $tSpecnew=100;
                          break;
                        }elseif($valueOuid['name']=='Tfluide > 120°C')
                        {
                          $tSpecnew=200;
                          break;
                        }
                      }
                      if($tSpecnew==100)
                      {
                        $countForDiameter['100']['countD']+=$surfaceIsolant;
                      }else if($tSpecnew==200)
                      {
                        $countForDiameter['200']['countD']+=$surfaceIsolant;
                      }
                    }
                  if($valIsolant['value']==-2)
                  {
                   
                    //advParams
                    foreach ($valIsolant['advParams'] as $keyAdvParams=>$valueIsolantStat)
                    {
                        if($keyAdvParams=='Marque')
                        {
                           $MarqueTmp=$valueIsolantStat['value'];
                        }
                        if($keyAdvParams=='Reference')
                        {
                           $ReferenceTmp=$valueIsolantStat['value'];
                        }
                        if($keyAdvParams=='Attribute 1')
                        {
                           $EpaisseurTmp=$valueIsolantStat['value'];
                        }
                        if($keyAdvParams=='Attribute 2')
                        {
                           $resThermiqTmp=$valueIsolantStat['value'];
                        }
                        if($keyAdvParams=='Attribute 3')
                        {
                           $attrribute3TMP=$valueIsolantStat['value'];
                        }
                        
                    }

                  }else{
                    $isolant=new \Dzeta\Core\Instance($valIsolant['value'], '');
                    $isolant=$_obj->getAllValues([$isolant])[$valIsolant['value']];

                    if(!empty($isolant))
                    {
                      foreach ($isolant as  $valIsolant1)
                      {
                        if($valIsolant1->getUid()=='1ff21b10-95c3-4557-aa9e-6e46a3d17c27')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                            $resThermiqTmp=$valIsolant1->getValue()->getValue();
                          }
                        }elseif($valIsolant1->getUid()=='86eeb716-de19-46a2-8b63-636aa8ae6da0')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                            $EpaisseurTmp=$valIsolant1->getValue()->getValue();
                          }
                        }elseif($valIsolant1->getUid()=='202fd14c-ea8e-4043-aa5c-a616641b7bae')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                            $MarqueTmp=$valIsolant1->getValue()->getName();
                          }
                        }elseif($valIsolant1->getUid()=='95cf9bce-3782-4b4c-bb4d-bb98a3708e58')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                             $ReferenceTmp=$valIsolant1->getValue()->getValue();
                          }
                        }elseif($valIsolant1->getUid()=='3c65e6db-8289-4d86-8e9d-66e9c39cc47f')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                             $ACERMItmp=$valIsolant1->getValue()->getValue();
                          }
                        }elseif($valIsolant1->getUid()=='6d3db389-dd94-4b2d-9f11-cde3418b9ec8')
                        {
                          if(!is_null($valIsolant1->getValue()))
                          {
                             $attrribute3TMP=$valIsolant1->getValue()->getValue();
                          }
                        }

                      }
                      $attrIsolant[]=[
                      [
                        'attr'=>'f1003f85-7512-4a73-88e6-589804ef9bea',
                        'name'=>'Isolant',
                        'datatype'=>'object',
                        'key'=>true,
                        'value'=>$valIsolant['value']
                      ],
                      [
                        'attr'=>'c9d1ae62-63cb-43b7-ba93-3eac1f9a3082',
                        'name'=>'Surface',
                        'datatype'=>'number',
                        'key'=>false,
                        'value'=>$valIsolant['surface']
                      ],

                    ];
                  }



                  }

                  $metkUnitt=$dataSett[0]['value'][3]['value'][0]['value'][2]['value'][9]['metka'];
                  //var_dump([$voper['cost_client'],$surFace]);
                  $ttc=empty($surFace)?null:$voper['cost_client']/$surFace;
                  $OperationTable['operTable']['Table.Operation.Title'][]=$voper['operation']['name'];
                  
                  $OperationTable['operTable'][$metkUnitt][]=$operation_Unit;

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][5]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['Secteur d’activité'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][6]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['Type de logement'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][7]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['Zone climatique'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][8]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['Type de pose'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][9]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['Énergie de chauffage'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][15]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['La température du fluide caloporteur'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][10]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['Type'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][11]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$checkText['Montant en kWh cumac par m2 d’isolant posé'];

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][12]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$this->conevrtSpecFormat($voper['reste_a_charge']);

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][13]['metka'];                  
                  $OperationTable['operTable'][$metkSectDeActi][]=$this->conevrtSpecFormat($voper['cumac']);

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][14]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$this->conevrtSpecFormat(round(($voper['prix_work']*$voper['surface']),2));
                  

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][0]['value'][2]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=empty($surfaceIsolant)? null:$this->conevrtSpecFormat(round(($surfaceIsolant*$voper['prix_unitaire']),2));

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][0]['value'][3]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$this->conevrtSpecFormat(round($voper['prix_unitaire'],2));

                  $metkSectDeActi=$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][16]['metka'];
                  $OperationTable['operTable'][$metkSectDeActi][]=$this->conevrtSpecFormat($priceMaprenov);


                  $OperationTable['operTable']['Table.Operation.Text title'][]=$oPERtITLEtEXT;
                  $OperationTable['operTable']['Table.Operations.Isolant.Surface'][]=empty($surfaceIsolant)? :$this->conevrtSpecFormat(round($surfaceIsolant,2));
                  $OperationTable['operTable']['Table.Operations.Isolant.Incitation'][]=!empty($ttc)?$this->conevrtSpecFormat(round($ttc*$surfaceIsolant,2)):'';
                  $OperationTable['operTable']['Table.Operations.Isolant.ACERMI'][]=$ACERMItmp;
                  $OperationTable['operTable']['Table.TTC'][]= empty($ttc)? null:$this->conevrtSpecFormat(round($voper['cost_client']/$surFace,2));
                  $OperationTable['operTable']['Table.Operations.Isolant.Marque'][]=$MarqueTmp;
                  $OperationTable['operTable']['Table.Operations.Isolant.Attribute 3'][]=$attrribute3TMP;
                   $OperationTable['operTable']['Table.Operations.Isolant.Résistance thermique'][]=empty($resThermiqTmp)? :str_replace('.',',',round($resThermiqTmp,2).'');
                  $OperationTable['operTable']['Table.Operations.Isolant.Reference'][]=$ReferenceTmp;
                  $OperationTable['operTable']['Table.Operations.Isolant.Epaisseur'][]=$voper['operation']['name']=='BAR-TH-160'||$voper['operation']['name']=='BAT-TH-146'? '': $EpaisseurTmp;
                  $OperationTable['operTable']['Table.Operations.Isolant.Diametre'][]=$voper['operation']['name']=='BAR-TH-160'||$voper['operation']['name']=='BAT-TH-146'?  $EpaisseurTmp:'';
                  $OperationTable['operTable']['Table.Operation.Text for Agreement and Invoice'][]=$Text_for_Agreement_and_Invoice;//
                  $objOperrr=new \Dzeta\Core\Instance($uidO, '');
                  $childAdditionBlocks=$_obj->getChildrenByType($objOperrr, new Core\Type\Type('bc85f46f-7fdd-496e-bfe0-1ac578cb66c1', ''));
                  $metkPusto=[];                
                
                  $metkDiametre=$dataSett[0]['value'][3]['value'][0]['value'][3]['value'][0]['metka'];
                  $metkLength=$dataSett[0]['value'][3]['value'][0]['value'][3]['value'][1]['metka'];
                  $metkEpp=$dataSett[0]['value'][3]['value'][0]['value'][3]['value'][2]['metka'];
                  $textAggrrrr=[];
                 
                  foreach ($IsolantDiametre as $kkkkkk=> $valekkkkkk) {
                    $tmpp=str_replace('{{'.$metkLength.'}}', $IsolantLentgh[$kkkkkk], $Text_for_Diameter_and_Length);
                     $tmpp=str_replace('{{'.$metkDiametre.'}}', $IsolantDiametre[$kkkkkk], $tmpp);
                     $tmpp=str_replace('{{'.$metkEpp.'}}', $IsolantEpass[$kkkkkk], $tmpp);
                     $textAggrrrr[]= $tmpp;
                  }
                   $OperationTable['operTable']['Table.Operation.Text for Diameter and Length'][]=implode('</w:t><w:br/><w:t xml:space="preserve">',$textAggrrrr);
                   $OperationTable['operTable']['List.Operation.Text for Diameter and Length'][]=implode('</w:t><w:br/><w:t xml:space="preserve">',$textAggrrrr);
                  
                    $OperationTable['operTable'][$dataSett[0]['value'][3]['value'][0]['value'][1]['value'][3]['metka']][]=implode('</w:t><w:br/><w:t xml:space="preserve">',$textAggrrrr);
                   $curGlobalData[]=[
                        'Isolant-Surface (m2)'=>$valIsolant['surface'],
                        'Isolant-Résistance Thermique'=>$resThermiqTmp,
                        'Isolant-Energie de chauffage'=>$EpaisseurTmp,
                        'Isolant-Marque'=> $MarqueTmp,
                       'Isolant-Reference'=> $ReferenceTmp,


                    ];
                  $resThermiqArr[]=is_numeric($resThermiqTmp)?str_replace('.',',',round($resThermiqTmp,2).''):$resThermiqTmp;
                  $EpaisseurArr[]=is_numeric($EpaisseurTmp)?str_replace('.',',',round($EpaisseurTmp,2).''):$EpaisseurTmp;
                  $resMarqueArr[]=is_numeric($MarqueTmp)?str_replace('.',',',round($MarqueTmp,2).''):$MarqueTmp;
                  $Attribute3Arr[]=is_numeric($attrribute3TMP)?str_replace('.',',',round($attrribute3TMP,2).''):$attrribute3TMP;
                  $ReferenceArr[]=$ReferenceTmp;
                  $SurfaceArr[]=$surfaceIsolant;//str_replace('.',',',round($surfaceIsolant,2).'');
                  $AcermiArr[]= $ACERMItmp;
                  $countDiametre[]=$countForDiameter;
              }

            }
            if(!empty($voper['bureaudecontrole']))
            {
              $buro=new \Dzeta\Core\Instance($voper['bureaudecontrole'], '');
              $buro=$_obj->getAllValues([$buro])[$voper['bureaudecontrole']];
              if(!empty($buro))
              {
                foreach ($buro as  $valueBuro)
                {
                  if($valueBuro->getUid()=='d1ce27ae-6927-4907-ae2c-5ef2e8f2730e')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $rSIREN=$valueBuro->getValue()->getValue();
                    }
                  }
                  if($valueBuro->getUid()=='0ace0f27-8a56-4e63-aca1-db94f39a87df')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $DateVal=$valueBuro->getValue()->getValueN()->format('d/m/Y');
                    }
                  }
                  if($valueBuro->getUid()=='d64797ca-4088-4f12-a190-e66ce3add196')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $norganisme=$valueBuro->getValue()->getValue();
                    }
                  }
                  if($valueBuro->getUid()=='df69b461-c591-4a7d-813d-99265abc7b73')
                  {
                    if(!is_null($valueBuro->getValue()))
                    {
                        $namePD=$valueBuro->getValue()->getValue();
                    }
                  }
                }
              }
            }
            
            if(isset($voper['referencerapport']))
            {
              $referencerapport=$voper['referencerapport'];
            }
            
            
            //$dtOperChildRange=$_obj->getChildrenByType($o11, $typeDateRange);
            $flP1=true;$flP2=true;
            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][12]['metka'];
            $operArr=  [
                'Operations.Bureau de controle.Date de validite'=>$DateVal,
                'Operations.Bureau de controle.Numero accreditation cofrac'=>$norganisme,
                'Operations.Bureau de controle.RAISON SOCIALE'=>$namePD,
                'Operations.Bureau de controle.SIREN'=>substr(str_replace(' ','',$rSIREN),0,9),
                'Operations.Isolant.Résistance thermique'=>str_replace('.', ',', round($resThermiq,2).''),
                'Operations.Référence rapport'=>str_replace('.', ',', round($referencerapport,2).''),
                'Operations.Surface'=>$this->conevrtSpecFormat(round($surFace,2)),
                'Operations.Pre visite'=>$preVisitteGlobal,
                'Incitation'=>$this->conevrtSpecFormat($voper['cost_client']),
                'Operations.CheckArray'=>$checkArr,
                'Operation.Title'=>$OPERtITLE,
                'Operation.Text title'=>$oPERtITLEtEXT,
                'Operation.Tax'=>$operation_TAX                
            ];
            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][12]['metka'];
            $operArr[$metkSectDeActi]=$checkText['Secteur d’activité'];

            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][13]['metka'];
            $operArr[$metkSectDeActi]=$checkText['Type de logement'];

            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][14]['metka'];
            $operArr[$metkSectDeActi]=$checkText['Zone climatique'];

            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][15]['metka'];
            $operArr[$metkSectDeActi]=$checkText['Type de pose'];

            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][16]['metka'];
            $operArr[$metkSectDeActi]=$checkText['Énergie de chauffage'];

            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][17]['metka'];
            $operArr[$metkSectDeActi]=$checkText['Type'];

            $metkSectDeActi=$dataSett[0]['value'][0]['value'][2]['value'][18]['metka'];
            $operArr[$metkSectDeActi]=$checkText['Montant en kWh cumac par m2 d’isolant posé'];

            if($numOperattt==0)
            {
                //$operationFirstMetka
                $metkSectDeActi=$dataSett[0]['value'][0]['value'][7]['value'][0]['metka'];
                $operationFirstMetka[$metkSectDeActi]=$checkText['Secteur d’activité'];

                $metkSectDeActi=$dataSett[0]['value'][0]['value'][7]['value'][1]['metka'];
                $operationFirstMetka[$metkSectDeActi]=$checkText['Énergie de chauffage'];
            }
            if(is_null($taxFirst))
            {
              $taxFirst= $operation_TAX;
            }
            $checkOnArr=[];
            if(!empty($voper['additional_blocks']))
            {
              $addSettMetk=[];
              if(isset($dataSett[0]['value'][4]['value'][0]['value']))
              {
                $addSettMetk=$dataSett[0]['value'][4]['value'][0]['value'];
              }
              
              foreach ($addSettMetk as $valueaddSettMetk) {
                 foreach ($valueaddSettMetk['value']['Block elements']['value'] as  $valuvlueee) 
                 {          
                    if(empty($valuvlueee['metka']))
                    {
                        continue;
                    }         
                    $exxp=explode('.',$valuvlueee['metka']);
                    if(isset($exxp[0])&&$exxp[0]=='CheckON')
                    {
                      //var_dump($valuvlueee['metka']);
                      $checkOnArr[]=$valuvlueee['metka'];
                    }else
                    {
                      $checkOnArr[]='CheckON.'.$valuvlueee['metka'];  
                      $operArr[$valuvlueee['metka']]= '';
                      $OperationTable['operTable'][$valuvlueee['metka']][$numOperattt]='';
                    }
                    
                   //$arrrrGlll1[$valuvlueee['metka']]= '';
                 }
                 $operArr[$valueaddSettMetk['metka']]= '';
                         $OperationTable['operTable'][$valueaddSettMetk['metka']][$numOperattt]='';
                 //  $arrrrGlll1[$valueaddSettMetk['metka']]= '';
              }
             
              //$addSettMetk[$nameP]['value']['Block elements']['value']
              // var_dump($voper['additional_blocks']);
              foreach ($voper['additional_blocks'] as $valueData) 
              {
                  $nameP=$valueData['name'];
                  $uidPP=$valueData['uid'];
                  $childrenData=empty($valueData['data'])? []:$valueData['data'];
                 
                  
                  if(empty($addSettMetk[$nameP]))
                  {
                   
                    continue;
                  }
                  if(empty($addSettMetk[$nameP]['metka']))
                  {
                    
                    continue;
                  }
                  $exxp=explode('.',$addSettMetk[$nameP]['metka']);
                  if(isset($exxp[0])&&$exxp[0]=='CheckON')
                  {
                    if(in_array($addSettMetk[$nameP]['metka'],$checkOnArr))
                    {
                      $kSearchh=array_search($addSettMetk[$nameP]['metka'],$checkOnArr);
                      $operArr['Operations.CheckArray'][]=$addSettMetk[$nameP]['metka'].'.OUI';
                      unset($checkOnArr[$kSearchh]);
                    }
                  }else
                  {
                    $kSearchh=array_search('CheckON.'.$addSettMetk[$nameP]['metka'],$checkOnArr);
                    $operArr['Operations.CheckArray'][]='CheckON.'.$addSettMetk[$nameP]['metka'].'.OUI';
                    unset($checkOnArr[$kSearchh]);
                  }
                  //тут делаем запрос в бд и берем значение
                  $valMetkDoppp='';
                  $oAddDop=new \Dzeta\Core\Instance($uidPP, '');
                  $dataAddDop = $_obj->getAllValues([$oAddDop])[$uidPP];
                  foreach ($dataAddDop as  $valuedataAddDop) {
                    switch ($valuedataAddDop->getUid()) {
                      case '8757ba2d-8b76-488d-b767-a15fec0f53af':
                        if(!is_null($valuedataAddDop->getValue()))
                        {
                          $valMetkDoppp=$valuedataAddDop->getValue()->getValue();
                        }
                        break;
                    }
                  }
                  $operArr[$addSettMetk[$nameP]['metka']]=$valMetkDoppp;
                  $operArr['Operations.CheckArray'][]='Check.'.$addSettMetk[$nameP]['metka'];
                  $OperationTable['operTable'][$addSettMetk[$nameP]['metka']][$numOperattt]=$valMetkDoppp;
                  $dataDopChildren=[];
                  if(isset($addSettMetk[$nameP]['value']['Block elements']['value']))
                  {
                    $dataDopChildren=$addSettMetk[$nameP]['value']['Block elements']['value'];
                  }
                  
                  foreach ($childrenData as $valuechildrenData)
                  {                 

                    if(isset($dataDopChildren[$valuechildrenData['name']]))
                    {

                      $valMetkDoppp='';
                      $oAddDop=new \Dzeta\Core\Instance($valuechildrenData['uid'], '');
                      $dataAddDop = $_obj->getAllValues([$oAddDop])[$valuechildrenData['uid']];
                      foreach ($dataAddDop as  $valuedataAddDop) {
                        switch ($valuedataAddDop->getUid()) {
                          case '77e083c5-0fb4-44af-979e-1bab31dcdab6':
                            if(!is_null($valuedataAddDop->getValue()))
                            {
                              $valMetkDoppp=$valuedataAddDop->getValue()->getValue();
                            }
                            break;
                        }
                      }
                      $exxp=explode('.',$dataDopChildren[$valuechildrenData['name']]['metka']);
                      if(isset($exxp[0])&&$exxp[0]=='CheckON')
                      {
                        if(in_array($dataDopChildren[$valuechildrenData['name']]['metka'],$checkOnArr))
                        {
                          $kSearchh=array_search($dataDopChildren[$valuechildrenData['name']]['metka'],$checkOnArr);
                          $operArr['Operations.CheckArray'][]=$dataDopChildren[$valuechildrenData['name']]['metka'].'.OUI';
                          unset($checkOnArr[$kSearchh]);
                        }
                      }else
                      {
                              $kSearchh=array_search('CheckON.'.$dataDopChildren[$valuechildrenData['name']]['metka'],$checkOnArr);
                            $operArr['Operations.CheckArray'][]='CheckON.'.$dataDopChildren[$valuechildrenData['name']]['metka'].'.OUI';
                            unset($checkOnArr[$kSearchh]);
                            $operArr['Operations.CheckArray'][]='Check.'.$dataDopChildren[$valuechildrenData['name']]['metka'];
                      }
                      $operArr[$dataDopChildren[$valuechildrenData['name']]['metka']]=$valMetkDoppp;               
                            $OperationTable['operTable'][$dataDopChildren[$valuechildrenData['name']]['metka']][$numOperattt]=$valMetkDoppp;
                    }
                    if(isset($valuechildrenData['userData']))
                    {
                      $valMetkDoppp='';
                      $oAddDop=new \Dzeta\Core\Instance($valuechildrenData['uid'], '');
                      $dataAddDop = $_obj->getAllValues([$oAddDop])[$valuechildrenData['uid']];
                      $nameAttrr='';
                      foreach ($dataAddDop as  $valuedataAddDop) 
                      {
                        switch ($valuedataAddDop->getUid()) {
                          case '104995b9-b651-447f-b483-682a914ec435':
                            if(!is_null($valuedataAddDop->getValue()))
                            {
                               $nameAttrr=$valuedataAddDop->getValue()->getName();

                            }                           
                            break;
                        }
                      }
                      if(isset($dataDopChildren[$valuechildrenData['name'].'.'.$nameAttrr]))
                      {
                        $exxp=explode('.',$dataDopChildren[$valuechildrenData['name'].'.'.$nameAttrr]['metka']);
                        if(isset($exxp[0])&&$exxp[0]=='CheckON')
                        {
                          if(in_array($dataDopChildren[$valuechildrenData['name'].'.'.$nameAttrr]['metka'],$checkOnArr))
                          {
                            $kSearchh=array_search($dataDopChildren[$valuechildrenData['name'].'.'.$nameAttrr]['metka'],$checkOnArr);
                            $operArr['Operations.CheckArray'][]=$dataDopChildren[$valuechildrenData['name'].'.'.$nameAttrr]['metka'].'.OUI';
                            unset($checkOnArr[$kSearchh]);
                          }
                        }
                        $operArr[$dataDopChildren[$valuechildrenData['name'].'.'.$nameAttrr]['metka']]=$valuechildrenData['userData'];
                        $OperationTable['operTable'][$dataDopChildren[$valuechildrenData['name'].'.'.$nameAttrr]['metka']][$numOperattt]=$valuechildrenData['userData'];
                      }
                     
                    }
                  }
              }
            }
            foreach ($checkOnArr as  $valuecheckOnArr) 
            {
              $operArr['Operations.CheckArray'][]=$valuecheckOnArr.'.NON';
            }
            
            if(empty($curGlobalData))
            {
              $curGlobalData[]=[
                  'operation-Operation'=>$voper['operation']['name'],
                  'operation-PrimeClient'=>$voper['cost_client'],
                  'operation-Prime'=>$voper['cost'],
                   'buro-RS'=>$namePD,
                    'buro-siren'=>substr(str_replace(' ','',$rSIREN),0,9),
                    'specOperIsolant'=> $specOperIsolant,
                    'type_de_pose'=>$Type_de_pose,
                    'Operation.DeActivitii'=>$sectDeActivity
              ];
            }else{
              foreach ($curGlobalData as $keycurGlobalData => $valuecurGlobalData) {
                $curGlobalData[$keycurGlobalData]['operation-Operation']=$voper['operation']['name'];
                $curGlobalData[$keycurGlobalData]['operation-PrimeClient']=$voper['cost_client'];
                $curGlobalData[$keycurGlobalData]['operation-Prime']=$voper['cost'];
                $curGlobalData[$keycurGlobalData]['buro-RS']=$namePD;
                $curGlobalData[$keycurGlobalData]['buro-siren']=substr(str_replace(' ','',$rSIREN),0,9);
                $curGlobalData[$keycurGlobalData]['specOperIsolant']=$specOperIsolant;
              }
                $tmpV=[];
               $tmpV[]=[
                  'operation-Operation'=>$voper['operation']['name'],
                  'operation-PrimeClient'=>$voper['cost_client'],
                  'operation-Prime'=>$voper['cost'],
                  'buro-RS'=>$namePD,
                  'buro-siren'=>substr(str_replace(' ','',$rSIREN),0,9),
                  'specOperIsolant'=> $specOperIsolant,
                  'Isolant-Surface (m2)'=>array_sum(array_column($curGlobalData, 'Isolant-Surface (m2)')),
                  'Isolant-Résistance Thermique'=>implode('/',array_column($curGlobalData, 'Isolant-Résistance Thermique')),
                  'Isolant-Energie de chauffage'=>implode('/',array_column($curGlobalData, 'Isolant-Energie de chauffage')),
                  'Isolant-Marque'=>implode('/',array_column($curGlobalData, 'Isolant-Marque')),
                  'Isolant-Reference'=>implode('/',array_column($curGlobalData, 'Isolant-Reference')),
                   'type_de_pose'=>$Type_de_pose,
                   'Operation.DeActivitii'=>$sectDeActivity
              ];
              $curGlobalData=$tmpV;
            }
            foreach ($curGlobalData as $keycurGlobalData => $valuecurGlobalData){
                $attrDataFinalData[]=$valuecurGlobalData;
            }

            if(empty($resThermiqArr))
            {
              $resThermiqArr[]='';
                $EpaisseurArr[]='';
              $resMarqueArr[]='';
              $ReferenceArr[]='';
               $SurfaceArr[]='';
            }

           
            if(!empty($dtOperChildRange))
            {
              $dtOperChildRangeV = $_obj->getAllValues($dtOperChildRange);

              foreach ($dtOperChildRangeV as  $attrsAll) {
                  $dateStartDateRange=null;
                  $dateFinishDateRange=null;
                  $Page1DateRange=null;
                  $Page2DateRange=null;
                  $priceOper=null;

                  foreach ($attrsAll as  $valAtttr)
                  {
                      $attrUid=$valAtttr->getUid();
                      if($attrUid=='06e55dbd-1574-4aa5-9b8e-f062b73aad62')
                      {
                        $dateStartDateRange=$valAtttr->getValue();
                      }elseif($attrUid=='23361d2b-5c69-432e-a7f5-34efa1168d4b')
                      {
                        $dateFinishDateRange=$valAtttr->getValue();
                      }elseif($attrUid=='065b778e-3b21-4424-a3fd-ad40eb25c368')
                      {
                        $Page1DateRange=$valAtttr->getValue();
                      }elseif($attrUid=='490b7211-75d5-404a-896b-de527ed70070')
                      {
                        $Page2DateRange=$valAtttr->getValue();
                      }else if($attrUid==$colorOper)
                      {
                        $priceOper=$valAtttr->getValue();
                      }
                  }

                  if(!is_null($dateStartDateRange)&&!is_null($dateFinishDateRange))
                  {
                    if($dateStartDateRange->getValueN()<$dateDeviss&&
                    $dateFinishDateRange->getValueN()>$dateDeviss)
                    {
                      //вот оно счастье и смотрим P1 и P2
                      if(!is_null($Page1DateRange))
                      {                       
                       // $urlPage1[]=['url'=>$Page1DateRange->getValue(),'pages'=>1,'dopData'=>$operArr];
                      }else
                      {
                        if(!is_null($dataPage1Oper))
                        {                       
                           // $urlPage1[]=['url'=>$dataPage1Oper->getValue(),'pages'=>1,'dopData'=>$operArr];
                        }

                      }
                      if(!is_null($Page2DateRange))
                      {
                            foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                            {
                      
                                  $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                                  
                                  $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                                  $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                                        $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
                                  $operArr['Operations.Isolant.Surface']=$this->conevrtSpecFormat($SurfaceArr[$keyResThermiq]);
                                  // var_dump($EpasseurBlockSpecArr);
                                  $metkaEpppasss=$dataSett[0]['value'][0]['value'][8]['value'][0]['metka'];
                                  if(isset($EpasseurBlockSpecArr[$keyResThermiq]))
                                  {
                                    $operArr[$metkaEpppasss]=$EpasseurBlockSpecArr[$keyResThermiq];
                                  }
                                  $metkaAttribute3=$dataSett[0]['value'][0]['value'][2]['value'][23]['metka'];
                                  $operArr[$metkaAttribute3]=$Attribute3Arr[$keyResThermiq];
                                  if(isset($countDiametre[$keyResThermiq]))
                                  {

                                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][1]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['20_65'];
                                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][2]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['65_100'];
                                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][3]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['100'];

                                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][4]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['20_65'];
                                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][5]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['65_100'];
                                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][6]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['100'];

                                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][7]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['countD'];
                                     $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][8]['metka'];
                                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['countD'];
                                  }

                                $urlPage2[]=['url'=>$Page2DateRange->getValue(),'pages'=>1,'aCode'=>$acode,'sPage'=>1,'dopData'=>$operArr];
                            }
                      }else
                      {
                            if(!is_null($dataPage2Oper))
                            {
                                foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                                {
                                      $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                                      $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                                      $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                                      $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
                                      $operArr['Operations.Isolant.Surface']=$this->conevrtSpecFormat($SurfaceArr[$keyResThermiq]);
                                      $metkaEpppasss=$dataSett[0]['value'][0]['value'][8]['value'][0]['metka'];
                                      if(isset($EpasseurBlockSpecArr[$keyResThermiq]))
                                      {
                                        $operArr[$metkaEpppasss]=$EpasseurBlockSpecArr[$keyResThermiq];
                                      }
                                      $metkaAttribute3=$dataSett[0]['value'][0]['value'][2]['value'][23]['metka'];
                                      $operArr[$metkaAttribute3]=$Attribute3Arr[$keyResThermiq];
                                      if(isset($countDiametre[$keyResThermiq]))
                                      {
                                        
                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][1]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['20_65'];
                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][2]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['65_100'];
                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][3]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['100'];

                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][4]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['20_65'];
                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][5]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['65_100'];
                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][6]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['100'];

                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][7]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['countD'];
                                        $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][8]['metka'];
                                        $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['countD'];
                                      }
                                      $urlPage2[]=['url'=>$dataPage2Oper->getValue(),'pages'=>1,'aCode'=>$acode,'sPage'=>1,'dopData'=>$operArr];
                                }
                            }
                      }
                      if(!is_null($priceOper)&&!is_null($colorOperCoef))
                      {
                        //тут надо аккуратно считать
                        $price1=$colorOperCoef*$voper['reste_a_charge'];
                        $price2=$priceOper->getValue()*$voper['surface'];                        
                        $colorOperArr[]=$price1>$price2? $price2:$price1;
                      }

                      $flP1=false;$flP2=false;
                      break;
                    }
                  }

              }

            }
            if(!empty($urllPrevisitte))
            {
                $urlPrevisite[]=['url'=>$urllPrevisitte,'dopData'=>$operArr,'t'=>'previsite'];
            }
            if(!empty($urllGenCond))
            {
                $urlGeneralConditions[]=['url'=>$urllGenCond,'dopData'=>$operArr,'t'=>'genCond'];
            }
            if(!empty($urllGeneralOperationATTESTATIOSIMPLIF))
            {
                $urlGeneralOperationATTESTATIOSIMPLIFIEE[]=['url'=>$urllGeneralOperationATTESTATIOSIMPLIF,'dopData'=>$operArr,'t'=>'ATTESTATION SIMPLIFIEE'];
            }
            if($flP1)
            {
              if(!is_null($dataPage1Oper))
              {               
                // $urlPage1[]=['url'=>$dataPage1Oper->getValue(),'pages'=>1,'dopData'=>$operArr];
              }
            }
            if($flP2)
            {
              if(!is_null($dataPage2Oper))
              {
                foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
                {
                  $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
                  $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
                  $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
                  $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
                  $operArr['Operations.Isolant.Surface']=$this->conevrtSpecFormat($SurfaceArr[$keyResThermiq]);
                  $metkaEpppasss=$dataSett[0]['value'][0]['value'][8]['value'][0]['metka'];
                  if(isset($EpasseurBlockSpecArr[$keyResThermiq]))
                  {
                    $operArr[$metkaEpppasss]=$EpasseurBlockSpecArr[$keyResThermiq];
                  }
                  $metkaAttribute3=$dataSett[0]['value'][0]['value'][2]['value'][23]['metka'];
                  $operArr[$metkaAttribute3]=$Attribute3Arr[$keyResThermiq];
                  if(isset($countDiametre[$keyResThermiq]))
                  {
                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][1]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['20_65'];
                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][2]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['65_100'];
                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][3]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['100'];

                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][4]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['20_65'];
                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][5]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['65_100'];
                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][6]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['100'];

                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][7]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['countD'];
                    $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][8]['metka'];
                    $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['countD'];
                  }

                  $urlPage2[]=['url'=>$dataPage2Oper->getValue(),'pages'=>1,'aCode'=>$acode,'sPage'=>1,'dopData'=>$operArr];
                }

              }
            }
            $urlPage1[]=['url'=>'','pages'=>1,'dopData'=>$operArr];
            //теперь надо данные по каждому изолянту для excel-ек
            $designation='';
            if(!empty($voper['additional_blocks']))
            {
              if(!empty($voper['additional_blocks'][0]['data']))
              {
                if(isset($voper['additional_blocks'][0]['data'][0]['name']))
                {
                  $designation=$voper['additional_blocks'][0]['data'][0]['name'];
                }
              }
            }
            foreach ($resThermiqArr as $keyResThermiq => $valueResThermiq)
            {
              $operArr['Operations.Isolant.Résistance thermique']=$valueResThermiq;
              $operArr['Operations.Isolant.Epaisseur']=$EpaisseurArr[$keyResThermiq];
              $operArr['Operations.Isolant.Marque']=$resMarqueArr[$keyResThermiq];
              $operArr['Operations.Isolant.Reference']=$ReferenceArr[$keyResThermiq];
              $operArr['Operations.Isolant.Surface']=$this->conevrtSpecFormat($SurfaceArr[$keyResThermiq]);
              $metkaEpppasss=$dataSett[0]['value'][0]['value'][8]['value'][0]['metka'];
              if(isset($EpasseurBlockSpecArr[$keyResThermiq]))
              {
                $operArr[$metkaEpppasss]=$EpasseurBlockSpecArr[$keyResThermiq];
              }
              $metkaAttribute3=$dataSett[0]['value'][0]['value'][2]['value'][23]['metka'];
              $operArr[$metkaAttribute3]=$Attribute3Arr[$keyResThermiq];
              if(isset($countDiametre[$keyResThermiq]))
              {                
                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][1]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['20_65'];
                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][2]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['65_100'];
                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][3]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['100'];

                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][4]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['20_65'];
                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][5]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['65_100'];
                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][6]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['100'];

                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][7]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['100']['countD'];
                $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][8]['metka'];
                $operArr[$metkCD]=$countDiametre[$keyResThermiq]['200']['countD'];
              }
              if(($type=='facture'||$type=='factureah')&&($voper['operation']['name']=='BAR-TH-161'||$voper['operation']['name']=='BAT-TH-155'))
              {
                $nameFileExcel=$information_lieu_travaux['nom_travaux'].' '.$voper['operation']['name'].' '.$resMarqueArr[$keyResThermiq].' '.$ReferenceArr[$keyResThermiq].'_'.($keyResThermiq+1);
                //тут таблица есть еще и спец приколы
                if(empty($countDiametre[$keyResThermiq]['100']['20_65']))
                {
                  $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][1]['metka'];
                  $operArr[$metkCD]='';
                }else
                {
                  $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][4]['metka'];
                  $operArr[$metkCD]='';
                }

                if(empty($countDiametre[$keyResThermiq]['100']['65_100']))
                {
                  $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][2]['metka'];
                  $operArr[$metkCD]='';
                }else
                {
                  $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][5]['metka'];
                  $operArr[$metkCD]='';
                }
                if(empty($countDiametre[$keyResThermiq]['100']['100']))
                {
                  $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][3]['metka'];
                  $operArr[$metkCD]='';
                }else
                {
                  $metkCD=$dataSett[0]['value'][0]['value'][8]['value'][6]['metka'];
                  $operArr[$metkCD]='';
                }
                //пока так так как Excel-ка одна
                //у нас еще есть спец таблица для Excel которая собирается для некоторых операций
                $tableForExcel=[];
                if($voper['operation']['name']=='BAR-TH-161'||$voper['operation']['name']=='BAT-TH-155')
                {
                  //собираем таблицу на основании всяких записей
                  if(isset($voper['isolant'][$keyResThermiq]['adv_size']))
                  {
                    foreach ($voper['isolant'][$keyResThermiq]['adv_size'] as $valueAdvSize)
                    {
                      for($i=0;$i<$valueAdvSize['l'];$i++)
                      {
                        $tableForExcel[]=[
                          'marque'=>$voper['isolant'][$keyResThermiq]['advParams']['Marque']['value'],
                          'designation'=>$designation,
                          'diametre'=>$valueAdvSize['d']
                        ];
                      }
                    }
                  }
                }

                $urlEXCEL[]=['dopData'=>$operArr,'nameFile'=>$nameFileExcel,'tableExcel'=>$tableForExcel];
              }
              
            }
          }



         
          $opCoopAve='';
          $imgSELogo='';
          $SE_ORGANIZATION='';
          $SE_ORGANIZATION_EMMY='';
          $SE_SIREN='';
          $SE_MENTION='';
          $SE_Addresse='';
          $orguid=$organisation['uid'];
          $orguid=empty($orguid)?'b63dd855-2b40-4cef-ba4a-201e76966136':$orguid;
          if(!empty($orguid))
          {
              $objS = new \Dzeta\Core\Instance($orguid, '');
              $dataZ = $_obj->getAllValues([$objS])[$orguid];
              foreach ($dataZ as  $valueORG)
              {
                  if($valueORG->getUid()=='896fe297-be94-4056-8ee8-07ae2635eada')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $urll=BASE_PATH.'/ufiles/'.$valueORG->getValue()->getValue().'.file';
                      $imgSELogo=['image'=>$urll];
                    }
                  }
                  if($valueORG->getUid()=='e5b7abf3-7c4d-4a82-bc06-939f6fba616b')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_ORGANIZATION=$valueORG->getValue()->getValue();
                    }
                  }
                  if($valueORG->getUid()=='85805457-9377-45c9-81fa-ddf27c9b6e35')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_ORGANIZATION_EMMY=$valueORG->getValue()->getValue();
                    }
                  }
                  if($valueORG->getUid()=='b264a8e8-2b7e-48ac-adc8-bd7a9646ee85')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_SIREN=substr(str_replace(' ','',$valueORG->getValue()->getValue()),0,9);
                    }
                  }
                  if($valueORG->getUid()=='1f225555-f8e9-4d39-98be-b0828a6d2f29')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_MENTION=$valueORG->getValue()->getValue();
                    }
                  }
                  if($valueORG->getUid()=='457ac944-e70e-4270-8a7f-94902aa1c2aa')
                  {
                    if(!is_null($valueORG->getValue()))
                    {
                      $SE_Addresse=$valueORG->getValue()->getValue();
                    }
                  }
              }
          }

          if(!empty($information_lieu_travaux["operationavec"]))
          {
            $opCoopAvec=$information_lieu_travaux["operationavec"];
            switch($opCoopAvec)
            {
              case 'Operation avec precarite':
                $opCoopAve='OPERATION AVEC PRECARITE';
              break;
              case 'QPV':
                $opCoopAve='QPV';
              break;
             case 'Bailleur Social':
              $opCoopAve='Bailleur Social';
              break;
            }
          }
          $barOArr=[];
          $sumMapprenovvv=0;
          if(!empty($colorOperArr))
          {
            $sumMapprenovvv=array_sum($colorOperArr);
          }
        //тут наш тип или девис или не девис
          if($type=='devis')
          {
            if( $operFirst=='BAR')
            {
              if($_REQUEST['persone']=='Personne physique'&&$sumMapprenovvv>0)
              {
                $barOArr[]=new \Dzeta\Core\Instance('04de02bb-9430-41fb-92e8-63ac6285f577', '');
              }elseif($_REQUEST['persone']=='Personne physique')
              {
                $barOArr[]=new \Dzeta\Core\Instance('b98b70f7-2b6a-4b71-8128-24c7c6761371', '');
              }else
              {
                $barOArr[]=new \Dzeta\Core\Instance('02a50c8f-42bc-4343-8295-502d83251988', '');
              }               
            }else
            {
              $barOArr[]=new \Dzeta\Core\Instance('7b2d3548-f09e-4e5e-b5d4-b17e949a1551', '');
            }
           
          }elseif($type=='facture')
          {
            if($_REQUEST['persone']=='Personne physique'&&$sumMapprenovvv>0)
            {
              $barOArr[]=new \Dzeta\Core\Instance('41973d41-42eb-483a-b25a-08c158f5c4f5', '');
            }elseif($_REQUEST['persone']=='Personne physique')
            {
              $barOArr[]=new \Dzeta\Core\Instance('d159a247-3628-4b48-83df-dff45cf006d0', '');
            }else
            {
              $barOArr[]=new \Dzeta\Core\Instance('b5aaeedb-922c-4755-869d-5d5e4a86040f', '');
            } 
            
          }elseif($type=='ah'||$type=='devisah')
          {
            if( $operFirst=='BAR')
            {                  
              if($_REQUEST['persone']=='Personne physique')
              {
                $barOArr[]=new \Dzeta\Core\Instance('631525b2-e240-4817-b9af-6f7798f04163', '');
              }else
              {
                $barOArr[]=new \Dzeta\Core\Instance('7315d6ed-6a0f-4715-b19f-91bb9dd15618', '');
              }             
            }else if($operFirst=='BAT')
            {
              $barOArr[]=new \Dzeta\Core\Instance('ef7a546d-94b0-4d91-b964-e2b6f6572dd9', '');
            }
          }else if($type=='factureah')
          {
            if($_REQUEST['persone']=='Personne physique'&&$sumMapprenovvv>0)
            {
              $barOArr[]=new \Dzeta\Core\Instance('41973d41-42eb-483a-b25a-08c158f5c4f5', '');
            }elseif($_REQUEST['persone']=='Personne physique')
            {
              $barOArr[]=new \Dzeta\Core\Instance('d159a247-3628-4b48-83df-dff45cf006d0', '');
            }else
            {
              $barOArr[]=new \Dzeta\Core\Instance('b5aaeedb-922c-4755-869d-5d5e4a86040f', '');
            }

            if( $operFirst=='BAR')
            {                  
              if($_REQUEST['persone']=='Personne physique')
              {
                $barOArr[]=new \Dzeta\Core\Instance('631525b2-e240-4817-b9af-6f7798f04163', '');
              }else
              {
                $barOArr[]=new \Dzeta\Core\Instance('7315d6ed-6a0f-4715-b19f-91bb9dd15618', '');
              }             
            }else if($operFirst=='BAT')
            {
              $barOArr[]=new \Dzeta\Core\Instance('ef7a546d-94b0-4d91-b964-e2b6f6572dd9', '');
            }
          }
          $uidsFile=[];
          foreach ($barOArr as $keyBarO => $barO) 
          {
            $dtChildrenPS=$_obj->getChildrenByType($barO, new Core\Type\Type('a657749a-7362-40ec-a1fd-4b678fc27f87', ''));
         
            $dtOperChildRangeV = $_obj->getAllValues($dtChildrenPS);
            $urllGlob=[];
            //возможно Катя пришлет детей не по имени
            foreach($dtChildrenPS as $keyO=> $attrsAll1)
            {
              $attrsAll=$attrsAll1->getAttributes();
              $pageTypee=null;
              $pageDescription=null;
              foreach ($attrsAll as  $vaPT) {
                switch ($vaPT->getUid()) {
                  case 'c8d0397d-281a-4ce6-ac74-aa1e94dc76d2':
                    $pageTypee=$vaPT;
                    break;
                  case '5dd3bc41-5d94-4d68-8bbb-ea28e06be458':
                    if(!is_null($vaPT->getValue()))
                    {
                      $pageDescription=$vaPT->getValue()->getValue();
                    }                    
                    break;
                }
              }
             
              if(!is_null($pageTypee->getValue()))
              {
                $valO=$pageTypee->getValue()->getUid();
                
                if($valO=='beefdb51-0ac8-48ae-90d7-e3d23d89e451')
                {
                  //
                  //тут печаль, и надо искать Version
                  $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                  new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                  $attrVPS=$_obj->getAllValues($dtVersPS);
                  foreach($attrVPS as $opVSAttr)
                  {
                    $urlVersion=null;
                    $date1Version=null;
                    $date2Version=null;
                    $sPageVersion=null;
                    foreach ($opVSAttr as $key => $valAttrrr) {
                      $attrrUi=$valAttrrr->getUid();
                      switch ($attrrUi) {
                        case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                          $date1Version=$valAttrrr;
                          break;
                        case '38707166-2c4e-4b73-838f-a5f3fec05351':
                          $date2Version=$valAttrrr;
                          break;
                        case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                        $urlVersion  =$valAttrrr;
                          break;
                        case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                          $sPageVersion=$valAttrrr;
                          break;
                      }
                    }
                    if(!is_null($date1Version->getValue())&&!is_null($date2Version->getValue()))
                    {
                      if($date1Version->getValue()->getValueN()<$dateDeviss&&
                      $date2Version->getValue()->getValueN()>$dateDeviss)
                      {
                        if(!is_null($urlVersion->getValue()))
                        {
                          $arrP=['url'=>$urlVersion->getValue()->getValue(),'type'=>'fixed'];

                          if(!is_null($sPageVersion->getValue()))
                          {
                            $arrP['sPage']=$sPageVersion->getValue()->getValue();
                          }
                          if($pageDescription=='Devis')
                          {
                            $arrP['retDoc']='devis';
                          }
                          $urllGlob[]=$arrP;
                        }
                      }
                    }
                  }
                }elseif($valO=='c282258e-4288-47d9-a538-35eb6c15c567'&&$taxFirst<20)
                {
                  $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                  new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                  $attrVPS=$_obj->getAllValues($dtVersPS);
                  foreach($attrVPS as $opVSAttr)
                  {
                    $urlVersion=null;
                    $date1Version=null;
                    $date2Version=null;
                    $sPageVersion=null;
                    foreach ($opVSAttr as $key => $valAttrrr) {
                      $attrrUi=$valAttrrr->getUid();
                      switch ($attrrUi) {
                        case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                          $date1Version=$valAttrrr;
                          break;
                        case '38707166-2c4e-4b73-838f-a5f3fec05351':
                          $date2Version=$valAttrrr;
                          break;
                        case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                        $urlVersion  =$valAttrrr;
                          break;
                        case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                          $sPageVersion=$valAttrrr;
                          break;
                      }
                    }
                    if(!is_null($date1Version->getValue())&&!is_null($date2Version->getValue()))
                    {
                      if($date1Version->getValue()->getValueN()<$dateDeviss&&
                      $date2Version->getValue()->getValueN()>$dateDeviss)
                      {
                        if(!is_null($urlVersion->getValue()))
                        {
                          $arrP=['url'=>$urlVersion->getValue()->getValue(),'type'=>'t5.5'];

                          if(!is_null($sPageVersion->getValue()))
                          {
                            $arrP['sPage']=$sPageVersion->getValue()->getValue();
                          }
                          $urllGlob[]=$arrP;
                        }
                      }
                    }
                  }
                }elseif($valO=='ee11791e-2585-4a8e-b367-2edd5f13b9e3')
                {
                  //P1
                  /*foreach($urlPage1 as $p1)
                  {
                    $urllGlob[]=$p1;
                  }*/
                  $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                  new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                  $attrVPS=$_obj->getAllValues($dtVersPS);
                  foreach($attrVPS as $opVSAttr)
                  {
                    $urlVersion=null;
                    $date1Version=null;
                    $date2Version=null;
                    $sPageVersion=null;
                    foreach ($opVSAttr as $key => $valAttrrr) {
                      $attrrUi=$valAttrrr->getUid();
                      switch ($attrrUi) {
                        case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                          $date1Version=$valAttrrr;
                          break;
                        case '38707166-2c4e-4b73-838f-a5f3fec05351':
                          $date2Version=$valAttrrr;
                          break;
                        case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                        $urlVersion  =$valAttrrr;
                          break;
                        case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                          $sPageVersion=$valAttrrr;
                          break;
                      }
                    }
                    if(!is_null($date1Version->getValue())&&!is_null($date2Version->getValue()))
                    {
                      if($date1Version->getValue()->getValueN()<$dateDeviss&&
                      $date2Version->getValue()->getValueN()>$dateDeviss)
                      {
                        if(!is_null($urlVersion->getValue()))
                        {
                          foreach ($urlPage1 as $valuePage1) 
                          {
                            $valuePage1['url']=$urlVersion->getValue()->getValue();
                            $valuePage1['type']='p1';
                            if(!is_null($sPageVersion->getValue()))
                            {
                              $valuePage1['sPage']=$sPageVersion->getValue()->getValue();
                            }
                            $urllGlob[]=$valuePage1;
                          }
                        }
                      }
                    }
                  }

                }elseif($valO=='ed6b2955-ebfb-4927-a251-2caf9fa11f31')
                {
                  //P2
                  foreach($urlPage2 as $p2)
                  {
                    $urllGlob[]=$p2;
                  }
                }elseif(($opCoopAve=='QPV'&&$valO=='e782fe9e-881b-4aa4-b423-d6b7281e0fe0')||
                    ($opCoopAve=='OPERATION AVEC PRECARITE'&&$valO=='0518aa50-b40b-4168-8c6a-452b1e79af5b')||
                    ($opCoopAve=='Bailleur Social'&&$valO=='4b43d186-dcae-419d-b77d-51d478ccf7c8'))
                {                
                    $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                    new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                    $attrVPS=$_obj->getAllValues($dtVersPS);
                    foreach($attrVPS as $opVSAttr)
                    {
                      $dateFrom=null;
                      $dateTo=null;
                      $linkkk=null;
                      $numberdPages=null;
                      foreach ($opVSAttr as  $valTGYH) {
                        switch ($valTGYH->getUid()) {
                          case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                            $dateFrom=$valTGYH;
                            break;
                          case '38707166-2c4e-4b73-838f-a5f3fec05351':
                            $dateTo=$valTGYH;
                            break;
                          case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                            $linkkk=$valTGYH;
                            break;
                          case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                            $numberdPages=$valTGYH;
                            break;
                        }
                      }
                      if(!is_null($dateFrom->getValue())&&!is_null($dateTo->getValue()))
                      {
                        if($dateFrom->getValue()->getValueN()<$dateDeviss&&
                        $dateTo->getValue()->getValueN()>$dateDeviss)
                        {
                          if(!is_null($linkkk->getValue()))
                          {
                            $arrP=['url'=>$linkkk->getValue()->getValue()];

                            if(!is_null($numberdPages->getValue()))
                            {
                              $arrP['sPage']=$numberdPages->getValue()->getValue();
                            }
                            $urllGlob[]=$arrP;
                          }
                        }
                      }
                    }
                  
                }else if($valO=='5cc6a302-b026-4a74-878f-f1d36fadf232'&&(!empty($beneficiaire['TableAflag'])||!empty($beneficiaire['TableBflag']))&&isset($_REQUEST['persone'])&&$_REQUEST['persone']=='Personne physique')
                {
                      $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                    new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                    $attrVPS=$_obj->getAllValues($dtVersPS);
                    foreach($attrVPS as $opVSAttr)
                    {
                      $dateFrom=null;
                      $dateTo=null;
                      $linkkk=null;
                      $numberdPages=null;
                      foreach ($opVSAttr as  $valTGYH) {
                        switch ($valTGYH->getUid()) {
                          case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                            $dateFrom=$valTGYH;
                            break;
                          case '38707166-2c4e-4b73-838f-a5f3fec05351':
                            $dateTo=$valTGYH;
                            break;
                          case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                            $linkkk=$valTGYH;
                            break;
                          case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                            $numberdPages=$valTGYH;
                            break;
                        }
                      }
                      if(!is_null($dateFrom->getValue())&&!is_null($dateTo->getValue()))
                      {
                        if($dateFrom->getValue()->getValueN()<$dateDeviss&&
                        $dateTo->getValue()->getValueN()>$dateDeviss)
                        {
                          if(!is_null($linkkk->getValue()))
                          {
                            $arrP=['url'=>$linkkk->getValue()->getValue()];
                             $arrP['R1']='true';
                            if(!is_null($numberdPages->getValue()))
                            {
                              $arrP['sPage']=$numberdPages->getValue()->getValue();
                            }
                            $urllGlob[]=$arrP;
                          }
                        }
                      }
                    }
                }else if($valO=='0a68db4d-ca33-4226-b780-7858ead03245'&&isset($_REQUEST['persone'])&&$_REQUEST['persone']=='Personne physique')
                {
                  //это таблицы физиков
                   $dtVersPS=$_obj->getChildrenByType($attrsAll1,
                    new Core\Type\Type('cca36e1c-293d-4ca6-9c7d-593a511aed55', ''));
                    $attrVPS=$_obj->getAllValues($dtVersPS);
                    foreach($attrVPS as $opVSAttr)
                    {
                      $dateFrom=null;
                      $dateTo=null;
                      $linkkk=null;
                      $numberdPages=null;
                      foreach ($opVSAttr as  $valTGYH) {
                        switch ($valTGYH->getUid()) {
                          case '668b7570-baf6-4fca-af7c-87d09a452bb7':
                            $dateFrom=$valTGYH;
                            break;
                          case '38707166-2c4e-4b73-838f-a5f3fec05351':
                            $dateTo=$valTGYH;
                            break;
                          case 'a69c87ce-4e86-499e-ad9a-1493be715d5f':
                            $linkkk=$valTGYH;
                            break;
                          case '6394728f-32cb-489d-a522-d720f9c7fc9a':
                            $numberdPages=$valTGYH;
                            break;
                        }
                      }
                      if(!is_null($dateFrom->getValue())&&!is_null($dateTo->getValue()))
                      {
                        if($dateFrom->getValue()->getValueN()<$dateDeviss&&
                        $dateTo->getValue()->getValueN()>$dateDeviss)
                        {
                          if(!is_null($linkkk->getValue()))
                          {
                              if(!empty($beneficiaire['persone_phys_data']))
                              {
                                  foreach ($beneficiaire['persone_phys_data'] as $valueBenif) 
                                  {
                                      $dopDataPhys=[];
                                      $numeriFBenifs='';
                                      $refAvisFBenifs='';                                   
                                      if(isset($valueBenif['numero']))
                                      {
                                          $numeriFBenifs=$valueBenif['numero'];
                                      }
                                      if(isset($valueBenif['reference']))
                                      {
                                          $refAvisFBenifs=$valueBenif['reference'];
                                      }
                                      $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][15]['metka'];
                                      $dopDataPhys[$metkaDateDeDebut]=$numeriFBenifs;
                                      $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][16]['metka'];
                                      $dopDataPhys[$metkaDateDeDebut]=$refAvisFBenifs;

                                      $textTableConfig='';
                                      $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][10]['metka'];
                                      if(!empty($valueBenif['JSON']))
                                      {
                                          $textTableConfig=$this->getSpecTableBenif(json_decode($valueBenif['JSON'],true));
                                      }
                                      $dopDataPhys[$metkaDateDeDebut]=$textTableConfig;
                                      $arrP=['url'=>$linkkk->getValue()->getValue()];                                    
                                      $arrP['dopData']=$dopDataPhys;
                                      $urllGlob[]=$arrP;
                                  }
                              }                            
                          }
                        }
                      }
                    }
                }else if($valO=='e57fb4db-1ab5-447b-8b34-f53f002c550a')
                {
                  foreach ($urlPrevisite as $value) 
                  {
                     $urllGlob[]=$value;
                  }
                }else if($valO=='16558399-511f-4bfe-ab61-33f95bb068c6')
                {
                  foreach ($urlGeneralConditions as $value) 
                  {
                     $urllGlob[]=$value;
                  }
                }else if($valO=='71a1b860-096b-4a66-b8a2-dd68a010abb6')
                {
                  foreach ($urlGeneralOperationATTESTATIOSIMPLIFIEE as $value) 
                  {
                     $urllGlob[]=$value;
                  }
                }
              }
            }

            $img=null;
            if(!empty($dataUserr['Logo upload']->getValue()))
            {
              $urll=BASE_PATH.'/ufiles/'.$dataUserr['Logo upload']->getValue()->getValue().'.file';
              $img=['image'=>$urll];
            }elseif(!empty($dataUserr['LOGO']->getValue()))
            {
              $ur=$dataUserr['LOGO']->getValue()->getValue();
              preg_match('/\/d\/(.*?)\//', $ur, $output_array);
              if(empty($output_array))
              {
                     $img=null;
              }else
              {
                     $urll="https://drive.google.com/u/0/uc?id={$output_array[1]}&export=download";
                     $img= ['image'=>$urll];
              }
            }
            
            $globalCheck=[];
            $flagSous_traitance=false;
            $flagSous_traitance=empty($Sous_traitance['sous_traitance'])? false:true;           
           
            //sous_traitance
            if($flagSous_traitance)
            {
              $globalCheck[]='Check.Sous traitance.Oui';
            }else{
              $globalCheck[]='Check.Sous traitance.Non';
              $metkSpec_FoundationLenvalNice='';
            }
         
           if(isset($OperationTable['operTable']))
           {
            $arrKeys=[];
              foreach ($OperationTable['operTable'] as $key => $value)
               {
                $aK=array_keys($value);
                foreach ($aK as   $vale) {
                 if(!in_array($vale, $arrKeys))
                 {
                  $arrKeys[]=$vale;
                 }
                }
                
              }
              foreach ($OperationTable['operTable'] as $key => $value) 
              {
                 foreach ( $arrKeys as  $valueK)
                 {
                  if(!isset($value[$valueK]))  
                  {
                    $OperationTable['operTable'][$key][$valueK]='';
                  }
                 }
              }
           }
           //getSpecTableBenif($dataRequest)
          $arrGl=[
            'Devis.Sous-traite'=>strip_tags($metkSpec_FoundationLenvalNice),
            'Bénéficiaire.Num Devis'=>$numDevissssss,
            'Organization.Organization'=>$SE_ORGANIZATION,
            'Organization.OrganizationEmmy'=>$SE_ORGANIZATION_EMMY,
            'Organization.Logo'=>$imgSELogo,
            'Organization.Adresse'=>$SE_Addresse,
            'Organization.Mentions finales'=>$SE_MENTION,
            'Organization.SIREN'=>substr(str_replace(' ','',$SE_SIREN),0,9),

            'Bénéficiaire.Adresse'=>isset($beneficiaire['adresse'])? $beneficiaire['adresse']:'',
            'Bénéficiaire.Code postale'=>isset($beneficiaire['code_postale'])? $beneficiaire['code_postale']:'',
            'Bénéficiaire.Email'=>isset($beneficiaire['email'])? $beneficiaire['email']:'',
            'Bénéficiaire.Fonction'=>isset($beneficiaire['fonction'])? $beneficiaire['fonction']:'',
            'Bénéficiaire.Nom'=>isset($beneficiaire['nom'])? $beneficiaire['nom']:'',
            'Bénéficiaire.Phone'=>isset($beneficiaire['phone'])? $beneficiaire['phone']:'',
            'Bénéficiaire.Prénom'=>isset($beneficiaire['prenom'])? $beneficiaire['prenom']:'',
            'Bénéficiaire.Raison sociale'=>isset($beneficiaire['raison_sociale'])? $beneficiaire['raison_sociale']:'',
            'Bénéficiaire.SIREN'=>isset($beneficiaire['siern'])? substr(str_replace(' ','',$beneficiaire['siern']),0,9):'',
            'Bénéficiaire.Ville'=>isset($beneficiaire['ville'])? $beneficiaire['ville']:'',

            'Date.Date de la facture'=>isset($dateClint['date_facture'])? $dateClint['date_facture']:'',
            'Date.Date de signature du devis'=>isset($dateClint['date_de_signature'])? $dateClint['date_de_signature']:'',
            'Date.Numéro de facture'=>isset($dateClint['numero_facture'])? $dateClint['numero_facture']:'',

            'Information lieu travaux.Adresse'=>isset($information_lieu_travaux['adresse_travaux'])? $information_lieu_travaux['adresse_travaux']:'',
            'Information lieu travaux.Code postale'=>isset($information_lieu_travaux['code_postale_travaux'])? $information_lieu_travaux['code_postale_travaux']:'',
            'Information lieu travaux.Nom de la copropriété'=>isset($information_lieu_travaux['nom_travaux'])? $information_lieu_travaux['nom_travaux']:'',
            'Information lieu travaux.OPERATION AVEC PRECARITE ou QPV'=>$opCoopAve,
            'Information lieu travaux.Ville'=>isset($information_lieu_travaux['ville_travaux'])? $information_lieu_travaux['ville_travaux']:'',

         //   'Users.ADRESSE MO'=>is_null($dataUserr['ADRESSE MO']->getValue())? '':$dataUserr['ADRESSE MO']->getValue()->getValue(),
         //   'Users.Capital social MO'=>is_null($dataUserr['Capital social MO']->getValue())? '':$dataUserr['Capital social MO']->getValue()->getValue(),
        //    'Users.CP MO'=>is_null($dataUserr['CP MO']->getValue())? '':$dataUserr['CP MO']->getValue()->getValue(),
         //   'Users.EMAIL MO'=>is_null($dataUserr['EMAIL MO']->getValue())? '':$dataUserr['EMAIL MO']->getValue()->getValue(),
         //   'Users.FONCTION MO'=>is_null($dataUserr['FONCTION MO']->getValue())? '':$dataUserr['FONCTION MO']->getValue()->getValue(),
            'Users.LOGO'=>$img,
         //   'Users.MOBILE MO'=>is_null($dataUserr['MOBILE MO']->getValue())? '':$dataUserr['MOBILE MO']->getValue()->getValue(),
         //   'Users.NOM MO'=>is_null($dataUserr['NOM MO']->getValue())? '':$dataUserr['NOM MO']->getValue()->getValue(),
         //   'Users.PRENOM MO'=>is_null($dataUserr['PRENOM MO']->getValue())? '':$dataUserr['PRENOM MO']->getValue()->getValue(),
         //   'Users.RAISON SOCIALE MO'=>is_null($dataUserr['RAISON SOCIALE MO']->getValue())? '':$dataUserr['RAISON SOCIALE MO']->getValue()->getValue(),
        //    'Users.SIRET MO'=>is_null($dataUserr['SIRET MO']->getValue())? '':$dataUserr['SIRET MO']->getValue()->getValue(),
         //   'Users.Tel MO'=>is_null($dataUserr['Tel MO']->getValue())? '':$dataUserr['Tel MO']->getValue()->getValue(),
         //   'Users.VILLE MO'=>is_null($dataUserr['VILLE MO']->getValue())? '':$dataUserr['VILLE MO']->getValue()->getValue(),
         //   'Users.site internet'=>is_null($dataUserr['site internet']->getValue())? '':$dataUserr['site internet']->getValue()->getValue(),

            'Operations.Pre visite'=>$preVisitteGlobal,
            'Incitation_Global'=>$this->conevrtSpecFormat(array_sum($IncitationArr)),
            //'Cumac_Global'=>array_sum($CumacArr).' kWhC',
            'SYSTEME'=>['tble'=>$OperationTable],//$this->conevrtSpecFormat($value)
            'Operation.Tax'=>str_replace('.',',',$taxFirst.''),
           
           'Total_TTC'=>$_REQUEST['persone']=='Personne physique'?$this->conevrtSpecFormat(round((array_sum($IncitationArr)+ $resteAchargeGlobal),2)):$this->conevrtSpecFormat(round((array_sum($IncitationArr)+ $resteAchargeGlobal),2)),
            //totaltcc reste a charge sum           
            'TOTAL'=>$_REQUEST['persone']=='Personne physique'?$this->conevrtSpecFormat(round((array_sum($IncitationArr)+ $resteAchargeGlobal)/(1+$taxFirst/100),2)):$this->conevrtSpecFormat(round((array_sum($IncitationArr)+ $resteAchargeGlobal)/(1+$taxFirst/100),2)),
            'TVA'=>$_REQUEST['persone']=='Personne physique'? str_replace('.',',',round((array_sum($IncitationArr)+ $resteAchargeGlobal)/(1+$taxFirst/100)*($taxFirst/100),2).''):
            str_replace('.',',',round((array_sum($IncitationArr)+ $resteAchargeGlobal)/(1+$taxFirst/100)*($taxFirst/100),2).''),//</w:t></w:r></w:p><w:p w:rsidR="00D11242"><w:r><w:t>
            'TVA'=>$_REQUEST['persone']=='Personne physique'? $this->conevrtSpecFormat(round((array_sum($IncitationArr)+ $resteAchargeGlobal)/(1+$taxFirst/100)*($taxFirst/100),2)):$this->conevrtSpecFormat(round((array_sum($IncitationArr)+ $resteAchargeGlobal)/(1+$taxFirst/100)*($taxFirst/100),2)),
            'List.Operations.Title'=>implode('</w:t><w:br/><w:t xml:space="preserve">',$operTitle),
            'Operations.Secteur d’activité'=>$Secteurd,
            'Operations.Énergie de chauffage'=>$Nergie_de_chauffage,


            'Sous traitance.Nom'=>empty($flagSous_traitance)?'': $Sous_traitance['nom'],
            'Sous traitance.Prenom'=>empty($flagSous_traitance)?'':$Sous_traitance['prenom'],
            'Sous traitance.Raison sociale'=>empty($flagSous_traitance)?'':$Sous_traitance['raison_sociale'],
            'Sous traitance.SIRET'=>empty($flagSous_traitance)?'':$Sous_traitance['siret'],

            'Global.Check'=>$globalCheck,

            'Information lieu travaux-Bailleur Social.Numéro SIREN du bailleur social'=>
            isset($information_lieu_travaux['adv_field']['siern'])? substr(str_replace(' ','',$information_lieu_travaux['adv_field']['siern']),0,9):'',
            'Information lieu travaux-Bailleur Social.Raison sociale du bailleur social'=>
            isset($information_lieu_travaux['adv_field']['raison_sociale'])? $information_lieu_travaux['adv_field']['raison_sociale']:'',
            'Information lieu travaux-Bailleur Social.Nom du signataire'=>
            isset($information_lieu_travaux['adv_field']['nom'])? $information_lieu_travaux['adv_field']['nom']:'',
            'Information lieu travaux-Bailleur Social.Prénom du signataire'=>
            isset($information_lieu_travaux['adv_field']['prenom'])? $information_lieu_travaux['adv_field']['prenom']:'',
            'Information lieu travaux-Bailleur Social.Fonction du signataire'=>
            isset($information_lieu_travaux['adv_field']['fonction'])? $information_lieu_travaux['adv_field']['fonction']:'',
            'Information lieu travaux-Bailleur Social.Telephone'=>
            isset($information_lieu_travaux['adv_field']['phone'])? $information_lieu_travaux['adv_field']['phone']:'',
            'Information lieu travaux-Bailleur Social.Adress'=>
            isset($information_lieu_travaux['adv_field']['adresse'])? $information_lieu_travaux['adv_field']['adresse']:'',
            'Information lieu travaux-Bailleur Social.Code postal'=>
            isset($information_lieu_travaux['adv_field']['code_postale'])? $information_lieu_travaux['adv_field']['code_postale']:'',
            'Information lieu travaux-Bailleur Social.Ville'=>
            isset($information_lieu_travaux['adv_field']['ville'])? $information_lieu_travaux['adv_field']['ville']:'',
            "Information lieu travaux-Bailleur Social.le nombre total de ménages concernés par l'opération"=>
            isset($information_lieu_travaux['adv_field']['nombre_total'])? $information_lieu_travaux['adv_field']['nombre_total']:'',
             "Information lieu travaux-Bailleur Social.Nombre total de ménages"=>
            isset($information_lieu_travaux['adv_field']['nombre_total_menages'])? $information_lieu_travaux['adv_field']['nombre_total_menages']:'',
            "Information lieu travaux-QPV.Code quartier du quartier prioritaire de la politique de la ville"=>
            isset($information_lieu_travaux['adv_field']['code_quartier'])? $information_lieu_travaux['adv_field']['code_quartier']:'',

          ];

          $benKeysArr=['Bénéficiaire.Adresse','Bénéficiaire.Code postale','Bénéficiaire.Email','Bénéficiaire.Fonction','Bénéficiaire.Nom','Bénéficiaire.Phone','Bénéficiaire.Prénom','Bénéficiaire.Raison sociale','Bénéficiaire.SIREN','Bénéficiaire.Ville'];
          
          if(!empty($beneficiaire['TableAflag']))
          {
              $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][12]['metka'];
              $arrGl['Global.Check'][]=$metkaDateDeDebut;
          }
          if(!empty($beneficiaire['TableBflag']))
          {
              $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][13]['metka'];
              $arrGl['Global.Check'][]=$metkaDateDeDebut;
          }
          $metkaDateDeDebut1=$dataSett[0]['value'][0]['value'][7]['value'][8]['metka'];
          $arrGl[$metkaDateDeDebut1]='-';
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][7]['value'][9]['metka'];
          $arrGl[$metkaDateDeDebut]='';
          $priceMaprrenov=0;
          if(!empty($colorOperArr))
          {
              $summ=array_sum($colorOperArr);
              //var_dump($summ);
              //var_dump($colorOperArr);
             
              if($summ>$defaultMaxValue)
              {
                  $arrGl[$metkaDateDeDebut1]=$this->conevrtSpecFormat($defaultMaxValue);
                  $priceMaprrenov=$defaultMaxValue;
              }else
              {
                  $arrGl[$metkaDateDeDebut1]=$this->conevrtSpecFormat($summ); 
                  $priceMaprrenov=$summ;
              }
              if(!empty($summ))
              {
                $uidObj_settt=$this->CONFIG_SETT['SUBVENTION MAPRIMERENOV']['obj'];
                $obj_settt=new \Dzeta\Core\Instance($uidObj_settt, '');
                $attrSettings=$_obj->getAllValues([$obj_settt])[$uidObj_settt];
                $strTexttt='';
                foreach ($attrSettings as $value) 
                {
                  switch ( $value->getUid()) 
                  {
                    case $this->CONFIG_SETT['SUBVENTION MAPRIMERENOV']['attr']:
                      if(!is_null($value->getValue()))
                      {
                        $strTexttt=$value->getValue()->getValue();
                      }
                      break;              
                  }
                }
                $arrGl[$metkaDateDeDebut]=strip_tags($strTexttt);
              }
              
          }
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][14]['metka'];       
          $arrGl[$metkaDateDeDebut]=$_REQUEST['persone']=='Personne physique'? '':'Représenté par: ';
          $benKeysArr[]=$metkaDateDeDebut;
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][17]['metka'];  
          $benKeysArr[]=$metkaDateDeDebut;
          $arrGl[$metkaDateDeDebut]= '';
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][18]['metka'];  
          $benKeysArr[]=$metkaDateDeDebut;
          $arrGl[$metkaDateDeDebut]= '';
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][19]['metka'];  
          $benKeysArr[]=$metkaDateDeDebut;
          $arrGl[$metkaDateDeDebut]= '';     

          
          $metkSectDeActi=$dataSett[0]['value'][0]['value'][7]['value'][2]['metka'];
          $arrGl[$metkSectDeActi]=empty($cumacGlobal)? $cumacGlobal:$this->conevrtSpecFormat($cumacGlobal);

          $metkSectDeActi=$dataSett[0]['value'][0]['value'][7]['value'][3]['metka'];
          $arrGl[$metkSectDeActi]=empty($resteAchargeGlobal)? $resteAchargeGlobal:$this->conevrtSpecFormat($resteAchargeGlobal);
          
          $metkSectDeActi=$dataSett[0]['value'][0]['value'][7]['value'][10]['metka'];
          $delta=$resteAchargeGlobal-$priceMaprrenov;
          $arrGl[$metkSectDeActi]=$delta<1 ? 1:$this->conevrtSpecFormat($delta);
          
          if(!empty($beneficiaire['persone_phys_data']))
          {
              //тут у нас метки которые бенефициаре
              $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][17]['metka'];  
              $numberPers='';
              if(isset($beneficiaire['number_de_perosne']))
              {
                  $numberPers=$beneficiaire['number_de_perosne'];
              }
              $benKeysArr[]=$metkaDateDeDebut;
              $arrGl[$metkaDateDeDebut]= $numberPers;
              
              $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][18]['metka'];  
              $numberPers='';
              if(isset($beneficiaire['persone_phys_data']))
              {
                  $numberPers=count($beneficiaire['persone_phys_data']);
              }
              $benKeysArr[]=$metkaDateDeDebut;
              $arrGl[$metkaDateDeDebut]= $numberPers;

              $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][19]['metka'];
              $fammm='';
              $copyBeny=$beneficiaire['persone_phys_data'];
              array_shift($copyBeny);
              $arrFam=[];
              foreach ($copyBeny as $valuePPD) 
              {
                  if(isset($valuePPD['JSON']))
                  {
                      $jsonn=json_decode($valuePPD['JSON'],true);
                      $arrFam[]=$jsonn['declarant1']['Nom'].' '.$jsonn['declarant1']['Prenom(s)'];
                  }
              }
              if(!empty($arrFam))
              {
                  $arrGl[$metkaDateDeDebut]= implode(';',$arrFam);
              }
          }
          
          //||!empty($beneficiaire['TableBflag'])
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][1]['value'][6]['metka'];
          $arrGl[$metkaDateDeDebut]=isset($information_lieu_travaux['parcelle_cadastrale'])? $information_lieu_travaux['parcelle_cadastrale']:'';
          
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][0]['value'][3]['metka'];
          $arrGl[$metkaDateDeDebut]=isset($dateClint['date_de_debut'])? $dateClint['date_de_debut']:'';
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][0]['value'][4]['metka'];
          $arrGl[$metkaDateDeDebut]=$numDevissssss;



          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][5]['value'][11]['metka'];
          $benKeysArr[]=$metkaDateDeDebut;
          $arrGl[$metkaDateDeDebut]=empty($beneficiaire['adress_error'])? '':$beneficiaire['adress_error'];
          //это из девиса ерем
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][0]['value'][5]['metka'];
          $arrGl[$metkaDateDeDebut]=$dateArr['agr_gen'];
          //это из фактуры
          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][0]['value'][6]['metka'];
          $arrGl[$metkaDateDeDebut]=$dateArr['inv_gen'];

          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][0]['value'][7]['metka'];
          $arrGl[$metkaDateDeDebut]=$numDevOrigggg;

          $metkaDateDeDebut=$dataSett[0]['value'][0]['value'][0]['value'][8]['metka'];
          $arrGl[$metkaDateDeDebut]=isset($dateClint['finDeTravaux'])? $dateClint['finDeTravaux']:'';

          foreach ($benKeysArr as $key => $value) 
          {
              if(isset( $arrGl[$value]))
              {                
                  if(!empty($beneficiaire['TableAflag']))
                  {
                       $arrGl[$value.'.TA']=$arrGl[$value];
                  }else{
                      $arrGl[$value.'.TA']='';
                  }
                  if(!empty($beneficiaire['TableBflag']))
                  {
                       $arrGl[$value.'.TB']=$arrGl[$value];
                  }else{
                      $arrGl[$value.'.TB']='';
                  }
              }else
              {
                  $arrGl[$value]='';
                  $arrGl[$value.'.TA']='';
                  $arrGl[$value.'.TB']='';
              }
          }
          foreach ($operationFirstMetka as $key => $value) {
              $arrGl[$key]=$value;
          }
         
          
          foreach ($arrrrGlll1 as $key => $value) {
           $arrGl[$key]=$value;
          }
           $arrGl1=[];
          if($readSettFile)
          {

               $arrGl1=$arrGl;
               $arrGl=[];
               $pathFileSetting=BASE_PATH.'/config/dop/DataEnemat/settEnemat.json';
               if(file_exists($pathFileSetting))
               {
                  $fileSetting=file_get_contents($pathFileSetting);
                  $dataSett=json_decode($fileSetting,true);

                  $resDataSett=[];
                  $metkSettZam=[];
                    $usersBlock=[];
                    if(isset($dataSett[0]['value'][1]['value'][1]))
                    {
                      $usersBlock=$dataSett[0]['value'][1]['value'][1]['value'];
                      $uidUserBlock=array_column($usersBlock, 'uid');
                      foreach ($dataUserValue as $key => $value) {
                        // $dataUserr['Tel MO'] = $value; getValue()->getValue()
                        $uidP=$value->getUid();
                        if($uidP=='6e8c9fcd-1d93-4395-8b90-cf7f4cd27bb6'||$uidP=='609972d3-9d0a-4d7a-8380-e55617b49873')
                        {
                          //это лого и лого имг
                          continue;
                        }
                       
                        $kkk=array_search($uidP, $uidUserBlock);
                        if($kkk!==false)
                        {
                          if(!empty($usersBlock[$kkk]['metka']))
                          {
                            if(!is_null($value->getValue()))
                            {
                                  if($value->getDatatype()==\Dzeta\Core\Type\DataType::TYPE_FILE)
                                  {
                                  //это штамп $uidP=='4d34f137-8684-4a14-9f3d-cf4afeaf0e7a'
                                      $urll=BASE_PATH.'/ufiles/'.$value->getValue()->getValue().'.file';
                                      $arrGl1[$usersBlock[$kkk]['metka']]=['image'=>$urll];
                                    }else{

                                      $arrGl1[$usersBlock[$kkk]['metka']]=$value->getValue()->getValue();
                                      if($uidP=='5d0b982c-a8e6-4020-959a-0190fa60826e')
                                      {
                                        $arrGl1[$usersBlock[$kkk]['metka']]=$this->conevrtSpecFormat($arrGl1[$usersBlock[$kkk]['metka']]);
                                      }  
                                    }                           
                            }else{
                              $arrGl1[$usersBlock[$kkk]['metka']]='';
                            }
                          }
                        }
                      }
                    }
                  $this->testSettData($dataSett,$resDataSett,$metkSettZam);
               
                  if(isset($metkSettZam['sous traitance.sous-traite']))
                  {
                    $metkSettZam['devis.sous-traite']=$metkSettZam['sous traitance.sous-traite'];
                  }
                  foreach ($metkSettZam as $key => $value) 
                  {  
                        //тут теперь перебираем все
                     // echo $key;
                      foreach ($arrGl1 as $keyarrGl1 => $arrGl1Value)
                      {
                        if(mb_strtolower($keyarrGl1)==mb_strtolower($key))
                        {                       
                            foreach ($value as $val)
                            {
                                if(isset($arrGl1[$val]))
                                {                                 
                                  $arrGl1[$keyarrGl1]=str_replace('{{'.$val.'}}',$arrGl1[$val] , $arrGl1[$keyarrGl1]);
                                }
                            }
                        }
                      }
                     
                      //теперь перебираем табличные вкладки на значения
                      if(isset($arrGl1['SYSTEME']['tble']['operTable']))
                      {
                        foreach ($arrGl1['SYSTEME']['tble']['operTable'] as $keyTable => $valueTable) 
                        {
                          foreach ($arrGl1 as $keyTTTTTTT => $valueTTTTTTTTTTTT) 
                          {
                            if(!is_array($valueTTTTTTTTTTTT))
                            {
                              $arrGl1['SYSTEME']['tble']['operTable'][$keyTable]=str_replace('{{'.$keyTTTTTTT.'}}', $valueTTTTTTTTTTTT, $arrGl1['SYSTEME']['tble']['operTable'][$keyTable]);
                            }
                          }
                         
                        }
                       
                        foreach ($arrGl1['SYSTEME']['tble']['operTable'] as $keyTable => $valueTable) {
                          foreach ($valueTable as $keyNNN => $valueNNN) {
                            foreach ($arrGl1['SYSTEME']['tble']['operTable'] as $keyTTTT => $valueTTTTT) {
                              if(isset($arrGl1['SYSTEME']['tble']['operTable'][$keyTTTT][$keyNNN]))
                              {
                                 $arrGl1['SYSTEME']['tble']['operTable'][$keyTable][$keyNNN]=
                              str_replace('{{'.$keyTTTT.'}}',$arrGl1['SYSTEME']['tble']['operTable'][$keyTTTT][$keyNNN] , $arrGl1['SYSTEME']['tble']['operTable'][$keyTable][$keyNNN]);
                              }
                             
                            }
                          }
                        }                     
                        
                        foreach ($arrGl1['SYSTEME']['tble']['operTable'] as $keyTable => $valueTable) 
                        {                        
                         foreach ($urllGlob as $keyurllGlob => $valueurllGlob) 
                         {
                          if(!empty($valueurllGlob['dopData']))
                          {
                            foreach ($valueurllGlob['dopData'] as $keyDopData => $valueDopData) 
                            {
                              if(!is_array( $valueDopData))
                              {
                                foreach ($arrGl1['SYSTEME']['tble']['operTable'][$keyTable] as $keykeyTable => $valuekeyTable) 
                                {
                                  $arrGl1['SYSTEME']['tble']['operTable'][$keyTable][$keykeyTable]=str_replace('{{'.$keyDopData.'}}',$valueDopData, $valuekeyTable);
                                } 
                             }
                           }
                          }
                         } 
                        }
                      }
                      
                      foreach ($urllGlob as $keyurllGlob => $valueurllGlob) {
                        if(isset($valueurllGlob['dopData']))
                        {
                          foreach ($valueurllGlob['dopData'] as $keyDopData => $valueDopData) {
                            if(mb_strtolower($keyDopData)==mb_strtolower($key)){
                              foreach ($value as $val)
                              {
                                  if(isset($arrGl1[$val]))
                                  {                                 
                                    $urllGlob[$keyurllGlob]['dopData'][$keyurllGlob][$keyDopData]=str_replace('{{'.$val.'}}',$arrGl1[$val] , $urllGlob[$keyurllGlob]['dopData'][$keyurllGlob][$keyDopData]);
                                  }
                              }
                            }
                          }
                        }
                      }
                  }
                
                  foreach ($arrGl1 as $key=> $value)
                  {
                    if(isset($resDataSett[mb_strtolower($key)]))
                    {
                        $arrGl[$resDataSett[mb_strtolower($key)]]=$value;
                    }else{
                        if($key=='Global.Check')
                        {
                          foreach ($value as  $val) {
                            if(isset($arrGl[$val]))
                            {
                              $arrGl[$key][]=$arrGl[$val];
                            }else{
                              $arrGl[$key][]=$val;
                            }
                          }
                        }else{
                          $arrGl[$key]=$value;
                        }

                    }
                  }


                  foreach ($urllGlob as $key => $value) {
                      if(isset($value['dopData']['Operations.CheckArray']))
                      {
                        foreach ($value['dopData']['Operations.CheckArray'] as $keye => $valu) {
                          if(isset($resDataSett[$valu]))
                          {

                            unset($urllGlob[$key]['dopData']['Operations.CheckArray'][$keye]);
                            $urllGlob[$key]['dopData']['Operations.CheckArray'][]= $resDataSett[$valu];
                          }
                        }

                      }
                  }


               }


          }
          

            $thtg=new \FillFile($urllGlob,$arrGl,$urlEXCEL);
        //f5191bc5-fb49-4017-8750-e2c3d9963c26


          if(!empty($thtg->uid))
          {
              $result=1;
              $key=$thtg->uid;

            //Если все ок, то пишем файл в параметр еще
            $uuidd=\Dzeta\Core\Value\File::GenerateUIDFile();
            $fileOrigPath=BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.'fillFileFr/'.$key.'/result.pdf';
            $res=copy($fileOrigPath, BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuidd.\Dzeta\Core\Value\File::FILE_EXT);
            
            if($res)
            {
              
              //у каждого свой
              if($type=='devis')
              {
                 $attrFiles=  [
                  'attr'=>'9065f915-ea0a-47f1-aa41-dc3ddb13f493',
                  'name'=>'PDF generated',
                  'datatype'=>'file',
                  'key'=>false
                ];
                 $nameFile='Devis ';
              }elseif($type=='ah'||$type=='devisah'){
                 $attrFiles=  [
                  'attr'=>'be249592-c0d8-43ea-b9c6-312e7385983f',
                  'name'=>'PDF generated',
                  'datatype'=>'file',
                  'key'=>false
                ];
                $nameFile='AH ';
              }elseif($type=='facture'){
                 $attrFiles=  [
                  'attr'=>'ac0f9b55-f91d-45ba-bc16-fc3d0a0fa069',
                  'name'=>'PDF generated',
                  'datatype'=>'file',
                  'key'=>false
                ];
                 $nameFile='Facture ';
                 //тут еще excel-ки
                  $attrFiles1=  [
                    'attr'=>'9f764ae2-21ba-4120-8d03-37daed0e7d84',
                    'name'=>'Excel table',
                    'datatype'=>'file',
                    'key'=>false
                  ];
                  $fileOrigPathExcel=BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.'fillFileFr/'.$key.'/excel';
                  if(is_dir($fileOrigPathExcel))
                  {

                    $filelist = array();

                    if ($handle = opendir($fileOrigPathExcel)) 
                    {
                      while ($entry = readdir($handle)) 
                      {

                        if ($entry!='.'&&$entry!='..') 
                        {
                          $filelist[] = $fileOrigPathExcel.'/'.$entry;
                        }
                      }
                      closedir($handle);
                    }
                    
                    if(count($filelist)>1)
                    { //тут зип
                      $zip = new \ZipArchive();
                      $temp =@tempnam("/tmp", "zip");;
                      $zip->open($temp, \ZIPARCHIVE::CREATE);
                      foreach ($filelist as $valueFl) 
                      {
                        $zip->addFromString(basename($valueFl), file_get_contents($valueFl));
                      }
                      $zip->close();
                      $uuiddSpecF=\Dzeta\Core\Value\File::GenerateUIDFile();
                      $res=copy($temp, BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuiddSpecF.\Dzeta\Core\Value\File::FILE_EXT);
                      $filePDFF=new \Dzeta\Core\Value\File($uuiddSpecF, 'excelTable.zip', null, filesize($temp));
                      $attr = new \Dzeta\Core\Type\Attribute($attrFiles1['attr'], $attrFiles1['name'],
                        \Dzeta\Core\Type\DataType::getIndexFromType('file'), false, false);
                      $attr->setValueS($filePDFF);
                      $objOBJET->setAttributes([$attr]);
                      $_obj->AddFiles($objOBJET, $this->session->get('logged')['User']);
                      $uidsFile[]=$uuiddSpecF;
                      if(isset($objROOT))
                      {
                        $objROOT->setAttributes([$attr]);
                        $_obj->AddFiles($objROOT, $this->session->get('logged')['User']);                
                      }
                    }elseif(count($filelist)==1)
                    {//тут просто файл
                      $uuiddSpecF=\Dzeta\Core\Value\File::GenerateUIDFile();
                      $res=copy($filelist[0], BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuiddSpecF.\Dzeta\Core\Value\File::FILE_EXT);
                      $filePDFF=new \Dzeta\Core\Value\File($uuiddSpecF, basename($filelist[0]), null, filesize($filelist[0]));
                      $attr = new \Dzeta\Core\Type\Attribute($attrFiles1['attr'], $attrFiles1['name'],
                        \Dzeta\Core\Type\DataType::getIndexFromType('file'), false, false);
                      $attr->setValueS($filePDFF);
                      $objOBJET->setAttributes([$attr]);
                      
                      $_obj->AddFiles($objOBJET, $this->session->get('logged')['User']);
                      $uidsFile[]=$uuiddSpecF;
                      //var_dump($objOBJET->getUid());
                      if(isset($objROOT))
                      {
                        //var_dump($objROOT->getUid());
                        $objROOT->setAttributes([$attr]);
                        $_obj->AddFiles($objROOT, $this->session->get('logged')['User']);                
                      }
                    }
                  }
                  
              }else if($type=='factureah')
              {
                if($keyBarO==0)
                {
                  $attrFiles=  [
                    'attr'=>'ac0f9b55-f91d-45ba-bc16-fc3d0a0fa069',
                    'name'=>'PDF generated',
                    'datatype'=>'file',
                    'key'=>false
                  ];
                  $nameFile='Facture ';
                  $objO1=$objFacture;
                  //тут еще excel-ки
                  $attrFiles1=  [
                    'attr'=>'9f764ae2-21ba-4120-8d03-37daed0e7d84',
                    'name'=>'Excel table',
                    'datatype'=>'file',
                    'key'=>false
                  ];
                  $fileOrigPathExcel=BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.'fillFileFr/'.$key.'/excel';
                  if(is_dir($fileOrigPathExcel))
                  {
                    $filelist = array();

                    if ($handle = opendir($fileOrigPathExcel)) 
                    {
                      while ($entry = readdir($handle)) 
                      {
                         if ($entry!='.'&&$entry!='..') 
                        {
                          $filelist[] = $fileOrigPathExcel.'/'.$entry;
                        }
                      }
                      closedir($handle);
                    }
                    if(count($filelist)>1)
                    { //тут зип
                      $zip = new \ZipArchive();
                      $temp =@tempnam("/tmp", "zip");;
                      $zip->open($temp, \ZIPARCHIVE::CREATE);
                      foreach ($filelist as $valueFl) 
                      {
                        $zip->addFromString(basename($valueFl), file_get_contents($valueFl));
                      }
                      $zip->close();
                      $uuiddSpecF=\Dzeta\Core\Value\File::GenerateUIDFile();
                      $res=copy($temp, BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuiddSpecF.\Dzeta\Core\Value\File::FILE_EXT);
                      $filePDFF=new \Dzeta\Core\Value\File($uuiddSpecF, 'excelTable.zip', null, filesize($temp));
                      $attr = new \Dzeta\Core\Type\Attribute($attrFiles1['attr'], $attrFiles1['name'],
                        \Dzeta\Core\Type\DataType::getIndexFromType('file'), false, false);
                      $attr->setValueS($filePDFF);
                      $objO1->setAttributes([$attr]);
                      $_obj->AddFiles($objO1, $this->session->get('logged')['User']);
                      $uidsFile[]=$uuiddSpecF;
                    }elseif(count($filelist)==1)
                    {//тут просто файл
                      $uuiddSpecF=\Dzeta\Core\Value\File::GenerateUIDFile();
                      $res=copy($filelist[0], BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuiddSpecF.\Dzeta\Core\Value\File::FILE_EXT);
                      $filePDFF=new \Dzeta\Core\Value\File($uuiddSpecF, basename($filelist[0]), null, filesize($filelist[0]));
                      $attr = new \Dzeta\Core\Type\Attribute($attrFiles1['attr'], $attrFiles1['name'],
                        \Dzeta\Core\Type\DataType::getIndexFromType('file'), false, false);
                      $attr->setValueS($filePDFF);
                      $objO1->setAttributes([$attr]);
                      $_obj->AddFiles($objO1, $this->session->get('logged')['User']);
                      $uidsFile[]=$uuiddSpecF;
                    }
                  }
                  
                }else if($keyBarO==1)
                {
                  $attrFiles=  [
                    'attr'=>'be249592-c0d8-43ea-b9c6-312e7385983f',
                    'name'=>'PDF generated',
                    'datatype'=>'file',
                    'key'=>false
                  ];
                  $nameFile='AH ';
                  $objO1=$objAH;
                }
                if($_REQUEST['persone']=='Personne physique')
                {
                   $arrGl['Information lieu travaux.Nom de la copropriété']=$_REQUEST['beneficiaire']['nom'].' '.$_REQUEST['beneficiaire']['prenom'];; 
                   
                }
                $nameFile.=$arrGl['Information lieu travaux.Nom de la copropriété'].'.pdf';  
                $filePDF=new \Dzeta\Core\Value\File($uuidd, $nameFile, null, filesize($fileOrigPath));
                $attr = new \Dzeta\Core\Type\Attribute($attrFiles['attr'], '',
                 \Dzeta\Core\Type\DataType::getIndexFromType($attrFiles['datatype']), $attrFiles['key'], false);
                $attr->setValueS($filePDF);
                $objO1->setAttributes([$attr]);
                $results = $_obj->AddFiles($objO1, $this->session->get('logged')['User']);
                $uidFile=$uuidd;
                $uidsFile[]=$uuidd;
              }
              if($type!='factureah')
              {
                  if($_REQUEST['persone']=='Personne physique')
                {
                   $arrGl['Information lieu travaux.Nom de la copropriété']=$_REQUEST['beneficiaire']['nom'].' '.$_REQUEST['beneficiaire']['prenom'];; 
                   
                }
                $nameFile.=$arrGl['Information lieu travaux.Nom de la copropriété'].'.pdf';
               
              

                $filePDF=new \Dzeta\Core\Value\File($uuidd, $nameFile, null, filesize($fileOrigPath));
                $attr = new \Dzeta\Core\Type\Attribute($attrFiles['attr'], '',
                 \Dzeta\Core\Type\DataType::getIndexFromType($attrFiles['datatype']), $attrFiles['key'], false);
                $attr->setValueS($filePDF);
                $objOBJET->setAttributes([$attr]);
                $results = $_obj->AddFiles($objOBJET, $this->session->get('logged')['User']);
                $uidFile=$uuidd;
                $uidsFile[]=$uuidd;
                if(isset($objROOT))
                {
                  $objROOT->setAttributes([$attr]);
                  $results = $_obj->AddFiles($objROOT, $this->session->get('logged')['User']);                
                }
                if($type=='devis')
                {
                  if(isset($thtg->filesSpec['devis']))
                  {
                    $uuiddSpec=\Dzeta\Core\Value\File::GenerateUIDFile();
                    $res=copy($thtg->filesSpec['devis'], BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuiddSpec.\Dzeta\Core\Value\File::FILE_EXT);
                    $nameFile1=$nameFile;  
                    $filePDF=new \Dzeta\Core\Value\File($uuiddSpec, $nameFile1, null, filesize($thtg->filesSpec['devis']));
                    $attr = new \Dzeta\Core\Type\Attribute('04308da2-0ca4-4ba2-b515-e89de186a6e8', 'Devis for signature',
                      \Dzeta\Core\Type\DataType::getIndexFromType('file'), false, false);
                    $attr->setValueS($filePDF);
                    $objROOT->setAttributes([$attr]);
                    $_obj->AddFiles($objROOT, $this->session->get('logged')['User']);
                  }                  
                }
              }
              

            }

          }
        }
      }
      echo json_encode(['result'=>1,['URLg'=>$urllGlob,'ARRg'=>$arrGl],'uidFile'=>$uidFile,'uidsFile'=>$uidsFile]);
    }
  
  
  private function fillParamForFormData($dataParamOBJET,$configParamData,$requestData,$dopSpec=[])
    {
        foreach ($dataParamOBJET as $key => $value) {
            $uidParam=$value->getUid();
            if(isset($configParamData[$uidParam]))
            {
              //значит параметр есть
              switch($configParamData[$uidParam]['type'])
              {
                  case 'form-json':
                    if(isset($configParamData[$uidParam]['notwrite']))
                    {
                      unset($dataParamOBJET[$key]);
                    }else{
                      $vvvv=$this->getValueForDataFormKeys($requestData,$configParamData[$uidParam]['keys']);
                      $vvvv=is_null($vvvv)? null:json_encode($vvvv);
                      $value->setValue(['value'=>$vvvv]);
                    }                   
                    break;
                  case 'form':
                    if(isset($configParamData[$uidParam]['notwrite']))
                    {
                      unset($dataParamOBJET[$key]);
                    }else{
                        $v=$this->getValueForDataFormKeys($requestData,$configParamData[$uidParam]['keys']);
                        if(isset($configParamData[$uidParam]['action']))
                        {
                            switch ($configParamData[$uidParam]['action']) {
                                case 'empty':
                                    $v=empty($v)? '':$v;
                                    $v=$v=='false'? '':$v;
                                    break;
                                case 'emptyNum':
                                    $v=empty($v)? 0:$v;
                                    $v=$v=='false'? 0:$v;
                                    $v=empty($v)? 0:1;
                                    break;
                            }
                        }
                       $value->setValue(['value'=>$v]);
                    }                   
                    break;
                  case 'dateNow':
                    if(empty($dopSpec['dateNow']))
                    {
                      unset($dataParamOBJET[$key]);
                    }else{
                      $value->setValue(['value'=>$dopSpec['dateNow']]);
                    }                    
                    break;
                  case 'curUser':                    
                      $value->setValue(['value'=>$this->session->get('logged')['User']->getUid()]); 
                    break;
                  case 'complAH':  
                      if(!empty($dopSpec['complAH']))
                      {
                        $value->setValue(['value'=>1]); 
                      }else
                      {
                        $value->setValue(['value'=>0]); 
                      }                  
                      
                    break;
                  case 'numDevis':
                    if(!empty($dopSpec['createDevis']))
                    {
                      //тут получаем numDevis и пишем его потом
                      $numD=$dopSpec['numDevis'];
                      $value->setValue(['value'=>$numD]); 
                    }else{
                       unset($dataParamOBJET[$key]);
                    }
                    
                    break;
                    case 'allRequest':
                    
                      $value->setValue(['value'=>json_encode($requestData)]); 
                   
                    
                    break;
                  case 'dopNumdevis':
                    if(!empty($dopSpec['createDevis']))
                    {
                      //тут получаем numDevis и пишем его потом
                      $numD=$configParamData[$uidParam]['symb'].$dopSpec['numDevis'];
                      $value->setValue(['value'=>$numD]); 
                    }else{
                       unset($dataParamOBJET[$key]);
                    }
                    break;
              }
            }
        }
        return $dataParamOBJET;
    }

    public function stepFormgetDefBenifAction()
    {
        $_obj=new Obj();
        $defBeni='Personne morale';
        $dataUser=$_obj->getAllValues([$this->session->get('logged')['User']])[$this->session->get('logged')['User']->getUid()];
          foreach ($dataUser as $key => $value) {
            switch ($value->getUid()) {
              case '065991e9-4913-43fe-9b4e-000b50a00e16':
                if(!is_null($value->getValue()))
                {
                    $defBeni=$value->getValue()->getValue();
                }
                
                break;
              
            }
          }
          echo json_encode(['defBeniaciare'=>$defBeni]);
    }

    private function writeDefBenificiare($benificiare)
    {
        $_obj=new Obj();
        $dataUser=$_obj->getAllValues([$this->session->get('logged')['User']])[$this->session->get('logged')['User']->getUid()];
      foreach ($dataUser as $key => $value) {
        switch ($value->getUid()) {
          case '065991e9-4913-43fe-9b4e-000b50a00e16':
            
            $dataUser[$key]->setValue(['value' =>$benificiare]);
            break;
          
          default:
            # code...
            break;
        }
      }
      $this->session->get('logged')['User']->setAttributes($dataUser);
      $_obj->checkUniqueAndUpdateValues($this->session->get('logged')['User'], $this->session->get('logged')['User']);
    }
    private function getLastDevis()
    {
      //тут получаем последний девис и делаем ему +1, и записываем его и в спец конфиг и в него
      $_obj=new Obj();
      $uidObj=$this->CONFIG_SETT['№devis']['obj'];
      $objGlobal=new \Dzeta\Core\Instance($uidObj, '',new Core\Type\Type($this->CONFIG_SETT['№devis']['type'], ''));
      $data=$_obj->getAllValues([$objGlobal])[$uidObj];
      $numDevis=null;
      foreach ($data as $key => $value) {
        switch ($value->getUid()) {
          case $this->CONFIG_SETT['№devis']['attr']:
            $numDevis=$value->getValue()->getValue();
            $numDevis=$numDevis+1;

            $data[$key]->setValue(['value' => $numDevis]);
            break;
          
          default:
            # code...
            break;
        }
      }
      
      $objGlobal->setAttributes($data);
      $_obj->checkUniqueAndUpdateValues($objGlobal, $this->session->get('logged')['User']);
      
      $dataUser=$_obj->getAllValues([$this->session->get('logged')['User']])[$this->session->get('logged')['User']->getUid()];
      foreach ($dataUser as $key => $value) {
        switch ($value->getUid()) {
          case '184259cd-1736-4476-ac17-28dfe3c14efd':
            if(is_null($value->getValue()))
            {
              $numDevis1=0;
            }else{
              $numDevis1=$value->getValue()->getValue();
            }
            
            $numDevis1=$numDevis1+1;
            $dataUser[$key]->setValue(['value' => $numDevis1]);
            break;
          
          default:
            # code...
            break;
        }
      }
      $this->session->get('logged')['User']->setAttributes($dataUser);
      $_obj->checkUniqueAndUpdateValues($this->session->get('logged')['User'], $this->session->get('logged')['User']);
      //184259cd-1736-4476-ac17-28dfe3c14efd
      return $numDevis.'-'.$numDevis1;
    }
    

    public function getConfigEniAction(){

      $configParamJSON=file_get_contents(BASE_PATH.'/config/dop/DataEnemat/configDataSpecEnemat.json');

      echo $configParamJSON;
    }

    

    private function getValueForDataFormKeys($data,$keys){

      if(empty($keys)) return null;
      if(count($keys)<=1)
      {

        if(isset($data[$keys[0]]))
        {

          return $data[$keys[0]];
        }
        return null;
      }
      $upKey=array_shift($keys);
      if(isset($data[$upKey]))
      {
          return $this->getValueForDataFormKeys($data[$upKey],$keys);
      }
      return null;

    }



    public function getObjAction()
    {
      $_obj = new Obj();
    //  $class = new Core\Type\Type($config['types'][$type]['class'], '');
      $obj = new \Dzeta\Core\Instance(\Dzeta\Core\Instance::INSTANCE_ROOT, '');
      $children = $_obj->getObjsByClass(
      
        new Core\Type\Type('bc85f46f-7fdd-496e-bfe0-1ac578cb66c1', ''),'');
      echo json_encode( $children);
    }

    public function getKeySettingsMetkaAllAction(){
      //собираем все ключи для меток наших

    }

    public function getChildObjValuesAction()
    {
        $result = -1;
        $data = [];
        if($this->request->isGet())
        {
            $_obj = new Obj();
            $obj=$this->request->get('obj');           
            $type=$this->request->get('type');    
            $data= $_obj->getChildrenByType(new \Dzeta\Core\Instance($obj, ''),new Core\Type\Type($type, ''));
            $_obj->getAllValues($data);
            foreach ($data as $key=>  $value) 
            {
                $data[$key]->setChildren($this->getAllChildObj($value));                
            }
            $result=1;            
        }
        echo json_encode(['result' => $result, 'data' => $data]);
    }



    public function getAllChildObjValuesAction(){
        $result = -1;
        $data = [];
        $objName='';
        if($this->request->isPost())
        {
            $_obj = new Obj();
            $objs=$this->request->get('obj');           
            foreach ($objs as $key => $value) {
              $obj = new \Dzeta\Core\Instance( $value, '');
              $data[ $value]=$this->getAllChildObj($obj);
            }
            
            $result=1;
            
        }
      
        echo json_encode(['result' => $result, 'data' => $data]);
    }


    private function getAllChildObj( \Dzeta\Core\Instance $obj )
    {
      $_obj = new Obj();
      $childObj= $_obj->getChildrenByType($obj);
      $data = $_obj->getAllValues($childObj);
      foreach ($childObj as $key => $value) {
        $childObj[$key]->setChildren($this->getAllChildObj($value));
      }
      return $childObj;
    }

    public function getSpecTableBenif($dataRequest)
    {
        /*$json='{"declarant1":{"Nom":"AKOUCH","Nom de naissance":"AKOUCH","Prenom(s)":"KARIM","Date de naissance":"14/08/1982","Adresse declaree au 1er janvier 2019":"41B AV MARECHAL FOCH 47400 TONNEINS"},"declarant2":{"Nom":"AKOUCH","Nom de naissance":"LAKRIFA","Prenom(s)":"LOUBNA","Date de naissance":"13/04/1985","Adresse declaree au 1er janvier 2019":""},"common":{"Date de mise en recouvrement de l\'avis d\'impot":"31/07/2019","Date d\'etablissement":"24/07/2019","Nombre de part(s)":"4.00","Situation de famille":"Marié(e)s","Nombre de personne(s) a charge":"3","Revenu brut global":"28541 €","Revenu imposable":"28541 €","Impot sur le revenu net avant corrections":"152 €","Montant de l\'impot":"Non imposable","Revenu fiscal de reference":"28541 €"}}';
        $dataRequest=json_decode($json,true);*/
        $nameFile=BASE_PATH.'/config/dop/DataEnemat/configTablejson.docx';
        $templ=new \MyTemplateProcessor($nameFile);
        $dfv=$templ->GetAllTableSettMetka();
        $tbl=$dfv[0]['table'];
        if(isset($dataRequest['declarant1']))
        {
            foreach ($dataRequest['declarant1'] as $key => $value) 
            {
                if(strpos($key,'Adresse ')===0)
                {
                    $dataRequest['declarant1']['Adresse']=$value;
                }
            }
        }
        if(isset($dataRequest['declarant1']))
        {
            foreach ($dataRequest['declarant2'] as $key => $value) 
            {
                if(strpos($key,'Adresse ')===0)
                {
                    $dataRequest['declarant2']['Adresse']=$value;
                }
            }
        }
        if(isset($dataRequest['common']))
        {
            if($dataRequest['common']['Year'])
            {
                $dataRequest['common']['Year-1']=$dataRequest['common']['Year']-1;
                $dataRequest['common']['Year']=$dataRequest['common']['Year'].' ';                
            }
        }
       
        foreach ($dfv[0]['metk'] as $key => $value) 
        {           
           $metk=strip_tags($value);
           $mValue=explode('.',$metk);
           $vRequest=$this->getValRequestData($dataRequest,$mValue);
           $tbl=str_replace('{{'.$value.'}}',$vRequest,$tbl);
           //var_dump(['metka'=>$metk,'val'=>$vRequest]);
        }
        \DZ::tbllee($tbl);
        return $tbl;
    }

    public function getValRequestData($dataRequest,$dataMetka)
    {
        if(count($dataMetka)==1)
        {
            $arrP=array_shift($dataMetka);
            if(isset($dataRequest[$arrP]))
            {
                return $dataRequest[$arrP];
            }            
            return '';
        }
        $arrP=array_shift($dataMetka);
        if(isset($dataRequest[$arrP]))
        {
            return $this->getValRequestData($dataRequest[$arrP],$dataMetka);
        }
        return '';
    }

    public function test123Action()
    {
        $json='{"result":1,"data":{"declarant1":{"Nom":"AKOUCH","Nom de naissance":"AKOUCH","Prenom(s)":"KARIM","Date de naissance":"14\/08\/1982","Adresse declaree au 1er janvier 2019":"41B AV MARECHAL FOCH 47400 TONNEINS"},"declarant2":{"Nom":"AKOUCH","Nom de naissance":"LAKRIFA","Prenom(s)":"LOUBNA","Date de naissance":"13\/04\/1985","Adresse declaree au 1er janvier 2019":""},"common":{"Date de mise en recouvrement de l\'avis d\'impot":"31\/07\/2019","Date d\'etablissement":"24\/07\/2019","Nombre de part(s)":"4.00","Situation de famille":"Mari\u00e9(e)s","Nombre de personne(s) a charge":"3","Revenu brut global":"28\u00a0541 \u20ac","Revenu imposable":"28\u00a0541 \u20ac","Impot sur le revenu net avant corrections":"152 \u20ac","Montant de l\'impot":"Non imposable","Revenu fiscal de reference":"28\u00a0541 \u20ac"}},"errors":[]}';
        //https://docs.google.com/document/d/1e8vFOLYjRMyCha0-f0lo72BPCK4cGaJaad28f8gOdKQ/edit
        $arrGl=[
            'Bénéficiaire.Persone phys data'=>$this->getSpecTableBenif(json_decode($json,true)['data'])
        ];
        $thtg=new FillFile([['url'=>'https://docs.google.com/document/d/1e8vFOLYjRMyCha0-f0lo72BPCK4cGaJaad28f8gOdKQ/edit']],$arrGl);
        var_dump($thtg->uid);
    }

    public function testzapPHPAction()
    {
        $googleAccountKeyFilePath = BASE_PATH.'/config/dop/gifted-honor-236118-0330fc9ff24c.json';
        putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();       
        $client->setScopes([\Google_Service_Docs::DOCUMENTS,'https://www.googleapis.com/auth/drive']);

        $serviceDisk = new Google_Service_Drive($client);
        $serviceDocs = new \Google_Service_Docs($client);

        $idDo='1zxXLny2IIHaaPTPD08t33r_hYq0UsNPhsjATpS5IXG8';

        $copiedFile = new Google_Service_Drive_DriveFile();
        $copiedFile->setMimeType('application/vnd.google-apps.document');
       
        $copiedFile->setParents(array('1c7eDHVmMRHs5cNI2s1KUSPUDJUtd68ND'));
        $resss=$serviceDisk->files->copy($idDo, $copiedFile);
        $id=$resss->id;        

        

        $replaceAllTextRequest = [
            'replaceAllText' => 
            [
                'replaceText' =>'@1234@' ,
                'containsText' => 
                [
                    'text' =>'{{Organization.Logo}}',
                    'matchCase' => true,
                ],
            ]
        ];
        $dataZamenaText=[
            'Organization.Organization'=>'Organization1',
            'Incitation'=>'1252',
            'Bénéficiaire.Raison sociale'=>'test1',
            'Bénéficiaire.Nom'=>'test2',
            'Bénéficiaire.Prénom'=>'test3',
            'Bénéficiaire.Adresse'=>'test4',
            'Bénéficiaire.Code postale'=>'test5',
            'Bénéficiaire.Ville'=>'test6',
            'Bénéficiaire.Phone'=>'test7',
            'Operations.Pre visite'=>'test8',
        ];

        $requests=[];
        foreach ($dataZamenaText as $key => $value) 
        {
            $requests[] = new \Google_Service_Docs_Request([
                'replaceAllText' => 
                [
                    'replaceText' => $value,
                    'containsText' => 
                    [
                        'text' =>'{{'.$key.'}}',
                        'matchCase' => true,
                    ]
                ]
            ]);
        }
        $document = $serviceDocs->documents->get($id);
        $imageContenet=[
            'Image.logo'=>[
               // 'uri'=>'https://enemat.misnik.by/object/downloadFile?file=c02ab3fe-776c-44c3-808c-31e711f6743a&keyAPI=7ef2acb5-5697-4c31-8c14-57ba61cee9dd'
                
                'uri'=>'https://drive.google.com/u/0/uc?id=1tRcTJc0GczyjwkWDKj7P6O9iAIhHjDfQ&export=download'
            ]

        ];
        
        //Перебираем контентную составляющую документа
        foreach($document->body->content as $content)
        {
            if(!empty($content['paragraph']))
            {
                foreach($content['paragraph']['elements'] as $elements)
                {
                  //ищем наше значение
                    foreach ($imageContenet as $key => $value) 
                    {
                        $pos = strpos($elements['textRun']['content'], $key);
                        if($pos!==false)
                        {
                            $requests[] = new \Google_Service_Docs_Request([
                                'insertInlineImage' => [
                                    'uri' =>  $value['uri'], //Вставляем картинку которая была загружена в гугл-диск
                                    'location' => array(
                                      'index' => $elements['startIndex'], //Наша стартовая позиция
                                    ),
                                    'objectSize' =>[
                                      'height' => [
                                          'magnitude' => 100,
                                          'unit' => 'PT',
                                    ],
                                    'width' => [
                                        'magnitude' => 100,
                                        'unit' => 'PT',
                                    ]
                                  ]
                                ]
                            ]);
                        }
                    }
                }
            }else if(!empty($content['table']))
            {
                foreach ($content['table']['tableRows'] as $tableRowskey => $tableRow) 
                {
                    foreach ($tableRow['tableCells'] as $keytableCells => $tableCell) 
                    {
                        foreach ($tableCell['content'] as $keycontent => $valuecontent) 
                        {
                            if(!empty($valuecontent['paragraph']))
                            {
                                foreach($valuecontent['paragraph']['elements'] as $elements)
                                {
                                  //ищем наше значение
                                    if ($elements->textRun) 
                                    {
                                        foreach ($imageContenet as $key => $value) 
                                        {
                                            $pos = strpos($elements['textRun']['content'], '{{'.$key.'}}');

                                            if($pos!==false)
                                            {                                                
                                                $requests[] = new \Google_Service_Docs_Request([
                                                    'insertInlineImage' => [
                                                        'uri' =>  $value['uri'], //Вставляем картинку которая была загружена в гугл-диск
                                                        'location' => array(
                                                          'index' => $elements['startIndex'], //Наша стартовая позиция
                                                        ),
                                                        'objectSize' =>[
                                                          'height' => [
                                                              'magnitude' => 100,
                                                              'unit' => 'PT',
                                                        ],
                                                        'width' => [
                                                            'magnitude' => 100,
                                                            'unit' => 'PT',
                                                        ]
                                                      ]
                                                    ]
                                                ]);
                                            }
                                        }  
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        foreach ($imageContenet as $key => $value) 
        {
            $requests[] = new \Google_Service_Docs_Request([
                'replaceAllText' => 
                [
                    'replaceText' =>'',
                    'containsText' => 
                    [
                        'text' =>'{{'.$key.'}}',
                        'matchCase' => true,
                    ]
                ]
            ]);
        }
        $batchUpdateRequest = new \Google_Service_Docs_BatchUpdateDocumentRequest(['requests' => $requests]);
        $response = $serviceDocs->documents->batchUpdate($id, $batchUpdateRequest);
         $content = $serviceDisk->files->export($id, 'application/pdf', [
         'alt' => 'media'
        ]);
        //var_dump($response);
        return;


        /*
            



        */

    }


    public function testFileAction()
    {
        $googleAccountKeyFilePath = BASE_PATH.'/config/dop/gifted-honor-236118-0330fc9ff24c.json';
        putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();       
        $client->setScopes([\Google_Service_Docs::DOCUMENTS,'https://www.googleapis.com/auth/drive']);

        $serviceDisk = new \Google_Service_Drive($client);
        $folderId='1f9Os36B6znCypZDy5ZDxUURWszjAi5W4';
        $optParams = array(
            'pageSize' => 300,
            'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
            'q' => "'".$folderId."' in parents"
        );
        $results = $serviceDisk->files->listFiles($optParams);
       
        $uniqID=uniqid();
        $folderSavePath=BASE_PATH.'/ufiles/fillFileFr/'.$uniqID.'_save';
        if (!mkdir($folderSavePath, 0777)) 
        {
            $uid='';
            return null;
        }
        chmod($folderSavePath, 0777);
        $filesWORDGoogle=[];
        mkdir($folderSavePath.'/goog', 0777);
        $realKey=1;
        foreach ($results->getFiles() as $key => $value) 
        {            
            if($value->getFileExtension()=='docx')
            {               
                $idFILE=$value->getID();
                $nameFile=$value->getName();
                if(strpos($nameFile,'~$')===0)
                {
                    continue;
                }
                if(file_exists($folderSavePath.'/'.$nameFile))
                {
                    $nameFile=$idFILE.$nameFile;
                }
                $nameFile=mb_eregi_replace('[а-яё]', '',$realKey.'_'.$nameFile);
                $realKey++;
                $filesWORDGoogle[]=['path'=>$folderSavePath.'/goog/'.$nameFile,'file'=>$nameFile,'id'=>$idFILE,'real'=>$value->getName()];
                $content = $serviceDisk->files->get($idFILE, array("alt" => "media"));
                $outHandle = fopen($folderSavePath.'/goog/'.$nameFile, "w+");
                while (!$content->getBody()->eof()) 
                {
                    fwrite($outHandle, $content->getBody()->read(1024));
                }        
                fclose($outHandle);                
            }
            
        }
        $pathWord=$folderSavePath.'/phpWord';
        mkdir($pathWord, 0777);
        $pathWordFiles=[];
        foreach ($filesWORDGoogle as $value) 
        {
            $templ=new \MyTemplateProcessor($value['path']);
            $variables=$templ->getVariables();
            $templ->saveAs($pathWord.'/'.$value['file']);
            $pathWordFiles[]=['path'=>$pathWord.'/'.$value['file'],'file'=>$value['file'],'real'=>$value['real'],'variables'=>$variables,'id'=>$value['id']];
        }
        $pathPDf=$folderSavePath.'/pdf';
       // \PhpOffice\PhpSpreadsheet\
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet(); 
        foreach ($pathWordFiles as $key=>  $value) 
        {
            $namePDF=$pathPDf.'/'.str_replace('.docx', '.pdf',$value['file']);
            $cmd=BASE_PATH . "/../".'doc2pdf/build/doc2pdf "'.$value['path'].'" "'.$namePDF.'"';
            $output = exec($cmd);
            $worksheet1 = $spreadsheet->createSheet();
            $worksheet1->setTitle(substr($value['file'],0,25));
            $worksheet1->setCellValue('A1', 'Variables');
            $worksheet1->setCellValue('B1', 'ID');
            $worksheet1->setCellValue('B2', $value['id']);
            $worksheet1->setCellValue('C1', 'Real name');
            $worksheet1->setCellValue('C2', $value['real']);
            $worksheet1->getColumnDimension('A')->setWidth(100);
            $worksheet1->getColumnDimension('B')->setWidth(40);
            $worksheet1->getColumnDimension('C')->setWidth(30);  
            $worksheet1->freezePane('A2');         
            sort($value['variables']);
            foreach ($value['variables'] as $numKey=> $valuevar) 
            {
                $worksheet1->setCellValue('A'.($numKey+2), $valuevar);  
            }
        }       
       
        $sheetIndex = $spreadsheet->getIndex($spreadsheet->getSheetByName('Worksheet'));
        $spreadsheet->removeSheetByIndex($sheetIndex);
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($folderSavePath.'/_result'.$uniqID.'.xlsx');

        $createZipFile=function($source, $destination)
        {
            if (!extension_loaded('zip') || !file_exists($source))
            {
                return false;
            }

            $zip = new \ZipArchive();
            if (!$zip->open($destination, \ZIPARCHIVE::CREATE)) 
            {
                return false;
            }
 
            $source = str_replace('\\', DIRECTORY_SEPARATOR, realpath($source));
            $source = str_replace('/', DIRECTORY_SEPARATOR, $source);
 
            if (is_dir($source) === true) 
            {
                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source),\RecursiveIteratorIterator::SELF_FIRST); 
                foreach ($files as $file) 
                {
                    $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
                    $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
         
                    if ($file == '.' || $file == '..' || empty($file) || $file == DIRECTORY_SEPARATOR) {
                        continue;
                    }
                    // Ignore "." and ".." folders
                    if (in_array(substr($file, strrpos($file, DIRECTORY_SEPARATOR) + 1), array('.', '..'))) {
                        continue;
                    }
         
                    $file = realpath($file);
                    $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
                    $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
         
                    if (is_dir($file) === true) {
                        $d = str_replace($source . DIRECTORY_SEPARATOR, '', $file);
                        if (empty($d)) {
                            continue;
                        }
                        $zip->addEmptyDir($d);
                    } elseif (is_file($file) === true) {
                        $zip->addFromString(str_replace($source . DIRECTORY_SEPARATOR, '', $file),
                            file_get_contents($file));
                    } else {
                        // do nothing
                    }
                }
            } elseif (is_file($source) === true) 
            {
                $zip->addFromString(basename($source), file_get_contents($source));
            } 
            return $zip->close();
        };       
        $zipFile=$folderSavePath.'/'.$uniqID.'.zip';
        $createZipFile($folderSavePath,$zipFile);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($zipFile).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($zipFile));
        readfile($zipFile);
        exit();
    }

    public function downloadFileSpecAction()
    {
      
      //$file=$value->getValue();  $file->getRealName()
      //readfile(BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$file->getuid().\Dzeta\Core\Value\File::FILE_EXT);
      //518454bc-b0b5-4485-b693-7939c42ec667  25ebca02-a44b-437e-aa17-1b955723a827
      $filesUIDs=['518454bc-b0b5-4485-b693-7939c42ec667','25ebca02-a44b-437e-aa17-1b955723a827'];
      $nameFile='testttg';
      
      if ($this->request->isGet())
      {
        $_file=new Files();
        $zip = new \ZipArchive();
        $temp =@tempnam("/tmp", "zip");;
        $zip->open($temp, \ZIPARCHIVE::CREATE);
        $files=$this->request->get('files');
        if(count($files)>1)
        {
          foreach ($files as $key => $value) 
          {
            $file=new \Dzeta\Core\Value\File($value, '');
            $res=$_file->getInfoFile($file);
            $zip->addFromString($file->getName(), file_get_contents(BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$file->getValue().\Dzeta\Core\Value\File::FILE_EXT));
            $nameFile=pathinfo($file->getName());
          }
          $zip->close();
          $t=explode(' ',$nameFile['filename']);
          array_shift($t);
          $nameFileZIP=implode(' ',$t);
          $nameFileZIP=trim(str_replace(array("/","|","\\", "?", ":", ";"), "", $nameFileZIP));
          if($nameFileZIP=='')
          {
            $nameFileZIP='tmp';
          }
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="'.$nameFileZIP.'.zip"');
          header('Expires: 0');
          header('Cache-Control: must-revalidate');
          header('Pragma: public');
          header('Content-Length: ' . filesize($temp));
          readfile($temp);
          exit();
        }else
        {
          $file=new \Dzeta\Core\Value\File($files[0], '');
          $_file=new Files();
          $res=$_file->getInfoFile($file);          
          \Dzeta\Core\Value\File::downloadFile($file);
        }
        

      }     
      
    }
  

    private function conevrtSpecFormat($value)
    {
      
      $nStr=number_format((float)$value, 2, ',', ' ');
      $nStr=str_replace(',00', '', $nStr);
      return $nStr;
    }

    public function testexcelAction()
    {
      $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
      $spreadsheet = $reader->load("/mnt/c/Users/NotePC/Downloads/testExcell.xlsx");
      $cells = $spreadsheet->getSheetByName('Sheet1')->getCellCollection();

        // Далее перебираем все заполненные строки
      $array=[];
       /* for ($row = 2; $row <= $cells->getHighestRow(); $row++){

            $array[$row]['id']=($cells->get('A'.$row))?$cells->get('A'.$row)->getValue():'';

            $array[$row]['name']=($cells->get('B'.$row))?$cells->get('B'.$row)->getValue():'';

            $array[$row]['description']=($cells->get('C'.$row))?$cells->get('C'.$row)->getValue():'';

            $array[$row]['price']=($cells->get('D'.$row))?$cells->get('D'.$row)->getValue():'';

            $array[$row]['currency']=($cells->get('E'.$row))?$cells->get('E'.$row)->getValue():'';

        }*/
        $sheetExcell=$spreadsheet->getSheetByName('Sheet1');
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        list($w, $h) = getimagesize('/mnt/c/Users/NotePC/Downloads/stamp.jpg');
        if($w/$h>2)
        {            
          $h=4.5*$h/$w;
          $w=4.5;
        }else
        {           
          $w=2.5*$w/$h;
          $h=2.5;
        }
        $w*=38;
        $h*=38;
        var_dump([$w,$h]);
        $drawing->setPath('/mnt/c/Users/NotePC/Downloads/stamp.jpg');
        $drawing->setName('testN');
        $drawing->setDescription('testD');
        $drawing->setHeight($h);
        $drawing->setWidth($w);
        $drawing->setCoordinates(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex(2) . 2);
        $drawing->setWorksheet($sheetExcell);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save("/mnt/c/Users/NotePC/Downloads/testExcell1.xlsx");

        echo json_encode($array);
    }
    /*public function stepFormEniTwoAction()
    {
      $loader = new \Phalcon\Loader();
      $loader->registerNamespaces(
      [
        'generateDocEnemat' =>APP_PATH . '/library/projFR/generateDocument/'
      ]
      );    
      $loader->register();
     
      $genDoc=new \generateDocEnemat\generateDocs($this->request->get(),$this->session->get('logged')['User']);
      $res=$genDoc->start();
      if(empty($res))
      {
        echo json_encode(['result'=>0]);
      }else
      {
        echo json_encode(['result'=>1,'uidsFile'=>$genDoc->genUidFiles()]);
      }
    }*/
}



