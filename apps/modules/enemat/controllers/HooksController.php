<?php
// declare(strict_types=1);

namespace Dzeta\Modules\Enemat\Controllers;

use Phalcon\Config\Adapter\Json;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

use Dzeta\Modules\Enemat\Helpers\SignHelper;

class HooksController extends Controller
{
    public function signedAction($token, $data) {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/SignDocConfig.json');
        if ($token == $config['hook_token']) {
            $result = file_get_contents('php://input');
            $result = json_decode($result, true);
            list($serviceName, $type) = explode('|', urldecode($data));
            SignHelper::getDocument($type, $serviceName, $result);
        }
    }

    public function getTextPositionsFromPdfAction($token) {
    	$result = ['result' => 0, 'data' => []];
    	if ($this->request->isPost()) {
	        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/SignDocConfig.json');
    		if ($token == $config['hook_token']) {
	            $data = file_get_contents('php://input');
	            $data = json_decode($data, true);
	            if (isset($data['file']) && isset($data['texts'])) {
	    			$filename = tempnam(sys_get_temp_dir(), 'pdf-text-pos');
	    			file_put_contents($filename, base64_decode($data['file']));
	    			unset($file);
	    			foreach ($data['texts'] as $text) {
	    				$positions = SignHelper::getTextPositionsFromPdf($filename, $text);
	    				$result['data'][$text] = [
	    					'positions' => $positions
	    				];
	    			}
	    			$result['result'] = 1;
	            }
    		}
    	}
    	echo json_encode($result);
    }

    public function convertDocxPDFAction()
    {
    	if($this->request->isPost())
    	{
    		$config = new Json(BASE_PATH . '/config/dop/DataEnemat/enematGlobal.json');
    		$key=$this->request->get('key');
    		$uniqID=$this->request->get('uniqID');
    		if($key!=$config['keyForConvertdocx'])
    		{
    			echo json_encode(['result'=>0]);
    			return;
    		}
    		$file=$this->request->get('file');
    		//помещаем во временное хранилище    		
    		//$path=BASE_PATH.'/ufiles/1/test';
    		$path=sys_get_temp_dir();
    		$attempt=0;
    		while(1)
    		{
    			$id=$uniqID;

	    		$tmpfnameDOCX =$path.'/'.$id.'.docx';
	    		$attempt++;
	    		$handle = fopen($tmpfnameDOCX, "w");
	    		if($handle===false)
	    		{
	    			
	    		}else
	    		{
	    			fwrite($handle, base64_decode($file));
					fclose($handle);
					break;
	    		}
				if($attempt>20)
				{
					echo json_encode(['result'=>-2]);
    				return;
				}
				usleep(5000);
    		}
    		    
			$countIterr=0;
			chmod($tmpfnameDOCX,0777);
			while(1)
			{
				$tmpfnamePDF =$path.'/'.$id.'.pdf';	
	    		$cmd=BASE_PATH . "/../".'doc2pdf/build/doc2pdf '.$tmpfnameDOCX.' '.$tmpfnamePDF;
	    		
	    		$output = exec($cmd);
	    		$countIterr++;
	    		if(file_exists($tmpfnamePDF))
	    		{
	    			break;
	    		}
	    		if($countIterr>10)
	    		{
	    			echo json_encode(['result'=>-1,'out'=>$output,'cmd'=>$cmd]);
    				return;
	    		}
	    		usleep(3000);
			}		
			if(filesize($tmpfnamePDF)>0)
			{
				echo json_encode(['result'=>1,'file'=>base64_encode(file_get_contents($tmpfnamePDF))]);
    			@unlink($tmpfnameDOCX);@unlink($tmpfnamePDF);
    			return;
			}
    		echo json_encode(['result'=>-3]);
    		return;
    		
    	}
    }
}