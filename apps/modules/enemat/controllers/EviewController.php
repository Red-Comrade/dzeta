<?php
/**
 * Created by PhpStorm.
 * User: Kirk7by
 * Date: 26.02.2021
 * Time: 13:38
 */

namespace Dzeta\Modules\Enemat\Controllers;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;

use Dzeta\Models\View;
use Dzeta\Models\Obj;

use Dzeta\Helpers\ViewHelper;
use Dzeta\Controllers\ControllerBase;


class EViewController extends ControllerBase
{

    const PARAM_ODT_DELETED=['attr'=>'47f83a82-7e3c-4116-96c2-3bac4d90ce21','dataType'=>1,'class'=>'5086f503-d8b2-4a5b-a3d9-e5abc7ccb728'];


    public function getConfigAction() {
        $config_t = $this->_getSettTreeTable();

        return json_encode(["data" => $config_t, "result" => 1]);
    }

    public function getTableDataAction() {
        $result = -1;
        $data = [];

        if ($this->request->isPost())
        {
            $_view = new View();
            $view = ViewHelper::getViewFromRequest($this->request);

            $UserID = null;
            if($this->session->has('logged')) {
                $UserID = $this->session->get('logged')['User']->getUid();

            }
            else if($this->config->dzeta->GuestUID){
                $UserID = $this->config->dzeta->GuestUID;
            }
            else {
                $this->response->redirect('login');
                return;
            }

             $data=$_view->getDataTable($view, $UserID);

             //Для энемата мы получаем спец данные выгрузки
             $config_t = $this->_getSettTreeTable();
             $assoc_data = $config_t["rootView"]["assoc_data"];


             //uid-классов и uid-первых параметров, чтобы проверить объекты
             $root_uid = $config_t["rootView"]["uid"];
             $root_uid_first_param = $config_t["rootView"]["assoc_data"]["name"];

             $facture_uid = "";
             $AH_uid = "";
             $devis_uid = "";
             $devis_uid_first_param = "";

             //Массив параметров для выгрузки
             $cl_params_data = [];

             foreach ($config_t["docView"] as $key=>$item) {
                 if($item["type"] == "devis") {
                     $devis_uid = $key;
                     $devis_uid_first_param = $item["assoc_data"]["date_create"];
                 }
                 else {
                     foreach ($item["assoc_data"] as $key_ac=>$item_ac) {
                         if(!empty($item_ac)) {
                             array_push($cl_params_data, ["cl" => $key, "attr" => $item_ac]);
                         }
                     }
                 }

                 if($item["type"] == "AH") {
                     $AH_uid = $key;
                 }
                 if($item["type"] == "facture") {
                     $facture_uid = $key;
                 }
             }
             $oper_uid = $config_t["operView"]["uid"];
             $oper_uid_first_param = $config_t["operView"]["assoc_data"]["name"];


             $obj_arr = [];

             $execution_time = 0;
             //Формируем данные
             $new_data = [];
             foreach ($data["data"] as $row) {
                 $obj_uid = $row[$root_uid_first_param]["o"];
                 if(!isset($new_data[$obj_uid])) {
                     $root_data = [
                         "data" => []
                     ];
                     $this->_convertDataAssoc($config_t["rootView"]["assoc_data"], $row, $root_data);
                     $root_data["data"]["key"] = $obj_uid;
                     $time_start = microtime(true);
                     $new_data[$obj_uid] = [
                         "key" => $obj_uid,
                         "leaf" => false,
                         "lvl" => 1,
                         "data" => $root_data["data"],
                         "devis_data" => [],
                        // "data_child" =>  $this->_getChildrenByType(["key" => $obj_uid], $AH_uid, $facture_uid),
                         "data_child" => [] // $this->_getChildrenByType(["key" => $obj_uid], $AH_uid, $facture_uid),
                     ];
                     $time_end = microtime(true);
                     $execution_time +=($time_end - $time_start);
                     array_push($obj_arr, $obj_uid);
                 }
                 if(isset($row[$devis_uid_first_param]["o"])) {
                     $devis_obj_uid = $row[$devis_uid_first_param]["o"];

                     if(!isset($new_data[$obj_uid]["devis_data"][$devis_obj_uid])) {
                         $devis_data = [
                             "key" => $devis_obj_uid,
                             "parent_key" => $obj_uid,
                             "type" => "devis",
                             "isSigned" => false,
                             "hasStatus" => false,
                             "class" => $devis_uid,
                             "data" => [],

                             "operations" => [], //operations
                         ];
                         $this->_convertDataAssoc($config_t["docView"][$devis_uid]["assoc_data"], $row, $devis_data);
                         $new_data[$obj_uid]["devis_data"][$devis_obj_uid] = $devis_data;
                     }

                     if(isset($row[$oper_uid_first_param]["o"])) {
                         $operations_obj_uid = $row[$oper_uid_first_param]["o"];
                         $operations_data = [
                             "data" => []
                         ];
                         $this->_convertDataAssoc($config_t["operView"]["assoc_data"], $row, $operations_data);
                         $new_data[$obj_uid]["devis_data"][$devis_obj_uid]["operations"][$operations_obj_uid] = $operations_data;
                     }
                 }
             }



            $_obj = new Obj();

            $time_start = microtime(true);
            //Получаем значения
            $res_data_value = $_obj->GetValuesForChildObjs($obj_arr, $cl_params_data);
            $time_end = microtime(true);
            $time_new = ($time_end - $time_start);


             $nodes = [];
             foreach ($new_data as $key_r=>$row) {
                 if( isset( $res_data_value[$row["key"]] ) ) {
                     $new_data[$key_r]["data_child"] = $res_data_value[$row["key"]]["objects"];
                 }

                 if(count($row["devis_data"]) > 0) {
                     $new_data[$key_r]["data"]["devis"] = $row["devis_data"][array_key_first($row["devis_data"])];
                 }

                 array_push($nodes, $new_data[$key_r]);
             }
             
            $result = 1;

            echo json_encode([
                'result' => $result,
                'data' => $data["data"],
                'isLoadTotal' => $data["isLoadTotal"],
                'allCount' => $data["allCount"],

                'config' => $config_t,
                'data_format' => $nodes,

                "time" => $execution_time,
                "new_time" => $time_new,
            ]);
        }
    }


    private function _convertDataAssoc($assoc_data, $row, &$data) {
        $signed_file = false; //наличие подписанного файла
        $date_signature = false; //Наличие записи о дате подписания

        foreach ($assoc_data as $key_asoc => $value_asoc) {
            $uid = $value_asoc;
            if(!empty($uid) && isset($row[$uid]["v"]) && $row[$uid]["v"]){
                //Значит файлы
                if(isset($row[$uid]["pvw"])) {
                    $data["data"][$key_asoc] =
                        [
                            "name" => $row[$uid]["v"],
                            "p_uid" => $value_asoc,
                            "preview" => null,
                        ];

                    $signed_file = true;
                }
                else if(isset($row[$uid]["ln"])) {
                    $data["data"][$key_asoc] = [
                        "name" => $row[$uid]["ln"],
                        "uid" => $row[$uid]["v"]
                    ];
                }
                else {
                    $data["data"][$key_asoc] = $row[$uid]["v"];

                    if ($key_asoc == 'date_signature') {
                        $date_signature = true;
                    }
                    if ($key_asoc == 'status') {
                        $data["hasStatus"] = true;
                    }
                }
            }
            else{
                $data["data"][$key_asoc] = "";
            }
        }
        if($signed_file && $date_signature) {
            $data["isSigned"] = true;
        }
    }

    private function _getChildrenByType($parentNode, $AH_uid, $facture_uid) {
        $_obj = new Obj();
        $obj = new Instance($parentNode["key"], '');

        $childObj = array_merge($_obj->getChildrenByType($obj, new Core\Type\Type($AH_uid, "tmp1")), $_obj->getChildrenByType($obj, new Core\Type\Type($facture_uid, "tmp2")));
        $attr_tmp = $_obj->getAllValues($childObj);
        $data_converted = [];

        //Стратегическое решение делать форматирование js
        return $childObj;
    }

    public function getSettTreeTableAction() {
       $dt = $this->_getSettTreeTable();
        echo json_encode(["sett"=>$dt]);
    }

    public function _getSettTreeTable() {
        $filePathMain=BASE_PATH.'/config/dop/DataEnemat/configTreeTableEnemat.json';
        $dt=[];
        if(file_exists($filePathMain))
        {
            $dt=json_decode(file_get_contents($filePathMain),true);
        }
        return $dt;
    }

    public function removeODTAction()
    {
        //"удаление"(установка флага) объекта objet des travaux 
        $result = ['result' => -1, 'data' => [], 'errors' => []];
        if ($this->request->isGet()) {
            $_obj = new \Dzeta\Models\Obj();
            $uidObjetDesTravaux = $this->request->get('odt');
            $typeAction = $this->request->get('action');
            if ($typeAction == 'archive') {
                $val = 2;
            }
            else if ($typeAction == 'delete') {
                $val = 1;
            }
            else if($typeAction == "unarchive") {
                $val = null;
            }
            else {
                echo json_encode($result);
                return;
            }
            $obj = new  \Dzeta\Core\Instance($uidObjetDesTravaux, '');
            $attrs = [];
            $attr = new \Dzeta\Core\Type\Attribute(self::PARAM_ODT_DELETED['attr'], '', self::PARAM_ODT_DELETED['dataType'], false, false);
            $attr->setValue(['value' => $val]);
            $attrs[] = $attr;
            $obj->setAttributes($attrs);
            $results = $_obj->addValues([$obj], $this->session->get('logged')['User']);
            if (isset($results['result'][self::PARAM_ODT_DELETED['attr']]) && $results['result'][self::PARAM_ODT_DELETED['attr']]['result'] == 1) {
                $result['result'] = 1;
            }
        }
        echo json_encode($result);
    }

}