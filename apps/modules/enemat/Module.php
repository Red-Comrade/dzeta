<?php

namespace Dzeta\Modules\Enemat;

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

define('ENEMAT_MODULE_PATH', __DIR__);

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers the module auto-loader
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();
        $config = $di->getConfig();

        $loader->registerNamespaces(
            [
                'Dzeta\Modules\Enemat\Controllers' => ENEMAT_MODULE_PATH . '/controllers/',
                'Dzeta\Modules\Enemat\Models' => ENEMAT_MODULE_PATH . '/models/',
                'Dzeta\Modules\Enemat\Helpers' => ENEMAT_MODULE_PATH . '/helpers',
                'Dzeta\Modules\Enemat\Library' => ENEMAT_MODULE_PATH . '/library',
                'Dzeta\Library\SignDoc' => $config->application->libraryDir . '/SignDoc',
            ]
        );

        $loader->register();
    }

    /**
     * Registers the module-only services
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {

        /**
         * Setting up the view component
         */
        $di->setShared('view', function () {
            $view = new View();
            $view->setDI($this);
            $view->setViewsDir(ENEMAT_MODULE_PATH . '/views/');
            return $view;
        });
    }
}