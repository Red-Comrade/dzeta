<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Models\Type;
use Dzeta\Models\Obj;
use Dzeta\Models\Calculation;
use Dzeta\Controllers\ControllerBase;

class TypeController extends ControllerBase
{
	/**
	 * Return all types by list
	 *
	 */
	public function getAllAction() {
		$type = new Type();
		$types = $type->getAll();
		echo json_encode($types);
	}

	/**
	 * Return all types by tree
	 *
	 */
	public function getTreeAction() {
		$type = new Type();
		$tree = $type->getTree();
		echo json_encode($tree);
	}

	/**
	 * Get children types
	 * $_GET['uid'] - uid of parent type
	 *
	 */
	public function getChildrenAction() {
		$result = -1;
		$data = [];

		if ($this->request->isGet()) {
			$parent = new Core\Type\Type($this->request->get('uid'), '');

			$_type = new Type();
			$data = $_type->getChildren($parent);
			$result = 1;
		}

		echo json_encode(['result' => $result, 'data' => $data]);
	}

	/**
	 * Get info about type
	 * $_GET['uid'] - uid of type
	 *
	 */
	public function getAction() {
		$result = -1;
		$data = [];

		echo json_encode(['result' => $result, 'data' => $data]);
	}

	/**
	 * Add type
	 * $_POST['name'] - name of type
	 * $_POST['parent'] - parent of type
	 * $_POST['isSystem'] - system flag of type
	 * $_POST['base'] - *optional* type used for inheritance
	 *
	 */
	public function addAction() 
	{
		$result = -1;
      	$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

		if ($this->request->isPost()) 
		{
			$parent = new Core\Type\Type($this->request->get('parent'), '', false);
			$type = new Core\Type\Type('', $this->request->get('name'), $this->request->get('isSystem'), $parent);
			if (!empty($this->request->get('base'))) 
			{
				$base = new Core\Type\Type($this->request->get('base'), '', false);
				$type->setBaseType($base);
			}

			$_type = new Type();
			$res = $_type->add($type, $this->session->get('logged')['User']);
			$dataResult['errors']=$res['error'];
			$result=$res['result'];
			$dataResult['data']['uid'] = $type->getUid();
		}
		$dataResult['result']=$result;
  		echo json_encode($dataResult);
	}

	/**
	 * Update type
	 * $_POST['uid'] - uid of type
	 * $_POST['name'] - name of type
	 */
	public function updateAction() 
	{
		$result = -1;
      	$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

		if ($this->request->isPost()) 
		{
			$type = new Core\Type\Type($this->request->get('uid'), $this->request->get('name'), false);
			$_type = new Type();
			$res = $_type->rename($type, $this->session->get('logged')['User']);
			$dataResult['errors']=$res['error'];
			$result=$res['result'];
			$dataResult['data']['uid'] = $type->getUid();
		}
		$dataResult['result']=$result;
  		echo json_encode($dataResult);
	}

	/**
	 * Delete type
	 * $_POST['type'] - uid of type
	 * $_POST['type_parent'] - parent uid of type
	 */
	public function deleteAction() {
		$result = -1;
		$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

		if ($this->request->isPost()) {
			$type = new Core\Type\Type($this->request->get('type'), '');
			$type->setParent(new Core\Type\Type($this->request->get('type_parent'), ''));
			$_type = new Type();
			$result = $_type->remove($type, $this->session->get('logged')['User']);
			if($result['result']!=1)
			{	
				$dataResult['errors']=$result['error'];
			}
			$result=$result['result'];
		}
		$dataResult['result']=$result;
		echo json_encode($dataResult);
	}

	/**
	 * Add link type
	 * $_POST['type'] - uid of type
	 * $_POST['type_parent'] - parent uid of type
	 */
	public function addLinkAction() {
		$result = -1;
		$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

		if ($this->request->isPost()) {
			$type = new Core\Type\Type($this->request->get('type'), '');
			$type->setParent(new Core\Type\Type($this->request->get('type_parent'), ''));
			$_type = new Type();
			$result = $_type->addLink($type, $this->session->get('logged')['User']);
			if($result['result']!=1)
			{	
				$dataResult['errors']=$result['error'];
			}
			$result=$result['result'];
		}
		$dataResult['result']=$result;
		echo json_encode($dataResult);
	}

	/**
	 * Get attributes groups for type
	 * $_GET['uid'] - uid of type
	 *
	 */
	public function getGroupsAction() {
		$result = -1;
		$data = [];

		if ($this->request->isGet()) {
			$result = 1;
			$type = new Core\Type\Type($this->request->getQuery('uid'), '');

			$_type = new Type();
			$data = $_type->getGroups($type);
		}

		echo json_encode(['result' => $result, 'data' => $data]);
	}

	public function getCalcsAction()
	{
		$result = -1;
		$data = [];

		if ($this->request->isGet()) {
			$result = 1;
			//new Core\Calculation($this->request->get('uid'), '', '')
			$type = new Core\Type\Type($this->request->getQuery('uid'), '');

			$_calc = new Calculation();
			$data = $_calc->getCalcClasses($type);
		}
			echo json_encode(['result' => $result, 'data' => $data]);
	}


	//removeAllobjsClass
    /**
     * Delete all objs type
     * $_POST['type'] - uid of type     
     */
    public function deleteAllObjsAction() {
        $result = -1;
        $dataResult = ["result"=>$result, 'data'=>[],'errors'=>[]];

        if ($this->request->isPost()) {
            $type = new Core\Type\Type($this->request->get('type'), '');
            $_obj = new Obj();
            $result = $_obj->removeAllobjsClass($type, $this->session->get('logged')['User'],0);
            if($result['result'] != 1)
            {               
                $dataResult['errors']=$result['error'];
            }

            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }

    /**
     * Delete all objs type
     * $_POST['type'] - uid of type     
     */
    public function deleteAllObjsCountAction() {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[],  "count" => 0,];

        if ($this->request->isPost()) {
            $type = new Core\Type\Type($this->request->get('type'), '');;            
            $_obj = new Obj();
            $result = $_obj->removeAllobjsClass($type, $this->session->get('logged')['User'],1);
            $dataResult["count"] = $result["count"];
            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }
}
