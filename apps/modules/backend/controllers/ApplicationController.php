<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Models\App;
use Dzeta\Models\View;
use Dzeta\Models\Folder;
use Dzeta\Controllers\ControllerBase;
use Dzeta\Helpers\ViewHelper;


class ApplicationController extends ControllerBase
{
    /**
    * Return all types by tree
    *
    */
     public function getTreeAction(){
        $_app = new App();


         $params = [
             "isGetPermiss" => $this->request->get('isGetPermiss'),
             "UserID" => $this->session->get('logged')['User']->getUid(),
             "GroupID" => $this->request->get('GroupID')
         ];

        echo json_encode($_app->getAllTreeAppView($params));
     }


    /**
     * Add application
     * $_POST['app_name']         - name of app
     * $_POST['app_settings']     - settings of app
     * $_POST['app_parent']       - parent of app
     * $_POST['folder']           - uid of folder
     */
    public function addAction()
    {
      $result = -1;
      $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
      if ($this->request->isPost())
      {
        $appName=$this->request->get('app_name');
        $sett=!empty($this->request->get('app_settings'))? $this->request->get('app_settings'):[];
        $application = new Core\App\App('',$appName,[],$sett);
        $_app = new App();
        $result = $_app->add($application, $this->session->get('logged')['User']);
        if($result['result']==1)
  			{
					$dataResult['data']['uid'] = $application->getUid();
          $folderUid = $this->request->get('folder');
          if (!empty($folderUid)) 
          {
            $folder = new Core\App\Folder($folderUid, '');
            $addToFolder = Folder::addApp($folder, $application, $this->session->get('logged')['User']);
            if (!$addToFolder['code'] != 1) 
            {
              $dataResult['errors'] = $addToFolder['errors'];
              $result['result'] = $addToFolder['code'];
            }
          }
  			}else 
        {
  				$dataResult['errors']=$result['error'];
  			}
  			$result=$result['result'];
      }
      $dataResult['result']=$result;
  		echo json_encode($dataResult);
    }
    /**
     * Rename application
     * $_POST['app_name'] - name of app
     * $_POST['app'] 	   - uid of app
     */
    public function renameAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $application = new Core\App\App($this->request->get('app'), $this->request->get('app_name'));
            $_app = new App();
            $result = $_app->rename($application, $this->session->get('logged')['User']);
            $dataResult['data']=$application;
            if($result['result']!=1)
      			{
      					$dataResult['errors']=$result['error'];
      			}
      			$result=$result['result'];
        }
        $dataResult['result']=$result;
    		echo json_encode($dataResult);
    }
    /**
     * Remove application
     * $_POST['app'] 	- uid of app
     */
    public function removeAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $application = new Core\App\App($this->request->get('app'), '');
            $_app = new App();
            $result = $_app->remove([$application], $this->session->get('logged')['User']);
          //  $dataResult['data']=$result['data'];
            if($result['result']!=1)
      			{
      					$dataResult['errors']=$result['error'];
      			}
      			$result=$result['result'];
        }
        $dataResult['result']=$result;
    		echo json_encode($dataResult);
    }    
    /**
     * Edit application
     * $_POST['app']  - uid of app
     * $_POST['app_settings']  - sett of app
     */
    public function editSettingsAction()
    {      
      $result = -1;
      $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
      if ($this->request->isPost()) 
      {
        $sett=!empty($this->request->get('app_settings'))? $this->request->get('app_settings'):[];
        if(isset($this->request->getUploadedFiles()[0]))
        {
          $file=$this->request->getUploadedFiles()[0];
          $uuidd=\Dzeta\Core\Value\File::GenerateUIDFile();
          if (!is_null($uuidd)) 
          {
            $filetype=$file->getRealType();
            $realName=$file->getName();
            $size=$file->getSize();
            $filePath=BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuidd.\Dzeta\Core\Value\File::FILE_EXT;
            $resFile=$file->moveTo($filePath);
            if($resFile)
            {
              $sett['icon_settings']['GenName']=$uuidd;//getPreviewFile
            }
          }else
          {
            unset($sett['icon_settings']);
          }          
        }       
        
        $application = new Core\App\App($this->request->get('app'),'',[],$sett);
        $_app = new App();
        $result = $_app->AppEditSettings($application, $this->session->get('logged')['User']);
        $dataResult['data']=$application;
        if($result['result']!=1)
        {
            $dataResult['errors']=$result['error'];
        }
        $result=$result['result'];
      }
      $dataResult['result']=$result;
      echo json_encode($dataResult);
    }

    public function getSettingsAction()
    {
      $result = -1;
      $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
      if($this->request->isGet())
      {
        $_app = new App();
        $application = new Core\App\App($this->request->get('app'),'',[],[]);
        $result=$_app->GetInfoApp($application);
        $dataResult['data']=$application;
        
        if($result['result']!=1)
        {
          $dataResult['errors']=$result['error'];
        }else{
          $dataResult['data']=[
          'app'=>$application->getUid(),
          'name'=>$application->getName(),
          'app_settings'=>$application->getSettings()
          ];
        }
        $result=$result['result'];
      }
      $dataResult['result']=$result;
      echo json_encode($dataResult);
    }
    /**
     * Get viewd for application
     * $_POST['app'] 	- uid of app
     */
    public function getViewsAction() {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

        if ($this->request->isGet()) {
            $uid=$this->request->get('app');
            $view_uid=$this->request->get('view');


            $_app = new App();
            $_view = new View();
            $application = new Core\App\App($uid, '');
            $result=1;
            $resData=[];

            if(empty($view_uid)) {
                $data = $_app->getViews($application);
                foreach ($data as $key => $value) {
                    $resData[]=['Name'=>$value->getName(),'View'=>$value->getUid(), 'isFirstRow' => 1, 'HasViews' => $value->getHasViews(), 'settings' => $value->getSettings()];
                }
                $dataResult['data']=$resData;
            }
            else {
                $app = new Core\App\App($view_uid, '', []);
                $result_info = $_app->GetInfoApp($app);
                $dataResult['data']= [["Name" => "tmp", 'View' => $view_uid, 'isFirstRow' => 0, "HasViews" => $app->getHasViews(), 'settings' => $app->getSettings()]];
            }

            if($this->session->has('logged')) {
                $UserID = $this->session->get('logged')['User']->getUid();
            }
            else if($this->config->dzeta->GuestUID){
                $UserID = $this->config->dzeta->GuestUID;
            }
            else {
                $this->response->redirect('login');
                return;
            }

            $FSettings = $_app->GenerateFSettings($UserID, $uid);

            $data_app["FSettings"] = $FSettings;
        }
        $dataResult['result']=$result;

        echo json_encode(["data_views"=>$dataResult, "data_app"=>$data_app, "result" => 1]);
    }




    /**
     * Get attribute for view
     * $_POST['uid'] 	- uid view
     */
    public function getAttributeViewAction()
    {
        $newArr=[];
        $result=-1;
        if ($this->request->isPost()) {
            $result=1;
            $view=new Core\App\View($this->request->get('uid'), '', []);
            $_view=new View();

            $UserID = null;
            if($this->session->has('logged')) {
                $UserID = $this->session->get('logged')['User']->getUid();

            }
            else if($this->config->dzeta->GuestUID){
                $UserID = $this->config->dzeta->GuestUID;
            }
            else {
                $this->response->redirect('login');
                return;
            }

            $filter = null;
            if(empty(!$this->request->get('attr_filter'))) {
                ViewHelper::parseFilterTable($this->request->get('attr_filter'), $filter);
            }
            $view->setDopSett('filter', $filter);
            $view->setDopSett('isNewMode', $this->request->get('isNewMode'));
            $view->setDopSett('ParentView', $this->request->get('ParentView'));
            $view->setDopSett('SelectObjs', $this->request->get('SelectObjs'));
            $view->setDopSett('ParentObj', $this->request->get('ParentObj'));
            if(!empty($this->request->get('ForView'))) {
                $view->setDopSett('ForView', $this->request->get('ForView') );
            }

            $data_test = [];

            $r = $_view->getAttributes($view, $UserID, $data_test);
            foreach ($r as  $value) {
              $calcUid=null;    
               if(!is_null($value->getDopSett('calc_color'))) 
               {  
                $calcUid=$value->getDopSett('calc_color')->getUid(); 
                }

                $tA=[
                    'attr' => $value, //Тут мы выводим все свойства атрибута
                      'uid'=>$value->getUid(),
                      'name'=>$value->getName(),
                      'datatype'=>$value->getDatatype(),
                      'isArray'=>(int)$value->isArray(),
                      "hide"=>$value->getDopSett('Hide'),
                      'require'=>$value->getRequare(),
                      'LinkedClass'=>$value->getLinkType(),
                      'key'=>$value->isKey(),
                      'view'=>$value->getDopSett('view'),
                      'calc_color'=>$calcUid,
                      'class'=>[
                          'uid'=>$value->getParent()->getUid(),
                          'name'=>$value->getParent()->getName(),
                          'parentUID'=>$value->getParent()->getParent()->getUid()
                      ]
                    ];
                    //если вдруг ключ надо всегда
                    /*
                    $tA['Filter']=!empty($value->getDopSett('Filter'))?$value->getDopSett('Filter'):[];
                    */
                if(!empty($value->getDopSett('Filter')))
                {
                  $tA['Filter']=$value->getDopSett('Filter');
                  foreach ( $tA['Filter'] as $keytaFilter => $valueTAFilter) {
                      if($valueTAFilter['op']=='IS NULL')
                      {
                        $tA['Filter'][$keytaFilter]['op']='=';
                        $tA['Filter'][$keytaFilter]['v']='NULL';
                      }elseif($valueTAFilter['op']=='IS NOT NULL'){
                        $tA['Filter'][$keytaFilter]['op']='!=';
                        $tA['Filter'][$keytaFilter]['v']='NULL';
                      }
                  }
                }
                $newArr[]=$tA;

            }
        }
        echo json_encode(['result' => $result, 'data' => $newArr, 'test' => $data_test]);
    }


    /**
     * Add attributes for view
     * $_POST['uid'] 	  - uid view
     * $_POST['attr'] 	- [] attr
     */
    public function attrAddAction()
    {
        $newArr=[];
        $result=-1;
        if ($this->request->isPost()) {
            $result=1;
            $view=new Core\App\View($this->request->get('uid'), '', []);
            $_view=new View();
            $attr=$this->request->get('attr');
            $attrNew=[];
            foreach ($attr as $key => $value) {
                $tmpArr=[];
                if (!empty($value['uid'])) {
                    $tmpArr['ID']=$value['uid'];
                }
                $tmpArr['TypeOfData']=\Dzeta\Core\Type\DataType::getIndexFromType($value['TypeOfData']);
                $tmpArr['Name']=$value['name'];
                $tmpArr['Class']=$value['class']['uid'];
                $tmpArr['Hide']=empty($value['hide'])? 0:$value['hide'];
                if(!empty($value['calc_color']))
                {
                  $tmpArr['CalcForColoring']=$value['calc_color'];
                }
                if(!empty($value['Filter']))
                {
                    $tmpF=[];
                    foreach ($value['Filter'] as  $valF)
                    {
                      if($valF['v']=='NULL')
                      {
                        if($valF['op']=='=')
                        {
                          $tmpF[]=[
                          'op'=>'IS NULL',
                         // 'v' =>''
                          ];
                        }elseif($valF['op']=='!='){
                           $tmpF[]=[
                          'op'=>'IS NOT NULL',
                         // 'v' =>''
                          ];
                        }
                      }else{
                         $tmpF[]=[
                          'op'=>$valF['op'],
                          'v' =>$valF['v']
                        ];
                      }                       
                    }
                    $tmpArr['Filter']=$tmpF;
                }
                if(!empty($value['view']))
                {
                    $tmpArr['ParentViews']=[$value['view']];
                }
                $attrNew[]=$tmpArr;
            }
            $r=$_view->addAttributes($view, json_encode($attrNew), $this->session->get('logged')['User']);
        }
        echo json_encode(['result' => $result, 'data' => $newArr]);
    }

    /**
     * Add class for view
     * $_POST['uidApp'] 	  - uid app
     * $_POST['uid'] 	- uid view
     * $_POST['classesLink'] 	- [] Class link
     */
    public function addViewClassAction()
    {
        $newArr=[];
        $result=-1;
        if ($this->request->isPost()) {
            $result=1;
            $application = new Core\App\App($this->request->get('uidApp'), '');
            $view=new Core\App\View($this->request->get('uid'), '', []);
            $view->setParentApp($application);
            $_view=new View();
            $clasesLink = !empty($this->request->get('classesLink')) ? $this->request->get('classesLink') : [];
            foreach ($clasesLink as $key => $value) {
                $mainClass = new Core\Type\Type($value['MainClass'], '', false);
                $class =new Core\Type\Type($value['Class'], '', false);
                $settingsLink=[];
                if(!empty($value['Settings']))
                {
                  if(isset($value['Settings']['single_mode_attr']))
                  {
                      $settingsLink['single_mode_attr']=$value['Settings']['single_mode_attr'];
                  }
                  if(isset($value['Settings']['single_mode_op']))
                  {
                      $settingsLink['single_mode_op']=$value['Settings']['single_mode_op'];
                  }
                }
                

                $view->addClassesLink($mainClass, $class,$settingsLink);
            }
            $result=$_view->addViewClass($view, $this->session->get('logged')['User']);
        }
        echo json_encode(['result' => $result, 'data' => $newArr]);
    }

    /**
     * Get class for view
     * $_POST['uid'] 	- uid view
     */
    public function getViewClassAction()
    {
        $newArr=[];
        $result=-1;
        if ($this->request->isPost()) {
            $result=1;
            $view=new Core\App\View($this->request->get('uid'), '', []);
            $_view=new View();
            $result=$_view->getViewClass($view);
            foreach ($view->getClassesLink() as $key => $value) {


                $newArr[] = [
                    'Class'=>$value->getDependentClass()->getUid(),
                    'MainClass'=>$value->getMainClass()->getUid(),
                    'ClassName'=>$value->getDependentClass()->getName(),
                    'MainClassName'=>$value->getMainClass()->getName(),
                    'Settings'=>empty($value->getSettings())? []:json_decode($value->getSettings()),

                ];

                /*
                $newArr[]=[
                    'Class'=>$value['Class']->getUid(),
                    'ClassName'=>$value['Class']->getName(),
                    'MainClass'=>$value['MainClass']->getUid(),
                    'MainClassName'=>$value['MainClass']->getName(),
                    'Settings'=>empty($value['Settings'])? []:json_decode($value['Settings'])
                ];
                */
            }
        }
        echo json_encode(['result' => $result, 'data' => $newArr]);
    }

    //addAttrrOrder
    public function addViewAttrOrderAction()
    {
        $newArr=[];
        $result=-1;
        if ($this->request->isPost()) {
            $result=1;
            $view=new Core\App\View($this->request->get('uid'), '', []);

            $_view=new View();
            $clasesLink = !empty($this->request->get('orderParams')) ? $this->request->get('orderParams') : [];
            $testArr=[];
            foreach ($clasesLink as $key => $value) {
              $testArr[]=['ID'=>$value,'Index'=>$key+1];
            }
            $result=$_view->addAttrrOrder($view,json_encode($testArr),$this->session->get('logged')['User']);
        }
        echo json_encode(['result' => $result, 'data' => $newArr]);
    }


    //Save Permission
    //App_access_add_forGroup
    public function addAccessForAppAction()
    {
        $result = -1;
        if ($this->request->isPost()) {
            $result = 1;

            $_view = new App();

            $data_result = $_view -> App_access_add_forGroup([
                'App'    => $this->request->get('App'),
                'Group'    => $this->request->get('Group'),
                'Settings'    => $this->request->get('Settings'),
                'User'    => $this->session->get('logged')['User']->getUid(),
            ]);


        }
        echo json_encode($data_result);
    }

    public function getInfoAppAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isGet()) {
            $app=new Core\App\App($this->request->get('app'), '', []);
            $_app=new App();
            $result = $_app->GetInfoApp($app);
            $dataResult['data']=$app;
            $dataResult['InfoApp'] = $result["data"];
            if($result['result']!=1)
            {
                $dataResult['errors']=$result['error'];
            }
            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }



    public function getHomeAppAction(){
        $_app = new App();

        $UserID = $this->session->get('logged')['User']->getUid();
        $user_groups = $_app->Object_BaseObjectsForObject($UserID);


       $params = [
          "isHome" => true,
           "UserID" => $this->session->get('logged')['User']->getUid(),
           "GroupIDs" => $user_groups
       ];

       $data_tree = $_app->getAllTreeAppView($params);

       if(isset($data_tree[0]["children"])) {
           foreach ($data_tree[0]["children"] as &$item) {
               $app=new Core\App\App($item["uid"], '', []);
               $result = $_app->GetInfoApp($app);
               $item["Settings"] = isset($result["data"]["Settings"]) ? json_decode($result["data"]["Settings"], true) : [];

               if(isset($item["children"])) {
                   foreach ($item["children"] as &$_item) {
                       $app=new Core\App\App($_item["uid"], '', []);
                       $result = $_app->GetInfoApp($app);
                       $_item["Settings"] = isset($result["data"]["Settings"]) ? json_decode($result["data"]["Settings"], true) : [];
                   }
               }
           }
       }

      echo json_encode($data_tree);
   }

}
