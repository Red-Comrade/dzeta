<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Core\User\UserGroup;
use Dzeta\Models\User;
use Dzeta\Controllers\ControllerBase;

class UserController extends ControllerBase
{
	/**
	 * Change user password
	 * $_POST['uid'] - uid of user
	 *
	 */
	public function changePasswordAction() 
	{
		$result = -1;
		$password = '';
		$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
		if ($this->request->isPost()) 
		{
			$user = new Core\User\User($this->request->get('uid'), '');
			$password=Core\User\User::GeneratePassword();
			$user->setPassword($password);
			$_user = new User();
			$res = $_user->changePassword($user, $this->session->get('logged')['User']);
			if($res['result']==\Dzeta\Library\Db::SUCCESS_CODE)
			{
				$dataResult['data']= $password;
			}else 
			{
				$dataResult['errors']=$result['error'];
			}
			$dataResult['result']= $res['result'];
		}
		echo json_encode($dataResult);
	}

}
