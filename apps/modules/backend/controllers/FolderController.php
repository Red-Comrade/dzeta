<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core\App;
use Dzeta\Models\Folder;
use Dzeta\Controllers\ControllerBase;

class FolderController extends ControllerBase
{
    /**
     * Get folders and their apps
     *
     * $_GET['include_apps'] - return apps of folders
     */
    public function getAllAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isGet()) {
    		$includeApps = $this->request->get('include_apps', null, false);
    		$folders = Folder::getAll();
    		$folders[] = new App\Folder('', 'Root');
    		if ($includeApps) {
    			foreach ($folders as $folder) {
    				Folder::getApps($folder);
    			}
    		}
    		$result['data'] = $folders;
    		$result['code'] = 1;
    	}

    	echo json_encode($result);
    }


    /**
     * Get folder's apps
     *
     * $_GET['uid'] - folder's uid (if empty return apps without folder)
     */
    public function getAppsAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isGet()) {
    		$uid = $this->request->get('uid', null, '');
    		$folder = new App\Folder($uid, 'Root');
			$apps = Folder::getApps($folder);
    		$result['data'] = $apps;
    		$result['code'] = 1;
    	}

    	echo json_encode($result);
    }

    /**
     * Add folder
     *
     * $_POST['name']       - name of folder
     * $_POST['settings']	- settings of folder
     */
    public function addAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isPost()) {
            $settings = $this->request->get('settings', null, []);
            $settings = empty($settings) ? [] : $settings;
    		$folder = new App\Folder('', $this->request->get('name'), $settings);
    		$add = Folder::add($folder, $this->session->get('logged')['User']);
    		if ($add['code'] == 1) {
    			$result['data']['uid'] = $folder->getUid();
    		} else {
    			$result['errors'] = $add['errors'];
    		}
    		$result['code'] = $add['code'];
    	}

    	echo json_encode($result);
    }

    /**
     * Rename folder
     *
     * $_POST['uid']	- uid of folder
     * $_POST['name']	- name of folder
     */
    public function renameAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isPost()) {
    		$folder = new App\Folder($this->request->get('uid'), $this->request->get('name'));
    		$rename = Folder::rename($folder, $this->session->get('logged')['User']);
    		if ($rename['code'] != 1) {
    			$result['errors'] = $rename['errors'];
    		}
    		$result['code'] = $rename['code'];
    	}

    	echo json_encode($result);
    }

    /**
     * Delete folder
     *
     * $_POST['uid'] - uid of folder
     */
    public function deleteAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isPost()) {
    		$folder = new App\Folder($this->request->get('uid'), '');
    		$delete = Folder::remove($folder, $this->session->get('logged')['User']);
    		if ($delete['code'] != 1) {
    			$result['errors'] = $delete['errors'];
    		}
    		$result['code'] = $delete['code'];
    	}

    	echo json_encode($result);
    }

    /**
     * Update folder's settings
     *
     * $_POST['uid']		- uid of folder
     * $_POST['settings']	- settings of folder
     */
    public function updateSettingsAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isPost()) {
            $settings = $this->request->get('settings', null, []);
            $settings = empty($settings) ? [] : $settings;
    		$folder = new App\Folder($this->request->get('uid'), '', );
    		$update = Folder::updateSettings($folder, $this->session->get('logged')['User']);
    		if ($update['code'] != 1) {
    			$result['errors'] = $update['errors'];
    		}
    		$result['code'] = $update['code'];
    	}

    	echo json_encode($result);
    }

    /**
     * Update order of folders
     *
     * $_POST['folders'] => [['uid', 'index']] - array of folders' uids and indexes
     */
    public function updateOrderAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isPost()) {
    		$folders = [];
    		$order = $this->request->get('folders', null, []);
    		foreach ($order as $folder) {
    			$folders[$folder['index']] = new App\Folder($folder['uid'], '');
    		}
    		$update = Folder::updateOrder($folders, $this->session->get('logged')['User']);
    		if ($update['code'] != 1) {
    			$result['errors'] = $update['errors'];
    		}
    		$result['code'] = $update['code'];
    	}

    	echo json_encode($result);
    }

    /**
     * Update order of apps in folder
     *
     * $_POST['folder']						- uid of folder
     * $_POST['apps'] => [['uid', 'index']] - array of apps' uids and indexes
     */
    public function updateAppsOrderAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isPost()) {
    		$apps = [];
    		$order = $this->request->get('apps', null, []);
    		foreach ($order as $app) {
    			$apps[$app['index']] = new App\App($app['uid'], '');
    		}
    		$folder = new App\Folder($this->request->get('folder'), '', [], $apps);
    		$update = Folder::updateAppsOrder($folder, $this->session->get('logged')['User']);
    		if ($update['code'] != 1) {
    			$result['errors'] = $update['errors'];
    		}
    		$result['code'] = $update['code'];
    	}

    	echo json_encode($result);
    }

    /**
     * Add app to folder
     *
     * $_POST['folder']	- uid of folder
     * $_POST['app']	- uid of app
     */
    public function addAppAction() {
    	$result = ['code' => -1, 'errors' => [], 'data' => []];

    	if ($this->request->isPost()) {
    		$folderUid = $this->request->get('folder', null, '');
    		$folder = new App\Folder($folderUid, '');
    		$app = new App\App($this->request->get('app'), '');
    		$add = Folder::addApp($folder, $app, $this->session->get('logged')['User']);
    		if ($add['code'] != 1) {
    			$result['errors'] = $add['errors'];
    		}
    		$result['code'] = $add['code'];
    	}

    	echo json_encode($result);
    }
}