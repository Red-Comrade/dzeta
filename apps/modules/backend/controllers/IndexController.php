<?php
declare(strict_types=1);

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Models\App;
use Dzeta\Type\Type;
use Dzeta\Controllers\ControllerBase;
use Dzeta\Core\Instance;
use Dzeta\Models\Obj;

class IndexController extends ControllerBase
{

    public function getMainInfoAction()
    {
        //name
        $user = $this->session->get('logged')['User'];
        $nameUser = $user->getName();
        $userUID = $user->getUid();
        $data = [];
        $data['module'] = array_column($user->getPermissionSession('module'), 'name');
        $data['nameUser'] = $nameUser;
        $data['userUID'] = $userUID;

        $_app = new App();

        $user_groups = $_app->Object_BaseObjectsForObject($userUID);


        //Тут мы получаем данные для пользователя как объекта
        $arrObj = [];
        $tmp = new Instance($userUID, '');
        $arrObj[] = $tmp;
        $_obj = new Obj();
        $data_obj = $_obj->getAllValues($arrObj);
        $UserObjData = $data_obj[$userUID];


        echo json_encode(['result' => 1, 'nameUser' => $nameUser, 'UserGroups' => $user_groups, 'data' => $data, 'UserObjData' =>$UserObjData]);
    }

}
