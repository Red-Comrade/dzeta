<?php
declare(strict_types=1);

namespace Dzeta\Modules\Backend\Controllers;

use Phalcon\Config\Adapter\Json;
use Phalcon\Filter;

use Dzeta\Core;
use Dzeta\Core\User\User;
use Dzeta\Core\Instance;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\Value\DateTimeV;
use Dzeta\Models\Obj;
use Dzeta\Models\Attr;
use Dzeta\Models\Import;
use Dzeta\Controllers\ControllerBase;

class KeysSoController extends ControllerBase
{
	/**
	 * Return website tops from keys.so
	 * $_POST['domain'] - website domain
	 */
	public function topsAction() {
		$result = ['result' => -1, 'data' => []];
		if ($this->request->isPost()) {
			$domain = $this->request->get('domain');
			if (!empty($domain)) {
				$domain = \KeysSoLib::parseDomain($domain);
				$config = new Json(BASE_PATH . '/config/KeysSoConfig.json');
				if ($config->has('api_key') && $config->has('classes')) {
					$classes = $config->get('classes');
					$websiteClass = new Core\Type\Type($classes['website']['class'], '');
			        $_obj = new Obj();
			        $parents = [];
					$website = $_obj->searchObj($websiteClass, "[{$classes['website']['attrs']['url']}]=#{$domain}#", $parents, 1);
					$dateClass = new Core\Type\Type($classes['date']['class'], '');
					$dateAttr = new Attribute($classes['date']['attrs']['date'], '', 0, false, false);
					$root = new Instance(Instance::INSTANCE_ROOT, '');
					$runQuery = true;
					if (!empty($website['data'])) {
						$website = $website['data'][0];
						$parents = [$website];
						$date = $_obj->searchObj($dateClass, "MAX[{$classes['date']['attrs']['date']}]", $parents, 1);
						if (!empty($date['data'])) {
							$date = $date['data'][0];
							$date->setAttributes([$dateAttr]);
							$dateData = $_obj->getAllValues([$date]);
							$dateAttr = $date->getAttribute($dateAttr->getUid());
							$dateAttrValue = $dateAttr->getValue();
							if (!empty($dateAttrValue)) {
								$dateValue = DateTime::createFromFormat(DateTimeV::DATE_FORMAT, $dateAttrValue->getValue());
								$dateValue->setTime(0, 0, 0, 0);
								$nowDateValue = new DateTime();
								$nowDateValue->setTime(0, 0, 0, 0);
								if ($dateValue == $nowDateValue) {
									$runQuery = false;
								}
							}
						}
					} else {
						$website = new Instance('', '');
						$website->setParent($root)->setType($websiteClass);
					}
					if ($runQuery) {
						$urlAttr = new Attribute($classes['website']['attrs']['url'], '', 0, false, false);
						$websiteTop10YandexAttr = new Attribute($classes['website']['attrs']['top10y'], '', 0, false, false);
						$websiteTop50YandexAttr = new Attribute($classes['website']['attrs']['top50y'], '', 0, false, false);
						$websiteTop10GoogleAttr = new Attribute($classes['website']['attrs']['top10g'], '', 0, false, false);
						$websiteTop50GoogleAttr = new Attribute($classes['website']['attrs']['top50g'], '', 0, false, false);
						$_attr = new Attr();
						$attrs = [
							$urlAttr,
							$websiteTop10YandexAttr,
							$websiteTop50YandexAttr,
							$websiteTop10GoogleAttr,
							$websiteTop50GoogleAttr,
						];
						$_attr->getAttributesByList($attrs);

						$keys = new \KeysSoLib($config->get('api_key'), BASE_PATH . $config->get('log'));
						$tops = $keys->tops($domain);

						$urlAttr->setValue(['value' => $domain]);
						$websiteTop50YandexAttr->setValue([
							'value' => empty($tops['yandex']['top50']) ? null : $tops['yandex']['top50']
						]);
						$websiteTop10YandexAttr->setValue([
							'value' => empty($tops['yandex']['top10']) ? null : $tops['yandex']['top10']
						]);
						$websiteTop50GoogleAttr->setValue([
							'value' => empty($tops['google']['top50']) ? null : $tops['google']['top50']
						]);
						$websiteTop10GoogleAttr->setValue([
							'value' => empty($tops['google']['top10']) ? null : $tops['google']['top10']
						]);
						$website->setAttributes([
							$urlAttr,
							$websiteTop50YandexAttr,
							$websiteTop10YandexAttr,
							$websiteTop50GoogleAttr,
							$websiteTop10GoogleAttr
						]);
						$systemUser = new User(User::SYSTEM_USER_UID, '');
						if (empty($website->getUid())) {
				            $results = $_obj->checkUniqueAndAddWithValues($website, Instance::ADDED_BY_EXTERNAL_SOURCE, $systemUser);
						} else {
			                $_obj->checkUniqueAndUpdateValues($website, $systemUser);
						}
					}
					$result['result'] = 1;
					$result['data']['website'] = !empty($website) ? $website->getUid() : null;
				}
			}
		}
		echo json_encode($result);
	}

	/**
	 * Query website info from keys.so
	 * $_POST['domain'] - website domain
	 */
	public function queryAction() {
		$result = ['result' => -1, 'data' => []];
		if ($this->request->isPost()) {
			$domain = $this->request->get('domain');
			if (!empty($domain)) {
				$domain = \KeysSoLib::parseDomain($domain);
				$config = new Json(BASE_PATH . '/config/KeysSoConfig.json');
				if ($config->has('api_key') && $config->has('classes')) {
					$classes = $config->get('classes');
					$websiteClass = new Core\Type\Type($classes['website']['class'], '');
			        $_obj = new Obj();
			        $parents = [];
					$website = $_obj->searchObj($websiteClass, "[{$classes['website']['attrs']['url']}]=#{$domain}#", $parents, 1);
					$dateClass = new Core\Type\Type($classes['date']['class'], '');
					$dateAttr = new Attribute($classes['date']['attrs']['date'], '', 0, false, false);
					$root = new Instance(Instance::INSTANCE_ROOT, '');
					$runQuery = true;
					if (!empty($website['data'])) {
						$website = $website['data'][0];
						$parents = [$website];
						$_obj->getAllValues($parents);
						$date = $_obj->searchObj($dateClass, "MAX[{$classes['date']['attrs']['date']}]", $parents, 1);
						if (!empty($date['data'])) {
							$date = $date['data'][0];
							$date->setAttributes([$dateAttr]);
							$dateData = $_obj->getAllValues([$date]);
							$dateAttr = $date->getAttribute($dateAttr->getUid());
							$dateAttrValue = $dateAttr->getValue();
							if (!empty($dateAttrValue)) {
								$dateValue = DateTime::createFromFormat(DateTimeV::DATE_FORMAT, $dateAttrValue->getValue());
								$dateValue->setTime(0, 0, 0, 0);
								$nowDateValue = new DateTime();
								$nowDateValue->setTime(0, 0, 0, 0);
								if ($dateValue == $nowDateValue) {
									$runQuery = false;
								}
							}
						}
						if ($runQuery) {
							$date = new Instance('', '');
							$date->setParent($website)->setType($dateClass);
						}
					} else {
						$website = new Instance('', '');
						$website->setParent($root)->setType($websiteClass);
						$date = new Instance('', '');
						$date->setParent($website)->setType($dateClass);
					}
					if ($runQuery) {
						$urlAttr = new Attribute($classes['website']['attrs']['url'], '', 0, false, false);
						$queryAttr = new Attribute($classes['query']['attrs']['query'], '', 0, false, false);
						$wordAttr = new Attribute($classes['position']['attrs']['query'], '', 0, false, false);
						$wsYandexAttr = new Attribute($classes['position']['attrs']['wsy'], '', 0, false, false);
						$googleAttr = new Attribute($classes['position']['attrs']['google'], '', 0, false, false);
						$yandexAttr = new Attribute($classes['position']['attrs']['yandex'], '', 0, false, false);
						$urlGoogleAttr = new Attribute($classes['position']['attrs']['urlg'], '', 0, false, false);
						$urlYandexAttr = new Attribute($classes['position']['attrs']['urly'], '', 0, false, false);
						$dateTop10YandexAttr = new Attribute($classes['date']['attrs']['top10y'], '', 0, false, false);
						$dateTop50YandexAttr = new Attribute($classes['date']['attrs']['top50y'], '', 0, false, false);
						$dateTop10GoogleAttr = new Attribute($classes['date']['attrs']['top10g'], '', 0, false, false);
						$dateTop50GoogleAttr = new Attribute($classes['date']['attrs']['top50g'], '', 0, false, false);

						$websiteTop10YandexAttr = $website->getAttribute($classes['website']['attrs']['top10y']);
						$websiteTop10YandexAttrValue = $websiteTop10YandexAttr->getValue();
						$websiteTop50YandexAttr = $website->getAttribute($classes['website']['attrs']['top50y']);
						$websiteTop50YandexAttrValue = $websiteTop50YandexAttr->getValue();
						$websiteTop10GoogleAttr = $website->getAttribute($classes['website']['attrs']['top10g']);
						$websiteTop10GoogleAttrValue = $websiteTop10GoogleAttr->getValue();
						$websiteTop50GoogleAttr = $website->getAttribute($classes['website']['attrs']['top50g']);
						$websiteTop50GoogleAttrValue = $websiteTop50GoogleAttr->getValue();

						$_attr = new Attr();
						$attrs = [
							$urlAttr,
							$queryAttr,
							$dateAttr,
							$wordAttr,
							$wsYandexAttr,
							$googleAttr,
							$yandexAttr,
							$urlGoogleAttr,
							$urlYandexAttr,
							$dateTop10YandexAttr,
							$dateTop50YandexAttr,
							$dateTop10GoogleAttr,
							$dateTop50GoogleAttr,
						];
						$_attr->getAttributesByList($attrs);
						$urlAttr->setValue(['value' => $domain]);
						$website->setAttributes([$urlAttr]);
			            $dateAttr->setValue(['value' => date('d/m/Y H:i:s')]);
						$date->setAttributes([$dateAttr]);

						$keys = new \KeysSoLib($config->get('api_key'), BASE_PATH . $config->get('log'));
						$data = $keys->searchStat($domain);
						// $tops = $keys->countTops($data);

						$systemUser = new User(User::SYSTEM_USER_UID, '');

						// $websiteTop50YandexAttr->setValue([
						// 	'value' => empty($tops['yandex']['top50']) ? null : $tops['yandex']['top50']
						// ]);
						// $websiteTop10YandexAttr->setValue([
						// 	'value' => empty($tops['yandex']['top10']) ? null : $tops['yandex']['top10']
						// ]);
						// $websiteTop50GoogleAttr->setValue([
						// 	'value' => empty($tops['google']['top50']) ? null : $tops['google']['top50']
						// ]);
						// $websiteTop10GoogleAttr->setValue([
						// 	'value' => empty($tops['google']['top10']) ? null : $tops['google']['top10']
						// ]);
						// $website->setAttributes([
						// 	$urlAttr,
						// 	$websiteTop50YandexAttr,
						// 	$websiteTop10YandexAttr,
						// 	$websiteTop50GoogleAttr,
						// 	$websiteTop10GoogleAttr
						// ]);
						// if (empty($website->getUid())) {
						//		$results = $_obj->checkUniqueAndAddWithValues($website, Instance::ADDED_BY_EXTERNAL_SOURCE, $systemUser);
						// } else {
						//	$_obj->checkUniqueAndUpdateValues($website, $systemUser);
						// }

						$dateTop50YandexAttr->setValue([
							'value' => empty($websiteTop50YandexAttrValue) ?  null : $websiteTop50YandexAttrValue->getValue()
						]);
						$dateTop10YandexAttr->setValue([
							'value' => empty($websiteTop10YandexAttrValue) ? null : $websiteTop10YandexAttrValue->getValue()
						]);
						$dateTop50GoogleAttr->setValue([
							'value' => empty($websiteTop50GoogleAttrValue) ? null : $websiteTop50GoogleAttrValue->getValue()
						]);
						$dateTop10GoogleAttr->setValue([
							'value' => empty($websiteTop10GoogleAttrValue) ? null : $websiteTop10GoogleAttrValue->getValue()
						]);
		                $date->setAttributes([
		                	$dateAttr,
		                	$dateTop50YandexAttr,
		                	$dateTop10YandexAttr,
		                	$dateTop50GoogleAttr,
		                	$dateTop10GoogleAttr,
		                ]);
		                if (empty($date->getUid())) {
				            $results = $_obj->checkUniqueAndAddWithValues($date, Instance::ADDED_BY_EXTERNAL_SOURCE, $systemUser);
		                } else {
			                $_obj->checkUniqueAndUpdateValues($date, $systemUser);
			            }

						$first = true;
						$positionBlock = null;
						$positions = [];
						$positionIndex = 1;
						$positionClass = new Core\Type\Type($classes['position']['class'], '');
						$_import = new Import();
						foreach ($data as $keyword) {
							$position = new Instance('', '');
							$position->setType($positionClass)->setParent($date);
							$wordAttr->setValue(['value' => $keyword['word']]);
							$wsYandexAttr->setValue([
								'value' => empty($keyword['yandex']['ws']) ? null : $keyword['yandex']['ws']
							]);
							$googleAttr->setValue([
								'value' => empty($keyword['google']['pos']) ? null : $keyword['google']['pos']
							]);
							$yandexAttr->setValue([
								'value' => empty($keyword['yandex']['pos']) ? null : $keyword['yandex']['pos']
							]);
							$urlGoogleAttr->setValue([
								'value' => empty($keyword['google']['url']) ? null : 'https://' . $domain . $keyword['google']['url']
							]);
							$urlYandexAttr->setValue([
								'value' => empty($keyword['yandex']['url']) ? null : 'https://' . $domain . $keyword['yandex']['url']
							]);
							$position->setAttributes([
								clone $wordAttr,
								clone $wsYandexAttr,
								clone $googleAttr,
								clone $yandexAttr,
								clone $urlGoogleAttr,
								clone $urlYandexAttr,
							]);
							$position->setOption('importIndex', $positionIndex);
							$position->setOption('importExcelRow', $positionIndex);
							$positionIndex++;

							$positions[] = $position;

							if (count($positions) == 10000) {
								$positionBlock = $_import->addCache($positions, $positionBlock, $first, $systemUser);
								$first = false;
								$positions = [];
							}

						}
						if (!empty($positions)) {
							$positionBlock = $_import->addCache($positions, $positionBlock, $first, $systemUser);
							$positions = [];
						}
				 		$results = $_import->addShort($positionBlock, null, Import::DO_NOTHING_ACTION, Import::CHECK_ERRORS_AND_ADD, $systemUser);
					}
					$result['result'] = 1;
					if ($runQuery) {
						$result['data']['block'] = $positionBlock;
					}
					$result['data']['website'] = !empty($website) ? $website->getUid() : null;
				}
			}
		}
		echo json_encode($result);
	}

	public function fillAction() {
		$result = ['result' => -1, 'data' => []];
		ignore_user_abort(true);
		if ($this->request->isPost()) {
			$block = $this->request->get('block', Filter::FILTER_INT, null);
			if (!empty($block)) {
				$_import = new Import();
				$_import->fillObjectsKeysSo($block);
				$_import->clearCache($block);
				$result['result'] = 1;
			}
		}
		echo json_encode($result);
	}
}