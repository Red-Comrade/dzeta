<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\Type\AttributeGroup;
use Dzeta\Models\Attr;
use Dzeta\Controllers\ControllerBase;

class AttrController extends ControllerBase
{
	/**
	 * Add attirbute
	 * $_POST['name'] - name of attribute
	 * $_POST['datatype'] - datatype of attribute
	 * $_POST['type_parent'] - parent type of attribute
	 * $_POST['TextType'] - text, ckeditor, color
	 * $_POST['group'] - group of attribute
	 * $_POST['array'] - array flag of attribute
	 * $_POST['unique'] - key flag of attribute
	 * $_POST['require'] - requare flag of attribute
	 * $_POST['type_link'] - linkType of attribute
	 * $_POST['calc_triggers']['manual'] - uid calc manual triggers
	 * $_POST['calc'] - uid calc of attribute
	 */
	public function addAction() {
		$result = -1;
		$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

		if ($this->request->isPost()) {
			$attr = new Attribute('',
			 	$this->request->get('name'),
				DataType::getIndexFromType($this->request->get('datatype')),
				(bool)$this->request->get('unique'),
				(bool)$this->request->get('array'));

            $attr->setTextType($this->request->get('TextType'));


            $attr->setParent(new Core\Type\Type($this->request->get('type_parent'), ''))->setGroup(new AttributeGroup($this->request->get('group'), ''));
			if(!empty($this->request->get('calc')))
			{
				$attr->setCalc(new Core\Calculation($this->request->get('calc'), ''));
			}
			if(!empty($this->request->get('type_link')))
			{
				$attr->setLinkType(new Core\Type\Type($this->request->get('type_link'), ''));
			}
			if(!empty($this->request->get('ShowCameraModule')))
			{
				$attr->setShowCamera(1);
			}
			if(!empty($this->request->get('calc_triggers')))
			{
				$calc_triggers=$this->request->get('calc_triggers');
				if(!empty($calc_triggers['manual']))
				{
						$attr->setCalcTrig(new Core\Calculation($calc_triggers['manual'], ''),"manual");
				}
			}
			if(!empty($this->request->get('require')))
			{
				$attr->setRequare(true);
			}
			$_attr = new Attr();
			$result = $_attr->add($attr, $this->session->get('logged')['User']);
			if($result['result']==1)
			{
					$dataResult['data']['uid'] = $attr->getUid();
			}else {
					$dataResult['errors']=$result['error'];
			}
			$result=$result['result'];
		}
		$dataResult['result']=$result;
		echo json_encode($dataResult);
	}

	/**
	 * Update attribute
	 * $_POST['attr'] - uid of attribute
	 * $_POST['name'] - uid of attribute
	 * $_POST['type_parent'] - parent type of attribute
     * $_POST['TextType'] - text, ckeditor, color
     * $_POST['array'] - array flag of attribute
	 * $_POST['unique'] - unique flag of attribute
	 * $_POST['require'] - requare flag of attribute
	 * $_POST['type_link'] - linkType of attribute
	 * $_POST['calc'] - uid calc of attribute
	 * $_POST['calc_triggers']['manual'] - uid calc manual triggers
	 */
	public function updateAction() {
		$result = -1;
		$data = [];
		$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {

            $attr = new Attribute($this->request->get('attr'),
                $this->request->get('name'),
                0,
                (bool)$this->request->get('unique'),
                (bool)$this->request->get('array'));
            $attr->setParent(new Core\Type\Type($this->request->get('type_parent'), ''));
            $attr->setTextType($this->request->get('TextType'));

            if (!empty($this->request->get('calc'))) {
                $attr->setCalc(new Core\Calculation($this->request->get('calc'), ''));
            }
            if (!empty($this->request->get('calc_triggers'))) {
                $calc_triggers = $this->request->get('calc_triggers');
                if (!empty($calc_triggers['manual'])) {
                    $attr->setCalcTrig(new Core\Calculation($calc_triggers['manual'], ''), "manual");
                }
            }

            if (!empty($this->request->get('type_link'))) {
                $attr->setLinkType(new Core\Type\Type($this->request->get('type_link'), ''));
            }
            if(!empty($this->request->get('ShowCameraModule')))
			{
				$attr->setShowCamera(1);
			}
            if (!empty($this->request->get('require'))) {
                $attr->setRequare(true);
            }
            $_attr = new Attr();
            $result = $_attr->edit($attr, $this->session->get('logged')['User']);
            if ($result['result'] == 1) {
                $dataResult['data']['uid'] = $attr->getUid();
            } else {
                $dataResult['errors'] = $result['error'];
            }
            $result = $result['result'];
        }
        $dataResult['result'] = $result;
        echo json_encode($dataResult);
	}

	/**
	 * Delete attributes
	 * $_POST['attr'] - attribut
	 * $_POST['type'] - type of attribut
	 */
	public function deleteAction() {

		$result = -1;
		$dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

		if ($this->request->isPost()) {
			$attr = new Attribute($this->request->get('attr'),'',1,false,false);
			$attr->setParent(new Core\Type\Type($this->request->get('type'), ''));
			$_attr = new Attr();
			$result = $_attr->remove($attr, $this->session->get('logged')['User']);
			if($result['result']==1)
			{
					$dataResult['data']['uid'] = $attr->getUid();
			}else {
					$dataResult['errors']=$result['error'];
			}
			$result=$result['result'];
		}
		$dataResult['result']=$result;
		echo json_encode($dataResult);
	}


    public function sortAction() {

        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];

        if ($this->request->isPost()) {
            $_attr = new Attr();

            $result = $_attr->sort($this->request->get('type'), $this->request->get('params'), $this->session->get('logged')['User']);

            if($result['result'] != 1) {
                $dataResult['errors']=$result['error'];
            }

            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }



    public function getAttributeAllAction()
	{
			$newArr=[];
			$result=1;
			$_attr=new Attr();
			$r=$_attr->getAllAtributes();
			foreach ($r as  $value) {
				$newArr[]=[
					'uid'=>$value->getUid(),
					'name'=>$value->getName(),
					'datatype'=>$value->getDatatype(),
					'class'=>[
						'uid'=>$value->getParent()->getUid(),
						'name'=>$value->getParent()->getName()
					]
				];
			}
			echo json_encode(['result' => $result, 'data' => $newArr]);
	}
}
