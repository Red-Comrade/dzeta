<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core\Progress;
use Dzeta\Controllers\ControllerBase;

class ProgressController extends ControllerBase
{
	public function getAction() {
		$progress = 0;
		if ($this->request->isGet() && $this->request->has('uid')) {
			$p = new Progress($this->request->get('uid'));
			$count = $p->getCount();
			$count = $count == 0 ? 1 : $count;
			$current = $p->getCurrent();
			$progress = round($current / $count * 100, 0);
		}
		echo json_encode(['progress' => $progress]);
	}
}