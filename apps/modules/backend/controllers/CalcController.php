<?php
declare(strict_types=1);

namespace Dzeta\Modules\Backend\Controllers;

use DzetaCalcParser\DzetaCalc;
use DzetaCalcParser\Core\Locales as DzetaCalcLocales;
use Dzeta\Core;
use Dzeta\Core\Instance;

use Dzeta\Models\Obj;
use Dzeta\Models\Attr;
use Dzeta\Models\Calculation;
use Dzeta\Helpers\LanguageHelper;

use Dzeta\Controllers\ControllerBase;

class CalcController extends ControllerBase
{
    /**
     * Save calc
     * $_POST['formula'] - calc formula
     * $_POST['class'] -  type
     * $_POST['calcName'] - calc name
     * $_POST['calc'] - calc uid, isset edit:add
     */
    public function saveAction()
    {
        $result = -1;
    	$dataResult = ['result' => -1, 'data' => [], 'errors' => []];
        if ($this->request->isPost()) {
            $formula = $this->request->get('formula');
            $class = $this->request->get('class');
            $name = $this->request->get('calcName');

            $calc = new Core\Calculation('', $name, $formula);
            $calc->setType(new Core\Type\Type($class, ''));
            $_calc = new Calculation();
            $res = [];
            if (empty($this->request->get('calc'))) {
                $res = $_calc->add($calc, $this->session->get('logged')['User']);
                $result = $res['result'];
            } else {
                $calc->setUid($this->request->get('calc'));
                $res = $_calc->edit($calc, $this->session->get('logged')['User']);
                $result = $res['result'];
            }
            if ($result == 1) {
                $dataResult['data']['uid'] = $calc->getUid();

                // $dzeta=new DzetaCalc(['Formula'=>$this->request->get('formula'),'Obj'=>new Core\Instance('', 'test', new Core\Type\Type($this->request->get('class'), '')),'noStart'=>true]);
                // $dbAttrGroup=$dzeta->testGrooups();
                $dbAttrGroup = [];
                $_calc->argAttrAdd($dbAttrGroup, $calc, $this->session->get('logged')['User']);
            } else {
                $dataResult['errors'] = $res['error'];
            }
        }
        $dataResult['result'] = $result;
    	echo json_encode($dataResult);
    }

    public function deleteAction()
    {
        $result = ['result' => -1, 'data' => []];
        if ($this->request->isPost()) {
            $uid = $this->request->get('uid');
            if (!empty($uid)) {
                $calc = new Core\Calculation($uid, '', '');
                $result = Calculation::remove($calc, $this->session->get('logged')['User']);
            }
        }
        echo json_encode($result);
    }

    public function getCalcAction()
    {
        $result = ['result' => -1, 'data' => []];
        if ($this->request->isGet()) {
            $calc = new Core\Calculation('', '', '$x = 0');
            DzetaCalcLocales::init(LanguageHelper::getFromCookies());
            $functions = DzetaCalc::getFunctionsCompletions();
            $methods = DzetaCalc::getMethodsCompletions();
            $uid = $this->request->get('uid');
            if (!empty($uid)) {
                $calc = new Core\Calculation($uid, '', '');
                $_calc = new Calculation();
                $_calc->get($calc);
            }
            $result['data'] = [
                'formula' => $calc->getFormula(),
                'name' => $calc->getName(),
                'functions' => $functions,
                'methods' => $methods,
            ];
            $result['result'] = 1;
        }
        echo json_encode($result);
    }

    public function runCalcAction()
    {
        ini_set('memory_limit', '2048M');
        $time = microtime(true);
        $result = ['result' => -1, 'data' => [], 'errors' => []];
        if ($this->request->isPost()) {
            $parent =  new Core\Instance('00000000-0000-0000-0000-000000000000', '');
            $_obj = new Obj();
            $children = $_obj->getChildrenByType($parent, new Core\Type\Type($this->request->get('class'), ''));
            foreach ($children as $key => $value) {
                if ($value->getUid() == 'd2d816b7-a74e-4594-a0b9-22636accde26')
                {
                    $children[0] = $value;
                    break;
                }
            }
            $dzeta = new DzetaCalc([
                'formula' => $this->request->get('formula'),
                'history' => true,
                'object' => isset($children[0]) ? $children[0] : null,
                'user' => $this->session->get('logged')['User'],
                'language' => LanguageHelper::getFromCookies(),
            ]);
            $dzeta->start();
            $result['result'] = 1;
            $result['data']['answer'] = $dzeta->getResults();
            $result['data']['errors'] = $dzeta->getErrors();
        }
        echo json_encode($result);
    }

    public function getResultAction()
    {
        $result = ['result' => -1, 'data' => []];
        if ($this->request->isGet()) {
            $calc = new Core\Calculation($this->request->get('uidCalc'), '', '');
            $_calc = new Calculation();
            $_calc->get($calc);

            $object = new Instance($this->request->get('uidObj'), '');
            $_obj = new Obj();
            $_obj->GetClassForObject($object);
            $dzeta = new DzetaCalc([
                'formula' => $calc->getFormula(),
                'object' => $object,
                'language' => LanguageHelper::getFromCookies(),
            ]);
            $dzeta->start();
            $answer = $dzeta->getResults();
            $result['result'] = 1;
            $result['data'] = array_pop($answer);
        }
        echo json_encode($result);
    }

    public function getRunTriggerAction()
    {
        $result = -1;
        $data = [];
        if ($this->request->isPost())
        {
            $uidCalc = $this->request->get('calc');//юид расчета
            $trigger = $this->request->get('trigger');//тип тригера
            $obj = $this->request->get('obj');
            $type = $this->request->get('type');
            $values = $this->request->get('values');
            $calc = new Core\Calculation($uidCalc, '', '');
            $_calc = new Calculation();
            $_calc->get($calc);
            $obj = new Core\Instance($obj, '');
            $obj->setType(new Core\Type\Type($type, ''));
            $attrs = [];
            foreach ($values as $value) {
                if(!isset($value['value']))
                {
                    continue;
                }
                $isArray = is_array($value['value']);
                $attr = new Core\Type\Attribute($value['attr'], '',  \Dzeta\Core\Type\DataType::getIndexFromType($value['datatype']), (bool)$value['key'], $isArray);
                if ($isArray)
                {
                    $tmpV = [];
                    foreach ($value['value'] as $valA) {
                        $tmpV[] = ['value' => $valA['value']];
                    }
                    $attr->setValue(['value' => $tmpV]);
                } else {
                    $attr->setValue(['value' => $value['value']]);
                }
                $attrs[] = $attr;
            }
            $obj->setAttributes($attrs);
            $dzeta = new DzetaCalc([
                'formula' => $calc->getFormula(),
                'trigger' => ['object' => $obj],
                'language' => LanguageHelper::getFromCookies(),
            ]);
            $dzeta->start();
            $data = $dzeta->getFormData();
            $result = 1;
        }
        echo json_encode(['result' => $result, 'data' => $data]);
    }

    public function runCalcDataRequestAction()
    {
        $result = ['result' => -1, 'data' => []];
        if ($this->request->isPost()) 
        {
            $calc = new Core\Calculation($this->request->get('calc'), '', '');
            $_calc = new Calculation();
            $_calc->get($calc);        
            $dzeta = new DzetaCalc([
                'formula' =>$calc->getFormula(),
                'history' => false,
                'object' => null,
                'user' =>$this->session->get('logged')['User'],
                'arguments' => 
                [
                    ['variable' => '$arg', 'value' =>[0=>json_encode($this->request->get('data'))]]
                ]
            ]);
            $dzeta->start();  
            $resultData=$dzeta->getResult();
            $result['request']=$this->request->get('data');
            if(empty($resultData))
            {
                $result['result']=0;
                $result['err']=$dzeta->getErrors();
            }else
            {
                $result['result']=1;
                $result['data']=$resultData;
            }
        }
        echo json_encode($result);
    }

    public function runCalcListAction()
    {
        $result = ['result' => -1, 'data' => []];
        if ($this->request->isPost()) 
        {
            $curObj=$this->request->get('obj',null,'');
            $type=$this->request->get('type',null,'');
            $dopData=$this->request->get('values');
            $calc = new Core\Calculation($this->request->get('calc'), '', '');
            $_calc = new Calculation();
            $_calc->get($calc);
            $cacheCalcObj=[];
            foreach ($dopData as $key => $value) 
            {                
                $isArray = is_array($value['value']);
                $attr=new Core\Type\Attribute($value['attr'], '',\Dzeta\Core\Type\DataType::getIndexFromType($value['datatype']), (bool)$value['key'], $isArray);
                if($isArray)
                {
                    $tmpV = [];
                    foreach ($value['value'] as $valA) 
                    {
                        $tmpV[] = ['value' => $valA['value']];
                    }
                    $attr->setValue(['value' => $tmpV]);
                }else 
                {
                    $attr->setValue(['value' => $value['value']]);
                }
                $cacheCalcObj[$curObj][]=$attr;
            }
            $cacheCalcObjn=[];
            foreach ($cacheCalcObj as $keyUid => $valueAttr) 
            {
                $objt=new Core\Instance($keyUid, ''); 
                $objt->setAttributes($valueAttr);
                $cacheCalcObjn[]=$objt;
            }
            unset($cacheCalcObj);
            $cObj=new Core\Instance($curObj, '');
            $cObj->setType(new Core\Type\Type($type, ''));            
            $r=$_calc->calcListValue($calc,$cObj,$cacheCalcObjn);
            if($r['result']==1)
            {                
                $result = ['result' => 1, 'data' => $r['data']];
            }
        }
        echo json_encode($result);
    }
}
