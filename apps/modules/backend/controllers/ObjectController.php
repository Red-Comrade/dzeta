<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;

use Dzeta\Core\User\User;
use Dzeta\Core\User\UserGroup;
use Dzeta\Models\Obj;
use Dzeta\Models\Type;
use Dzeta\Models\Files;
use Dzeta\Controllers\ControllerBase;

class ObjectController extends ControllerBase
{
    /**
     * Get object children of specific type
     * $_GET['parent'] - parent object
     * $_GET['type'] - children type
     *
     */
    public function getChildrenByTypeAction()
    {
        $result = -1;
        $data = [];

        if ($this->request->isGet()) {
            $parent = new Instance($this->request->get('parent'), '');
            $type = new Core\Type\Type($this->request->get('type'), '');

            $_obj = new Obj();
            $data = $_obj->getChildrenByType($parent, $type);
            $result = 1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }

    /**
     * Add object
     * $_POST['type'] - type of object
     * $_POST['obj_parent'] - parent of object
     * $_POST['values'] => ['attr', 'key', 'datatype', 'value'] - attribute values of object
     *
     */
    public function addAction()
    {
        $result = -1;
        $data = [];
        $objName = '';
        $errors = [];
        if ($this->request->isPost()) {
            $obj = new Instance('', '');
            $obj->setType(new Core\Type\Type($this->request->get('type'), ''))->setParent(new Instance($this->request->get('parent'), ''));
            $attrs = [];
            foreach ($this->request->get('values', null, []) as $value) {
                $isArray = is_array($value['value']);
                $attr = new Attribute($value['attr'], '', DataType::getIndexFromType($value['datatype']), $value['key'], $isArray);
                if($isArray)
                {
                  $tmpV=[];
                  foreach ($value['value'] as  $valA) {
                    $tmpV[]=['value'=>$valA['value']];
                  }

                  $attr->setValue(['value'=>$tmpV]);
                }else {
                  $attr->setValue(['value'=>$value['value']]);
                }
                $attrs[] = $attr;
            }
            $obj->setAttributes($attrs);

            $_obj = new Obj();
            $results = $_obj->checkUniqueAndAddWithValues($obj, Instance::ADDED_BY_USER, $this->session->get('logged')['User']);
            $objName=$obj->getName();
            if (is_array($results['result'])) {
                $result = 1;
                $_obj->addEventQueueAfterCreateObj($obj,$this->session->get('logged')['User']);
                $data = ['uid' => $obj->getUid(), 'attrs' => $results];
            } else {
                $result = $results['result'];
                $errors = $results['errors'];
            }
        }

        echo json_encode([
            'result' => $result,
            'data' => $data,
            'objName' => $objName,
            'errors' => $errors,
        ]);
    }

    /**
     * Update object attribute values
     * $_POST['obj'] - uid of object
     * $_POST['type'] - class of object
     * $_POST['obj_parent'] - parent of object
     * $_POST['skip_unique_check'] - skip unique check
     * $_POST['values'] => ['attr', 'key', 'value'] - attribute values of object
     *
     */
    public function updateAction()
    {
        $result = -1;
        $data = [];
        $objName = '';
        if ($this->request->isPost()) {
            $obj = new Instance($this->request->get('obj'), '');
            $skipUniqueCheck = $this->request->get('skip_unique_check', null, false);
            $obj->setType(new Core\Type\Type($this->request->get('type'), ''));
            $attrs = [];
            foreach ($this->request->get('values', null, []) as $value) {
                $isArray = is_array($value['value']);
                $attr = new Attribute($value['attr'], '', DataType::getIndexFromType($value['datatype']), $value['key'], $isArray);
                if($isArray)
                {
                  $tmpV=[];
                  foreach ($value['value'] as  $valA) {
                    $tmpV[]=['value'=>$valA['value']];
                  }
                  $attr->setValue(['value'=>$tmpV]);
                }else {
                  $attr->setValue(['value'=>$value['value']]);
                }
                $attrs[] = $attr;
            }
            $obj->setAttributes($attrs);

            $_obj = new Obj();
            if ($skipUniqueCheck) {
                $results = $_obj->addValues([$obj], $this->session->get('logged')['User']);
            } else {
                $results = $_obj->checkUniqueAndUpdateValues($obj, $this->session->get('logged')['User']);
            }
            $objName = $obj->getName();
            if (is_array($results)) {
                $result = 1;
                $data = $results;
            } else {
                $result = $results;
            }
        }

        echo json_encode(['result' => $result, 'data' => $data,'objName'=>$objName]);
    }

    /**
     * Get all values of object
     * $_GET['uid'] - uid of object
     *
     */
    public function getAllValuesAction()
    {
        $result = -1;
        $data = [];

        if ($this->request->isGet()) {
            $arrObj=[];
            $attrs=[];
            if ($this->request->get('attrs')!==null) {
                foreach ($this->request->get('attrs') as $key => $value) {
                    $attrs[]=new \Dzeta\Core\Type\Attribute($value, '', DataType::getIndexFromType(DataType::TYPE_TEXT), false, false);
                }
            }
            if ($this->request->get('obj')!==null) {
                $tmp=new Instance($this->request->get('obj'), '');
                if (!empty($attrs)) {
                    $tmp->setAttributes($attrs);
                }
                $arrObj[]=$tmp;
            } elseif ($this->request->get('objs')!==null) {
                foreach ($this->request->get('objs') as $key => $value) {
                    $tmp=new Instance($value, '');
                    if (!empty($attrs)) {
                        $tmp->setAttributes($attrs);
                    }
                    $arrObj[]=$tmp;
                }
            } else {
                echo json_encode(['result' => -1, 'data' => []]);
                return;
            }

            $_obj = new Obj();

            $data = $_obj->getAllValues($arrObj);

            $result = 1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }


    /**
     * Get all values of object
     * $_GET['uid'] - uid of object
     *
     */
    public function getValuesAndLastUpdateTimeAction()
    {
        $result = -1;
        $data = [];

        if ($this->request->isGet() && !empty($this->request->get('obj'))) {
            $obj = new Instance($this->request->get('obj'), '');;
            $_obj = new Obj();
            $data = $_obj->getValuesAndLastUpdateTime($obj);
            $result = 1;
        }
        else {
            echo json_encode(['result' => -1, 'data' => []]);
            return;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }
    /**
     * Get all values of object
     * $_GET['uid'] - uid of object
     *
     */
    public function getKeysValuesAction()
    {
        $result = -1;
        $data = [];

        if ($this->request->isGet()) {
            $arrObj=[];
            $attrs=[];
            if ($this->request->get('attrs')!==null) {
                foreach ($this->request->get('attrs') as $key => $value) {
                    $attrs[]=new \Dzeta\Core\Type\Attribute($value, '', DataType::getIndexFromType(DataType::TYPE_TEXT), false, false);
                }
            }
            if ($this->request->get('obj')!==null) {
                $tmp=new Instance($this->request->get('obj'), '');
                if (!empty($attrs)) {
                    $tmp->setAttributes($attrs);
                }
                $arrObj[]=$tmp;
            } elseif ($this->request->get('objs')!==null) {
                foreach ($this->request->get('objs') as $key => $value) {
                    $tmp=new Instance($value, '');
                    if (!empty($attrs)) {
                        $tmp->setAttributes($attrs);
                    }
                    $arrObj[]=$tmp;
                }
            } else {
                echo json_encode(['result' => -1, 'data' => []]);
                return;
            }

            $_obj = new Obj();
            $data = $_obj->getAllValues($arrObj);
            $result = 1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }

    /**
     * Get count of objects that will be deleted with passed object
     * $_POST['uid'] - uid of object
     *
     */
    public function getDeleteCountAction()
    {
        $result = -1;
        $data = [];

        if ($this->request->isPost()) {
            $obj = new Instance($this->request->get('uid'), '');

            $_obj = new Obj();
            $data = $_obj->getDeleteCount($obj, $this->session->get('logged')['User']);
            $result = 1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }

    /**
     * Delete object
     * $_POST['uid'] - uid of object
     *
     */
    public function deleteAction()
    {
        $result = -1;
        $data = [];
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) 
        {
            $obj = new Instance($this->request->get('uid'), '');
            $_obj = new Obj();
            $res = $_obj->remove($obj, $this->session->get('logged')['User']);
            $dataResult['result']=$res['result'];
            $dataResult['errors']=$res['error'];
        }

        echo json_encode($dataResult);
    }

    public function addFileAction()
    {
        $FilesArr=[];
        if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $uuidd=\Dzeta\Core\Value\File::GenerateUIDFile();
                if (is_null($uuidd)) {
                    $FilesArr[]=['err'=>true,'value'=>null,'codeErr'=>1];
                    continue;
                }
                $filetype=$file->getRealType();
                $realName=$file->getName();
                $size=$file->getSize();
                $filePath=BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuidd.\Dzeta\Core\Value\File::FILE_EXT;
                $resFile=$file->moveTo($filePath);
                if ($resFile) {
                    $filetype = substr($filetype, 0, strpos($filetype, '/'));
                    $uuiddPrev=\Dzeta\Core\Value\File::GenerateUIDFile();
                    if (is_null($uuiddPrev)) {
                        $FilesArr[]=['err'=>true,'value'=>null,'codeErr'=>3];
                        continue;
                    }
                    if ($filetype=='image') {
                        $image = new \Phalcon\Image\Adapter\Imagick($filePath);
                        $w=$image->getWidth();
                        $h=$image->getHeight();
                        $size=1000;
                        if ($w<=$size&&$h<=$size) {
                            $uuiddPrev=$uuidd;
                        } else {
                            if ($w<=$size&&$h>$size||$h>$w) {
                                $image->resize($w/$h*$size, $size);
                            } elseif ($w>$size&&$h<=$size||$w>$h) {
                                $image->resize($size, $h/$w*$size);
                            }
                            $image->getInternalImInstance()->writeImage(BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uuiddPrev.\Dzeta\Core\Value\File::FILE_EXT);
                        }
                    } else {
                        $uuiddPrev=null;
                    }
                    $FilesArr[]=['err'=>false,'value'=>new \Dzeta\Core\Value\File($uuidd, $realName, $uuiddPrev, $size)];
                } else {
                    $FilesArr[]=['err'=>true,'value'=>null,'codeErr'=>2];
                    continue;
                }
            }
        }

        $data = [];
        if ($this->request->isPost()) {
            $obj = new Instance($this->request->get('uid'), '');
            $obj->setType(new Core\Type\Type($this->request->get('type'), ''));
            $attrs = [];
            $valuess=json_decode($this->request->get('values'), true);
            \DZ::valuess($valuess);
            foreach ($valuess as $value) {
                $isArr=is_array($value['value']);
                $attr = new Attribute($value['uid'], '', 0, $value['key'], $isArr);
                $val=null;
                if ($isArr) {
                    $val=[];
                    foreach ($value['value'] as $valArr) {
                        if ($valArr['new']) {
                            if (!empty($FilesArr[$valArr['value']])) {
                                if (empty($FilesArr[$valArr['value']]['err'])) {
                                    $val[]=$FilesArr[$valArr['value']]['value'];
                                }
                            }
                        } else {
                            $val[]=new \Dzeta\Core\Value\File($valArr['value'], $valArr['clientName'], $valArr['previewName'], $valArr['size']);
                        }
                    }
                } else {
                    if (!empty($value['new'])) {
                        if (!empty($FilesArr[$value['value']])) {
                            if (empty($FilesArr[$value['value']]['err'])) {
                                $val=$FilesArr[$value['value']]['value'];
                            }
                        }
                    } elseif (isset($value['del'])) {
                        $val=new \Dzeta\Core\Value\File('');
                    } else {
                        $val=new \Dzeta\Core\Value\File($value['value'], $value['clientName'], $value['previewName'], $value['size']);
                    }
                }
                $attr->setValueS($val);
                $attrs[] = $attr;
            }

            $obj->setAttributes($attrs);

            $_obj = new Obj();
            $results = $_obj->AddFiles($obj, $this->session->get('logged')['User']);
            if (is_array($results)) {
                $result = 1;
                $data = $results;
            } else {
                $result = $results;
            }
        }

        echo json_encode(["result" => $result, 'data' => $data]);
    }

    /**
     * Get Preview file
     * $_GET['uid'] - uid of object
     *
     */

    public function getPreviewFileAction($uid)
    {
        if ($uid) {
            $uid = explode('.', $uid)[0]; //.JPG, Чтобы мы могли передавать с расширением
        }
        if (file_exists(BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uid.\Dzeta\Core\Value\File::FILE_EXT)) {
            readfile(BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$uid.\Dzeta\Core\Value\File::FILE_EXT);
            header("content-type: image/JPEG");
        }
    }
    /**
     * Download File
     * $_GET['file'] - uid of object
     *
     */
    public function downloadFileAction()
    {
        if ($this->request->isGet()) {
            if (!empty($this->request->get('file'))) {
                $file=new \Dzeta\Core\Value\File($this->request->get('file'), '');
                $_file=new Files();
                $res=$_file->getInfoFile($file);
                if ($res==1) {
                    \Dzeta\Core\Value\File::downloadFile($file);
                }
            } elseif (!empty($this->request->get('attr'))&&!empty($this->request->get('obj'))) {
                $obj = new Instance($this->request->get('obj'), '');
                $obj->setAttributes([new \Dzeta\Core\Type\Attribute($this->request->get('attr'), '', DataType::getIndexFromType(DataType::TYPE_FILE), false, false)]);
                $_obj = new Obj();
                $_obj->getAllValues([$obj]);
                $file=$obj->getAttributes()[0]->getValue();
                \Dzeta\Core\Value\File::downloadFile($file);
            }
        }
    }


    /**
     * Download File
     * $_GET['uid'] - uid of object
     *
     */
    public function dwnlFileObjAttrAction()
    {
        if ($this->request->isGet()) {
            //uid
            //	if(isset())
            $paramUID=$this->request->get('attrUid');
            $obj = new Instance($this->request->get('uid'), '');
            $_obj = new Obj();
            $data = $_obj->getAllValues($obj)[$this->request->get('uid')];
            foreach ($data as $key => $value) {
                if ($value->getUid()==$paramUID) {
                    $file=$value->getValue();
                    header('Content-Type: application/octet-stream;charset=utf-8');
                    header('Content-Disposition: attachment; filename="' . $file->getRealName(). '";');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    readfile(BASE_PATH.\Dzeta\Core\Value\File::FILE_FOLDER.$file->getuid().\Dzeta\Core\Value\File::FILE_EXT);
                    exit;
                }
            }
        }
    }


    public function changeLinkObjAction()
    {
       
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $obj = new Instance($this->request->get('uidObj'), '');
            $obj->setParent(new Instance($this->request->get('uidOldObj'), ''));
            $newParent=new Instance($this->request->get('uidNewObj'), '');
            $_obj = new Obj();
            $res = $_obj->ChangeBaseObj($obj, $newParent, $this->session->get('logged')['User']);
            $dataResult['result']=$res['result'];
            $dataResult['errors']=$res['error'];
        }
        echo json_encode($dataResult);
    }


    public function getChildAllObjAction()
    {
        $result = -1;
        $data = [];

        if ($this->request->isGet()) {
            $result = 1;
            $parent = new Core\Type\Type($this->request->get('uidClass'), '');
            $_type = new Type();
            $types = $_type->getChildren($parent);
            $childd=[];
            foreach ($types as $key => $value) {
                $parent = new Instance($this->request->get('uid'), '');
                $_obj = new Obj();
                $childd= $_obj->getChildrenByType($parent, $value);
                foreach ($childd as  $val) {
                    $data[]=[
                                'uidO'=>$val->getUid(),
                                'uuidC'=>$value->getUid(),
                                'nameC'=>$value->getName(),
                                'nameO'=>$val->getName(),
                            ];
                }
            }
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }

    /**
     * Getting objects by type using a filter
     * $_GET['type']   - uid of type
     * $_GET['filter'] - filter
     */
    public function getObjsByTypeForFilterAction()
    {
        $result = -1;
        $data = [];
        if ($this->request->isGet()) {
            $type = new Core\Type\Type($this->request->get('type'), '');
            $filter=$this->request->get('filter')===null? '':$this->request->get('filter');
            $_obj = new Obj();
            $res=$_obj->getObjsByClass($type, $filter);
            if ($res['result']==-1) {
                $result = -1;
            } else {
                $result = 1;
                if (isset($res['count'])) {
                    $data['count']=$res['count'];
                    $data['type']=$res['type'];
                } else {
                    $data['type']='list';
                    $data['data']=$res;
                }
            }
        }
        echo json_encode(['result' => $result, 'data' => $data]);
    }

    public function updateTmpTestAction()
    {
        $result = -1;
        $data = [];
        $objName='';
        if ($this->request->isPost()) {
            $obj = new Instance($this->request->get('obj'), '');
            $obj->setType(new Core\Type\Type($this->request->get('type'), ''));
           
            $attr = new Attribute($this->request->get('attr'), '', DataType::getIndexFromType($this->request->get('datatype')),false /*$value['key']*/, false);
            $attr->setValue(['value'=>$this->request->get('value')]);                
            $obj->setAttributes([$attr]);
            $_obj = new Obj();
            $results = $_obj->addValues([$obj], $this->session->get('logged')['User']);
          //  $objName=$obj->getName();
            if (is_array($results)) {
                $result = 1;
                $data = $results;
            } else {
                $result = $results;
            }
        }

        echo json_encode(['result' => $result, 'data' => $data/*,'objName'=>$objName*/]);
    }

    public function getAllChildObjValuesAction(){
        $result = -1;
        $data = [];
        $objName='';
        if($this->request->isGet())
        {
            $_obj = new Obj();
            $obj = new Instance($this->request->get('obj'), '');
            $childObj= $_obj->getChildrenByType($obj);
           
            $data = $_obj->getAllValues($childObj);
            $data=$childObj;
            $result=1;
            //406599fb-d6d4-4450-a1e2-fb55edda732f
        }
      
        echo json_encode(['result' => $result, 'data' => $data]);
    }


    //Получение имён объектов
    public function getNameObjectsAction() {
        $result = -1;
        $data = [];
        $objName='';
        if($this->request->isPost())
        {
            $_obj = new Obj();
            $data= $_obj->Object_GetNames($this->request->get('Objs'), true);

            $result=1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }
    //Массовое удаление объектов по конкретным айди
    public function removeObjsAction() {
        $result = -1;
        $data = [];
        if($this->request->isPost())
        {
            $_obj = new Obj();
            $data= $_obj->removeObjs($this->request->get('Objs'), $this->session->get('logged')['User']);
            $result=1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }


    public function getValueForParamObjAction() {
        $result = -1;
        $data = [];
        if($this->request->isGet())
        {
            $_obj = new Obj();

            $param = $this->request->get('param');
            $obj = $this->request->get('obj');

            $data= $_obj->getValueForParamObj($obj, $param);
            $result=1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }

    //копирование объектов
    public function copyAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if($this->request->isPost())
        {
            $_obj = new Obj();
            $objUid=$this->request->get('obj_parent');            
            $dataOBJs=[];
            $reqData=$this->request->get('objs_copy');
            $copyChildren=$this->request->get('copyChildren'); 
            foreach ($reqData as $key => $value)
            {
                $objCopy=new Instance( $value['obj'], '');
                $attrsForOBJ=[];
                if(isset($value['attrs'])) {
                    $attrs=$value['attrs'];
                    foreach ($attrs as $valueAttrs)
                    {

                        $attr = new Attribute($valueAttrs['attr'],'', DataType::getIndexFromType($valueAttrs['datatype']),false,false);
                        $attr->setValue(['value'=>$valueAttrs['value']]);
                        $attrsForOBJ[]=$attr;
                    }
                }
                $objCopy->setAttributes($attrsForOBJ);
                $dataOBJs[]=$objCopy;
            }
            $dt=$_obj->copy(new Instance($objUid, ''),$dataOBJs,null,$copyChildren,$this->session->get('logged')['User']);
            if($dt['result']!=1)
            {
                $dataResult = ["result"=>$dt['result'],'data'=>[],'errors'=>$dt['error']];
            }else
            {
                $dataResult['result']=1;
                $dataResult['CreatedObjs'] = json_decode($dt["CreatedObjs"]);
            }
        }
        echo json_encode($dataResult);
    }

    //Получение информации по объекту (Дата создания и владелец)
    public function getInfObjectAction()
    {
        $result = -1;
        $data = [];

        if ($this->request->isPost()) {
            $_obj = new Obj();
            $data = $_obj->getInfObject($this->request->get('uid'), $this->session->get('logged')['User']);

            $obj = new Instance($this->request->get('uid'), '');;
            $_obj = new Obj();
            $data_attr = $_obj->getValuesAndLastUpdateTime($obj);

            $data["attrs"] = $data_attr;

            $result = 1;
        }

        echo json_encode(['result' => $result, 'data' => $data]);
    }
}
