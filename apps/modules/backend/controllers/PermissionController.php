<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;

use Dzeta\Core\User\User;
use Dzeta\Core\User\UserGroup;
use Dzeta\Models\Permission;
use Dzeta\Controllers\ControllerBase;

class PermissionController extends ControllerBase
{
    /**
  	 * Get all modules System
  	 *
  	 */
    public function getModulesSystemAction()
    {
        $_perm=new Permission();
        $data=$_perm->getAllModules();

        $result = 1;
    		$dataResult = ["result"=>$result,'data'=>array_keys($data),'errors'=>[]];
        echo json_encode($dataResult);
    }

    public function addPermissionGroupAction()
    {
        $result = -1;
        $data = [];
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $_perm=new Permission();
            $group=new Core\User\UserGroup($this->request->get('group'),[]);
            $modules=$this->request->get('modules');
            $allModules=$_perm->getAllModules();
            $allModules=array_keys($allModules);
            $settModules=[];
            foreach ($modules as $key => $value)
            {
                if(in_array($value['module'],$allModules))
                {
                  $settModules[]=['module'=>$value['module'],'allow'=>empty($value["allow"])? 0:1];
                }
            }
            $group->setPermission(['modules'=>$settModules]);
            $dResult=$_perm->addPermissionGroup($group,$this->session->get('logged')['User']);
            if($dResult['result']!=1)
      			{
      					$dataResult['errors']=$result['error'];
      			}
      			$result=$dResult['result'];
      }
      $dataResult['result']=$result;
      echo json_encode($dataResult);
    }

    public function getModulesSettGroupAction()
    {
      $result = -1;
      $data = [];
      $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
      if ($this->request->isGet()) {
          $_perm=new Permission();
          $group=new Core\User\UserGroup($this->request->get('group'),[]);
          $dResult=$_perm->getAccessGroup($group,$this->session->get('logged')['User']);
          if($dResult['result']!=1)
          {
              $dataResult['errors']=$result['error'];
          }else {
              $dataResult['data']['modules']=$dResult['data'];
          }
          $result=$dResult['result'];
      }
      $dataResult['result']=$result;
      echo json_encode($dataResult);
    }

}
