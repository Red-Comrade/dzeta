<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\Export\ExportView;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;

use Dzeta\Core\User\User;
use Dzeta\Core\User\UserGroup;
use Dzeta\Core\Type\AttributeGroup;

use Dzeta\Models\App;
use Dzeta\Models\View;
use Dzeta\Models\Obj;
use Dzeta\Models\AttrGroup;
use Dzeta\Models\Calculation;

use Dzeta\Helpers\ViewHelper;

use Dzeta\Controllers\ControllerBase;

use DzetaCalcParser\DzetaCalc;

class ViewController extends ControllerBase
{
    /**
     *TODO описание
     *
    *type-GET
    *Получение таблицы
    	*view-uid view
    	*mode-=0- можешь не присылать
    	*attr_sort- uid атрибута по которому сорт
    	*start- начало
    	*num-количество
    	*attr_filter- если присылаешь, то массив из объектов
    	*	op- тип операции(<,>,=,~,!= и т.д)-если не прислал то я делаю ~. Для глобал поиска присылай ~
    	*	v - значение
    *		attr- uid атрибута, если не прислал, то все что выше это глобальный поиск
    *	Получение все как и ранее
  *  Получение допустимых значений для колонки
    *	view-uid view
  *  	mode-=1
  *  	attr_border- uid атрибута по которому будет вестись фильтр
  *  	attr_filter- если присылаешь, то массив из объектов
  *  		op- тип операции(<,>,=,~,!= и т.д)-если не прислал то я делаю ~. Для глобал поиска присылай ~
*    		v - значение
  *  		attr- uid атрибута, если не прислал, то все что выше это глобальный поиск
  *  	borderSearch- тут значение если идет по строкам(если количество более определенного), можешь не слать
  *  	Ответ:
  *  		result=1-все good, -1-все плохо
  *  		data:
  *  			1. type=search, тут не повезло и надо юзеру вводить значения, так как список большой, введенное шлешь в borderSearch
  *  				count , тут количество, если пришел 0, то пишем ничего не найдено, если более 0, просим ввести еще
  *  			2. type=list, тут получается список
    *				data- массив из объектов, где пока ключ V-значение
    *			3. type=border, тут приходят границы
  *  				min- минимальное значение
  *  				max-максимальное значение
     */
    public function getTableDataAction() {
        $result = -1;
        $data = [];

        if ($this->request->isPost())
        {
            $_view = new View();
            $view = ViewHelper::getViewFromRequest($this->request);
            $mode = empty($this->request->get('mode'))? 0:1;

            $UserID = null;
            if($this->session->has('logged')) {
                $UserID = $this->session->get('logged')['User']->getUid();

            }
            else if($this->config->dzeta->GuestUID){
                $UserID = $this->config->dzeta->GuestUID;
            }
            else {
                $this->response->redirect('login');
                return;
            }


            if (empty($mode))
            {
                $data=$_view->getDataTable($view, $UserID);

                $result = 1;
                echo json_encode([
                    'result' => $result,
                    'data' => $data["data"],
                    'isLoadTotal' => $data["isLoadTotal"],
                    'allCount' => $data["allCount"]
                ]);
                return;
            } else
            {
                $dt = $_view->getFilterValueTable($view, $UserID);
                if (empty($dt))
                {
                  echo json_encode([
                      'result' => -1,
                      'data' => []
                  ]);
                  return;
                } else {
                  echo json_encode([
                      'result' => 1,
                      'data' => $dt
                  ]);
                  return;
                }
            }
        }
    }

    /**
     *TODO описание
     *Футер ИТОГО для таблицы
     */
    public function getFooterTableDataAction() {
        $result = -1;
        $data = [];

        if ($this->request->isPost())
        {
            $_view = new View();
            $view = ViewHelper::getViewFromRequest($this->request);           
            
            $UserID = null;
            if($this->session->has('logged')) 
            {
                $UserID = $this->session->get('logged')['User']->getUid();
            }
            else if($this->config->dzeta->GuestUID)
            {
                $UserID = $this->config->dzeta->GuestUID;
            }
            else 
            {
                $this->response->redirect('login');
                return;
            }
            $js=\DzetaCalcParser\Core\Helper::convertViewDopSettingJSON($view);
            $data=[]; 
            $result = 0;             
            foreach ($this->request->get('block_calcs') as $key => $value) 
            {
                $calc = new Core\Calculation($value['uid'], '', '');
                $_calc = new Calculation();
                $_calc->get($calc);                
                $dzeta = new DzetaCalc([
                'formula' =>$calc->getFormula(),//'$x=getViewData($arg[0],null,10,$arg[1]);'
                'history' => false,
                'object' => null,
                'user' =>new Core\User\User($UserID, ''),
                'arguments' => [
                        ['variable' => '$arg', 'value' =>[0=>$view->getUid(),1=>$js]]
                    ]
                ]);
                $dzeta->start();  
                $resultData=$dzeta->getResult();  
                $r=\DzetaCalcParser\Core\Beautify::formatFooterDataTable($resultData);                
                if($r['result']==1)
                {
                    $data=array_merge($data,$r['data']);
                    $result = 1; 
                }else
                {
                    $data=[]; 
                    $result = 0;
                    break;
                }
            }
            echo json_encode([
                'result' => $result,
                'data' => $data,
            ]);
        }
    }

    /**
     * Add view for application
     * $_POST['app'] 	  - uid of app
     * $_POST['viewSettings'] 	- [] settings view
     * $_POST['view']    - uid View
     * $_POST['viewName']       - name View
     */
    public function addAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $application = new Core\App\App($this->request->get('app'), '');
            $sett=!empty($this->request->get('viewSettings'))? $this->request->get('viewSettings'):[];
            $view=new Core\App\View('', $this->request->get('viewName'), $sett, new Core\App\View($this->request->get('view'), ''));
            $view->setParentApp($application);
            $_view=new View();
            $result = $_view->add($view, $this->session->get('logged')['User']);
            if($result['result']==1)
      			{
      					$dataResult['data']['uid'] = $view->getUid();
      			}else {
      					$dataResult['errors']=$result['error'];
      			}
      			$result=$result['result'];
        }
        $dataResult['result']=$result;
    		echo json_encode($dataResult);
    }

    /**
     * Edit view
     * $_POST['viewSettings'] 	- [] settings view
     * $_POST['view']    		- uid View
     * $_POST['viewName']       - name View
     */
    public function editAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $sett=!empty($this->request->get('viewSettings'))? $this->request->get('viewSettings'):[];
            $view=new Core\App\View($this->request->get('view'), $this->request->get('viewName'), $sett);
            $_view=new View();
            $result = $_view->edit($view, $this->session->get('logged')['User']);
            if($result['result']==1)
      			{
      					$dataResult['data']['uid'] = $view->getUid();
      			}else {
      					$dataResult['errors']=$result['error'];
      			}
      			$result=$result['result'];
        }
        $dataResult['result']=$result;
    		echo json_encode($dataResult);
    }

    /**
     * Remove view
     * $_POST['view'] 	- uid view
     */
    public function removeAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $view=new Core\App\View($this->request->get('view'), '', []);
            $_view=new View();
            $result = $_view->remove([$view], $this->session->get('logged')['User']);
            $dataResult['data']=$result['data'];
            if($result['result']!=1)
            {
                $dataResult['errors']=$result['error'];
            }
            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }

    public function getParentToRootAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isGet()) {
            $view=new Core\App\View($this->request->get('view'), '', []);
            $_view=new View();
            $result = $_view->GetParrentsToRoot($view);
            $dataResult['data']=$result['data'];
            if($result['result']!=1)
            {
                $dataResult['errors']=$result['error'];
            }
            $resD=[];
            foreach ($dataResult['data'] as $key => $value) {
              if(get_class($value)=='Dzeta\Core\App\View')
              {
                $resD[]=['type'=>'view','uid'=>$value->getUid(),'name'=>$value->getName()];
              }else {
                $resD[]=['type'=>'app','uid'=>$value->getUid(),'name'=>$value->getName()];
              }
            }
            $dataResult['data']=$resD;
            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }
    public function getParrentViewsAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isGet()) {
            $view=new Core\App\View($this->request->get('view'), '', []);
            $_view=new View();
            $tableOrForm=null;
            if($this->request->get('table_or_form')!==null)
            {
              $tableOrForm=$this->request->get('table_or_form');
            }
            $result = $_view->GetParrentViews($view, $tableOrForm);
            $dataResult['data']=$result['data'];
            if($result['result']!=1)
            {
                $dataResult['errors']=$result['error'];
            }
            $resD=[];
            foreach ($dataResult['data'] as $key => $value) {
              if(get_class($value)=='Dzeta\Core\App\View')
              {
                $resD[]=['type'=>'view','uid'=>$value->getUid(),'name'=>$value->getName()];
              }else {
                $resD[]=['type'=>'app','uid'=>$value->getUid(),'name'=>$value->getName()];
              }
            }
            $dataResult['data']=$resD;
            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }

    //GetInfoView(Core\App\View $view)
    public function getInfoViewAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isGet()) {
            $view=new Core\App\View($this->request->get('view'), '', []);
            $_view=new View();
            $result = $_view->GetInfoView($view);
            $dataResult['data']=$view;
            if($result['result']!=1)
            {
                $dataResult['errors']=$result['error'];
            }
            $result=$result['result'];
        }
        $dataResult['result']=$result;
        echo json_encode($dataResult);
    }

    public function exportAction()
    {
        if ($this->request->isPost()) {
            $UserID = null;
            if ($this->session->has('logged')) {
                $UserID = $this->session->get('logged')['User']->getUid();
            } else if ($this->config->dzeta->GuestUID) {
                $UserID = $this->config->dzeta->GuestUID;
            } else {
                $this->response->redirect('login');
                return;
            }
            $view = ViewHelper::getViewFromRequest($this->request);
            $_view = new View();
            $export = new ExportView();

            $attrs = $_view->getAttributes($view, $UserID);
            $header = [];
            foreach ($attrs as $attr) {
                $header[] = ['v' => $attr->getName()];
            }
            $export->addRows([$header], true);

            $start = 0;
            $num = 1000;
            $view->setDopSett('num', $num);
            $data = $_view->getDataTable($view, $UserID);
            $start += 1;
            while (!empty($data['data'])) {
                $export->addRows($data['data']);
                $start += $num + 1;
                $view->setDopSett('start', $start);
                $data = $_view->getDataTable($view, $UserID);
            }
            $response = $this->response;
            $response->setHeader("Content-Type", 'application/vnd.ms-excel');
            $response->setHeader("Content-Disposition", 'attachment; filename="export.xlsx"');
            readfile($export->save());
        }
    }

    public function getChildrenViewsAction(){
         $tree=[];
        if ($this->request->isGet()) {
            $uid_view=$this->request->get('view')/*'947e4c66-62d8-467e-8479-d16079298d42'*/;
            $view = new Core\App\View($uid_view, '', []);
            $_app=new App();
           // $data=$_view->GetParrentViews($view);
            $data=$_app-> getAllTreeAppViewDB();
            $tree=$this->searchParenttest($uid_view,$data);
        }
        
     /*  $arrColumn=array_column($data, 'ID');
       $arrSearh=array_search($uid_view, $arrColumn);
*/
        echo json_encode($tree);
    }

    public function getWindowDataAction() {
        $result = -1;
        $data = [];
        $tree = [];
        //Пост потому что скоро будет много данных на сервер посылаться
        if ($this->request->isPost()) {
            $is_add = $this->request->get('is_add');
            $app = $this->request->get('app');

            if($this->session->has('logged')) {
                $UserID = $this->session->get('logged')['User']->getUid();
            }
            else if($this->config->dzeta->GuestUID){
                $UserID = $this->config->dzeta->GuestUID;
            }
            else {
                $this->response->redirect('login');
                return;
            }

            $params_data = [];
            $obj_uid = null;
            $type = null;

            #region get_values
            if(empty($is_add)) {
                $obj_uid = $this->request->get('obj');

                $arrObj=[];
                $attrs=[];
                if ($this->request->get('attrs')!==null) {
                    foreach ($this->request->get('attrs') as $key => $value) {
                        $attrs[]=new \Dzeta\Core\Type\Attribute($value, '', DataType::getIndexFromType(DataType::TYPE_TEXT), false, false);
                    }
                }
                if ($this->request->get('obj')!==null) {
                    $tmp=new Instance($this->request->get('obj'), '');
                    if (!empty($attrs)) {
                        $tmp->setAttributes($attrs);
                    }
                    $arrObj[]=$tmp;
                } elseif ($this->request->get('objs')!==null) {
                    foreach ($this->request->get('objs') as $key => $value) {
                        $tmp=new Instance($value, '');
                        if (!empty($attrs)) {
                            $tmp->setAttributes($attrs);
                        }
                        $arrObj[]=$tmp;
                    }
                } else {
                    echo json_encode(['result' => -1, 'data' => []]);
                    return;
                }
                $_obj = new Obj();
                $data_v = $_obj->getAllValues($arrObj);
                $data = $data_v[$obj_uid];

                $data = json_decode(json_encode($data), true);


                foreach ($data as $item_param) {
                    $params_data[$item_param["uid"]] = $item_param;
                }
            }
            else {
                $type = $this->request->get('type');
                $group = new AttributeGroup('00000000-0000-0000-0000-000000000000', '');
                $group->setParent(new Core\Type\Type($type, ''));

                $_group = new AttrGroup();
                $data = $_group->getAttributes($group);

                $data = json_decode(json_encode($data), true);

                foreach ($data as $item_param) {
                    $params_data[$item_param["uid"]] = $item_param;
                }
            }

            #endregion


            $uid_view=$this->request->get('view')/*'947e4c66-62d8-467e-8479-d16079298d42'*/;
            $view = new Core\App\View($uid_view, '', []);
            $_app = new App();

            $_app->GenerateFSettings($UserID, $uid_view);
            $FSettings = $_app->GenerateFSettings($UserID, $app);


            $data_tree = $_app -> getAllTreeAppViewDB([]);
            $views_ = $this->searchParenttest($uid_view, $data_tree, 1, $is_add, $FSettings);
            $tables_ = $this->searchParenttest($uid_view, $data_tree, 0, $is_add, $FSettings);


            $views_ = json_decode(json_encode($views_), true);
            $tables_ = json_decode(json_encode($tables_), true);

            if(count($views_) > 0) {
                $fill_value_params = function(&$views) use ($params_data, &$fill_value_params, &$is_add)  {
                    foreach ($views as &$item_v) {
                        $attrs_d = [];
                        foreach ($item_v["attrs"] as $item_attr) {
                            if(isset($params_data[$item_attr["uid"]])) {
                                $param_view = $params_data[$item_attr["uid"]];
                                $param_view["dopSett"] = $item_attr["dopSett"];
                                array_push($attrs_d, $param_view);
                            }
                        }

                        $item_v["params_data"] = $attrs_d;

                        if(count($item_v["views"]) > 0) {
                            $fill_value_params($item_v["views"]);
                        }
                    }
                };
                $fill_value_params($views_);

                $tree = [
                    "views" => $views_,
                    "tables" => $tables_
                ];
            }
            else {
                //Строим всё по-старому

                //если мы не настроили форму для отображения, то отображаем все параметры + конфигурируем параметры таблицы
                $UserID = $this->session->get('logged')['User']->getUid();
                $_view=new View();
                $params_data_view = $_view->getAttributes($view, $UserID);
                $params_data_view = json_decode(json_encode($params_data_view), true);

                foreach ($data as &$item_param) {
                    foreach ($params_data_view as $item_param_view) {
                        if($item_param["uid"] == $item_param_view["uid"]) {
                            $item_param["dopSett"] = $item_param_view["dopSett"];
                            break;
                        }
                    }
                }

                $tree = [
                    "setTableOrForm" => 2,
                    "views" =>
                    [
                        [
                            "setTableOrForm" => 1,
                            "params_data" => $data,
                            "params_data_view" => $params_data_view,
                            "name" => "",
                            "views" => [],
                            "tables" => [],
                            "hide_h" => 1,
                        ]
                    ],
                    "tables" => []
                ];
            }



            $result = 1;
        }

        echo json_encode([ "result" => $result, "window_data" => $data, "tree_data" => $tree , "f" => $FSettings]);
    }



    private function searchParenttest($uidView, $dataAll, $TableOrForm = null, $is_add = false, $FSettings = [])
    {
        //0 - table, 1 - form, 2 - container
        //Теперь нам без разницы какой тип
        //string $uid, string $name,array $settings=[],?View $parentView=null,array $views = []
        $retuenData=[];
        $UserID = $this->session->get('logged')['User']->getUid();
        $_view=new View();
        foreach ($dataAll as $key => $value) {
            $tmpTableOrForm = $value["TableOrForm"] != 0 ? 1 : 0; //если это не таблица, то мы пушим
            if($value['Parent']==$uidView && $tmpTableOrForm == $TableOrForm && (!$is_add || $tmpTableOrForm!= 0)) {
                //Проверяем на права для таблиц
                if($tmpTableOrForm != 0 || isset($FSettings[$value['ID']]) && !empty($FSettings[$value['ID']]["ExRead"]) ) {
                    $view = new Core\App\View($value['ID'], $value['Name'],[],new Core\App\View($uidView, ''), []);
                    $r = $_view->getAttributes($view, $UserID);
                    if($TableOrForm == 1 || $TableOrForm == 2) {
                        //Для таблиц мы не получаем нижестоящих вкладок
                        $views_ = $this->searchParenttest($value['ID'], $dataAll, 1, $is_add, $FSettings);
                        $tables_ = $this->searchParenttest($value['ID'], $dataAll, 0, $is_add, $FSettings);

                        foreach ($views_ as &$item_view) {
                            $item_view->setViews($this->searchParenttest($item_view->getUid(), $dataAll, 1, $is_add, $FSettings));
                        }
                        foreach ($tables_ as &$item_table) {
                            $item_table->setViews($this->searchParenttest($item_table->getUid(), $dataAll, 0, $is_add, $FSettings));
                        }

                        $view->setViews( $views_ );
                        $view->setTables( $tables_ );
                    }
                    $view->setTableOrForm($value["TableOrForm"]);
                    $newAttrs=[];
                    foreach ($r as  $value) {
                        $calcUid=null;
                        if(!is_null($value->getDopSett('calc_color')))
                        {
                            $calcUid=$value->getDopSett('calc_color')->getUid();
                        }

                        /*
                        $tA=[
                            'attr' => $value, //Тут мы выводим все свойства атрибута
                              'uid'=>$value->getUid(),
                              'name'=>$value->getName(),
                              'datatype'=>$value->getDatatype(),
                              'isArray'=>(int)$value->isArray(),
                              "hide"=>$value->getDopSett('Hide'),
                              'require'=>$value->getRequare(),
                              'LinkedClass'=>$value->getLinkType(),
                              'key'=>$value->isKey(),
                              'view'=>$value->getDopSett('view'),
                              'calc_color'=>$calcUid,
                              'class'=>[
                                  'uid'=>$value->getParent()->getUid(),
                                  'name'=>$value->getParent()->getName(),
                                  'parentUID'=>$value->getParent()->getParent()->getUid()
                              ]
                            ];

                        */
                        $tA = $value;



                        $newAttrs[]=$tA;
                    }
                    $view->setAttrinutes($newAttrs);

                    $retuenData[] = $view;
                }
            }


            /*
            if($value['Parent']==$uidView && !empty($value['IsView']) && intval($value["TableOrForm"]) == $TableOrForm )
            {
                $view=new Core\App\View($value['ID'], $value['Name'],[],new Core\App\View($uidView, ''), []);
                $r = $_view->getAttributes($view);
                if($TableOrForm == 1) {
                    //Для таблиц мы не получаем нижестоящих вкладок
                    $view->setViews($this->searchParenttest($value['ID'],$dataAll, 1) );
                    $tables_ = $this->searchParenttest($value['ID'],$dataAll, 0);

                    foreach ($tables_ as &$item_table) {
                        $item_table->setViews($this->searchParenttest($item_table->getUid(),$dataAll, 0));
                    }

                    $view->setTables( $tables_ );
                }
                $view->setTableOrForm($value["TableOrForm"]);
                $newAttrs=[];
                foreach ($r as  $value) {
                    $calcUid=null;
                    if(!is_null($value->getDopSett('calc_color')))
                    {
                        $calcUid=$value->getDopSett('calc_color')->getUid();
                    }

                    $tA = $value;


                   
                    $newAttrs[]=$tA;
                }
                $view->setAttrinutes($newAttrs);

                $retuenData[] = $view;
            }

            */
        }
        return $retuenData;
    }

    //TableOrForm


    public function getViewAction()
    {
        $result = -1;
        $dataResult = ["result" => $result, 'data' => null, 'errors' => []];
        if ($this->request->isGet()) {
            $UserID = null;
            if ($this->session->has('logged')) {
                $UserID = $this->session->get('logged')['User']->getUid();

            } else if ($this->config->dzeta->GuestUID) {
                $UserID = $this->config->dzeta->GuestUID;
            } else {
                $this->response->redirect('login');
                return;
            }

            $_view = new View();
            $view = new Core\App\View($this->request->get('view'), '');
            $result = 1;
            $_view->GetInfoView($view);
            $_view->getAttributes($view, $UserID);
            $_view->getViewClass($view);
            $rootData = $_view->GetParrentsToRoot($view);
            $resD = [];
            foreach ($rootData['data'] as $key => $value) {
                if (get_class($value) == 'Dzeta\Core\App\View') {
                    $resD[] = ['type' => 'view', 'uid' => $value->getUid(), 'name' => $value->getName()];
                } else {
                    $resD[] = ['type' => 'app', 'uid' => $value->getUid(), 'name' => $value->getName()];
                }
            }
            $dataResult['data']['view'] = $view;
            $dataResult['data']['ParrentsToRoot'] = $resD;
        }
        $dataResult['result'] = $result;
        echo json_encode($dataResult);
    }

    public function saveAction()
    {
      $result = -1;
      $dataResult = ["result"=>$result,'data'=>null,'errors'=>[]];
      if($this->request->isPost())
      {
        $_view=new View();
        $application = new Core\App\App($this->request->get('appUid'), '');
        
        /* $sett['charts']=[
            ['uid'=>'0a7a0330-4d6e-4b0d-a99e-2dbc329dd97d','name'=>'testGraphik','class_uid'=>'516979ad-e391-4501-9584-3187dc3dd475']
        ];*/
        $view=new Core\App\View($this->request->get('uid'), $this->request->get('name'), $this->request->get('settings'));
        $view->setParentApp($application);
        $attrs=$this->request->get('attrs');
        $attrNew=[];
        foreach($attrs as $key => $value) {
            $tmpArr=[];
                if (!empty($value['uid'])) {
                    $tmpArr['ID']=$value['uid'];
                }
                $tmpArr['TypeOfData']=\Dzeta\Core\Type\DataType::getIndexFromType($value['datatype']);
                $tmpArr['Name']=$value['name'];
                $tmpArr['Class']=empty($value['parent']['uid'])? $value['class']['uid']:$value['parent']['uid'];
                $tmpArr['Hide']=0;
                
                $tmpArr['Settings']=$value['dopSett'];
                if(!empty($value['dopSett']['calc_color']))
                {
                  $tmpArr['CalcForColoring']=$value['dopSett']['calc_color']['uid'];
                }
                if(!empty($value['dopSett']['Hide'])){
                  $tmpArr['Hide']=$value['dopSett']['Hide'];
                }                
                if(!empty($value['dopSett']['view']))
                {
                    $tmpArr['ParentViews']=[$value['dopSett']['view']['uid']];
                }
                if(!empty($value['dopSett']['data_source']))
                {
                    $tmpArr['CalcForDataSourse']=$value['dopSett']['data_source']['uid'];
                }
                $attrNew[]=$tmpArr;
        }
        $clasesLink = !empty($this->request->get('ClasseLinks')) ? $this->request->get('ClasseLinks') : [];
        $clLink=[];
        foreach ($clasesLink as $key => $value) {
          
          $mainClass = new Core\Type\Type($value['MainClass']['uid']==-1? \Dzeta\Core\Type\Type::TYPE_ROOT:$value['MainClass']['uid'], $value['MainClass']['name'], false);
          $class =new Core\Type\Type($value['DependentClass']['uid']==-1? \Dzeta\Core\Type\Type::TYPE_ROOT:$value['DependentClass']['uid'], $value['DependentClass']['name'], false);
          $tmp=[];
          if(!empty($value['Settings']))
          {                              
            if(!empty($value['Settings']['single_mode_attr']))
            {
              $tmp['single_mode_attr']= new Core\Type\Attribute($value['Settings']['single_mode_attr']['uid'], '', 1, false, false);
            }
            if(!empty($value['Settings']['single_mode_op']))
            {
              $tmp['single_mode_op']=$value['Settings']['single_mode_op'];
            }
          }
          $ClassesLink=new \Dzeta\Core\App\View\ClassesLink($mainClass,$class,new \Dzeta\Core\App\View\SettClassesLink($tmp));
           $view->addClassesLink($ClassesLink);
        }
        $resultEdit = $_view->edit($view, $this->session->get('logged')['User']);
        $res=$_view->addAttributes($view, json_encode($attrNew), $this->session->get('logged')['User']);
        if($res['result']!= \Dzeta\Library\Db::SUCCESS_CODE)
        {
            $result = $res['result'];
            $dataResult['errors']=$res['error'];
        }else
        {
            $res=$_view->addViewClass($view, $this->session->get('logged')['User']);
            $result = $res['result'];
            if($result!=\Dzeta\Library\Db::SUCCESS_CODE)
            {                
                $dataResult['errors']=$res['error'];
            }
        }
      }
      $dataResult['result'] =$result;        
      echo json_encode($dataResult);
    }

    public function getDataGraphAction()
    {
        $_view=new View();
        $view_uid=$this->request->get('view');
        $view=new Core\App\View($view_uid, '',[]);
        $_view->GetInfoView($view);
        $dataGraph=[];
        $settView=$view->getSettings();
        foreach ($settView['charts'] as  $value) 
        {            
            $calc=new Core\Calculation( $value['uid'],  $value['name'], '');
            $_calc=new Calculation();
            $_calc->get($calc);
            $dzeta=new DzetaCalc(['Formula'=>$calc->getFormula(),'ObjUid'=>\Dzeta\Core\Instance::INSTANCE_ROOT]);
            $lastValue=$dzeta->getAnswer();
            if(isset($lastValue['val'])&&!is_null($lastValue['val']))
            {
                $dataGraph[]=json_decode($lastValue['val']);
            }
            
        }
        echo json_encode($dataGraph);

    }
}
