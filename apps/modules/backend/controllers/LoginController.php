<?php

namespace Dzeta\Modules\Backend\Controllers;

use Phalcon\Mvc\Controller;

use Dzeta\Core;
use Dzeta\Core\User\UserGroup;
use Dzeta\Controllers\ControllerBase;

use Dzeta\Models\User;
use Dzeta\Models\Permission;
use Dzeta\Models\Obj;
use Dzeta\Models\Type;
use Dzeta\Models\Files;
use Dzeta\Core\Instance;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;

class LoginController extends Controller
{
    public function loginAction()
    {
        $result = -1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
            $login = $this->request->getPost('login');
            $password = $this->request->getPost('pwd');
            $ip = $this->request->getClientAddress();

            $user = new Core\User\User('', $login);
            $user->setPassword($password)->setIP($ip);

            $_user = new User();
            $result = $_user->checkPassword($user);
            if ($result['result'] == 1) 
            {
                $_perm=new Permission();
                $perm=$_perm->getUserSessPermission($user);
                if(!empty($perm['modules']))
                {
                    $user->setPermissionSession("module",$perm['modules']);
                    $user->setPermissionSession("controller",$perm['controllers']);
                    $dataResult['result']= 1;
                    $this->session->set('logged', [
                        'User' => $user
                    ]);
                }else 
                {
                    $dataResult['result']=0;
                }                
            }else
            {
                $dataResult['result']= $result['result'];
                $dataResult['errors']= $result['error'];
            }
        }

        echo json_encode($dataResult);
    }

    public function logoutAction()
    {
        $this->session->destroy();
        $this->response->redirect('login');
    }

    public function forgotPasswordAction()
    {
        $result=-1;
        $dataResult = ["result"=>$result,'data'=>[],'errors'=>[]];
        if ($this->request->isPost()) {
    				$user = new Core\User\User('',$this->request->get('login'));
    				$_user = new User();
    				$res = $_user->forgotPasswordEmail($user);
    				if($res['result']==1)
    				{
    				}else {
    						$dataResult['errors']=$res['error'];
    				}
    				$dataResult['result']= $res['result'];
    		}

    		echo json_encode($dataResult);

    }


    public function registrationAction() {
        $result = -1;
        $data = [];
        $objName = '';
        $errors = [];
        if ($this->request->isPost()) {
            $obj = new Instance('', '');
            $obj->setType(new Core\Type\Type($this->request->get('type'), ''))->setParent(new Instance($this->request->get('parent'), ''));
            $attrs = [];
            $password = $this->request->get('password');


            foreach ($this->request->get('values', null, []) as $value) {
                $isArray = is_array($value['value']);
                $attr = new Attribute($value['attr'], '', DataType::getIndexFromType($value['datatype']), $value['key'], $isArray);
                if($isArray)
                {
                    $tmpV=[];
                    foreach ($value['value'] as  $valA) {
                        $tmpV[]=['value'=>$valA['value']];
                    }

                    $attr->setValue(['value'=>$tmpV]);
                }else {
                    $attr->setValue(['value'=>$value['value']]);
                }
                $attrs[] = $attr;
            }
            $obj->setAttributes($attrs);

            $_obj = new Obj();
            //Получаем пользователя от которого мы будем добавлять других пользователей
            $user_login = new Core\User\User($this->config->dzeta->registration_guest_UID, $this->config->dzeta->registration_guest_LOGIN);
            $results = $_obj->checkUniqueAndAddWithValues($obj, Instance::ADDED_BY_USER, $user_login );
            $objName=$obj->getName();
            if (is_array($results['result'])) {
                $result = 1;
                $_obj->addEventQueueAfterCreateObj($obj, $user_login);
                $data = ['uid' => $obj->getUid(), 'attrs' => $results];



                $user = new Core\User\User($data["uid"], '');
                //$password=Core\User\User::GeneratePassword();
                $user->setPassword($password);
                $_user = new User();
                $res = $_user->changePassword($user, $user_login);

                if($res['result']==\Dzeta\Library\Db::SUCCESS_CODE)
                {
                    $dataResult['data']= $password;
                }else
                {
                    $dataResult['errors']=$result['error'];
                }
                $dataResult['result']= $res['result'];




            } else {
                $result = $results['result'];
                $errors = $results['errors'];
            }
        }

        echo json_encode([
            'result' => $result,
            'data' => $data,
            'objName' => $objName,
            'errors' => $errors,
        ]);
    }
}
