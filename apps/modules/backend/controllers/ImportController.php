<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Core\Import\ChunkReadFilter;
use Dzeta\Models\Import;
use Dzeta\Controllers\ControllerBase;

class ImportController extends ControllerBase
{
	const EXCEL_FILE_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
	const MAX_READ_ROW = 20;

	public function index()
	{
	}

	public function getExcelDataAction() {
		$result = -1;
		$data = [];
		if ($this->request->hasFiles()) {
		 	$file = $this->request->getUploadedFiles()[0];
		 	if ($file->getRealType() == self::EXCEL_FILE_TYPE) {
		 		$options = LIBXML_COMPACT | LIBXML_PARSEHUGE;
			    \PhpOffice\PhpSpreadsheet\Settings::setLibXmlLoaderOptions($options);
		 		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file->getTempName());
		        $chunkFilter = new ChunkReadFilter();
	            $chunkFilter->setRows(1, self::MAX_READ_ROW);
	            $reader->setReadFilter($chunkFilter);
				$spreadsheet = $reader->load($file->getTempName());
				$sheet = $spreadsheet->getActiveSheet();
				$column = $sheet->getHighestDataColumn();
				$row = $sheet->getHighestDataRow();
				$row = $row > self::MAX_READ_ROW ? self::MAX_READ_ROW : $row;
				$range = $sheet->rangeToArray("A1:{$column}{$row}", '', true, true);

				$columns = range('A', $column);
				$data['columns'] = $columns;
				$data['sheet'] = $range;

				$result = 1;
		 	} else {
		 		$result = -2;
		 	}
		}

		echo json_encode(['result' => $result, 'data' => $data]);
	}

	public function runAction() {
		$result = -1;
		$data = [];
		if ($this->request->hasFiles() && $this->request->hasPost('settings') && $this->request->hasPost('class') && $this->request->hasPost('uid')) {
		 	$file = $this->request->getUploadedFiles()[0];
		 	if ($file->getRealType() == self::EXCEL_FILE_TYPE) {
		 		$uid = $this->request->getPost('uid');
		 		$settings = $this->request->getPost('settings');
		 		$class = $this->request->getPost('class');
		 		$parent = $this->request->getPost('parent', null, null);
		 		$rewrite = $this->request->getPost('rewrite', \Phalcon\Filter::FILTER_ABSINT, Import::REWRITE_ACTION);

		 		$_import = new Import();

		 		$results = $_import->runCache($uid, $file->getTempName(), $class, $parent, $settings, $rewrite);

		 		$data = $results;

				$result = 1;
		 	} else {
		 		$result = -2;
		 	}
		}

		header_remove('Set-Cookie');
		echo json_encode(['result' => $result, 'data' => $data]);
	}
}