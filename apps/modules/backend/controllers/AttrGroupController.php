<?php

namespace Dzeta\Modules\Backend\Controllers;

use Dzeta\Core;
use Dzeta\Core\Type\AttributeGroup;
use Dzeta\Models\AttrGroup;
use Dzeta\Controllers\ControllerBase;

class AttrGroupController extends ControllerBase
{
	/**
	 * Add group
	 * $_POST['name'] - name of group
	 * $_POST['parent'] - parent type of group
	 *
	 */
	public function addAction() {
		$res = -1;
		$data = [];
		$dataResult = ["result"=>$res,'data'=>[],'errors'=>[]];
		if ($this->request->isPost()) 
		{
			$group = new AttributeGroup('', $this->request->get('name'));
			$group->setParent(new Core\Type\Type($this->request->get('parent'), ''));

			$_group = new AttrGroup();
			$result = $_group->add($group, $this->session->get('logged')['User']);
			$res=$result['result'];			
			$dataResult['errors'] = $result['errors'];
			$dataResult['data']['uid'] = $group->getUid();
		}
		$dataResult['result']=$res;
  		echo json_encode($dataResult);
	}

	/**
	 * Update group
	 * $_POST['uid'] - uid of group
	 * $_POST['name'] - name of group
	 *
	 */
	public function updateAction() {
		$res = -1;
		$data = [];
		$dataResult = ["result"=>$res,'data'=>[],'errors'=>[]];

		if ($this->request->isPost()) 
		{
			$group = new AttributeGroup($this->request->get('uid'), $this->request->get('name'));

			$_group = new AttrGroup();
			$result = $_group->rename($group, $this->session->get('logged')['User']);
			$res=$result['result'];			
			$dataResult['errors'] = $result['errors'];
			$dataResult['data']['uid'] = $group->getUid();
		}
		$dataResult['result']=$res;
  		echo json_encode($dataResult);
	}

	/**
	 * Delete group
	 * $_POST['uid'] - uid of group
	 *
	 */
	public function deleteAction() {
		$result = -1;
		$data = [];

		if ($this->request->isPost()) {

		}

		echo json_encode(['result' => $result, 'data' => $data]);
	}

	/**
	 * Get attributes of group
	 * $_GET['type'] - uid of type
	 * $_GET['group'] - uid of group
	 *
	 */
	public function getAttributesAction() {
		$result = -1;
		$data = [];

		if ($this->request->isGet()) {
			$group = new AttributeGroup($this->request->get('group'), '');
			$group->setParent(new Core\Type\Type($this->request->get('type'), ''));

			$_group = new AttrGroup();
			$data = $_group->getAttributes($group);
			$result = 1;
		}

		echo json_encode(['result' => $result, 'data' => $data]);
	}
}