<?php
// declare(strict_types=1);

namespace Dzeta\Modules\Backend\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class CronController extends Controller
{
	const DataQueue=
	[
		"calc"=>[
			'key'=>'48597b40035b3ad72309de85f2045909',
			'url'=>'cron/queuecalciiprocflow'
		],
	];
	


	public function queuecalciiprocAction($key)
	{
		/*Отвечает за запуск очереди расчетов*/
		if($key!=='a2b347f2a722b7397157a3c322734d32')
		{
			echo json_encode(['res'=>1]);
			return false;
		}
		//теперь раскидываем по потокам, чтобы каждый смог взял свое
		$constNumQueue=1;

		$flows=$this->createFlowIn(self::DataQueue['calc']['url'],self::DataQueue['calc']['key'],$constNumQueue);
		$this->runFlows($flows);
		echo json_encode('1121212');
	}

	public function queuecalciiprocflowAction($key,$num)
	{
		/*Отвечает за расчет одного потока*/
		if($key!==self::DataQueue['calc']['key'])
		{
			echo json_encode(['res'=>rand(1,100)]);
			return false;
		}
		//теперь делаем запрос в модельку, где все считаем
		echo 12345;
	}

	public function testAction(){
		\DZ::yyy($_SERVER);
	}

	private function createFlowIn($url,$key,$num,$dataUrl=[])
	{
		$fl_array=[];
		$urlInit=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_ADDR'].'/'.$url.'/'.$key;
		$dopUrl='';
		if(!empty($dataUrl))
		{
			$dopUrl='/'.implode('/',$dataUrl);
		}
		for($i=0;$i<$num;$i++)
		{
			$u=$urlInit.'/'.($i+1).$dopUrl;
			$ch = curl_init($u);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($_SERVER['HTTP_USER_AGENT']));
            $fl_array[] = $ch;
            echo $u.PHP_EOL;
		}
		return $fl_array;
	}

	private function runFlows($flows)
	{
		$mh = curl_multi_init(); //создаем набор дескрипторов cURL
		
        for ($i = 0,$lenFlows=count($flows); $i < $lenFlows; $i++)
        {
            curl_multi_add_handle($mh, $flows[$i]);
        }
        while (curl_multi_exec($mh, $running) == CURLM_CALL_MULTI_PERFORM); 
        $status = curl_multi_exec($mh, $running);
        while ($running > 0 && $status == CURLM_OK)
        {
            $sel = curl_multi_select($mh, 4);
            usleep(100000);
            while (($status = curl_multi_exec($mh, $running)) == CURLM_CALL_MULTI_PERFORM);
            while (($info = curl_multi_info_read($mh)) != false)
            {
                $err = curl_error($info['handle']);
                $easyHandle = $info['handle'];    
                $one = curl_getinfo($easyHandle);  
                $httpCode = $one['http_code'];
                echo    $httpCode ."_";          
                // flush(); //Сразу выводим инфу в браузер
                /*
                if ($httpCode == 200)
                {   
                    $content = curl_multi_getcontent($info['handle']);
                   
                }*/
                curl_multi_remove_handle($mh, $easyHandle);
                curl_close($easyHandle);
            }
        }
	}
}