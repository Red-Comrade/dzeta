<?php

namespace DzetaCalcParser\Functions;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Core\BaseFunctions;

class BigleFunctions extends BaseFunctions
{
	protected static $methods = [];
	protected static $functions = [
		'sendJsonDataBigle' => [
            'name' => 'sendJsonDataBigle',
            'completion' => [
            	'label' => 'sendJsonDataBigle',
            	'detail' => 'sendJsonDataBigle(string type, string id, string action, array fields)',
            	'kind' => 'function',
            	'insertText' => 'sendJsonDataBigle(${1:type}, ${2:id}, ${3:action}, ${4:fields})'
            ],
            'arguments' => [
            	[
            		'type' => Constants::TYPE_STRING
            	],
            	[
            		'type' => Constants::TYPE_STRING
            	],
            	[
            		'type' => Constants::TYPE_STRING
            	],
            	[
            		'type' => Constants::TYPE_ARRAY
            	],
            ]
        ],
	];

	public static function sendJsonDataBigle($dzetaCalc, $paramsFunc, $paramMethod, $position, $currentPath)
	{
		if(empty($paramMethod))
		{
			$check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
			if ($check)
			{
				$type = $paramsFunc[0]['value'];
				$id = $paramsFunc[1]['value'];
				$action = $paramsFunc[2]['value'];
				$fields = $paramsFunc[3]['value'];

				$fieldsData = [];
				foreach ($fields as $key => $value)
				{
					if ($value['type'] == Constants::TYPE_ARRAY)
					{
						foreach ($value as $v)
						{
							$fieldsData[$key][] = $v;
						}
					} else
					{
						$fieldsData[$key] = $value;
					}
				}

				$data = [
					'entity' => $type,
					'id' => $id,
					'action' => $action,
					'fields' => $fieldsData
				];
				$request = http_build_query(['data' => json_encode($data)]);
		        $settings = parse_ini_file(BASE_PATH . '/config/dop/bigle/bigle.ini', true);
		        if (isset($settings['biglejson']['url']))
          		{
		        	$url = $settings['biglejson']['url'] . '&' . $request;
		           	$json = file_get_contents($url);
		           	$result = json_encode($json, true);
		           	if (!empty($result['result']))
           			{
           				return [
           					'type' => Constants::TYPE_STRING,
           					'value' => $url,
           					'ps' => $position['ps'],
           					'pf' => $position['pf'],
           					'rows' => $position['rows']
           				];
           			}
          		}
			}
		}
	}
}