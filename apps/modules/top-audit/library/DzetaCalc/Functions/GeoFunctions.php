<?php

namespace DzetaCalcParser\Functions;

use Dzeta\Modules\TopAudit\Models\Geo;
use Dzeta\Core\Instance;
use Dzeta\Core\Type\Attribute;

use DzetaCalcParser\Core\Constants;
use DzetaCalcParser\Core\Helper;
use DzetaCalcParser\Core\Error;
use DzetaCalcParser\Core\BaseFunctions;

class GeoFunctions extends BaseFunctions
{
    protected static $methods = [];

    protected static $functions = [
        'getUsersWithinRadius' => [
            'name' => 'getUsersWithinRadius',
            'completion' => [
                'label' => 'getUsersWithinRadius',
                'detail' => 'getUsersWithinRadius(array places, string placesCoordinatesAttr, string userRadiusAttr, string userCoordinatesAttr)',
                'kind' => 'function',
                'insertText' => 'getUsersWithinRadius(${1:places}, ${2:placesCoordinatesAttr}, ${3:userRadiusAttr}, ${4:userCoordinatesAttr})',
            ],
            'arguments' => [
                [
                    'type' => Constants::TYPE_ARRAY
                ],
                [
                    'type' => [Constants::TYPE_STRING, Constants::TYPE_ATTRIBUTE],
                ],
                [
                    'type' => [Constants::TYPE_STRING, Constants::TYPE_ATTRIBUTE],
                ],
                [
                    'type' => [Constants::TYPE_STRING, Constants::TYPE_ATTRIBUTE],
                ]
            ]
        ],
    ];

    public static function getUsersWithinRadius($dzetaCalc, $paramsFunc, $paramProc)
    {
        if (empty($paramMethod))
        {
            $check = self::checkArguments($dzetaCalc, __FUNCTION__, $paramsFunc, $position);
            if ($check)
            {
                $places = [];
                foreach ($paramsFunc[0]['value'] as $value)
                {
                    if ($value['type'] == Constants::TYPE_STRING || $value['type'] == Constants::TYPE_OBJECT)
                    {
                        $places[] = new Instance($value['value'], '');
                    } else
                    {
                        $function = self::getFunctionByName(__FUNCTION__);
                        Error::addError(
                            ['general'],
                            ['functions', 'params_array_elems_format'],
                            [
                                'index' => 2,
                                'function' => $function['calcName'],
                            ],
                            $dzetaCalc,
                            [
                                'ps' => $position['ps'],
                                'pf' => $position['pf'],
                                'rows' => $position['rows'],
                            ]
                        );
                        $check = false;
                        break;
                    }
                }

                if ($check)
                {
                    if ($paramsFunc[1]['type'] == Constants::TYPE_STRING)
                    {
                        $placesCoordinatesAttr = new Attribute($paramsFunc[1]['value'], '', 1, false, false);
                    } else if ($paramsFunc[1]['type'] == Constants::TYPE_ATTRIBUTE)
                    {
                        $placesCoordinatesAttr = new Attribute($paramsFunc[1]['value'], $paramsFunc[1]['props']['name'], 1, false, false);
                    }

                    if ($paramsFunc[2]['type'] == Constants::TYPE_STRING)
                    {
                        $userRadiusAttr = new Attribute($paramsFunc[2]['value'], '', 1, false, false);
                    } else if ($paramsFunc[2]['type'] == Constants::TYPE_ATTRIBUTE)
                    {
                        $userRadiusAttr = new Attribute($paramsFunc[2]['value'], $paramsFunc[2]['props']['name'], 1, false, false);
                    }

                    if ($paramsFunc[3]['type'] == Constants::TYPE_STRING)
                    {
                        $userCoordinatesAttr = new Attribute($paramsFunc[3]['value'], '', 1, false, false);
                    } else if ($paramsFunc[3]['type'] == Constants::TYPE_ATTRIBUTE)
                    {
                        $userCoordinatesAttr = new Attribute($paramsFunc[3]['value'], $paramsFunc[3]['props']['name'], 1, false, false);
                    }

                    $result = Geo::getUsersWithinRadius($places, $placesCoordinatesAttr, $userRadiusAttr, $userCoordinatesAttr);
                    $users = [];
                    foreach ($result['data'] as $user) {
                        $class = $user->getType();
                        $users[] = [
                            'type' => Constants::TYPE_OBJECT,
                            'value' => $user->getUid(),
                            'ps' => $position['ps'],
                            'pf' => $position['pf'],
                            'rows' => $position['rows'],
                            'props' => [
                                'objClass' => !empty($class) ? $class->getuid() : null,
                                'name' => $user->getName(),
                            ]
                        ];
                    }
                    return [
                        'type' => Constants::TYPE_ARRAY,
                        'value' => $users,
                        'ps' => $position['ps'],
                        'pf' => $position['pf'],
                        'rows' => $position['rows'],
                    ];
                }
            }
        }
    }
}