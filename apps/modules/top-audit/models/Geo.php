<?php

namespace Dzeta\Modules\TopAudit\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core\Instance;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Geo extends Model
{
	 const PROCEDURES = [
        'getUsersWithinRadius' => [
            'name' => 'Get_Users_Within_Radius',
            'params' => [
                'Objs' => [ 'type' => DataType::TYPE_ARRAY ],
                'AttrIDOfObjCoordinates' => [ 'type' => DataType::TYPE_STRING ],
                'AttrIDOfUserRadius' => [ 'type' => DataType::TYPE_STRING ],
                'AttrIDOfUserCoordinates' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ]
    ];


    public static function getUsersWithinRadius(array $placesObjects, Attribute $placeCoordinatesAttr, Attribute $userRadiusAttr, Attribute $userCoordinatesAttr) {
        $result = [
            'result' => Db::INITIAL_CODE,
            'error' => [],
            'data' => [],
        ];
        $objs = [];
        foreach ($placesObjects as $place) {
            $objs[] = $place->getUid();
        }
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Objs' => $objs,
            'AttrIDOfObjCoordinates' => $placeCoordinatesAttr->getUid(),
            'AttrIDOfUserRadius' => $userRadiusAttr->getUid(),
            'AttrIDOfUserCoordinates' => $userCoordinatesAttr->getUid(),
        ]);

        if (!empty($data)) {
            $result['result'] = Db::SUCCESS_CODE;
            foreach ($data as $value) {
                $result['data'][$value['Obj']] = isset($result['data'][$value['Obj']]) ? $result['data'][$value['Obj']] : ['place' => new Instance($value['Obj'], ''), 'users' => []];
                $result['data'][$value['Obj']]['users'][] = new Instance($value['User'], '');
            }
        }
        if ($result['result'] != Db::SUCCESS_CODE) {
            $result['error'][] = [
                'text' => DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result['result'])
            ];
        }
        return $result;
    }
}