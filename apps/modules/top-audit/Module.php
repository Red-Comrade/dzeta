<?php

namespace Dzeta\Modules\TopAudit;

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

define('TOPAUDIT_MODULE_PATH', __DIR__);

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers the module auto-loader
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();
        $config = $di->getConfig();

        $loader->registerNamespaces(
            [
                'Dzeta\Modules\TopAudit\Controllers' => TOPAUDIT_MODULE_PATH . '/controllers/',
                'Dzeta\Modules\TopAudit\Models' => TOPAUDIT_MODULE_PATH . '/models/',
                'Dzeta\Modules\TopAudit\Helpers' => TOPAUDIT_MODULE_PATH . '/helpers',
                'Dzeta\Modules\TopAudit\Library' => TOPAUDIT_MODULE_PATH . '/library',
            ]
        );

        $loader->register();
    }

    /**
     * Registers the module-only services
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {

        /**
         * Setting up the view component
         */
        $di->setShared('view', function () {
            $view = new View();
            $view->setDI($this);
            $view->setViewsDir(TOPAUDIT_MODULE_PATH . '/views/');
            return $view;
        });
    }
}