<?php
// declare(strict_types=1);

namespace Dzeta\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{

    function initialize() {
      ini_set('error_reporting', E_ALL);
      ini_set('display_errors', "1");
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher) {
      $continue = true;
      if (!$this->session->has('logged')) {
          if(!$this->checkGuestAction($dispatcher)) {
              $this->view->setVar('logout', '1');
              //$this->response->redirect('login');
              $continue = true;
          }
      }
      else if (!$this->checkPermissions($dispatcher)) {
        echo json_encode(['result' => -100,'data'=>[],'errors'=>[["text"=>'np prava']]]);
      //  header('HTTP/1.0 404 Not Found', true);
        $continue = false;
      }

      return $continue;
    }

    //TODO: Real permissions check
    function checkPermissions($dispatcher) {
    //  return true;
      $user = $this->session->get('logged')['User'];
      $config = \Phalcon\Di::getDefault()->getShared('config');
      $controllers=$config->permission;
      $contrl=strtolower($dispatcher->getControllerName());
      $action=strtolower($dispatcher->getActionName());
      $flag=false;
      $allControllers=$user->getPermissionSession('controller');
      $arrControo=array_keys($allControllers);
      $keyContrl=array_search($contrl,$arrControo);
      if($keyContrl!==false)
      {
        if(is_null($allControllers[$arrControo[$keyContrl]]))
        {
            return true;
        }elseif(in_array($action,$allControllers[$arrControo[$keyContrl]]))
        {
          return true;
        }

      }
      return false;
    }

    function checkGuestAction($dispatcher) {
        $contrl=strtolower($dispatcher->getControllerName());
        $action=strtolower($dispatcher->getActionName());

        if($contrl == "application") {
            if($action == "getviews") {
                return true;
            }
            else if($action == "getattributeview") {
                return true;
            }
        }
        else if($contrl  == "view") {
            if($action == "gettabledata") {
                return true;
            }
            else if($action == "getwindowdata") {
                return true;
            }
        }
        else if($contrl  == "attrgroup") {
            if($action == "getattributes") {
                return true;
            }
        }
        else if($contrl  == "login") {
            if($action == "registration") {
                return true;
            }
        }
        else if($contrl  == "keysso") {
            if($action == "query" || $action == 'fill' || $action == 'tops') {
                return true;
            }
        }
        else if($contrl == "index") {
            if($action == "getmaininfo") {
                return true;
            }
        }else if($contrl=='object')
        {
          if($action=='downloadfile')
          {
           
            $paramsKey=$this->request->get('keyAPI');   
            $configApp = new  \Phalcon\Config\Adapter\Ini(BASE_PATH . '/config/Appconfig.ini');
            $configApp =$configApp->get('keys')->toArray();
            if(isset($configApp['dwnloadFile']))
            {
              if($paramsKey==$configApp['dwnloadFile'])
              {
                return true;
              }
            }
          }
        }

        return false;
    }

}
