<?php

use Phalcon\Cli\Task;
use Phalcon\Config;
use Phalcon\Config\Adapter\Json;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\User\User;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Value\DateTimeV;

class FetchTask extends Task
{
	const CONSTANT_PLACEMENT_BEFORE = 'before';
	const CONSTANT_PLACEMENT_AFTER = 'after';

	private $results = [];

    public function runAction(?string $entry = null) {
		$config = new Json(BASE_PATH . '/config/FetchConfig.json');
        if (isset($entry)) {
        	if ($config->has($entry)) {
        		$entryConfig = $config->get($entry);
        		$get_start = microtime(true);
        		$url = $entryConfig['url'];
        		if (!empty($entryConfig['variables'])) {
        			foreach ($entryConfig['variables'] as $key => $variable) {
        				$value = $this->processVariable($variable);
        				$url = $this->replaceVariable($url, $key, $value);
        			}
        		}
        		$data = file_get_contents($url);
				$data = str_replace("\xEF\xBB\xBF", '', $data);
        		$data = json_decode($data, true);
        		$data = empty($data) ? [] : $data;
        		if (!empty($entryConfig['data'])) {
        			$data = $this->getArrayElement($data, $entryConfig['data']->toArray(), []);
        		}
        		echo 'count: ' . count($data) . PHP_EOL;
        		foreach ($data as $key => $values) {
        			$start = microtime(true);
        			foreach ($entryConfig['classes'] as $class) {
			            $this->createObjects($class, $values, []);
        			}
        			echo 'key: ' . $key . ' time: ' . (microtime(true) - $start) . PHP_EOL;
        		}
        		if (!empty($entryConfig['hooks'])) {
        			if (!empty($entryConfig['hooks']['end'])) {
        				$context = stream_context_create([
				    		'http' => [
				    			'method' => 'POST',
				    			'header'  => 'Content-Type: application/x-www-form-urlencoded',
				    			'content' => http_build_query([
				    				'results' => $this->results
				    			])
				    		]
				    	]);
				    	file_get_contents($entryConfig['hooks']['end'], false, $context);
        			}
        		}
        	}
        }
    }

    private function createObjects(Phalcon\Config $class, array $values, array $fields) {
		$_attr = new Attr();
        $_obj = new Obj();
    	$attrs = [];
        foreach ($class['attributes'] as $attribute) {
			if (!empty($attribute['attribute'])) {
				$attr = new Attribute($attribute['attribute'], '', 0, false, false);
				if (!empty($attribute['objects_matching'])) {
					$attr->setDopSett('fetchObjectsMatching', $attribute['objects_matching']);
				}
				if (!empty($attribute['convert'])) {
					$attr->setDopSett('fetchConvert', $attribute['convert']);
				}
				if (!empty($attribute['array_delimiter'])) {
					$attr->setDopSett('fetchArrayDelimiter', $attribute['array_delimiter']);
				}
				if (!empty($attribute['constant'])) {
					$attr->setDopSett('fetchConstant', $attribute['constant']);
				}
				if (!empty($attribute['default_value'])) {
					$attr->setDopSett('fetchDefaultValue', $attribute['default_value']);
				}
				if (!empty($attribute['search'])) {
					$attr->setDopSett('fetchSearch', $attribute['search']);
				}
				if (!empty($attribute['value_convert'])) {
					$attr->setDopSett('fetchValueConvert', $attribute['value_convert']);
				}
				if (!empty($attribute['value_convert_data'])) {
					$attr->setDopSett('fetchValueConvertData', $attribute['value_convert_data']);
				}
				$attr->setDopSett('fetchField', $attribute['key']);
				$attr->setDopSett('fetchValueField', $attribute['valueKey']);
				$attrs[$attribute['attribute']] = $attr;
			}
			$_fields = $this->mergeFields($fields, $attribute['key']);
			$value = $this->getArrayElement($values, $_fields);
			if (!is_null($value)) {
				if (!empty($attribute['convert'])) {
					$convertData = isset($attribute['convert_data']) ? $attribute['convert_data'] : null;
					if ($attribute['convert'] instanceof Config) {
						foreach ($attribute['convert'] as $converter) {
							$value = $this->convert($value, $converter, $convertData);
						}
					} else {
						$value = $this->convert($value, $attribute['convert'], $convertData);
					}
					$values = $this->setArrayElement($values, $_fields, $value);
				}
			}
        }
		$_attr->getAttributesByList($attrs);
		foreach ($attrs as $attr) {
			$field = $attr->getDopSett('fetchField');
			$valueField = $attr->getDopSett('fetchValueField');
			$_fields = $this->mergeFields($fields, $field);
			$_fields = $this->mergeFields($_fields, $valueField);
			$value = $this->getArrayElement($values, $_fields);
			$this->processValue($value, $attr, $values);
		}
		if (!empty($attrs)) {
			if (is_array($class['object'])) {
				$objectFields = $class['object'];
			} else {
				$objectFields = $fields;
				$objectFields[] = $class['object'];
			}
			$uid = $this->getArrayElement($values, $objectFields);
			$uid = !is_null($uid) ? $uid : '';
			if (!empty($class['parent_search'])) {
				if (!is_null($class['parent'])) {
					if ($class['parent'] instanceof Phalcon\Config) {
						$parentFields = $class['parent']->toArray();
					} else {
						$parentFields = $fields;
						$parentFields[] = $class['parent'];
					}
					$parent = $this->getArrayElement($values, $parentFields);
				} else {
					$parent = $values;
				}
				$parent = $this->searchParent($class['parent_search'], $parent);
			} else {
				if ($class['parent'] instanceof Phalcon\Config) {
					$parentFields = $class['parent']->toArray();
				} else {
					$parentFields = $fields;
					$parentFields[] = $class['parent'];
				}
				$parent = $this->getArrayElement($values, $parentFields);
			}
			$parent = !is_null($parent) ? $parent : Instance::INSTANCE_ROOT;
			$obj = new Instance($uid, '');
            $obj->setType(new Core\Type\Type($class['class'], ''))->setParent(new Instance($parent, ''));
			$obj->setAttributes($attrs);
            $result = $_obj->checkUniqueAndAddWithValues($obj, Instance::ADDED_BY_EXTERNAL_SOURCE, new User(User::SYSTEM_USER_UID, ''));
            if (!empty($obj->getUid()) && $result == 2) {
        		$result = $_obj->addValues([$obj], new User(User::SYSTEM_USER_UID, ''));
	        } else if ($result != 1) {
	        	if (!empty($class['hook_id'])) {
	        		if (is_array($class['object'])) {
						$hookFields = $class['hook_id'];
					} else {
						$hookFields = $fields;
						$hookFields[] = $class['hook_id'];
					}
					$hookId = $this->getArrayElement($values, $hookFields);
					if (!empty($hookId)) {
		            	$this->results[] = ['id' => $hookId, 'result' => $result];
					}
	        	}
	        }
	        unset($obj, $_obj, $parent);
		}
		foreach ($class['attributes'] as $attribute) {
			if (!empty($attribute['classes'])) {
				$_fields = $this->mergeFields($fields, $attribute['key']);
				$_fields = $this->mergeFields($_fields, $attribute['valueKey']);
				$value = $this->getArrayElement($values, $_fields);
				if (is_array($value)) {
        			foreach ($attribute['classes'] as $c) {
						if ($this->isAssociative($value)) {
							$this->createObjects($c, $values, $_fields);
						} else {
							foreach ($value as $key => $v) {
								$__fields = $this->mergeFields($_fields, $key);
								$this->createObjects($c, $values, $__fields);
							}
						}
					}
				}
			}
		}
		unset($class, $attrs, $values, $fields, $_attr);
	}

    private function searchParent($parent, $values) {
		$search = [];
		foreach ($parent['search'] as $attribute) {
			$value = $this->getArrayElement($values, $attribute['value']->toArray());
			$convertData = isset($attribute['convert_data']) ? $attribute['convert_data'] : null;
			if ($attribute['convert'] instanceof Config) {
				foreach ($attribute['convert'] as $converter) {
					$value = $this->convert($value, $converter, $convertData);
				}
			} else {
				$value = $this->convert($value, $attribute['convert'], $convertData);
			}
			if (!empty($attribute['valueKey'])) {
				$value = $this->getArrayElement($value, $attribute['valueKey']->toArray(), $value);
			}
			$search[] = '[' . $attribute['attribute'] . ']' . $attribute['sign'] . '#' . $value . '#';
		}
		$_obj = new Obj();
		$class = new \Dzeta\Core\Type\Type($parent['class'], '');
		$parents = [];
		$objs = $_obj->searchObj($class, implode(' AND ', $search), $parents, 1);
		if (count($objs['data']) == 1) {
			return $objs['data'][0]->getUid();
		}
		return null;
    }

    private function searchObject($values, Attribute $attr) {
    	$object = null;
    	$datatype = $attr->getDatatype();
    	if ($datatype == DataType::TYPE_OBJECT) {
    		$class = $attr->getLinkType();
    		$search = $attr->getDopSett('fetchSearch');
    		$filter = [];
    		foreach ($search as $attribute) {
    			$fields = [$attribute['key']];
    			if (!empty($attribute['valueKey'])) {
	    			$fields = array_merge($fields, $attribute['valueKey']->toArray());
    			}
    			$v = $this->getArrayElement($values, $fields);
    			if (!is_array($v)) {
	    			$filter[] = '[' . $attribute['attribute'] . ']' . $attribute['sign'] . '#' . $v .'#';
    			}
    		}
    		$_obj = new Obj();
			$class = new \Dzeta\Core\Type\Type($class->getUid(), '');
			$parents = [];
			$objs = $_obj->searchObj($class, implode(' AND ', $filter), $parents, 1);
			if (count($objs['data']) == 1) {
				$object = $objs['data'][0]->getUid();
			}
    	}
    	return $object;
    }

    private function processValue($fetchValue, Attribute $attr, $values) {
    	$objectsMatching = $attr->getDopSett('fetchObjectsMatching');
		$convert = $attr->getDopSett('fetchConvert');
		$valueConvert = $attr->getDopSett('fetchValueConvert');
		$valueConvertData = $attr->getDopSett('fetchValueConvertData');
		$constant = $attr->getDopSett('fetchConstant');
		$defaultValue = $attr->getDopSett('fetchDefaultValue');
		$search = $attr->getDopSett('fetchSearch');
		if (!empty($valueConvert)) {
			$fetchValue = $this->convert($fetchValue, $valueConvert, $valueConvertData);
		}
		if (is_null($fetchValue) || $fetchValue == '') {
			$fetchValue = $defaultValue;
		}
		if ($attr->isArray()) {
			$arrayDelimiter = $attr->getDopSett('fetchArrayDelimiter');
			if (!is_null($arrayDelimiter) && is_string($fetchValue)) {
				$fetchValue = explode($arrayDelimiter, $fetchValue);
			}
			if (is_array($fetchValue)) {
				$arrayValues = [];
				foreach ($fetchValue as $value) {
					if (!empty($objectsMatching)) {
						if (!is_null($value)) {
							$uid = $objectsMatching->get($value);
							if (!is_null($uid)) {
								$arrayValues[] = ['value' => $uid->get('uid')];
							}
						}
					} else {
						$arrayValues[] = ['value' => $this->processConstantValue($value, $constant)];
					}
				}
				$attr->setValue(['value' => $arrayValues]);
			}
		} else {
			if (!empty($search)) {
				$fetchValue = $this->searchObject($values, $attr);
			}
			if (!empty($objectsMatching)) {
				if (!is_null($fetchValue)) {
					$uid = $objectsMatching->get($fetchValue);
					if (!is_null($uid)) {
						$attr->setValue(['value' => $uid->get('uid')]);
					}
				}
			} else {
				$attr->setValue(['value' => $this->processConstantValue($fetchValue, $constant)]);
			}
		}
    }

    private function processConstantValue($value, $constant) {
    	if (!is_null($value) && !empty($constant)) {
    		if ($constant['placement'] == self::CONSTANT_PLACEMENT_BEFORE) {
    			$value = $constant['value'] . $value;
    		} else if ($constant['placement'] == self::CONSTANT_PLACEMENT_AFTER) {
    			$value .= $constant['value'];
    		}
    	}
    	return $value;
    }

    private function convert($value, $converter, $data = null) {
    	$converted = $value;
    	switch ($converter) {
			case 'timestamp_to_date':
				$converted = date(DateTimeV::DATE_FORMAT, $value);
				break;
			case 'string_to_date':
				$format = isset($data['format']) ? $data['format'] : 'd.m.Y';
				$date = date_create_from_format($format, $value);
				$converted = $date->format(DateTimeV::DATE_FORMAT);
				break;
			case 'add_hours':
				if (!empty($data['count'])) {
					$date = date_create_from_format(DateTimeV::DATE_FORMAT, $value);
					$interval = date_interval_create_from_date_string($data['count'] . ' hours');
					$date->add($interval);
					$converted = $date->format(DateTimeV::DATE_FORMAT);
				}
				break;
			case 'json':
				$converted = json_decode($value, true);
				break;
			case 'to_json':
				$converted = !is_null($value) ? json_encode($value) : $value;
				break;
			case 'keys_to_array':
				$converted = [];
				$keys = array_keys($value);
				foreach ($keys as $key) {
					$converted[] = ['key' => $key, 'value' => $value[$key]];
				}
				break;
			case 'array_value_to_true_keys_array':
				if (is_array($value)) {
					foreach ($converted as $key => $val) {
						if (isset($val['value']) && is_array($val['value'])) {
							$converted[$key]['value'] = [];
							foreach ($val['value'] as $k => $v) {
								if ($v) {
									$converted[$key]['value'][] = $k;
								}
							}
						}
					}
				}
				break;
		}
		return $converted;
    }

    private function processVariable($variable) {
    	$value = null;
    	switch ($variable['type']) {
    		case 'date':
    			$date = date_create($variable['value']);
    			if ($date) {
    				$value = $date->format($variable['format']);
    			}
    			break;
    	}
    	return $value;
    }

    private function replaceVariable($string, $key, $value) {
    	return str_replace('{{' . $key . '}}', $value, $string);
    }

    private function mergeFields(array $firstFileds, $secondFields) {
    	$fields = $firstFileds;
    	if ($secondFields instanceof Phalcon\Config) {
    		foreach ($secondFields as $field) {
    			$fields[] = $field;
    		}
    	} else {
    		if (!is_null($secondFields)) {
    			$fields[] = $secondFields;
    		}
    	}
    	return $fields;
    }

    private function isAssociative(array $array) {
	    if (array() === $array) return false;
	    return array_keys($array) !== range(0, count($array) - 1);
	}

	private function getArrayElement(array $array, array $keys, $default = null) {
        foreach ($keys as $key) {
            if (!is_array($array)) {
                return $default;
            }
            if (array_key_exists($key, $array)) {
                $array = $array[$key];
            } else {
                return $default;
            }
        }
        return $array;
	}

	private function setArrayElement(array $array, array $keys, $value) {
        $result = $array;

        if (empty($keys)) {
            return $result;
        }

        $tmp = &$result;

        while (count($keys) > 0) {
            $key = array_shift($keys);
            if (!is_array($tmp)) {
                $tmp = [];
            }
            if ($key === '[]') {
                $tmp[] = null;
                end($tmp);
                $key = key($tmp);
            }
            $tmp = &$tmp[$key];
        }
        $tmp = $value;

        return $result;
    }
}