<?php

use Phalcon\Cli\Task;
use Phalcon\Config;
use Phalcon\Config\Adapter\Json;



class QueueTask extends Task
{
    public function calcAction(int $numFlow) {

    	$_queue=new Queue();
    	$dataQueue=$_queue->queueGetForCalc($numFlow);
    	//$dataResult['data'][]=['calc'=>$calc,'obj'=>$obj,'user'=>$user,'id'=>$value['id']];
    	$finData=[];

    	foreach ($dataQueue['data'] as $key => $value)
    	{
    		$t=microtime(true);
    		$dzeta = new DzetaCalcParser\DzetaCalc([
                'formula' => $value['calc']->getFormula(),
                'object' => $value['obj'],
                'user' => $value['user']
            ]);
            echo json_encode($dzeta);
            echo PHP_EOL;
    		$finData[]=['t'=>(microtime(true)-$t)*1000,'id'=> $value['id']];
    	}
    	if(!empty($finData))
    	{
    		$_queue->queueFinCalc($finData,5);
    	}
    	echo json_encode(1);

    }
}