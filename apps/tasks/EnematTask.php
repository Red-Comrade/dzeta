<?php

use Phalcon\Cli\Task;
use Phalcon\Config;
use Phalcon\Config\Adapter\Json;



class EnematTask extends Task
{
	public function test1Action($pathFile)
	{
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/enematGlobal.json');
		$start=microtime(true);
		$time=[];
		if(!file_exists($pathFile))
		{
			return;
		}
		$readFile=file_get_contents($pathFile);
		$sett=json_decode($readFile,true);
		//теперь скачиваем файл
		$fileGoogle=$sett['path'].$sett['id'].'_g.docx';
        $googleTry=0;
        while (true) 
        {
            $res=\FillFile::dwnlFileGoogleP($sett['url'],$fileGoogle);
            $googleTry++;
            if($res)
            {
                break;
            }
            if($googleTry>125)
            {
                break;
            }
            usleep(5000);
        }
		$time['googleTry']=$googleTry;
		$time['dwnlGoogle']=microtime(true)-$start;
		$start=microtime(true);
		if(!(file_exists($fileGoogle)&&filesize($fileGoogle)>0))
		{
            $fileJSON=fopen($sett['path'].$sett['id'].'_t.txt', "w");
            fwrite($fileJSON, json_encode($time));
            fclose($fileJSON);
			return;
		}
		$fileFinal=$sett['path'].$sett['id'].'_f.docx';
		FillFile::replaceMetkaDocxP($fileGoogle,$sett['data'],$fileFinal);
		$time['replace']=microtime(true)-$start;
		$start=microtime(true);
		
        chmod($fileFinal, 0777);
        $countP=0;
        $filePDF='';
        while(1)
        {
        	//$cmd='libreoffice --headless --convert-to pdf  --outdir '.$sett['path'].' '."$fileFinal";
          //$cmd="unoconv -f pdf $fileFinal"; c:\Users\PussHP\Downloads\  6_PAGEINTRODUCTIONV2.docx
            //break;
            $filePDF=str_replace('.docx', '.pdf',$fileFinal );

            //$cmd='/var/www/enemat.misnik.by/doc2pdf/build/doc2pdf '.$fileFinal.' '.$filePDF;
            $url = "https://enemat.misnik.by/enemat/hooks/convertDocxPDF";
            $post_data =
            [
                "key" =>'' ,
                'file'=>base64_encode(file_get_contents($fileFinal))
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            $output = curl_exec($ch);
            curl_close($ch);
            $res=json_decode($output,true);
            if($res['result']==1)
            {
                $handle = fopen($filePDF, "w");
                fwrite($handle, base64_decode($res['file']));
                fclose($handle);
                break;
            }else if($res['result']==-1)
            {
                $time['err']=$res['out'];
                $time['cmd']=$res['cmd'];
            }
            
            /*$cmd='cd '.BASE_PATH . "/../".'doc2pdf &&'.BASE_PATH . "/../".'doc2pdf/build/doc2pdf '.$fileFinal.' '.$filePDF;
            $cmd=BASE_PATH . "/../".'doc2pdf/build/doc2pdf '.$fileFinal.' '.$filePDF;
        	$output = exec($cmd); */
            
        	$countP++; 
            
            if($countP>10)
            {
                $time['nogen']=1;
                break;
            }
            usleep(5000);
        }
        if(file_exists($filePDF))
        {
            $time['fileSize']=filesize($filePDF);
        }
       
        
        $time['outputCount']=$countP;        
        $time['convert']=microtime(true)-$start;
       
        $fileJSON=fopen($sett['path'].$sett['id'].'_t.txt', "w");
        fwrite($fileJSON, json_encode($time));
        fclose($fileJSON);
	}
    

    public function genLibraAction($txtPath)
    {
        $start=microtime(true);
        $time=[];
        if(!file_exists($txtPath))
        {
            $fileJSON=fopen($sett['folderMain'].'/err.txt', "w");
            fclose($fileJSON);
            return;
        }
        $readFile=file_get_contents($txtPath);
        $sett=json_decode($readFile,true);
        //['data' 'urlFile' 'pathTXT' 'id' 'folderMain']

        $fileGoogle_Path=$sett['folderMain'].'/'.$sett['id'].'_g.docx';
        $res=\FillFile::dwnlFileGoogleP($sett['urlFile'],$fileGoogle_Path);
        $time['dwnlGoogle']=microtime(true)-$start;
        
        if(empty($res))
        {
            $fileJSON=fopen($sett['folderMain'].'/err.txt', "w");
            fclose($fileJSON);
        }

        $start=microtime(true);
        $fileFinal_Path=$sett['folderMain'].'/'.$sett['id'].'_f.docx';
        FillFile::replaceMetkaDocxP_new($fileGoogle_Path,$sett['data'],$fileFinal_Path);
        $time['replace']=microtime(true)-$start;

        $start=microtime(true);
        $cmd='libreoffice --headless --convert-to pdf  --outdir '.$sett['folderMain'].' '."$fileFinal_Path";
        $cmdOut=exec($cmd);
        $time['libra']=microtime(true)-$start;
        $time['cmd']=$cmd;
        $time['output']=$cmdOut;
        $fileJSON=fopen($sett['folderMain'].'/'.$sett['id'].'_t.txt', "w");
        fwrite($fileJSON, json_encode($time));
        fclose($fileJSON);
    }


    public function generateDocAction($pathFile)
    {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/enematGlobal.json');
        $start=microtime(true);
        $time=[];

        if(!file_exists($pathFile))
        {            
            return;
        }
        $readFile=file_get_contents($pathFile);
        $sett=json_decode($readFile,true);
        //теперь скачиваем файл
        $fileGoogle=$sett['path'].$sett['id'].'_g.docx';
        $googleTry=0;

        while (true) 
        {
            $res=\FillFile::dwnlFileGoogleP($sett['url'],$fileGoogle);
            $googleTry++;
            if($res)
            {
                break;
            }
            if($googleTry>125)
            {
                break;
            }
            usleep(5000);
        }
        $time['googleTry']=$googleTry;
        $time['dwnlGoogle']=microtime(true)-$start;
        $start=microtime(true);
        if(!(file_exists($fileGoogle)&&filesize($fileGoogle)>0))
        {
            $fileJSON=fopen($sett['path'].$sett['id'].'_t.logt', "w");
            fwrite($fileJSON, json_encode($time));
            fclose($fileJSON);
            return;
        }
        $fileFinal=$sett['path'].$sett['id'].'_f.docx';
        //\FillFile::replaceMetkaDocxP($fileGoogle,$sett['data'],$fileFinal);
        \FillFile::replaceMetkaDocxP($fileGoogle,$sett['data'],$fileFinal);
        //replaceMetkaDocxP_new
        $time['replace']=microtime(true)-$start;
        $start=microtime(true);
        
        chmod($fileFinal, 0777);
        $countP=0;
        $filePDF=str_replace('.docx', '.pdf',$fileFinal);
        if($config['modeConvert']=='uLibra')
        {                
            while(true)
            {
                $cmd='libreoffice --headless --convert-to pdf  --outdir '.$sett['path'].' '.$fileFinal;
                exec($cmd);
                if(file_exists($filePDF)&&filesize($filePDF)>0)
                {
                    break;
                }
                usleep(5000);
                $countP++;
                if($countP>30)
                {
                    break;
                }
            } 
        }else if($config['modeConvert']=='acermi')
        {
            while(true)
            {
                $cmd=BASE_PATH . "/../".'doc2pdf/build/doc2pdf '.$fileFinal.' '.$filePDF;
                exec($cmd);
                if(file_exists($filePDF)&&filesize($filePDF)>0)
                {
                    break;
                }
                usleep(5000);
                $countP++;
                if($countP>30)
                {
                    break;
                }
            }
        }else if($config['modeConvert']=='Racermi')
        {
            $post_data =
            [
                "key" => $config['keyForConvertdocx'],
                "uniqID"=>$sett['id'],
                'file'=>base64_encode(file_get_contents($fileFinal))
            ];
            while(true)
            {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $config['urlRemoteGenerate']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                $output = curl_exec($ch);
                curl_close($ch);
                $res=json_decode($output,true);
                if($res['result']==1)
                {
                    $handle = fopen($filePDF, "w");
                    fwrite($handle, base64_decode($res['file']));
                    fclose($handle);
                    break;
                }
                usleep(5000);
                $countP++;
                if($countP>30)
                {
                    break;
                }
            } 
        }
            
        if(file_exists($filePDF))
        {
            $time['fileSize']=filesize($filePDF);
        }
       
        
        $time['outputCount']=$countP;        
        $time['convert']=microtime(true)-$start;
       
        $fileJSON=fopen($sett['path'].$sett['id'].'_t.logt', "w");
        fwrite($fileJSON, json_encode($time));
        fclose($fileJSON);
    }

    public function generateDocnewAction($pathFile)
    {
        $config = new Json(BASE_PATH . '/config/dop/DataEnemat/enematGlobal.json');
        $start=microtime(true);
        $time=[];

        if(!file_exists($pathFile))
        {            
            return;
        }
        $readFile=file_get_contents($pathFile);
        $sett=json_decode($readFile,true);
        //теперь скачиваем файл
        $fileGoogle=$sett['path'].$sett['id'].'_g.docx';
        $googleTry=0;

        while (true) 
        {
            $res=\FillFile::dwnlFileGoogleP($sett['url'],$fileGoogle);
            $googleTry++;
            if($res)
            {
                break;
            }
            if($googleTry>125)
            {
                break;
            }
            usleep(5000);
        }
        $time['googleTry']=$googleTry;
        $time['dwnlGoogle']=microtime(true)-$start;
        $start=microtime(true);
        if(!(file_exists($fileGoogle)&&filesize($fileGoogle)>0))
        {
            $fileJSON=fopen($sett['path'].$sett['id'].'_t.txt', "w");
            fwrite($fileJSON, json_encode($time));
            fclose($fileJSON);
            return;
        }
        $fileFinal=$sett['path'].$sett['id'].'_f.docx';
        //\FillFile::replaceMetkaDocxP($fileGoogle,$sett['data'],$fileFinal);
        \FillFile::replaceMetkaDocxP_new($fileGoogle,$sett['data'],$fileFinal);
        //
        $time['replace']=microtime(true)-$start;
        $start=microtime(true);
        
        chmod($fileFinal, 0777);
        $countP=0;
        $filePDF=str_replace('.docx', '.pdf',$fileFinal);
        if($config['modeConvert']=='uLibra')
        {                
            while(true)
            {
                $cmd='libreoffice --headless --convert-to pdf  --outdir '.$sett['path'].' '.$fileFinal;
                exec($cmd);
                if(file_exists($filePDF)&&filesize($filePDF)>0)
                {
                    break;
                }
                usleep(5000);
                $countP++;
                if($countP>30)
                {
                    break;
                }
            } 
        }else if($config['modeConvert']=='acermi')
        {
            while(true)
            {
                $cmd=BASE_PATH . "/../".'doc2pdf/build/doc2pdf '.$fileFinal.' '.$filePDF;
                exec($cmd);
                if(file_exists($filePDF)&&filesize($filePDF)>0)
                {
                    break;
                }
                usleep(5000);
                $countP++;
                if($countP>30)
                {
                    break;
                }
            }
        }else if($config['modeConvert']=='Racermi')
        {
            $post_data =
            [
                "key" => $config['keyForConvertdocx'],
                'file'=>base64_encode(file_get_contents($fileFinal))
            ];
            while(true)
            {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $config['urlRemoteGenerate']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                $output = curl_exec($ch);
                curl_close($ch);
                $res=json_decode($output,true);
                if($res['result']==1)
                {
                    $handle = fopen($filePDF, "w");
                    fwrite($handle, base64_decode($res['file']));
                    fclose($handle);
                    break;
                }
                usleep(5000);
                $countP++;
                if($countP>30)
                {
                    break;
                }
            } 
        }
            
        if(file_exists($filePDF))
        {
            $time['fileSize']=filesize($filePDF);
        }
       
        
        $time['outputCount']=$countP;        
        $time['convert']=microtime(true)-$start;
       
        $fileJSON=fopen($sett['path'].$sett['id'].'_t.logt', "w");
        fwrite($fileJSON, json_encode($time));
        fclose($fileJSON);
    }
}