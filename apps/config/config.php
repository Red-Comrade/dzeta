<?php

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/apps');
use Phalcon\Config\Adapter\Ini;
use Phalcon\Db\Adapter\PdoFactory;

$configDB = new Ini(BASE_PATH . '/config/DBconfig.ini');
$configApp = new Ini(BASE_PATH . '/config/Appconfig.ini');
$allModulesArr=[
    //тут указываем список модулей, и разрешенных контроллеров, при желании можно и методы указать
    //модуль GLOBAL-> на него не назначаются права, и его контроллеры достпуны всем
    'modules' => [
        'users' => [
            'location'=>'index#/users',
            'controllers'=>
            [
              'user'=>[
                "changePassword"
              ],
              'permission'=>[
                "getModulesSystem",
                'addPermissionGroup',
                'getModulesSettGroup'
              ]
            ]
        ],
        'home' => [
                'location'=>'index#/home',
                'controllers'=>
                [
                    "index"=>null,
                    'application' => null,
                    'object' => null,
                    'type' => null, 'attr' => null, 'attrgroup' => null
                ]
         ],
        'classes' => [
            'location'=>'index#/classes',
            'controllers'=>
            [
              'type' => null, 'attr' => null, 'attrgroup' => null
            ]
        ],
        'objects' => [
            'location'=>'index#/objects',
            'controllers'=>
            [
              'type' =>null,
              'object' => null,
              'attrgroup' => ['getattributes'],
              'import' => ['getexceldata', 'run'],
              'attr' => ['getattributeall']
            ]
        ],
        'settApp' => [
            'location'=>'index#/application',
            'controllers'=>
            [
              'application' => null,
            ]
        ],
        'import' =>[
            'location'=>'index#/import',
            'controllers'=>
            ['import' => null, 'attr' => ['getattributeall']]
        ],
        'stepFromEnemat' => [
            'location'=>'index#/stepform',
            'controllers'=>
            [
              'type' =>null, 'object' => null, 'dop' => null,'attr'=>null,'attrgroup'=>null,'application'=>null,'calc'=>null
            ]
        ],
        'taxesEnemat' => [
            'location'=>'index#/taxes',
            'controllers'=>
            [
              'import' => null,
            ]
        ],
        'treeTableEnemat' => [
            'location'=>'index#/treetable',
            'controllers'=>
            [
              'type' =>null, 'object' => null, 'dop' => null,'attr'=>null,'attrgroup'=>null,'calc'=>null
            ]
        ],
        'stepFromEnematSettings' => [
            'location'=>'index#/stepformSetting',
            'controllers'=>
            [
              'type' =>null, 'object' => null, 'dop' => null,'attr'=>null,'attrgroup'=>null
            ]
        ],
        'GLOBAL' => [
            'controllers' => [
                'calc' => null,
                'index' => null,
                'login' => null,
                'progress' => null,
                'view' => null,
                'folder' => null,
                'keys_so' => null,
                'eview' => null,
            ]
        ]
    ]
];
$confModd=$configApp->get('modules')->toArray();
foreach ($allModulesArr['modules'] as $key => $value) {
    if($key=='GLOBAL') continue;
    if(!in_array($key,$confModd['module']))
    {
        unset($allModulesArr['modules'][$key]);
    }

}

return new \Phalcon\Config([
    'database' => $configDB->get('database')->toArray(),
    'application' => [
        'appDir'         => APP_PATH . '/',
        'coreDir'        => APP_PATH . '/core/',
        'controllersDir' => APP_PATH . '/controllers/',
        'tasksDir'       => APP_PATH . '/tasks/',
        'helpersDir'     => APP_PATH . '/helpers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'baseUri'        => '/',
    ],
    'dzeta' => $configApp->get('app')->toArray(),
    'permission' => $allModulesArr
]);
