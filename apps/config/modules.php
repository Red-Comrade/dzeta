<?php

use Phalcon\Config\Adapter\Json;

/**
 * Register application modules
 */

$modules = new Json(BASE_PATH . '/config/Modules.json');
foreach ($modules as $key => $value) {
    $modules[$key]['path'] = APP_PATH . $value['path'];
}

$application->registerModules($modules->toArray());