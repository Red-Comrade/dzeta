<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->libraryDir,
        $config->application->tasksDir,
        $config->application->helpers,
    ]
);
$loader->registerNamespaces(
  [
    'Dzeta\Core' => $config->application->coreDir,
    'DzetaCalcParser' => $config->application->libraryDir . '/DzetaCalc',
    'Dzeta\Models' => $config->application->modelsDir,
    'Dzeta\Controllers' => $config->application->controllersDir,
    'Dzeta\Helpers' => $config->application->helpersDir,
  ]
);
$loader->registerFiles(
  [
    BASE_PATH . '/vendor/php-console/php-console/src/PhpConsole/__autoload.php',
    BASE_PATH . '/vendor/autoload.php',
  ]
);
$loader->registerClasses(
    [
      'DZ' => $config->application->libraryDir . 'DZ.php',
      'Dzeta\Library\Db' => $config->application->libraryDir . 'Db.php',
      'Dzeta\Library\DbError' => $config->application->libraryDir . 'DbError/DbError.php',
      'MyTemplateProcessor'=> $config->application->libraryDir . 'phpWord/MyTemplateProcessor.php',
      'FillFile' => $config->application->libraryDir .'projFR/'. 'FillFile.php',
      'KeysSoLib' => $config->application->libraryDir .'KeysSo/'. 'KeysSoLib.php',
    ]
);
if (!$config->dzeta->useDB) {
  $loader->registerClasses(
      [
        'MockDb' => $config->application->libraryDir . 'MockDb.php',
      ]
  );
}
$loader->register();

\PhpConsole\Helper::register();
\PhpConsole\Connector::getInstance()->getDebugDispatcher()->setDumper(
    new PhpConsole\Dumper(10, 500) // set new dumper with levelLimit=10
);
