<?php

namespace Dzeta\Helpers;

use Dzeta\Core;

class ViewHelper
{
    public static function getViewFromRequest($request)
    {
        $view = new Core\App\View($request->get('view'), '', []);

        $mode = empty($request->get('mode')) ? 0 : 1;

        $ParentObj = $request->get('ParentObj');
        $ParentView = $request->get('ParentView');
        $SelectObjs = $request->get('SelectObjs');
        $header_data = $request->get('header_data');
        $exe_data_source_method = $request->get('exe_data_source_method');

        if (empty($mode)) {
            $uidSortAttr = $request->get('attr_sort');
            $view_sorts = $request->get('view_sorts');
            $typeSort = 1;
            if ($request->get('sort') !== null) {
                $typeSort = $request->get('sort');
            }
            $start = $request->get('start');
            $start = $start == 0 ? 1 : $start;

            $num = $request->get('num');
            $NumOfObjOfMainClass = $request->get('NumOfObjOfMainClass');

            $filter = null;
            if (!empty($request->get('attr_filter'))) {
                ViewHelper::parseFilterTable($request->get('attr_filter'), $filter);
            }

            $view->setDopSett('isNewMode', $request->get('isNewMode'));
            $view->setDopSett('filter', $filter);
            $view->setDopSett('attrSort', new Core\Type\Attribute($uidSortAttr, '', 1, false, false));
            $view->setDopSett('sort', $typeSort);
            $view->setDopSett('view_sorts', $view_sorts);

            $view->setDopSett('num', $num);
            $view->setDopSett('NumOfObjOfMainClass', $NumOfObjOfMainClass);
            $view->setDopSett('start', $start);

            $view->setDopSett('ParentView', $ParentView);
            $view->setDopSett('ParentObj', $ParentObj);
            $view->setDopSett('SelectObjs', $SelectObjs);
            $view->setDopSett('header_data', $header_data);
            $view->setDopSett('exe_data_source_method', $exe_data_source_method);
            $view->setDopSett('HatID', $request->get('HatID'));
        }
        else {
            $filter = null;
            $uidBattrAttr = $request->get('attr_border');
            $view->setDopSett('attrBorder', new Core\Type\Attribute($uidBattrAttr, '', 1, false, false));
            if (!empty($request->get('attr_filter'))) {
                $filter = [];
                foreach ($request->get('attr_filter') as $key => $value) {
                    $tmp = [
                        'op' => empty($value['op']) ? '=' : $value['op'],
                        'v' => $value['v']
                    ];
                    if (!empty($value['attr'])) {
                        $tmp['attr'] = $value['attr'];
                    }
                    $filter[] = $tmp;
                }
            }
            if (null !== $request->get('borderSearch')) {
                if (is_null($filter)) {
                    $filter = [];
                }
                $filter[] = ['attr' => $uidBattrAttr, 'op' => '~', 'v' => $request->get('borderSearch')];
            }
            $view->setDopSett('filter', $filter);
        }

        return $view;
    }

    public static function parseFilterTable($attr_filter, &$filter) {
        $filter = [];
        foreach ($attr_filter as $key => $value) {
            //тут нужно посмотреть
            if (is_array($value['v'])) {
                if($value['op'] == "ALL" || $value['op'] == "ANY") {
                    $tmp = [
                        'op' => $value['op'],
                        'v' => '{"'.implode("\",\"", $value['v']).'"}',
                    ];
                }
                else {
                    $tmp = [
                        'op' => 'IN',
                        'v' => "'" . implode("','", $value['v']) . "'",
                    ];
                }
            }
            else {
                if($value['op'] == "ALL" || $value['op'] == "ANY") {
                    $tmp = [
                        'op' => $value['op'],
                        'v' => '{"'.$value['v'].'"}',
                    ];
                }
                else {
                    $tmp = [
                        'op' => empty($value['op']) ? '~' : $value['op'],
                        'v' => $value['v'],
                    ];
                }
            }

            if (!empty($value['attr'])) {
                $tmp['attr'] = $value['attr'];
            }
            $filter[] = $tmp;
        }
    }


    public static function getLoaderHTML($config) {

        if($config->img_logo_min_alt == "Enemat") {
            return '
                <div id="splash-screen" class="kt-splash-screen ">
                    <svg style="width: 280px" class="enemat_loader_0"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 520 139">
                        <title>123</title>
                        <g id="Layer_2" data-name="Layer 2">
                            <rect className="cls-1" x="-257" y="-314.5" width="1024" height="768"/>
                        </g>
                        <g id="Layer_1" data-name="Layer 1">
                            <path class="cls-2 cls-a cls-a0" d="M102.71,19.51,55.19,6.84a5.17,5.17,0,0,0-6.34,3.67l-1.7,6.38L57,14.43a7.63,7.63,0,0,1,7.7,2.51l7.51,2a2.49,2.49,0,1,1-1.29,4.8l-4-1.06,1.84,7.37,8.57,2.29a2.09,2.09,0,0,1,.46.17h9.52a7.64,7.64,0,0,1,7.62,7.64V68.9l11.48-43.05A5.18,5.18,0,0,0,102.71,19.51Z"/>
                            <path class="cls-2 cls-a cls-a0" d="M63,21.65a5.17,5.17,0,0,0-6.27-3.77L9,29.82A5.18,5.18,0,0,0,5.24,36.1L19.11,91.51a76.8,76.8,0,0,1,9.49,2.95V85.53l-1.31.33A2.48,2.48,0,1,1,26.09,81l2.51-.63V73.55l-4.12,1a2.49,2.49,0,1,1-1.2-4.83l5.32-1.33V61.56l-7,1.74a2.49,2.49,0,0,1-1.2-4.83l8.15-2V49.57L18.83,52a2.49,2.49,0,0,1-1.21-4.83l11-2.74V40.15a7.39,7.39,0,0,1,.49-2.7L16,40.73A2.49,2.49,0,1,1,14.8,35.9l14.92-3.73a2.47,2.47,0,0,1,2.81,1.3,7.57,7.57,0,0,1,3.69-1h29.5Z"/>
                            <path class="cls-2 cls-a cls-a0" d="M86.34,35.57H37.16A5.18,5.18,0,0,0,32,40.75v55c5.26,2.16,9.21,4.17,9.21,4.17s3.14-2.25,8-5.56H41.3a2.49,2.49,0,0,1,0-5H56.52c3.2-2.13,6.69-4.4,10.26-6.65H41.3a2.49,2.49,0,1,1,0-5H74.91A124.25,124.25,0,0,1,91.52,69.2V40.75A5.18,5.18,0,0,0,86.34,35.57Zm-45,7.31H56.68a2.49,2.49,0,1,1,0,5H41.3a2.49,2.49,0,1,1,0-5Zm0,11.63H65.09a2.49,2.49,0,1,1,0,5H41.3a2.49,2.49,0,1,1,0-5ZM82.19,71.12H41.3a2.49,2.49,0,1,1,0-5H82.19a2.49,2.49,0,1,1,0,5Z"/>
                            <path class="cls-2 cls-a cls-a0" d="M115.54,59.48c-6.52,4.36-12.88,8.88-19,13.7s-12,9.87-17.68,15.14S67.77,99,62.67,104.77a179.42,179.42,0,0,0-14.21,17.81l-6.77,9.76-5.93-9.82c-.92-1.52-1.82-3.08-2.78-4.62s-1.95-3.05-3-4.49a106,106,0,0,0-6.77-8.63,93.85,93.85,0,0,0-7.83-7.88,59.13,59.13,0,0,0-9.07-6.77,62.45,62.45,0,0,1,11.22,3.13A103.54,103.54,0,0,1,28.36,98.1a115.78,115.78,0,0,1,10.34,6.15c1,.63,2,1.33,3,2l3.38-2.87c3.14-2.54,6.26-5.08,9.46-7.5,6.38-4.85,12.87-9.48,19.54-13.78S87.52,73.76,94.45,70,108.41,62.72,115.54,59.48Z"/>
                            <path class="cls-2 cls-a cls-a1" d="M182.33,22.67a13.53,13.53,0,0,1,5.12,3.82,17,17,0,0,1,2.74,4.6,15.18,15.18,0,0,1,1,5.74,16.35,16.35,0,0,1-2,7.65,11,11,0,0,1-6.48,5.32,10.16,10.16,0,0,1,5.35,4.32q1.57,2.79,1.58,8.54v3.67a27.82,27.82,0,0,0,.3,5.08,4.54,4.54,0,0,0,2.11,3.11v1.37H179.49c-.34-1.21-.59-2.19-.74-2.92a28.27,28.27,0,0,1-.48-4.71l-.07-5.08q-.07-5.24-1.81-7t-6.5-1.74H158.74V75.89H147.59V21.26h26.13A24,24,0,0,1,182.33,22.67Zm-23.59,8.09V45.43H171a13.18,13.18,0,0,0,5.49-.89q3.22-1.56,3.23-6.15c0-3.31-1-5.54-3.13-6.67a11.21,11.21,0,0,0-5.28-1Z"/>
                            <path class="cls-2 cls-a cls-a2" d="M258.2,30.94H229.29v11.6h26.54V52H229.29v14h30.24v9.82H218.14V21.27H258.2Z"/>
                            <path class="cls-2 cls-a cls-a3" d="M324.86,21.27v9.67H308.52v45H297V30.94H280.61V21.27Z"/>
                            <path class="cls-2 cls-a cls-a4" d="M358.69,75.89H347.35V21.27h11.34Z"/>
                            <path class="cls-2 cls-a cls-a5" d="M385.91,21.27h12l21.69,38.09V21.27h10.63V75.89H418.78L396.54,37.13V75.89H385.91V21.27Z"/>
                            <path class="cls-2 cls-a cls-a6" d="M472.67,21.27h12.92l19.32,54.62H492.54l-3.61-11.23H468.82l-3.71,11.23H453.18Zm-.75,34h14L479,33.76Z"/>
                            <path class="cls-2 cls-a cls-a1" d="M259.53,94.44h2.73v2.22a7.11,7.11,0,0,1,1.84-1.77,6,6,0,0,1,3.34-.93,6.25,6.25,0,0,1,4.82,2.17,8.91,8.91,0,0,1,2,6.22c0,3.64-1,6.24-2.86,7.8a6.44,6.44,0,0,1-4.21,1.48,5.75,5.75,0,0,1-3.17-.82,7.39,7.39,0,0,1-1.67-1.61v8.53h-2.81V94.44Zm10.54,13.1a7.81,7.81,0,0,0,1.31-4.92,9.25,9.25,0,0,0-.58-3.43,4.23,4.23,0,0,0-8,.15,11.66,11.66,0,0,0-.58,4,8.36,8.36,0,0,0,.58,3.3,4.24,4.24,0,0,0,7.26.94Z"/>
                            <path class="cls-2 cls-a cls-a1" d="M293.34,101a1.54,1.54,0,0,0,1.3-.81,2.4,2.4,0,0,0,.19-1.08,2.28,2.28,0,0,0-1-2.08,5.51,5.51,0,0,0-2.93-.65,3.72,3.72,0,0,0-3.12,1.19,4,4,0,0,0-.67,2h-2.62q.07-3.09,2-4.3A8.22,8.22,0,0,1,290.94,94a9.22,9.22,0,0,1,4.8,1.12,3.83,3.83,0,0,1,1.82,3.5v9.62a1.24,1.24,0,0,0,.18.71c.12.18.37.26.76.26l.42,0,.5-.07v2.07a7.12,7.12,0,0,1-1,.24,7.9,7.9,0,0,1-.94,0,2.3,2.3,0,0,1-2.1-1,4.1,4.1,0,0,1-.49-1.55,7.09,7.09,0,0,1-2.46,2,7.66,7.66,0,0,1-3.54.83,5.35,5.35,0,0,1-3.81-1.41,4.72,4.72,0,0,1-1.47-3.54,4.53,4.53,0,0,1,1.45-3.6,6.89,6.89,0,0,1,3.81-1.58Zm-5.91,7.6a3.37,3.37,0,0,0,2.11.7,6.35,6.35,0,0,0,2.87-.69,3.86,3.86,0,0,0,2.34-3.73v-2.26a4.76,4.76,0,0,1-1.33.55,11.43,11.43,0,0,1-1.59.31l-1.7.22a6.4,6.4,0,0,0-2.29.64,2.5,2.5,0,0,0-1.29,2.34A2.32,2.32,0,0,0,287.43,108.55Z"/>
                            <path class="cls-2 cls-a cls-a1" d="M309.16,94.37h2.67v2.88a6.46,6.46,0,0,1,1.6-2A4.17,4.17,0,0,1,316.38,94l.27,0,.64.06v3l-.46-.06-.46,0a4,4,0,0,0-3.26,1.37,4.71,4.71,0,0,0-1.14,3.14v9.61h-2.81V94.37Z"/>
                            <path class="cls-2 cls-a cls-a2" d="M343.33,88.16H360V91H346.35v7H359v2.65H346.35v7.78h13.92v2.72H343.33Z"/>
                            <path class="cls-2 cls-a cls-a2" d="M370.9,88.16h3.67l11.57,18.56V88.16h2.95v22.92h-3.48L373.87,92.52v18.56h-3V88.16Z"/>
                            <path class="cls-2 cls-a cls-a3" d="M401.2,88.16h16.71V91H404.22v7h12.66v2.65H404.22v7.78h13.92v2.72H401.2Z"/>
                            <path class="cls-2 cls-a cls-a4" d="M428.74,88.16h4.45l6.58,19.37,6.54-19.37h4.4v22.92h-2.95V97.55c0-.47,0-1.24,0-2.32s0-2.25,0-3.49l-6.53,19.34h-3.08l-6.58-19.34v.71c0,.56,0,1.42,0,2.56s0,2,0,2.54v13.53h-3V88.16Z"/>
                            <path class="cls-2 cls-a cls-a5" d="M469.16,88.16h3.51L481,111.08h-3.4l-2.33-6.87H466.2l-2.48,6.87h-3.19Zm5.1,13.52-3.48-10.12-3.69,10.12Z"/>
                            <path class="cls-2 cls-a cls-a6" d="M504.91,88.16v2.73h-7.72v20.19H494V90.89h-7.72V88.16Z"/>
                        </g>
                    </svg>
                </div>
                <style>
                    .enemat_loader_0 {
                        fill: transparent;
                    }
                    .enemat_loader_0 .cls-1 {
                        fill: transparent;
                    }
                    .enemat_loader_0 .cls-2 {
                        fill: #0071bc;
                    }
                    .enemat_loader_0 .cls-a {
                        opacity: 0;
                        -webkit-animation: animate_en_0 3.0s infinite;
                        -moz-animation: animate_en_0 3.0s infinite;
                        -ms-animation: animate_en_0 3.0s infinite;
                        -o-animation: animate_en_0 3.0s infinite;
                        animation: animate_en_0 3.0s infinite;
                    }
                    .enemat_loader_0 .cls-a0 {
                        animation-delay: 0.05s;
                    }
                    .enemat_loader_0 .cls-a1 {
                        animation-delay: 0.1s;
                    }
                    .enemat_loader_0 .cls-a2 {
                        animation-delay: 0.3s;
                    }
                    .enemat_loader_0 .cls-a3 {
                        animation-delay: 0.5s;
                    }
                    .enemat_loader_0 .cls-a4 {
                        animation-delay: 0.7s;
                    }
                    .enemat_loader_0 .cls-a5 {
                        animation-delay: 0.9s;
                    }
                    .enemat_loader_0 .cls-a6 {
                        animation-delay: 1.1s;
                    }
                    @keyframes animate_en_0 {
                        0% {
                          opacity: 0;
                        }
                        30% {
                          opacity: 1;
                        }
                        
                        50% {
                          opacity: 1;
                        }
                        
                        100% {
                          opacity: 0;
                        }
                    }
                    
                </style>
            ';
        }
        else {
            return '
            <div id="splash-screen" class="kt-splash-screen ">
              <!--<img src="/assets/img/logo.svg" alt="dzeta logo" style="width: 160px"> -->
              <img src="'.$config->img_logo_url.'" alt="'.$config->img_logo_min_alt.'" style="width: 160px">
              <svg id="spinner-splash-screen" class="spinner" viewBox="0 0 50 50"><circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle></svg>
          </div>
        ';
        }
    }
}