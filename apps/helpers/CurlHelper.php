<?php

namespace Dzeta\Helpers;

class CurlHelper
{
	private $curl;

	public function __construct(string $url, string $method = 'GET', array $request = [], array $headers = [], array $options = []) {
		if ($method == 'GET' && !empty($request)) {
			$url .= http_build_query($request);
		}

		$this->curl = curl_init($url);

		if (($method == 'POST' || $method == 'PUT') && !empty($request)) {
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($request));
		}
		curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $method);

		if (!empty($headers)) {
			$_headers = [];
			foreach ($headers as $key => $header) {
				$_headers[] = $key . ': ' . $header;
			}
			curl_setopt($this->curl, CURLOPT_HTTPHEADER, $_headers);
		}
		foreach ($options as $key => $option) {
			curl_setopt($this->curl, $key, $option);
		}
	}

	public function json() {
		$response = $this->run();
		$json = @json_decode($response, true);
		return $json;
	}

	public function raw() {
		$response = $this->run();
		return $response;
	}

	private function run() {
		$response = curl_exec($this->curl);
		curl_close($this->curl);
		return $response;
	}
}