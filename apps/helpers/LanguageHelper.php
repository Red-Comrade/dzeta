<?php

namespace Dzeta\Helpers;

class LanguageHelper
{
	public static function getFromCookies() {
		$cook = \Phalcon\Di::getDefault()->getCookies();
		if ($cook->has('dzetalang')) {
    		$language = $cook->get('dzetalang')->getValue();
      	} else {
        	$language = 'en';
      	}

    	switch ($language) {
      		case 'ru':
      		case 'en':
      		case 'fr':
        		$language = $language;
        		break;
      		default:
        		$language = 'en';
        		break;
    	}

    	return $language;
	}
}