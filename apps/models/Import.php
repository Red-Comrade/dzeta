<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\Progress;
use Dzeta\Core\User\User;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Import extends Model
{
    const PROCEDURES = [
        'add' => [
            'name' => 'Object_MultiAddition',
            'params' => [
                'Settings' => [ 'type' => DataType::TYPE_JSON ],
                'NoActionOrAppendOrRewriteAttrValsForExistsObjs' => [ 'type' => DataType::TYPE_NUMBER ],
                'User' => [ 'type' => DataType::TYPE_STRING ],
                'CheckErrors' => [ 'type' => DataType::TYPE_NUMBER ],
                'Block' => [ 'type' => DataType::TYPE_NUMBER ],
            ],
            'debug' => true,
        ],
        'addShort' => [
            'name' => 'Object_MultiAddition_short_for_audit',
            'params' => [
                'Settings' => [ 'type' => DataType::TYPE_JSON ],
                'NoActionOrAppendOrRewriteAttrValsForExistsObjs' => [ 'type' => DataType::TYPE_NUMBER ],
                'User' => [ 'type' => DataType::TYPE_STRING ],
                'CheckErrors' => [ 'type' => DataType::TYPE_NUMBER ],
                'Block' => [ 'type' => DataType::TYPE_NUMBER ],
            ],
            'debug' => true,
        ],
        'addCache' => [
            'name' => 'Cache_for_import_Add',
            'params' => [
                'Block_id' => [ 'type' => DataType::TYPE_NUMBER ],
                'Data' => [ 'type' => DataType::TYPE_STRING ],
                'FirstBlock' => [ 'type' => DataType::TYPE_NUMBER ],
            ],
            'debug' => true,
        ],
        'clearCache' => [
            'name' => 'Сache_for_import_Clear',
            'params' => [
                'Block_id' => [ 'type' => DataType::TYPE_NUMBER ],
            ],
            'debug' => true,
        ],
        'fillObjectsKeysSo' => [
            'name' => 'Object_MultiAddition_additional_action_for_audit',
            'params' => [
                'Block' => [ 'type' => DataType::TYPE_NUMBER ],
            ],
            'debug' => true,
        ],
    ];
    const DO_NOTHING_ACTION = 0;
    const WRITE_EMPTY_ACTION = 1;
    const REWRITE_ACTION = 2;

    const ONLY_CHECK_ERRORS = 1;
    const CHECK_ERRORS_AND_ADD = 0;

    private $index = 1;

	public function run(string $uid, string $filename, string $class, string $parent, array $settings) {
		$import = new Core\Import\Import($filename, $settings);
		$progress = new Progress($uid);
		$totalRows = $import->getTotalRows();
		$progress->setCount($totalRows);

		$_attr = new Attr();
		$attrs = [];
		foreach ($settings as $key => $value) {
			if ($value['attribute'] != '') {
				$attr = new Attribute($value['attribute'], '', 0, false, false);
				$attr->setDopSett('excelColumn', $value['column']);
				$attrs[] = $attr;
			}
		}
		$_attr->getAttributesByList($attrs);
		$results = [];
 		while (($values = $import->run()) != []) {
 			$objsResults = $this->addObjects($uid, $class, $parent, $values, $attrs);
 			$results = array_merge($results, $objsResults);
 		}
 		return $results;
	}

	private function addObjects(string $uid, string $class, string $parent, array $data, array $attributes) {
        $_obj = new Obj();
        $results = [];
        $objectsForNames = [];
		foreach ($attributes as $attribute) {
			$column = $attribute->getDopSett('excelColumn');
			if ($attribute->getDatatype() == DataType::TYPE_OBJECT) {
				$uid = $attribute->getUid();
		        foreach ($data as $values) {
		        	if (isset($values[$column])) {
						$objectsForNames[$uid] = isset($objectsForNames[$uid]) ? $objectsForNames[$uid] : ['attr' => $attribute, 'names' => []];
		        		if ($attribute->isArray()) {
							$exploded = explode(Core\Import\Import::ARRAY_DELIMITER, $values[$column]);
							$exploded = array_map('trim', $exploded);
							foreach ($exploded as $value) {
			        			if (!in_array($value, $objectsForNames[$uid]['names'])) {
									$objectsForNames[$uid]['names'][] = $value;
			        			}
							}
		        		} else {
		        			if (!in_array($values[$column], $objectsForNames[$uid]['names'])) {
								$objectsForNames[$uid]['names'][] = $values[$column];
		        			}
		        		}
		        	}
				}
			}
		}
		$objectsNames = [];
		if (!empty($objectsForNames)) {
			$objectsNames = $_obj->getObjectsByAttributesAndObjectNames($objectsForNames);
		}
		$progress = new Progress($uid);
		foreach ($data as $values) {
			$attrs = [];
			foreach ($attributes as $attribute) {
				$column = $attribute->getDopSett('excelColumn');
				if (isset($values[$column])) {
					if ($attribute->isArray()) {
						$exploded = explode(Core\Import\Import::ARRAY_DELIMITER, $values[$column]);
						$exploded = array_map('trim', $exploded);
						$arrayValues = [];
						foreach ($exploded as $value) {
							if ($attribute->getDatatype() == DataType::TYPE_OBJECT) {
								if (isset($objectsNames[$attribute->getUid()]['objects'][$value]) && count($objectsNames[$attribute->getUid()]['objects'][$value]) == 1) {
									$value = $objectsNames[$attribute->getUid()]['objects'][$value][0]->getUid();
								} else {
									$value = null;
								}
							}
							$arrayValues[] = ['value' => $value];
						}
						$attribute->setValue(['value' => $arrayValues]);
						$attribute->setDopSett('addValues', true);
					} else {
						if ($attribute->getDatatype() == DataType::TYPE_OBJECT) {
							if (isset($objectsNames[$attribute->getUid()]['objects'][$values[$column]]) && count($objectsNames[$attribute->getUid()]['objects'][$values[$column]]) == 1) {
								$values[$column] = $objectsNames[$attribute->getUid()]['objects'][$values[$column]][0]->getUid();
							} else {
								$values[$column] = null;
							}
						}
		              	$attribute->setValue(['value' => $values[$column]]);
					}
					$attrs[] = $attribute;
				}
			}
			if (!empty($attrs)) {
		        $obj = new Instance('', '');
		        $obj->setType(new Core\Type\Type($class, ''))->setParent(new Instance($parent, ''));
				$obj->setAttributes($attrs);
		        $result = $_obj->checkUniqueAndAddWithValues($obj, Instance::ADDED_BY_IMPORT, $this->getDI()->getSession()->get('logged')['User']);
		        if (!empty($obj->getUid())) {
		        	$result = $_obj->addValues([$obj], $this->getDI()->getSession()->get('logged')['User']);
		        }
		        $results[] = $result;
			}
            $progress->increaseCurrent();
		}
		return $results;
	}

	public function runCache(string $importUid, string $filename, string $class, string $parent, array $settings, int $rewrite) {
		$import = new Core\Import\Import($filename, $settings);
		$progress = new Progress($importUid);
		$totalRows = $import->getTotalRows();
		$progress->setCount($totalRows);

		$_attr = new Attr();
		$attrs = [];
		foreach ($settings as $key => $value) {
			if ($value['attribute'] != '') {
				$attr = new Attribute($value['attribute'], '', 0, false, false);
				$attr->setDopSett('excelColumn', $value['column']);
				$attrs[] = $attr;
			}
		}
		$_attr->getAttributesByList($attrs);
		$classes = [];
		foreach ($attrs as $attr) {
			$_class = $attr->getParent();
			if (!empty($_class)) {
				$uid = $_class->getUid();
				$classes[$uid] = isset($classes[$uid]) ? $classes[$uid] : ['class' => $_class, 'attrs' => []];
				$classes[$uid]['attrs'][] = $attr;
			}
		}
		$_type = new Type();
		$allClasses = $_type->getAll(true);
		$allClasses = array_combine(array_column($allClasses, 'ID'), $allClasses);
		foreach ($classes as $uid => $_class) {
			if (isset($allClasses[$uid])) {
				$classes[$uid]['class']->setParent(new Core\Type\Type($allClasses[$uid]['BaseClass'], ''));
			}
		}
		$first = true;
		$block = null;
 		while (($values = $import->run()) != []) {
 			$result = $this->addObjectsCache($importUid, $class, $parent, $values, $classes, $block, $first);
 			$block = isset($result['block']) ? $result['block'] : $block;
 			$first = false;
 		}
 		$user = $this->getDI()->getSession()->get('logged')['User'];
 		$results = $this->add($block, null, $rewrite, self::CHECK_ERRORS_AND_ADD, $user);
 		if (!empty($block)) {
 			$this->clearCache($block);
 		}
 		$progress->setCurrent($totalRows);
 		return ['results' => $results, 'block' => $block];
	}

	private function addObjectsCache(string $importUid, string $class, string $parent, array $data, array $classes, ?int $block, bool $first) {
		$progress = new Progress($importUid);
		$objects = [];
		foreach ($data as $row => $values) {
			$objs = [];
			foreach ($classes as $_class) {
				$attrs = [];
				foreach ($_class['attrs'] as $attribute) {
					$attribute->setDatatype(DataType::getIndexFromType(DataType::TYPE_TEXT));
					$column = $attribute->getDopSett('excelColumn');
					if (isset($values[$column]) || @is_null($values[$column])) {
						if ($attribute->isArray()) {
							if ($values[$column] !== '') {
								$exploded = explode(Core\Import\Import::ARRAY_DELIMITER, $values[$column]);
								$exploded = array_map('trim', $exploded);
								$arrayValues = [];
								foreach ($exploded as $value) {
									$arrayValues[] = ['value' => $value];
								}
							} else {
								$arrayValues = null;
							}
							$attribute->setValue(['value' => $arrayValues]);
							$attribute->setDopSett('addValues', true);
						} else {
			              	$attribute->setValue(['value' => $values[$column]]);
						}
						$attrs[] = clone $attribute;
					}
				}
				if (!empty($attrs)) {
					$_classUid = $_class['class']->getUid();
			        $objs[$_classUid] = new Instance('', '');
			        $objs[$_classUid]->setOption('importIndex', $this->index);
			        $objs[$_classUid]->setOption('importExcelRow', $row);
					$objs[$_classUid]->setAttributes($attrs);
					$objs[$_classUid]->setType($_class['class']);
					$objects[] = $objs[$_classUid];
			        $this->index++;
				}
			}
			foreach ($objs as $_class => $obj) {
				if ($_class == $class && !empty($parent)) {
					$obj->setParent(new Instance($parent, ''));
				} else {
					$_parent = $obj->getType()->getParent();
		        	if (!empty($_parent)) {
		        		$_parentUid = $_parent->getUid();
		        		if (isset($objs[$_parentUid])) {
		        			$importIndex = $objs[$_parentUid]->getOption('importIndex');
		        			$obj->setOption('importParentIndex', $importIndex);
		        		}
		        	}
				}
			}
            $progress->increaseCurrent();
		}
		$block = $this->addCache($objects, $block, $first);
		return ['block' => $block];
	}

	public function addCache(array $objects, ?int $block = null, bool $firstBlock = false, ?User $user = null) {
		$user = !empty($user) ? $user : $this->getDI()->getSession()->get('logged')['User'];
		$settings = $this->_objectsToSettings($objects, $user);
		if (!empty($settings)) {
			$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
	            'Block_id' => $block,
	            'Data' => json_encode($settings, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
	            'FirstBlock' => $firstBlock ? 1 : 0,
	        ]);
	        if (!empty($data)) {
	        	$block = empty($data[0]['IDb']) ? $block : $data[0]['IDb'];
	        }
		}

        return $block;
	}

	public function clearCache(int $block) {
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Block_id' => $block,
        ]);

        return $block;
	}

	public function add(?int $block, ?array $objects, int $action, int $check, User $user) {
		$result = ['result' => ['updated' => 0, 'created' => 0], 'errors' => []];
		$settings = !empty($objects) ? $this->_objectsToSettings($objects, $user) : null;
		$errors = [];
		if (!empty($settings) || !empty($block)) {
			$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
	            'Settings' => $settings,
	            'NoActionOrAppendOrRewriteAttrValsForExistsObjs' => $action,
	            'User' => $user->getUid(),
	            'CheckErrors' => $check,
	            'Block' => $block,
	        ]);
	        if (!empty($data)) {
	        	$errors = !empty($data[0]['q']) ? json_decode($data[0]['q'], true) : [];
	        	foreach ($errors as $key => $value) {
	        		if (isset($value['NumObjsImported']) || isset($value['NumObjsUpdated'])) {
	        			unset($errors[$key]);
	        			$result['result']['updated'] = $value['NumObjsUpdated'];
	        			$result['result']['created'] = $value['NumObjsImported'];
	        		} else {
		        		if (isset($value['info'])) {
		        			$errors[$key]['info'] .= ' -'.$data[0]['ErrCode'];
		        		}
	        		}
	        	}
	        	$result['errors'] = array_values($errors);
	        }
		}

        return $result;
	}

	public function addShort(?int $block, ?array $objects, int $action, int $check, User $user) {
		$settings = !empty($objects) ? $this->_objectsToSettings($objects, $user) : null;
		$errors = [];
		if (!empty($settings) || !empty($block)) {
			$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
	            'Settings' => $settings,
	            'NoActionOrAppendOrRewriteAttrValsForExistsObjs' => $action,
	            'User' => $user->getUid(),
	            'CheckErrors' => $check,
	            'Block' => $block,
	        ]);
	        if (!empty($data)) {
	        	$errors = json_decode($data[0]['q']);
	        }
		}

        return $errors;
	}

	public function fillObjectsKeysSo(int $block) {
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Block' => $block,
        ]);

        return $block;
	}

	private function _objectsToSettings(array $objects, User $user) {
		$settings = [];
		foreach ($objects as $object) {
			$values = Obj::convertObject($object, $user);
			if (!empty($values)) {
				$setting = [
					'n' => $object->getOption('importIndex'),
					'num_ex' => $object->getOption('importExcelRow'),
					'cl' => $object->getType()->getUid(),
					'v' => $values['Vals']
				];
				$unique = [];
				foreach ($object->getAttributes() as $attr) {
					if ($attr->isKey()) {
						$value = $attr->getValue();
						if (!empty($value)) {
							// $datatype = $attr->getDatatype();
							// if ($datatype == DataType::TYPE_DATETIME) {
			                    // $unique[$attr->getUid()] = $value->getValueDB();
			                // } else if ($datatype == DataType::TYPE_OBJECT) {
			                	// $unique[$attr->getUid()] = $value->getUid();
			                // } else {
			                    // $unique[$attr->getUid()] = $value->getValue();
			                // }
		                    $unique[$attr->getUid()] = $value->getValue();
						} else {
							$unique[$attr->getUid()] = null;
						}
					}
				}
				$setting['un'] = $unique;
				$parent = $object->getParent();
				if (!empty($parent)) {
					$setting['pid'] = $parent->getUid();
				} else {
					$parentIndex = $object->getOption('importParentIndex');
					if (!empty($parentIndex)) {
						$setting['pn'] = $parentIndex;
					} else {
						$setting['pid'] = Instance::INSTANCE_ROOT;
					}
				}
				$settings[] = $setting;
			}
		}
		return $settings;
	}
}