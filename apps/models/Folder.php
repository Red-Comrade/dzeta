<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\App;
use Dzeta\Core\User\User;
use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Folder extends Model
{
	const PROCEDURES = [
		'getAll' => [
			'name' => 'Folder_GetAll',
            'params' => [],
            'debug' => true,
		],
		'getApps' => [
			'name' => 'Folder_GetAppsForFolder',
            'params' => [
                'Folder' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
		],
		'add' => [
			'name' => 'Folder_AddNew',
            'params' => [
                'Name' => [ 'type' => DataType::TYPE_STRING ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
                'Settings' => [ 'type' => DataType::TYPE_JSON ],
            ],
            'debug' => true,
		],
		'rename' => [
			'name' => 'Folder_Rename',
            'params' => [
                'ID' => [ 'type' => DataType::TYPE_STRING ],
                'Name' => [ 'type' => DataType::TYPE_STRING ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
		],
		'remove' => [
			'name' => 'Folder_Delete',
            'params' => [
                'Folder' => [ 'type' => DataType::TYPE_STRING ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
		],
		'updateSettings' => [
			'name' => 'Folder_EditSettings',
            'params' => [
                'ID' => [ 'type' => DataType::TYPE_STRING ],
                'Settings' => [ 'type' => DataType::TYPE_JSON ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
		],
		'updateOrder' => [
			'name' => 'Folder_AddOrder',
            'params' => [
                'Order' => [ 'type' => DataType::TYPE_JSON ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
		],
        'updateAppsOrder' => [
            'name' => 'Folder_AddOrderForApps',
            'params' => [
                'Folder' => [ 'type' => DataType::TYPE_STRING ],
                'Order' => [ 'type' => DataType::TYPE_JSON ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
		'addApp' => [
			'name' => 'Folder_AddApp',
            'params' => [
                'App' => [ 'type' => DataType::TYPE_STRING ],
                'Folder' => [ 'type' => DataType::TYPE_STRING ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
		],
	];

    /**
     * Get all folders
     *
     * @return Core\App\Folder[]
     */
	public static function getAll() {
		$folders = [];
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], []);
        if (!empty($data)) {
            foreach ($data as $folder) {
            	$settings = empty($folder['Settings']) ? [] : json_decode($folder['Settings'], true);
            	$settings = is_array($settings) ? $settings : [];
            	$folders[] = new App\Folder($folder['ID'], $folder['Name'], $settings);
            }
        }

        return $folders;
	}

    /**
     * Get apps in folder
     *
     * @param Core\App\Folder $folder
     *
     * @return Core\App\App[]
     */
	public static function getApps(App\Folder $folder) {
		$apps = [];
		$uid = $folder->getUid();
		$uid = empty($uid) ? null : $uid;
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'Folder' => $uid
		]);
        if (!empty($data)) {
            foreach ($data as $app) {
            	$settings = empty($app['Settings']) ? [] : json_decode($app['Settings'], true);
            	$settings = is_array($settings) ? $settings : [];
            	$apps[] = new App\App($app['ID'], $app['Name'], [], $settings);
            }
        	$folder->setOrder($apps);
        }

        return $apps;
	}

    /**
     * Add folder
     *
     * @param Core\App\Folder  $folder
     * @param Core\User\User   $user
     *
     * @return ['errors' => [], 'code' => 0, 'data' => []]
     */
	public static function add(App\Folder $folder, User $user) 
    {
        $result = ['errors' => [], 'code' => -1, 'data' => []];
        
        $settings = $folder->getSettings();
        $settings = empty($settings) ? null : $settings;        
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Name' => $folder->getName(),
            'UserID' => $user->getUid(),
            'Settings' => $settings,
        ]);
        $codeText='';
        if (!empty($data)) 
        {
            $result['code'] = $data[0]['Rcode'];
            if ($result['code'] == 1) 
            {
                $folder->setUid($data[0]['IDf']);
            }else
            {
                $codeText=$data[0]['ErrCode'];
            }
        }     
        if ($result['code'] != 1)
        {
            $result['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result['code'],
                [0=>$folder->getName()],
                null,
                $codeText
            )];
        }
        return $result;
	}

    /**
     * Rename folder
     *
     * @param Core\App\Folder  $folder
     * @param Core\User\User   $user
     *
     * @return ['errors' => [], 'code' => 0, 'data' => []]
     */
	public static function rename(App\Folder $folder, User $user) {
        $result = ['errors' => [], 'code' => -1, 'data' => []];
        $codeText='';
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'ID' => $folder->getUid(),
            'Name' => $folder->getName(),
            'UserID' => $user->getUid()
        ]);
        if (!empty($data)) 
        {
            $result['code'] = $data[0]['Rcode'];
            if($result['code']!=1)
            {
                $codeText= $data[0]['ErrCode'];
            }
        }
        if ($result['code']!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result['code'],
                [0=>$folder->getName()],
                null,
                $codeText
            )];
        }   
        return $result;
	}

    /**
     * Remove folder
     *
     * @param Core\App\Folder  $folder
     * @param Core\User\User   $user
     *
     * @return ['errors' => [], 'code' => 0, 'data' => []]
     */
	public static function remove(App\Folder $folder, User $user) {
        $result = ['errors' => [], 'code' => -1, 'data' => []];
        $codeText='';
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Folder' => $folder->getUid(),
            'UserID' => $user->getUid()
        ]);
        if (!empty($data)) {
            $result['code'] = $data[0]['Rcode'];
            if($result['code']!=1)
            {
                $codeText=$data[0]['ErrCode'];
            }
        }
        if ($result['code']!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result['code'],
                [],
                null,
                $codeText
            )];
        }        
        return $result;
	}

    /**
     * Update folder's settings
     *
     * @param Core\App\Folder  $folder
     * @param Core\User\User   $user
     *
     * @return ['errors' => [], 'code' => 0, 'data' => []]
     */
	public static function updateSettings(App\Folder $folder, User $user) 
    {
        $result = ['errors' => [], 'code' => -1, 'data' => []];
        $codeText='';
        $settings = $folder->getSettings();
        $settings = empty($settings) ? null : $settings;
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'ID' => $folder->getUid(),
            'Settings' => $settings,
            'UserID' => $user->getUid()
        ]);
        if (!empty($data)) 
        {
            $result['code'] = $data[0]['Rcode'];
            if($result['code']!=1)
            {
                $codeText=$data[0]['ErrCode'];
            }
        }
        if ($result['code']!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result['code'],
                [],
                null,
                $codeText
            )];
        }   
        return $result;
	}

    /**
     * Update folders' order
     *
     * @param Core\App\Folder[]	$folders
     * @param Core\User\User	$user
     *
     * @return ['errors' => [], 'code' => 0, 'data' => []]
     */
	public static function updateOrder(array $folders, User $user) {
        $result = ['errors' => [], 'code' => -1, 'data' => []];
        $order = [];
        foreach ($folders as $key => $folder) {
        	$order[] = ['Index' => $key, 'ID' => $folder->getUid()];
        }
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Order' => $order,
            'UserID' => $user->getUid()
        ]);
        if (!empty($data)) {
            $result['code'] = $data[0]['Rcode'];
        }
        if ($result['code'] != 1) {
            $result['errors'][] = [
            	'text' => DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result['code'])
            ];
        }
        return $result;
	}

    /**
     * Update apps' order in folder
     *
     * @param Core\App\Folder $folder
     * @param Core\User\User  $user
     *
     * @return ['errors' => [], 'code' => 0, 'data' => []]
     */
    public static function updateAppsOrder(App\Folder $folder, User $user) {
        $result = ['errors' => [], 'code' => -1, 'data' => []];
        $order = [];
        foreach ($folder->getOrder() as $key => $app) {
            $order[] = ['Index' => $key, 'ID' => $app->getUid()];
        }
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Folder' => $folder->getUid(),
            'Order' => $order,
            'UserID' => $user->getUid()
        ]);
        if (!empty($data)) {
            $result['code'] = $data[0]['Rcode'];
        }
        if ($result['code'] != 1) {
            $result['errors'][] = [
                'text' => DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result['code'])
            ];
        }
        return $result;
    }

    /**
     * Add app to folder
     *
     * @param Core\App\Folder  $folder
     * @param Core\App\App     $app
     * @param Core\User\User   $user
     *
     * @return ['errors' => [], 'code' => 0, 'data' => []]
     */
	public static function addApp(App\Folder $folder, App\App $app, User $user) {
		$result = ['errors' => [], 'code' => -1, 'data' => []];
		$uid = $folder->getUid();
		$uid = empty($uid) ? null : $uid;
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'App' => $app->getUid(),
            'Folder' => $uid,
            'UserID' => $user->getUid()
        ]);
        if (!empty($data)) {
            $result['code'] = $data[0]['Rcode'];
        }
        if ($result['code'] != 1) {
            $result['errors'][] = [
            	'text' => DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result['code'])
            ];
        }
        return $result;
	}
}