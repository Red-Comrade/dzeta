<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\User\User;
use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class App extends Model
{
    const PROCEDURES = [
        'add' => [
            'name' => 'App_AddNew',
            'params' => [
                'Name'    => [ 'type' => DataType::TYPE_STRING ],
                'UserID'  => [ 'type' => DataType::TYPE_STRING ],
                'Settings'=> [ 'type' => DataType::TYPE_JSON ],
            ],
            'debug' => true,
        ],
        'rename' => [
         'name' => 'App_Rename',
          'params' => [
            'ID'      => [ 'type' => DataType::TYPE_STRING ],
            'Name'    => [ 'type' => DataType::TYPE_STRING ],
            'UserID'  => [ 'type' => DataType::TYPE_STRING ],
          ],
          'debug' => true,
        ],
        'remove' => [
            'name' => 'App_delete',
            'params' => [
                'App'     => [ 'type' => DataType::TYPE_STRING ],
                'UserID'  => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'getAllTreeAppViewDB'=>[
            'name' => 'App_GetTree',
            'params' => [
            ],
            'debug' => true,
        ],
        'getViews'=>[
            'name' => 'App_GetViews',
            'params' => [
                'App'     => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'GetInfoApp'=> [
            'name' => 'App_GetInfo',
            'params' => [
                'App'      => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'AppEditSettings'=> [
            'name' => 'App_EditSettings',
            'params' => [
                'ID'      => [ 'type' => DataType::TYPE_STRING ],
                'Settings'=> [ 'type' => DataType::TYPE_JSON ],
                'UserID'  => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],


        'App_access_add_forGroup'=> [
            'name' => 'App_access_add_forGroup',
            'params' => [
                'App'      => [ 'type' => DataType::TYPE_STRING ],
                'Group'      => [ 'type' => DataType::TYPE_STRING ],
                'Settings'      => [ 'type' => DataType::TYPE_JSON ],
                'User'      => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'App_access_GetForApp'=> [
            'name' => 'App_access_GetForApp',
            'params' => [
                'App'      => [ 'type' => DataType::TYPE_STRING ],
                'Group'      => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'App_access_Get'=> [
            'name' => 'App_access_Get',
            'params' => [
                'View'      => [ 'type' => DataType::TYPE_STRING ],
                'UserID'      => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'Object_BaseObjectsForObject'=> [
            'name' => 'Object_BaseObjectsForObject',
            'params' => [
                'Object'      => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
    ];



        /**
     * App_EditSettings App
     *
     * @param Core\App\App        $app
     * @param Core\User\User      $user
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function AppEditSettings(Core\App\App $app, User $user)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'ID'      => $app->getUid(),
          'Settings'=> $app->getSettings(),
          'UserID'  => $user->getUid()
        ]);
        
        if (!empty($data))
        {           
            $result=$data[0]['Rcode'];
            $dataResult['result']= $result;           
        }
        if ($result!=1) {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PPOCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }

        /**
     * GetInfoView App
     *
     * @param Core\App\App        $app
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function GetInfoApp(Core\App\App $app)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'App'    => $app->getUid()
        ]);

        if (!empty($data))
        {
            $data=$data[0];
            $result=1;
            $dataResult['result']=1;
            $dataResult['data'] = $data;
            $app->setName($data['Name']);
            $app->setHasViews($data['HasViews']);
            $app->setSettings(json_decode($data['Settings'],true));
        }
        if ($result!=1) {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }


		/**
		 * Add application
		 *
		 * @param Core\App\App        $app
		 * @param Core\User\User      $user
		 *
		 * @return ['error'=>[],'result'=>[],'data'=>[]]
		 */
    public function add(Core\App\App $app, User $user)
    {
        $flagErr=false;
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        $result = -1;
        $appName=$app->getName();
        $codeText='';
        if ($appName=='') 
        {
            $flagErr=true;
            $result=-2;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-2,
                'details'=>'Не введено имя приложения',
                'codeID'=>'a6c834e4-774f-4e48-87c1-6fe3ea5858cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        if(!$flagErr) 
        {
            $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'Name'   => $appName,
                'UserID' => $user->getUid(),
                'Settings'=>$app->getSettings()
            ]);
            if (!empty($data)) 
            {
                $result = $data[0]['Rcode'];
                if ($result == 1) 
                {
                    $app->setUid($data[0]['IDa']);
                }else
                {
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
        
        if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$appName],
                null,
                $codeText
            )];
        }

        $dataResult['result']=$result;
        return $dataResult;
    }

		/**
		 * rename application
		 *
		 * @param Core\App\App        $app
		 * @param Core\User\User      $user
		 *
		 * @return ['error'=>[],'result'=>[],'data'=>[]]
		 */
    public function rename(Core\App\App $app, User $user)
    {
		$appName=$app->getName();
		$flagErr=false;
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        $result = -1;
        $appName=$app->getName();
        if ($appName=='') {
            $flagErr=true;
            $result=-2;
        }
		if(!$flagErr)
		{
				$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
		      'ID'     => $app->getUid(),
		      'Name'   => $appName,
		      'UserID' => $user->getUid()
		    ]);
            $result = -1;
            if (!empty($data))
            {
                $result = $data[0]['Rcode'];
            }
		}
		if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result,[0=>$appName])];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }
	/**
	 * rename applications
	 *
	 * @param Core\App\App[]        $apps
	 * @param Core\User\User      $user
	 *
	 * @return ['error'=>[],'result'=>[],'data'=>[]]
	 */
    public function remove(array $apps, User $user)
    {
		$dataResult['error']=[];
		$dataResult['result']=-1;
		$dataResult['data']=[];
		$uids=[];
		foreach ($apps as $app) {
			$uids[]=$app->getUid();
		}
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
		      'App'     => '{'.implode(',',$uids).'}',
		      'UserID' => $user->getUid()
		    ]);

        $result = -1;
        if (!empty($data)) 
        {
			foreach ($data as $key => $value) {
				$dataResult['data'][]=['app'=>$value['View'],'result'=>$value['Rcode']];
				if($value['Rcode']!=1)
				{
					  $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result),'app'=>$value['View']];
				}
			}
			if(empty($dataResult['error']))
			{
					$result=1;
			}
        }
		if ($result!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }


    /**
     * Get views for application
     *
     * @param Core\App\App        $application
     *  @return Core\App\View[]
     */
    public function getViews($application)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'App' =>$application->getUid(),
        ]);
        $views=[];
        foreach ($data as  $viewDB) {
            $sett=[];
            if(!empty($viewDB['Settings']))
            {
                $sett=json_decode($viewDB['Settings'],true);
            }
            $sett=empty($sett)? []:$sett;
            $view = new Core\App\View($viewDB['View'], $viewDB['Name'],$sett);
            $view->setHasViews($viewDB["HasChildren"]);
                //$HasViews
            $views[] = $view;
        }
        $application->setViews($views);
        return $views;
    }
    public function getAllTreeAppViewDB(){
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
        ]);
        return $data;
    }

    public function getAllTreeAppView($params)
    {
        $data = $this->getAllTreeAppViewDB();
        $res=[];

        $res = $this->createTreeApp(null, $data, null, $params);
        $res = $this->createTreeAppFolders($res);
        return $res;
    }

    private function createTreeApp($uid, &$data, $uipApp, $params)
    {
        $arr=[];
        for ($i=0,$len=count($data);$i<$len;$i++) {
            if ($data[$i]['Parent']==$uid) {
                $tmp=[
                        'parent'=>$uid,
                        'uid'=>$data[$i]['ID'],
                        'name'=>$data[$i]['Name']
                    ];
                if ($data[$i]['IsView']) {
                    $tmp['isView']=true;
                    $tmp['appUid']=$uipApp;
                    $tmp['table_or_form']=$data[$i]['TableOrForm'];
                    if ( $tmp['table_or_form'] === null) {
                        $tmp['table_or_form'] = 1;
                    }
                    if(!empty($params["isGetPermiss"])) {
                        $view = new Core\App\View($tmp['uid'], '', []);
                        $_view = new View();
                        $result = $_view->getViewClass($view);


                        foreach ($view->getClassesLink() as $key => $value) {
                            //'Class'=>$value['Class']->getUid(),
                            //'MainClass'=>$value['MainClass']->getUid(),
                            //'Class'=>$value['Class']->getUid(),



                            $tmp['classes'][] = [
                                'Class'=>$value->getDependentClass()->getUid(),
                                'MainClass'=>$value->getMainClass()->getUid(),
                                'ClassName'=>$value->getDependentClass()->getName(),
                                'MainClassName'=>$value->getMainClass()->getName(),
                            ];

                        }
                    }
                }
                else {
                    $tmp['isApp']=true;
                    $uipApp=$data[$i]['ID'];

                    if(!empty($params["isGetPermiss"]) ) {
                        $tmp["permission"] = $this->App_access_GetForApp(["App" => $uipApp, "GroupID" => $params["GroupID"]]);
                    }
                    else if( !empty($params["isHome"]) ) {

                        $FSettings = [];
                        foreach($params["GroupIDs"] as $item ) {
                            $data_perm = $this->App_access_GetForApp(["App" => $uipApp, "GroupID" => $item["IDo"]]);
                            foreach($data_perm["FSettings"] as $f_key => $f_item) {
                                if(!isset($FSettings[$f_key])) {
                                    $FSettings[$f_key] = [];
                                }
                                foreach($data_perm["FSettings"][$f_key] as $c_key => $c_item) {
                                    if(isset($FSettings[$f_key][$c_key])){
                                        $FSettings[$f_key][$c_key]["Add"] = $data_perm["FSettings"][$f_key][$c_key]["Add"] == 1 ? $data_perm["FSettings"][$f_key][$c_key]["Add"] : $FSettings[$f_key][$c_key]["Add"];
                                        $FSettings[$f_key][$c_key]["EditDel"] = $data_perm["FSettings"][$f_key][$c_key]["EditDel"] == 1 ? $data_perm["FSettings"][$f_key][$c_key]["EditDel"] : $FSettings[$f_key][$c_key]["EditDel"];
                                        $FSettings[$f_key][$c_key]["Read"] = $data_perm["FSettings"][$f_key][$c_key]["Read"] == 1 ? $data_perm["FSettings"][$f_key][$c_key]["Read"] : $FSettings[$f_key][$c_key]["Read"];
                                    }
                                    else {
                                        $FSettings[$f_key][$c_key] = $c_item;
                                    }

                                     if($FSettings[$f_key][$c_key]["Read"] == 1) {
                                         $data_app["FSettings"] = $FSettings;
                                         $tmp["Read"] = 1;
                                         break 3;
                                         //Выходим из всех циклов, так как тут есть что читать
                                     }
                                }
                            }
                        }
                        $tmp["permission"] = ["FSettings" => $FSettings];
                    }

                }
                if ($data[$i]['Children']) {
                    if(!empty($params["isHome"]) && $uipApp != "00000000-0000-0000-0000-000000000000") {
                        //Сделано, чтобы не получать лишние узлы при построении домашней страницы
                        //Мы получаем только корневые приложения
                    }
                    else {
                       $tmp['children']=$this->createTreeApp($data[$i]['ID'], $data, $uipApp, $params);
                    }

                } else {
                    $tmp['children']=[];
                }


                $tmp["Name"] = $tmp["name"];
                $tmp["permissionStatus"] = "allow";

                $arr[]=$tmp;

            }
        }
        return $arr;
    }

    private function createTreeAppFolders($tree)
    {
        $folders = Folder::getAll();
        $foldersNodes = [];
        foreach ($folders as $folder) {
            $apps = Folder::getApps($folder);
            $folderNode = [
                'Name' => $folder->getName(),
                'children' => [],
                'isFolder' => true,
                'name' => $folder->getName(),
                'parent' => Core\Type\Type::TYPE_ROOT,
                'permissionStatus' => 'allow',
                'uid' => $folder->getUid(),
                'Read' => 0,
            ];
            $isRead = false;
            foreach ($apps as $app) {
                foreach ($tree[0]['children'] as $key => $node) {
                    if ($node['uid'] == $app->getUid()) {
                        if(!empty($node["Read"])) {
                            $isRead = true;
                        }
                        $folderNode['children'][] = $node;
                        unset($tree[0]['children'][$key]);
                    }
                }
            }
            if($isRead) {
                $folderNode["Read"] = 1;
            }

            $foldersNodes[] = $folderNode;
        }
        $tree[0]['children'] = array_values($tree[0]['children']);
        array_unshift($tree[0]['children'], ...$foldersNodes);
        return $tree;
    }


    //Получение прав по приложению
    public function App_access_GetForApp($params)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'App'    => $params["App"],
            'Group'    => $params["GroupID"]
        ]);

        $result = 1;
        $dataResult['result'] = 1;
        $dataResult['Settings'] = $data;


        $func_r = function($data) {
            $f_data = [];
            if(!empty($data)) {
                foreach ($data as $key => $value) {
                    if(!isset($f_data[$value["View"]])) {
                        $f_data[$value["View"]] = [];
                    }

                    $f_data[$value["View"]][$value["Class"]] = $value;
                }
            }

            return $f_data;
        };

        $fdata = $func_r($data);

        $dataResult["FSettings"] = $fdata;

        if ($result!=1) {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }
    //Получение прав
    public function App_access_Get($params)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'View'    => $params["View"],
            'UserID'    => $params["UserID"]
        ]);

        if (!empty($data)) {
            $data = $data[0];
            $result = 1;
            $dataResult['result'] = 1;
            //$app->setName($data['Name']);
            //$app->setSettings(json_decode($data['Settings'], true));
        }
        if ($result!=1) {
            $dataResult['error']=[];
            //$dataResult['error'][]=['text'=>DbError::getError(self::PPOCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }
    //Установка прав
    public function App_access_add_forGroup($params)
    {

        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'App'    => $params["App"],
            'Group'    => $params["Group"],
            'Settings'    => $params["Settings"],
             'User'    => $params["User"],
        ]);

        $result = 1;
        $dataResult['result'] = 1;

        if ($result!=1) {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }

    //Получение прав
    public function Object_BaseObjectsForObject($ObjID)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Object'    => $ObjID,
        ]);

        return $data;

        if (!empty($data)) {
            $data = $data[0];
            $result = 1;
            $dataResult['result'] = 1;
            //$app->setName($data['Name']);
            //$app->setSettings(json_decode($data['Settings'], true));
        }
        if ($result!=1) {
            $dataResult['error']=[];
            //$dataResult['error'][]=['text'=>DbError::getError(self::PPOCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }


    public function GenerateFSettings( $UserID, $AppUid ) {
        $user_groups = $this->Object_BaseObjectsForObject($UserID);
        $FSettings = [];
        foreach($user_groups as $item ) {
            $data_perm = $this->App_access_GetForApp(["App" => $AppUid, "GroupID" => $item["IDo"]]);
            foreach($data_perm["FSettings"] as $f_key => $f_item) {
                if(!isset($FSettings[$f_key])) {
                    $FSettings[$f_key] = [];
                }
                foreach($data_perm["FSettings"][$f_key] as $c_key => $c_item) {
                    if(isset($FSettings[$f_key][$c_key])) {
                        $FSettings[$f_key][$c_key]["Add"] = $data_perm["FSettings"][$f_key][$c_key]["Add"] == 1 ? $data_perm["FSettings"][$f_key][$c_key]["Add"] : $FSettings[$f_key][$c_key]["Add"];
                        $FSettings[$f_key][$c_key]["EditDel"] = $data_perm["FSettings"][$f_key][$c_key]["EditDel"] == 1 ? $data_perm["FSettings"][$f_key][$c_key]["EditDel"] : $FSettings[$f_key][$c_key]["EditDel"];
                        $FSettings[$f_key][$c_key]["Read"] = $data_perm["FSettings"][$f_key][$c_key]["Read"] == 1 ? $data_perm["FSettings"][$f_key][$c_key]["Read"] : $FSettings[$f_key][$c_key]["Read"];
                    }
                    else {
                        $FSettings[$f_key][$c_key] = $c_item;
                    }

                    if($FSettings[$f_key][$c_key]["Read"] == 1) {
                        $FSettings[$f_key]["ExRead"] = 1;
                        //Значит это можно читать
                    }
                }
            }
        }

        return $FSettings;
    }
}
