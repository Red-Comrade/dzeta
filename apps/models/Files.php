<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Files extends Model
{
	const PROCEDURES = [
		'getInfoFile' => [
			'name' => 'Vfile_GetName',
			'params' => [
				'Obj'       => [ 'type' => DataType::TYPE_STRING ]
			],
			'debug' => true,
		],

	];

  public function getInfoFile(Core\Value\File $file)
  {
    $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'Obj'       => $file->getValue()
		]);

		if (!empty($data)) {
        $file->setName($data[0]['RealName']);
        $file->setSize($data[0]['Size']);
        return 1;
		}else {
      return -1;
    }

  }

}
