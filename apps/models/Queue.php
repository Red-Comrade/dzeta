<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\User\User;
use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Queue extends Model
{
    /*Model for working with queues*/
    const PROCEDURES = [
        'queueGetForCalc' => [
            'name' => 'calc_queue_getforcalc',
            'params' => [
                'Num'    => [ 'type' => DataType::TYPE_NUMBER ]
            ],
            'debug' => true,
        ],
        'queueFinCalc'=>[
            'name' => 'calc_queue_addresults',
            'params' => [
                'IDs'    => [ 'type' => DataType::TYPE_JSON ],
                'Result' => [ 'type' => DataType::TYPE_NUMBER ],
            ],
            'debug' => true,
        ]
    ];

    public function queueFinCalc(array $IDs,$Result){

        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'IDs'   =>$IDs,
          'Result' =>$Result
        ]);
        if (!empty($data))
        {
            $dataResult['result']=$data[0]['Rcode'];
        }
        if($dataResult['result']!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }       
        return $dataResult;
    }


    public function queueGetForCalc(int $num)
    {

        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];

       
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'Num'   =>$num
        ]);
        echo json_encode($data);
        if (!empty($data))
        {
            // RETURNS TABLE(calc uuid, settings text, class uuid, obj uuid, "user" uuid, "id" int)
           foreach ($data as $key => $value)
           {
                $calc=new Core\Calculation($value['calc'], '', '');
                $name='';
                $calc->setName($name);
                $js=$value['settings'];
                $frml=json_decode($js, true)['fOrig'];
                $calc->setFormula($frml);
                $class=new Core\Type\Type($value['class'], '');
                $obj = new Core\Instance($value['obj'], '');
                $obj->setType($class);
                $user = new Core\User\User($value['user'], '');
                $dataResult['data'][]=['calc'=>$calc,'obj'=>$obj,'user'=>$user,'id'=>$value['id']];
           }
        }
            
        return $dataResult;
    }



}