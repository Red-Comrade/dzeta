<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\AttributeGroup;
use Dzeta\Core\User\User;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Type extends Model
{
	const PROCEDURES = [
		'add' => [
			'name' => 'Class_AddNew',
			'params' => [
				'ClassName' => [ 'type' => DataType::TYPE_STRING ],
				'BaseClass' => [ 'type' => DataType::TYPE_STRING ],
				'IsSystem' => [ 'type' => DataType::TYPE_NUMBER ],
				'InheritedFromClass' => [ 'type' => DataType::TYPE_STRING ],
				'UserID' => [ 'type' => DataType::TYPE_STRING ],
			],
			'debug' => true,
		],
		'rename' => [
			'name' => 'ClassRename',
			'params' => [
				'ClassName' => [ 'type' => DataType::TYPE_STRING ],
				'IDc' => [ 'type' => DataType::TYPE_STRING ],
				'UserID' => [ 'type' => DataType::TYPE_STRING ]
			],
			'debug' => true,
		],
		'remove' => [
			'name' => 'Class_Delete',
			'params' => [
				'ID' => [ 'type' => DataType::TYPE_STRING ],
				'UserID' => [ 'type' => DataType::TYPE_STRING ],
				'BsClass'=> [ 'type' => DataType::TYPE_STRING ]
			],
			'debug' => true,
		],
		'addLink'=>[
			'name' => 'Class_AddNewLink',
			'params' => [
				'ChClass' => [ 'type' => DataType::TYPE_STRING ],
				'BsClass' => [ 'type' => DataType::TYPE_STRING ],
				'UserID'=> [ 'type' => DataType::TYPE_STRING ]
			],
			'debug' => true,
		],
		'getChildren' => [
			'name' => 'Class_GetByBaseClass',
			'params' => [
				'BaseClass' => [ 'type' => DataType::TYPE_STRING ]
			],
			'debug' => true,
		],
		'getGroups' => [
			'name' => 'Attr_group_GetByClass',
			'params' => [
	            'Class' => [ 'type'=>DataType::TYPE_STRING ],
			],
			'debug' => true,
		],
		'GetIDByName'=> [
			'name' => 'Class_GetIDByName',
			'params' => [
	            'ClassName' => [ 'type'=>DataType::TYPE_STRING ],
			],
			'debug' => true,
		],
		'getAll' => [
			'name' => 'Class_GetTree',
			'params' => [],
			'debug' => true,
		],
		'getNameClass'=> [
            'name' => 'Class_GetNameByID',
            'params' => [
                'ID'      => [ 'type' => DataType::TYPE_STRING ]                
            ],
            'debug' => true,
        ]
	];
  
    public function getNameClass(Core\Type\Type $type){
        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];

        
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'ID'   => $type->getUid()
        ]);
        if (!empty($data))
        {
            $dataResult['result']=1;
        }
        foreach ($data as $key => $value) {
           	$type->setName($value['Name']);
            $dataResult['data'][]=$type;
        }
        if($dataResult['result']!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }       
        return $dataResult;
    }
	private function _initTypes(array $data) {
		$types = [];
		foreach ($data as $value) {
			if (isset($types[$value['BaseClass']])) {
				$parent = $types[$value['BaseClass']];
			} else {
				if ($value['BaseClass'] == Core\Type\Type::TYPE_ROOT) {
					//TODO: Replace root name with i18n translation
					$parent = new Core\Type\Type($value['BaseClass'], 'Root', true);
				} else {
					$parent = new Core\Type\Type($value['BaseClass'], '', false);
				}
				$types[$value['BaseClass']] = $parent;
			}
			if (isset($types[$value['ID']])) {
				if ($value['ID'] != Core\Type\Type::TYPE_ROOT) {
					$types[$value['ID']]->setName($value['Name'])->setIsSystem((bool)$value['IsSystem']);
					if (is_null($types[$value['ID']]->getParent())) {
						$types[$value['ID']]->setParent($parent);
					}
				}
			} else {
				if ($value['ID'] == Core\Type\Type::TYPE_ROOT) {
					//TODO: Replace root name with i18n translation
					$types[$value['ID']] = new Core\Type\Type($value['ID'], 'Root', true);
				} else {
					$types[$value['ID']] = new Core\Type\Type($value['ID'], $value['Name'], (bool)$value['IsSystem'], $parent);
				}
			}
		}
		return $types;
	}

	private function _initTypesTree(array $types, array $parent, array $ids_closest, array $calcs = [])
	{
		$tree = [];
		foreach ($types as $type) {
			if (empty($parent)) {
				$_parent = null;
			} else {
				if ($type['BaseClass'] == Core\Type\Type::TYPE_ROOT) {
					$_parent = new Core\Type\Type($type['BaseClass'], 'Root', true);
				} else {
					$_parent = new Core\Type\Type($parent['ID'], $parent['Name'], $parent['IsSystem']);
				}
			}

			$children = [];
            if (!empty($type['Children'])) {
                if(!in_array($type['ID'], $ids_closest)) {
                    array_push($ids_closest, $type['ID']);
                    $children = $this->_initTypesTree($type['Children'], $type, $ids_closest, $calcs);

                }
                else {
                    $type['Name'] = $type['Name'] . '(R)';
                }
            }

			$_type = new Core\Type\Type($type['ID'], $type['Name'], $type['IsSystem'], $_parent);
			$this->setCalcs($_type, $calcs);
			

            if(count($children) > 0 ) {
                $_type->setChildren($children);
            }

			$tree[] = $_type;
		}
		return $tree;
	}

	private function setCalcs(Core\Type\Type $type, array $calcs) {
		$id = $type->getUid();
		foreach ($calcs as $calc) {
			if ($calc->getType()->getUid() == $id) {
				$type->addCalculation($calc);
			}
		}
	}

	public function GetIDByName(Core\Type\Type $type) {

		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'ClassName' => '{'.$type->getName()."}",
		]);

		$result = -1;
		if(!empty($data)) {
			$result = 1;
				$type->setUid($data[0]['IDc']);
		}

		return $result;
	}

	public function add(Core\Type\Type $type, User $user) 
	{
		$flagErr=false;
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        $result = -1;
        $codeText='';

		$inheritedFrom = is_null($type->getBaseType()) ? null : $type->getBaseType()->getUid();
		$className=$type->getName();
		if ($className=='') 
        {
            $flagErr=true;
            $result=-3;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
                'details'=>'Не введено имя класса',
                'codeID'=>'24c46420-e5e6-487b-a7ba-118c232a9bde',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        if (!$flagErr) 
        {
        	$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
				'ClassName' => $className,
				'BaseClass' => $type->getParent()->getUid(),
				'IsSystem' => (int)$type->isSystem(),
				'InheritedFromClass' => $inheritedFrom,
				'UserID' => $user->getUid()
			]);

			if (!empty($data))
            {
                $result = $data[0]['Rcode'];
                if ($result == 1) 
                {
                    $type->setUid($data[0]['IDc']);
                }else
                {
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
        if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$className],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
	}

	public function rename(Core\Type\Type $type, User $user) 
	{
		$flagErr=false;
		$codeText='';
        $dataResult=['error'=>[],'result'=>-1,'data'=>[]];

        $className=$type->getName();
		if ($className=='') 
        {
            $flagErr=true;
            $result=-3;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
                'details'=>'Не введено имя класса',
                'codeID'=>'35eefba1-011f-4829-9ff5-5bb0ee7dfbf6',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        if(!$flagErr)
        {
        	$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
				'ClassName' =>$className,
				'IDc' => $type->getUid(),
		   		'UserID' => $user->getUid()
			]);
			if (!empty($data))
            {
                $result = $data[0]['Rcode'];
                if ($result != 1) 
                {                    
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
		if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$className],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
	}

	
	public function addLink(Core\Type\Type $type, User $user) {
		$dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;

		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'ChClass' => $type->getUid(),
	   		'BsClass' =>$type->getParent()->getUid(),
	   		'UserID'=> $user->getUid()
		]);

		
		if (!empty($data))
        {           
            $result=$data[0]['Rcode'];
            $dataResult['result']= $result;           
        }
        if ($result!=1) {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
	}

	public function remove(Core\Type\Type $type, User $user) 
	{
		$dataResult=['error'=>[],'result'=>-1,'data'=>[]];
        $result=-1;
        $codeText='';
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'ID' => $type->getUid(),
	   		'UserID' => $user->getUid(),
	   		'BsClass'=>$type->getParent()->getUid()
		]);
		
		if (!empty($data))
        {           
            $result=$data[0]['Rcode'];
            if ($result != 1) 
            {                    
                $codeText=$data[0]['ErrCode'];
            }
            $dataResult['result']= $result;           
        }
        if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
	}

	public function getChildren(Core\Type\Type $type) {
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'BaseClass' => $type->getUid()
		]);

		$types = [];
		foreach ($data as $value) {
			$types[] = new Core\Type\Type($value['IDc'], $value['ClassName'], (bool)$value['IsSystemClass'], $type);
		}

		return $types;
	}

	public function getGroups(Core\Type\Type $type) {
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'Class' => $type->getUid()
		]);

		$groups = [];
		//TODO: Replace main group name with i18n translation
    	$groups[] = new AttributeGroup(AttributeGroup::ATTRIBUTE_GROUP_ROOT, 'Main', []);
	    foreach ($data as $value) {
      		$groups[] = new AttributeGroup($value['ID'], $value['Name'], []);
	    }

	    return $groups;
	}

	public function getAttributes() {
		return [];
	}

	public function getAll($rawData = false) {
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], []);

		if ($rawData) {
			$types = $data;
		} else {
			$types = $this->_initTypes($data);
		}
		return $types;
	}

	public function getTree() {
		$data = $this->getAll(true);

		$types = [];
		$children = [];
		$typesUids = array_column($data, 'ID');
		foreach($data as &$item) {
        	$children[$item['BaseClass']][] = &$item;
        }
        unset($item);

        foreach($data as &$item) {
            if (isset($children[$item['ID']]))
                $item['Children'] = $children[$item['ID']];
        }

		//TODO: Replace root name with i18n translation
        $root = new Core\Type\Type(Core\Type\Type::TYPE_ROOT, 'Root', true);
		$calcs = Calculation::getCalcClasses();

		$tree = isset($children[Core\Type\Type::TYPE_ROOT]) ? $this->_initTypesTree($children[Core\Type\Type::TYPE_ROOT], [], [], $calcs) : [];
		$root->setChildren($tree);
		$this->setCalcs($root, $calcs);
		return [$root];
	}
}
