<?php

namespace Dzeta\Models;
use Phalcon\Mvc\Model;
use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;

class Log extends Model
{
	const PROCEDURES = [
		'addLogError' => 
		[
			'name' => 'Err_Add',
            'params' => 
            [
            	'UserID'	=> [ 'type' => DataType::TYPE_STRING ],
            	'Details'	=> [ 'type' => DataType::TYPE_JSON ]
            ],
            'debug' => true,
		]
	];

	/**
	* Add error log
	*
	* @param array      		 $logData
	* @param Core\User\User      $user
	*
	* @return string $codeIDe
	*/
	public static function addLogError(array $logData,\Dzeta\Core\User\User $user)
	{
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [            
            'UserID' => $user->getUid(),
            'Details' => $logData
        ]);
		$codeIDe='_dzErr_';
        if(isset($data[0])&&isset($data[0]['IDe']))
        {
        	$codeIDe=$data[0]['IDe'];
        }
        return $codeIDe;
	}
}