<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\User\User;
use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

use DzetaCalcParser\DzetaCalc;

class Calculation extends Model
{
    const PROCEDURES = [
        'add' => [
            'name' => 'Calc_AddNew',
            'params' => [
                'Name'    => [ 'type' => DataType::TYPE_STRING ],
                'Class' => [ 'type' => DataType::TYPE_STRING ],
                'Settings'=> [ 'type' => DataType::TYPE_STRING ],
                'UserID'  => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'edit' => [
            'name' => 'Calc_Edit',
            'params' => [
                'ID'    => [ 'type' => DataType::TYPE_STRING ],
                'Name' => [ 'type' => DataType::TYPE_STRING ],
                'Settings'=> [ 'type' => DataType::TYPE_STRING ],
                'UserID'  => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'remove' => [
            'name' => 'Calc_Delete',
            'params' => [
                'ID' => [ 'type' => DataType::TYPE_STRING ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'get' => [
            'name' => 'Calc_GetInfo',
            'params' => [
                'ID'    => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'getCalcClasses' => [
            'name' => 'Calc_GetCalcByClass',
            'params' => [
                'ID'    => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'argAttrAdd' => [
            'name' => 'Calc_arguments_add',
            'params' => [
                'Args'    => [ 'type' => DataType::TYPE_JSON ],
                'Calc'    => [ 'type' => DataType::TYPE_STRING ],
                'UserID'    => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        //

    ];

    /**
    *
    *
    */
    public function argAttrAdd($attrData,Core\Calculation $calc, User $user){

        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];

        $attrJson=[];
        foreach ($attrData as $key => $value) 
        {
            $attrJson[]=[
                'Attr'=>$value['Attr']->getUid(),
                'Group'=>$value['Group']
            ];
        }
        $codeText='';

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'Args'   =>$attrJson,
          'Calc' =>$calc->getUid(),
          'UserID'=> $user->getUid(),
        ]);
        if (!empty($data)) 
        {
            $result = $data[0]['Rcode'];
            if ($result != 1) 
            {               
                $codeText=$data[0]['ErrCode'];
            }
        }
        if($result!=1)
        {
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;       
        return $dataResult;
    }

    /**
  	 * Add calc
     *
  	 * @param Core\Calculation $calc
  	 * @param Core\User\User   $user
  	 *
  	 * @return ['error'=>[],'result'=>[],'data'=>[]]
  	 */
    public function add(Core\Calculation $calc, User $user)
    {
        $flagErr=false;
        $formula=$calc->getFormula();
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];

        $codeText='';

        if($formula=='')
        {            
            $flagErr=true;
            $result=-6;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-6,
                'details'=>'Не введена формула',
                'codeID'=>'b6c834e4-774f-4e48-87c1-6fe3ea5857cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        $name= $calc->getName();
        if($name=='')
        {
            $flagErr=true;
            $result=-2;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
                'details'=>'Не заполнена имя расчета',
                'codeID'=>'b6c834s5-774f-4e48-87c1-6fe3ea5857cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        $sett=['fOrig'=>$formula ];
        $result = -1;
        if(!$flagErr)
        {
            $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                  'Name'   => $name,
                  'Class' =>$calc->getType()->getUid(),
                  'Settings'=> json_encode($sett),
                  'UserID' => $user->getUid()
            ]);
            if (!empty($data)) 
            {
                $result = $data[0]['Rcode'];
                if ($result == 1) 
                {
                    $calc->setUid($data[0]['IDc']);
                }else
                {
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
        if($result!=1)
    	{    	   
           $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$name],
                null,
                $codeText
            )];
    	}
        $dataResult['result']=$result;
        return $dataResult;
    }
    /**
  	 * Edit calc
     *
  	 * @param Core\Calculation $calc
  	 * @param Core\User\User   $user
  	 *
  	 * @return ['error'=>[],'result'=>[],'data'=>[]]
  	 */
    public function edit(Core\Calculation $calc, User $user)
    {
        $flagErr=false;
        $formula=$calc->getFormula();
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        if($formula=='')
        {
            $flagErr=true;
            $result=-6;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-6,
                'details'=>'Не введена формула',
                'codeID'=>'b6c834e4-774f-4e48-87c1-6fe3ea5857cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        $name= $calc->getName();
        if($name=='')
        {
            $flagErr=true;
            $result=-2;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
                'details'=>'Не заполнена имя расчета',
                'codeID'=>'b6c834s5-774f-4e48-87c1-6fe3ea5857cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        $sett=['fOrig'=>$formula];
        $result = -1;
        if(!$flagErr)
        {
            $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'ID'=>$calc->getUid(),
                'Name'   => $calc->getName(),
                'Settings'=> json_encode($sett),
                'UserID' => $user->getUid()
            ]);
            if (!empty($data)) 
            {
                $result = $data[0]['Rcode'];
                if ($result == 1) 
                {
                    if (!empty($data[0]['IDc']))
                    {
                        $calc->setUid($data[0]['IDc']);
                    }
                }else
                {
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
        if($result!=1)
        {          
           $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$name],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }

    public static function remove(Core\Calculation $calc, User $user)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'ID' => $calc->getUid(),
            'UserID' => $user->getUid(),
        ]);
        $result = ['data' => [], 'error' => [], 'result' => -1];
        if (!empty($data)) {
            if ($data[0]['Rcode'] != 1)  {
                $result['error'][] = [
                    'text' => DbError::getError(
                        self::PROCEDURES[__FUNCTION__]['name'],
                        $data[0]['Rcode'],
                        [],
                        null,
                        $data[0]['ErrCode']
                    )
                ];
            }
            $result['result'] = $data[0]['Rcode'];
        }

        return $result;
    }

    public static function getCalcClasses(?Core\Type\Type $type = null)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'ID' => !empty($type) ? $type->getUid() : null
        ]);
        $result = -1;
        $calcs = [];
        if (!empty($data)) {
            $types = [];
            foreach ($data as $key => $value) {
                $calc = new Core\Calculation($value['IDc'], empty($value['Name']) ? '' : $value['Name']);
                if (empty($type)) {
                    if (!isset($types[$value['Class']])) {
                        $types[$value['Class']] = new Core\Type\Type(!empty($value['Class']) ? $value['Class'] : Core\Type\Type::TYPE_ROOT, '');
                    }
                    $calc->setType($types[$value['Class']]);
                }
                $calcs[] = $calc;
            }
        }
        if ($type) {
            $type->setCalculations($calcs);
        }

        return $calcs;
    }


    public function get(Core\Calculation $calc)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'ID' => !empty($calc) ? $calc->getUid() : null
        ]);
        $result = -1;
        if (!empty($data)) {
            $result = 1;
            //$calc->setUid($data[0]['IDc']);
            $calc->setUid($data[0]['IDc']);
            $name = is_null($data[0]['Name']) ? '' : $data[0]['Name'];
            $calc->setName($name);
            $json = $data[0]['Settings'];
            $settings = json_decode($json, true);
            $formula = isset($settings['fOrig']) ? $settings['fOrig'] : '';
            $calc->setFormula($formula);
        }

        return $result;
    }


    public function paintCellTable(Core\Calculation $calc, $currnetCell, $attrs, $data){
        //тут все получаем, после считаем и возвращаем массив
        $attrsData = [];
        $curCell = null;

        foreach ($data as $key => $value)
        {
            if ($key == 'ID' || $key == 'Num') continue;
            $attrUID = $key;
            if (!empty($value))
            {
                if (isset($attrs[$attrUID]))
                {
                    $value['v'] = isset($value['v']) ? $value['v'] : null;
                    $attr = $attrs[$attrUID]['attr'];
                    if (!$attr->isArray() && \Dzeta\Core\Type\DataType::TYPE_FILE != $attr->getDatatype())
                    {
                        if(\Dzeta\Core\Type\DataType::TYPE_OBJECT == $attr->getDatatype())
                        {
                            if (isset($value['ln']))
                            {
                                $attr->setValue(['value' => $value['v'], 'name' => $value['ln']]);
                            } else
                            {
                                $attr->setValue(['value' => $value['v']]);
                            }
                        } else
                        {
                            $attr->setValue(['value'=>$value['v']]);
                        }
                    }
                    $attrsData[$value['o']][] = $attr;
                    if ($currnetCell['attr'] == $attrUID)
                    {
                        $curCell = new Core\Instance($value['o'], '');
                        $curCell->setAttributes([$attr]);
                    }
                }
            }
        }

        $objs = [];
        foreach ($attrsData as $key => $value) {
            $obj = new Core\Instance($key, '');
            $obj->setAttributes($value);
            $objs[] = $obj;
        }
        $dzeta = new DzetaCalc([
            'formula' => $calc->getFormula(),
            'databaseCache' => $objs,
            'arguments' => [
                ['variable' => '$cellValue', 'value' => $curCell]
            ]
        ]);

        $dzeta->start();
        $lastValue = $dzeta->getResult();//получаем последний ответ
        // if($lastValue->Type=='Color')
        // {
        //     return 1;
        // }
        return $lastValue;
    }


    public function calcListValue(Core\Calculation $calc,$curObject,$dopDataObj=[])
    {
        $uidsObj=[];
        $objs = [];
        $objs[]=$curObject;
        foreach ($dopDataObj as $key=>$valueDopData) 
        {
            $objs[]=$valueDopData;
        }       

        $dzeta = new DzetaCalc([
            'formula' => $calc->getFormula(),
            'databaseCache' => $objs,
            'object'=>$curObject
        ]);
        $dzeta->start();
        $lastValue = $dzeta->getResult();
        
        if(empty($lastValue))
        {
            return ['result'=>-1];
        }
        return \DzetaCalcParser\Core\Beautify::formatCalcListData($lastValue);
    }
}
