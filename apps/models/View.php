<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\User\User;
use Dzeta\Core\Type\DataType;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class View extends Model
{
    const PROCEDURES = [
        'add' => [
            'name' => 'View_AddNew',
            'params' => [
                'Name'      => [ 'type' => DataType::TYPE_STRING ],
                'App'       => [ 'type' => DataType::TYPE_STRING ],
                'ParentV'   => [ 'type' => DataType::TYPE_STRING ],
                'User'      => [ 'type' => DataType::TYPE_STRING ],
                'Settings'  => [ 'type' => DataType::TYPE_JSON ],
                'TableOrForm'=> [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
      'edit'=> [
              'name' => 'View_EditNameSettings',
              'params' => [
                  'ID'         => [ 'type' => DataType::TYPE_STRING ],
                  'Name'       => [ 'type' => DataType::TYPE_STRING ],
                  'Settings'   => [ 'type' => DataType::TYPE_JSON ],
                  'UserID'     => [ 'type' => DataType::TYPE_STRING ],
                  'TableOrForm'=> [ 'type' => DataType::TYPE_STRING ],
              ],
              'debug' => true,
          ],
    'remove' => [
            'name' => 'View_delete',
            'params' => [
                'Views'     => [ 'type' => DataType::TYPE_STRING ],
                'UserID'    => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
    'getAttributes'=> [
            'name' => 'View_attr_GetAttrsForView_Table',
            'params' => [
                'View'     => [ 'type' => DataType::TYPE_STRING ],
                'ViewFilter' => [ 'type' => DataType::TYPE_JSON ],
                'ParentView'     => [ 'type' => DataType::TYPE_STRING ],
                'SelectObjs'    => ['type' => DataType::TYPE_ARRAY],
                'User' => [ 'type' => DataType::TYPE_STRING ],
                'ParentObj' => [ 'type' => DataType::TYPE_STRING ],
                'ForView' => [ 'type' => DataType::TYPE_NUMBER ],
            ],
            'debug' => true,
        ],
    'addViewClass'=>
    [
      'name' => 'View_class_Add',
            'params' => [
                'View'      => [ 'type' => DataType::TYPE_STRING ],
                'App'       => [ 'type' => DataType::TYPE_STRING ],
                'Classes'   => [ 'type' => DataType::TYPE_STRING ],
                'User'      => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
    ],
    'getViewClass'=> [
            'name' => 'View_class_GetClassForView',
            'params' => [
                'View'     => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
    'addAttributes'=> [
            'name' => 'View_attr_Add',
            'params' => [
                'View'     => [ 'type' => DataType::TYPE_STRING ],
                'Attrs'     => [ 'type' => DataType::TYPE_STRING ],
                'User'     => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],

      'View_GetData'=> [
          'name' => 'View_GetData',
          'params' => [
              'View'          => ['type' => DataType::TYPE_STRING ],
              'SortAttr'   	  => ['type' => DataType::TYPE_JSON ],
              'Start'         => ['type' => DataType::TYPE_NUMBER ],
              'Num'           => ['type' => DataType::TYPE_NUMBER ],
              'Filter'        => ['type' => DataType::TYPE_JSON],
              'GetValsForAttr'=> ['type' => DataType::TYPE_STRING ],
              'Order'         => ['type' => DataType::TYPE_NUMBER ],
              'ParentView'    => ['type' => DataType::TYPE_STRING],
              'SelectObjs'    => ['type' => DataType::TYPE_ARRAY],
              'User'          => ['type' => DataType::TYPE_STRING],
              'ParentObj'     => ['type' => DataType::TYPE_STRING],
              'GetCount'      => ['type' => DataType::TYPE_NUMBER],
              'NumOfObjOfMainClass'     => ['type' => DataType::TYPE_NUMBER],
          ],
          'debug' => true,
      ],
        'View_GetData_with_searchparam'=> [
            'name' => 'View_GetData_with_searchparam',
            'params' => [
                'View'          => ['type' => DataType::TYPE_STRING ],
                'SortAttr'   	  => ['type' => DataType::TYPE_JSON ],
                'Start'         => ['type' => DataType::TYPE_NUMBER ],
                'Num'           => ['type' => DataType::TYPE_NUMBER ],
                'Filter'        => ['type' => DataType::TYPE_JSON],
                'GetValsForAttr'=> ['type' => DataType::TYPE_STRING ],
                'Order'         => ['type' => DataType::TYPE_NUMBER ],
                'ParentView'    => ['type' => DataType::TYPE_STRING],
                'SelectObjs'    => ['type' => DataType::TYPE_ARRAY],
                'User'          => ['type' => DataType::TYPE_STRING],
                'ParentObj'     => ['type' => DataType::TYPE_STRING],
                'GetCount'     => ['type' => DataType::TYPE_NUMBER],
                'NumOfObjOfMainClass'     => ['type' => DataType::TYPE_NUMBER],
                'HatID'     => ['type' => DataType::TYPE_NUMBER],
            ],
            'debug' => true,
        ],
      'addAttrrOrder'=> [
            'name' => 'View_attr_AddOrderForAttrs',
            'params' => [
                'View'         => [ 'type' => DataType::TYPE_STRING ],
                'Order'   		 => [ 'type' => DataType::TYPE_STRING ],
                'User'    => [ 'type' => DataType::TYPE_NUMBER ]
            ],
            'debug' => true,
        ],
        'View_GetChildren'=> [
            'name' => 'View_GetChildren',
            'params' => [
                'View'         => [ 'type' => DataType::TYPE_STRING ],
                'GelOnly1Level' => [ 'type' => DataType::TYPE_NUMBER ]
            ],
            'debug' => true,
        ],
        'GetParrentsToRoot'=> [
            'name' => 'View_GetParrentsToRoot',
            'params' => [
                'View'         => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'GetParrentViews'=> [
            'name' => 'View_GetParrentViews',
            'params' => [
                'View'         => [ 'type' => DataType::TYPE_STRING ],
                'TableOrForm'=> [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'GetInfoView'=> [
            'name' => 'View_GetInfo',
            'params' => [
                'View'         => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
      //
    ];


    /**
     * GetInfoView view
     *
     * @param Core\App\View        $view
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function GetInfoView(Core\App\View $view)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'View'    => $view->getUid()
        ]);
        if (!empty($data))
        {
            $data=$data[0];
            $result=1;
            $dataResult['result']=1;
            $view->setName($data['Name']);
            $sett=json_decode($data['settings'],true);
             $sett['table_or_form']=is_null($data['TableOrForm'])? 0:$data['TableOrForm'];
            $view->setSetting($sett);
        }
        if ($result!=1) {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }

    /**
     * GetParrentsToRoot view
     *
     * @param Core\App\View        $view
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function GetParrentsToRoot(Core\App\View $view)
    {
        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'View'    => $view->getUid()
        ]);
        if (!empty($data))
        {
            foreach ($data as $key => $value)
            {
                if($value['AppOrView']==0)//app
                {
                  $dataResult['data'][]=  new Core\App\App($value['ID'], $value['Name']);
                }elseif($value['AppOrView']==1)//view
                {
                  $dataResult['data'][]=  new Core\App\View($value['ID'], $value['Name'], []);
                }
            }
        }
        return $dataResult;
    }


    /**
     * View_GetParrentViews view
     *
     * @param Core\App\View        $view
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function GetParrentViews(Core\App\View $view, $tableORForm=null)
    {
        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'View'    => $view->getUid(),
          'TableOrForm'=>$tableORForm
        ]);
        if (!empty($data))
        {
            foreach ($data as $key => $value)
            {
                $dataResult['data'][]=  new Core\App\App($value['ID'], $value['Name']);
            }
        }

        return $dataResult;
    }




    public function View_GetChildren($application, $view_uid, $GelOnly1Level=1)
    {
        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];

        $data = Db::runProcedure(self::PROCEDURES['View_GetChildren'], [
          'View'    => $view_uid,
          'GelOnly1Level' => $GelOnly1Level
        ]);

        $views=[];
        foreach ($data as  $viewDB) {
          $sett=[];
          if(!empty($viewDB['Settings']))
          {
              $sett=json_decode($viewDB['Settings'],true);
          }
          $sett=empty($sett)? []:$sett;
          $views[]=new Core\App\View($viewDB['ID'], $viewDB['Name'],$sett);
        }
        $application->setViews($views);

        return $views;
    }



    /**
     * Add view
     *
     * @param Core\App\View        $view
     * @param Core\User\User      $user
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function add(Core\App\View $view, User $user)
    {
        $dataResult=['error'=>[],'result'=>-1,'data'=>[]];
        $result = -1;
        $flagErr=false;
        $codeText='';
        $viewName=$view->getName();
        if ($viewName=='') 
        {
          $flagErr=true;
          $result=-3;
          $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
            'details'=>'Не введено имя представления',
            'codeID'=>'7112e3b4-3312-453b-ae3b-190c883d36f2',
            'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        if (!$flagErr) 
        {
          $sett=$view->getSettings();
          $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Name'    => $viewName,
            'App'     => $view->getParentApp()->getUid(),
            'ParentV' => $view->getParentView()->getUid(),
            'User'    => $user->getUid(),
            'Settings'=> $sett,
            'TableOrForm'=>$sett['table_or_form']
          ]);
          
          if (!empty($data)) 
          {
            $dataResult['result'] = $data[0]['Rcode'];
            if($dataResult['result'] == 1) 
            {
              $view->setUid($data[0]['IDv']);
            }else
            {
              $codeText=$data[0]['ErrCode'];
            }
          }
        }

        if ($dataResult['result']!=1) 
        {
          $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $dataResult['result'],
                [0=>$viewName],
                null,
                $codeText
          )];
        }
        return $dataResult;
    }
    /**
     * Edit view
     *
     * @param Core\App\View        $view
     * @param Core\User\User      $user
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function edit(Core\App\View $view, User $user)
    {
      $codeText='';
      $dataResult=['error'=>[],'result'=>-1,'data'=>[]];
      $flagErr=false;
      $viewName=$view->getName();
      if ($viewName=='') 
      {
        $flagErr=true;
        $result=-3;
        $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
          'details'=>'Не введено имя представления',
          'codeID'=>'d4076084-34b1-44d7-810c-9381ed9e6319',
          'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
      }
      if (!$flagErr) 
      {
        $sett= $view->getSettings();

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'ID'        => $view->getUid(),
          'Name'      => $viewName,
          'Settings'  => $sett,
          'UserID'    => $user->getUid(),
          'Settings'=> $sett,
          'TableOrForm'=>$sett['table_or_form']
        ]);
        if (!empty($data)) 
        {
          $dataResult['result'] = $data[0]['Rcode'];
          if($dataResult['result'] != 1) 
          {
            $codeText=$data[0]['ErrCode'];
          }
        }
      }
      if ($dataResult['result']!=1) 
      {
        $dataResult['error'][]=['text'=>DbError::getError(
            self::PROCEDURES[__FUNCTION__]['name'], 
            $dataResult['result'],
            [0=>$viewName],
            null,
            $codeText
        )];
      }      
      return $dataResult;
    }
    /**
		 * remove views
		 *
		 * @param Core\App\View[]        $views
		 * @param Core\User\User      $user
		 *
		 * @return ['error'=>[],'result'=>[],'data'=>[]]
		 */
    public function remove(array $views, User $user)
    {
      $dataResult['error']=[];
      $dataResult['result']=-1;
      $dataResult['data']=[];
      $uids=[];
      foreach ($views as $view) {
        $uids[]=$view->getUid();
      }
      $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
        'Views'     => '{'.implode(',',$uids).'}',
        'UserID' => $user->getUid()
      ]);

      $result = -1;
      if (!empty($data)) {
          foreach ($data as $key => $value) {
            $dataResult['data'][]=['view'=>$value['View'],'result'=>$value['Rcode']];
            if($value['Rcode']!=1)
            {
                $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result),'view'=>$value['View']];
            }
          }
          if(empty($dataResult['error']))
          {
              $result=1;
          }
      }
      if ($result==-1) {
          $dataResult['error']=[];
          $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
      }
      $dataResult['result']=$result;
      return $dataResult;
    }



    public function getDataTable(Core\App\View $view, $UserID)
    {
        $data=$this->View_GetData($view,$UserID);

        $json_table = '';

        foreach ($data as $item) {
            if (isset($item["dt"])) {
                $json_table .= $item["dt"];
            }
        }
        $_calc=new Calculation();
        $header_data = $view->getDopSett('header_data');
        $exe_data_source_method = $view->getDopSett('exe_data_source_method');
        $header_data=empty($header_data)? []:$header_data;
        $attrParamsPaint=[];
        $attrListCalcs = [];
        //получили шапку для краски, там же заоодно и получаем расчет на краску к ячейке
        for ($i = 0, $len = count($header_data); $i < $len; $i++) {
            if(isset($header_data[$i]['data']["name"])) {
                $attrParamsPaint[$header_data[$i]['data']['uid']] = ['attr' => new Core\Type\Attribute(
                    $header_data[$i]['data']['uid'],
                    $header_data[$i]['data']['name'],
                    \Dzeta\Core\Type\DataType::getIndexFromType($header_data[$i]['data']['datatype']),
                    false,
                    (bool)$header_data[$i]['data']['isArray'])];
                // TODO-test
                if (!empty($header_data[$i]['data']['calc_color'])) {
                    $calc = new Core\Calculation($header_data[$i]['data']['calc_color'], '', '');
                    $_calc->get($calc);
                    $attrParamsPaint[$header_data[$i]['data']['uid']]['calc'] = $calc;
                }
                if (!empty($header_data[$i]['data']['LinkedClass']['uid'])) {

                    $attrParamsPaint[$header_data[$i]['data']['uid']]['attr']->setParent(new Core\Type\Type($header_data[$i]['data']['LinkedClass']['uid'], ''));
                }
                if (!empty($header_data[$i]['data']['attr']['dopSett']['data_source']['uid']) && $exe_data_source_method == 1) {
                    $calc = new Core\Calculation($header_data[$i]['data']['attr']['dopSett']['data_source']['uid'], '', '');
                    $_calc->get($calc);
                    $attrListCalcs[$header_data[$i]['data']['uid']]['calc'] = $calc;
                }
            }
        }

        $result = -1;
        $allCount = -1;
        $isLoadTotal = false;
        $data = [];
        $pStart=$view->getDopSett('start');
        if ($json_table) {
            $n_data = json_decode($json_table, true);

            if ($pStart == 1) {
                $allCount = $n_data[0]["Num"];
                if(isset($n_data[0]["NumMainObj"])) {
                    $allCount = $n_data[0]["NumMainObj"];
                }
                $isLoadTotal = true;
            }

            $i = 0;
            $_calc=new Calculation();
            
            foreach ($n_data as $keyR => $row) {
                $data[$i] = [];
                $flagColorRow=null;
                $is_disabled_td = false;

                if(isset($row["edit"]) && $row["edit"] == 0) 
                {
                    $is_disabled_td = true;
                }
                //массив для создания кэша объектов данных для расчета
                if($exe_data_source_method == 1) {
                    $cacheCalcObj=[];
                    foreach ($row as $keyC => $cell)
                    {
                        if ($keyC !== "ID" && $keyC !== "Num"&& $keyC !== "NumMainObj" && $keyC !== "edit")
                        {
                            if(!empty($cell['o'])&&isset($cell['v']))
                            {
                                if(!isset($cacheCalcObj[$cell['o']]))
                                {
                                    $cacheCalcObj[$cell['o']]=[];
                                }
                                $attrObj=$attrParamsPaint[$keyC]['attr'];

                                if(!$attrObj->isArray())
                                {
                                    switch ($attrObj->getDatatype())
                                    {
                                        case DataType::TYPE_TEXT:
                                        case DataType::TYPE_NUMBER:
                                        case DataType::TYPE_DATETIME:
                                            $attrObj->setValue(['value'=>$cell['v']]);
                                            $cacheCalcObj[$cell['o']][]=$attrObj;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    $cacheCalcObjn=[];
                    foreach ($cacheCalcObj as $keyUid => $valueAttr)
                    {
                        $objt=new Core\Instance($keyUid, '');
                        $objt->setAttributes($valueAttr);
                        $cacheCalcObjn[]=$objt;
                    }
                    unset($cacheCalcObj);

                }


                foreach ($row as $keyC => $cell) 
                {
                    if ($keyC !== "ID" && $keyC !== "Num"&& $keyC !== "NumMainObj" && $keyC !== "edit")
                    {
                        if(empty($cell)) 
                        {
                            $cell = ["o" => null];
                        }
                        //TODO для теста пока
                       
                        if(isset($attrListCalcs[$keyC])&&!empty($cell['o']) && $exe_data_source_method == 1)
                        {
                            $r=$_calc->calcListValue($attrListCalcs[$keyC]['calc'],new Core\Instance($cell['o'], ''),$cacheCalcObjn);
                            
                            if($r['result']==1)
                            {
                                $cell['list']=$r['data'];
                            }                            
                        }
                        $cell["is_disabled"] = $is_disabled_td;
                        $data[$i][$keyC] = $cell;
                        if(!empty($attrParamsPaint[$keyC]['calc']))
                        {
                        //  $data[$i][$keyC]['color']= 'red';
                          $CellCalColor=$_calc->paintCellTable(
                          $attrParamsPaint[$keyC]['calc'],
                            ['attr'=>$keyC,'obj'=>$cell['o']],
                            $attrParamsPaint,
                            $row);

                          if(isset($CellCalColor['value']))
                          {
                            if(!empty($CellCalColor['value']['color']))
                            {
                              if(!empty($CellCalColor['value']['color']['value']))
                              {
                                $data[$i][$keyC]['color']=$CellCalColor['value']['color']['value'];
                              }

                            }
                            if(!empty($CellCalColor['value']['backGround']))
                            {
                              if(!empty($CellCalColor['value']['backGround']['value']))
                              {
                                $data[$i][$keyC]['background']=$CellCalColor['value']['backGround']['value'];
                              }
                            }
                            if(!empty($CellCalColor['value']['row']))
                            {
                                if(!empty($CellCalColor['value']['color']))
                                {
                                  if(!empty($CellCalColor['value']['color']['value']))
                                  {
                                    $flagColorRow['color']=$CellCalColor['value']['color']['value'];
                                  }
                                }
                                if(!empty($CellCalColor['value']['backGround']))
                                {
                                  if(!empty($CellCalColor['value']['backGround']['value']))
                                  {
                                    $flagColorRow['background']=$CellCalColor['value']['backGround']['value'];
                                  }
                                }

                            }

                          }

                        }
                        //тут вызываем краску
                      //
                    }
                }
                if(!empty($flagColorRow))
                {
                    $data[$i]["row_number"]=[];
                    foreach ($data[$i] as $keyClac=>  $valTRColor)
                    {
                        if(empty($valTRColor['color'])&&!empty($flagColorRow['color']))
                        {
                          $data[$i][$keyClac]['color']=$flagColorRow['color'];
                        }
                        if(empty($valTRColor['background'])&&!empty($flagColorRow['background']))
                        {
                          $data[$i][$keyClac]['background']=$flagColorRow['background'];
                        }
                    }

                }
                $i++;
            }

            #region merge attr
            if($header_data) {
                for($i = 0; $i < count($data); $i++) {
                    foreach ($data[$i] as $key=>$item) {
                        if($i == 0 || empty($data[$i][$key]["o"]) || $data[$i - 1][$key]["o"] != $data[$i][$key]["o"] ) {
                            $data[$i][$key]["is_merge"] = 0;
                        }
                        else {
                            $data[$i][$key]["is_merge"] = 1;
                        }
                    }
                    $index_a = $i + $pStart;
                    if(isset($data[$i]["row_number"]))
                    {
                      $data[$i]["row_number"]['v']= $index_a;
                      $data[$i]["row_number"]['o']= null;
                    }else
                    {
                      $data[$i]["row_number"] = ["v" => $index_a, "o" => null];
                    }

                }
            }

            #endregion


            $result = 1;
        } else {
            $result = 1;

            if ($pStart == 1) {
                $allCount = 0;
                $isLoadTotal = true;
            }
        }

        return [
            "result" => $result,
            "allCount" => $allCount,
            "isLoadTotal" => $isLoadTotal,
            "data" => $data,
        ];
    }

    public function getFilterValueTable(Core\App\View $view, $UserID)
    {
        $data=$this->View_GetData($view, $UserID);
        $json_table = '';
        $dt=[];
        foreach ($data as $item) {
            if (!empty($item["dt"])) {
                $dt=json_decode($item["dt"], true);
            }
        }
        if (empty($dt)) {
            return [];
        }
        if (isset($dt[0]['cnt'])) {
            return [
            'count'=>$dt[0]['cnt'],
            'type'=> 'search'
          ];
        }
        if (isset($dt[0]['V'])) {
            return [
            'data'=>$dt,
            'type'=> 'list'
          ];
        }
        if (isset($dt[0]['minv'])) {
            return [
            'min'=>$dt[0]['minv'],
            'max'=>$dt[0]['maxv'],
            'type'=> 'border'
          ];
        }
    }

    public function View_GetData(Core\App\View $view, $UserID)
    {
        $sortAttr=$view->getDopSett('attrSort');
        $view_sorts=$view->getDopSett('view_sorts');

        $sortAttr=is_null($sortAttr)? null:$sortAttr->getUid();
        $typeSort=is_null($view->getDopSett('sort')) ? null:$view->getDopSett('sort');
        $borderAttr=$view->getDopSett('attrBorder');
        $borderAttr=is_null($borderAttr)? null:$borderAttr->getUid();
        $ParentObj=is_null($view->getDopSett('ParentObj')) ? null:$view->getDopSett('ParentObj');
        $ParentView=is_null($view->getDopSett('ParentView')) ? null:$view->getDopSett('ParentView');
        $ParentObj=is_null($view->getDopSett('ParentObj')) ? null:$view->getDopSett('ParentObj');
        $SelectObjs=is_null($view->getDopSett('SelectObjs')) ? null:$view->getDopSett('SelectObjs');
        $GetCount=is_null($view->getDopSett('GetCount')) ? null:$view->getDopSett('GetCount');

        $HatID=empty($view->getDopSett('HatID')) ? null : $view->getDopSett('HatID');


        $NumOfObjOfMainClass=is_null($view->getDopSett('NumOfObjOfMainClass')) ? null:$view->getDopSett('NumOfObjOfMainClass');

        $order =  $typeSort == 1 ? "desc" : "asc";
        if (!empty($sortAttr)) {
            $sortAttr = [
                ["uid" => $sortAttr, "order" => $order]
            ];
        }
        else {
            $sortAttr = null;
        }

        if(!empty($view_sorts)) {
            $sortAttr = $view_sorts;
        }

        $typeSort = null;

        if(!empty($view->getDopSett('isNewMode'))) {
            $data = Db::runProcedure(self::PROCEDURES['View_GetData_with_searchparam'], [
                'View' => $view->getUid(),
                'SortAttr' => $sortAttr,
                'Start' => $view->getDopSett('start'),
                'Num' => $view->getDopSett('num'),
                'NumOfObjOfMainClass' => $NumOfObjOfMainClass,
                'Filter'=> $view->getDopSett('filter'),
                'GetValsForAttr'=>$borderAttr,
                'Order'=>$typeSort,
                'ParentView' => $ParentView,
                'SelectObjs' => $SelectObjs,
                'User' => $UserID,
                'ParentObj' => $ParentObj,
                'GetCount' => $GetCount,
                'HatID' => $HatID,
            ]);
        }
        else {
            $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'View' => $view->getUid(),
                'SortAttr' => $sortAttr,
                'Start' => $view->getDopSett('start'),
                'Num' => $view->getDopSett('num'),
                'NumOfObjOfMainClass' => $NumOfObjOfMainClass,
                'Filter'=> $view->getDopSett('filter'),
                'GetValsForAttr'=>$borderAttr,
                'Order'=>$typeSort,
                'ParentView' => $ParentView,
                'SelectObjs' => $SelectObjs,
                'User' => $UserID,
                'ParentObj' => $ParentObj,
                'GetCount' => $GetCount,
            ]);
        }

        return $data;
    }



    public function addAttrrOrder(Core\App\View $view, $json, User $user)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
        'View'    => $view->getUid(),
        'Order'   => $json,
        'User'    => $user->getUid()
      ]);
        $result = -1;
        if (!empty($data)) {
            $result = $data[0]['Rcode'];
        }
        return $result;
    }


    public function getAttributes(Core\App\View $view, $UserID, &$dataOut = null)
    {
        $time = microtime(true);
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'View' => $view->getUid(),
            'ViewFilter'=> $view->getDopSett('filter'),
            'ParentView'=> $view->getDopSett('ParentView'),
            'SelectObjs' => $view->getDopSett('SelectObjs'),
            'User' => $UserID,
            'ParentObj' => $view->getDopSett('ParentObj'),
            'ForView' => $view->getDopSett('ForView')
        ]);
        $result = [];

        $dataOut = $data;

        foreach ($data as $key => $value) {
            $type = new \Dzeta\Core\Type\Type($value['Class'], $value['ClassName'], false, new \Dzeta\Core\Type\Type(is_null($value['PClass']) ? \Dzeta\Core\Type\Type::TYPE_ROOT : $value['PClass'], ''));
            $attr = new \Dzeta\Core\Type\Attribute($value['Attrs'], $value['Names'], $value['TypeOfData'], (bool)$value['Key'], (bool)$value['Array']);

            if (!empty($value['Settings'])) {
                $charlistics = json_decode($value['Settings'], true);
                $attr->setCharlistics($charlistics);
            }

            if (!empty($value['Filter'])) {
                $fltr = json_decode($value['Filter'], true);
                $attr->setDopSett('Filter', $fltr);
            }
            $attr->setDopSett('Hide', empty($value['Hide']) ? 0 : $value['Hide']);
            $attr->setDopSett('HatID', empty($value['IDHat']) ? null : $value['IDHat']);
            $attr->setDopSett('HatLv', empty($value['HatLv']) ? null : $value['HatLv']);
            $attr->setDopSett('Hat1LvName', empty($value['Hat1LvName']) ? null : $value['Hat1LvName']);

            $settView = empty($value['Settings_view']) ? [] : json_decode($value['Settings_view'], true);
            $attr->setDopSett('isFixed', 0);
            if (isset($settView['isFixed'])) {
                $attr->setDopSett('isFixed', $settView['isFixed']);
            }
            $attr->setDopSett('alias', '');
            if (isset($settView['alias'])) {
                $attr->setDopSett('alias', $settView['alias']);
            }
            if (isset($settView['width'])) {
                $attr->setDopSett('width', $settView['width']);
            }
            $attr->setDopSett('calc_color', is_null($value['CalcForColoring']) || is_null($value['NameCalcForColoring']) ? null : new Core\Calculation($value['CalcForColoring'], $value['NameCalcForColoring'], ''));
            $attr->setDopSett('data_source', is_null($value['CalcForDataSourse']) || is_null($value['NameCalcForDataSourse']) ? null : new Core\Calculation($value['CalcForDataSourse'], $value['NameCalcForDataSourse'], ''));

            if (!empty($value['LinkedClass']) && $value['LinkedClass'] != \Dzeta\Core\Type\Type::TYPE_ROOT) {
                $attr->setLinkType(new \Dzeta\Core\Type\Type($value['LinkedClass'], ''));
            }
            $attr->setRequare((bool)$value['Require']);


            if (!empty($value['ParentViews'])) {
                $attr->setDopSett('view', new Core\App\View($value['ParentViews'], $value['ParentViewsNames']));
            }
            $attr->setParent($type);

            $result[] = $attr;
        }
        $view->setAttrinutes($result);
        return $result;
    }


    public function addAttributes(Core\App\View $view, $json, User $user)
    {
      $flagErr=false;
      $codeText='';
      $dataResult=['error'=>[],'result'=>-1,'data'=>[]];

      $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
        'View'      => $view->getUid(),
        'Attrs'     => $json,
        'User'      =>$user->getUid()
      ]);      
      if (!empty($data))
      {
        $dataResult['result']=$data[0]['Rcode'];
        if($dataResult['result']!=1)
        {
          $codeText=$data[0]['ErrCode'];
        }
      }
      if ($dataResult['result']!=1) 
      {
        $dataResult['error']=[];
        $dataResult['error'][]=['text'=>DbError::getError(
            self::PROCEDURES[__FUNCTION__]['name'], 
            $dataResult['result'],
            [],
            null,
            $codeText
        )];
      }      
      return $dataResult;
    }

    public function addViewClass(Core\App\View $view, User $user)
    {
      $codeText='';
      $dataResult=['error'=>[],'result'=>-1,'data'=>[]];
      $newArr=[];
      foreach ($view->getClassesLink() as $key => $value) 
      {
        $tmp=[
          'Class'=>$value->getDependentClass()->getUid(),
          'MainClass'=>$value->getMainClass()->getUid()
        ];
        if(!empty($value->getSettings()))
        {
           $tmpSett=$value->getSettings();
           if(!empty($tmpSett->single_mode_attr))
           {
             $tmp['Settings']['single_mode_attr']=$tmpSett->single_mode_attr->getUid();
           }
         if(!empty($tmpSett->single_mode_op))
           {
              $tmp['Settings']['single_mode_op']=$tmpSett->single_mode_op;
           }

        }
        $newArr[]=$tmp;
      }

      $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
        'View'    => $view->getUid(),
        'App'     => $view->getParentApp()->getUid(),
        'Classes' => json_encode($newArr),
        'User'    => $user->getUid()
      ]);        
      if (!empty($data)) 
      {
        $dataResult['result']= $data[0]['Rcode'];
        if($dataResult['result']!=1)
        {
          $codeText=$data[0]['ErrCode'];
        }
      }
      if($dataResult['result']!=1)
      {
        $dataResult['error'][]=['text'=>DbError::getError(
          self::PROCEDURES[__FUNCTION__]['name'], 
          $dataResult['result'],
          [],
          null,
          $codeText
        )];
      }
      return $dataResult;
    }

    public function getViewClass(Core\App\View $view)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'View'    => $view->getUid(),
        ]);
        $result = -1;
        if (!empty($data)) {
            $result =1;
            foreach ($data as $key => $value) {
                $mainClass = new Core\Type\Type($value['MainClass'], $value['MainClassName'], false);
                $class =new Core\Type\Type($value['Class'], $value['Name'], false);

                $tmp=[];
                if(!empty($value['Settings']))
                {
                  $settings=json_decode($value['Settings'],true);
                  if(!empty($settings['single_mode_attr']))
                  {

                    $tmp['single_mode_attr']= new Core\Type\Attribute($settings['single_mode_attr'], '', 1, false, false);
                  }
                  if(!empty($settings['single_mode_op']))
                  {
                    $tmp['single_mode_op']=$settings['single_mode_op'];
                  }

                }
                $ClassesLink=new \Dzeta\Core\App\View\ClassesLink($mainClass,$class,new \Dzeta\Core\App\View\SettClassesLink($tmp));
                /**/
                //$view->addClassesLink($mainClass, $class,empty($value['Settings'])? []:$value['Settings']);
                $view->addClassesLink($ClassesLink);
            }
        }
        return $result;
    }
}
