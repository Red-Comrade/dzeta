<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\User\UserGroup;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Permission extends Model
{
	const PROCEDURES = [
		'getAccessForGroupDB' => [
			'name' => 'Group_access_GetAccessForGroup',
			'params' => [
				'Group'       => [ 'type' => DataType::TYPE_STRING ]
			],
			'debug' => true,
		],
		'addPermissionGroup' => [
			'name' => 'Group_access_Add',
			'params' => [
				'Group'       => [ 'type' => DataType::TYPE_STRING ],
				'Settings'		=> [ 'type' => DataType::TYPE_JSON ],
				'User'				=> [ 'type' => DataType::TYPE_STRING ],
			],
			'debug' => true,
		],
		'getAccessForUserDB' => [
			'name' => 'Group_access_GetAccessForUser',
			'params' => [
				'User'				=> [ 'type' => DataType::TYPE_STRING ],
			],
			'debug' => true,
		],
	];

	private function getAccessForUserDB(Core\User\User $user)
	{
			$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
					'User'=> $user->getUid()
			]);
			$dataResult=[];
			$sett=[];
			if(!empty($data))
			{
					foreach ($data as $key => $value)
					{
							if(!empty($value['Settings']))
							{
								$t=json_decode($value['Settings'],true);
								if(!empty($t))
								{
									$sett[]=$t;
								}
							}
					}
			}
			return $sett;
	}
	public function addPermissionGroup(Core\User\UserGroup $group,Core\User\User $user)
	{
		$dataResult['error']=[];
		$dataResult['result']=-1;
		$dataResult['data']=[];
		$result = -1;
		$codeText='';
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'Group'=>$group->getUid(),
			'Settings'=>$group->getPermisson(),
			'User'=>$user->getUid()
		]);
		if (!empty($data)) 
		{
			$result = $data[0]['Rcode'];
			if($result!=1)
			{
				$codeText=$data[0]['ErrCode'];
			}
		}

		if ($result!=1) 
		{
			$dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [],
                null,
                $codeText
            )];
		}
		$dataResult['result']=$result;
		return $dataResult;
	}


	private function getAccessForGroupDB(Core\User\UserGroup $group)
	{
			$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
					'Group'=> $group->getUid()
			]);
			//f6841cae-3b17-4c33-ad26-4ccacbaf3012
			$dataResult=[];
			$sett=[];
			if(!empty($data))
			{
					foreach ($data as $key => $value)
					{
							if(!empty($value['Settings']))
							{
								$t=json_decode($value['Settings'],true);
								if(!empty($t))
								{
									$sett[]=$t;
								}

							}
					}
			}
			return $sett;
	}

	public function getAccessGroup(Core\User\UserGroup $group)
	{
			$dataResult['error']=[];
			$dataResult['result']=1;
			$dataResult['data']=[];
			$dataDB=$this->getAccessForGroupDB($group);
			$dataSystem=$this->getAllModules();
			$dataSystem=array_keys($dataSystem);
			$data=[];
			\DZ::moduless($dataDB);
			foreach ($dataDB as $key => $value)
			{
					if(isset($value['modules']))
					{
							$modules=$value['modules'];

							if(is_array($modules))
							{
									foreach ($modules as  $valMod)
									{
											if(isset($valMod['module'])&&isset($valMod['allow']))
											{
												if(in_array($valMod['module'],$dataSystem))
												{
													$data[]=['module'=>$valMod['module'],'allow'=>$valMod['allow']];
													$delKey=array_search($valMod['module'],$dataSystem);
													unset($dataSystem[$delKey]);
												}
											}
									}
							}
					}
			}
			foreach ($dataSystem as $key => $value)
			{
					$data[]=['module'=>$value,'allow'=>0];
			}
			$group->setPermission(['modules'=>$data]);
				$dataResult['data']=$data;
			return $dataResult;
	}

  public function getUserSessPermission(Core\User\User $user)
  {
      //тут еще делаем запрос к БД
      $config = \Phalcon\Di::getDefault()->getShared('config');
      $permission=$config->permission;
			$permission=$config->permission;
			$modulesG1=$permission->modules->toArray();
      $modulesG=[];
      $controllers=[];
			$dataDB=$this->getAccessForUserDB($user);
			$dataSystem=$this->getAllModules();
			$dataSystem=array_keys($dataSystem);
			$data=[];

			foreach ($dataDB as $key => $value)
			{
					if(isset($value['modules']))
					{
							$modules=$value['modules'];

							if(is_array($modules))
							{
									foreach ($modules as  $valMod)
									{
											if(isset($valMod['module'])&&isset($valMod['allow']))
											{
												if(in_array($valMod['module'],$dataSystem))
												{
													if($valMod['allow']==1)
													{
															$data[]=$valMod['module'];
													}
												}
											}
									}
							}
					}
			}
      foreach ($modulesG1 as $key => $modCont)
			{
					if($key==='GLOBAL'||in_array($key,$data))
					{
							if($key!=='GLOBAL')
							{
									$modulesG[]=['name'=>$key,'path'=>$modCont['location']];
							}
							foreach ($modCont['controllers'] as  $keyContr=>$valCont)
							{
								if(isset($controllers[strtolower($keyContr)]))
								{
									if(is_null($controllers[strtolower($keyContr)]))
									{

									}else {
										if(is_array($valCont))
										{
												foreach ($valCont as  $value) {
													$controllers[strtolower($keyContr)][]=strtolower($value);
												}
										}else {
												if(is_null($valCont))
												{
													$controllers[strtolower($keyContr)]=$valCont;
												}else {
													$controllers[strtolower($keyContr)][]=$valCont;
												}

										}
									}
								}else {
									if(is_array($valCont))
									{
											$controllers[strtolower($keyContr)]=array_map('strtolower', $valCont);
									}else {
											$controllers[strtolower($keyContr)]=null;
									}
								}
							}
					}
      }
      return ['modules'=>$modulesG,'controllers'=>$controllers];

  }

	public function getAllModules()
	{
			$config = \Phalcon\Di::getDefault()->getShared('config');
			$permission=$config->permission;
			$modules=$permission->modules->toArray();
			unset($modules['GLOBAL']);
			return $modules;
	}
}
