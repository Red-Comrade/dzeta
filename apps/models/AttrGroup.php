<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\AttributeGroup;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\User\User;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class AttrGroup extends Model
{
	const PROCEDURES = [
		'add' => [
			'name' => 'Attr_group_AddNew',
			'params' => [
				'GroupName' => [ 'type' => DataType::TYPE_STRING ],
				'Class' => [ 'type' => DataType::TYPE_STRING ],
				'UserID' => [ 'type' => DataType::TYPE_STRING ],
			],
			'debug' => true,
		],
		'rename' => [
			'name' => 'Attr_group_Rename',
			'params' => [
				'GroupName' => [ 'type' => DataType::TYPE_STRING ],
	            'IDg' => [ 'type' => DataType::TYPE_STRING ],
	            'UserID' => [ 'type' => DataType::TYPE_STRING ],
			],
			'debug' => true,
		],
		'getAttributes' => [
			'name' => 'Attr_GetByClassGroup',//LinkedClass
			'params' => [
				'Class' => [ 'type' => DataType::TYPE_STRING ],
	            'Group' => [ 'type' => DataType::TYPE_STRING ],
			],
			'debug' => true,
		]
	];

	public function add(AttributeGroup $group, User $user) 
	{
		$dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];

        $groupName=$group->getName();
        $codeText='';
        $flagErr=false;
        if ($groupName=='') 
        {
            $flagErr=true;
            $result=-2;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
                'details'=>'Не введено имя группы',
                'codeID'=>'a5c834e4-774f-4e48-87c1-6fe3ea5858cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        if(!$flagErr) 
        {
        	$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
				'GroupName' => $groupName,
				'Class' => $group->getParent()->getUid(),
				'UserID' => $user->getUid()
			]);
			if (!empty($data)) 
            {
                $result = $data[0]['Rcode'];
                if ($result == 1) 
                {
                    $group->setUid($data[0]['IDc']);
                }else
                {
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
        if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$groupName],
                null,
                $codeText
            )];
        }

        $dataResult['result']=$result;
        return $dataResult;
	}

	public function rename(AttributeGroup $group, User $user) 
	{
		$dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];

        $groupName=$group->getName();
        $codeText='';
        $flagErr=false;
        if ($groupName=='') 
        {
            $flagErr=true;
            $result=-2;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-3,
                'details'=>'Не введено имя группы',
                'codeID'=>'a5c834e1-774f-4e48-87c1-6fe3ea5858cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        if(!$flagErr)
        {
        	$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
				'GroupName' => $group->getName(),
			 	'IDg' => $group->getUid(),
			 	'UserID' => $user->getUid()
			]);
			if (!empty($data)) 
            {
                $result = $data[0]['Rcode'];
                if ($result != 1) 
                {
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
		if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$groupName],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
		return $dataResult;
	}

    public function getAttributes(AttributeGroup $group)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Class' => $group->getParent()->getUid(),
            'Group' => $group->getUid(),
        ]);

        $attributes = [];
        foreach ($data as $value) {
            $attribute = new Attribute($value['IDc'], $value['Name'], $value['TypeOfStoredData'], (bool)$value['Uniq'], (bool)$value['Array'], false);
            $attribute->setIndex($value['NumInGroup']);
            //$attribute->setTextType($value["TextType"]);

            if (!empty($value['Calc'])) {
                $value['CalcName'] = empty($value['CalcName']) ? 'noname' : $value['CalcName'];
                $attribute->setCalc(new Core\Calculation($value['Calc'], $value['CalcName'], ''));
            }
            if (!empty($value['LinkedClass'])) {
                if ($value['LinkedClass'] != Core\Type\Type::TYPE_ROOT) {
                    $attribute->setLinkType(new Core\Type\Type($value['LinkedClass'], $value['LinkedClassName']));
                }
            }
            if (!empty($value['Settings'])) {
                $charlistics = json_decode($value['Settings'], true);
                $attribute->setCharlistics($charlistics);
            }
            $attributes[] = $attribute;
        }

	    $group->setAttributes($attributes);

	    return $attributes;
	}
}
