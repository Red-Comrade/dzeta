<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\User\UserGroup;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;


class User extends Model
{
	const PROCEDURES = [
		'checkPassword' => [
			'name' => 'Pass_Check',
			'params' => [
				'IP'       => [ 'type' => DataType::TYPE_STRING ],
				'Login'    => [ 'type' => DataType::TYPE_STRING ],
				'Password' => [ 'type' => DataType::TYPE_NUMBER ]
			],
			'debug' => true,
		],
		'changePassword'=> [
			'name' => 'Pass_AddNew',
			'params' => [
				'Obj'         => [ 'type' => DataType::TYPE_STRING ],
				'Password'    => [ 'type' => DataType::TYPE_STRING ],
				'User'        => [ 'type' => DataType::TYPE_NUMBER ]
			],
			'debug' => true,
		],
		'getUserByLogin'=> [
			'name' => 'User_GetUserByLogin',
			'params' => [
				'Login'         => [ 'type' => DataType::TYPE_STRING ]
			],
			'debug' => true,
		],
		//
	];
	/**
	 * Forget password from email
	 *
	 * @param Core\User\User $user
	 *
	 * @return int
	 */
	public function forgotPasswordEmail($user)
	{
		$dataResult=['error'=>[],'data'=>[],'result'=>-1];
		$res=$this->getUserByLogin($user);
		if($res['result']!=1)
		{
			$dataResult['error']=$res['error'];
			$dataResult['result']=$res['result'];
		}else
		{
			if (filter_var($user->getName(), FILTER_VALIDATE_EMAIL) !== false)
			{
				$password=Core\User\User::GeneratePassword();
				$textEmail="Your password for form.enemat.fr: $password";
				$mailTo=$user->getName();
				$subject="Change Password";
				$mail=false;
				$loader = new \Phalcon\Loader();
		        $loader->registerClasses(
		            [
		              'ElasticEmail' => APP_PATH . '/library/ElasticEmail.php'
		            ]
		        );
		        $loader->register();
				$mail=\ElasticEmail::sendMail($subject,$mailTo,$textEmail);
				if($mail)
				{
					$user->setPassword($password);
					$res=$this->changePassword($user, $user);
					if($res['result']!=\Dzeta\Library\Db::SUCCESS_CODE)
					{
						$dataResult['error']=$res['error'];
						$dataResult['result']=$res['result'];
					}else
					{
						$dataResult['result']=$res['result'];
					}
				}else 
				{
					$dataResult['error'][]=['text'=>DbError::getError('User.forgotPasswordEmail', 3)];
					$dataResult['result']=3;
				}
			}
			else
			{
				$dataResult['error'][]=['text'=>DbError::getError('User.forgotPasswordEmail', 2)];
				$dataResult['result']=2;
			}
		}
		return $dataResult;
	}

	/**
	 * Get User by Login
	 *
	 * @param Core\User\User $user
	 *
	 * @return int
	 */
	public function getUserByLogin(Core\User\User $user)
	{
			$dataResult['error']=[];
			$dataResult['result']=-1;
			$dataResult['data']=[];
			\DZ::usertg($user);
			$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
				'Login'       => $user->getName(),
			]);

			$result = -1;
			if (!empty($data)) {
				$userUid = $data[0]['User'];
				$result=2;
				if (!is_null($userUid)) {
					$user->setUid($userUid);
					$result=1;
				}
			}
			if($result!=1)
			{
					$dataResult['error']=[];
					$dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
			}
			$dataResult['result']=$result;
			return $dataResult;
	}

	/**
	 * Check user login and password for signing in
	 *
	 * @param Core\User\User $user
	 *
	 * @return int
	 */
	public function checkPassword(Core\User\User $user) 
	{
		$dataResult['error']=[];
		$dataResult['result']=-1;
		$dataResult['data']=[];

		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'IP'       => $user->getIP(),
			'Login'    => $user->getName(),
			'Password' => $user->getPassword(),
		]);

		$result = -1;
		if (!empty($data)) 
		{
			$result = $data[0]['Rcode'];
			if ($result == 1) 
			{
				$user->setUid($data[0]['User']);
				$user->setPassword('');
			}
		}
		if($result != 1)
		{
			$dataResult['error']=[];
			$dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
		}
		$dataResult['result']=$result;
		return $dataResult;
	}

	/**
	 * Change user password
	 *
	 * @param Core\User\User $updatedUser
	 * @param Core\User\User $currentUser
	 *
	 * @return ['error'=>[],'result'=>[],'data'=>[]]
	 */
	public function changePassword(Core\User\User $updatedUser, Core\User\User $currentUser) {

		$dataResult=['error'=>[],'result'=>-1,'data'=>[]];		
		$codeText='';
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'Obj'       => $updatedUser->getUid(),
			'Password'  => $updatedUser->getPassword(),
			'User'      => $currentUser->getUid()
		]);

		$result = -1;
		if (!empty($data)) 
		{
			$dataResult['result'] = $data[0]['Rcode'];
			if($dataResult['result']!=1)
			{
				$codeText=$data[0]['ErrCode'];
			}
		}
		if ($dataResult['result']!=1)
		{
			$dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $dataResult['result'],
                [],
                null,
                $codeText
            )];
		}		
		return $dataResult;
	}

}
