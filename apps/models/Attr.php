<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core\Type\Type;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;
use Dzeta\Core\User\User;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Attr extends Model
{
    const PROCEDURES = [
        'add' => [
            'name' => 'Attr_AddNew',
            'params' => [
                'Settings' => [ 'type' => DataType::TYPE_JSON ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],

            ],
            'debug' => true,
        ],
        'edit' => [
            'name' => 'Attr_EditSettings',
            'params' => [
                'ID' => [ 'type' => DataType::TYPE_STRING ],
                'Settings' => [ 'type' => DataType::TYPE_JSON ],
                'UserID' => [ 'type' => DataType::TYPE_STRING]
            ],
            'debug' => true,
        ],
        'remove'=> [
            'name' => 'Attr_Delete',
            'params' => [
                'Attr' => [ 'type' => DataType::TYPE_STRING ],
                'Class' => [ 'type' => DataType::TYPE_STRING ],
                'UserID' => [ 'type' => DataType::TYPE_STRING]
            ],
            'debug' => true,
        ],
        'sort'=> [
            'name' => 'Class_attr_AddOrderForAttrs',
            'params' => [
                'Class' => [ 'type' => DataType::TYPE_STRING ],
                'Order' => [ 'type' => DataType::TYPE_JSON ],
                'UserID' => [ 'type' => DataType::TYPE_STRING]
            ],
            'debug' => true,
        ],
        'getAllAtributes'=>[
            'name' => 'Attr_GetAllAttrsForAllClasses',
            'params' => [
            ],
            'debug' => true,
        ],
        'GetIDByName'=>[
            'name' => 'Attr_GetIDByName',
            'params' => [
                'json' => [ 'type' => DataType::TYPE_JSON ],
            ],
            'debug' => true,
        ],
		'getAttributesByList' => [
			'name' => 'Attr_GetInfoForAttrs',
			'params' => [
				'Attrs' => [ 'type' => DataType::TYPE_STRING ],
			],
			'debug' => true,
		]
    ];

    /**
     * Add param
     *
     * @param Attribute        $attr
     * @param Core\User\User   $user
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function add(Attribute $attr, User $user)
    {
        $calcUID=null;
        $linkType=null;
        if ($attr->getCalc()!=null) 
        {
            $calcUID=$attr->getCalc()->getUid();
        }
        if ($attr->getLinkType()!=null) 
        {
            $linkType=$attr->getLinkType()->getUid();
        }
        $attrName=$attr->getName();
        $charlistics=$attr->getCharlictics();
        $flagErr=false;
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        $result = -1;
        $codeText='';
        if ($attrName=='') 
        {
            $flagErr=true;
            $result=-2;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-2,
                'details'=>'Не введено имя атрибута',
                'codeID'=>'a6c834e4-774f-4e48-87c1-6fe3ea5857cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }
        if (!$flagErr) 
        {
            $data1=[
                    'AttrName'=>$attrName,
                    'TypeOfStoredData'=> DataType::getIndexFromType($attr->getDatatype()),
                    'Class'=>$attr->getParent()->getUid(),
                    'Array'=>(int)$attr->isArray(),
                    'Uniq'=> (int)$attr->isKey(),
                    'GroupID' =>$attr->getGroup()->getUid(),
                    'Calc'=>$calcUID,
                    'LinkedClass'=>$linkType,
                    'Settings'=>json_encode($charlistics)
            ];
            $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'Settings' => [$data1],
                'UserID' => $user->getUid()
            ]);

            if (!empty($data))
            {
                $result = $data[0]['Rcode'];
                if ($result == 1) 
                {
                    $attr->setUid($data[0]['IDattr']);
                }else
                {
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
        if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$attrName],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }
    /**
     * Edit param
     *
     * @param Attribute        $attr
     * @param Core\User\User   $user
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function edit(Attribute $attr, User $user)
    {
        $calcUID=null;
        $linkType=null;
        if ($attr->getCalc()!=null) {
            $calcUID=$attr->getCalc()->getUid();
        }
        if ($attr->getLinkType()!=null) {
            $linkType=$attr->getLinkType()->getUid();
        }
        $charlistics=$attr->getCharlictics();
        $attrName=$attr->getName();
        $flagErr=false;
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        $codeText='';
        $result = -1;
        if ($attrName=='') 
        {
            $flagErr=true;
            $result=-2;
            $codeText=\Dzeta\Models\Log::addLogError(['Rcode'=>-2,
                'details'=>'Не введено имя атрибута',
                'codeID'=>'a6c834e4-774f-4e48-87c1-63e3ea5858cd',
                'query'=>self::PROCEDURES[__FUNCTION__]['name']],$user);
        }

        if (!$flagErr) 
        {
            $data1=[
                'AttrName'=>$attr->getName(),
                'Class'=>$attr->getParent()->getUid(),
                'Array'=>(int)$attr->isArray(),
                'Uniq'=> (int)$attr->isKey(),
                'Calc'=>$calcUID,
                'LinkedClass'=>$linkType,
                'Settings'=>json_encode($charlistics)
            ];
            $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'ID' => $attr->getUid(),
                'Settings' => [$data1],
                'UserID' => $user->getUid()
            ]);
            $result = -1;
            $dataResult=[];
            if (!empty($data)) 
            {
                $result = $data[0]['Rcode'];
                if ($result != 1) 
                {                   
                    $codeText=$data[0]['ErrCode'];
                }
            }
        }
        if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [0=>$attrName],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }
    /**
     * remove param
     *
     * @param Attribute        $attr
     * @param Core\User\User   $user
     *
     * @return ['error'=>[],'result'=>[],'data'=>[]]
     */
    public function remove(Attribute $attr, User $user)
    {
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        $codeText='';
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'Attr' => $attr->getUid(),
                'Class' => $attr->getParent()->getUid(),
                'UserID' => $user->getUid()
            ]);
        $result=-1;
        if (!empty($data)) 
        {
            $result = $data[0]['Rcode'];
            if($result!=1)
            {
                $codeText=$data[0]['ErrCode'];
            }
        }
        if ($result!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }


    public function sort($classUID, $Order, User $user)
    {
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        $codeText='';
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'Class' => $classUID,
                'Order' => $Order,
                'UserID' => $user->getUid()
            ]);
        $result=-1;
        if (!empty($data))
        {
            $result = $data[0]['Rcode'];
            if($result!=1)
            {
                $codeText=$data[0]['ErrCode'];
            }
        }
        if ($result!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'],
                $result,
                [],
                null,
                $codeText
            )];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }
    /**
     *  Get all atributes system
     *
     * @return Attr[]
     */
    public function getAllAtributes()
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], []);
        if (isset($data[0]['Data'])) {
            $data=json_decode($data[0]['Data'], true);
        } else {
            $data=[];
        }
        $attrs=[];
        foreach ($data as $key => $value) {
            $type=new \Dzeta\Core\Type\Type($value['cl']['ID'], $value['cl']['Name']);
            $attr=new \Dzeta\Core\Type\Attribute($value['ID'], $value['Name'], $value['TypeOfStoredData'], false, false);
            $attr->setParent($type);
            $attrs[]=$attr;
        }
        return $attrs;
    }

		/**
		 *  Get uid attributes for name
		 *
		 * @return ['error'=>[],'result'=>[],'data'=>[]]
		 */
    public function GetIDByName(array $attrs)
    {
        $classTmp=[];
        foreach ($attrs as $key => $value) {
            $classTmp[$value->getParent()->getUid()][]=$value->getName();
        }
        $jsonArr=[];
        foreach ($classTmp as $key => $value) {
            $jsonArr[]=[
                        'Class'=>$key,
                        'Names'=>$value
                    ];
        }
        $result = -1;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'json' =>$jsonArr
            ]);
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];
        if (!empty($data)) {
            $arrAtrrKey=array_column($data, 'AttrName');
            $result = 1;
            foreach ($attrs as  $value) {
                $reyAtrr=array_search($value->getName(), $arrAtrrKey);
                $attrName=$value->getName();
                if ($reyAtrr!==false) {
                    $value->setUid($data[$reyAtrr]['IDa']);
                    $value->setDatatype($data[$reyAtrr]['DataType']);
                    $dataResult['data'][$value->getParent()->getUid()][$attrName]=['uid'=>$value->getUid(),'result'=>1];
                } else {
                    $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], 2, [0=>$attrName]),'attr'=>$attrName];
                    $dataResult['data'][$value->getParent()->getUid()][$attrName]=['uid'=>$value->getUid(),'result'=>2];
                }
            }
            if (empty($dataResult['error'])) {
                $dataResult['result']=1;
            }
        }
        return $dataResult;
    }

	public function getAttributesByList(array $attributes) {
		$uids = [];
		foreach ($attributes as $attr) {
			$uids[] = $attr->getUid();
		}
		$data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
			'Attrs' => empty($uids) ? null : '{' . implode(',', $uids) . '}'
		]);
		if (isset($data[0]['Data'])) {
            $data = json_decode($data[0]['Data'], true);
        } else {
            $data = [];
        }
	    foreach ($data as $key => $value) {
	        $type = new \Dzeta\Core\Type\Type($value['cl']['ID'], $value['cl']['Name']);
	        foreach ($attributes as $attr) {
	        	if ($attr->getUid() == $value['ID']) {
	        		$attr->setName($value['Name'])->setDatatype($value['TypeOfStoredData'])->setIsKey($value['Uniq'] ? true : false)->setIsArray($value['Array'] ? true : false);
			        $attr->setParent($type);
                    if (!empty($value['LinkedClass'])) {
                        if ($value['LinkedClass'] != \Dzeta\Core\Type\Type::TYPE_ROOT) {
                            $attr->setLinkType(new \Dzeta\Core\Type\Type($value['LinkedClass'], ''));
                        }
                    }
                    $attr->setIsArray((bool)$value['Array']);
	        	}
	        }
	    }
	}
}
