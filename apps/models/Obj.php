<?php

namespace Dzeta\Models;

use Phalcon\Mvc\Model;

use Dzeta\Core;
use Dzeta\Core\Instance;
use Dzeta\Core\User\User;
use Dzeta\Core\User\UserGroup;
use Dzeta\Core\Type\DataType;
use Dzeta\Core\Type\Attribute;
use Dzeta\Library\Db;
use Dzeta\Library\DbError;

class Obj extends Model
{
    const PROCEDURES = [
        'add' => [
            'name' => 'object_AddNew',
            'params' => [
                'Class' => [ 'type' => DataType::TYPE_STRING ],
                'PID' => [ 'type' => DataType::TYPE_STRING ],
                'AddedBy' => [ 'type' => DataType::TYPE_NUMBER ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
                'Object' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'checkUnique' => [
            'name' => 'Object_CheckUniq',
            'params' => [
                'Class' => [ 'type' => DataType::TYPE_STRING ],
                'Attrs' => [ 'type' => DataType::TYPE_STRING ],
                'Vals' => [ 'type' => DataType::TYPE_STRING ],
                'BaseObject' => [ 'type' => DataType::TYPE_STRING ],
                'IDExistObj' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'remove' => [
            'name' => 'Object_Delete',
            'params' => [
                'IDs' => [ 'type' => DataType::TYPE_STRING ],
                'UserID' => [ 'type' => DataType::TYPE_STRING ],
                'ReturnNumBeforDel' => [ 'type' => DataType::TYPE_NUMBER ],
                'DeleteAllObjsOfClass'=> [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'getAll' => [
            'name' => 'Object_GetTree',
            'params' => [],
            'debug' => true,
        ],
         'getAlltest' => [
            'name' => 'Object_GetTree',
            'params' => [],
            'debug' => true,
        ],
        'getChildrenByType' => [
            'name' => 'Object_GetByBaseObject',
            'params' => [
                'BaseObject' => [ 'type' => DataType::TYPE_STRING ],
                'Class' => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'getAllValues' => [
            'name' => 'V_GetValues',
            'params' => [
                'ObjsNeedAllAttrs' 				=> [ 'type' => DataType::TYPE_STRING ],
                'ObjsNeedSpecifiedAttrs'	=> [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'getValuesAndLastUpdateTime' => [
            'name' => 'V_GetValuesAndLastUpdateTime',
            'params' => [
                'Obj' 				=> [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'GetValuesForChildObjs' => [
            'name' => 'V_GetValuesForChildObjs',
            'params' => [
                'ParentObs' => [ 'type' => DataType::TYPE_ARRAY ],
                'ChildClassesAndAtrrs'	=> [ 'type' => DataType::TYPE_JSON ],
            ],
            'debug' => true,
        ],
        'passCheck' => [
            'name' => 'Pass_Check',
            'params' => [
                'IP'       => [ 'type' => DataType::TYPE_STRING ],
                'Login'    => [ 'type' => DataType::TYPE_STRING ],
                'Password' => [ 'type' => DataType::TYPE_NUMBER ]
            ],
            'debug' => true,
        ],
        'PassNew'=> [
            'name' => 'Pass_AddNew',
            'params' => [
                'Obj'         => [ 'type' => DataType::TYPE_STRING ],
                'Password'    => [ 'type' => DataType::TYPE_STRING ],
                'User'        => [ 'type' => DataType::TYPE_NUMBER ]
            ],
            'debug' => true,
        ],
        'AddFiles'=> [
            'name' => 'V_AddFiles',
            'params' => [
                'Objs'         => [ 'type' => DataType::TYPE_STRING ],
                'Attrs'   		 => [ 'type' => DataType::TYPE_STRING ],
                'RealNames'    => [ 'type' => DataType::TYPE_STRING ],
                'GenNames'     => [ 'type' => DataType::TYPE_STRING ],
                'Size'    		 => [ 'type' => DataType::TYPE_STRING ],
                'Previews'     => [ 'type' => DataType::TYPE_STRING ],
                'User'         => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'ChangeBaseObj'=> [
            'name' => 'Object_ChangeBaseObj',
            'params' => [
                'Objs'         => [ 'type' => DataType::TYPE_STRING ],
                'PIDold'   		 => [ 'type' => DataType::TYPE_STRING ],
                'PIDnew'    => [ 'type' => DataType::TYPE_STRING ],
                'UserID'         => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'getChildrenAggr' => [
            'name' => 'V_GetAgrValsForChildObjs',
            'params' => [
                'BaseObj'         => [ 'type' => DataType::TYPE_STRING ],
                'ChildClass'   		 => [ 'type' => DataType::TYPE_STRING ],
                'Attr'    => [ 'type' => DataType::TYPE_STRING ],
                'Operation'         => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'getObjsByClass'=> [
            'name' => 'Object_GetObjsByClass',
            'params' => [
                'Class'    => [ 'type' => DataType::TYPE_STRING ],
                'Filter'   => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'addValues'=> [
            'name' => 'V_Add_json',
            'params' => [
                'Values'    => [ 'type' => DataType::TYPE_JSON ]
            ],
            'debug' => true,
        ],
        'getObjectsByAttributesAndObjectNames'=> [
            'name' => 'Import_GetObjIDsByAttrsObjNames',
            'params' => [
                'Attrs'    => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'addEventQueueAfterCreateObj'=>[
            'name'=>'Object_created_Add_to_event_queue',
            'params'=>[
                'ID'=>['type' => DataType::TYPE_STRING],
                'User'=>['type' => DataType::TYPE_STRING],
            ],
            'debug'=>true
        ],
        'GetClassForObject'=>[
            'name'=>'Object_GetClassForObject',
            'params'=>[
                'Object'=>['type' => DataType::TYPE_STRING]                
            ],
            'debug'=>true
        ],
        'getParent'=> [
            'name' => 'Object_BaseObjectsForObject',
            'params' => [
                'Object'      => [ 'type' => DataType::TYPE_STRING ],
            ],
            'debug' => true,
        ],
        'searchObj'=> [
            'name' => 'Calc_GetObjsByFilter',
            'params' => [
                'Class'      => [ 'type' => DataType::TYPE_STRING ],
                'Filters'   => [ 'type' => DataType::TYPE_STRING ],
                'BaseObjs'  => [ 'type' => DataType::TYPE_ARRAY ],
                'SearchBy'  => [ 'type' => DataType::TYPE_NUMBER ],
                'GetAttrVals' => [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'Object_GetNames' => [
            'name' => 'Object_GetNames',
            'params' => [
                'Objs'  => [ 'type' => DataType::TYPE_ARRAY ],
                'GetNumOfChildren' => [ 'type' => DataType::TYPE_NUMBER]
            ],
            'debug' => true,
        ],
        'ObjectGetNames' => [
            'name' => 'Object_GetNames',
            'params' => [
                'Objs'  => [ 'type' => DataType::TYPE_ARRAY ],
                'GetNumOfChildren' => [ 'type' => DataType::TYPE_NUMBER]
            ],
            'debug' => true,
        ],
        'copy' => [
            'name' => 'Objects_Сopy',
            'params' => [
                'Parent' =>   [ 'type' => DataType::TYPE_STRING ],
                'Data' =>   [ 'type' => DataType::TYPE_JSON],
                'Block'=> [ 'type' => DataType::TYPE_NUMBER],
                'CopyObjsСhidren'=> [ 'type' => DataType::TYPE_NUMBER],
                'User'=>   [ 'type' => DataType::TYPE_STRING ]
            ],
            'debug' => true,
        ],
        'Object_addNewLink'=>[
            'name' => 'Object_AddNewLink',
            'params' => [
                'Obj' =>   [ 'type' => DataType::TYPE_STRING],
                'PID' =>   [ 'type' => DataType::TYPE_STRING],
                'User'=>   [ 'type' => DataType::TYPE_STRING]
            ],
            'debug' => true,
        ],
        'Object_GetOwner'=>[
            'name' => 'Object_GetOwner',
            'params' => [
                'Obj' =>   [ 'type' => DataType::TYPE_STRING],
            ],
            'debug' => true,
        ]
        //Object_created_Add_to_event_queue
    ];
    public function Object_addNewLink(Instance $obj,Instance $ParentLobj,User $user)
    {
        $dataResult=['error'=>[],'result'=>-1,'data'=>[]];        
        $codeText='';  
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'Obj' => $obj->getUid(),
          'PID' => $ParentLobj->getUid(),
          'User' =>$user->getUid()
        ]);
        if(!empty($data))
        {
            $dataResult['result']=$data[0]['Rcode'];
            if($dataResult['result']!=1)
            {
                $codeText=$data[0]['ErrCode'];
            }
        }   
        if ($dataResult['result']!=1) 
        {            
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $dataResult['result'],
                [],
                null,
                $codeText
            )];
        }
        return $dataResult;
    }
    public function copy(Instance $parentOBJ,$dataOBJs,$blockValue,$copyChildren,User $user)
    {
        $dataResult['error']=[];
        $dataResult['result']=0;
        $dataResult['data']=[];
        $dataResult['CreatedObjs']=[];

        $dataForDB=[];
        foreach ($dataOBJs as $key => $value) 
        {
            $numOBJ=$key+1;
            $objUID= $value->getUid();
            $attrsDB=[];
            $vals=$value->getAttributes();

            foreach ($vals as $attr) 
            {
                $tmp=[];
                if(empty($attr->getValue()))
                {
                    continue;
                }
                switch ($attr->getDatatype()) 
                {
                    case DataType::TYPE_NUMBER:
                        $tmp['v']=$attr->getValue()->getValue();
                        $tmp['vm']=$attr->getValue()->getValue();
                        break;
                    case DataType::TYPE_OBJECT:
                        $tmp['v']=$attr->getValue()->getUid();
                        break;
                    case DataType::TYPE_DATETIME:
                        $tmp['v']=$attr->getValue()->getValueDB();
                        break;
                    default:
                        $tmp['v']=$attr->getValue()->getValue();
                        break;
                }
                $tmp['attr']=$attr->getUid();
                $attrsDB[]=$tmp;
            }
            $dataForDB[]=[
                'n'=>$numOBJ,
                'Obj'=>$objUID,
                'Vals'=>$attrsDB
            ];
        }
        
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Parent' =>$parentOBJ->getUid(),
            'Data' => $dataForDB,
            'Block' =>$blockValue,
            'CopyObjsСhidren' => $copyChildren,
            'User' => $user->getUid()
        ]);
        foreach ($data as $value) 
        {
            if(!empty($value['createdobjs'])) 
            {
                $dataResult["CreatedObjs"] = $value['createdobjs'];
            }
            if(empty($value['q'])) 
            {
                $dataResult['result']=1;
            }
            else 
            {
                $dataResult['result']=0;
                $json=json_decode($value['q'],true);
                $codeText=$value['ErrCode'];
                foreach ($json as $valueErr) 
                {
                    $dataResult['error'][]=['text'=>$valueErr['name'].': '.DbError::getError(self::PROCEDURES[__FUNCTION__]['name'],  $valueErr['err'],[],null,$codeText)];
                }
            }
        }
        return $dataResult;
    }
    //Object_BaseObjectsForObject
    public function searchObj(Core\Type\Type $class,string $filters,array $objs,int $searchBy){
        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];
        $objUids=[];
        foreach ($objs as $key => $value) {
            $objUids[]=$value->getUid();
        }
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'Class' => $class->getUid(),
          'Filters' => $filters,
          'BaseObjs' => empty($objUids) ? null : $objUids,
          'SearchBy' => $searchBy,
          'GetAttrVals'=>null
        ]);
        if (!empty($data))
        {
            $dataResult['result']=1;
        }
       
        foreach ($data as $key => $value) {
            $objs=new Instance($value['IDs'],is_null($value['Names'])? 'noName':$value['Names'],$class);
          
            $dataResult['data'][]=$objs;
        }
        if($dataResult['result']!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }       
        return $dataResult;
    }
     public function getParent(Instance $obj){
        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];

        
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'Object'   => $obj->getUid()
        ]);
        if (!empty($data))
        {
            $dataResult['result']=1;
        }
        foreach ($data as $key => $value) {
            $parentObj=new Instance($value['IDo'],$value['Name'],new Core\Type\Type($value['Class'],$value['ClassName'] ) );
           $obj->setParent($parentObj);
            $dataResult['data'][]=$parentObj;
        }
        if($dataResult['result']!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }       
        return $dataResult;
    }
    public function GetClassForObject(Instance $obj){
        $dataResult['error']=[];
        $dataResult['result']=1;
        $dataResult['data']=[];

        
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'Object'   => $obj->getUid()
        ]);
        if (!empty($data))
        {
            $dataResult['result']=1;
        }
        foreach ($data as $key => $value) {
            $type=new Core\Type\Type($value['Class'],$value['CLassName'] );
            $obj->setType($type);
            $dataResult['data'][]=$type;
        }
        if($dataResult['result']!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)];
        }       
        return $dataResult;
    }

    public static function convertObjects(array $objs, User $user) {
        $arrayDB = [];
        foreach ($objs as $obj) {
            $tmpDB = self::convertObject($obj, $user);
            $arrayDB[] = $tmpDB;
            unset($tmpDB);
        }
        return $arrayDB;
    }

    public static function convertObject(Instance $obj, User $user) {
        $tmpDB = [];
        $tmpDB['UserID'] = $user->getUid();
        $tmpDB['Obj'] = $obj->getUid();
        $values = [];
        foreach ($obj->getAttributes() as $attr) {
            $v = [];
            $v['attr'] = $attr->getUid();
            $vTmp = null;
            $value = $attr->getValue();
            if (is_null($value)) {
                $vTmp = null;
            } else {
                switch ($attr->getDatatype()) {
                    case DataType::TYPE_NUMBER:
                        if ($attr->isArray()) {
                          foreach ($value as  $valuArr) {
                            $v['vm'][] = $valuArr->getValue()."";
                            $vTmp[] = $valuArr->getValue()."";
                          }
                        } else {
                            $v['vm'][] = $value->getValue()."";
                            $vTmp[] = $value->getValue()."";
                        }
                        break;
                    case DataType::TYPE_DATETIME:
                        if ($attr->isArray()) {
                          foreach ($value as  $valuArr) {
                            $vTmp[] = $valuArr->getValueDB();
                          }
                        } else {
                            $vTmp[] = $value->getValueDB();
                        }
                        break;
                    case DataType::TYPE_OBJECT:
                        if ($attr->isArray()) {
                          foreach ($value as $valuArr) {
                            $vTmp[] = $valuArr->getUid();
                          }
                        } else {
                            $vTmp[] = $value->getUid();
                        }
                        break;
                    default:
                        if ($attr->isArray()) {
                          foreach ($value as $valuArr) {
                            $vTmp[] = $valuArr->getValue();
                          }
                        } else {
                            $vTmp[] = $value->getValue();
                        }
                        break;
                }
            }
            $v['v'] = $vTmp;
            $v['add'] = $attr->getDopSett('addValues') ? 1 : 0;
            $values[] = $v;
        }
        $tmpDB['Vals'] = $values;
        return $tmpDB;
    }

    public function addEventQueueAfterCreateObj(Instance $obj, User $user){
        $dataResult['error']=[];
        $dataResult['result']=-1;
        $dataResult['data']=[];

        
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'ID'   => $obj->getUid(),
          'User'=> $user->getUid(),
        ]);
        if (!empty($data))
        {
            $dataResult['result']=$data[0]['Rcode'];
        }
        if($dataResult['result']!=1)
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $dataResult['result'])];
        }       
        return $dataResult;
    }

    /**
     * Add values to objects
     *
     * @param Instance[] $obj
     * @param User $user
     *
     * @return array
     */
    public function addValues(array $objs, User $user)
    {
        $attrs = [];
        $values = [];
        $results = [];
        $errors = [];

        $arrayDB = self::convertObjects($objs, $user);

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Values' => $arrayDB,
        ]);

        if (!empty($data)) {
            foreach ($data as $value) {
                $results[$value['Attr']]['result'] = $value['Rcode'];
                $objs[0]->setName($value['Name']);
                if ($value['Rcode'] != Db::SUCCESS_CODE) {
                    $errors[] = [
                        'text' => DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $value['Rcode'])
                    ];
                }
            }
        }
        return ['result' => $results, 'errors' => $errors];
    }


    public function getObjsByClass(Core\Type\Type $type, string $filter)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
          'Class' =>$type->getUid(),
          'Filter' => $filter
      ]);
        $dt=null;
        foreach ($data as $item) {
            if (!empty($item["objs"])) {
                $dt=json_decode($item["objs"], true);
            }
        }

        if (empty($dt)) {
            return ['result'=>-1];
        }
        if (isset($dt[0]['cnt'])) {
            return [
            'result'=>1,
            'count'=>$dt[0]['cnt'],
            'type'=> 'search'
          ];
        }
        $objs=[];
        if (isset($dt[0]['ID'])) {
            foreach ($dt as $key => $value) {
                $value['Name']=is_null($value['Name'])?'':$value['Name'];
                $objs[]= new Instance($value['ID'], $value['Name'], $type);
            }
        }
        return ['result'=>1,'objs'=>$objs];
    }

    public function getChildrenAggr(Instance $obj, Core\Type\Type $class, Attribute $attr, string $aggr)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'BaseObj' => $obj->getUid(),
            'ChildClass' => $class->getUid(),
            'Attr' => $attr->getUid(),
            'Operation' => $aggr,
        ]);

        $result = null;
        if (!empty($data)) {
            $result = $data[0]['Val'];
        }
        return $result;
    }

    public function ChangeBaseObj(Instance $obj, Instance $objN, User $user)
    {
        $dataResult=['error'=>[],'result'=>-1,'data'=>[]];
        $codeText='';
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Objs' => $obj->getUid(),
            'PIDold' => $obj->getParent()->getUid(),
            'PIDnew' => $objN->getUid(),
            'UserID' => $user->getUid(),
        ]);        
        if (!empty($data)) 
        {
            $dataResult['result'] = $data[0]['Rcode'];
            if ($result == 1) 
            {
                $obj->setParent($objN);
            }else
            {
                $codeText=$data[0]['ErrCode'];
            }
        }
        if ($dataResult['result']!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $result,
                [],
                null,
                $codeText
            )];
        }
        return $dataResult;
    }

    private function _initObjs(array $data)
    {
        $objs = [];
        foreach ($data as $value) {
            if (isset($objs[$value['PID']])) {
                $parent = $objs[$value['PID']];
            } else {
                if ($value['PID'] == Instance::INSTANCE_ROOT) {
                    //TODO: Replace root name with i18n translation
                    $parent = new Instance($value['PID'], 'Root');
                } else {
                    $parent = new Instance($value['PID'], '');
                }
                $objs[$value['PID']] = $parent;
            }
            if (isset($objs[$value['ID']])) {
                if ($value['ID'] != Instance::INSTANCE_ROOT) {
                    $objs[$value['ID']]->setName($value['Name']);
                    if (is_null($objs[$value['ID']]->getParent())) {
                        $objs[$value['ID']]->setParent($parent);
                    }
                }
            } else {
                if ($value['ID'] == Instance::INSTANCE_ROOT) {
                    //TODO: Replace root name with i18n translation
                    $objs[$value['ID']] = new Instance($value['ID'], 'Root');
                } else {
                    $objs[$value['ID']] = new Instance($value['ID'], $value['Name'], null, $parent);
                }
            }
        }
        return $objs;
    }


    private function _initObjsAttrs(array $data)
    {
        $attrs = [];
        $_attrs = [];
        foreach ($data as $value) {
            $_attrs[$value['Obj']] = isset($_attrs[$value['Obj']]) ? $_attrs[$value['Obj']] : [];

            $tnpmV = [
                'value' => $value['V'],
                'LinkedObjName' => $value['LinkedObjName'],
                'GenFileName' => $value['GenFileName'],
                'PreViewFile' => $value['PreViewFile'],
                'Size' => $value['Size'],
                'RealFileName' => $value['RealFileName'],
                'LinkedClass' => $value['LinkedClass'],
            ];
            if ($value['Array']) {
                $_attrs[$value['Obj']][$value['Attr']] = isset($_attrs[$value['Obj']][$value['Attr']]) ? $_attrs[$value['Obj']][$value['Attr']] : ['attr' => $value, 'value' => []];
                if ($value['TypeOfData'] != 5) {
                    if (!is_null($value['V'])) {
                        $_attrs[$value['Obj']][$value['Attr']]['value'][] = $tnpmV;
                    }
                } else {
                    if (!is_null($value['GenFileName'])) {
                        $_attrs[$value['Obj']][$value['Attr']]['value'][] = $tnpmV;
                    }
                }
            } else {
                $_attrs[$value['Obj']][$value['Attr']] = ['attr' => $value, 'value' => $tnpmV];
            }
        }
        foreach ($_attrs as $obj => $attr) {
            $attrs[$obj] = isset($attrs[$obj]) ? $attrs[$obj] : [];
            foreach ($attr as $uid => $value) {
                $value['attr']['Uniq'] = is_null($value['attr']['Uniq']) ? 0 : $value['attr']['Uniq'];
                $value['attr']['Array'] = is_null($value['attr']['Array']) ? 0 : $value['attr']['Array'];
                $_attr = new Attribute(
                    $value['attr']['Attr'],
                    $value['attr']['Name'],
                    $value['attr']['TypeOfData'],
                    $value['attr']['Uniq'],
                    $value['attr']['Array']
                );
                if (!empty($value['attr']['Settings'])) {
					$charlistics = json_decode($value['attr']['Settings'], true);
                    if (!empty($charlistics)) {
                        $_attr->setCharlistics($charlistics);
                    }
        		}
                switch (DataType::getTypeFromIndex($value['attr']['TypeOfData'])) {
                    case DataType::TYPE_NUMBER:
                        if ($value['attr']['Array']) {
                            $tmpV = [];
                            foreach ($value['value'] as $key => $valAttr) {
                                $tmpV[] = ['value' => $valAttr['value'], 'convValue' => null];
                            }
                            $_attr->setValue(['value' => $tmpV]);
                        } else {
                            $_attr->setValue(['value' => $value['value']['value'], 'convValue' => null]);
                        }
                        //TODO converted value
                        break;
                    case DataType::TYPE_TEXT:
                    case DataType::TYPE_DATETIME:
                        if ($value['attr']['Array']) {
                            $tmpV = [];
                            foreach ($value['value'] as $key => $valAttr) {
                                $tmpV[] = ['value' => $valAttr['value']];
                            }
                            $_attr->setValue(['value' => $tmpV]);
                        } else {
                            $_attr->setValue(['value' => $value['value']['value']]);
                        }
                        break;
                    case DataType::TYPE_OBJECT:
                        if ($value['attr']['Array']) {
                            $tmpV = [];
                            foreach ($value['value'] as $key => $valAttr) {
                                $tmpV[] = [
                                    'value' => $valAttr['value'],
                                    'name' => $valAttr['LinkedObjName'],
                                    'type' => [
                                        'value' => $valAttr['LinkedClass'],
                                        'name' => ''
                                    ]
                                ];
                            }
                            $_attr->setValue(['value'=>$tmpV]);
                        } else {
                            $_attr->setValue([
                                'value' => $value['value']['value'],
                                'name' => $value['value']['LinkedObjName'],
                                'type' => [
                                    'value' => $value['value']['LinkedClass'],
                                    'name' => ''
                                ]
                            ]);
                        }
                        break;
                    case DataType::TYPE_COUNTER:
                        if ($value['attr']['Array']){
                            $tmpV = [];
                            foreach ($value['value'] as $key => $valAttr) {
                                $tmpV[] = ['value' => $valAttr['value']];
                            }
                            $_attr->setValue(['value' => $tmpV]);
                        } else {
                            $_attr->setValue(['value' => $value['value']['value']]);
                        }
                        break;
                    case DataType::TYPE_FILE:
                        if ($value['attr']['Array']){
                            $tmpV=[];
                            foreach ($value['value'] as $key => $valAttr) {
                                $tmpV[] = [
                                    'value' => $valAttr['GenFileName'],
                                    'name' => $valAttr['RealFileName'],
                                    'preview' => $valAttr['PreViewFile'],
                                    'size' => $valAttr['Size']
                                ];
                            }
                            $_attr->setValue(['value'=>$tmpV]);
                        } else {
                            $_attr->setValue([
                                'value' => $value['value']['GenFileName'],
                                'name' => $value['value']['RealFileName'],
                                'preview' => $value['value']['PreViewFile'],
                                'size' => $value['value']['Size']
                            ]);
                        }
                        break;
                }
                if (!empty($value['attr']['Calc'])) {
                    $_attr->setCalc(new Core\Calculation($value['attr']['Calc'], $value['attr']['CalcName'], ''));
                }
                if (!empty($value['attr']['LinkedClass'])) {
                    if ($value['attr']['LinkedClass']!=Core\Type\Type::TYPE_ROOT) {
                        $_attr->setLinkType(new Core\Type\Type($value['attr']['LinkedClass'], ''));
                    }
                }
                $attrs[$obj][] = $_attr;
            }
        }

        return $attrs;
    }

    /**
     * Add object
     *
     * @param Instance $obj
     * @param int $addedBy
     * @param User $user
     *
     * @return int
     */
    public function add(Instance $obj, int $addedBy, User $user)
    {
        $parent = !is_null($obj->getParent()) ? $obj->getParent()->getUid() : null;
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Class' => $obj->getType()->getUid(),
            'PID' => $parent,
            'AddedBy' => $addedBy,
            'UserID' => $user->getUid(),
            'Object' => empty($obj->getUid()) ? null : $obj->getUid(),
        ]);

        $result = Db::INITIAL_CODE;
        $errors = [];
        $codeText='';
        if (!empty($data)) 
        {
            $result = $data[0]['Rcode'];
            if ($result == Db::SUCCESS_CODE) 
            {
                $obj->setUid($data[0]['IDo']);
            }else 
            {
                $codeText=$data[0]['ErrCode'];
                $errors[] = ['text'=>DbError::getError(
                    self::PROCEDURES[__FUNCTION__]['name'], 
                    $result,
                    [],
                    null,
                    $codeText
                )];
                
            }
        }
        return ['result' => $result, 'errors' => $errors];
    }

    /**
     * Check uniqueness of object key params
     *
     * @param Instance $obj
     *
     * @return type
     */
    public function checkUnique(Instance $obj)
    {
        $parent = !is_null($obj->getParent()) ? $obj->getParent()->getUid() : null;
        $attrs = [];
        $values = [];

        foreach ($obj->getAttributes() as $attr) {
            if ($attr->isKey()) {
                $attrs[] = $attr->getUid();
                $val=$attr->getValue();
                if (is_null($val)) {
                    $values[] = null;
                } elseif ($attr->getDatatype()==DataType::TYPE_DATETIME) {
                    $values[] = $attr->getValue()->getValueDB();
                } elseif ($attr->getDatatype()==DataType::TYPE_OBJECT) {
                    $values[] = $attr->getValue()->getUid();
                }else {
                    $values[] = $attr->getValue()->getValue();
                }
            }
        }
        $values=Db::_escapeValue($values);
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Class' => $obj->getType()->getUid(),
            'Attrs' => implode(',', $attrs),
            'Vals' => implode(',', $values),
            'BaseObject' => $parent,
            'IDExistObj' => empty($obj->getUid()) ? null : $obj->getUid(),
        ]);

        $result = Db::INITIAL_CODE;
        $errors = [];
        if (!empty($data)) {
            $result = $data[0]['Rcode'];
            if (!empty($data[0]['Obj'])) {
                $obj->setUid($data[0]['Obj']);
                $result = 2;
            }
            if ($result != Db::SUCCESS_CODE) {
                $errors[] = [
                    'text' => DbError::getError(self::PROCEDURES[__FUNCTION__]['name'], $result)
                ];
            }
        }
        return ['result' => $result, 'errors' => $errors];
    }

    /**
     * Check uniqueness of object key params. Add object if uniqueness check is successfull
     *
     * @param Instance $obj
     * @param int $addedBy
     * @param User $user
     *
     * @return int
     */
    public function checkUniqueAndAdd(Instance $obj, int $addedBy, User $user)
    {
        $result = $this->checkUnique($obj);
        if ($result['result'] == Db::SUCCESS_CODE) {
            $result = $this->add($obj, $addedBy, $user);
        }

        return $result;
    }

    /**
     * Check uniqueness of object key params. Add object and values of params if uniqueness check is successfull
     *
     * @param Instance $obj
     * @param int $addedBy
     * @param User $user
     *
     * @return int|array
     */
    public function checkUniqueAndAddWithValues(Instance $obj, int $addedBy, User $user)
    {
        $result = $this->checkUniqueAndAdd($obj, $addedBy, $user);
        if ($result['result'] == Db::SUCCESS_CODE) {
            $result = $this->addValues([$obj], $user);
        }

        return $result;
    }

    /**
     * Check uniqueness of object key params. Update values of params if uniqueness check is successfull
     *
     * @param Instance $obj
     * @param User $user
     *
     * @return int|array
     */

    public function checkUniqueAndUpdateValues(Instance $obj, User $user)
    {
        $result = $this->checkUnique($obj);
        if ($result['result'] == Db::SUCCESS_CODE) {
            $result = $this->addValues([$obj], $user);
        }

        return $result;
    }

    /**
     * Delete object
     *
     * @param Instance $obj
     * @param User $user
     *
     * @return int
     */
    public function remove(Instance $obj, User $user)
    {
        $dataResult=['error'=>[],'result'=>-1,'data'=>[]];
        $codeText='';
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'IDs' => '{' . $obj->getUid() . '}',
            'UserID' => $user->getUid(),
            'ReturnNumBeforDel' => 0,
            'DeleteAllObjsOfClass'=>null
        ]);        
        if (!empty($data)) 
        {
            $dataResult['result'] = $data[0]['Rcode'];
            if($dataResult['result']!=1)
            {
                $codeText= $data[0]['ErrCode'];
            }
        }        
        if ($dataResult['result']!=1) 
        {
            $dataResult['error']=[];
            $dataResult['error'][]=['text'=>DbError::getError(
                self::PROCEDURES[__FUNCTION__]['name'], 
                $dataResult['result'],
                [],
                null,
                $codeText
            )];
        }
        return $dataResult;
    }

    /**
     * DELETE OBJECTS
     *
     * */
    public function removeObjs($ids, $user)
    {
        $data = Db::runProcedure(self::PROCEDURES["remove"], [
            'IDs' => '{' . implode(',', $ids ) . '}',
            'UserID' => $user->getUid(),
            'ReturnNumBeforDel' => 0,
            'DeleteAllObjsOfClass'=>null
        ]);

        $result = -1;
        if (!empty($data)) {
            $result = $data[0]['Rcode'];
        }
        return $result;
    }


    /**
     * Delete all objects class
     *
     * @param Core\Type\Type  $type
     * @param User $user
     * @param int $mode 0-delete|1-count
     * @return int
     */
    public function removeAllobjsClass(Core\Type\Type $type, User $user,$mode=0)
    {
        $dataResult['error']=[];
        $dataResult['data']=[];
        $result=-1;
        $data = Db::runProcedure(self::PROCEDURES['remove'], [
            'IDs' => null,
            'UserID' => $user->getUid(),
            'ReturnNumBeforDel' => $mode,
            'DeleteAllObjsOfClass'=>$type->getUid()
        ]);

        if (!empty($data))
        {

            $result = $data[0]['Rcode'];
            $dataResult['result']= $result;
            $dataResult['count'] = $data[0]['Num'];
        }
        if ($result != 1 && $mode == 1) {
            $dataResult['error'] = [];
            $dataResult['error'][] = ['text' => DbError::getError(self::PROCEDURES['remove']['name'], $result)];
        }
        $dataResult['result']=$result;
        return $dataResult;
    }

    /**
     * Return count of objects that will be deleted with passed object
     *
     * @param Instance $obj
     * @param User $user
     *
     * @return int
     */
    public function getDeleteCount(Instance $obj, User $user)
    {
        $data = Db::runProcedure(self::PROCEDURES['remove'], [
            'IDs' => '{' . $obj->getUid() . '}',
            'UserID' => $user->getUid(),
            'ReturnNumBeforDel' => 1,
            'DeleteAllObjsOfClass'=>null
        ]);

        $num = 0;
        if (!empty($data)) {
            $num = $data[0]['Num'];
        }
        return $num;
    }

    /**
     * Return all objects
     *
     * @return Instance[]
     */
    public function getAll()
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], []);
        echo json_encode($data);
        return [];
        $objs = $this->_initObjs($data);
        return $objs;
    }
    public function getAlltest()
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], []);
       
        return $data;
    }

    /**
     * Get object children of specific type
     *
     * @param Instance $obj
     * @param Core\Type\Type $type|null
     *
     * @return type
     */
    public function getChildrenByType(Instance $obj,  $type=null)
    {
        $uidType=null;
        if(!is_null($type))
        {
            $uidType=$type->getUid();
        }
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'BaseObject' => $obj->getUid(),
            'Class' =>$uidType,
        ]);

        $objects = [];
        foreach ($data as $value) {             
            $_parent = new Core\Type\Type( $value['ChildClass'],$value['ChildClassName']);
            $objects[] = new Instance($value['IDo'], is_null($value['Name']) ? '' : $value['Name'], $_parent);
        }

        return $objects;
    }

    /**
     * Return all values of object
     *
     * @param Instance[] $obj
     *
     * @return Attribute[]
     */
    public function getAllValues(array $objs)
    {
        $uids=[];
        $objArr=[];
        if(empty($objs)) return [];
        $mode=empty($objs[0]->getAttributes())? 1:2;
        foreach ($objs as $key => $valObj) {
            if ($mode==1) {
                $uids[]=$valObj->getUid();
            } elseif ($mode==2) {
                $attrArr=[];
                foreach ($valObj->getAttributes() as $valAttr) {
                    $attrArr[]=$valAttr->getUid();
                }
                $objArr[]=[
              'Obj'=>$valObj->getUid(),
              'Attrs'=>$attrArr
            ];
            }
        }

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'ObjsNeedAllAttrs' => empty($uids)?null:'{' .implode(',', $uids). '}',
            'ObjsNeedSpecifiedAttrs'=>empty($objArr)?null:json_encode($objArr)
        ]);
        $attrs=$this->_initObjsAttrs($data);
       
        foreach ($objs as $key => $value) {
            $value->setAttributes($attrs[$value->getUid()]);
        }

        return $attrs;
    }




    private function _fillObjAttrs($data) {
        $attrs = [];
        $_attrs = [];

        foreach ($data as $value) {
            $tnpmV = [
                'value' => $value['V'],
                'LinkedObjName' => $value['LinkedObjName'],
                'GenFileName' => $value['GenFileName'],
                'PreViewFile' => $value['PreViewFile'],
                'Size' => $value['Size'],
                'RealFileName' => $value['RealFileName'],
                'LinkedClass' => (isset($value['LinkedClass']) ? $value['LinkedClass'] : null),
            ];
            if(!isset($value['Array'])) {
                $value['Array'] = null;
            }
            $value['Array'] = null;
            if ($value['Array']) {
                $_attrs[$value['Attr']] = isset($_attrs[$value['Attr']]) ? $_attrs[$value['Attr']] : ['attr' => $value, 'value' => []];
                if ($value['TypeOfData'] != 5) {
                    if (!is_null($value['V'])) {
                        $_attrs[$value['Attr']]['value'][] = $tnpmV;
                    }
                } else {
                    if (!is_null($value['GenFileName'])) {
                        $_attrs[$value['Attr']]['value'][] = $tnpmV;
                    }
                }
            } else {
                $_attrs[$value['Attr']] = ['attr' => $value, 'value' => $tnpmV];
            }
        }


        foreach ($_attrs as $uid => $value) {
            $value['attr']['Uniq'] = is_null($value['attr']['Uniq']) ? 0 : $value['attr']['Uniq'];
            $value['attr']['Array'] = is_null($value['attr']['Array']) ? 0 : $value['attr']['Array'];
            if(isset($value['attr']['AttrName'])) {
                $value['attr']['Name'] = $value['attr']['AttrName'];
            }
            $_attr = new Attribute(
                $value['attr']['Attr'],
                $value['attr']['Name'],
                $value['attr']['TypeOfData'],
                $value['attr']['Uniq'],
                $value['attr']['Array']
            );
            if (!empty($value['attr']['Settings'])) {
                $charlistics = json_decode($value['attr']['Settings'], true);
                if (!empty($charlistics)) {
                    $_attr->setCharlistics($charlistics);
                }
            }
            switch (DataType::getTypeFromIndex($value['attr']['TypeOfData'])) {
                case DataType::TYPE_NUMBER:
                    if ($value['attr']['Array']) {
                        $tmpV = [];
                        foreach ($value['value'] as $key => $valAttr) {
                            $tmpV[] = ['value' => $valAttr['value'], 'convValue' => null];
                        }
                        $_attr->setValue(['value' => $tmpV]);
                    } else {
                        $_attr->setValue(['value' => $value['value']['value'], 'convValue' => null]);
                    }
                    //TODO converted value
                    break;
                case DataType::TYPE_TEXT:
                case DataType::TYPE_DATETIME:
                    if ($value['attr']['Array']) {
                        $tmpV = [];
                        foreach ($value['value'] as $key => $valAttr) {
                            $tmpV[] = ['value' => $valAttr['value']];
                        }
                        $_attr->setValue(['value' => $tmpV]);
                    } else {
                        $_attr->setValue(['value' => $value['value']['value']]);
                    }
                    break;
                case DataType::TYPE_OBJECT:
                    if ($value['attr']['Array']) {
                        $tmpV = [];
                        foreach ($value['value'] as $key => $valAttr) {
                            $tmpV[] = [
                                'value' => $valAttr['value'],
                                'name' => $valAttr['LinkedObjName'],
                                'type' => [
                                    'value' => $valAttr['LinkedClass'],
                                    'name' => ''
                                ]
                            ];
                        }
                        $_attr->setValue(['value'=>$tmpV]);
                    } else {
                        $_attr->setValue([
                            'value' => $value['value']['value'],
                            'name' => $value['value']['LinkedObjName'],
                            'type' => [
                                'value' => $value['value']['LinkedClass'],
                                'name' => ''
                            ]
                        ]);
                    }
                    break;
                case DataType::TYPE_COUNTER:
                    if ($value['attr']['Array']){
                        $tmpV = [];
                        foreach ($value['value'] as $key => $valAttr) {
                            $tmpV[] = ['value' => $valAttr['value']];
                        }
                        $_attr->setValue(['value' => $tmpV]);
                    } else {
                        $_attr->setValue(['value' => $value['value']['value']]);
                    }
                    break;
                case DataType::TYPE_FILE:
                    if ($value['attr']['Array']){
                        $tmpV=[];
                        foreach ($value['value'] as $key => $valAttr) {
                            $tmpV[] = [
                                'value' => $valAttr['GenFileName'],
                                'name' => $valAttr['RealFileName'],
                                'preview' => $valAttr['PreViewFile'],
                                'size' => $valAttr['Size']
                            ];
                        }
                        $_attr->setValue(['value'=>$tmpV]);
                    } else {
                        $_attr->setValue([
                            'value' => $value['value']['GenFileName'],
                            'name' => $value['value']['RealFileName'],
                            'preview' => $value['value']['PreViewFile'],
                            'size' => $value['value']['Size']
                        ]);
                    }
                    break;
            }
            if (!empty($value['attr']['Calc'])) {
                $_attr->setCalc(new Core\Calculation($value['attr']['Calc'], $value['attr']['CalcName'], ''));
            }
            if (!empty($value['attr']['LinkedClass'])) {
                if ($value['attr']['LinkedClass']!=Core\Type\Type::TYPE_ROOT) {
                    $_attr->setLinkType(new Core\Type\Type($value['attr']['LinkedClass'], ''));
                }
            }

            if (!empty($value['attr']['LastUpdated'])) {
                $_attr->setLastUpdated($value['attr']['LastUpdated']);
            }
            if (!empty($value['attr']['Login'])) {
                $_attr->setLogin($value['attr']['Login']);
            }
            $attrs[] = $_attr;
        }

        return $_attrs;
    }

    /**
     * кто будет рефакторить, может дописать
     *
     * Return values of object for Last Update Time
     *
     * @param Instance $obj
     *
     * @return Attribute[]
     */
    public function getValuesAndLastUpdateTime(Instance $obj)
    {
        $attr = [];
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Obj' => $obj->getUid()
        ]);

        $attr=$this->_fillObjAttrs($data);

        $obj->setAttributes($attr);
        return $attr;
    }



    /**
     Если кто-то будет смотреть
     * ParentObs: string[],
     * ChildClassesAndAtrrs: {cl:string, attr:string}[]
     *
     *
     * return {[objPar:string]: { objects:{object}[]  }  }
     */
    public function GetValuesForChildObjs(array $ParentObs, $ChildClassesAndAtrrs)
    {
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'ParentObs' => $ParentObs,
            'ChildClassesAndAtrrs' => $ChildClassesAndAtrrs
        ]);

        $p_objs = [];
        foreach ($data as $value) {
            if( !isset($p_objs[$value['PID']]) ) {
                $p_objs[$value['PID']] = [
                    "_objects" => []
                ];
            }
            if( !isset($p_objs[$value['PID']]["_objects"][$value['ID']]) ) {
                $p_objs[$value['PID']]["_objects"][$value['ID']] = [
                    "_attrs" => [],
                    "data" => $value,
                ];
            }

            $value["Obj"] = $value['ID'];
            $value["Name"] = $value['AttrName'];

            array_push($p_objs[$value['PID']]["_objects"][$value['ID']]["_attrs"], $value);
        }

        $data_res = [];
        foreach ($p_objs as $p_key=>$p_item) {
            $objects_tmp = [];
            foreach ($p_item['_objects'] as $key_child=>$child) {
                //форматируем данные
                $attr_tmp = $this->_initObjsAttrs($p_objs[$p_key]['_objects'][$key_child]["_attrs"])[$key_child];
                $_type = new Core\Type\Type( $child["data"]['Class'], $child["data"]["ClassName"]);
                $_parent = new Instance( $child["data"]['PID'], "tmp_obj_name");
                $object_tmp = new Instance($child["data"]['ID'], is_null($child["data"]['ObjName']) ? '' : $child["data"]['ObjName'], $_type, $_parent, $attr_tmp);

                array_push($objects_tmp, $object_tmp);
            }

            $data_res[$p_key]["objects"] = $objects_tmp;
        }
        return $data_res;
    }

    /**
     * Add files to object
     *
     * @param Instance $obj
     * @param User $user
     *
     * @return array
     */
    public function AddFiles(Instance $obj, User $user)
    {
        $attrs = [];
        $realNames=[];
        $GenNames=[];
        $Sizes=[];
        $Previews=[];
        $results = [];
        foreach ($obj->getAttributes() as $attr) {
            $value = $attr->getValue();
            $attrs[] = $attr->getUid();
            if (is_null($value)) {
            } else {
                if ($attr->isArray()) {
                    $realNamesA=[];
                    $GenNamesA=[];
                    $SizesA=[];
                    $PreviewsS=[];
                    foreach ($value as  $valArr) {
                        $realNamesA[]=$valArr->getName();
                        $GenNamesA[]=$valArr->getUid();
                        $SizesA[]=$valArr->getSize();
                        $PreviewsS[]=$valArr->getPreviewDB();
                    }
                    //DataType::ARRAY_DELIMITERDataType::ARRAY_DELIMITER
                    $realNames[]=implode(DataType::ARRAY_DELIMITER, $realNamesA);
                    $GenNames[]=implode(DataType::ARRAY_DELIMITER, $GenNamesA);
                    $Sizes[]=implode(DataType::ARRAY_DELIMITER, $SizesA);
                    $Previews[]=implode(DataType::ARRAY_DELIMITER, $PreviewsS);
                } else {
                    $name=$value->getValue();
                    if (!empty($name)) {
                        $realNames[]=$value->getName();
                        $GenNames[]=$value->getValue();
                        $Sizes[]=$value->getSize();
                        $Previews[]=$value->getPreviewDB();
                    } else {
                        $realNames[]=null;
                        $GenNames[]=null;
                        $Sizes[]=null;
                        $Previews[]=null;
                    }
                }
            }
            $results[$attr->getUid()] = ['attr' => $attr, 'result' => -1];
        }
        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Objs' => '{' . $obj->getUid() . '}',
            'Attrs' => '{' .  implode(',', $attrs) . '}',
            'RealNames'=> '{"' . implode('", "', $realNames) . '"}',
            'GenNames' => '{"' . implode('", "', $GenNames) . '"}',
            'Size' => '{"' . implode('", "', $Sizes) . '"}',
            'Previews' => '{"' . implode('", "', $Previews) . '"}',
            'User' => '{' . $user->getUid() . '}',
        ]);

        if (!empty($data)) {
            foreach ($data as $value) {
                $results[$value['Attr']]['result'] = $value['Rcode'];
            }
        }
        return $results;
    }

    /**
     * Return objects of attribute linked class by objects names
     *
     * @param array $attributes
     *
     * @return array
     */
    public function getObjectsByAttributesAndObjectNames(array $attributes) {
        $attrs = [];
        foreach ($attributes as $attribute) {
            if (!empty($attribute['names'])) {
                $attrs[] = [
                    'Attr' => $attribute['attr']->getUid(),
                    'Names' => $attribute['names'],
                ];
            }
        }

        $objects = [];
        if (!empty($attrs)) {
            $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
                'Attrs' => json_encode($attrs)
            ]);

            foreach ($data as $value) {
                $objects[$value['Attr']] = isset($objects[$value['Attr']]) ? $objects[$value['Attr']] : ['attr' => null, 'objects' => []];
                foreach ($attributes as $attribute) {
                    if ($attribute['attr']->getUid() == $value['Attr']) {
                        $objects[$value['Attr']]['attr'] = $attribute['attr'];
                        break;
                    }
                }
                $objects[$value['Attr']]['objects'][$value['OName']] = isset($objects[$value['Attr']]['objects'][$value['OName']]) ? $objects[$value['Attr']]['objects'][$value['OName']] : [];
                $objects[$value['Attr']]['objects'][$value['OName']][] = new Instance($value['ID'], $value['OName']);
            }
        }

        return $objects;
    }

    public function Object_GetNames($objs, $GetNumOfChildren = 0) {
        $uids=[];
        $objArr=[];
        if(empty($objs)) return [];
        

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Objs' => $objs,
            'GetNumOfChildren' => intval($GetNumOfChildren)
        ]);


        return $data;
    }
    public function ObjectGetNames(array $objs, $GetNumOfChildren = 0) {
        $uids=[];
        $objArr=[];
        if(empty($objs)) return [];
        foreach ($objs as $key => $value) 
        {
            $objArr[]=$value->getUid();
        }

        $data = Db::runProcedure(self::PROCEDURES[__FUNCTION__], [
            'Objs' => $objArr,
            'GetNumOfChildren' => intval($GetNumOfChildren)
        ]);
        foreach ($data as $key => $value) 
        {
            $keyNum=array_search($value['ID'], $objArr);
            if($keyNum!==false)
            {
                $objs[$key]->setName($value['Name']);
            }
        }

        return $data;
    }


    public function getValueForParamObj($obj, $param) {
        $data = Db::runProcedure(self::PROCEDURES['getAllValues'], [
            'ObjsNeedAllAttrs' => null,
            'ObjsNeedSpecifiedAttrs' => json_encode([["Obj" => $obj, "Attrs" => [$param] ]])
        ]);
        return $data[0];
    }

    public function getInfObject($obj, $user_uid) {
        $data = Db::runProcedure(self::PROCEDURES['Object_GetOwner'], [
            'Obj' => $obj
        ]);
        return isset($data[0]) ? $data[0] : [];
    }
}
