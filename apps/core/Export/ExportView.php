<?php

namespace Dzeta\Core\Export;

class ExportView extends Export
{
	public function addRows(array $rows, bool $isHeader = false) {
		foreach ($rows as $row) {
			$_row = [];
			foreach ($row as $key => $value) {
				if ($key !== 'row_number') {
					$v = isset($value['v']) ? $value['v'] : null;
					$v = isset($value['ln']) ? $value['ln'] : $v;
					$_row[] = $v;
				}
			}
			$this->writeRow($_row, $isHeader);
		}
	}
}