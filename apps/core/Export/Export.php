<?php

namespace Dzeta\Core\Export;

use Dzeta\Core\Progress;

abstract class Export
{
	const SHEET = 'Sheet1';

	protected $writer;

	abstract public function addRows(array $data, bool $isHeader);

	public function __construct() {
		$this->writer = new \XLSXWriter();
	}

	public function save() {
		$tempname = tempnam(sys_get_temp_dir(), 'export');
		$this->writer->writeToFile($tempname);
		return $tempname;
	}

	public function mergeCells($startRow = 0, $startCol = 0, $endRow = 0, $endCol = 0) {
		$this->writer->markMergedCell(self::SHEET, $startRow, $startCol, $endRow, $endCol);
	}

	public function writeRow(array $row, bool $isHeader, bool $isHorizontalCenter = false, bool $isVerticalCenter = false, bool $isWrap = false) {
		$style = [];
		foreach ($row as $value) {
			$cellStyle = [
				'border' => 'top,left,right,bottom',
				'border-style' => 'thin',
			];
			if ($isHeader) {
				$cellStyle['font-style'] = 'bold';
			}
			if ($isHorizontalCenter) {
				$cellStyle['halign'] = 'center';
			}
			if ($isVerticalCenter) {
				$cellStyle['valign'] = 'center';
			}
			if ($isWrap) {
				$cellStyle['wrap_text'] = true;
			}
			$style[] = $cellStyle;
		}
		$this->writer->writeSheetRow(self::SHEET, $row, $style);
	}

	public function writeHeadersOptions(array $types, $options = null) {
		$this->writer->writeSheetHeader(self::SHEET, $types, $options);
	}
}