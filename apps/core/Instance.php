<?php

namespace Dzeta\Core;

use Dzeta\Core\Type\Type;

class Instance extends Entity implements \JsonSerializable
{
	const INSTANCE_ROOT = '00000000-0000-0000-0000-000000000000';

	const ADDED_BY_USER = 0;
	const ADDED_BY_IMPORT = 1;
	const ADDED_BY_EXTERNAL_SOURCE = 2;
	const ADDED_BY_CALC = 3;

	/**
	 * Parent of the instance
	 *
	 * @var Instance
	 */
	private $parent;

	/**
	 * Type of the instance
	 *
	 * @var Type
	 */
	private $type;

	/**
	 * Attributes of the instance
	 *
	 * @var Attributes[]
	 */
	private $attributes;

	/**
	 * Children of the instance
	 *
	 * @var Instance[]
	 */
	private $children;

	function __construct(string $uid, string $name, ?Type $type = null, ?Instance $parent = null, array $attributes = [], array $children = []) {
		parent::__construct($uid, $name);
		$this->attributes = $attributes;
		$this->type = $type;
		$this->parent = $parent;
		$this->children = $children;
	}

	public function getAttributes() {
		return $this->attributes;
	}

	public function getAttribute(string $uid) {
		foreach ($this->attributes as $attribute) {
			if ($attribute->getUid() == $uid) {
				return $attribute;
			}
		}
		return null;
	}

	public function getType() {
		return $this->type;
	}

	public function getParent() {
		return $this->parent;
	}

	public function getChildren() {
		return $this->children;
	}

	public function setAttributes(array $attributes) {
		$this->attributes = $attributes;
		return $this;
	}

	public function setType(Type $type) {
		$this->type = $type;
		return $this;
	}

	public function setParent(Instance $parent) {
		$this->parent = $parent;
		return $this;
	}

	public function setChildren(array $children) {
		$this->children = $children;
		return $this;
	}

	public function jsonSerialize() {
		$vars = get_object_vars($this);
		$parentVars = parent::jsonSerialize();
		return array_merge($vars, $parentVars);
	}
}
