<?php

namespace Dzeta\Core\Type;

use Dzeta\Core\Unit\Unit;
use Dzeta\Core\Unit\UnitGroup;
use Dzeta\Core\Instance;
use Dzeta\Core\Entity;

use Dzeta\Core\Value\TextV;
use Dzeta\Core\Value\DateTimeV;
use Dzeta\Core\Value\File;
use Dzeta\Core\Value\NumV;
use Dzeta\Core\Value\CounterV;

class Attribute extends Entity implements \JsonSerializable
{
    /**
     * Type of the attribute
     *
     * @var string
     */
    private $datatype;

    /**
     * Key flag
     *
     * @var bool
     */
    private $isKey;


    /**
     * Array flag
     *
     * @var bool
     */
    private $isArray;

    /**
     * Name flag
     *
     * @var bool
     */
    private $isName;

    /**
     * Attribute index in group
     *
     * @var int
     */
    private $index;

    /**
     * Unit group of the attribute
     *
     * @var UnitGroup
     */
    private $unitGroup;

    /**
     * Unit of the attribute
     *
     * @var Unit
     */
    private $unit;

    /**
     * Parent type of the attribute
     *
     * @var Type
     *
     */
    private $parent;

    /**
     * Group of the attribute
     *
     * @var AttributeGroup
     */
    private $group;

    /**
     * Value of the attribute for instance
     *
     * @var mixed
     */
    private $value;
    /**
     * Group of the attribute
     *
     * @var TextType
     */
    private $TextType;


    private $require=false;

    private $ShowCameraModule=0;

    private $LastUpdated=null;
    private $Login=null;


    private $calc_triggers=['manual'=>null];


    private $dopSett=[];
    /**
    *
    *
    *
    */
    private $calc=null;

    private $LinkType=null;
    /**
     * Create a new Attribute
     *
     * @param string $uid
     * @param string $name
     * @param string $TextType
     * @param int $datatype
     * @param bool $isKey
     * @param bool $isArray
     * @param bool $isName
     */
    public function __construct(string $uid, string $name, int $datatype, bool $isKey, bool $isArray, bool $isName = false)
    {
        parent::__construct($uid, $name);
        $this->datatype = DataType::getTypeFromIndex($datatype);
        $this->isArray = $isArray;
        $this->isName = $isName;
        $this->setIsKey($isKey);
    }

    public function setCalcTrig($calc,$type)
    {
        switch ($type) {
          case 'manual':
            $this->calc_triggers=['manual'=>$calc];
            break;
          default:
            // code...
            break;
        }
    }
    public function setCharlistics(array $charlistics)
    {

      if(isset($charlistics['require']))
      {
        $this->require=(bool)$charlistics['require'];
      }
      if(isset($charlistics["TextType"])) {
          $this->TextType = $charlistics["TextType"];
      }
      if(isset($charlistics["ShowCameraModule"])) {
          $this->ShowCameraModule = empty($charlistics["ShowCameraModule"])? 0:1;
      }
      if(isset($charlistics["LastUpdated"])) {
          $this->LastUpdated = empty($charlistics["LastUpdated"])? null:$charlistics["LastUpdated"];
      }
      if(isset($charlistics["Login"])) {
          $this->Login = empty($charlistics["Login"])? null:$charlistics["Login"];
      }
      if(isset($charlistics['calc_triggers']))
      {
        foreach ($charlistics['calc_triggers'] as $key=>  $value) {

          if($key=='manual'&&!empty($value))
          {
            $this->calc_triggers['manual']=new \Dzeta\Core\Calculation($value, '');
          }
        }
        $this->require=(bool)$charlistics['require'];
      }
    }
    public function getCharlictics()
    {

        return [
          'require'=>(int)$this->require,
          'TextType'=>$this->TextType,
          'ShowCameraModule'=>(int)$this->ShowCameraModule,
          'LastUpdated'=>$this->LastUpdated,
          'Login'=>$this->Login,
          'calc_triggers'=>[
              'manual'=>is_null($this->calc_triggers['manual'])? null:$this->calc_triggers['manual']->getUid()
          ]
        ];
    }
    public function setDopSett($key, $val)
    {
        $this->dopSett[$key]=$val;
    }

    public function getDopSett($key)
    {
        if (isset($this->dopSett[$key])) {
            return $this->dopSett[$key];
        }
        return null;
    }

    public function getRequare()
    {
        return $this->require;
    }
    public function setRequare(bool $requare)
    {
        if(!$requare&&$this->isName())
        {
            return;
        }
        $this->require=$requare;
    }
    public function getShowCamera()
    {
        return $this->ShowCameraModule;
    }
    public function getLastUpdated()
    {
        return $this->LastUpdated;
    }
    public function getLogin()
    {
        return $this->Login;
    }
    public function setShowCamera($showCamera)
    {
       $this->ShowCameraModule=empty($showCamera)? 0:1;
    }
    public function setLastUpdated($LastUpdated)
    {
       $this->LastUpdated=empty($LastUpdated)? null:$LastUpdated;
    }
    public function setLogin($Login)
    {
       $this->Login=empty($Login)? null:$Login;
    }


    public function getDatatype()
    {
        return $this->datatype;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getGroup()
    {
        return $this->group;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getUnitGroup()
    {
        return $this->unitGroup;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getCalc()
    {
        return $this->calc;
    }
    public function getTextType()
    {
        return $this->TextType;
    }

    public function isKey()
    {
        return $this->isKey;
    }

    public function isArray()
    {
        return $this->isArray;
    }

    public function isName()
    {
        return $this->isName;
    }

    public function setCalc($calc)
    {
        $this->calc=$calc;
    }
    public function setTextType($TextType)
    {
        $this->TextType=$TextType;
    }

    public function setGroup(AttributeGroup $group)
    {
        $this->group = $group;
        return $this;
    }

    public function setParent(Type $parent)
    {
        $this->parent = $parent;
        return $this;
    }
    public function setLinkType(Type $LinkType)
    {
        $this->LinkType = $LinkType;
        return $this;
    }
    public function getLinkType()
    {
        return $this->LinkType;
    }


    public function setIndex(int $index)
    {
        $this->index = $index;
        return $this;
    }
    public function setDatatype(int $datatype)
    {
        $this->datatype = DataType::getTypeFromIndex($datatype);
        return $this;
    }

    public function setIsKey(bool $isKey)
    {
        if($isKey)
        {
          if(DataType::TYPE_COUNTER!=$this->datatype)
          {

        //    $this->require=true;
          }
        }
        $this->isKey = $isKey;
        return $this;
    }

    public function setIsArray(bool $isArray)
    {
        $this->isArray = $isArray;
        return $this;
    }

    public function setUnitGroup(UnitGroup $unitGroup)
    {
        $this->unitGroup = $unitGroup;
        return $this;
    }

    public function setUnit(Unit $unit)
    {
        $this->unit = $unit;
        return $this;
    }

    public function setValue($value)
    {
        $v = null;
        if (isset($value['value'])) {
            if (is_array($value['value'])) {
                if (count($value['value']) == 0) {
                    $value['value'] = null;
                }
            }
        }

        if (!is_null($value['value']) && $value['value'] !== '') {
            switch ($this->datatype) {
                case DataType::TYPE_TEXT:
                    if($this->isArray) {
                        $tmpV = [];
                        foreach ($value['value'] as  $val) {
                            $tmpV[] = new TextV($val['value']);
                        }
                        $v = $tmpV;
                    } else {
                        $v = new TextV($value['value']);
                    }
                    break;
                case DataType::TYPE_NUMBER:
                    if ($this->isArray) {
                        $tmpV=[];
                        foreach ($value['value'] as $val) {
                            $convValue = isset($val['convValue']) ? $val['convValue'] : null;
                            $tmpV[] = new NumV($val['value'], $convValue);
                        }
                        $v = $tmpV;
                    } else {
                        $convValue = isset($value['convValue']) ? $value['convValue'] : null;
                        $v = new NumV($value['value'], $convValue);
                    }
                    break;
                case DataType::TYPE_DATETIME:
                    if ($this->isArray) {
                        $tmpV = [];
                        foreach ($value['value'] as $val) {
                            $td = \DateTime::createFromFormat(DateTimeV::DATE_FORMAT, $val['value']);
                            if ($td === false) {
                                $td = new \DateTime($val['value']);
                            }
                            if (!empty($td)) {
                                $tmpV[] = new DateTimeV($td);
                            }
                        }
                        if (!empty($tmpV)) {
                            $v = $tmpV;
                        }
                    } else {
                        $td = \DateTime::createFromFormat(DateTimeV::DATE_FORMAT, $value['value']);
                        if ($td === false) {
                            $td = \DateTime::createFromFormat(DateTimeV::DATE_FORMAT_new, $value['value']);
                            if ($td === false) {
                                try {
                                    $td = new \DateTime($value['value']);
                                } catch (\Exception $e) {
                                    $td = false;
                                }
                            }
                        }
                        if (!empty($td)) {
                            $v = new DateTimeV($td);
                        }
                    }
                    break;
                case DataType::TYPE_OBJECT:
                    if ($this->isArray) {
                        $tmpV = [];
                        foreach ($value['value'] as $val) {
                            $uid = isset($val['value']) ? $val['value'] : '';
                            $name = isset($val['name']) ? $val['name'] : '';
                            $type = null;
                            if (isset($val['type'])) {
                                $type = new Type(
                                    isset($val['type']['value']) ? $val['type']['value'] : '',
                                    isset($val['type']['name']) ? $val['type']['name'] : ''
                                );
                            }
                            $tmpV[] = new Instance($uid, $name, $type);
                        }
                        $v = $tmpV;
                    } else {
                        $uid = isset($value['value']) ? $value['value'] : '';
                        $name = isset($value['name']) ? $value['name'] : '';
                        $type = null;
                        if (isset($value['type'])) {
                            $type = new Type(
                                isset($value['type']['value']) ? $value['type']['value'] : '',
                                isset($value['type']['name']) ? $value['type']['name'] : ''
                            );
                        }
                        $v = new Instance($uid, $name, $type);
                    }
                    break;
                case DataType::TYPE_COUNTER:
                    if ($this->isArray) {
                        $v = null;
                    } else {
                        $v = new CounterV($value['value']);
                    }
                    break;
                case DataType::TYPE_FILE:
                    if ($this->isArray) {
                        $tmpV = [];
                        foreach ($value['value'] as $val) {
                            $tmpV[] = new File(
                                $val['value'],
                                $val['name'],
                                $val['preview'],
                                $val['size']
                            );
                        }
                        $v = $tmpV;
                    } else {
                        $v = new File(
                            $value['value'],
                            $value['name'],
                            $value['preview'],
                            $value['size']
                        );
                    }
                    break;
            }
        }
        $this->value = $v;
        return $this;
    }
    public function  setValueS($v)
    {
      $this->value = $v;
      return $this;
    }


    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        return array_merge($vars, $parentVars);
    }
}
