<?php

namespace Dzeta\Core\Type;

use Dzeta\Core\Entity;
use Dzeta\Core\Calculation;

class Type extends Entity implements \JsonSerializable
{
	const TYPE_ROOT = '00000000-0000-0000-0000-000000000000';

	/**
	 * Parent of the type
	 *
	 * @var Type
	 */
	private $parent;

	/**
	 * Type used for inheritance
	 *
	 * @var Type
	 */
	private $baseType;

	/**
	 * Children of the type
	 *
	 * @var Type[]
	 */
	private $children;

	/**
	 * Attribute groups of the type
	 *
	 * @var AttributeGroup[]
	 */
	private $groups = [];

	/**
	 * Calculations of the type
	 * 
	 * @var Calculation[]
	 */
	private $calculations = [];

	/**
	 * System type flag
	 *
	 * @var bool
	 */
	private $isSystem;

	function __construct(string $uid, string $name, bool $isSystem = false, ?Type $parent = null, ?Type $baseType = null, array $groups = []) {
		parent::__construct($uid, $name);
		$this->groups = $groups;
		$this->parent = $parent;
		$this->baseType = $baseType;
		$this->isSystem = $isSystem;
		$this->children = [];
	}

	public function getGroups() {
		return $this->groups;
	}

	public function getCalculations() {
		return $this->calculations;
	}

	public function getParent() {
		return $this->parent;
	}

	public function getBaseType() {
		return $this->baseType;
	}

	public function isSystem() {
		return $this->isSystem;
	}

	public function setGroups(array $groups) {
		$this->groups = $groups;
		return $this;
	}

	public function setCalculations(array $calculations) {
		$this->calculations = $calculations;
		return $this;
	}

	public function setParent(Type $parent) {
		$this->parent = $parent;
		return $this;
	}

	public function setBaseType(Type $baseType) {
		$this->baseType = $baseType;
		return $this;
	}

	public function setIsSystem(bool $isSystem) {
		$this->isSystem = $isSystem;
		return $this;
	}

	public function setChildren(array $children) {
		$this->children = $children;
		return $this;
	}

	public function addChild(Type $child) {
		$this->children[] = $child;
		return $this;
	}

	public function addCalculation(Calculation $calculation) {
		$this->calculations[] = $calculation;
		return $this;
	}

	public function jsonSerialize() {
		$vars = get_object_vars($this);
		$parentVars = parent::jsonSerialize();
		return array_merge($vars, $parentVars);
	}
}
