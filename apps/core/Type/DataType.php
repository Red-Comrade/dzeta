<?php

namespace Dzeta\Core\Type;

class DataType
{
	const TYPE_STRING = 'string';
	const TYPE_NUMBER = 'number';
	const TYPE_DATE = 'date';
	const TYPE_DATETIME = 'datetime';
	const TYPE_TIME = 'time';
	const TYPE_TEXT = 'text';
	const TYPE_FILE = 'file';
	const TYPE_COUNTER = 'counter';
	const TYPE_OBJECT = 'object';
	const TYPE_JSON = 'json';
	const TYPE_ARRAY = 'array';

	const ARRAY_DELIMITER = '@#';
	const UNIT_VALUE_DELIMITER = '!';

	/**
	 * Return type from index
	 *
	 * @param number $index
	 *
	 * @return string
	 */
	public static function getTypeFromIndex(int $index) {
		$datatype = null;
		switch ($index) {
			case 1:
				$datatype = self::TYPE_NUMBER;
				break;
			case 2:
				$datatype = self::TYPE_TEXT;
				break;
			case 3:
				$datatype = self::TYPE_DATETIME;
				break;
			case 4:
				$datatype = self::TYPE_OBJECT;
				break;
			case 5:
				$datatype = self::TYPE_FILE;
				break;
			case 6:
				$datatype = self::TYPE_COUNTER;
				break;
		}
		return $datatype;
	}

	/**
	 * Return index from type
	 *
	 * @param string $type
	 *
	 * @return int
	 */
	public static function getIndexFromType(string $type) {
		$datatype = null;
		switch ($type) {
			case self::TYPE_NUMBER:
				$datatype = 1;
				break;
			case self::TYPE_TEXT:
				$datatype = 2;
				break;
			case self::TYPE_DATETIME:
				$datatype = 3;
				break;
			case self::TYPE_OBJECT:
				$datatype = 4;
				break;
			case self::TYPE_FILE:
				$datatype = 5;
				break;
			case self::TYPE_COUNTER:
				$datatype = 6;
				break;
		}
		return $datatype;
	}
}
