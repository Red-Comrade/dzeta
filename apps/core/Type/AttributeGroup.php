<?php

namespace Dzeta\Core\Type;

use Dzeta\Core\Entity;

class AttributeGroup extends Entity implements \JsonSerializable
{

	const ATTRIBUTE_GROUP_ROOT = '00000000-0000-0000-0000-000000000000';

	/**
	 * Parent type of the group
	 *
	 * @var Type
	 */
	private $parent;

	/**
	 * Attributes of the group
	 *
	 * @var Attribute[]
	 */
	private $attributes;

	function __construct(string $uid, string $name, array $attributes = []) {
		parent::__construct($uid, $name);
		$this->attributes = $attributes;
	}

	public function getParent() {
		return $this->parent;
	}

	public function getAttributes() {
		return $this->attributes;
	}

	public function setParent(Type $parent) {
		$this->parent = $parent;
		return $this;
	}

	public function setAttributes(array $attributes) {
		$this->attributes = $attributes;
		return $this;
	}

	public function jsonSerialize() {
		$vars = get_object_vars($this);
		$parentVars = parent::jsonSerialize();
		return array_merge($vars, $parentVars);
	}
}
