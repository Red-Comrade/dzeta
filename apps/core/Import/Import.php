<?php

namespace Dzeta\Core\Import;

use Dzeta\Core\Progress;

class Import
{
    const ARRAY_DELIMITER = ',';

	private $filename;

	private $settings;

	private $sheet;

    private $bounds;

    private $totalRows;

    private $chunkSize;

    private $startRow;

    private $currentRow;

    private $reader;

	public function __construct(string $filename = '', array $settings = [], ?string $sheet = null) {
		$this->filename = $filename;
        $_settings = [];
        foreach ($settings as $column) {
            if ($column['attribute'] != '') {
                $_settings[] = $column;
            }
        }
		$this->settings = $_settings;
        $this->sheet = $sheet;
        $this->startRow = 1;
        if ($filename != '') {
            $options = LIBXML_COMPACT | LIBXML_PARSEHUGE;
            \PhpOffice\PhpSpreadsheet\Settings::setLibXmlLoaderOptions($options);
            \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder(new \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder());
            $this->reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($this->filename);
            if (!empty($this->sheet)) {
                $this->reader->setLoadSheetsOnly($this->sheet);
            }
            $this->findBounds();
            $this->countTotalRows();
            $this->calculateChunkSize();
        }
	}

    public function getTotalRows() {
        return $this->totalRows;
    }

	public function setSheet($sheet) {
		$this->sheet = $sheet;
	}

	public function run() {
        $values = [];
        if (is_null($this->reader)) {
            $options = LIBXML_COMPACT | LIBXML_PARSEHUGE;
            \PhpOffice\PhpSpreadsheet\Settings::setLibXmlLoaderOptions($options);
            $this->reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($this->filename);
            if (!empty($this->sheet)) {
                $this->reader->setLoadSheetsOnly($this->sheet);
            }
            $this->findBounds();
            $this->countTotalRows();
            $this->calculateChunkSize();
        }
        $chunkFilter = new ChunkReadFilter();

        if ($this->startRow <= $this->bounds['end']) {
            $chunkFilter->setRows($this->startRow, $this->chunkSize);
            $this->reader->setReadFilter($chunkFilter);
            $excel = $this->reader->load($this->filename);

            if (!empty($this->sheet)) {
                $sheets = $excel->getSheetNames();
                if (!in_array($this->sheet, $sheets)) {
                    return $values;
                }
            }

            $sheet = $excel->getSheet(0);
            $this->sheet = $sheet->getTitle();

            $chunkRow = $this->startRow + $this->chunkSize;
            $endRow = $chunkRow > $this->bounds['end'] ? $this->bounds['end'] : $chunkRow;
            $endRow += ($this->startRow + $this->chunkSize) >= $this->bounds['end'] ? 1 : 0;
            for ($row = $this->startRow; $row < $endRow; $row++) {
                $this->currentRow = $row;
                $values[$row] = $this->getRowValues($sheet);

            }
            $this->startRow += $this->chunkSize;
            unset($excel);
        }
        return $values;
	}

    private function getRowValues($sheet) {
        $values = [];
        foreach ($this->settings as $column) {
            $ceil = $column['column'] . $this->currentRow;
            $values[$column['column']] = $this->getCellValue($ceil, $sheet);
        }
        return $values;
    }

    private function getCellValue($ceil, $sheet) {
        $value = '';
        preg_match("/([A-Za-z]+)(\d+)/", $ceil, $match);
        $isCellExists = !empty($match);
        $cell = $isCellExists ? $sheet->getCell($ceil) : NULL;
        if ($isCellExists && \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell)) {
            $calcValue = $cell->getOldCalculatedValue();
            $calcValue = $calcValue === null ? $cell->getCalculatedValue() : $calcValue;
            $date = is_numeric($calcValue) ? \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($calcValue) : NULL;
            if (!is_numeric($calcValue)) {
                $value = $calcValue;
            } else if (is_numeric($calcValue) && fmod($calcValue, 1) == 0) {
                $value = !empty($date) ? $date->format('d.m.Y') : '';
            } else if (is_numeric($calcValue) && $calcValue < 1) {
                $value = !empty($date) ? $date->format('H:i:s') : '';
            } else {
                $value = !empty($date) ? $date->format('d.m.Y H:i') : '';
            }
        } else if ($isCellExists) {
            $value = $cell->getOldCalculatedValue();
            $value = $value === null ? $cell->getCalculatedValue() : $value;
        } else {
            $value = '';
        }
        return $value;
    }

    private function countTotalRows() {
        if ($this->reader) {
            if (is_null($this->bounds)) {
                $this->findBounds();
            }

            $this->totalRows = $this->bounds['end'] - $this->bounds['start'] + 1;
            $this->totalRows = $this->totalRows < 0 ? 0 : $this->totalRows;
        }
    }

    private function findBounds() {
        if ($this->reader) {
            $excel = $this->reader->load($this->filename);
            $sheet = $excel->getSheet(0);
            $this->bounds = ['start' => 1, 'end' => 0];
            $this->bounds['end'] = $sheet->getHighestDataRow();
        }
    }

    private function calculateChunkSize() {
        $columns = count($this->settings);
        $tempChunkSize = 1000 / ($columns == 0 ? 1 : $columns);
        $arrayChunkSize = explode('.', $tempChunkSize);
        $precission = -1 * (strlen($arrayChunkSize[0]) - 1);
        $roundChunkSize = round($tempChunkSize, $precission);
        $chunkSize = $roundChunkSize > $this->totalRows ? $this->totalRows : $roundChunkSize;
        $chunkSize = $chunkSize == 0 ? 1 : $chunkSize;
        $this->chunkSize = $chunkSize;
    }
}