<?php

namespace Dzeta\Core\Import;

class ChunkReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    private $_startRow = 0;
    private $_endRow = 0;
    private $_rows = array();

    public function setRows($startRow, $chunkSize) {
        $this->_startRow = $startRow;
        $this->_endRow = $startRow + $chunkSize;
    }

    public function setRowsArray($rows) {
        $this->_rows = $rows;
    }

    public function readCell($column, $row, $worksheetName = '') {
        if (($row >= $this->_startRow && $row < $this->_endRow) || in_array($row, $this->_rows)) {
            return true;
        }
        return false;
    }
}