<?php

namespace Dzeta\Core;

class Entity implements \JsonSerializable
{
	/**
	 * Identifier of the enitty
	 *
	 * @var string
	 */
	private $uid;

	/**
	 * Name of the entity
	 *
	 * @var string
	 */
	private $name;

	private $options = [];

	function __construct(string $uid, string $name) {
		$this->uid = $uid;
		$this->setName($name);
	}

	public function getUid() {
		return $this->uid;
	}

	public function getName() {
		return $this->name;
	}

	public function getOption(string $key) {
		return isset($this->options[$key]) ? $this->options[$key] : null;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setName($name) {
		$name = preg_replace('|\s+|', ' ', $name);
		$this->name = trim($name);
		return $this;
	}

	public function setUid($uid) {
		$this->uid = $uid;
		return $this;
	}

	public function setOption(string $key, $value) {
		$this->options[$key] = $value;
		return $this;
	}

	public function setOptions(array $options) {
		$this->options = $options;
		return $this;
	}

	public function jsonSerialize() {
		$vars = get_object_vars($this);
		return $vars;
	}

	public static function GenerateUID()
	{
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        mt_rand( 0, 0xffff ),

        mt_rand( 0, 0x0fff ) | 0x4000,

        mt_rand( 0, 0x3fff ) | 0x8000,

        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
	}
}
