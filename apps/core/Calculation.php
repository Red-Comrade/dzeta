<?php

namespace Dzeta\Core;
use Dzeta\Core\Type\Type;

class Calculation extends Entity implements \JsonSerializable
{
	/**
	 * Formula of the calculation
	 *
	 * @var string
	 */
	private $formula = '';
	private $settings=[];

	/**
	 *  Type of the calc
	 *
	 * @var Type
	 *
	 */
	private $type;

	function __construct(string $uid, string $name, string $formula = '') {
		parent::__construct($uid, $name);
		$this->formula = $formula;
	}
	public function getType() {
		return $this->type;
	}

	public function setType(Type $type) {
		 $this->type = $type;
	}

	public function setFormula(string $formula) {
		$this->formula=$formula;
	}
	public function getFormula() {
		return $this->formula;
	}

	 public function jsonSerialize() {
      $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        return array_merge($vars, $parentVars);
    }
}
