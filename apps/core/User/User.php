<?php

namespace Dzeta\Core\User;

use Dzeta\Core\Type\Type;
use Dzeta\Core\Instance;

class User extends Instance
{
	const USER_TYPE_UID = '00000000-0000-0000-0000-000000000001';
	const SYSTEM_USER_UID = '00000000-0000-0000-0000-000000000003';

	/**
	 * Password of the user
	 *
	 * @var string
	 */
	private $password;

	private $permission_session=['module'=>[],'controller'=>[]];

	/**
	 * IP of the user when login
	 *
	 * @var string
	 */
	private $ip;

	function __construct(string $uid, string $login, ?UserGroup $parent = null, array $attributes = []) {
		$parentType = is_null($parent) ? null : $parent->getType();
		parent::__construct($uid, $login, new Type(self::USER_TYPE_UID, '', true, $parentType), $parent, $attributes);
	}

	public function setPermissionSession($key, $val)
	{
			$this->permission_session[$key]=$val;
	}

	public function getPermissionSession($key)
	{
			if (isset($this->permission_session[$key])) {
					return $this->permission_session[$key];
			}
			return null;
	}

	public function getPassword() {
		return $this->password;
	}

	public function getIP() {
		return $this->ip;
	}

	public function setPassword(string $password) {
		$this->password = $password;
		return $this;
	}

	public function setIP(string $ip) {
		$this->ip = $ip;
		return $this;
	}

	public static function GeneratePassword()
	{
			$random = new \Phalcon\Security\Random();
			$password = $random->base62(10);
			return $password;
	}
}
