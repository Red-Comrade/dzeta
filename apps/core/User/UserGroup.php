<?php

namespace Dzeta\Core\User;

use Dzeta\Core\Type\Type;
use Dzeta\Core\Instance;

class UserGroup extends Instance
{
	const USER_GROUP_TYPE_UID = '00000000-0000-0000-0000-000000000002';

	private $permission=[];
	function __construct(string $uid, array $attributes, array $users = []) {
		parent::__construct($uid, '', new Type(self::USER_GROUP_TYPE_UID, '', true, new Type(Type::TYPE_ROOT, '', true)), new Instance(Instance::INSTANCE_ROOT, '', new Type(Type::TYPE_ROOT, '', true)), $attributes, $users);
	}

	public function setPermission(array $perm)
	{
			$this->permission=$perm;
	}
	public function getPermisson()
	{
			return $this->permission;
	}
}
