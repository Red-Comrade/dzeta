<?php

namespace Dzeta\Core;

class ValueV implements \JsonSerializable
{

    protected $value;

    function __construct( $value) {
  		$this->value = $value;

  	}


    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
      $this->value=$value;
    }

    public function jsonSerialize() {
      $vars = get_object_vars($this);
      return $vars;
    }
}
