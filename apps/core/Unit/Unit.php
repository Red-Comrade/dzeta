<?php

namespace Dzeta\Core\Unit;

use \Dzeta\Core\Entity;
use \Dzeta\Core\Calculation;

class Unit extends Entity
{
	/**
	 * Main unit flag
	 *
	 * @var bool
	 */
	private $isMain;

	/**
	 * Calculation for conversion from current to main unit
	 *
	 * @var Calculation
	 */
	private $fromCalculation;

	/**
	 * Calculation for conversion from main to current unit
	 *
	 * @var Calculation
	 */
	private $toCalculation;

	function __construct(string $uid, string $name, bool $isMain, Calculation $fromCalculation, Calculation $toCalculation) {
		parent::__construct($uid, $name);
		$this->isMain = $isMain;
		$this->fromCalculation = $fromCalculation;
		$this->toCalculation = $toCalculation;
	}

	public function getFromCalculation() {
		return $this->fromCalculation;
	}

	public function getToCalculation() {
		return $this->toCalculation;
	}
}
