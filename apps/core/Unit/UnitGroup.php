<?php

namespace Dzeta\Core\Unit;

use \Dzeta\Core\Entity;

class UnitGroup extends Entity
{
	/**
	 * Units of the unit group
	 *
	 * @var Unit[]
	 */
	private $units = [];

	function __construct(string $uid, string $name, array $units = []) {
		parent::__construct($uid, $name);
		$this->units = $units;
	}

	public function getUnits() {
		return $this->units;
	}
}
