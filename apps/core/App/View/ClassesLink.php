<?php

namespace Dzeta\Core\App\View;


class ClassesLink implements \JsonSerializable
{

	/**
     * Main Type in view
     *
     * @var Dzeta\Core\Type
     */
	private \Dzeta\Core\Type\Type $MainClass;
	/**
     * Dependent Type in view
     *
     * @var Dzeta\Core\Type
     */
	private ?\Dzeta\Core\Type\Type $DependentClass;

	/**
     * Settings relations
     *
     * @var 
     */
	private $Settings;
	function __construct(\Dzeta\Core\Type\Type $MainClass,\Dzeta\Core\Type\Type $DependentClass,?\Dzeta\Core\App\View\SettClassesLink $settings=null ){
		$this->MainClass=$MainClass;
		$this->DependentClass=$DependentClass;
		$this->Settings=$settings;
	}

	public function setMainClass(Dzeta\Core\Type $MainClass){
		$this->MainClass=$MainClass;
		return $this;
	}
	public function getMainClass(){
		return $this->MainClass;
	}

	public function setDependentClass(Dzeta\Core\Type $DependentClass){
		$this->DependentClass=$DependentClass;
		return $this;
	}
	public function getDependentClass(){
		return $this->DependentClass;
	}
	public function getSettings(){
		return $this->Settings;
	}


	public function jsonSerialize()
    {
        $vars = get_object_vars($this);
       
        return $vars;
    }

}


