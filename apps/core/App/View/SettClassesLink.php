<?php

namespace Dzeta\Core\App\View;

class SettClassesLink implements \JsonSerializable
{

	/**
     * Attribute one-row mode
     *
     * @var Dzeta\Core\Type\Attribute
     */
	public ?\Dzeta\Core\Type\Attribute $single_mode_attr=null;

	/**
	* Type one-row mode
	* max или min
	*/
	
	public $single_mode_op=null;

	function __construct($settings){
		if(isset($settings['single_mode_attr']))
		{
			$this->single_mode_attr=$settings['single_mode_attr'];
		}
		if(isset($settings['single_mode_op']))
		{
			$this->single_mode_op=$settings['single_mode_op'];
		}
		
	}

	public function getSingleModeAttr(){
		return $this->single_mode_attr;
	}

	public function setSingleModeAttr(\Dzeta\Core\Type\Attribute $attrSingleMode){
		$this->single_mode_attr=$attrSingleMode;
		return $this;
	}

	public function jsonSerialize()
    {
        $vars = get_object_vars($this);
       
        return $vars;
    }


}