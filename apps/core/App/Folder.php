<?php

namespace Dzeta\Core\App;

use Dzeta\Core\Entity;

class Folder extends Entity implements \JsonSerializable
{
	/**
	 * Folder's settings
	 *
	 * @var array
	 */
	private array $settings = [];

	/**
	 * Order of views in folder
	 *
	 * @var array
	 */
	private array $order = [];

	public function __construct(string $uid, string $name, array $settings = [], array $order = []) {
		parent::__construct($uid, $name);
		$this->setSettings($settings);
		$this->setOrder($order);
	}

	public function getSettings() {
		return $this->settings;
	}

	public function getOrder() {
		return $this->order;
	}

	public function setSettings(array $settings) {
		$this->settings = $settings;
		return $this;
	}

	public function setOrder(array $order) {
		$this->order = $order;
		return $this;
	}

    public function jsonSerialize() {
        $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        return array_merge($vars, $parentVars);
    }
}
