<?php

namespace Dzeta\Core\App;

use Dzeta\Core\Entity;

class App extends Entity implements \JsonSerializable
{
//	const TYPE_ROOT = '00000000-0000-0000-0000-000000000000';
    /**
     * Attribute groups of the type
     *
     * @var View[]
     */
    private $views = [];
    private $settings=[];
    private $HasViews = false;

    function __construct(string $uid, string $name, array $views = [], array $settings=[]) {
  		parent::__construct($uid, $name);
  		$this->views = $views;
      $this->settings=$this->checkValidateSettings($settings);
  	}
    public function setSettings($settings) {
      $this->settings=$this->checkValidateSettings($settings);
      return $this;
    }

    public function setViews($views)
    {
      $this->views = $views;
    }
    public function setHasViews($HasViews)
    {
      $this->HasViews = $HasViews;
    }

    public function getSettings() {
      return  $this->settings;
    }

    public function getHasViews() {
        return  $this->HasViews;
    }

    private function checkValidateSettings($settings)
    {
        $settings=empty($settings)? []:$settings;
        $DefaultSettings=[
          'app_color'=>['defauleValue'=>'#ffffff'],
          'icon_settings'=>['defauleValue'=>null]

        ];
        $newSett=[];
        foreach ($settings as $key => $value)
        {
          if(isset($DefaultSettings[$key]))
          {
            if(isset($DefaultSettings[$key]['possible_value']))
            {
              if(in_array($value, $DefaultSettings[$key]['possible_value']))
              {
                $newSett[$key]=$value;
                unset($DefaultSettings[$key]);
              }
            }else{
                $newSett[$key]=$value;
                unset($DefaultSettings[$key]);
            }
          }
        }
        foreach ($DefaultSettings as $key => $value) {
          $newSett[$key]=$value['defauleValue'];
        }
        return $newSett;
    }


    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        return array_merge($vars, $parentVars);
    }

}
