<?php

namespace Dzeta\Core\App;

use Dzeta\Core\Entity;
use Dzeta\Core\App\App;
class View extends Entity implements \JsonSerializable
{
  	const ViewApp_ROOT = '00000000-0000-0000-0000-000000000000';



    private $parentView=null;
    private $parentApp=null;
    /**
     * Attribute groups of the type
     *
     * @var View[]
     */
    private $views = [];

    private $tables = [];

    private $ClasseLinks=[];

    private $settings=[];

    private $setTableOrForm=[];

    private $dopSett=[];

    private $attrs=[];

    private $HasViews = false;


    function __construct(string $uid, string $name,array $settings=[],?View $parentView=null,array $views = [], array $tables = []) {
  		parent::__construct($uid, $name);
  		$this->views      = $views;
  		$this->tables      = $tables;
        $this->parentView = $parentView;
        $this->settings   = $this->checkValidateSettings($settings);
  	}
    public function setDopSett($key,$val)
    {
        $this->dopSett[$key]=$val;
    }

    public function getDopSett($key)
    {
        if(isset($this->dopSett[$key]))
        {
          return $this->dopSett[$key];
        }
        return null;
    }

    public function setSetting($settings)
    {
        $this->settings = $this->checkValidateSettings($settings);
        return $this;
    }

    public function setParentApp(App $application) {
  		$this->parentApp = $application;
  	}
    public function setHasViews($HasViews) {
  		$this->HasViews = $HasViews;
  	}
  	public function setViews(array $views = []) {
        if(empty($views))
            $views = [];
        $this->views = $views;
  	}
  	public function setTables(array $tables = []) {
        if(empty($tables))
            $tables = [];
        $this->tables = $tables;
  	}
  	public function setTableOrForm($setTableOrForm) {
        $this->setTableOrForm = intval($setTableOrForm);
  	}
    public function getParentApp() {
        return  $this->parentApp;
    }
    public function getParentView() {
        return  $this->parentView ;
    }
    public function getHasViews() {
        return  $this->HasViews ;
    }
    public function getSettingsJSON() {
        return  json_encode($this->settings);
    }
    public function getSettings() {
        return  $this->settings;
    }

   /* public function addClassesLink($mainClass,$class,$settings=[])
    {
      $this->ClasseLinks[]=['MainClass'=>$mainClass,'Class'=>$class,'Settings'=>$settings];
    }*/
    public function addClassesLink(\Dzeta\Core\App\View\ClassesLink $classesLink)
    {
      $this->ClasseLinks[]=$classesLink;
    }


    public function getClassesLink()
    {
      return $this->ClasseLinks;
    }


    private function checkValidateSettings($settings)
    {
        $DefaultSettings=[
          'display_pers_obj'=>['defauleValue'=>0,'possible_value'=>[0,1,2,3,4]],
          'table_or_form'=>['defauleValue'=>0,'possible_value'=>[0,1,2,3]],
          'charts'=>['defauleValue'=>[]/*,'possible_value'=>[]*/],
          'display_number_of_row'=>['defauleValue'=>0,'possible_value'=>[0,1]],
          'apply_filter_before_or_after_single_mode'=>['defauleValue'=>null,'possible_value'=>[0,1]]
        ];
        $newSett=[];
        foreach ($settings as $key => $value)
        {          
          if(isset($DefaultSettings[$key]))
          {
            if(isset($DefaultSettings[$key]['possible_value']))
            {
              if($value=="")
              {
                $value=null;
              }
              if(in_array($value, $DefaultSettings[$key]['possible_value']))
              {                
                $newSett[$key]=$value;
                unset($DefaultSettings[$key]);
              }
            }else
            {
              $newSett[$key]=$value;
              unset($DefaultSettings[$key]);
            }
            
          }else{
            $newSett[$key]=$value;
          }
        }
        foreach ($DefaultSettings as $key => $value) {
          $newSett[$key]=$value['defauleValue'];
        }
        
        return $newSett;
    }

    public function setAttrinutes($attrs)
    {
        $this->attrs=$attrs;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        return array_merge($vars, $parentVars);
    }




}
