<?php

namespace Dzeta\Core\Value;

use Dzeta\Core\ValueV;

class File extends ValueV implements \JsonSerializable
{
    const FILE_FOLDER='/ufiles/';
    const FILE_EXT='.file';
    const PREVIEW_EMPTY='00000000-0000-0000-0000-000000000000';

    /**
     * Path preview  file
     *
     * @var string|null
     */
    private $preview=null;
    /**
     * Size  file
     *
     * @var float
     */
    private $size=0;

    private $name='';
    public function __construct(string $value,string  $realName='', ?string $preview=null, float $size=0)
    {
        parent::__construct($value);
        $this->name=$realName;
        $this->size=$size;
        if($preview!=File::PREVIEW_EMPTY&&!empty($preview))
        {
            $this->preview=$preview;
        }

    }

  /*  public function __construct(string $uid, string  $realName='', ?string $preview=null, float $size=0)
    {
        parent::__construct($uid, $realName);
        $this->size=$size;
        if($preview!=File::PREVIEW_EMPTY&&!empty($preview))
        {
            $this->preview=$preview;
        }
    }*/

    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * Get Preview path
     *
     * @return string
     */
    public function getPreviewDB()
    {
        if ($this->preview==null) {
            return self::PREVIEW_EMPTY;
        }
        return $this->preview;
    }
    /**
     * Get size	 file
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
  	 * Set size file
  	 *
  	 * @var string
  	 */
    public function setSize(float $size)
    {
        $this->size=$size;
    }
    public function getName()
    {
      return $this->name;
    }
    public function setName(string $name)
    {
      $this->name=$name;
    }

    public function getPath()
    {
      return BASE_PATH . self::FILE_FOLDER . $this->getValue() . self::FILE_EXT;
    }


    /**
     * Generate uid of file
     *
     *
     * @return string|null
     */
    public static function GenerateUIDFile()
    {
        $i=0;
        $uuidd=null;
        while (1) {
            $uuidd=\Dzeta\Core\Entity::GenerateUID();
            if (!file_exists(BASE_PATH.self::FILE_FOLDER.$uuidd.self::FILE_EXT)) {
                break;
            }
            $i++;
            if ($i>10) {
                break;
            }
        }
        return $uuidd;
    }

    public static function downloadFile(File $file)
    {
        header('Content-Type: application/octet-stream;charset=utf-8');
    		header('Content-Disposition: attachment; filename="' . $file->getName(). '";');
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate');
    		header('Pragma: public');
    		readfile(BASE_PATH.self::FILE_FOLDER.$file->getValue().self::FILE_EXT);
    }


    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        $arrM=array_merge($vars, $parentVars);
        $arrM['uid']=$arrM['value'];
        unset($arrM['value']);
        return $arrM;
    }
}
