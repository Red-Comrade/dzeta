<?php

namespace Dzeta\Core\Value;

use Dzeta\Core\ValueV;

class DateTimeV extends ValueV implements \JsonSerializable
{

      const DATE_FORMAT = 'd/m/Y H:i:s';
      const DATE_FORMAT_DB = 'Y/m/d H:i:s';
      const DATE_FORMAT_new = 'd/m/Y';
      const DATE_FORMAT_SEARCH = 'm/d/Y';

      function __construct($value)
      {
          parent::__construct($value);
      }
      public function getValueN()
      {
          return $this->value;
      }
      public function getValue()
      {
          return $this->value->format(self::DATE_FORMAT);
      }
      public function getValueDB()
      {
          return $this->value->format(self::DATE_FORMAT_DB);
      }

      public function jsonSerialize()
      {
          $vars = get_object_vars($this);
          $parentVars = parent::jsonSerialize();
          $arrM=array_merge($vars, $parentVars);
          $arrM['value']=$arrM['value']->format(self::DATE_FORMAT);
          return $arrM;
      }

}
