<?php

namespace Dzeta\Core\Value;

use Dzeta\Core\ValueV;

class CounterV extends ValueV implements \JsonSerializable
{

    public function __construct($value)
    {
        parent::__construct(self::cleanValue($value));
    }


    public function setValue($value)
    {
        $this->value=self::cleanValue($value);
    }

    public static function cleanValue($value)
    {
        if(is_null($value))
        {
          return null;
        }
        $value=$value."";
        if (strpos($value, ".")||strpos($value, ",")) {
            $value = rtrim($value."", '0');
            $len = strlen($value);
            if ($len>0&&($value[$len - 1] == ','||$value[$len - 1] == '.')) {
                $value = substr_replace($value, "", -1);
            }
        }
        $value=(float)$value;
        return $value;
    }
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        return array_merge($vars, $parentVars);
    }
}
