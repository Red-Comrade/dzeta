<?php
namespace Dzeta\Core\Value;

use Dzeta\Core\ValueV;

class TextV extends ValueV implements \JsonSerializable
{


  function __construct($value)
  {
      parent::__construct($value);
  }

  public function setValue($value)
  {
        $value=$value."";
        if(strpos($value,".")||strpos($value,","))
        {
          $value = rtrim($value."", '0');
          $len = strlen($value);
          if ($len>0&&($value[$len - 1] == ','||$value[$len - 1] == '.')) {
            $value = substr_replace($value, "", -1);
          }

          $value=(float)$value;
          $value=$value."";

        }
        $this->value=(float)$value;
  }


  public function jsonSerialize()
  {
      $vars = get_object_vars($this);
      $parentVars = parent::jsonSerialize();
      return array_merge($vars, $parentVars);
  }

}
