<?php

namespace Dzeta\Core\Value;

use Dzeta\Core\ValueV;

class NumV extends ValueV implements \JsonSerializable
{
    private $convertedValue=null;
    public function __construct($value,$convertedValue)
    {
        parent::__construct(self::cleanValue($value));
        $this->convertedValue=self::cleanValue($convertedValue);
    }


    public function setValue($value)
    {
        $this->value=self::cleanValue($value);
    }

    public static function cleanValue($value)
    {
        if(is_null($value))
        {
          return null;
        }
        $value=$value."";
        if (strpos($value, ".")||strpos($value, ",")) {
            $value = rtrim($value."", '0');
            $len = strlen($value);
            if ($len>0&&($value[$len - 1] == ','||$value[$len - 1] == '.')) {
                $value = substr_replace($value, "", -1);
            }

        }
        $value=(float)$value;
        return $value;
    }

    public function getConvertedValue()
    {
      //  return $this->convertedValue;
        return 0;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        $parentVars = parent::jsonSerialize();
        return array_merge($vars, $parentVars);
    }
}
