<?php

namespace Dzeta\Core;

use Phalcon\Cache;
use Phalcon\Cache\AdapterFactory;
use Phalcon\Storage\SerializerFactory;

class Progress
{
	const KEY_DELIMETER = '-';

	private $sessionName;

	private $uid;

	private $cache;

	private $name;

	public function __construct(?string $uid = null, ?string $sessionName = null) {
		if (is_null($uid)) {
			$this->uid = Entity::GenerateUID();
		} else {
			$this->uid = $uid;
		}
		$this->sessionName = is_null($sessionName) ? 'progress' : $sessionName;
		$this->init();
		if (!$this->isExistCurrentValue()) {
			$this->setCurrentValue(['current' => 0, 'count' => 0]);
		}
	}

	public function getValue() {
		return $this->getCurrentValue();
	}

	public function getCount() {
		$value = $this->getCurrentValue();
		return isset($value['count']) ? $value['count'] : 0;
	}

	public function getCurrent() {
		$value = $this->getCurrentValue();
		return isset($value['current']) ? $value['current'] : 0;
	}

	public function setCount($count) {
		$value = $this->getCurrentValue();
		$value['count'] = $count;
		$this->setCurrentValue($value);
	}

	public function setCurrent($current) {
		$value = $this->getCurrentValue();
		$value['current'] = $current;
		$this->setCurrentValue($value);
	}

	public function increaseCurrent() {
		$value = $this->getCurrentValue();
		$value['current']++;
		$this->setCurrentValue($value);
	}

	private function init() {
		$serializerFactory = new SerializerFactory();
		$adapterFactory = new AdapterFactory($serializerFactory);

		$options = [
		    'defaultSerializer' => 'Php',
		    'lifetime' => 0
		];

		$adapter = $adapterFactory->newInstance('apcu', $options);

		$this->cache = new Cache($adapter);
		$this->name = $this->sessionName . self::KEY_DELIMETER . $this->uid;
	}

	private function isExistCurrentValue() {
		return $this->cache->has($this->name);
	}

	private function getCurrentValue() {
		return $this->cache->get($this->name);
	}

	private function setCurrentValue($value) {
		$this->cache->set($this->name, $value);
	}
}